package com.svarochi.wifi.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

/**
 * Created by rahul.dhanuka on 12/12/2017.
 */

public class CommonUtils {

    /**
     * open soft keyboard.
     *
     * @param context
     * @param view
     */
    public static void showKeyBoard(Context context, View view) {
        try {
            InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(view, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open soft keyboard.
     *
     * @param mActivity context
     */
    public static void showKeyBoard(Activity mActivity) {
        try {
            View view = mActivity.getCurrentFocus();
            if (view != null) {
                InputMethodManager keyboard = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInputFromInputMethod(view.getWindowToken(), InputMethodManager.SHOW_FORCED);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open soft keyboard.
     *
     * @param mFragment context
     */
    public static void showKeyBoard(Fragment mFragment) {
        try {
            if (mFragment == null || mFragment.getActivity() == null) {
                return;
            }
            View view = mFragment.getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager keyboard = (InputMethodManager) mFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInputFromInputMethod(view.getWindowToken(), InputMethodManager.SHOW_FORCED);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * hide soft keyboard.
     *
     * @param context
     * @param view
     */
    public static void hideKeyBoard(Context context, View view) {
        try {
            InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * close opened soft keyboard.
     *
     * @param mActivity context
     */
    public static void hideSoftKeyboard(Activity mActivity) {
        try {
            View view = mActivity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * close opened soft keyboard.
     *
     * @param mFragment context
     */
    public static void hideSoftKeyboard(Fragment mFragment) {
        try {
            if (mFragment == null || mFragment.getActivity() == null) {
                return;
            }
            View view = mFragment.getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) mFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * toggle soft keyboard.
     *
     * @param context
     */
    public static void toggleSoftKeyboard(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isValidEmail(CharSequence target) {
        return target != null && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void showToast(Context context, String message) {
        if (context != null && !TextUtils.isEmpty(message)) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public static void viewBrowser(Context context, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        PackageManager packageManager = context.getPackageManager();
        if (browserIntent.resolveActivity(packageManager) != null) {
            context.startActivity(browserIntent);
        } else {
            showToast(context, "Install app or web browser");
        }
    }

    public static void openLinkInBrowser(Context context, String url) {

    }


    public static String getYoutubeVideoCodeFromURL(String youtubeVideoURL) {
        String videoCode = youtubeVideoURL.substring(youtubeVideoURL.lastIndexOf("/") + 1, youtubeVideoURL.length());
        videoCode = videoCode.substring(videoCode.lastIndexOf("=") + 1, videoCode.length());
        return videoCode;
    }

    public static void watchYoutubeVideo(Context context, String id) {
        try {
            Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
            // Check if the youtube app exists on the device
            if (appIntent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(appIntent);
                return;
            }

            // If the youtube app doesn't exist, then use the browser
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + id));
            if (webIntent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(webIntent);
                return;
            }

            showToast(context, "Install YouTube app or web browser");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openEmail(Context context, String email) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            if (emailIntent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(emailIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openDialer(Context context, String phone) {
        Intent dialerIntent = new Intent(Intent.ACTION_DIAL);
        dialerIntent.setData(Uri.parse("tel:" + phone));
        if (dialerIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(dialerIntent);
        }
    }

    public static void sleepUI(long timePeriod) {
        try {
            Thread.sleep(timePeriod);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static SpannableStringBuilder getSuperScriptSmallString(String completeText, String textToSuperscript) {
        return showSmallSizeText(showSuperscriptText(new SpannableStringBuilder(completeText), completeText, textToSuperscript), completeText, textToSuperscript);
    }

    public static SpannableStringBuilder getSubScriptSmallString(String completeText, String textToSubscript) {
        return showSmallSizeText(showSubscriptText(new SpannableStringBuilder(completeText), completeText, textToSubscript), completeText, textToSubscript);
    }

    // Custom method to make text size small
    protected static SpannableStringBuilder showSmallSizeText(SpannableStringBuilder mSSBuilder, String completeText, String textToSmall) {
        // Initialize a new RelativeSizeSpan instance
        RelativeSizeSpan relativeSizeSpan = new RelativeSizeSpan(1f);

        if (mSSBuilder == null) {
            mSSBuilder = new SpannableStringBuilder(completeText);
        }

        // Apply the RelativeSizeSpan to display small text
        mSSBuilder.setSpan(
                relativeSizeSpan,
                completeText.indexOf(textToSmall),
                completeText.indexOf(textToSmall) + String.valueOf(textToSmall).length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return mSSBuilder;
    }

    // Custom method to make text subscript
    protected static SpannableStringBuilder showSubscriptText(SpannableStringBuilder mSSBuilder, String completeText, String textToSubscript) {
        // Initialize a new SubscriptSpan instance
        SubscriptSpan subscriptSpan = new SubscriptSpan();

        if (mSSBuilder == null) {
            mSSBuilder = new SpannableStringBuilder(completeText);
        }

        // Apply the SubscriptSpan
        mSSBuilder.setSpan(
                subscriptSpan,
                completeText.indexOf(textToSubscript),
                completeText.indexOf(textToSubscript) + String.valueOf(textToSubscript).length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return mSSBuilder;
    }

    // Custom method to make text Superscript
    protected static SpannableStringBuilder showSuperscriptText(SpannableStringBuilder mSSBuilder, String completeText, String textToSuperscript) {
        // Initialize a new SuperscriptSpan instance
        SuperscriptSpan subscriptSpan = new SuperscriptSpan();

        if (mSSBuilder == null) {
            mSSBuilder = new SpannableStringBuilder(completeText);
        }

        // Apply the SuperscriptSpan
        mSSBuilder.setSpan(
                subscriptSpan,
                completeText.indexOf(textToSuperscript),
                completeText.indexOf(textToSuperscript) + String.valueOf(textToSuperscript).length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return mSSBuilder;
    }

    public static String getBundleKeyValues(Bundle bundle) {
        StringBuilder builder = new StringBuilder();
        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            builder.append("[");
            builder.append(key);
            builder.append("]=[");
            builder.append(value);
            builder.append("](");
            builder.append((value != null) ? value.getClass().getSimpleName() : "null");
            builder.append("), ");
        }
        return builder.toString();
    }
}
