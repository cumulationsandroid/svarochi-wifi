package com.svarochi.wifi.common

open class Constants {
    companion object {

        @kotlin.jvm.JvmField
        val DEFAULT_FIRMWARE_VERSION: String = ""

        /*Database*/
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME: String = "WifiModuleDatabase.db"

        // To identify svarochi lamp hotspot
        const val SVAROCHI_LAMP_IDENTIFIER: String = "SVAROCHI_LAMP_"

        /*Max values*/
        val MAX_PROJECTS = 100
        val MAX_NETWORKS = 15
        val MAX_GROUPS = 6
        val MAX_SCENES = 10
        val MAX_WIFI_SCAN_COUNT = 2
        val MAX_LIGHTS_IN_GROUP = 10
        val MAX_LIGHTS_IN_SCENE = 10
        val MAX_LIGHTS_IN_SCHEDULE = 10


        /*Communication*/
        val DELIMITER = "7E"

        /*MQTT Broker*/
        var BROKER_DETAILS = "tcp://35.167.76.35:1884"
        //var BROKER_DETAILS = "tcp://192.168.0.134:1884"

        /*MQTT - JSON*/
        val JSON_STATUS = "Status"
        val JSON_FIRMWARE_VERSION = "FwVer"
        val JSON_FRAME_NUM = "FrameNum"
        val JSON_PACKET_ID = "PktId"
        val JSON_MAC_ID = "MacId"
        val JSON_R_VALUE = "R"
        val JSON_G_VALUE = "G"
        val JSON_B_VALUE = "B"
        val JSON_W_VALUE = "W"
        val JSON_C_VALUE = "C"
        val JSON_BRIGHTNESS_VALUE = "Br"
        val JSON_ON_OFF_VALUE = "OnOff"
        val JSON_ON_OFF_STATUS = "OnOffStatus"
        val JSON_RSSI = "Rssi"
        val JSON_PRESET_SCENE_VALUE = "PreSce"
        val JSON_SCENE_NUM = "SceneNum"
        val JSON_SCHEDULE_ID = "SchId"
        val JSON_SCHEDULE_START_TIME = "StTime"
        val JSON_SCHEDULE_STOP_TIME = "SpTime"
        val JSON_SCHEDULE_R_VALUE = "R"
        val JSON_SCHEDULE_G_VALUE = "G"
        val JSON_SCHEDULE_B_VALUE = "B"
        val JSON_SCHEDULE_W_VALUE = "W"
        val JSON_SCHEDULE_C_VALUE = "C"
        val JSON_SCHEDULE_BRIGHTNESS_VALUE = "Br"
        val JSON_FW_VERSION = "FwVersion"

        /*Connection*/
        val DEFAULT_LAMP_IP = "192.168.1.10"
        val DEFAULT_LAMP_PORT = 8000
        val DEFAULT_BCAST_PORT = 1234
        val BROADCAST_TIMEOUT: Long = 10
        val DEFAULT_MAC_ID = "000000000000"
        val BROADCAST_RETRY: Long = 2
        val CONNECTION_RETRY_COUNT = 1

        /*Authentication modes*/
        const val WPA2_PSK_SHA256 = "07"
        const val WPA2_CCKM = "06"
        const val WPA_CCKM = "05"
        const val WPA2_PSK = "04"
        const val WPA_PSK = "03"
        const val WPA2 = "02"
        const val WPA = "01"
        const val NO_AUTHENTICATION = "00"

        /*Encryption lampType*/
        val TKIP = "02"
        val AES = "03"
        val NO_ENCRYPTION = "00"

        /*Events*/
        val MAX_REACHED = 1
        val ALREADY_EXISTS = 2
        val CONNECTION_FAILURE = 3
        val LIGHT_CONTROL = 4

        /*Bundle*/
        val BUNDLE_LIGHT_NAME = "light_name"
        val BUNDLE_CUSTOM_SCENE_DEVICE = "custom_scene_device"
        val BUNDLE_LIGHT_MACID = "light_macid"
        val BUNDLE_LIGHT_TYPE = "light_type"
        val BUNDLE_LIGHT = "light"
        val BUNDLE_LIGHT_IP = "light_ip"
        val BUNDLE_NETWORK_ID = "networkId"
        val BUNDLE_GROUP = "group"
        val BUNDLE_GROUP_ID = "groupId"
        val BUNDLE_GROUP_NAMES = "groupNames"
        val BUNDLE_SCHEDULE_ID = "schedule_id"
        val BUNDLE_SCHEDULE = "schedule"
        val BUNDLE_EDIT_SCHEDULE = "edit_schedule"
        val BUNDLE_OPEN_SCHEDULE = 1
        val BUNDLE_RSSI_VALUE = "rssi_value"
        val BUNDLE_PROJECT_ID = "project_id"
        val BUNDLE_PROJECT_NAME = "project_name"
        const val BUNDLE_WIFI = "wifi"
        val BUNDLE_R_VALUE = "rValue"
        val BUNDLE_G_VALUE = "gValue"
        val BUNDLE_B_VALUE = "bValue"
        val BUNDLE_W_VALUE = "wValue"
        val BUNDLE_C_VALUE = "cValue"
        val BUNDLE_BRIGHTNESS_VALUE = "brightnessValue"
        val BUNDLE_DAYLIGHT_VALUE = "daylight_value"
        val BUNDLE_BLUETOOTH = "bluetooth"
        val BUNDLE_ON_OFF_STATUS = "onOffValue"
        val BUNDLE_ACTION_DETAILS = "actionDetails"
        val BUNDLE_SHARED_WITH = "shared_with"
        val BUNDLE_SHARED_WITH_ID = "shared_with_id"
        val BUNDLE_EDIT_PROFILE = "edit_profile"
        val BUNDLE_PROFILE_NAME = "profile_name"
        val BUNDLE_PROFILE = "profile"
        val BUNDLE_NETWORK_NAME = "network_name"
        val BUNDLE_LIGHT_TURNED_ON = "light_turnedOn"
        val BUNDLE_IMAGE_PICK_TYPE = "IMAGE_PICK_TYPE"
        val ADD_GROUP_ITEM = 1
        val GROUP_ITEM = 0
        val ADD_SCENE_ITEM = 1
        val SCENE_ITEM = 0
        val ADD_PROJECT_ITEM = 1
        val PROJECT_ITEM = 0
        val DEFAULT_LAMP_TYPE = "ZZZ"
        val BUNDLE_SSID = "ssid"
        val BUNDLE_PASSWORD = "password"
        val BUNDLE_SSID_TO_CONFIGURE = "ssid_to_configure"
        val BUNDLE_LAMP_SSID = "lamp_ssid"
        val BUNDLE_AUTHENTICATION_MODE = "authenticationMode"
        val BUNDLE_ENCRYPTION_TYPE = "encryptionType"
        val BUNDLE_SHOULD_INITIATE_TIMER = "should_initiate_timer"
        val BUNDLE_SELECTED_IDS = "selected_ids"
        val BUNDLE_START_TIME = "start_time"
        val BUNDLE_STOP_TIME = "stop_time"
        val BUNDLE_EDIT_NETWORK = "edit_network"
        val BUNDLE_NETWORK = "network"
        val BUNDLE_NETWORK_NAMES = "network_names"

        // No internet available
        val NO_INTERNET_AVAILABLE = 981

        // Project contains networks
        val CONTAINS_NETWORKS: Int = 922

        // Network contains devices
        val CONTAINS_DEVICES: Int = 946

        /*Group tags*/
        val GROUP_TAG_BRIGHTNESS_ONLY = 1
        val GROUP_TAG_DAYLIGHT_ONLY = 2
        val GROUP_TAG_COLOR_ONLY = 3
        val GROUP_TAG_BRIGHTNESS_AND_DAYLIGHT = 4
        val GROUP_TAG_BRIGHTNESS_AND_COLOR = 5
        val GROUP_TAG_DAYLIGHT_AND_COLOR = 6
        val GROUP_TAG_ALL = 7
        val GROUP_TAG_NONE = 8

        /*Intent*/
        val ACCESS_CAMERA_PERMISSIONS_REQUEST = 1
        val ACCESS_LOCATION_PERMISSIONS_REQUEST = 2
        val REQUEST_LOCATION_SETTINGS = 8
        val SET_LIGHT = 9
        val RECORD_PERMISSION = 10
        val SET_WIFI_CREDENTIALS = 9
        val READ_EXTERNAL_STORAGE_PERMISSION = 11

        /*Wifi setup states*/
        val INITIAL = "init"
        val CONNECTING_TO_SVAROCHI_WIFI = "Connecting to svarochi wifi"
        val CONNECTING_TO_SVAROCHI_TCP = "Establishing TCP connection"
        val WRITING_WIFI_CREDENTIALS = "Writing wifi credentials"


        /*Api response*/
        val BUNDLE_API_DIMMABLE = "dimmable"
        val BUNDLE_API_DAYLIGHT = "daylight"
        val BUNDLE_API_COLOR = "color"
        val BUNDLE_API_ON = "ON"

        // Timer
        val MQTT_TIMEOUT: Long = 10000
        val AZURE_MQTT_TIMEOUT: Long = 9
        val TCP_TIMEOUT = 4000

        // MQTT
        val NO_MQTT_CONNECTION = 12
        val NO_TCP_CONNECTION = 14

        // Connection Type
        val TCP_CONNECTION = 0
        val MQTT_CONNECTION = 1

        // Timestamps
        val API_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        // API Bundle
        val BUNDLE_API_GROUPS = "GROUP"
        val BUNDLE_API_LIGHTS = "LIGHT"

        const val CAMERA = 0
        const val PHOTO_GALLERY = 1

        // Custom Effect Color List
        val CUSTOM_EFFECT_PURPLE = 9699539
        val CUSTOM_EFFECT_DARK_PURPLE = 4915330
        val CUSTOM_EFFECT_BLUE = 255
        val CUSTOM_EFFECT_GREEN = 65280

        // Custom Effect BottomSheetItem
        val ITEM_CAMERA = 1
        val ITEM_PHOTO_GALLERY = 2
        val ITEM_COLOR_PALETTE = 3
        val ITEM_CANCEL = 4

        // Schedule Feature
        val SCHEDULE_RESET_START_TIME_MQTT: Long = 86401
        val SCHEDULE_RESET_END_TIME_MQTT: Long = 86400
        val SCHEDULE_RESET_START_TIME_TCP: String = "015181"
        val SCHEDULE_RESET_END_TIME_TCP: String = "015180"
        val SCHEDULE_RESET_VALUE_TCP = "00"
        val SCHEDULE_RESET_VALUE_MQTT = 0
        val SCHEDULE_BRIGHTNESS_RESET_VALUE_MQTT = 1
        val SCHEDULE_BRIGHTNESS_RESET_VALUE_TCP = "01"
        val INVALID_SLOT_NUMBER = 3
        val MAX_SCHEDULES = 5

        // Reset
        val RESET_OP_VALUE = "01"

        // Add New Light
        val ALREADY_EXISTS_WITH_ANOTHER_USER = 1231

        // User Profile
        val CURRENT_PROFILE_SELF = "self"

        // Light Sync Status
        val LIGHT_SYNCED = 1
        val LIGHT_UNSYNCED = 1

        // Device TCP disconnection timeout
        val LAMP_TCP_DISCONNECTION = 110000

        val MQTT_TRANSITION_DELAY = 300
        val TCP_TRANSITION_DELAY = 80
        val GROUP_TRANSITION_DELAY = 300

        const val LOCAL_AVAILABLE = true

        const val MAX_TCP_RETRY = 1

        const val YOUTUBE = "-26KmYbOy44"
        const val YOUTUBE_HOW_IT_WORKS = "XZ3ro9_T1c0"//"had67cbIixc";

        const val SVAROCHI_PAGE = "http://svarochi.com/"
        const val BUNDLE_NAVIGATE_FROM = "navigate_from"

        const val SCREEN_WIDTH = 720
        const val SCREEN_HEIGHT = 1280
        const val SCREEN_DENSITY = 2f

    }


}