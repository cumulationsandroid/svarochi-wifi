package com.svarochi.wifi.common

class Urls {

    companion object {
        //val BASE_URL = "http://35.167.76.35:8088/api/"
        //val BASE_URL = "https://testing.cumulations.com/svarochi/api/"
        val BASE_URL = "https://svarochiwifiapi.com/api/"

        const val GET_PROJECTS = "getprojects"
        const val ADD_PROJECT = "addproject"
        const val ADD_NETWORK = "addnetwork"
        const val GET_NETWORKS = "getnetworks"
        const val ADD_DEVICE = "adddevice"
        const val GET_DEVICES = "getdevices"
        const val ADD_GROUP = "addgroup"
        const val GET_GROUPS = "getgroups"
        const val UPDATE_GROUP = "updategroup"
        const val GET_GROUP_DETAILS = "getgroupdetails"
        const val GET_SCENES = "getscenes"
        const val ADD_SCENE = "addscene"
        const val GET_SCENE_DETAILS = "getscenedetails"
        const val UPDATE_SCENE = "updatescene"
        const val GET_SCHEDULES = "getschedules"
        const val ADD_SCHEDULE = "addschedule"
        const val EDIT_SCHEDULE = "updateschedule"
        const val DELETE_SCHEDULE = "deleteschedule"
        const val DELETE_DEVICE = "deletedevice"
        const val UPDATE_NETWORK = "updatenetwork"
        const val UPDATE_PROJECT = "updateproject"
        const val UPDATE_DEVICE = "updatedevice"
        const val DELETE_SCENE = "deletescene"
        const val DELETE_GROUP = "deletegroup"
        const val CHECK_USER = "checkuser"
        const val UPDATE_PROFILE = "updateprofile"
        const val CREATE_PROFILE = "createprofile"
        const val GET_SHARED_PROFILES = "getsharedprofiles"
        const val GET_SHARED_PROJECTS = "getsharedprojects"
        const val GET_PROFILES = "getprofiles"
        const val GET_SHARED_NETWORKS = "getsharednetworks"
        const val DELETE_PROJECT = "deleteproject"
        const val DELETE_NETWORK = "deletenetwork"
        const val GET_SHARED_DEVICES = "getshareddevices"
    }

}