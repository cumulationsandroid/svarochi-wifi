package com.svarochi.wifi.common

import android.content.Context
import android.graphics.Point
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.ScrollView
import com.svarochi.wifi.common.Constants.Companion.API_TIMESTAMP
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.database.entity.Scene
import com.svarochi.wifi.model.communication.BaseRequest
import com.svarochi.wifi.model.communication.LampType
import com.svarochi.wifi.model.communication.PacketType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.model.mqtt.commands.SetColourRequest
import com.svarochi.wifi.model.mqtt.commands.SetPresetSceneRequest
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import org.json.JSONObject
import retrofit2.Response
import timber.log.Timber
import java.math.BigInteger
import java.net.NetworkInterface
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.text.SimpleDateFormat
import java.util.*
import java.util.Collections.sort
import kotlin.experimental.and


class Utilities {

    companion object {
        fun stringToHexString(data: String): String {
            return String.format("%x", BigInteger(1, data.toByteArray()/*YOUR_CHARSET?*/))
        }

        fun hexToString(str: String): String {
            return String(BigInteger(str, 16).toByteArray())
        }

        fun stringToHexByteArray(packet: String, packetLength: Int): ByteArray {
            var len = packet.length
            var data = ByteArray(packetLength)

            var i = 0
            while (i < len) {
                data[i / 2] = ((Character.digit(packet[i], 16) shl 4) + Character.digit(packet[i + 1], 16)).toByte()
                i += 2
            }

            return data
        }

        fun getRandomSequenceNumber() = (9999999..100000000).random().toString()

        fun appendZerosToSSID(ssidHex: String): String {
            val len = ssidHex.length
            val zerosToAppend = 64 - len

            val ssidHexBuilder = StringBuilder(ssidHex)
            for (i in 0 until zerosToAppend) {
                ssidHexBuilder.append("0")
            }

            return ssidHexBuilder.toString()

        }

        fun appendZerosToPP(ppHex: String): String {
            val len = ppHex.length
            val zerosToAppend = 128 - len

            val ppHexBuilder = StringBuilder(ppHex)

            for (i in 0 until zerosToAppend) {
                ppHexBuilder.append("0")
            }
            return ppHexBuilder.toString()

        }

        fun toString(bytes: ByteArray): String {
            val sb = StringBuilder()
            sb.append("[ ")
            for (b in bytes) {
                sb.append(String.format("%02X ", b))
            }
            sb.append("]")
            return sb.toString()
        }

        fun hexByteToString(byte: Byte): String {
            return String.format("%02X", byte)
        }

        fun dipToPixels(context: Context, dipValue: Float): Float {
            val metrics = context.resources.displayMetrics
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics)
        }

        fun findRouterBCastAddress(): Observable<String> {
            return Observable.create(object : ObservableOnSubscribe<String> {
                override fun subscribe(emitter: ObservableEmitter<String>) {
                    try {
                        var addressFound = false
                        val en = NetworkInterface.getNetworkInterfaces()
                        while (en.hasMoreElements()) {
                            val ni = en.nextElement()
                            if (ni.displayName == "wlan0") {
                                val list = ni.interfaceAddresses
                                val it = list.iterator()

                                while (it.hasNext()) {
                                    val ia = it.next()
                                    if (ia != null && ia.broadcast != null) {
                                        addressFound = true
                                        emitter.onNext(ia.broadcast.hostAddress)
                                        break
                                    }
                                }
                                if (addressFound) {
                                    break
                                }
                            }
                        }
                        if (!addressFound) {
                            Timber.d("Broadcast address not found")
                            emitter.onError(Exception("No broadcast address found message"))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Timber.d("Broadcast address not found")
                        emitter.onError(Exception("No broadcast address found message"))
                    }
                }
            })
        }

        fun convertToIp(ipAddressOfLamp: String?): String {

            var ip = ""
            var i = 0
            while (i < ipAddressOfLamp!!.length) {
                ip = ip + Integer.valueOf(ipAddressOfLamp.substring(i, i + 2), 16) + "."
                i += 2
            }

            return ip.substring(0, ip.length - 1)
        }

        fun hexToIp(hex: String): String {
            var len = hex.length
            var i = 0
            var ip = ""
            while (i < len) {
                ip = ip + Integer.parseInt(hex.substring(i, i + 2), 16) + "."
                i += 2
            }
            return ip.substring(0, ip.length - 1)
        }


        fun convertColorToHexString(value: Int): String {
            return String.format("%06X", 0xFFFFFF and value)
        }

        fun convertIntToHexString(value: Int): String {
            return String.format("%02X", 0xFF and value)
        }

        fun getWValue(daylightValue: Int): String {
            return convertIntToHexString((255 * (1 - (daylightValue * 0.01))).toInt())
        }

        fun getCValue(daylightValue: Int): String {
            return convertIntToHexString((255 * (daylightValue * 0.01)).toInt())
        }

        /*fun showToast(context: Context, message: String) {
            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }*/

        fun getLampType(value: String): LampType? {
            for (e in LampType.values()) {
                if (e.value.equals(value)) {
                    return e
                }
            }
            return null
        }

        fun getSceneType(value: String): SceneType? {
            for (e in SceneType.values()) {
                if (e.value.equals(value)) {
                    return e
                }
            }
            return null
        }

        fun convertHexStringToInt(value: String): Int {
            return Integer.parseInt(value, 16)
        }

        fun getDaylightValue(cValue: String): Int {
            var cValueInInt = convertHexStringToInt(cValue)
            return ((cValueInInt / 255) * 100)
        }

        fun getLampOnOffStatus(status: Boolean): String {
            if (status) {
                return "ON"
            }
            return "OFF"
        }

        fun getLampOnOffStatus(status: String): Boolean {
            return status.equals("ON")
        }


        fun WaveLengthToRGB(Wavelength: Double): IntArray {
            val factor: Double
            val Red: Double
            val Green: Double
            val Blue: Double
            val Gamma = 0.8
            val IntensityMax = 255.0

            if (Wavelength >= 380 && Wavelength < 440) {
                Red = -(Wavelength - 440) / (440 - 380)
                Green = 0.0
                Blue = 1.0
            } else if (Wavelength >= 440 && Wavelength < 490) {
                Red = 0.0
                Green = (Wavelength - 440) / (490 - 440)
                Blue = 1.0
            } else if (Wavelength >= 490 && Wavelength < 510) {
                Red = 0.0
                Green = 1.0
                Blue = -(Wavelength - 510) / (510 - 490)
            } else if (Wavelength >= 510 && Wavelength < 580) {
                Red = (Wavelength - 510) / (580 - 510)
                Green = 1.0
                Blue = 0.0
            } else if (Wavelength >= 580 && Wavelength < 645) {
                Red = 1.0
                Green = -(Wavelength - 645) / (645 - 580)
                Blue = 0.0
            } else if (Wavelength >= 645 && Wavelength < 781) {
                Red = 1.0
                Green = 0.0
                Blue = 0.0
            } else {
                Red = 255.0
                Green = 0.0
                Blue = 170.0
            }


            // Let the intensity fall off near the vision limits

            if (Wavelength >= 380 && Wavelength < 420) {
                factor = 0.3 + 0.7 * (Wavelength - 380) / (420 - 380)
            } else if (Wavelength >= 420 && Wavelength < 701) {
                factor = 1.0
            } else if (Wavelength >= 701 && Wavelength < 781) {
                factor = 0.3 + 0.7 * (780 - Wavelength) / (780 - 700)
            } else {
                factor = 0.0
            }


            val rgb = IntArray(3)

            if (factor != 0.0) {
                // Don't want 0^x = 1 for x <> 0
                rgb[0] = if (Red == 0.0) 0 else Math.round(IntensityMax * Math.pow(Red * factor, Gamma)).toInt()
                rgb[1] = if (Green == 0.0) 0 else Math.round(IntensityMax * Math.pow(Green * factor, Gamma)).toInt()
                rgb[2] = if (Blue == 0.0) 0 else Math.round(IntensityMax * Math.pow(Blue * factor, Gamma)).toInt()
            } else {// Don't want 0^x = 1 for x <> 0
                rgb[0] = 255
                rgb[1] = 0
                rgb[2] = 170
            }

            return rgb
        }

        fun MilliSecondsToTimerHMS(milliseconds: Int): String {
            var hoursString = ""
            var secondsString = ""
            var minutesString = ""
            //Convert total duration into time
            val hours = (milliseconds / (1000 * 60 * 60)).toInt()
            val minutes = (milliseconds % (1000 * 60 * 60)).toInt() / (1000 * 60)
            val seconds =
                    if (milliseconds < 1000 && milliseconds > 0) 1 else (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
            // Pre appending 0 to hours if it is one digit
            if (hours < 10) {
                hoursString = "0$hours"
            } else {
                hoursString = "" + hours
            }
            // Pre appending 0 to minutes if it is one digit
            if (minutes < 10) {
                minutesString = "0$minutes"
            } else {
                minutesString = "" + minutes
            }
            // Pre appending 0 to seconds if it is one digit
            if (seconds < 10) {
                secondsString = "0$seconds"
            } else {
                secondsString = "" + seconds
            }
            // return timer string
            return "$hoursString:$minutesString:$secondsString"
        }

        fun byteArrayToIntArray(byteArray: ByteArray): IntArray {
            val intBuf = ByteBuffer.wrap(byteArray)
                    .order(ByteOrder.BIG_ENDIAN)
                    .asIntBuffer()
            val array = IntArray(intBuf.remaining())
            intBuf.get(array)
            return array
        }

        /**
         * Converts a 4 byte array of unsigned mFFTVisualizerBytes to an long
         *
         * @param b an array of 4 unsigned mFFTVisualizerBytes
         * @return a long representing the unsigned int
         */
        fun unsignedIntToLong(b: ByteArray): Long {
            var l: Long = 0
            l = l or (b[0] and 0xFF.toByte()).toLong()
            l = l shl 8
            l = l or (b[1] and 0xFF.toByte()).toLong()
            l = l shl 8
            l = l or (b[2] and 0xFF.toByte()).toLong()
            l = l shl 8
            l = l or (b[3] and 0xFF.toByte()).toLong()
            return l
        }

        /**
         * Converts a two byte array to an integer
         *
         * @param b a byte array of length 2
         * @return an int representing the unsigned short
         */
        fun unsignedShortToInt(b: ByteArray): Int {
            var i = 0
            i = i or ((b[0] and 0xFF.toByte()).toInt())
            i = i shl 8
            i = i or ((b[1] and 0xFF.toByte()).toInt())
            return i
        }

        fun intArrayToByteArray(values: IntArray): ByteArray {
            val byteBuffer = ByteBuffer.allocate(values.size * 4)
            val intBuffer = byteBuffer.asIntBuffer()
            intBuffer.put(values)

            return byteBuffer.array()
        }

        fun getErrorMessage(response: Response<*>): String? {
            if (response.errorBody() != null) {
                Timber.d("Error response - ${response.errorBody()!!.string()}")

                try {
                    var json = JSONObject(response.errorBody()!!.string())
                    if (json["description"] != null) {
                        Timber.d("Server message - ${(json["description"] as String)}")
                        return (json["description"] as String)
                    } else {
                        Timber.d("Server message - **NO ERROR MESSAGE RECEIVED**")
                        return null
                    }
                } catch (e: Exception) {
                    Timber.d("Failed while parsing error response")
                    return null
                }
            }
            return null
        }

        fun convertBaseRequestToMqttJson(request: BaseRequest): JSONObject? {
            var requestByteArray = request.getByteArray()

            Timber.d(toString(requestByteArray))

            var requestInJson: JSONObject? = null

            when (request.packetType) {
                PacketType.SET_COLOR -> {
                    // 7e 0013 845dd7696379 36373834 9000   ff   0f   2a   3b   4c   64  01
                    // 0  1-2  3-8           9-12    13-14  15   16   17   18   19   20  21

                    var status = hexByteToString(requestByteArray[21]).equals("01")

                    requestInJson = SetColourRequest(
                            request.macId,
                            request.sequenceNumber,
                            hexByteToString(requestByteArray[15]),
                            hexByteToString(requestByteArray[16]),
                            hexByteToString(requestByteArray[17]),
                            hexByteToString(requestByteArray[18]),
                            hexByteToString(requestByteArray[19]),
                            convertHexStringToInt(hexByteToString(requestByteArray[20])),
                            status
                    ).getRequestJson()
                }

                PacketType.SET_SCENE -> {
                    // 7e 000d 845dd7696379 36373834 9001 03
                    var sceneType = getSceneType(hexByteToString(requestByteArray[15]))

                    requestInJson = SetPresetSceneRequest(
                            request.macId,
                            request.sequenceNumber,
                            sceneType!!
                    ).getRequestJson()

                }
            }

            return requestInJson
        }

        fun getDateAndTime(timeStamp: Long): String {
            val date = Date()
            date.time = timeStamp

            val outputTimeFormat = SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.ENGLISH)
            outputTimeFormat.timeZone = TimeZone.getTimeZone("Asia/Kolkata")
            return outputTimeFormat.format(date)
        }

        fun getTime(timeStamp: String): String {
            return try {
                val outputFormat = SimpleDateFormat("h:mm a", Locale.ENGLISH)
                val inputFormat = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
                // java.util.Date date = this.getTimeFrom();
                val inputDate = inputFormat.parse(timeStamp)
                outputFormat.format(inputDate)
            } catch (pe: Exception) {
                "-"
            }

        }

        fun getTimeInMilliseconds(timestamp: String): Long {
            Timber.d("Timestamp - $timestamp")
            var inputFormat = SimpleDateFormat(API_TIMESTAMP, Locale.ENGLISH)
            inputFormat.timeZone = TimeZone.getTimeZone("UTC")

            val date = inputFormat.parse(timestamp)
            return date.time / 1000
        }

        fun getHourOfDay(timeStamp: Long): Int {
            var date = Date()
            date.time = timeStamp

            val outputTimeFormat = SimpleDateFormat("HH", Locale.ENGLISH)
            return outputTimeFormat.format(date).toInt()
        }

        fun getMinuteOfHour(timeStamp: Long): Int {
            var date = Date()
            date.time = timeStamp

            var outputTimeFormat = SimpleDateFormat("mm", Locale.ENGLISH)
            return outputTimeFormat.format(date).toInt()
        }

        fun getStartTimeInSeconds(
                selectedStartHour: Int,
                selectedStartMinute: Int,
                today: Boolean
        ): Long {
            var startTime: Long = 0L
            var currentTimeInMillis = Calendar.getInstance().timeInMillis

            if (today) {

                var selectedStartTimecalendar = Calendar.getInstance()

                selectedStartTimecalendar.set(
                        Calendar.getInstance().get(Calendar.YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                        selectedStartHour,
                        selectedStartMinute
                )

                startTime = (selectedStartTimecalendar.timeInMillis - currentTimeInMillis) / 1000
                Timber.d(
                        "Final Start time in seconds = $startTime\n" +
                                "Start time in millis = ${selectedStartTimecalendar.timeInMillis}\n" +
                                "Current time in millis = ${currentTimeInMillis}"
                )

            } else {

                var date = Date()
                val calendarInstance = Calendar.getInstance()
                calendarInstance.time = date
                calendarInstance.add(Calendar.DATE, 1)

                var selectedStartTimecalendar = Calendar.getInstance()

                selectedStartTimecalendar.set(
                        calendarInstance.get(Calendar.YEAR),
                        calendarInstance.get(Calendar.MONTH),
                        calendarInstance.get(Calendar.DAY_OF_MONTH),
                        selectedStartHour,
                        selectedStartMinute
                )

                startTime = (selectedStartTimecalendar.timeInMillis - currentTimeInMillis) / 1000
                Timber.d(
                        "Final Start time in seconds = $startTime\n" +
                                "Start time in millis = ${selectedStartTimecalendar.timeInMillis}\n" +
                                "Current time in millis = ${currentTimeInMillis}"
                )
            }

            return startTime
        }

        fun getEndTimeInSeconds(startHour: Int, startMinute: Int, endHour: Int, endMinute: Int, today: Boolean): Long {
            var endTime: Long = 0

            if (today) {

                var startTimecalendar = Calendar.getInstance()

                startTimecalendar.set(
                        startTimecalendar.get(Calendar.YEAR),
                        startTimecalendar.get(Calendar.MONTH),
                        startTimecalendar.get(Calendar.DAY_OF_MONTH),
                        startHour,
                        startMinute
                )


                if (endHour < startHour || ((endHour == startHour) && (endMinute < startMinute))) {
                    Timber.d("Selected today, end time is less than start time")
                    Timber.d("Selecting end time for tomorrow")


                    var endTimeCalendar = Calendar.getInstance()
                    endTimeCalendar.add(Calendar.DAY_OF_MONTH, 1)

                    endTimeCalendar.set(
                            endTimeCalendar.get(Calendar.YEAR),
                            endTimeCalendar.get(Calendar.MONTH),
                            endTimeCalendar.get(Calendar.DAY_OF_MONTH),
                            endHour,
                            endMinute
                    )

                    endTime = (endTimeCalendar.timeInMillis - startTimecalendar.timeInMillis) / 1000
                    Timber.d(
                            "Final End time in seconds = $endTime\n" +
                                    "Start time in millis = ${startTimecalendar.timeInMillis}\n" +
                                    "End time in millis = ${endTimeCalendar.timeInMillis}"
                    )

                } else {
                    Timber.d("Selected today, end time is more than or equal to start time")
                    Timber.d("Selecting end time for today")

                    var endTimeCalendar = Calendar.getInstance()

                    endTimeCalendar.set(
                            endTimeCalendar.get(Calendar.YEAR),
                            endTimeCalendar.get(Calendar.MONTH),
                            endTimeCalendar.get(Calendar.DAY_OF_MONTH),
                            endHour,
                            endMinute
                    )

                    endTime = (endTimeCalendar.timeInMillis - startTimecalendar.timeInMillis) / 1000
                    Timber.d(
                            "Final End time in seconds = $endTime\n" +
                                    "Start time in millis = ${startTimecalendar.timeInMillis}\n" +
                                    "End time in millis = ${endTimeCalendar.timeInMillis}"
                    )
                }

            } else {

                if (endHour < startHour || ((endHour == startHour) && (endMinute < startMinute))) {
                    Timber.d("Selected tomorrow, end time is less than start time")
                    Timber.e("Invalid end time")

                    endTime = -1
                } else {
                    var tempCalendar = Calendar.getInstance()
                    tempCalendar.add(Calendar.DATE, 1)

                    var startTimecalendar = Calendar.getInstance()

                    startTimecalendar.set(
                            tempCalendar.get(Calendar.YEAR),
                            tempCalendar.get(Calendar.MONTH),
                            tempCalendar.get(Calendar.DAY_OF_MONTH),
                            startHour,
                            startMinute
                    )

                    var endTimeCalendar = Calendar.getInstance()

                    endTimeCalendar.set(
                            tempCalendar.get(Calendar.YEAR),
                            tempCalendar.get(Calendar.MONTH),
                            tempCalendar.get(Calendar.DAY_OF_MONTH),
                            endHour,
                            endMinute
                    )

                    endTime = (endTimeCalendar.timeInMillis - startTimecalendar.timeInMillis) / 1000
                    Timber.d(
                            "Final End time in seconds = $endTime\n" +
                                    "Start time in millis = ${startTimecalendar.timeInMillis}\n" +
                                    "End time in millis = ${endTimeCalendar.timeInMillis}"
                    )
                }
            }
            return endTime
        }

        fun drawColorCircle(color: Int): ShapeDrawable {

            var shape = ShapeDrawable(OvalShape())
            shape.paint.color = color
            return shape
        }

        fun getTimeInMillis(hour: Int, minute: Int, isToday: Boolean): Long {
            if (isToday) {

                var calendar = Calendar.getInstance()

                calendar.set(
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH),
                        hour,
                        minute
                )

                return calendar.timeInMillis

            } else {

                var calendar = Calendar.getInstance()
                calendar.add(Calendar.DATE, 1)

                var startTimecalendar = Calendar.getInstance()

                startTimecalendar.set(
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH),
                        hour,
                        minute
                )

                return startTimecalendar.timeInMillis
            }
        }

        fun getEndTimeInMillis(startHour: Int, startMinute: Int, endHour: Int, endMinute: Int, isToday: Boolean): Long {
            if (isToday) {

                if (endHour < startHour || ((endHour == startHour) && (endMinute < startMinute))) {
                    Timber.d("Selected today, end time is less than start time")
                    Timber.d("Setting end time for tomorrow")

                    var calendar = Calendar.getInstance()

                    calendar.add(Calendar.DAY_OF_MONTH, 1)

                    calendar.set(
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH),
                            endHour,
                            endMinute
                    )

                    return calendar.timeInMillis
                } else {
                    Timber.d("Selected today, end time is more than start time")
                    Timber.d("Setting end time for today")

                    var calendar = Calendar.getInstance()

                    calendar.set(
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH),
                            endHour,
                            endMinute
                    )

                    return calendar.timeInMillis
                }


            } else {

                if (endHour < startHour || ((endHour == startHour) && (endMinute < startMinute))) {
                    // Should not occur
                    Timber.d("Selected tomorrow, end time is less than start time")
                    Timber.e("Invalid end time")
                    return -1.toLong()
                } else {
                    Timber.d("Selected tomorrow, end time is more than start time")

                    var calendar = Calendar.getInstance()
                    calendar.add(Calendar.DATE, 1)

                    var startTimecalendar = Calendar.getInstance()

                    startTimecalendar.set(
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH),
                            endHour,
                            endMinute
                    )

                    return startTimecalendar.timeInMillis

                }

            }
        }


        fun convertLongToHexString(valueToConvert: Long): String {
            var hexString = java.lang.Long.toHexString(valueToConvert)
            var difference = 6 - hexString.length
            for (i in 1..difference) {
                hexString = "0$hexString"
            }
            return hexString
        }

        fun sortProjects(list: List<Project>) {
            if (list.isNotEmpty()) {
                Timber.d("Sorting projects")
                sort(list) { object1, object2 -> object1.name.toLowerCase().compareTo(object2.name.toLowerCase()) }
            }
        }

        fun sortNetworks(list: List<Network>) {
            if (list.isNotEmpty()) {
                Timber.d("Sorting networks")
                sort(list) { object1, object2 -> object1.name.toLowerCase().compareTo(object2.name.toLowerCase()) }
            }
        }

        fun sortGroups(list: List<Group>) {
            if (list.isNotEmpty()) {
                Timber.d("Sorting groups")
                sort(list) { object1, object2 -> object1.name.toLowerCase().compareTo(object2.name.toLowerCase()) }
            }
        }

        fun sortScenes(list: List<Scene>) {
            if (list.isNotEmpty()) {
                Timber.d("Sorting scenes")
                sort(list) { object1, object2 -> object1.name.toLowerCase().compareTo(object2.name.toLowerCase()) }
            }
        }

        fun capitalizeWords(input: String): String {
            val words = input.split(" ").toMutableList()
            var output = ""
            for (word in words) {
                output += word.capitalize() + " "
            }
            output = output.trim()

            return output
        }

        fun scrollToView(scrollViewParent: ScrollView, view: View) {
            // Get deepChild Offset
            val childOffset = Point()
            getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset)
            // Scroll to child.
            scrollViewParent.smoothScrollTo(0, childOffset.y)
        }

        fun getDeepChildOffset(mainParent: ViewGroup, parent: ViewParent, child: View, accumulatedOffset: Point) {
            val parentGroup = parent as ViewGroup
            accumulatedOffset.x += child.getLeft()
            accumulatedOffset.y += child.getTop()
            if (parentGroup == mainParent) {
                return
            }
            getDeepChildOffset(mainParent, parentGroup.parent, parentGroup, accumulatedOffset)
        }

        fun convertToUTC(value: String): String {
            var timeFormat = SimpleDateFormat("HH:mm:ss")
            timeFormat.timeZone = TimeZone.getTimeZone("UTC")
            var myDate = timeFormat.parse(value)
            var outputDate = Date(myDate.time - Calendar.getInstance().getTimeZone().getOffset(myDate.time))

            return timeFormat.format(outputDate)
        }

        fun utcToLocal(value: String): String {
            var timeFormat = SimpleDateFormat("HH:mm:ss")
            timeFormat.timeZone = TimeZone.getTimeZone("UTC")
            var myDate = timeFormat.parse(value)
            var outputDate = Date(myDate.time + Calendar.getInstance().getTimeZone().getOffset(myDate.time))

            return timeFormat.format(outputDate)
        }

        fun isEmail(value: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches()
        }

        fun convertToFWVersion(value: String): String {
            // Used for GET FIRMWARE VERSION tcp response
            // Input - 0109
            // Output - 1.09

            if (value.length == 4) {
                var fwVersionString: String = ""
                if (value[0] != '0') {
                    fwVersionString += value[0]
                }

                fwVersionString += value[1]
                fwVersionString += "."
                fwVersionString += value[2]
                fwVersionString += value[3]

                return fwVersionString
            }
            return ""
        }
    }
}