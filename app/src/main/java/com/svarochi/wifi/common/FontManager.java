package com.svarochi.wifi.common;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class FontManager {

    public static final String FONT_REGULAR = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/Roboto-Regular.ttf";
    public static final String FONT_ITALIC = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-Italic.ttf";
    public static final String FONT_BOLD = "fonts/helvetica_neue_lt_std_bold.ttf";
    public static final String FONT_SEMI_BOLD = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-Semibold.otf";
    public static final String FONT_BOLD_ITALIC = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-BoldItalic.ttf";
    public static final String FONT_SEMI_BOLD_ITALIC = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-SemiboldItalic.ttf";
    public static final String FONT_LIGHT_ITALIC = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-LightItalic.ttf";
    public static final String FONT_EXTRA_LIGHT_ITALIC = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-ExtraLightItalic.ttf";
    public static final String FONT_LIGHT = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-Light.ttf";
    public static final String FONT_EXTRA_LIGHT = "fonts/helvetica-neue-lt-std-roman.ttf";//"fonts/SourceSansPro-ExtraLight.ttf";
    public static final String FONT_ROMAN = "fonts/helvetica-neue-lt-std-roman.ttf";

    private Map<String, Typeface> fontCache = new HashMap<String, Typeface>();
    private static FontManager instance = null;
    private Context mContext;

    private FontManager(Context mContext2) {
        mContext = mContext2;
    }

    public synchronized static FontManager getInstance(Context mContext) {

        if (instance == null) {
            instance = new FontManager(mContext);
        }
        return instance;
    }

    public Typeface loadFont(String font) {
        if (!fontCache.containsKey(font)) {
            fontCache.put(font, Typeface.createFromAsset(mContext.getAssets(), font));
        }
        return fontCache.get(font);
    }
}
