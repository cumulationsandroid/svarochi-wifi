package com.svarochi.wifi.common;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;

import com.svarochi.wifi.widgets.IEditTextDialogClickListener;

import bizbrolly.svarochiapp.R;

/**
 * Created by rahul.dhanuka on 1/8/2018.
 */

public class DialogUtils {

    /**
     * Usage: PopUpUtils.showAlertMessage(mActivity, getString(R.string.app_name), getString(R.string.lorem_imsum_), "OK", null);
     * PopUpUtils.showAlertMessage(mActivity, "", getString(R.string.lorem_imsum_), "OK", null);
     *
     * @param context
     * @param title           Alert message title text
     * @param message         Alert message title message
     * @param positiveText    Alert message btn text
     * @param onPositiveClick Alert message btn click event
     */
    public static void showDefaultAlertMessage(final Context context, String title, String message, String positiveText, final View.OnClickListener onPositiveClick) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            if (!TextUtils.isEmpty(title)) {
                dialogBuilder.setTitle(title);
            }
            dialogBuilder.setMessage(message);
            dialogBuilder.setPositiveButton(positiveText, (dialog, which) -> {
                // continue with delete
                dialog.dismiss();
                if (onPositiveClick != null)
                    onPositiveClick.onClick(null);
            });
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setOnShowListener(arg0 -> {
                //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.color_196_55_55));
            });
            alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * To show only two menu button options pass menu2Text param as null or "", because if menu text is blank or null then we are hiding that menu btn
     * <p>
     * Usagae: PopUpUtils.showMenuOption(mActivity, "Take Photo", -1, "Choose Photo", -1, "Delete Photo", -1, null, null, null, null);
     * PopUpUtils.showMenuOption(mActivity, "Take Photo", -1, "", -1, "Delete Photo", -1, null, null, null, null);
     *
     * @param context
     * @param menu1Text     Menu 1 btn text
     * @param menu1Color    Menu 1 btn text color //default - color_3_122_255
     * @param menu2Text     Menu 2 btn text
     * @param menu2Color    Menu 2 btn text color //default - color_3_122_255
     * @param menu3Text     Menu 3 btn text
     * @param menu3Color    Menu 3 btn text color //default - color_255_6_6
     * @param onMenu1Click  Menu 1 btn click event
     * @param onMenu2Click  Menu 2 btn click event
     * @param onMenu3Click  Menu 3 btn click event
     * @param onCancelClick Cancel btn click event //default - color_3_122_255
     */
    public static void showAlertMenuOption(final Context context, String title, String menu1Text, int menu1Color, String menu2Text, int menu2Color, String menu3Text, int menu3Color, final View.OnClickListener onMenu1Click, final View.OnClickListener onMenu2Click, final View.OnClickListener onMenu3Click, final View.OnClickListener onCancelClick) {
        final CharSequence[] items = new CharSequence[3];
        items[0] = menu1Text;
        items[1] = menu2Text;
        items[2] = menu3Text;
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setNegativeButton(context.getString(R.string.cancel), (dialogInterface, i) -> {

                })
                .setItems(items, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            onMenu1Click.onClick(null);
                            break;
                        case 1:
                            onMenu2Click.onClick(null);
                            break;
                        case 2:
                            onMenu3Click.onClick(null);
                            break;
                    }
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(arg0 -> {
            //alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
        });
        alertDialog.show();
    }

    public static AlertDialog showEditTextDialog(Context context,
                                                 String title,
                                                 String hint, String text,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 int inputType,
                                                 int maxLength,
                                                 final IEditTextDialogClickListener clickListener) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        final EditText editText = new EditText(context);
        editText.setLines(1);
        editText.setMaxLines(1);
        editText.setSingleLine(true);
        editText.setHint(hint != null ? hint : "");
        editText.setText(text != null ? text : "");
        editText.setSelection(editText.getText().length());
        editText.setInputType(inputType);
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(context.getResources().getDimensionPixelSize(R.dimen.dp_20), 0, context.getResources().getDimensionPixelSize(R.dimen.dp_20), 0);
        editText.setLayoutParams(layoutParams);
        layout.addView(editText);
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setView(layout)
                .setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clickListener.onPositiveClick(editText.getText().toString().trim());
                    }
                })
                .setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clickListener.onNegativeClick();
                    }
                }).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
        return alertDialog;
    }

    public static void openAppSettings(final Activity activity, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setPositiveButton("Open Settings", (dialog, id) -> {
            dialog.dismiss();
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + activity.getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            activity.startActivity(i);
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setOnKeyListener((dialog1, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dialog1.dismiss();
            }
            return false;
        });
        dialog.show();
    }

    public static void showDefaultAlert(final Context context, String title, String message, String positiveText, String negativeText, final View.OnClickListener onPositiveClick, final View.OnClickListener onNegativeClick, boolean cancellableOutside, final float buttonTextSize) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            if (!TextUtils.isEmpty(title)) {
                dialogBuilder.setTitle(title);
            }
            dialogBuilder.setMessage(message);
            dialogBuilder.setPositiveButton(positiveText, (dialog, which) -> {
                // continue with delete
                dialog.dismiss();
                if (onPositiveClick != null)
                    onPositiveClick.onClick(null);
            });
            dialogBuilder.setNegativeButton(negativeText, (dialog, which) -> {
                // do nothing
                dialog.dismiss();
                if (onNegativeClick != null)
                    onNegativeClick.onClick(null);
            });
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(cancellableOutside);
            alertDialog.setCanceledOnTouchOutside(cancellableOutside);
            alertDialog.setOnShowListener(arg0 -> {
                //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.color_196_55_55));
                //alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.color_196_55_55));
                //setTransformationMethod to stop capitalization of button text
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTransformationMethod(null);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTransformationMethod(null);
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(buttonTextSize);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(buttonTextSize);
            });
            alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
