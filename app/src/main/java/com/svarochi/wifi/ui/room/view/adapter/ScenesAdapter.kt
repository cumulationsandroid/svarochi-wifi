package com.svarochi.wifi.ui.room.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ADD_SCENE_ITEM
import com.svarochi.wifi.common.Constants.Companion.SCENE_ITEM
import com.svarochi.wifi.database.entity.Scene
import com.svarochi.wifi.ui.room.view.fragment.ScenesFragment
import timber.log.Timber

class ScenesAdapter(
        var scenesList: List<Scene>,
        var isSelfProfile: Boolean,
        var context: ScenesFragment
) :
        androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        lateinit var view: View
        lateinit var viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder
        if (viewType == SCENE_ITEM) {
            view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_wifi_scene, viewGroup, false)
            viewHolder = ScenesViewHolder(view)
        } else {
            view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_new_scene, viewGroup, false)
            viewHolder = AddSceneViewHolder(view)
        }
        return viewHolder
    }

    override fun getItemCount(): Int {
       return scenesList.size
    }

    override fun getItemViewType(position: Int): Int {
       if(scenesList[position].id == (-1).toLong()){
           return ADD_SCENE_ITEM
       }
        return SCENE_ITEM
    }

    override fun onBindViewHolder(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (viewHolder.itemViewType == SCENE_ITEM) {
            (viewHolder as ScenesViewHolder).sceneName.text = scenesList.get(position).name

            if (isSelfProfile) {
                viewHolder.editSceneName.visibility = View.VISIBLE
            } else {
                viewHolder.editSceneName.visibility = View.GONE
            }
            // viewHolder.onOffCheckbox.isChecked = scenesList.get(position).status
        }
    }

    inner class ScenesViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var sceneName: TextView = itemView.findViewById(R.id.tv_sceneName)
        var editSceneName: ImageView = itemView.findViewById(R.id.iv_editSceneName)
        var editSceneLights: ImageView = itemView.findViewById(R.id.iv_editSceneLights)
        var onOffCheckbox: CheckBox = itemView.findViewById(R.id.cb_onOffScene)

        init {
            editSceneName.setOnClickListener {
                if (isSelfProfile) {
                    context.editSceneName(scenesList.get(adapterPosition).id!!, scenesList.get(adapterPosition).name)
                } else {
                    Timber.d("User is in guest mode hence cannot edit scene name")
                }
            }

            editSceneLights.setOnClickListener {
                if (isSelfProfile) {
                    context.editSceneLights(scenesList.get(adapterPosition).id)
                } else {
                    Timber.d("User is in guest mode hence cannot edit lights of the scene")
                }
            }

            onOffCheckbox.setOnClickListener {
                var position = adapterPosition
                if (context.isSceneContainingLights(scenesList.get(position).id)) {
                    if (onOffCheckbox.isChecked) {
                        // Turn on scene
                        context.turnOnScene(scenesList.get(position).id)
                    } else {
                        // Turn off scene
                        context.turnOffScene(scenesList.get(position).id!!)
                    }
                }else{
                    Timber.d("Scene is not containing any lights")
                    context.showToast(context.getString(R.string.toast_no_lights_associated_to_the_scene))
                    onOffCheckbox.isChecked = onOffCheckbox.isChecked.not()
                }
            }

            sceneName.setOnLongClickListener {
                if (isSelfProfile) {
                    context.showDeleteSceneConfirmationDialog(scenesList.get(adapterPosition).id!!)
                } else {
                    Timber.d("User is in guest mode hence cannot delete the scene")
                    true
                }
            }

            sceneName.setOnClickListener {
                if (isSelfProfile) {
                    context.editSceneLights(scenesList.get(adapterPosition).id!!)
                } else {
                    Timber.d("User is in guest mode hence cannot edit scene lights")
                }
            }

        }
    }

    inner class AddSceneViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var addSceneText: TextView = itemView.findViewById(R.id.tv_addScene)
        var addSceneImage: ImageView = itemView.findViewById(R.id.iv_addScene)

        init {
            addSceneImage.setOnClickListener {
                context.showAddSceneDialog()
            }
            addSceneText.setOnClickListener {
                context.showAddSceneDialog()
            }
        }
    }

}