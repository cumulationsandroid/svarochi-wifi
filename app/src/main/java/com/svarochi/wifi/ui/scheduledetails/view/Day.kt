package com.svarochi.wifi.ui.scheduledetails.view

data class Day(var id: String, var name: String, var isSelected: Boolean)