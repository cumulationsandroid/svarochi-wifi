package com.svarochi.wifi.ui.setwifidetails.viewmodel

import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.setwifidetails.repository.SetWifiDetailsRepository
import io.reactivex.disposables.CompositeDisposable
import com.svarochi.wifi.ui.setwifidetails.SetWifiDetailsImpl

class SetWifiDetailsViewModel(var repository: SetWifiDetailsRepository) : BaseViewModel(), SetWifiDetailsImpl {


    private var disposables = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()
    private var isObservingResponse = false

    init {
        // Observe light list of roomRepository
        initResponseObserver()
    }

    override fun onCleared() {
        isObservingResponse = false
        disposables.dispose()
        disposables = CompositeDisposable()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    override fun getWifiDetails() {
        response.postValue(Event(EventType.GET_WIFI_DETAILS, EventStatus.LOADING, null, null))
        repository.getWifiDetails()
    }

    override fun setWifiDetails(ssid: String, password: String, authenticationMode: String, encryptionType: String) {
        response.postValue(Event(EventType.SET_WIFI_DETAILS, EventStatus.LOADING, null, null))
        repository.setWifiDetails(ssid, password, authenticationMode, encryptionType)
    }
}