package com.svarochi.wifi.ui.scheduledetails.viewmodel

import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.GroupLight
import com.svarochi.wifi.database.entity.Schedule
import com.svarochi.wifi.model.api.common.EndAction
import com.svarochi.wifi.model.api.common.StartAction
import com.svarochi.wifi.model.common.CustomGroup
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.scheduledetails.repository.ScheduleDetailsRepository
import io.reactivex.disposables.CompositeDisposable

class ScheduleDetailsViewModel(var repository: ScheduleDetailsRepository) : BaseViewModel() {
    private var disposables = CompositeDisposable()
    var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver(disposables, repository, response)
    }

    override fun onCleared() {
        disposables.dispose()
        disposables = CompositeDisposable()
        repository.dispose()
    }

    fun addNewSchedule(
            scheduleName: String,
            days: ArrayList<String>,
            startTime: String,
            startAction: StartAction,
            endTime: String,
            endAction: EndAction,
            devices: ArrayList<String>,
            groups: ArrayList<Int>
    ) {
        response.value = Event(EventType.ADD_NEW_SCHEDULE, EventStatus.LOADING, null, null)
        repository.addNewSchedule(
                scheduleName,
                days,
                startTime,
                startAction,
                endTime,
                endAction,
                devices,
                groups
        )
    }

    fun editSchedule(
            scheduleId: Long,
            scheduleName: String,
            days: ArrayList<String>,
            startTime: String,
            startAction: StartAction,
            endTime: String,
            endAction: EndAction,
            devices: ArrayList<String>,
            groups: ArrayList<Int>
    ) {
        response.value = Event(EventType.EDIT_SCHEDULE, EventStatus.LOADING, null, null)
        repository.editSchedule(
                scheduleId,
                scheduleName,
                days,
                startTime,
                startAction,
                endTime,
                endAction,
                devices,
                groups
        )
    }

    fun deleteSchedule(
            scheduleId: Long
    ) {
        response.value = Event(EventType.DELETE_SCHEDULE, EventStatus.LOADING, null, null)
        repository.deleteSchedule(scheduleId.toInt())
    }

    fun getAllGroups(): ArrayList<CustomGroup> {
        return repository.getAllGroups()
    }

    fun getAllLights(): ArrayList<CustomLight> {
        return repository.getAllLights()
    }

    fun getGroupLights(groupId: Long): ArrayList<GroupLight> {
        return repository.getGroupLights(groupId)
    }

    fun getSchedule(scheduleId: Long): Schedule {
        return repository.getSchedule(scheduleId)
    }

    fun isScheduleNameTaken(name: String, scheduleId: Long): Boolean {
        return repository.isScheduleNameTaken(name, scheduleId)
    }

    fun isScheduleNameTaken(name: String): Boolean {
        return repository.isScheduleNameTaken(name)
    }


}