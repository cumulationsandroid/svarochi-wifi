package com.svarochi.wifi.ui.schedulelist.repository

import ApiSchedule
import GetSchedulesResponse
import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.Schedule
import com.svarochi.wifi.model.api.common.EndAction
import com.svarochi.wifi.model.api.common.StartAction
import com.svarochi.wifi.model.api.response.DeleteScheduleResponse
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.schedulelist.ScheduleListImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class ScheduleListRepository(var application: SvarochiApplication) : BaseRepositoryImpl, BaseRepository(),
        ScheduleListImpl {
    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    private val responseObservable = PublishSubject.create<Event>()
    private var disposables = CompositeDisposable()
    private var userId: String = ""
    private var projectId: Long = 0
    private var networkId: Long = 0
    private var sharedPreference: SharedPreference? = null

    override fun initCommunicationResponseObserver() {
        // No implementations as its not using service
    }

    init {
        fetchIdsFromPreferences()
        callApis()
    }

    private fun callApis() {
        getSchedulesFromApi()
    }

    private fun getSchedulesFromApi() {
        disposables.add(
                ApiRequestParser.getSchedules(userId, networkId.toInt(), projectId.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetSchedulesResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed get schedules api")
                            }

                            override fun onNext(response: Response<GetSchedulesResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Schedules API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Successfully received schedules from server")
                                            if (response.body()!!.records != null) {
                                                storeSchedulesInLocalDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("No records for schedules found from server")

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.GET_SCHEDULES,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching schedules from server")
                                        Timber.d("No response got for networks from server")

                                        responseObservable.onNext(
                                                Event(
                                                        EventType.GET_SCHEDULES,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching schedules from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.d("Response error message - $errorMessage")
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.GET_SCHEDULES,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching schedules from server")
                                Timber.e(error)

                                responseObservable.onNext(
                                        Event(
                                                EventType.GET_SCHEDULES,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }

                        })
        )
    }

    private fun storeSchedulesInLocalDb(schedules: List<ApiSchedule>) {
        application.wifiLocalDatasource.clearSchedules()
        schedules.forEach { apiSchedule ->

            if(sharedPreference == null){
                sharedPreference = SharedPreference(application)
            }

            var networkId = sharedPreference?.getNetworkId()!!
            var projectId = sharedPreference?.getNetworkId()!!

            var scheduleDevicesList = apiSchedule.schedule_devices.toMutableList()

            apiSchedule.schedule_devices.forEach {
                if(sharedPreference!!.deviceExistsInToBeDeleted(it, networkId, projectId)){
                    scheduleDevicesList.remove(it)
                }
            }

            var startAction = StartAction(
                    apiSchedule.start_power,
                    apiSchedule.start_red,
                    apiSchedule.start_green,
                    apiSchedule.start_blue,
                    apiSchedule.start_brightness,
                    apiSchedule.start_warm,
                    apiSchedule.start_cool,
                    apiSchedule.start_scene
            )

            var endAction = EndAction(
                    apiSchedule.end_power,
                    apiSchedule.end_red,
                    apiSchedule.end_green,
                    apiSchedule.end_blue,
                    apiSchedule.end_brightness,
                    apiSchedule.end_warm,
                    apiSchedule.end_cool,
                    apiSchedule.end_scene
            )
            var newSchedule = Schedule(
                    apiSchedule.schedule_id.toLong(),
                    apiSchedule.schedule_name,
                    apiSchedule.network.toLong(),
                    Converters.fromStringList(apiSchedule.schedule_days),
                    Utilities.utcToLocal(apiSchedule.start_time),
                    Converters.fromAction(startAction),
                    Utilities.utcToLocal(apiSchedule.end_time),
                    Converters.fromAction(endAction),
                    Converters.fromStringList(scheduleDevicesList),
                    Converters.fromIntList(apiSchedule.schedule_groups)
            )

            application.wifiLocalDatasource.addNewSchedule(newSchedule)
        }

        responseObservable.onNext(
                Event(
                        EventType.GET_SCHEDULES,
                        EventStatus.SUCCESS,
                        null,
                        null
                )
        )
    }


    private fun fetchIdsFromPreferences() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        userId = sharedPreference!!.getUserId()
        projectId = sharedPreference!!.getProjectId()
        networkId = sharedPreference!!.getNetworkId()
    }

    override fun getCommunicationResponseObserver() = responseObservable

    fun getScehdules(): LiveData<List<Schedule>> {
        return application.wifiLocalDatasource.getSchedules(networkId)
    }


    fun deleteSchedule(scheduleId: Int) {
        disposables.add(
                ApiRequestParser.deleteSchedule(
                        userId,
                        networkId.toInt(),
                        projectId.toInt(),
                        scheduleId
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<DeleteScheduleResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed Delete Schedule Api")
                            }

                            override fun onNext(response: Response<DeleteScheduleResponse>) {
                                if (response.isSuccessful) {

                                    if (response.body() != null) {
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Received success response while deleting schedule in server")
                                            deleteScheduleFromLocalDb(scheduleId)

                                        } else {
                                            Timber.d("Something went wrong while deleting schedule in server")
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.d("Response error message - $errorMessage")
                                            }
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.DELETE_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }

                                    } else {
                                        Timber.d("Something went wrong while deleting schedule in server")
                                        Timber.d("No response body received from server")
                                        responseObservable.onNext(
                                                Event(
                                                        EventType.DELETE_SCHEDULE,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }

                                } else {
                                    Timber.d("Something went wrong while deleting schedule in server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.d("Response error message - $errorMessage")
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.DELETE_SCHEDULE,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                            }

                            override fun onError(e: Throwable) {
                                Timber.d("Something went wrong while deleting schedule in server")
                                Timber.e(e)
                                e.printStackTrace()
                                responseObservable.onNext(Event(EventType.DELETE_SCHEDULE, EventStatus.FAILURE, null, null))
                            }
                        }
                        )
        )
    }


    private fun deleteScheduleFromLocalDb(scheduleId: Int) {
        application.wifiLocalDatasource.deleteSchedule(scheduleId.toLong())
        responseObservable.onNext(
                Event(
                        EventType.DELETE_SCHEDULE,
                        EventStatus.SUCCESS,
                        null,
                        null
                )
        )
    }

}