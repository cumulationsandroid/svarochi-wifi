package com.svarochi.wifi.ui.music.viewmodel

import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.ui.music.repository.MusicRepository
import io.reactivex.disposables.CompositeDisposable
import com.svarochi.wifi.ui.music.MusicImpl

class MusicViewModel(var repository: MusicRepository) : BaseViewModel(), MusicImpl {

    private var disposables = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()
    private var isObservingResponse = false

    override fun onCleared() {
        isObservingResponse = false
        disposables.dispose()
        disposables = CompositeDisposable()
        repository.dispose()
    }

    init {
        initResponseObserver()
    }


    private fun initResponseObserver() {
        if (!isObservingResponse) {
            isObservingResponse = true
            initResponseObserver(disposables, repository, response)
        }
    }

    fun getResponse() = response

    override fun setGroupRgbWithBrightness(groupId: Long, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        repository.setGroupRgbWithBrightness(groupId, r_value, g_value, b_value, brightness_value)
    }

    override fun setLightRgbWithBrightness(macId: String, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        repository.setLightRgbWithBrightness(macId, r_value, g_value, b_value, brightness_value)
    }

    fun setAudioFlag(){
        repository.setAudioFlag()
    }

    fun resetAudioFlag() {
        repository.resetAudioFlag()
    }

}