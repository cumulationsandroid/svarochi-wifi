package com.svarochi.wifi.ui.lightcontrol.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.ui.lightcontrol.LightControlImpl
import com.svarochi.wifi.ui.lightcontrol.repository.LightControlRepository
import io.reactivex.disposables.CompositeDisposable

class LightControlViewModel(var repository: LightControlRepository) : BaseViewModel(), LightControlImpl {

    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }


    override fun onCleared() {
        disposables.clear()
        repository.dispose()

    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    override fun setLampColor(
            macId: String,
            r_value: String,
            g_value: String,
            b_value: String
    ) {
        repository.setLampColor(macId, r_value, g_value, b_value)
        response.value = Event(EventType.SET_LIGHT_RGB, EventStatus.LOADING, null, null)
    }

    override fun setScene(macId: String, sceneType: SceneType) {
        repository.setScene(macId, sceneType)
        response.value = Event(EventType.SET_LIGHT_PRESET_SCENE, EventStatus.LOADING, null, null)
    }

    override fun turnOnLight(macId: String) {
        repository.turnOnLight(macId)
        response.value = Event(EventType.TURN_ON_LIGHT, EventStatus.LOADING, null, null)
    }

    override fun turnOffLight(macId: String) {
        repository.turnOffLight(macId)
        response.value = Event(EventType.TURN_OFF_LIGHT, EventStatus.LOADING, null, null)

    }

    override fun setLampBrightness(
            macId: String,
            brightnessValue: Int
    ) {
        repository.setLampBrightness(macId, brightnessValue)
        response.value = Event(EventType.SET_LIGHT_BRIGHTNESS, EventStatus.LOADING, null, null)
    }

    override fun setDaylight(
            macId: String,
            daylightValue: Int
    ) {
        repository.setDaylight(macId, daylightValue)
        response.value = Event(EventType.SET_LIGHT_DAYLIGHT, EventStatus.LOADING, null, null)
    }

    fun getLightLiveData(macId: String): LiveData<Light> {
        return repository.getLightLiveData(macId)
    }

    override fun editLightName(macId: String, newName: String) {
        response.value = Event(EventType.EDIT_LIGHT_NAME, EventStatus.LOADING, null, null)

        repository.editLightName(macId, newName)
    }

    fun turnOffDynamicScene(macId: String) {
        response.value = Event(EventType.TURN_OFF_DYNAMIC_SCENE, EventStatus.LOADING, null, null)
        repository.turnOffDynamicScene(macId)
    }


    fun getLightStatus(macId: String) {
        repository.getLightStatus(macId)
    }

    override fun deleteLamp(macId: String) {
        response.value = Event(EventType.DELETE_LAMP, EventStatus.LOADING, null, null)
        repository.deleteLamp(macId)
    }

    fun deleteUnsyncedLamp(macId: String) {
        response.value = Event(EventType.DELETE_LAMP, EventStatus.LOADING, null, null)
        repository.deleteUnSyncedLamp(macId)
    }

    fun getFirmwareVersion(macId: String) {
        response.value = Event(EventType.GET_FIRMWARE_VERSION, EventStatus.LOADING, null, null)
        repository.getFirmwareVersion(macId)
    }

}