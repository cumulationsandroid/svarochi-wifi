package com.svarochi.wifi.ui.groupcontrol.repository

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_B_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_G_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_R_VALUE
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.LocalDataSource
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.database.entity.GroupLight
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.api.common.UpdateGroupType
import com.svarochi.wifi.model.api.response.UpdateGroupResponse
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.Command
import com.svarochi.wifi.model.communication.LampType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.service.communication.CommunicationService
import com.svarochi.wifi.ui.groupcontrol.GroupControlImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class GroupControlRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl,
        GroupControlImpl {
    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var communicationService: CommunicationService = application.wifiCommunicationService
    private var localDataSource: LocalDataSource = application.wifiLocalDatasource
    private var sharedPreference: SharedPreference? = null
    private var projectId: Long = -1
    private var networkId: Long = -1
    private var userId = ""

    init {
        // Observe the responseObservable in service
        initCommunicationResponseObserver()
        fetchIdsFromPreferences()
    }

    private fun fetchIdsFromPreferences() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        projectId = sharedPreference!!.getProjectId()
        networkId = sharedPreference!!.getNetworkId()
        userId = sharedPreference!!.getUserId()
    }

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    override fun initCommunicationResponseObserver() {
        communicationService.getResponseObservable()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Timber.d("Listening to responseObservable of service...")
                    disposables.add(it!!)
                }
                .subscribe(object : DisposableObserver<Event>() {
                    override fun onComplete() {
                    }

                    override fun onNext(event: Event) {
                        responseObservable.onNext(event)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                        //responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.ERROR, null, null))
                    }
                })
    }

    override fun getCommunicationResponseObserver() = responseObservable

    override fun turnOnGroup(groupId: Long) {

        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            group.rValue,
            group.gValue,
            group.bValue,
            group.wValue,
            group.cValue,
            group.sceneValue,
            group.brightnessValue,
            true
        )*/

        Timber.d("Turn on command sent to lights of group - $groupId")
        var command = Command(EventType.TURN_ON_GROUP, groupId, null)
        communicationService.sendCommand(command)

        responseObservable.onNext(
                Event(
                        EventType.TURN_ON_GROUP,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    /*private fun updateGroupStatus(
        groupId: Long,
        rValue: String?,
        gValue: String?,
        bValue: String?,
        wValue: String?,
        cValue: String?,
        sceneValue: String,
        brightnessValue: Int,
        onOffStatus: Boolean
    ) {
        localDataSource.updateGroupStatus(
            groupId,
            rValue,
            gValue,
            bValue,
            wValue,
            cValue,
            sceneValue,
            brightnessValue,
            onOffStatus
        )
    }*/

    private fun getLatestLight(macId: String): Light? {
        Timber.d("Fetching light of macId($macId)")
        return localDataSource.getLightOfMacId(macId)
    }

    override fun turnOffGroup(groupId: Long) {
        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            group.rValue,
            group.gValue,
            group.bValue,
            group.wValue,
            group.cValue,
            group.sceneValue,
            group.brightnessValue,
            false
        )*/

        var command = Command(EventType.TURN_OFF_GROUP, groupId, null)
        communicationService.sendCommand(command)
        Timber.d("Turn off command sent to lights of group - $groupId")

        responseObservable.onNext(
                Event(
                        EventType.TURN_OFF_GROUP,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    private fun getLatestGroup(groupId: Long): Group {
        return localDataSource.getLatestGroup(groupId)
    }

    override fun setGroupScene(groupId: Long, sceneType: SceneType) {
        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            group.rValue,
            group.gValue,
            group.bValue,
            group.wValue,
            group.cValue,
            sceneType.value,
            group.brightnessValue,
            true
        )*/

        var command = Command(EventType.SET_GROUP_SCENE, groupId, sceneType)
        communicationService.sendCommand(command)

        Timber.d("Set scene command sent to lights of group - $groupId")

        responseObservable.onNext(
                Event(
                        EventType.SET_GROUP_SCENE,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    override fun setGroupDaylight(
            groupId: Long,
            daylightValue: Int
    ) {
        var w_value = Utilities.convertIntToHexString((255 * (1 - (daylightValue * 0.01))).toInt())
        var c_value = Utilities.convertIntToHexString((255 * (daylightValue * 0.01)).toInt())

        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            group.rValue,
            group.gValue,
            group.bValue,
            wValue,
            cValue,
            group.sceneValue,
            group.brightnessValue,
            true
        )*/

        var command = Command(EventType.SET_GROUP_DAYLIGHT, groupId, daylightValue)
        communicationService.sendCommand(command)
        Timber.d("Set daylight command sent to lights of group - $groupId")
        responseObservable.onNext(
                Event(
                        EventType.SET_GROUP_DAYLIGHT,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    override fun setGroupBrightness(
            groupId: Long,
            brightnessValue: Int
    ) {
        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            group.rValue,
            group.gValue,
            group.bValue,
            group.wValue,
            group.cValue,
            group.sceneValue,
            brightnessValue,
            true
        )*/

        var command = Command(EventType.SET_GROUP_BRIGHTNESS, groupId, brightnessValue)
        communicationService.sendCommand(command)
        Timber.d("Set brightness command sent to lights of group - $groupId")

        responseObservable.onNext(
                Event(
                        EventType.SET_GROUP_BRIGHTNESS,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    override fun setGroupColor(
            groupId: Long,
            r_value: String,
            g_value: String,
            b_value: String
    ) {

        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            rValue,
            gValue,
            bValue,
            group.wValue,
            group.cValue,
            group.sceneValue,
            group.brightnessValue,
            true
        )*/

        var rgbPayload = HashMap<String, String>()
        rgbPayload[BUNDLE_R_VALUE] = r_value
        rgbPayload[BUNDLE_G_VALUE] = g_value
        rgbPayload[BUNDLE_B_VALUE] = b_value

        var command = Command(EventType.SET_GROUP_COLOR, groupId, rgbPayload)
        communicationService.sendCommand(command)
        Timber.d("Set color command sent to lights of group - $groupId")
        responseObservable.onNext(
                Event(
                        EventType.SET_GROUP_COLOR,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    fun getLightsOfGroup(groupId: Long, networkId: Long) {
        Timber.d("Fetching lights of network - $networkId")
        var lightList = localDataSource.getLights(networkId)
        Timber.d("Got ${lightList.size} lights of network - $networkId")

        var customLightList = ArrayList<CustomLight>(lightList.size)

        for (light in lightList) {
            var customLight = CustomLight(light)
            Timber.d("Checking if ${light.macId} belongs to group")
            var queryList = localDataSource.getLightsFromGroupsOfMacId(groupId, light.macId)

            if (queryList.isEmpty()) {
                Timber.d("Light ${light.macId} doesn't belong to group")
                customLight.setSelected(false)
            } else {
                Timber.d("Light ${light.macId} belongs to group")
                customLight.setSelected(true)
            }
            customLightList.add(customLight)
        }

        responseObservable.onNext(
                Event(
                        EventType.GET_LIGHTS_OF_GROUP,
                        EventStatus.SUCCESS,
                        customLightList,
                        null
                )
        )
    }

    override fun addLightToGroup(macId: String, groupId: Long) {
        Timber.d("Fetching lights of group($groupId)")
        var lightsList = getLightsOfGroup(groupId)
        var lightsListToSend = ArrayList<String>()

        for (light in lightsList) {
            lightsListToSend.add(light.macId)
        }
        lightsListToSend.add(macId)


        Timber.d("Fetching group($groupId) name")
        var group = localDataSource.getLatestGroup(groupId)


        callUpdateGroupApi(lightsListToSend.toList(), groupId, group.name, UpdateGroupType.ADD_LIGHT, macId)
    }

    private fun callUpdateGroupApi(
            macIds: List<String>,
            groupId: Long,
            groupName: String,
            requestType: UpdateGroupType,
            macId: String?
    ) {

        if(GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.updateGroup(
                            userId,
                            macIds,
                            groupId.toInt(),
                            groupName,
                            networkId.toInt(),
                            projectId.toInt()
                    )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<UpdateGroupResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed Update group api")
                                }

                                override fun onNext(response: Response<UpdateGroupResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("Update Group API response body - ${response.toString()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully updated group in server")
                                                if (response.body()!!.group_id != null) {
                                                    when (requestType) {
                                                        UpdateGroupType.ADD_LIGHT -> {
                                                            updateGroupLightsList(macIds, groupId, requestType)
                                                        }

                                                        UpdateGroupType.REMOVE_LIGHT -> {
                                                            updateGroupLightsList(macIds, groupId, requestType)
                                                        }

                                                        UpdateGroupType.UPDATE_NAME -> {
                                                            updateGroupName(groupId, groupName)
                                                        }
                                                    }
                                                } else {
                                                    Timber.d("No groupId received from server")
                                                }
                                            }
                                        } else {
                                            Timber.d("Something went wrong while updating group in server")
                                            Timber.d("No response got from server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            when (requestType) {
                                                UpdateGroupType.ADD_LIGHT -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.ADD_LIGHT_TO_GROUP,
                                                                    EventStatus.FAILURE,
                                                                    macId!!,
                                                                    null
                                                            )
                                                    )
                                                }

                                                UpdateGroupType.REMOVE_LIGHT -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.REMOVE_LIGHT_FROM_GROUP,
                                                                    EventStatus.FAILURE,
                                                                    macId!!,
                                                                    null
                                                            )
                                                    )
                                                }
                                                UpdateGroupType.UPDATE_NAME -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.EDIT_GROUP_NAME,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }
                                            }

                                        }
                                    } else {
                                        Timber.d("Something went wrong while updating group into server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                        when (requestType) {
                                            UpdateGroupType.ADD_LIGHT -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.ADD_LIGHT_TO_GROUP,
                                                                EventStatus.FAILURE,
                                                                macId!!,
                                                                null
                                                        )
                                                )
                                            }

                                            UpdateGroupType.REMOVE_LIGHT -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.REMOVE_LIGHT_FROM_GROUP,
                                                                EventStatus.FAILURE,
                                                                macId!!,
                                                                null
                                                        )
                                                )
                                            }

                                            UpdateGroupType.UPDATE_NAME -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_GROUP_NAME,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        }
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while adding group into server")
                                    Timber.e(error)
                                    when (requestType) {
                                        UpdateGroupType.ADD_LIGHT -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_LIGHT_TO_GROUP,
                                                            EventStatus.FAILURE,
                                                            macId!!,
                                                            null
                                                    )
                                            )
                                        }

                                        UpdateGroupType.REMOVE_LIGHT -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.REMOVE_LIGHT_FROM_GROUP,
                                                            EventStatus.FAILURE,
                                                            macId!!,
                                                            null
                                                    )
                                            )
                                        }

                                        UpdateGroupType.UPDATE_NAME -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_GROUP_NAME,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    }
                                }

                            })
            )
        }else{
            var errorMap = HashMap<Int, String>()
            errorMap[NO_INTERNET_AVAILABLE] = macId!!

            when (requestType) {
                UpdateGroupType.ADD_LIGHT -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.ADD_LIGHT_TO_GROUP,
                                    EventStatus.FAILURE,
                                    errorMap,
                                    null
                            )
                    )
                }

                UpdateGroupType.REMOVE_LIGHT -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.REMOVE_LIGHT_FROM_GROUP,
                                    EventStatus.FAILURE,
                                    errorMap,
                                    null
                            )
                    )
                }

                UpdateGroupType.UPDATE_NAME -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.EDIT_GROUP_NAME,
                                    EventStatus.FAILURE,
                                    errorMap,
                                    null
                            )
                    )
                }
            }
        }
    }

    private fun updateGroupName(groupId: Long, groupName: String) {
        application.wifiLocalDatasource.updateGroupName(groupId, groupName)
        Timber.d("Successfully updated group($groupId) name($groupName) in db")
        responseObservable.onNext(Event(EventType.EDIT_GROUP_NAME, EventStatus.SUCCESS, null, null))
    }

    private fun updateGroupLightsList(devices: List<String>, groupId: Long, requestType: UpdateGroupType) {
        Timber.d("Removing all lights of group($groupId)")
        localDataSource.clearLightsOfGroup(groupId)

        var groupLightsList = ArrayList<GroupLight>()
        for (lightMacId in devices) {

            var groupLightToAdd = GroupLight()
            groupLightToAdd.setGroupId(groupId)
            groupLightToAdd.setMacId(lightMacId)

            groupLightsList.add(groupLightToAdd)
        }

        localDataSource.insertGroupLights(groupLightsList)

        updateGroupTag(groupId)

        when (requestType) {
            UpdateGroupType.ADD_LIGHT -> {
                responseObservable.onNext(
                        Event(
                                EventType.ADD_LIGHT_TO_GROUP,
                                EventStatus.SUCCESS,
                                null,
                                null
                        )
                )
            }
            UpdateGroupType.REMOVE_LIGHT -> {
                responseObservable.onNext(
                        Event(
                                EventType.REMOVE_LIGHT_FROM_GROUP,
                                EventStatus.SUCCESS,
                                null,
                                null
                        )
                )
            }
        }
    }

    override fun removeLightFromGroup(macId: String, groupId: Long) {
        Timber.d("Fetching lights of group($groupId)")
        var lightsList = getLightsOfGroup(groupId)
        var lightsListToSend = ArrayList<String>()

        for (light in lightsList) {
            lightsListToSend.add(light.macId)
        }
        if (lightsListToSend.contains(macId)) {
            lightsListToSend.remove(macId)

            Timber.d("Fetching group($groupId) name")
            var group = localDataSource.getLatestGroup(groupId)


            callUpdateGroupApi(lightsListToSend.toList(), groupId, group.name, UpdateGroupType.REMOVE_LIGHT, macId)

        } else {
            responseObservable.onNext(
                    Event(
                            EventType.REMOVE_LIGHT_FROM_GROUP,
                            EventStatus.SUCCESS,
                            null,
                            null
                    )
            )
        }
    }

    private fun updateGroupTag(groupId: Long) {
        Timber.d("Calculating groupTag of the group($groupId)")
        var groupLights = getLightsOfGroup(groupId)
        var brightnessTypeCount = 0
        var daylightTypeCount = 0
        var colorTypeCount = 0
        var groupTag = 0

        for (groupLight in groupLights) {
            var light = getLatestLight(groupLight.macId)
            when (light!!.lampType) {
                LampType.BRIGHT_AND_DIM.value -> {
                    brightnessTypeCount++
                }
                LampType.WARM_AND_COOL.value -> {
                    daylightTypeCount++
                }
                LampType.COLOR_AND_DAYLIGHT.value -> {
                    colorTypeCount++
                }
            }
        }

        if ((brightnessTypeCount > 0) && (daylightTypeCount == 0) && (colorTypeCount == 0)) {
            groupTag = Constants.GROUP_TAG_BRIGHTNESS_ONLY
        } else if ((brightnessTypeCount == 0) && (daylightTypeCount > 0) && (colorTypeCount == 0)) {
            groupTag = Constants.GROUP_TAG_DAYLIGHT_ONLY
        } else if ((brightnessTypeCount == 0) && (daylightTypeCount == 0) && (colorTypeCount > 0)) {
            groupTag = Constants.GROUP_TAG_COLOR_ONLY
        } else if ((brightnessTypeCount > 0) && (daylightTypeCount == 0) && (colorTypeCount > 0)) {
            groupTag = Constants.GROUP_TAG_BRIGHTNESS_AND_COLOR
        } else if ((brightnessTypeCount > 0) && (daylightTypeCount > 0) && (colorTypeCount == 0)) {
            groupTag = Constants.GROUP_TAG_BRIGHTNESS_AND_DAYLIGHT
        } else if ((brightnessTypeCount == 0) && (daylightTypeCount > 0) && (colorTypeCount > 0)) {
            groupTag = Constants.GROUP_TAG_DAYLIGHT_AND_COLOR
        } else if ((brightnessTypeCount > 0) && (daylightTypeCount > 0) && (colorTypeCount > 0)) {
            groupTag = Constants.GROUP_TAG_ALL
        } else {
            groupTag = Constants.GROUP_TAG_NONE
        }

        setGroupTag(groupId, groupTag)
    }

    private fun setGroupTag(groupId: Long, groupTag: Int) {
        Timber.d("Setting group groupTag($groupTag) to group($groupId)")
        localDataSource.setGroupTag(groupId, groupTag)
    }

    private fun getLightsOfGroup(groupId: Long): List<GroupLight> {
        Timber.d("Fetching lights of group($groupId)")
        return localDataSource.getLightsOfGroup(groupId)
    }

    fun getGroupLiveData(groupId: Long): LiveData<Group> {
        Timber.d("Fetching lights of group($groupId)")
        return localDataSource.getGroupLiveData(groupId)
    }

    override fun editGroupName(groupId: Long, name: String) {
        if (isGroupNameAlreadyExists(name).not()) {

            var groupLights = getLightsOfGroup(groupId)
            var lightsList = ArrayList<String>()
            for (groupLight in groupLights) {
                lightsList.add(groupLight.macId)
            }

            callUpdateGroupApi(lightsList, groupId, name, UpdateGroupType.UPDATE_NAME, null)
        } else {
            Timber.d("Group by the name $name already exists")
            responseObservable.onNext(Event(EventType.EDIT_GROUP_NAME, EventStatus.FAILURE, ALREADY_EXISTS, null))
        }
    }

    private fun isGroupNameAlreadyExists(name: String): Boolean {
        var groupsList = application.wifiLocalDatasource.getGroups(networkId)

        for (group in groupsList) {
            if (group.name.equals(name)) {
                return true
            }
        }
        return false
    }

    fun turnOffDynamicScene(groupId: Long) {
        /**
         * To turn off a dynamic we have to turn on the lamp with previous saved RGBWC values
         * */
        setLightSceneOfGroupToDefault(groupId)
        turnOnGroup(groupId)
    }

    private fun setLightSceneOfGroupToDefault(groupId: Long) {
        getLightsOfGroup(groupId).forEach {
            setLightSceneToDefault(it.macId)
        }
    }

    private fun setLightSceneToDefault(macId: String){
        localDataSource.setLightSceneToDefault(macId)
    }
}