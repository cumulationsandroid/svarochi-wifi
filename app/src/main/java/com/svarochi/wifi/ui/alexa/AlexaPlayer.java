package com.svarochi.wifi.ui.alexa;

import android.content.Context;
import androidx.fragment.app.DialogFragment;
import android.util.Log;

import com.willblaschko.android.alexa.audioplayer.AlexaAudioPlayer;
import com.willblaschko.android.alexa.interfaces.AvsItem;
import com.willblaschko.android.alexa.interfaces.audioplayer.AvsPlayAudioItem;
import com.willblaschko.android.alexa.interfaces.audioplayer.AvsPlayContentItem;
import com.willblaschko.android.alexa.interfaces.audioplayer.AvsPlayRemoteItem;
import com.willblaschko.android.alexa.interfaces.errors.AvsResponseException;
import com.willblaschko.android.alexa.interfaces.playbackcontrol.AvsMediaNextCommandItem;
import com.willblaschko.android.alexa.interfaces.playbackcontrol.AvsMediaPauseCommandItem;
import com.willblaschko.android.alexa.interfaces.playbackcontrol.AvsMediaPlayCommandItem;
import com.willblaschko.android.alexa.interfaces.playbackcontrol.AvsMediaPreviousCommandItem;
import com.willblaschko.android.alexa.interfaces.playbackcontrol.AvsReplaceAllItem;
import com.willblaschko.android.alexa.interfaces.playbackcontrol.AvsReplaceEnqueuedItem;
import com.willblaschko.android.alexa.interfaces.playbackcontrol.AvsStopItem;
import com.willblaschko.android.alexa.interfaces.speaker.AvsAdjustVolumeItem;
import com.willblaschko.android.alexa.interfaces.speaker.AvsSetMuteItem;
import com.willblaschko.android.alexa.interfaces.speaker.AvsSetVolumeItem;
import com.willblaschko.android.alexa.interfaces.speechrecognizer.AvsExpectSpeechItem;
import com.willblaschko.android.alexa.interfaces.speechrecognizer.AvsStopCaptureItem;
import com.willblaschko.android.alexa.interfaces.speechsynthesizer.AvsSpeakItem;

import bizbrolly.svarochiapp.BuildConfig;

/**
 * Implementation for Alexa and FCM
 */
public abstract class AlexaPlayer extends DialogFragment {
    private static final String TAG = "AlexaPlayer";
    public boolean canPlay = true;
    protected AlexaRecorder.AlexaRequestState state;
    private AlexaAudioPlayer audioPlayer;
    private PlayerCallback alexaAudioPlayerCallback = new PlayerCallback();

    public void setState(AlexaRequestState state) {
        this.state = state;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        audioPlayer = AlexaAudioPlayer.getInstance(context);
        audioPlayer.addCallback(alexaAudioPlayerCallback);

    }

    public abstract void onStateChanged(AlexaRequestState state);

    public void dispatchResponse(final AvsItem current) {
        if (!canPlay) {
            return;
        }
        Log.i(TAG, "Item type " + current.getClass().getName());
        if (current instanceof AvsPlayRemoteItem) {
            //play a URL
            if (!audioPlayer.isPlaying()) {
                audioPlayer.playItem((AvsPlayRemoteItem) current);
            }
        } else if (current instanceof AvsPlayContentItem) {
            //play a URL
            if (!audioPlayer.isPlaying()) {
                audioPlayer.playItem((AvsPlayContentItem) current);
            }
        } else if (current instanceof AvsSpeakItem) {
            //play a sound file
            if (!audioPlayer.isPlaying()) {
                audioPlayer.playItem((AvsSpeakItem) current);
            }
            setState(AlexaRequestState.ANSWERING);
        } else if (current instanceof AvsStopItem) {
            //stop our play
            audioPlayer.stop();
        } else if (current instanceof AvsReplaceAllItem) {
            //clear all items
            //mAvsItemQueue.clear();
            audioPlayer.stop();
        } else if (current instanceof AvsReplaceEnqueuedItem) {
            //clear all items
            //mAvsItemQueue.clear();
        } else if (current instanceof AvsExpectSpeechItem) {

            //listen for user input
            audioPlayer.stop();
//            startListening();
        } else if (current instanceof AvsSetVolumeItem) {
            //set our volume
        } else if (current instanceof AvsAdjustVolumeItem) {
            //adjust the volume
        } else if (current instanceof AvsSetMuteItem) {
            //mute/unmute the device
        } else if (current instanceof AvsMediaPlayCommandItem) {
            //fake a hardware "play" press
        } else if (current instanceof AvsMediaPauseCommandItem) {
            //fake a hardware "pause" press
        } else if (current instanceof AvsMediaNextCommandItem) {
            //fake a hardware "next" press
        } else if (current instanceof AvsMediaPreviousCommandItem) {
            //fake a hardware "previous" press
        } else if (current instanceof AvsResponseException) {
            setState(AlexaRequestState.ERROR);
        } else if (current instanceof AvsStopCaptureItem) {
            setState(AlexaRequestState.ERROR);
        } else if (current instanceof AvsStopItem) {
            setState(AlexaRequestState.ERROR);
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state == AlexaRequestState.ERROR) {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            new AlertDialog.Builder(getActivity())
//                                    .setTitle("Error")
//                                    .setMessage(((AvsResponseException) current).getDirective().getPayload().getCode() + ": " + ((AvsResponseException) current).getDirective().getPayload().getDescription())
//                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                        }
//                                    })
//                                    .show();
//                        }
//                    });
                } else if (state != AlexaRequestState.ANSWERING) {
                    state = AlexaRequestState.COMPLETED;
                }
                onStateChanged(state);

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e(TAG, "onDestroyView");
        canPlay = false;
        if (audioPlayer != null) {
            try {
                alexaAudioPlayerCallback.canPlay = false;
                audioPlayer.removeCallback(alexaAudioPlayerCallback);
                audioPlayer.stop();
                audioPlayer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public enum AlexaRequestState {
        LISTENING,
        PROCESSING,
        ERROR,
        ANSWERING,
        COMPLETED
    }

    class PlayerCallback implements AlexaAudioPlayer.Callback {

        public boolean canPlay = true;
        private boolean almostDoneFired = false;
        private boolean playbackStartedFired = false;

        @Override
        public void playerPrepared(AvsItem pendingItem) {

        }

        @Override
        public void playerProgress(AvsItem item, long offsetInMilliseconds, float percent) {
            if (BuildConfig.DEBUG) {
                //Log.i(TAG, "Player percent: " + percent);
            }
            if (item instanceof AvsPlayContentItem || item == null) {
                return;
            }
            if (!playbackStartedFired) {
                if (BuildConfig.DEBUG) {
                    Log.i(TAG, "PlaybackStarted " + item.getToken() + " fired: " + percent);
                }
                playbackStartedFired = true;
//                sendPlaybackStartedEvent(item, offsetInMilliseconds);
            }
            if (!almostDoneFired && percent > .8f) {
                if (BuildConfig.DEBUG) {
                    Log.i(TAG, "AlmostDone " + item.getToken() + " fired: " + percent);
                }
                almostDoneFired = true;
                if (item instanceof AvsPlayAudioItem) {
//                    sendPlaybackNearlyFinishedEvent((AvsPlayAudioItem) item, offsetInMilliseconds);
                }
            }
        }

        @Override
        public void itemComplete(AvsItem completedItem) {
            if (!canPlay) {
                return;
            }
            almostDoneFired = false;
            playbackStartedFired = false;
            if (completedItem instanceof AvsPlayContentItem || completedItem == null) {
                return;
            }
            if (BuildConfig.DEBUG) {
                Log.i(TAG, "Complete " + completedItem.getToken() + " fired");
            }
            setState(AlexaRequestState.COMPLETED);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onStateChanged(state);
                }
            });
        }

        @Override
        public boolean playerError(AvsItem item, int what, int extra) {
            return false;
        }

        @Override
        public void dataError(AvsItem item, Exception e) {
            e.printStackTrace();
        }
    }
}
