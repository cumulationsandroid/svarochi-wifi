package com.svarochi.wifi.ui.userSession.activities

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.akkipedia.skeleton.utils.GeneralUtils
import com.bizbrolly.WebServiceRequests
import com.bizbrolly.entities.ResendOTPResponse
import com.bizbrolly.entities.VerifyOTPResponse
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.common.CommonUtils
import com.svarochi.wifi.common.DialogUtils
import com.svarochi.wifi.preference.Preferences
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NAVIGATE_FROM
import com.svarochi.wifi.ui.projects.view.activity.ProjectsActivity
import kotlinx.android.synthetic.main.activity_otp.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OTPActivity : BaseActivity() {
    private var otp = ""
    private var resendCount = 0

    private var editorActionListener: TextView.OnEditorActionListener = TextView.OnEditorActionListener { v, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            Log.e(TAG, "requestVerifyOTP 1")
            requestVerifyOTP()
        }
        false
    }

    // variable to track event time
    private var mLastClickTime: Long = 0
    private var deleteKeyListener: View.OnKeyListener = View.OnKeyListener { v, keyCode, event ->
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTime < 100) {
            return@OnKeyListener false
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            when (v.id) {
                R.id.etOtp1 -> {
                    Log.e(TAG, "etOtp1")
                    when {
                        etOtp4.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp4.setText("")
                            }
                            etOtp4.requestFocus()
                        }
                        etOtp3.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp3.setText("")
                            }
                            etOtp3.requestFocus()
                        }
                        etOtp2.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp2.setText("")
                            }
                            etOtp2.requestFocus()
                        }
                        etOtp1.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp1.setText("")
                            }
                            etOtp1.requestFocus()
                        }
                    }
                }
                R.id.etOtp2 -> {
                    Log.e(TAG, "etOtp2")
                    if (etOtp2.text?.length!! == 0) {
                        etOtp1.setText("")
                    }
                    etOtp1.requestFocus()
                }
                R.id.etOtp3 -> {
                    Log.e(TAG, "etOtp3")
                    if (etOtp3.text?.length!! == 0) {
                        etOtp2.setText("")
                    }
                    etOtp2.requestFocus()
                }
                R.id.etOtp4 -> {
                    Log.e(TAG, "etOtp4")
                    if (etOtp4.text?.length!! == 0) {
                        etOtp3.setText("")
                    }
                    etOtp3.requestFocus()
                }
            }
        }
        false
    }

    private var otpTextWatcher: TextWatcher = object : TextWatcher {

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            Log.e(TAG, "onTextChanged")
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            Log.e(TAG, "beforeTextChanged")
        }

        override fun afterTextChanged(s: Editable) {
            Log.e(TAG, "afterTextChanged")
            if (etOtp1.text.hashCode() == s.hashCode()) {
                if (etOtp1.length() > 0) {
                    if (etOtp2.length() > 0) {
                        if (etOtp3.length() > 0) {
                            if (etOtp4.length() > 0) {
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                Log.e(TAG, "requestVerifyOTP 2")
                                requestVerifyOTP()
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp3.requestFocus()
                        }
                    } else {
                        etOtp2.requestFocus()
                    }
                } else {
                    etOtp1.requestFocus()
                }
            } else if (etOtp2.text.hashCode() == s.hashCode()) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        if (etOtp3.length() > 0) {
                            if (etOtp4.length() > 0) {
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                Log.e(TAG, "requestVerifyOTP 3")
                                requestVerifyOTP()
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp3.requestFocus()
                        }
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else if (etOtp3.text.hashCode() == s.hashCode()) {
                if (etOtp3.length() > 0) {
                    if (etOtp2.length() > 0) {
                        if (etOtp1.length() > 0) {
                            if (etOtp4.length() > 0) {
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                Log.e(TAG, "requestVerifyOTP 4")
                                requestVerifyOTP()
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp1.requestFocus()
                        }
                    } else {
                        if (etOtp1.length() > 0) {
                            etOtp2.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    }
                } else {
                    if (etOtp2.length() > 0) {
                        if (etOtp1.length() > 0) {
                            etOtp3.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    } else {
                        if (etOtp1.length() > 0) {
                            etOtp2.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    }
                }
            } else if (etOtp4.text.hashCode() == s.hashCode()) {
                if (etOtp4.length() > 0) {
                    if (etOtp3.length() > 0) {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                Log.e(TAG, "requestVerifyOTP 5")
                                requestVerifyOTP()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    } else {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp3.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    }
                } else {
                    if (etOtp3.length() > 0) {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp4.requestFocus()
                                //requestVerifyOTP();
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    } else {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp3.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        init()
    }

    private fun init() {
        setListeners()
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        etOtp1.setOnEditorActionListener(editorActionListener)
        etOtp2.setOnEditorActionListener(editorActionListener)
        etOtp3.setOnEditorActionListener(editorActionListener)
        etOtp4.setOnEditorActionListener(editorActionListener)

        etOtp1.setOnKeyListener(deleteKeyListener)
        etOtp2.setOnKeyListener(deleteKeyListener)
        etOtp3.setOnKeyListener(deleteKeyListener)
        etOtp4.setOnKeyListener(deleteKeyListener)

        etOtp1.addTextChangedListener(otpTextWatcher)
        etOtp2.addTextChangedListener(otpTextWatcher)
        etOtp3.addTextChangedListener(otpTextWatcher)
        etOtp4.addTextChangedListener(otpTextWatcher)

        etOtp1.requestFocus()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        Log.e(TAG, keyCode.toString() + "")
        return super.onKeyDown(keyCode, event)
    }

    fun actionResendOTP(view: View) {
        if (resendCount >= 3) {
            showToast(getString(R.string.you_have_reached_to_the_resend_otp_limit))
            return
        }
        if (!GeneralUtils.isInternetAvailable(this@OTPActivity)) {
            showToast(getString(R.string.no_internet))
            return
        }
        clearOTP()
        showProgressDialog(getString(R.string.resending_otp), getString(R.string.please_wait_))
        ++resendCount
        WebServiceRequests.getInstance().resendOTP(
                Preferences.getInstance(this@OTPActivity).email,
                Preferences.getInstance(this@OTPActivity).phone,
                object : Callback<ResendOTPResponse> {
                    override fun onResponse(call: Call<ResendOTPResponse>, response: Response<ResendOTPResponse>) {
                        if (resendCount == 3) {
                            //llResendOtp.setEnabled(false);
                            llResendOtp.setAlpha(0.5f)
                        }
                        Log.e(TAG, "response resendOTP")
                        hideProgressDialog()
                        if (response?.body() != null && response.body()!!.getDBDetailsResult != null) {
                            if (response.body()!!.getDBDetailsResult.isResult) {
                                DialogUtils.showDefaultAlertMessage(this@OTPActivity, "", getString(R.string.an_otp_has_been_sent_to_your_registered_email_or_phone), getString(R.string.ok), null)
                            } else {
                                if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorDetails != null)
                                    showToast(response.body()!!.getDBDetailsResult.errorDetail.errorDetails)
                            }
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }

                    override fun onFailure(call: Call<ResendOTPResponse>, t: Throwable) {
                        hideProgressDialog()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                }
        )
    }

    private fun validateOTP(): String {
        var otp = ""
        if (etOtp4.length() > 0) {
            if (etOtp3.length() > 0) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        etOtp3.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            }
        } else {
            if (etOtp3.length() > 0) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {

                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        etOtp3.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            }
        }

        return otp
    }

    private fun requestVerifyOTP() {
        Log.e(TAG, "requestVerifyOTP")
        val OTP = validateOTP()
        if (TextUtils.isEmpty(OTP) || OTP.length < 4) {
            showToast(getString(R.string.invalid_otp))
            return
        }
        etOtp4.requestFocus()
        if (!GeneralUtils.isInternetAvailable(this)) {
            showToast(getString(R.string.no_internet))
            return
        }
        showProgressDialog(getString(R.string.validating_otp), getString(R.string.please_wait_))
        WebServiceRequests.getInstance().verifyOTP(
                OTP.trim { it <= ' ' },
                Preferences.getInstance(this@OTPActivity).email,
                Preferences.getInstance(this@OTPActivity).phone,
                Preferences.getInstance(this@OTPActivity).networkPassword,
                object : Callback<VerifyOTPResponse> {
                    override fun onResponse(call: Call<VerifyOTPResponse>, response: Response<VerifyOTPResponse>) {
                        Log.e(TAG, "response verifyOTP")
                        hideProgressDialog()
                        if (response?.body() != null) {
                            if (response.body()!!.getDBDetailsResult.isResult) {
                                Preferences.getInstance(this@OTPActivity).isOTPWaiting = false
                                DialogUtils.showDefaultAlertMessage(this@OTPActivity, "", getString(R.string.you_are_registered_successfully), getString(R.string.ok)) {
//                                    AppDatabase.clearDatabase()
                                    //Rahul; 2019/04/23 Doing so to not do any azure maintenance work after login/register as there is not need of data push if we are coming from login or register
                                    Preferences.getInstance(this@OTPActivity).putBoolean(Preferences.PREF_ALREADY_AZURE_MAINTAINED, true)
                                    /**** Implementation for Wifi Module  */
                                    val intent = Intent(this@OTPActivity, ProjectsActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                    startActivity(intent)
                                    finish()
                                }
                            } else {
                                clearOTP()
                                if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorDetails != null) {
                                    showToast(response.body()!!.getDBDetailsResult.errorDetail.errorDetails)
                                } else {
                                    showToast(getString(R.string.something_went_wrong))
                                }
                            }
                        } else {
                            clearOTP()
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }

                    override fun onFailure(call: Call<VerifyOTPResponse>, t: Throwable) {
                        hideProgressDialog()
                        clearOTP()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                }
        )
    }

    private fun clearOTP() {
        etOtp4.setText("")
        etOtp3.setText("")
        etOtp2.setText("")
        etOtp1.setText("")
        etOtp1.requestFocus()
        CommonUtils.showKeyBoard(this@OTPActivity)
    }

    override fun onBackPressed() {
        if (intent != null
                && intent.getStringExtra(BUNDLE_NAVIGATE_FROM) != null
                && intent.getStringExtra(BUNDLE_NAVIGATE_FROM).equals("SplashActivity", ignoreCase = true)) {
            val intent = Intent(this, RegistrationActivity::class.java)
            intent.putExtra(BUNDLE_NAVIGATE_FROM, OTPActivity::class.java.simpleName)
            startActivity(intent)
            finish()
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        private val TAG = OTPActivity::class.java.simpleName
    }
}
