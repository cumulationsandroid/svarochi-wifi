package com.svarochi.wifi.ui.room.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants.Companion.LIGHT_SYNCED
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.ui.room.view.fragment.MyGroupsFragment

class GroupsLightsAdapter(
        var lightsList: List<CustomLight>,
        var context: MyGroupsFragment
) :
        androidx.recyclerview.widget.RecyclerView.Adapter<GroupsLightsAdapter.AddNewGroupLightsViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): AddNewGroupLightsViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_new_group_light, viewGroup, false)
        return AddNewGroupLightsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lightsList.size
    }

    override fun onBindViewHolder(viewHolder: AddNewGroupLightsViewHolder, positiion: Int) {
        viewHolder.lightName.text = lightsList.get(positiion).light.name
        viewHolder.selectLightCheckBox.isChecked = lightsList.get(positiion).isSelected()

    }


    inner class AddNewGroupLightsViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var lightName: TextView = itemView.findViewById(R.id.tv_lightName)
        var selectLightCheckBox: CheckBox = itemView.findViewById(R.id.cb_selectLightCheckBox)

        init {
            selectLightCheckBox.setOnClickListener {
                if (lightsList.get(adapterPosition).light.isSynced == LIGHT_SYNCED) {
                    lightsList.get(adapterPosition).setSelected(selectLightCheckBox.isChecked)
                } else {
                    context.showToast(context.getString(R.string.toast_light_unsynced_group_scene))
                    selectLightCheckBox.isChecked = selectLightCheckBox.isChecked.not()
                }
            }
        }
    }
}