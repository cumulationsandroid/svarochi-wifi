package com.svarochi.wifi.ui.alexa;

import android.util.Log;

import com.willblaschko.android.alexa.callbacks.AsyncCallback;
import com.willblaschko.android.alexa.interfaces.AvsItem;
import com.willblaschko.android.alexa.interfaces.AvsResponse;
import com.willblaschko.android.alexa.interfaces.speechsynthesizer.AvsSpeakItem;
import com.willblaschko.android.alexa.requestbody.DataRequestBody;

import java.io.IOException;

import com.svarochi.wifi.SvarochiApplication;

import ee.ioc.phon.android.speechutils.AudioRecorder;
import ee.ioc.phon.android.speechutils.RawAudioRecorder;
import okio.BufferedSink;

/**
 * Implementation for Alexa and FCM
 */
public abstract class AlexaRecorder extends AlexaPlayer {
    private static final String TAG = "AlexaRecorder";
    private static final int AUDIO_RATE = 16000;
    protected RawAudioRecorder recorder;

    private AsyncCallback<AvsResponse, Exception> requestCallback = new AsyncCallback<AvsResponse, Exception>() {
        public long startTime;
        public String TAG = "AsyncCallback";

        @Override
        public void start() {
            if (isDetached()) {
                return;
            }
            startTime = System.currentTimeMillis();
            Log.i(TAG, "Event Start");
            setState(AlexaRequestState.LISTENING);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onStateChanged(state);
                }
            });

        }

        @Override
        public void success(AvsResponse result) {
            if (getActivity() == null) {
                return;
            }
            Log.i(TAG, "Event Success");
            if (result != null && result.size() > 0) {
                stopListening();
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i) != null && (result.get(i) instanceof AvsSpeakItem)) {
                        Log.e(TAG, "cid -" + ((AvsSpeakItem) result.get(i)).getCid());
                    } else {
                        Log.e(TAG, "noncid -" + result.get(i).toString());
                    }
                }
                dispatchResponse(result.get(0));
            } else {
                setState(AlexaRequestState.ERROR);
                stopListening();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onStateChanged(state);
                    }
                });
            }
        }

        @Override
        public void failure(Exception error) {
            if (getActivity() == null) {
                return;
            }
            error.printStackTrace();
            Log.i(TAG, "Event Error");
            setState(AlexaRequestState.ERROR);
            stopListening();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onStateChanged(state);
                }
            });
        }

        @Override
        public void complete() {
            if (getActivity() == null) {
                return;
            }
            Log.i(TAG, "Event Complete");
            long totalTime = System.currentTimeMillis() - startTime;
            Log.i(TAG, "Total request time: " + totalTime + " miliseconds");
        }
    };
    private DataRequestBody requestBody = new DataRequestBody() {
        @Override
        public void writeTo(BufferedSink sink) throws IOException {
            try {
                while (recorder != null && recorder.getState() != AudioRecorder.State.ERROR && !recorder.isPausing()) {
                    if (recorder != null) {
                        final float rmsdb = recorder.getRmsdb();
                        onAmplitudeChanged(rmsdb);
                        if (sink != null && recorder != null) {
                            sink.write(recorder.consumeRecording());
                        }
                    }
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            stopListening();
        }

    };

    protected void startListening() {
        Log.e(TAG, "startListening");
        if (recorder == null) {
            recorder = new RawAudioRecorder(AUDIO_RATE);
        } else {
            stopListening();
            recorder = new RawAudioRecorder(AUDIO_RATE);
        }
        SvarochiApplication.getAlexaManager().sendAudioRequest(requestBody, requestCallback);
        recorder.start();
    }

    protected void stopListening() {
        Log.e(TAG, "stopListening");
        try {
            if (recorder != null) {
                recorder.stop();
                recorder.release();
                recorder = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopListening();
    }

    @Override
    public void dispatchResponse(AvsItem current) {
        super.dispatchResponse(current);
    }

    public abstract void onAmplitudeChanged(float level);


}
