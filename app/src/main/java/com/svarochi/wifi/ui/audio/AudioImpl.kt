package com.svarochi.wifi.ui.audio

interface AudioImpl {
    fun getLights(networkId: Long)

    fun getGroups(networkId: Long)
}