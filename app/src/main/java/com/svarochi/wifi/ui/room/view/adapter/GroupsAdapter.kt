package com.svarochi.wifi.ui.room.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants.Companion.ADD_GROUP_ITEM
import com.svarochi.wifi.common.Constants.Companion.GROUP_ITEM
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.ui.room.view.fragment.MyGroupsFragment
import timber.log.Timber

class GroupsAdapter(var groupsList: ArrayList<Group>, var isSelfProfile: Boolean, var context: MyGroupsFragment) :
        androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        var mainView: View
        if (viewType == ADD_GROUP_ITEM) {
            mainView = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.item_add_new_group, viewGroup, false)

            return AddNewGroupViewHolder(mainView)
        }
        mainView = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_wifi_group, viewGroup, false)

        return GroupsViewHolder(mainView)
    }

    override fun getItemCount(): Int {
        return groupsList.size
    }

    override fun getItemViewType(position: Int): Int {
        if (groupsList[position].id == (-1).toLong()) {
            return ADD_GROUP_ITEM
        }
        return GROUP_ITEM
    }

    override fun onBindViewHolder(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (viewHolder.itemViewType == GROUP_ITEM) {
            (viewHolder as GroupsViewHolder).groupName.text = groupsList.get(position).name

            // viewHolder.groupCheckBox.isChecked = groupsList.get(position).onOffValue
        }
    }

    fun updateGroup(updatedGroup: Group) {
        var position = -1
        for (group in groupsList) {
            position++
            if (group.id == updatedGroup.id) {
                groupsList.set(position, updatedGroup)
                break
            }
        }
        notifyDataSetChanged()
    }

    inner class AddNewGroupViewHolder(mainView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(mainView) {
        var addGroup: ImageView = mainView.findViewById(R.id.iv_add)
        var addGroupText: TextView = mainView.findViewById(R.id.tv_addGroup)

        init {
            addGroup.setOnClickListener {
                // Open add group dialog

                context.showAddGroupDialog()

            }
            addGroupText.setOnClickListener {
                // Open add group dialog

                context.showAddGroupDialog()
            }
        }
    }

    inner class GroupsViewHolder(mainView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(mainView) {
        var groupName: TextView = mainView.findViewById(R.id.tv_groupName)
        var groupSettings: ImageView = mainView.findViewById(R.id.iv_groupSettings)
        var groupCheckBox: CheckBox = mainView.findViewById(R.id.cb_groupCheckBox)
        var groupEditLights: ImageView = mainView.findViewById(R.id.iv_editGroupLights)

        init {
            groupSettings.setOnClickListener {
                // Open group lamp control screen
                var position = adapterPosition
                if (context.isGroupContainingLights(groupsList.get(position).id!!)) {
                    context.openGroupControlScreen(groupsList.get(position).id!!)
                } else {
                    context.showToast(context.getString(R.string.toast_no_lights_associated_to_the_group))
                }
            }
            groupCheckBox.setOnClickListener {
                var position = adapterPosition
                Timber.d("Selected group id = ${groupsList.get(position).id!!}")
                if (context.isGroupContainingLights(groupsList.get(position).id!!)) {
                    Timber.d("Selected group is containing lights = ${groupsList.get(position).id!!}")

                    if (groupCheckBox.isChecked) {
                        // Turn on group
                        context.turnOnGroup(groupsList.get(position).id!!)
                    } else {
                        // Turn off group
                        context.turnOffGroup(groupsList.get(position).id!!)
                    }
                } else {
                    context.showToast(context.getString(R.string.toast_no_lights_associated_to_the_group))
                    Timber.d("Selected group is not containing lights = ${groupsList.get(position).id!!}")
                    groupCheckBox.isChecked = groupCheckBox.isChecked.not()
                }
            }

            groupEditLights.setOnClickListener {
                if (isSelfProfile) {
                    context.editGroupLightsList(groupsList.get(adapterPosition).name!!, groupsList.get(adapterPosition).id!!)
                } else {
                    Timber.d("User is in guest mode hence cannot edit group lights")
                }
            }

            groupName.setOnClickListener {
                if (isSelfProfile) {
                    context.editGroupLightsList(groupsList.get(adapterPosition).name!!, groupsList.get(adapterPosition).id!!)
                } else {
                    Timber.d("User is in guest mode hence cannot edit group lights")
                }
            }

            groupName.setOnLongClickListener {
                if (isSelfProfile) {
                    context.showDeleteGroupConfirmationDialog(groupsList.get(adapterPosition).id!!)
                } else {
                    Timber.d("User is in guest mode hence cannot delete the group")
                    true
                }
            }
        }
    }

}