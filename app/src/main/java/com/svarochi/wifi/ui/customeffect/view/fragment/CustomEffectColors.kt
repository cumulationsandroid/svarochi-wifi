package com.svarochi.wifi.ui.customeffect.view.fragment


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ITEM_CAMERA
import com.svarochi.wifi.common.Constants.Companion.ITEM_CANCEL
import com.svarochi.wifi.common.Constants.Companion.ITEM_COLOR_PALETTE
import com.svarochi.wifi.common.Constants.Companion.ITEM_PHOTO_GALLERY
import com.svarochi.wifi.ui.customeffect.view.activity.CustomEffectActivity
import com.svarochi.wifi.ui.customeffect.view.adapter.CustomEffectColorAdapter
import com.svarochi.wifi.ui.customeffect.view.callback.OnBottomSheetItemClickListener
import kotlinx.android.synthetic.main.fragment_custom_effect_colors.*
import timber.log.Timber


class CustomEffectColors : androidx.fragment.app.Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                iv_customEffectAddOwnColor.id -> {
                    showAddOwnColorBottomSheet()
                }
            }
        } else {
            Timber.d("$TAG View is null")
        }
    }

    private var TAG = "CustomEffectColorsFragment -"
    private var activityContext: CustomEffectActivity? = null
    private var colorList = ArrayList<Int>()
    private var bottomSheetItemCallback = object : OnBottomSheetItemClickListener {
        override fun onBottomSheetItemClick(item: Int) {
            when (item) {
                ITEM_CAMERA -> {

                }
                ITEM_PHOTO_GALLERY -> {

                }
                ITEM_CANCEL -> {
                    addPhotoBottomDialogFragment.dismiss()
                }
                ITEM_COLOR_PALETTE -> {

                }
            }
        }

    }
    private var addPhotoBottomDialogFragment: AddOwnColorFragment = AddOwnColorFragment.newInstance(bottomSheetItemCallback)


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_custom_effect_colors, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context != null) {
            activityContext = context as CustomEffectActivity
        } else {
            Timber.d("$TAG OnAttach context is null")
        }
    }

    private fun init() {
        initColorRv()
        initClickListeners()
    }

    private fun initClickListeners() {
        iv_customEffectAddOwnColor.setOnClickListener(this)
    }

    private fun initColorRv() {
        if (colorList.isEmpty()) {
            colorList.add(Constants.CUSTOM_EFFECT_PURPLE)
            colorList.add(Constants.CUSTOM_EFFECT_DARK_PURPLE)
            colorList.add(Constants.CUSTOM_EFFECT_BLUE)
            colorList.add(Constants.CUSTOM_EFFECT_GREEN)
        }

        if (activityContext != null) {
            rv_customEffectColors.layoutManager =
                    androidx.recyclerview.widget.LinearLayoutManager(
                            activityContext,
                            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
                            false
                    )
            rv_customEffectColors.adapter = CustomEffectColorAdapter(activityContext!!, colorList)
        } else {
            Timber.d("$TAG activity context is null")
        }
    }

    private fun showAddOwnColorBottomSheet() {
        addPhotoBottomDialogFragment.show(
                activityContext!!.supportFragmentManager,
                "add_photo_dialog_fragment"
        )
    }


}
