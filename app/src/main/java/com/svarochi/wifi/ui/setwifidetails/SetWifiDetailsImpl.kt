package com.svarochi.wifi.ui.setwifidetails

interface SetWifiDetailsImpl {
    fun getWifiDetails()

    fun setWifiDetails(ssid: String, password: String, authenticationMode: String, encryptionType: String)

}