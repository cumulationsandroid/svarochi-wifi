package com.svarochi.wifi.ui.customeffect.view.activity

import android.os.Bundle
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.ui.customeffect.view.fragment.CustomEffectColors


class CustomEffectActivity : WifiBaseActivity(), ServiceConnectionCallback {
    override fun serviceConnected() {
        init()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_effect)
        requestToBindService(this)
    }

    private fun init() {
        initFragments()
    }

    private fun initFragments() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction
                .add(R.id.f_fragmentContainer, CustomEffectColors())
                .commit()
    }
}
