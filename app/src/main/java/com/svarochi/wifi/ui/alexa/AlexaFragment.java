package com.svarochi.wifi.ui.alexa;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.svarochi.wifi.preference.Preferences;
import com.willblaschko.android.alexa.callbacks.AsyncCallback;
import com.willblaschko.android.alexa.callbacks.ImplAsyncCallback;
import com.willblaschko.android.alexa.callbacks.ImplAuthorizationCallback;
import com.willblaschko.android.alexa.utility.Util;

import bizbrolly.svarochiapp.R;

import com.svarochi.wifi.SvarochiApplication;

import static com.willblaschko.android.alexa.AlexaManager.KEY_URL_ENDPOINT;

/**
 * Implementation for Alexa and FCM
 */
public class AlexaFragment extends AlexaRecorder {

    private static final String TAG = "AlexaFragment";
    private ProgressBar recordingView;
    private TextView textView;
    private Handler handler = new Handler();
    private Runnable timeout = new Runnable() {
        @Override
        public void run() {
            stopListening();
        }
    };

    public AlexaFragment() {

    }

//    public static DummyFcmTester newInstance(String title) {
//        DummyFcmTester frag = new DummyFcmTester();
//        return frag;
//    }

    public static AlexaFragment newInstance(String title) {
        AlexaFragment frag = new AlexaFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    public static void signOut(Context context, AsyncCallback<Boolean, Throwable> callback) {
        Util.getPreferences(context).edit().putString(KEY_URL_ENDPOINT,"").commit();
        SvarochiApplication.getAlexaManager().signOut(callback);
        Preferences.getInstance(context).setAmEmail("");
        Preferences.getInstance(context).setToken("");
//        FcmParser.unRegister();
    }

    private static void doLogin(final AsyncCallback<Boolean, Throwable> callback) {
        callback.start();
        SvarochiApplication.getAlexaManager().logIn(new ImplAuthorizationCallback() {
            @Override
            public void onCancel() {
                callback.success(false);
                callback.failure(new Throwable("Authorization canceled"));
            }

            @Override
            public void onSuccess() {
                // don't show first time
                callback.success(true);
            }

            @Override
            public void onError(Exception error) {
                callback.success(false);
                callback.failure(new Throwable("Authorization onError:" + error.getMessage()));
            }

            @Override
            public void onLoading() {
                if (callback != null && callback instanceof ImplAsyncCallback) {
                    ((ImplAsyncCallback) callback).onLoading();
                }
            }
        });
    }

    public static void updateFCM(Context context) {
        String oldToken = Preferences.getInstance(context).getToken();
        String newToken = FirebaseInstanceId.getInstance().getToken();
        if (newToken != null &&
                newToken.length() > 0 &&
                !oldToken.equalsIgnoreCase(newToken)) {
            String[] temp = Preferences.getInstance(context).getAmEmail().split("\\.");
            if (temp != null && temp.length > 0 && temp[0].length() > 0) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                DatabaseReference myRef = mDatabase.getDatabase().getReference("/userinfo/" + temp[0] + "/deviceToken");
                myRef.setValue(newToken);
                Preferences.getInstance(context).setToken(newToken);
            } else {
                Log.e(TAG, "Amazon email is " + Preferences.getInstance(context).getAmEmail());
            }
        } else {
            Log.e(TAG, "FCM token is not going to push");
        }
    }

    public void show(AppCompatActivity activity, String tag) {
        super.show(activity.getSupportFragmentManager(), tag);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_alexa, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recordingView = (ProgressBar) view.findViewById(R.id.recorder_view);
        textView = (TextView) view.findViewById(R.id.txt_status);
        getDialog().setCanceledOnTouchOutside(false);
        handler.postDelayed(timeout, 5 * 1000);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#90000000")));
        startListening();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStateChanged(AlexaRecorder.AlexaRequestState state) {
        textView.setText(state.name());
        if (state == AlexaRequestState.ERROR) {
            Toast.makeText(getActivity(), "Please try again ", Toast.LENGTH_SHORT).show();
            dismiss();
        } else if (state == AlexaRequestState.COMPLETED) {
            dismiss();
        }
    }

    @Override
    public void onAmplitudeChanged(final float level) {
//        Log.e(TAG,"rdms-"+level);
    }
}
