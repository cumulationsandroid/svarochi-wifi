package com.svarochi.wifi.ui.addnewnetwork.viewmodel

import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.addnewnetwork.repository.NetworkDetailsRepository
import io.reactivex.disposables.CompositeDisposable

class NetworkDetailsViewModel(var repository: NetworkDetailsRepository) : BaseViewModel() {

    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }


    override fun onCleared() {
        disposables.clear()
        repository.dispose()
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    fun addNewNetwork(name: String, ssid: String, password: String, authenticationMode: String, encryptionType: String) {
        response.value = Event(EventType.ADD_NEW_NETWORK, EventStatus.LOADING, null, null)
        repository.addNewNetwork(name, ssid, password, authenticationMode, encryptionType)
    }

    fun editNetwork(networkId:Long, name: String, ssid: String, password: String, authenticationMode: String, encryptionType: String) {
        response.value = Event(EventType.EDIT_NETWORK, EventStatus.LOADING, null, null)
        repository.editNetwork(networkId, name, ssid, password, authenticationMode, encryptionType)
    }

    fun getLightsCount(networkId: Long): Int {
        return repository.getLightsCount(networkId)
    }

    fun deleteNetwork(networkId: Long, projectId:Long) {
        response.value = Event(EventType.DELETE_NETWORK, EventStatus.LOADING, null, null)
        repository.deleteNetwork(networkId,projectId)
    }


}