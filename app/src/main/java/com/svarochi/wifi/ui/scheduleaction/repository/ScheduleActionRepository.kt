package com.svarochi.wifi.ui.scheduleaction.repository

import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.Command
import com.svarochi.wifi.ui.scheduledetails.view.ActionDetails
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class ScheduleActionRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl {
    private val responseObservable = PublishSubject.create<Event>()
    private var TAG = "ScheduleActionRepository"

    override fun initCommunicationResponseObserver() {
    }

    override fun getCommunicationResponseObserver(): PublishSubject<Event> = responseObservable

    override fun dispose() {
    }

    fun showPreview(action: ActionDetails): Event {
        var startTime = 0.toLong()
        var endTime = 5.toLong()

        if (application.wifiCommunicationService.connectedToWifi || application.wifiCommunicationService.connectedToMobileData) {
            var schedulePayload = HashMap<String, Any>()

            if (action.onOffValue!!) {
                schedulePayload[Constants.BUNDLE_SCHEDULE_ID] = 1
                schedulePayload[Constants.BUNDLE_START_TIME] = startTime
                schedulePayload[Constants.BUNDLE_STOP_TIME] = endTime
                schedulePayload[Constants.BUNDLE_R_VALUE] = Utilities.convertHexStringToInt(action.rValue!!)
                schedulePayload[Constants.BUNDLE_G_VALUE] = Utilities.convertHexStringToInt(action.gValue!!)
                schedulePayload[Constants.BUNDLE_B_VALUE] = Utilities.convertHexStringToInt(action.bValue!!)
                schedulePayload[Constants.BUNDLE_W_VALUE] = Utilities.convertHexStringToInt(action.wValue!!)
                schedulePayload[Constants.BUNDLE_C_VALUE] = Utilities.convertHexStringToInt(action.cValue!!)
                schedulePayload[Constants.BUNDLE_BRIGHTNESS_VALUE] = action.brightnessValue!!
            } else {
                schedulePayload[Constants.BUNDLE_SCHEDULE_ID] = 1
                schedulePayload[Constants.BUNDLE_START_TIME] = startTime
                schedulePayload[Constants.BUNDLE_STOP_TIME] = endTime
                schedulePayload[Constants.BUNDLE_R_VALUE] = 0
                schedulePayload[Constants.BUNDLE_G_VALUE] = 0
                schedulePayload[Constants.BUNDLE_B_VALUE] = 0
                schedulePayload[Constants.BUNDLE_W_VALUE] = 0
                schedulePayload[Constants.BUNDLE_C_VALUE] = 0
                schedulePayload[Constants.BUNDLE_BRIGHTNESS_VALUE] = action.brightnessValue!!
            }

            action.selectedMacIds!!.forEach {
                Timber.d("$TAG Requesting SET_SCHEDULE command for device(${it}) via MQTT")
                var command = Command(EventType.ADD_NEW_SCHEDULE, it, schedulePayload)
                application.wifiCommunicationService.sendCommand(command)
            }

            return Event.setEvent(EventType.ADD_NEW_SCHEDULE, EventStatus.SUCCESS, null, null)
        } else {
            return Event.setEvent(EventType.ADD_NEW_SCHEDULE, EventStatus.FAILURE, Constants.NO_MQTT_CONNECTION, null)
        }
    }
}