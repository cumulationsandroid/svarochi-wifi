package com.svarochi.wifi.ui.schedulelist.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Utilities.Companion.getTime
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.Schedule

class ScheduleListAdapter(
        var schedulesList: ArrayList<Schedule>,
        var context: ScheduleListActivity
) : androidx.recyclerview.widget.RecyclerView.Adapter<ScheduleListAdapter.ScheduleListViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ScheduleListViewHolder {
        return ScheduleListViewHolder(
                LayoutInflater.from(viewGroup.context)
                        .inflate(R.layout.item_schedule, viewGroup, false)
        )
    }

    override fun getItemCount(): Int {
        return schedulesList.size
    }

    override fun onBindViewHolder(viewHolder: ScheduleListViewHolder, position: Int) {
        viewHolder.scheduleName.text = schedulesList[position].name
        viewHolder.endTime.text = getTime(schedulesList[position].endTime)
        viewHolder.startTime.text = getTime(schedulesList[position].startTime)

        var daysList = Converters.toStringArrayList(schedulesList[position].days)

        viewHolder.selectedDays.text = daysList[0].substring(0, 1).toUpperCase() + daysList[0].substring(1, 3)


        if (daysList.size - 1 != 0) {
            viewHolder.selectedDaysCountLayout.visibility = View.VISIBLE
            viewHolder.selectedDaysCountText.text = "+${daysList.size - 1}"
        }else{
            viewHolder.selectedDaysCountLayout.visibility = View.GONE
        }

    }


    inner class ScheduleListViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var scheduleName = itemView.findViewById<TextView>(R.id.tv_scheduleName)
        var endTime = itemView.findViewById<TextView>(R.id.tv_endTime)
        var startTime = itemView.findViewById<TextView>(R.id.tv_startTime)
        var selectedDays = itemView.findViewById<TextView>(R.id.tv_selectedDays)
        var container = itemView.findViewById<LinearLayout>(R.id.ll_scheduleLayout)
        var deleteSchedule = itemView.findViewById<ImageView>(R.id.iv_deleteSchedule)
        var selectedDaysCountLayout = itemView.findViewById<FrameLayout>(R.id.fl_selectedDaysCount)
        var selectedDaysCountText = itemView.findViewById<TextView>(R.id.tv_selectedDaysCount)

        init {
            container.setOnClickListener {
                var position = adapterPosition
                context.openSchedule(schedulesList.get(position))
            }

            deleteSchedule.setOnClickListener {
                var position = adapterPosition
                context.deleteSchedule(schedulesList.get(position).id!!)
            }
        }
    }
}