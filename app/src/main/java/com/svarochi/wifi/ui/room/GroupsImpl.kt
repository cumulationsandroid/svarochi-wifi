package com.svarochi.wifi.ui.room

import com.svarochi.wifi.database.entity.Light

interface GroupsImpl {

    fun getGroups(networkId: Long)

    fun addNewGroup(name: String, lightList: ArrayList<Light>)

    fun checkIfGroupExists(name: String, networkId: Long)

    fun editGroupName(groupId: Long, name: String)

    fun turnOnGroup(groupId: Long)

    fun turnOffGroup(groupId: Long)

    fun getLightsAndMarkThoseOfGroup(networkId: Long, groupId: Long) // For editing existing Group

    fun editGroupLightsList(groupId: Long, lightList: ArrayList<String>)

    fun addLightToGroup(macId: String, groupId: Long)

    fun removeLightFromGroup(macId: String, groupId: Long)

}