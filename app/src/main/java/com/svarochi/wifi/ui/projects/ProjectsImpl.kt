package com.svarochi.wifi.ui.projects

interface ProjectsImpl {

    fun getProjects()

    fun addNewProject(name: String)

    fun editProjectName(projectId:Long, name: String)

    fun deleteProject(projectId: Long)

}