package com.svarochi.wifi.ui.audio.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.CustomGroup
import com.svarochi.wifi.model.common.CustomLight
import timber.log.Timber
import com.svarochi.wifi.ui.audio.view.AudioActivity

class AudioDevicesAdapter(var context: AudioActivity) :
        androidx.recyclerview.widget.RecyclerView.Adapter<AudioDevicesAdapter.DeviceViewHolder>() {

    private var TYPE_LIGHT = 1
    private var TYPE_GROUP = 2
    private var lightsAndGroupList = ArrayList<Any>()
    private var isDeviceRecording = false

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): DeviceViewHolder {
        return DeviceViewHolder(
                LayoutInflater.from(viewGroup.context).inflate(
                        R.layout.item_audio_device,
                        viewGroup,
                        false
                )
        )
    }

    override fun getItemCount(): Int {
        return lightsAndGroupList.size
    }

    override fun onBindViewHolder(viewHolder: DeviceViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_LIGHT) {
            viewHolder.name.text = (lightsAndGroupList[position] as CustomLight).light.name
            viewHolder.deviceCheckbox.isChecked = (lightsAndGroupList[position] as CustomLight).isSelected()
        } else {
            viewHolder.name.text = (lightsAndGroupList[position] as CustomGroup).group.name
            viewHolder.deviceCheckbox.isChecked = (lightsAndGroupList[position] as CustomGroup).isSelected
        }
    }

    inner class DeviceViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var name = itemView.findViewById<TextView>(R.id.tv_deviceName)
        var deviceCheckbox = itemView.findViewById<CheckBox>(R.id.cb_deviceCheckbox)

        init {
            deviceCheckbox.setOnClickListener {
                var position = adapterPosition

                if (isDeviceRecording.not()) {
                    if (deviceCheckbox.isChecked) {
                        // Device is selected
                        if (getItemViewType(position) == TYPE_LIGHT) {
                            (lightsAndGroupList[position] as CustomLight).setSelected(true)
                            context.deviceSelected((lightsAndGroupList[position] as CustomLight).light.macId)
                        } else {
                            (lightsAndGroupList[position] as CustomGroup).isSelected = true
                            context.deviceSelected((lightsAndGroupList[position] as CustomGroup).group.id!!)
                        }

                    } else {
                        // Device is not selected
                        if (getItemViewType(position) == TYPE_LIGHT) {
                            context.deviceUnSelected((lightsAndGroupList[position] as CustomLight).light.macId)
                            (lightsAndGroupList[position] as CustomLight).setSelected(false)
                        } else {
                            context.deviceUnSelected((lightsAndGroupList[position] as CustomGroup).group.id!!)
                            (lightsAndGroupList[position] as CustomGroup).isSelected = false
                        }
                    }
                } else {
                    deviceCheckbox.isChecked = deviceCheckbox.isChecked.not()
                    context.showToast(context.getString(R.string.toast_stop_recording_and_then_unselect_the_device))
                }
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        if (lightsAndGroupList[position] is CustomLight) {
            return TYPE_LIGHT
        }
        return TYPE_GROUP
    }

    fun setRecordingStatus(flag: Boolean) {
        isDeviceRecording = flag
    }

    fun addToList(devices: List<Any>) {
        for (device in devices) {
            if (device is Light) {
                var customLight = CustomLight(device)
                /*if (device.lampType == LampType.COLOR_AND_DAYLIGHT.value) {
                    lightsAndGroupList.add(device)
                }*/
                // TODO - Uncomment above if block and comment below if block
                if (true) {
                    lightsAndGroupList.add(customLight)
                }

            } else if (device is Group) {
                var customGroup = CustomGroup(device)
                /*if (device.groupTag == GROUP_TAG_COLOR_ONLY) {
                    lightsAndGroupList.add(device)
                }*/
                // TODO - Uncomment above if block and comment below if block
                if (true) {
                    lightsAndGroupList.add(customGroup)
                }
            }
        }
        notifyDataSetChanged()
    }

    fun resetCheckStatus(id: Any) {
        for (device in lightsAndGroupList) {
            if (device is CustomLight) {
                if (device.light.macId.equals(id)) {
                    Timber.d("Light status - ${device.isSelected().not()}")
                    device.setSelected(device.isSelected().not())
                    Timber.d("Light(${device.light.macId}) is unselected")
                    notifyDataSetChanged()
                    break
                }
            } else if (device is CustomGroup) {
                if (device.group.id == id) {
                    Timber.d("Group status - ${device.isSelected.not()}")
                    device.isSelected = device.isSelected.not()
                    Timber.d("Group(${device.group.id}) is unselected")
                    notifyDataSetChanged()
                    break
                }
            }
        }
    }

}