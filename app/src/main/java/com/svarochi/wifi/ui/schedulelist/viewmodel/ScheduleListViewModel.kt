package com.svarochi.wifi.ui.schedulelist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Schedule
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.schedulelist.ScheduleListImpl
import com.svarochi.wifi.ui.schedulelist.repository.ScheduleListRepository
import io.reactivex.disposables.CompositeDisposable

class ScheduleListViewModel(var repository: ScheduleListRepository) : BaseViewModel(), ScheduleListImpl {

    private var disposables: CompositeDisposable = CompositeDisposable()
    var response: MutableLiveData<Event> = MutableLiveData()

    override fun onCleared() {
        repository.dispose()
    }

    init {
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }

    fun getSchedules(): LiveData<List<Schedule>> {
        return repository.getScehdules()
    }

    fun deleteSchedule(scheduleId: Long) {
        response.value = Event(EventType.DELETE_SCHEDULE, EventStatus.LOADING, null, null)
        repository.deleteSchedule(scheduleId.toInt())
    }


}