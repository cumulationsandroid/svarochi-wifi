package com.svarochi.wifi.ui.room.view.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NETWORK
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NETWORK_ID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PROJECT_ID
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.common.Constants.Companion.SVAROCHI_LAMP_IDENTIFIER
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.audio.view.AudioActivity
import com.svarochi.wifi.ui.projects.view.activity.ProjectsActivity
import com.svarochi.wifi.ui.room.view.adapter.RoomPagerAdapter
import com.svarochi.wifi.ui.room.view.fragment.BleLightsFragment
import com.svarochi.wifi.ui.room.view.fragment.MyGroupsFragment
import com.svarochi.wifi.ui.room.view.fragment.ScenesFragment
import com.svarochi.wifi.ui.room.viewmodel.RoomViewModel
import com.svarochi.wifi.ui.schedulelist.view.ScheduleListActivity
import kotlinx.android.synthetic.main.activity_room.*
import timber.log.Timber


class RoomActivity : WifiBaseActivity(), View.OnClickListener, ServiceConnectionCallback {
    private lateinit var tabAdapter: RoomPagerAdapter
    var viewModel: RoomViewModel? = null
    private var isInitialised = false
    lateinit var wifiManager: WifiManager
    private var locationManager: LocationManager? = null
    var connectivityManager: ConnectivityManager? = null
    private var bleLightsFragment: BleLightsFragment? = null
    var lightsCount = 0
    private var macIdMap = HashMap<String, String>()
    var shouldWriteWifiCredentials = false
    var dialog: Dialog? = null
    var network: com.svarochi.wifi.database.entity.Network? = null
    private var doubleBackToExitPressedOnce = false
    private var isNetworkAvailable: Boolean = false
    private lateinit var preference: SharedPreference
    var DEVICE_SETUP_STATE: String = Constants.INITIAL
    var isSelfProfile = true
    private var previousConnectedSsid = ""

    override fun onClick(v: View?) {
        when (v!!.id) {
            iv_music.id -> {
                openMusicActivity()
            }
            iv_options.id -> {
                showOptionsMenu(v)
            }

            iv_scheduler.id -> {
                showScheduleListActivity()
            }
            iv_back.id -> {
                backOnPress()
            }
        }
    }

    private fun backOnPress() {
        viewModel?.stopBroadcast()
        preference.setNetworkId(-1)
        finish()
    }

    private fun showScheduleListActivity() {
        if (network != null) {
            startActivity(
                    Intent(this, ScheduleListActivity::class.java)
                            .putExtra(BUNDLE_PROJECT_ID, network!!.projectId)
                            .putExtra(BUNDLE_NETWORK_ID, network!!.id)
            )
        } else {
            // Should not occur
            Timber.d("Network is null")
        }
    }

    private fun openMusicActivity() {
        if (network != null) {
            startActivity(Intent(this, AudioActivity::class.java).putExtra(BUNDLE_NETWORK_ID, network!!.id))
        } else {
            // Should not occur
            Timber.d("Network is null")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room)
        requestToBindService(this)
    }


    override fun serviceConnected() {
        init()
    }

    private fun init() {
        preference = SharedPreference(this)
        isSelfProfile = preference.getCurrentProfile().equals(CURRENT_PROFILE_SELF)
        initBundleData()
        saveToPreferences(false)
        initViewModel()
        initObservers()
        initFragments()
        initClickListeners()
        initManagersAndCallbacks()
        initViews()
        viewModel?.stopBroadcast()
        viewModel?.startBroadcast()
    }

    fun initBundleData() {
        if (intent.extras != null) {
            network = intent.getSerializableExtra(BUNDLE_NETWORK) as com.svarochi.wifi.database.entity.Network
        }
    }

    private fun saveToPreferences(resetToDefault: Boolean) {
        if (resetToDefault) {
            Timber.d("Resetting networkId and networkName to defaults(-1)")
            preference.setNetworkId(-1)
            preference.setNetworkName("")
        } else {
            if (network != null) {
                Timber.d("Saving the last opened network to preference - NetworkId(${network!!.id}), NetworkName(${network!!.name})")
                preference.setNetworkId(network!!.id)
                preference.setNetworkName(network!!.name)
            }
        }
    }

    private fun initObservers() {
        viewModel!!.getResponse().observe(this, responseObserver)
    }

    private var responseObserver = Observer<Event> {
        processResponse(it!!)
    }

    private fun processResponse(event: Event) {
        when (event.type) {
            EventType.UDP_BROADCAST -> {
                when (event.status) {

                    EventStatus.SUCCESS -> {
                        var macId = (event.data!! as String)

                        macIdMap[macId] = macId
                    }

                    EventStatus.FAILURE -> {
                        var macId = (event.data!! as String)

                        macIdMap.remove(macId)
                    }
                }
            }
        }
    }

    private fun initViews() {
        if (network != null) {
            tv_roomName.text = network!!.name
        }
        initDialogs()
        if (isSelfProfile) {
            iv_scheduler.visibility = View.VISIBLE
        } else {
            iv_scheduler.visibility = View.GONE
        }

    }

    fun initDialogs() {
        dialog = Dialog(this@RoomActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.dialog_loading)
        dialog!!.setCanceledOnTouchOutside(false)
    }

    private fun initClickListeners() {
        iv_music.setOnClickListener(this)
        iv_options.setOnClickListener(this)
        iv_scheduler.setOnClickListener(this)
        iv_back.setOnClickListener(this)
    }

    fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(RoomViewModel::class.java)
    }


    private fun initFragments() {
        tabAdapter = RoomPagerAdapter(supportFragmentManager)
        bleLightsFragment = BleLightsFragment()
        tabAdapter.addFragment(bleLightsFragment!!, "Lights")
        tabAdapter.addFragment(MyGroupsFragment.newInstance(), "Groups")
        tabAdapter.addFragment(ScenesFragment.newInstance(), "Scenes")

        vp_roomsViewPager.adapter = tabAdapter
        tl_roomTabLayout.setupWithViewPager(vp_roomsViewPager)
        vp_roomsViewPager.offscreenPageLimit = 3
        vp_roomsViewPager.setPagingEnabled(false)

    }

    private fun initManagersAndCallbacks() {
        wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        connectivityManager =
                applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager


    }

    override fun onStart() {
        if (viewModel != null) {
            viewModel!!.initObservers()
        }
        reqisterNetworkCallback()
        super.onStart()

    }

    override fun onStop() {
        unregisterNetworkCallback()
        super.onStop()
    }

    private fun showOptionsMenu(view: View) {
        var popup = PopupMenu(this, view)
        popup.menuInflater.inflate(R.menu.options_room, popup.menu)
        popup.setOnMenuItemClickListener(optionMenuItemClickListener)
        popup.show()

    }

    private val optionMenuItemClickListener = object : PopupMenu.OnMenuItemClickListener {
        override fun onMenuItemClick(item: MenuItem?): Boolean {
            when (item!!.itemId) {
                R.id.mi_projects -> {
                    openProjectsScreen()
                }
            }
            return false
        }

    }

    private fun openProjectsScreen() {
        saveToPreferences(true)
        startActivity(Intent(this, ProjectsActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }

    override fun onBackPressed() {
        backOnPress()
    }

    private var wifiChangeCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            Timber.d("Wifi Network - Available")
            isNetworkAvailable = true

            if ((!wifiManager.connectionInfo.ssid.isNullOrEmpty() &&
                            !wifiManager.connectionInfo.ssid.contains(SVAROCHI_LAMP_IDENTIFIER, false) &&
                            !wifiManager.connectionInfo.ssid.contains("<unknown ssid>", true)) ||
                    (connectivityManager!!.getNetworkInfo(network) != null &&
                            !connectivityManager!!.getNetworkInfo(network).extraInfo.isNullOrEmpty() &&
                            !connectivityManager!!.getNetworkInfo(network).extraInfo.contains(
                                    SVAROCHI_LAMP_IDENTIFIER,
                                    false
                            ))
            ) {
                Timber.d("Successfully connected to non svarochi wifi network - ${wifiManager.connectionInfo.ssid}")

                if (shouldWriteWifiCredentials && !previousConnectedSsid.equals(getConnectedSsid())) {
                    Timber.d("Failed to connect to Svarochi hotspot")
                    bleLightsFragment!!.phoneFailedToConnectToHotspot()
                }

                if (shouldWriteWifiCredentials) {
                    previousConnectedSsid = ""
                    shouldWriteWifiCredentials = false
                }
            } else {
                Timber.d("Successfully connected to svarochi wifi network - ${wifiManager.connectionInfo.ssid}")
                if (wifiManager.connectionInfo.ssid.contains(
                                SVAROCHI_LAMP_IDENTIFIER,
                                false
                        ) && shouldWriteWifiCredentials
                ) {
                    previousConnectedSsid = ""
                    bleLightsFragment!!.showLightNameDialog(wifiManager.connectionInfo.ssid)
                }
            }

        }

        override fun onUnavailable() {
            isNetworkAvailable = false
            Timber.d("Wifi Network - onUnAvailable")
        }

        override fun onLost(network: Network?) {
            isNetworkAvailable = false
            Timber.d("Wifi Network - onLost")
        }
    }

    private fun reqisterNetworkCallback() {
        var builder = NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build()
        if (connectivityManager != null) {
            connectivityManager!!.registerNetworkCallback(builder, wifiChangeCallback)
            Timber.d("Registered to wifi change callback")
        }
    }

    private fun unregisterNetworkCallback() {
        if (connectivityManager != null) {
            connectivityManager!!.unregisterNetworkCallback(wifiChangeCallback)
            Timber.d("Unregistered from Wifi change callback")
        }
    }

    fun closePopupIfNetworkAvailable() {
        if (isNetworkAvailable) {
            dialog!!.cancel()
        }
    }

    private fun setCurrentState(value: String) {
        DEVICE_SETUP_STATE = value
        Timber.d("DEVICE SETUP STATE - ${DEVICE_SETUP_STATE}")
    }

    fun showLightsFragmentToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun configureNewDevice() {
        shouldWriteWifiCredentials = true
        previousConnectedSsid = getConnectedSsid()
        openWifiSettingsScreen()
    }

    private fun openWifiSettingsScreen() {
        startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
    }

    private fun getConnectedSsid(): String {
        if (wifiManager != null && wifiManager.connectionInfo != null && wifiManager.connectionInfo.ssid != null && wifiManager.connectionInfo.ssid.isNotEmpty()) {
            return wifiManager.connectionInfo.ssid
        }
        return ""
    }


}
