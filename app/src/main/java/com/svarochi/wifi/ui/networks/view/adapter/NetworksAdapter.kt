package com.svarochi.wifi.ui.networks.view.adapter

import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.ui.networks.view.activity.NetworksActivity

class NetworksAdapter(var networksList: List<Network>, var isSelfProfile: Boolean, var context: NetworksActivity) :
        androidx.recyclerview.widget.RecyclerView.Adapter<NetworksAdapter.NetworksViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): NetworksViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_networks, viewGroup, false)
        return NetworksViewHolder(view)
    }

    override fun getItemCount(): Int = networksList.size

    override fun onBindViewHolder(viewHolder: NetworksViewHolder, position: Int) {
        viewHolder.name.text = networksList.get(position).name
        viewHolder.type.setImageResource(R.drawable.ic_wifi)

        if (isSelfProfile){
            viewHolder.edit.visibility = View.VISIBLE
        }else{
            viewHolder.edit.visibility = View.GONE
        }
    }


    inner class NetworksViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.tv_networkName)
        var type: ImageView = itemView.findViewById(R.id.iv_networkType)
        var edit: ImageView = itemView.findViewById(R.id.iv_edit)



        init {
            edit.setOnClickListener {
                context.editNetwork(networksList.get(adapterPosition))
            }
            name.setOnClickListener {
                context.openNetwork(networksList.get(adapterPosition))
            }
        }
    }
}