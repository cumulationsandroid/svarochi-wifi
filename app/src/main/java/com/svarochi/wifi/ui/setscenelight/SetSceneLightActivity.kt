package com.svarochi.wifi.ui.setscenelight

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.akkipedia.skeleton.utils.ScreenUtils
import com.jakewharton.rxbinding2.widget.RxSeekBar
import com.jakewharton.rxbinding2.widget.SeekBarProgressChangeEvent
import com.jakewharton.rxbinding2.widget.SeekBarStartChangeEvent
import com.jakewharton.rxbinding2.widget.SeekBarStopChangeEvent
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.colorpicker.WifiColorPickerDialog
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_CUSTOM_SCENE_DEVICE
import com.svarochi.wifi.common.Constants.Companion.MQTT_TRANSITION_DELAY
import com.svarochi.wifi.common.Constants.Companion.PHOTO_GALLERY
import com.svarochi.wifi.common.Constants.Companion.TCP_TRANSITION_DELAY
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.common.Utilities.Companion.convertHexStringToInt
import com.svarochi.wifi.common.Utilities.Companion.convertIntToHexString
import com.svarochi.wifi.common.Utilities.Companion.getLampOnOffStatus
import com.svarochi.wifi.database.LampType
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.api.common.CustomSceneDevice
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.ui.lightcontrol.view.fragment.ImagePickerDialogFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_set_scene_light.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue
import kotlin.math.roundToInt

class SetSceneLightActivity : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener, ImagePickerDialogFragment.ColorPickerInterface {
    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                iv_back.id -> {
                    onBackPressed()
                }
                ll_rainbow.id -> {
                    setScene(SceneType.RAINBOW)
                }
                tv_study.id -> {

                    setScene(SceneType.STUDY)
                }
                ll_energise.id -> {
                    setScene(SceneType.ENERGISE)
                }
                tv_aqua.id -> {
                    setScene(SceneType.AQUA)
                }
                ll_candleLight.id -> {
                    setScene(SceneType.CANDLE_LIGHT)
                }
                tv_moonLight.id -> {
                    setScene(SceneType.MOON_LIGHT)
                }
                ll_partyMode.id -> {
                    setScene(SceneType.PARTY_MODE)
                }
                ll_sunset.id -> {
                    setScene(SceneType.SUNSET)
                }
                tv_meditation.id -> {
                    setScene(SceneType.MEDITATION)
                }
                ll_colorBlast.id -> {
                    setScene(SceneType.COLOR_BLAST)
                }
                tv_colorPalette.id -> {
                    showColorPalette()
                }
                tv_photoGallery.id -> {
                    openPhotoGallery()
                }
                tv_camera.id -> {
                    openCamera()
                }
                ll_wc_candleLight.id -> {
                    setScene(SceneType.CANDLE_LIGHT)
                }
            }
        }
    }

    private var sceneDevice: CustomSceneDevice? = null
    private var viewModel: SetSceneLightViewModel? = null
    private var isLocallyAvailable = false
    private var disposables = CompositeDisposable()
    private val BRIGHTNESS_MAX = 100
    private val BRIGHTNESS_MIN = 10
    private val BRIGHTNESS_STEP = 1
    private var mColorPickerDialog: WifiColorPickerDialog? = null
    private var lastSentColor = "000000"
    private var lastColorSentTime: Long = 0
    private var isInit = true

    override fun serviceConnected() {
        init()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_scene_light)
        requestToBindService(this)
    }

    private fun init() {
        getBundleData()
        initViewModel()
        listenToLightChanges()
        initListeners()
        initClickListeners()
        initColorPickerDialog()
    }

    private fun initClickListeners() {
        iv_back.setOnClickListener(this)
        tv_aqua.setOnClickListener(this)
        ll_candleLight.setOnClickListener(this)
        tv_moonLight.setOnClickListener(this)
        ll_rainbow.setOnClickListener(this)
        ll_sunset.setOnClickListener(this)
        ll_partyMode.setOnClickListener(this)
        tv_meditation.setOnClickListener(this)
        ll_colorBlast.setOnClickListener(this)
        ll_energise.setOnClickListener(this)
        tv_study.setOnClickListener(this)
        tv_colorPalette.setOnClickListener(this)
        tv_photoGallery.setOnClickListener(this)
        tv_camera.setOnClickListener(this)
        ll_wc_candleLight.setOnClickListener(this)
    }

    private fun initListeners() {
        listenToBrightnessChangeEvents()
        listenToDaylightChangeEvents()

        cb_onOffLight.setOnClickListener {
            if (cb_onOffLight.isChecked) {
                cb_onOffLight.isChecked = cb_onOffLight.isChecked.not()
                //requestingTurnOn = true
                turnOnLight()
            } else {
                cb_onOffLight.isChecked = cb_onOffLight.isChecked.not()
                //requestingTurnOn = false
                turnOffLight()
            }
        }
    }

    private fun initColorPickerDialog() {
        if (mColorPickerDialog == null) {
            mColorPickerDialog = WifiColorPickerDialog.createColorPickerDialog(this)
            mColorPickerDialog!!.hideColorComponentsInfo()
            mColorPickerDialog!!.hideOpacityBar()
            mColorPickerDialog!!.hideHexaDecimalValue()
            mColorPickerDialog!!.hidePreviewBox()
            mColorPickerDialog!!.setOnColorPickedListener(onColorPickedListener)
        } else {
            Timber.d("Color picker dialog already initialised")
        }
    }

    private fun listenToLightChanges() {
        if (viewModel != null && sceneDevice != null) {
            viewModel!!.getLight(sceneDevice!!.mac_id).observe(this, Observer({

                if (isInit) {
                    isInit = false
                    updateEssentialLightDetails(it)
                    updateUiBasedOnSceneDevice()
                } else {
                    updateUiBasedOnDb(it)
                }
            }))
        }
    }

    private fun updateUiBasedOnSceneDevice() {
        if (sceneDevice != null) {
            cb_onOffLight.isChecked = getLampOnOffStatus(sceneDevice!!.status)

            sb_daylightControl.progress = sceneDevice!!.daylight_control

            sb_brightness.progress = calculateProgress((sceneDevice!!.brightness.toFloat()).roundToInt(), BRIGHTNESS_MIN, BRIGHTNESS_MAX, BRIGHTNESS_STEP)

            when (convertIntToHexString(sceneDevice!!.scene_value)) {
                SceneType.CANDLE_LIGHT.value -> {
                    ll_candleLight.setBackgroundResource(R.drawable.today_tomorrow_selected)

                    ll_rainbow.background = null
                    ll_energise.background = null
                    ll_colorBlast.background = null
                    ll_sunset.background = null
                    ll_partyMode.background = null
                }
                SceneType.ENERGISE.value -> {
                    ll_energise.setBackgroundResource(R.drawable.today_tomorrow_selected)

                    ll_rainbow.background = null
                    ll_candleLight.background = null
                    ll_colorBlast.background = null
                    ll_sunset.background = null
                    ll_partyMode.background = null
                }
                SceneType.SUNSET.value -> {
                    ll_sunset.setBackgroundResource(R.drawable.today_tomorrow_selected)

                    ll_rainbow.background = null
                    ll_candleLight.background = null
                    ll_colorBlast.background = null
                    ll_energise.background = null
                    ll_partyMode.background = null
                }
                SceneType.COLOR_BLAST.value -> {
                    ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                    ll_rainbow.background = null
                    ll_candleLight.background = null
                    ll_sunset.background = null
                    ll_energise.background = null
                    ll_partyMode.background = null
                }
                SceneType.PARTY_MODE.value -> {
                    ll_partyMode.setBackgroundResource(R.drawable.today_tomorrow_selected)

                    ll_rainbow.background = null
                    ll_candleLight.background = null
                    ll_sunset.background = null
                    ll_energise.background = null
                    ll_colorBlast.background = null
                }
                SceneType.RAINBOW.value -> {
                    ll_rainbow.setBackgroundResource(R.drawable.today_tomorrow_selected)

                    ll_partyMode.background = null
                    ll_candleLight.background = null
                    ll_sunset.background = null
                    ll_energise.background = null
                    ll_colorBlast.background = null
                }
                else -> {
                    ll_rainbow.background = null
                    ll_partyMode.background = null
                    ll_candleLight.background = null
                    ll_sunset.background = null
                    ll_energise.background = null
                    ll_colorBlast.background = null
                }
            }
        }
    }

    private fun updateEssentialLightDetails(updatedLight: Light) {
        when (updatedLight.lampType) {
            LampType.NONE.value -> {
                hideAllControls()
            }
            LampType.BRIGHT_AND_DIM.value -> {
                ivLightType.setImageResource(R.drawable.icon_bright_dim)
                showBrightAndDimLampControls()
            }
            LampType.WARM_AND_COOL.value -> {
                ivLightType.setImageResource(R.drawable.icon_warm_cool)
                showWarmAndCoolLampControls()
            }
            LampType.COLOR_AND_DAYLIGHT.value -> {
                ivLightType.setImageResource(R.drawable.icon_color_daylight)
                showColorLampControls()
            }
            else -> {
                ivLightType.setImageResource(R.drawable.icon_unpaired)
                hideAllControls()
            }
        }

        tv_lightHeaderName.text = updatedLight.name
        tv_lightName.text = updatedLight.name

        when (updatedLight.lampType) {
            LampType.COLOR_AND_DAYLIGHT.value -> {
                tv_lightTypeName.text =
                        Html.fromHtml("${getString(R.string.label_color_and_daylight)}<sup><small>${getString(R.string.label_plus)}</small></sup>")
            }
            LampType.WARM_AND_COOL.value -> {
                tv_lightTypeName.text = getString(R.string.label_warm_and_cool)
            }
            LampType.BRIGHT_AND_DIM.value -> {
                tv_lightTypeName.text = getString(R.string.label_bright_and_dim)
            }
            else -> {
                tv_lightTypeName.text = getString(R.string._dash)
            }
        }

        tv_lightFwVersion.visibility = View.VISIBLE
        tv_lightFwVersion.text = getString(R.string.label_firmware_version_) + updatedLight.firmwareVersion
    }

    private fun updateUiBasedOnDb(updatedLight: Light) {
        updateSceneDeviceBasedOnDb(updatedLight)
        updateUiBasedOnSceneDevice()
    }

    private fun updateSceneDeviceBasedOnDb(updatedLight: Light) {
        sceneDevice!!.red = convertHexStringToInt(updatedLight.rValue)
        sceneDevice!!.green = convertHexStringToInt(updatedLight.gValue)
        sceneDevice!!.blue = convertHexStringToInt(updatedLight.bValue)
        sceneDevice!!.daylight_control = Utilities.getDaylightValue(updatedLight.cValue)
        sceneDevice!!.brightness = updatedLight.brightnessValue
        sceneDevice!!.status = getLampOnOffStatus(updatedLight.onOffValue)
    }

    private fun calculateProgress(value: Int, MIN: Int, MAX: Int, STEP: Int): Int {
        return 100 * (value - MIN) / (MAX - MIN)
    }

    private fun calculatePercentage(value: Float): Float {
        return (value / 255) * 100
    }

    private fun showColorLampControls() {
        ll_warmAndCoolEffectsControl.visibility = View.GONE

        ll_allEffectsControl.visibility = View.VISIBLE
        ll_colorControl.visibility = View.VISIBLE
        ll_nameLayout.visibility = View.VISIBLE
        ll_brightnessControl.visibility = View.VISIBLE
        ll_daylightControl.visibility = View.VISIBLE
    }

    private fun showWarmAndCoolLampControls() {
        ll_allEffectsControl.visibility = View.GONE
        ll_colorControl.visibility = View.GONE
        ll_warmAndCoolEffectsControl.visibility = View.GONE

        ll_nameLayout.visibility = View.VISIBLE
        ll_brightnessControl.visibility = View.VISIBLE
        ll_daylightControl.visibility = View.VISIBLE
    }

    private fun showBrightAndDimLampControls() {
        ll_allEffectsControl.visibility = View.GONE
        ll_warmAndCoolEffectsControl.visibility = View.GONE
        ll_daylightControl.visibility = View.GONE
        ll_colorControl.visibility = View.GONE

        ll_nameLayout.visibility = View.VISIBLE
        ll_brightnessControl.visibility = View.VISIBLE
    }

    private fun hideAllControls() {
        ll_allEffectsControl.visibility = View.GONE
        ll_warmAndCoolEffectsControl.visibility = View.GONE
        ll_nameLayout.visibility = View.GONE
        ll_brightnessControl.visibility = View.GONE
        ll_daylightControl.visibility = View.GONE
    }


    private fun getBundleData() {
        if (intent.extras != null) {
            sceneDevice = intent.getSerializableExtra(Constants.BUNDLE_CUSTOM_SCENE_DEVICE) as CustomSceneDevice
        } else {
            finish()
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(SetSceneLightViewModel::class.java)
        viewModel?.response!!.observe(this, Observer { t -> processResponse(t!!) })
    }

    private fun processResponse(event: Event) {
        when (event.type) {
            EventType.CONNECTION -> {
                when (event.status) {
                    EventStatus.FAILURE -> {
                        showLoadingPopup(false, null)
                        if (event.data != null && event.data is String) {
                            Timber.d("${event.data} is not locally available")
                            isLocallyAvailable = false

                            disposables.clear()
                            disposables = CompositeDisposable()
                            listenToBrightnessChangeEvents()
                            listenToDaylightChangeEvents()
                        }
                    }
                    EventStatus.SUCCESS -> {
                        if (event.data != null && event.data is String) {
                            Timber.d("${event.data}  is locally available")
                            isLocallyAvailable = true

                            disposables.clear()
                            disposables = CompositeDisposable()
                            listenToBrightnessChangeEvents()
                            listenToDaylightChangeEvents()
                        }
                    }
                }
            }
        }
    }

    private fun listenToBrightnessChangeEvents() {
        disposables.add(RxSeekBar.changeEvents(sb_brightness)
                .flatMap {
                    when (it) {
                        is SeekBarProgressChangeEvent -> {
                            if (it.fromUser()) {
                                var value = Math.round((it.progress().toDouble()) * (BRIGHTNESS_MAX - BRIGHTNESS_MIN) / 100)
                                var displayValue = (value.toInt() + BRIGHTNESS_MIN) / BRIGHTNESS_STEP * BRIGHTNESS_STEP
                                tvBubbleIntensityValue.text = "$displayValue%"
                                rlBubbleIntensity.x = (getIntensitySliderThumbPos() - ScreenUtils.dpToPx(13)).toFloat()
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Start")
                            rlBubbleIntensity.visibility = View.VISIBLE
                        }
                        is SeekBarStopChangeEvent -> {
                            rlBubbleIntensity.visibility = View.GONE
                        }
                    }
                    Observable.just(it)
                }
                .debounce(if (isLocallyAvailable) TCP_TRANSITION_DELAY.toLong() else MQTT_TRANSITION_DELAY.toLong(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekBarChangeEvent ->
                    when (seekBarChangeEvent) {
                        is SeekBarProgressChangeEvent -> {
                            Timber.d("Change Events - Progress from user = ${seekBarChangeEvent.fromUser()}")
                            Timber.d("Change Events - Progress = ${seekBarChangeEvent.progress()}")

                            if (seekBarChangeEvent.fromUser()) {
                                var value = Math.round((seekBarChangeEvent.progress().toDouble()) * (BRIGHTNESS_MAX - BRIGHTNESS_MIN) / 100)
                                var displayValue = (value.toInt() + BRIGHTNESS_MIN) / BRIGHTNESS_STEP * BRIGHTNESS_STEP
                                setLightBrightness(displayValue)
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Start")
                        }
                        is SeekBarStopChangeEvent -> {
                            Timber.d("Change Events - Stop = ${seekBarChangeEvent.view().progress}")
                            Timber.d("Change Events - Stop")


                            var value = Math.round((seekBarChangeEvent.view().progress.toDouble()) * (BRIGHTNESS_MAX - BRIGHTNESS_MIN) / 100)
                            var displayValue = (value.toInt() + BRIGHTNESS_MIN) / BRIGHTNESS_STEP * BRIGHTNESS_STEP
                            setLightBrightness(displayValue)

                        }
                    }
                }
        )
    }

    private fun listenToDaylightChangeEvents() {
        disposables.add(RxSeekBar.changeEvents(sb_daylightControl)
                .debounce(if (isLocallyAvailable) TCP_TRANSITION_DELAY.toLong() else MQTT_TRANSITION_DELAY.toLong(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekBarChangeEvent ->
                    when (seekBarChangeEvent) {
                        is SeekBarProgressChangeEvent -> {
                            Timber.d("Change Events - Daylight Progress from user = ${seekBarChangeEvent.fromUser()}")
                            Timber.d("Change Events - Daylight Progress = ${seekBarChangeEvent.progress()}")

                            if (seekBarChangeEvent.fromUser()) {
                                setDaylight(seekBarChangeEvent.progress())
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Daylight Start")
                        }
                        is SeekBarStopChangeEvent -> {
                            Timber.d("Change Events - Daylight Stop = ${seekBarChangeEvent.view().progress}")
                            Timber.d("Change Events - Daylight Stop")

                            setDaylight(seekBarChangeEvent.view().progress)

                        }
                    }
                }
        )
    }

    private fun getIntensitySliderThumbPos(): Int {
        val width = (sb_brightness.width
                - sb_brightness.paddingLeft
                - sb_brightness.paddingRight)
        return sb_brightness.paddingLeft + width * sb_brightness.progress / sb_brightness.max + sb_brightness.thumb.intrinsicWidth / 2
    }


    private fun setDaylight(value: Int) {
        cb_onOffLight.isChecked = true

        if (viewModel != null) {

            sceneDevice?.red = 0
            sceneDevice?.green = 0
            sceneDevice?.blue = 0
            sceneDevice?.daylight_control = value
            sceneDevice?.scene_value = convertHexStringToInt(SceneType.DEFAULT.value)
            sceneDevice?.status = getLampOnOffStatus(true)

            updateUiBasedOnSceneDevice()

            viewModel!!.setDaylight(
                    sceneDevice!!.mac_id,
                    value
            )
        }
    }

    private fun setLightBrightness(value: Int) {
        cb_onOffLight.isChecked = true

        if (viewModel != null) {

            sceneDevice?.brightness = value
            sceneDevice?.status = getLampOnOffStatus(true)

            viewModel!!.setLampBrightness(
                    sceneDevice!!.mac_id,
                    value
            )
        }
    }

    private fun turnOffLight() {
        if (viewModel != null) {

            sceneDevice?.status = Utilities.getLampOnOffStatus(false)

            viewModel!!.turnOffLight(
                    sceneDevice!!.mac_id
            )
        }


    }

    private fun turnOnLight() {
        if (viewModel != null) {

            sceneDevice?.status = Utilities.getLampOnOffStatus(true)


            viewModel!!.turnOnLight(
                    sceneDevice!!.mac_id
            )
        }
    }

    private fun setScene(sceneType: SceneType) {
        when (sceneType) {
            SceneType.ENERGISE -> {
                ll_energise.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                setSceneToLamp(sceneType)
            }
            SceneType.CANDLE_LIGHT -> {
                ll_candleLight.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_energise.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                setSceneToLamp(sceneType)


            }
            SceneType.SUNSET -> {
                ll_sunset.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                setSceneToLamp(sceneType)

            }
            SceneType.COLOR_BLAST -> {
                ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_partyMode.background = null
                // TODO - Dynamic scene feature can be turned off

                if (sceneDevice != null) {
                    if (sceneDevice!!.scene_value.equals(convertHexStringToInt(SceneType.COLOR_BLAST.value))) {
                        // TODO - Color blast is already set so we need to turn off
                        ll_colorBlast.background = null
                        Timber.d("Color blast is already set, turning off color blast")

                        turnOffDynamicScene()


                    } else {
                        // TODO - Color blast is not set so we need to turn on
                        ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                        Timber.d("Color blast is not set, setting color blast scene")

                        setSceneToLamp(sceneType)

                    }
                } else {
                    // Should not occur
                    Timber.d("Scene device is null")
                }
            }
            SceneType.PARTY_MODE -> {
                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null

                if (sceneDevice != null) {
                    if (sceneDevice!!.scene_value.equals(convertHexStringToInt(SceneType.PARTY_MODE.value))) {
                        // Party mode is already set so we need to turn off
                        ll_partyMode.background = null
                        Timber.d("Party mode is already set, turning off Party mode")
                        turnOffDynamicScene()


                    } else {
                        // Party mode is not set so we need to turn on
                        ll_partyMode.setBackgroundResource(R.drawable.today_tomorrow_selected)
                        Timber.d("Party mode is not set, setting color Party mode")

                        setSceneToLamp(sceneType)

                    }
                } else {
                    // Should not occur
                    Timber.d("Scene Device value is null")
                }
            }
            SceneType.RAINBOW -> {
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                if (sceneDevice != null) {
                    if (sceneDevice!!.scene_value.equals(convertHexStringToInt(SceneType.RAINBOW.value))) {
                        // Rainbow is already set so we need to turn off
                        ll_rainbow.background = null
                        Timber.d("Rainbow is already set, turning off Rainbow")

                        turnOffDynamicScene()


                    } else {
                        // Rainbow is not set so we need to turn on
                        ll_rainbow.setBackgroundResource(R.drawable.today_tomorrow_selected)
                        Timber.d("Rainbow is not set, setting color Rainbow")

                        setSceneToLamp(sceneType)

                    }
                } else {
                    // Should not occur
                    Timber.d("Scene Device value is null")
                }
            }
        }
    }

    private fun showColorPalette() {
        if (mColorPickerDialog == null) {
            initColorPickerDialog()
        }
        lastSentColor = "000000"
        mColorPickerDialog!!.show()
    }

    private fun openPhotoGallery() {
        lastSentColor = "000000"
        ImagePickerDialogFragment.newInstance(PHOTO_GALLERY.absoluteValue).show(supportFragmentManager, "image_picker_fragment")
    }


    private var onColorPickedListener = object : WifiColorPickerDialog.OnColorPickedListener {
        override fun onForceColorPicked(color: Int, hexVal: String?) {
            Timber.d("Color picked from color palette - ${Utilities.convertColorToHexString(color)}")
            setLightColor(Utilities.convertColorToHexString(color), true)
        }

        override fun onColorPicked(color: Int, hexVal: String) {
            Timber.d("Color picked from color palette - ${Utilities.convertColorToHexString(color)}")
            setLightColor(Utilities.convertColorToHexString(color), false)

        }
    }


    override fun onColorSelected(color: String) {
        // From Photo gallery
        Timber.d("User selected from gallery - $color color")
        setLightColor(color, false)
    }

    override fun onForceColorSelected(color: String) {
        // From Photo gallery
        Timber.d("User selected from gallery - $color color")
        setLightColor(color, true)
    }

    private fun openCamera() {
        lastSentColor = "000000"
        ImagePickerDialogFragment.newInstance(PHOTO_GALLERY.absoluteValue).show(supportFragmentManager, "image_picker_fragment")
    }


    private fun setLightColor(color: String, isForced: Boolean) {
        // Sample input - 00FF00
        if (color.equals(lastSentColor).not()) {
            if (isForced) {
                lastSentColor = color
                if (viewModel != null) {
                    sceneDevice?.red = convertHexStringToInt(color.substring(0, 2))
                    sceneDevice?.green = convertHexStringToInt(color.substring(2, 4))
                    sceneDevice?.blue = convertHexStringToInt(color.substring(4))
                    sceneDevice?.daylight_control = 0
                    sceneDevice?.scene_value = convertHexStringToInt(SceneType.DEFAULT.value)
                    sceneDevice?.status = Utilities.getLampOnOffStatus(true)

                    viewModel!!.setLampColor(
                            sceneDevice!!.mac_id,
                            r_value = color.substring(0, 2),
                            g_value = color.substring(2, 4),
                            b_value = color.substring(4)
                    )
                }
            } else {
                var currentTime = System.currentTimeMillis()
                val delay = if (isLocallyAvailable) TCP_TRANSITION_DELAY.toLong() else MQTT_TRANSITION_DELAY.toLong()
                if ((currentTime - lastColorSentTime) >= delay) {
                    if (viewModel != null) {

                        sceneDevice?.red = convertHexStringToInt(color.substring(0, 2))
                        sceneDevice?.green = convertHexStringToInt(color.substring(2, 4))
                        sceneDevice?.blue = convertHexStringToInt(color.substring(4))
                        sceneDevice?.daylight_control = 0
                        sceneDevice?.scene_value = convertHexStringToInt(SceneType.DEFAULT.value)
                        sceneDevice?.status = Utilities.getLampOnOffStatus(true)

                        viewModel!!.setLampColor(
                                sceneDevice!!.mac_id,
                                r_value = color.substring(0, 2),
                                g_value = color.substring(2, 4),
                                b_value = color.substring(4)
                        )
                    }
                }
            }
        }
    }

    private fun turnOffDynamicScene() {
        sceneDevice?.red = 0
        sceneDevice?.green = 0
        sceneDevice?.blue = 0
        sceneDevice?.daylight_control = 100
        sceneDevice?.scene_value = convertHexStringToInt(SceneType.DEFAULT.value)
        sceneDevice?.brightness = sceneDevice!!.brightness
        sceneDevice?.status = Utilities.getLampOnOffStatus(true)

        if (viewModel != null) {
            viewModel!!.turnOffDynamicScene(sceneDevice!!.mac_id)
        }

    }

    private fun setSceneToLamp(sceneType: SceneType) {

        sceneDevice?.red = 0
        sceneDevice?.green = 0
        sceneDevice?.blue = 0
        sceneDevice?.daylight_control = 0
        sceneDevice?.scene_value = convertHexStringToInt(sceneType.value)
        sceneDevice?.brightness = sceneDevice!!.brightness
        sceneDevice?.status = Utilities.getLampOnOffStatus(true)


        if (viewModel != null) {
            viewModel!!.setScene(
                    sceneDevice!!.mac_id,
                    sceneType
            )
        }
    }

    override fun onBackPressed() {
        if (sceneDevice != null) {
            setResult(Activity.RESULT_OK, Intent().putExtra(BUNDLE_CUSTOM_SCENE_DEVICE, sceneDevice))
        } else {
            setResult(Activity.RESULT_CANCELED)
        }
        finish()
    }
}
