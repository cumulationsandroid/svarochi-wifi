package com.svarochi.wifi.ui.lightcontrol.repository

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_B_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_G_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_R_VALUE
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Constants.Companion.NO_TCP_CONNECTION
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.LocalDataSource
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.api.response.EditLightNameResponse
import com.svarochi.wifi.model.common.LightStatus
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.Command
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.service.communication.CommunicationService
import com.svarochi.wifi.ui.lightcontrol.LightControlImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class LightControlRepository(var application: SvarochiApplication) : BaseRepository(),
        BaseRepositoryImpl, LightControlImpl {

    private var responseObservable = PublishSubject.create<Event>()
    private var pendingLightStatusMap = HashMap<String, LightStatus>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var communicationService: CommunicationService = application.wifiCommunicationService
    private var localDataSource: LocalDataSource = application.wifiLocalDatasource
    private var sharedPreference: SharedPreference? = null
    private var networkId: Long = -1
    private var projectId: Long = -1
    private var userId: String = ""

    init {
        // Observe the responseObservable in service
        initCommunicationResponseObserver()
        fetchIdsFromSharedPreference()
    }

    private fun fetchIdsFromSharedPreference() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        projectId = sharedPreference!!.getProjectId()
        networkId = sharedPreference!!.getNetworkId()
        userId = sharedPreference!!.getUserId()
    }

    override fun getCommunicationResponseObserver() = responseObservable

    override fun initCommunicationResponseObserver() {
        communicationService.getResponseObservable()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Timber.d("Listening to responseObservable of service...")
                    disposables.add(it!!)
                }
                .subscribe(object : DisposableObserver<Event>() {
                    override fun onComplete() {
                    }

                    override fun onNext(event: Event) {
                        responseObservable.onNext(event)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                        //responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.ERROR, null, null))
                    }
                })
    }

    override fun setDaylight(
            macId: String,
            daylightValue: Int
    ) {
        var command = Command(EventType.SET_LIGHT_DAYLIGHT, macId, daylightValue)
        communicationService.sendCommand(command)
    }

    override fun setScene(macId: String, sceneType: SceneType) {
        var command = Command(EventType.SET_LIGHT_PRESET_SCENE, macId, sceneType)
        communicationService.sendCommand(command)
    }


    fun getLightStatus(macId: String) {
        var command = Command(EventType.GET_LIGHT_STATUS, macId, null)
        communicationService.sendCommand(command)
    }

    override fun setLampColor(
            macId: String,
            r_value: String,
            g_value: String,
            b_value: String
    ) {
        var rgbPayload = HashMap<String, String>()
        rgbPayload[BUNDLE_R_VALUE] = r_value
        rgbPayload[BUNDLE_G_VALUE] = g_value
        rgbPayload[BUNDLE_B_VALUE] = b_value

        var command = Command(EventType.SET_LIGHT_RGB, macId, rgbPayload)
        communicationService.sendCommand(command)
    }

    override fun turnOnLight(macId: String) {
        var command = Command(EventType.TURN_ON_LIGHT, macId, null)
        communicationService.sendCommand(command)
    }

    override fun turnOffLight(macId: String) {
        var command = Command(EventType.TURN_OFF_LIGHT, macId, null)
        communicationService.sendCommand(command)
    }

    override fun setLampBrightness(
            macId: String,
            brightnessValue: Int
    ) {
        var command = Command(EventType.SET_LIGHT_BRIGHTNESS, macId, brightnessValue)
        communicationService.sendCommand(command)
    }

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    fun getLightLiveData(macId: String): LiveData<Light> {
        return localDataSource.getLightLiveData(macId)
    }


    override fun editLightName(macId: String, newName: String) {
        if (GeneralUtils.isInternetAvailable(application)) {
            if (isLightNameAlreadyExisting(newName).not()) {
                disposables.add(
                        ApiRequestParser.editLightName(userId, projectId.toInt(), networkId.toInt(), macId, newName)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(object : DisposableObserver<Response<EditLightNameResponse>>() {
                                    override fun onComplete() {
                                        Timber.d("Completed edit light name api")
                                    }

                                    override fun onNext(response: Response<EditLightNameResponse>) {
                                        if (response.isSuccessful) {
                                            if (response.body() != null) {
                                                Timber.d("Edit Light Name API response body - $response")
                                                if (response.body()!!.status.contains("success", true)) {
                                                    Timber.d("Successfully edited light name in server")
                                                    updateLightNameInDb(macId, newName)
                                                } else {
                                                    Timber.d("Something went wrong while editing light name in server")

                                                    var errorMessage = Utilities.getErrorMessage(response)
                                                    if (errorMessage != null) {
                                                        Timber.e(errorMessage)
                                                    } else {
                                                        Timber.e("No error message received")
                                                    }

                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.EDIT_LIGHT_NAME,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }
                                            } else {
                                                Timber.d("Something went wrong while editing light name in server")
                                                Timber.d("No response got from server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_LIGHT_NAME,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("Something went wrong while editing light name in server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_LIGHT_NAME,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    }

                                    override fun onError(error: Throwable) {
                                        Timber.d("Something went wrong while editing light name in server")
                                        error.printStackTrace()
                                        Timber.e(error)

                                        responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.FAILURE, null, null))
                                    }

                                })
                )
            } else {
                Timber.d("Light by name($newName) already exists")
                responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.FAILURE, ALREADY_EXISTS, null))
            }
        } else {
            Timber.d("No Internet available")
            responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }

    }

    private fun isLightNameAlreadyExisting(newName: String): Boolean {
        var lightsList = localDataSource.getLights(networkId)
        for (light in lightsList) {
            if (light.name.equals(newName)) {
                return true
            }
        }
        return false
    }

    private fun updateLightNameInDb(macId: String, newName: String) {
        localDataSource.updateLightName(macId, newName)
        Timber.d("Successfully edited light name in db")
        responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.SUCCESS, null, null))
    }

    fun turnOffDynamicScene(macId: String) {
        /**
         * To turn off a dynamic we have to turn on the lamp with previous saved RGBWC values
         * */
        setLightSceneToDefault(macId)
        turnOnLight(macId)
    }


    private fun sendResetCommandToDevice(macId: String) {
        var command = Command(EventType.DELETE_LAMP, macId, null)
        communicationService.sendCommand(command)
    }

    private fun setLightSceneToDefault(macId: String) {
        localDataSource.setLightSceneToDefault(macId)
    }

    override fun deleteLamp(macId: String) {
        /**
         * An active internet is needed to update server
         *
         * Delete lamp -
         * 1. Send RESET command to device via TCP
         * 2. Call delete API
         * 3. On Success of the API remove the lamp from local database
         *
         * */
        if (application.wifiCommunicationService.isDeviceLocallyAvailable(macId).not()) {
            responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, NO_TCP_CONNECTION, null))
        } else if (GeneralUtils.isInternetAvailable(application).not()) {
            responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        } else {
            sendResetCommandToDevice(macId)
        }
    }

    fun deleteUnSyncedLamp(macId: String) {
        /**
         *
         * No need of active internet as the lamp is not present in server
         * No need to Call delete API as device is not synced
         *
         * Delete lamp -
         * 1. Send RESET command to device via TCP
         * 2. On Success of the API remove the lamp from local database
         *
         * */
        if (application.wifiCommunicationService.isDeviceLocallyAvailable(macId).not()) {
            responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, NO_TCP_CONNECTION, null))
        } else {
            sendResetCommandToDevice(macId)
        }
    }

    fun getFirmwareVersion(macId: String){
        var command = Command(EventType.GET_FIRMWARE_VERSION, macId, null)
        communicationService.sendCommand(command)
    }
}