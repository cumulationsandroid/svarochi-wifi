package com.svarochi.wifi.ui.addnewnetwork.view

import android.net.wifi.ScanResult
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import bizbrolly.svarochiapp.R

class WifiNetworksAdapter(var context: NetworkDetailsActivity, var wifiList: List<ScanResult>) : RecyclerView.Adapter<WifiNetworksAdapter.WifiNetworkViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WifiNetworkViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_wifi_network, parent, false)
        return WifiNetworkViewHolder(view)
    }

    override fun getItemCount(): Int {
        return wifiList.size
    }

    override fun onBindViewHolder(holder: WifiNetworkViewHolder, position: Int) {
        holder.bind(position)
    }


    inner class WifiNetworkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var wifiNetworkName = itemView.findViewById<TextView>(R.id.tv_wifiNetworkName)
        var wifiNetworkLayout = itemView.findViewById<LinearLayout>(R.id.ll_wifiNetwork)

        fun bind(position: Int) {
            if (wifiList[position].SSID != null) {
                wifiNetworkName.text = wifiList[position].SSID
            } else {
                wifiNetworkName.text = context.getString(R.string._dash)
            }
        }

        init {
            wifiNetworkLayout.setOnClickListener {
                var position = adapterPosition
                context.selectedWifiNetwork(wifiList[position])
            }
        }

    }
}