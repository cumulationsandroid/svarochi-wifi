package com.svarochi.wifi.ui.lightcontrol.view.fragment


import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import bizbrolly.svarochiapp.R
import com.ablanco.imageprovider.ImageProvider
import com.ablanco.imageprovider.ImageSource
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_IMAGE_PICK_TYPE
import com.svarochi.wifi.common.Constants.Companion.PHOTO_GALLERY
import com.svarochi.wifi.common.Utilities.Companion.convertColorToHexString
import kotlinx.android.synthetic.main.fragment_image_picker_dialog.*
import timber.log.Timber


class ImagePickerDialogFragment : androidx.fragment.app.DialogFragment(), View.OnTouchListener,
        View.OnClickListener {

    private lateinit var listener: ColorPickerInterface
    private var imageHeight: Int = 0
    private var imageWidth: Int = 0

    override fun onClick(v: View?) {
        when (v!!.id) {
            bt_cancelButton.id -> {
                try {
                    dismiss()
                } catch (e: Exception) {

                }
            }
        }
    }

    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        if (event != null) {
            placePointer(event.x, event.y)
        }

        if (view!!.id == iv_galleryImage.id && event != null) {
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    val touchedRGB = getColorAtPosition(event.x, event.y, view)
                    var colorSelected = convertColorToHexString(touchedRGB)
                    Timber.d("Touched color: $colorSelected")
                    listener.onForceColorSelected(colorSelected)
                }

                MotionEvent.ACTION_MOVE -> {
                    val touchedRGB = getColorAtPosition(event.x, event.y, view)
                    var colorSelected = convertColorToHexString(touchedRGB)
                    Timber.d("Touched color: $colorSelected")
                    listener.onColorSelected(colorSelected)
                }
            }
        }
        return true
    }

    private fun getColorAtPosition(xPosition: Float, yPosition: Float, view: View): Int {
        var eventX = xPosition
        var eventY = yPosition
        val eventXY = floatArrayOf(eventX, eventY)

        val invertMatrix = Matrix()
        (view as ImageView).imageMatrix.invert(invertMatrix)

        invertMatrix.mapPoints(eventXY)
        var x = Integer.valueOf(eventXY[0].toInt())
        var y = Integer.valueOf(eventXY[1].toInt())

        val imgDrawable = view.drawable
        val bitmap = (imgDrawable as BitmapDrawable).bitmap


        //Limit x, y range within bitmap
        if (x < 0) {
            x = 0
        } else if (x > bitmap.width - 1) {
            x = bitmap.width - 1
        }

        if (y < 0) {
            y = 0
        } else if (y > bitmap.height - 1) {
            y = bitmap.height - 1
        }

        return bitmap.getPixel(x, y)
    }

    private fun placePointer(x: Float, y: Float) {
        iv_thumb.visibility = View.VISIBLE
        imageHeight = iv_galleryImage.measuredHeight
        imageWidth = iv_galleryImage.measuredWidth

        var xpos = x
        var ypos = y
        if (imageWidth > 0 && imageHeight > 0) {
            if (x < 0)
                xpos = 0f
            else if (x > imageWidth)
                xpos = imageWidth.toFloat()

            if (y < 0)
                ypos = 0f
            else if (y > imageHeight)
                ypos = imageWidth.toFloat()

            iv_thumb.x = xpos //- Utilities.dipToPixels(activity!!, 6f)
            iv_thumb.y = ypos //- Utilities.dipToPixels(activity!!, 6f)
        }
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_picker_dialog, container, false)
    }

    private var imagePickType: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            imagePickType = it.getInt(BUNDLE_IMAGE_PICK_TYPE)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bt_cancelButton.setOnClickListener(this)

        listener = (activity as ColorPickerInterface)

        if (imagePickType == PHOTO_GALLERY) {
            ImageProvider(activity!!).getImage(ImageSource.GALLERY, this::selectedImage)
        } else {
            ImageProvider(activity!!).getImage(ImageSource.CAMERA, this::selectedImage)

        }
        iv_galleryImage.setOnTouchListener(this)
    }


    interface ColorPickerInterface {
        fun onColorSelected(color: String)
        fun onForceColorSelected(color: String)
    }

    fun selectedImage(image: Bitmap?) {
        if (image != null) {
            iv_galleryImage.setImageBitmap(image)
            ll_imagePickerLayout.visibility = View.VISIBLE
        } else {
            dismissAllowingStateLoss()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(type: Int) = ImagePickerDialogFragment().apply {
            arguments = Bundle().apply {
                putInt(BUNDLE_IMAGE_PICK_TYPE, type)
            }
        }
    }

}