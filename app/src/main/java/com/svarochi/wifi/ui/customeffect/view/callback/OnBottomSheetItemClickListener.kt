package com.svarochi.wifi.ui.customeffect.view.callback

interface OnBottomSheetItemClickListener {
    fun onBottomSheetItemClick(item: Int)
}