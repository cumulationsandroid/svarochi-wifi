package com.svarochi.wifi.ui.setwifidetails.view

import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_AUTHENTICATION_MODE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_ENCRYPTION_TYPE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PASSWORD
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SSID
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.setwifidetails.viewmodel.SetWifiDetailsViewModel
import kotlinx.android.synthetic.main.activity_set_wifi_details.*
import timber.log.Timber
import java.util.*


class SetWifiDetailsActivity : WifiBaseActivity(), View.OnClickListener {
    private lateinit var wifiManager: WifiManager
    private lateinit var connectivityManager: ConnectivityManager
    override fun onClick(v: View?) {
        when (v!!.id) {
            iv_back.id -> {
                finish()
            }

            bt_saveWifiDetails.id -> {
                checkEnteredDetails()
            }
        }
    }


    private lateinit var viewModel: SetWifiDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_wifi_details)

        initViewModels()
        initSpinners()
        initViews()
        initClickListeners()
    }

    private fun checkForWifiConnections() {

    }


    private fun initClickListeners() {
        iv_back.setOnClickListener(this)
        bt_saveWifiDetails.setOnClickListener(this)
    }

    private fun initViews() {
        wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        connectivityManager =
                applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        getWifiDetails()
    }

    private fun getWifiDetails() {
        viewModel.getWifiDetails()
    }

    private fun initViewModels() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(SetWifiDetailsViewModel::class.java)
        viewModel.getResponse().observe(this, Observer { t -> processResponse(t) })
    }

    private fun processResponse(event: Event?) {
        when (event!!.type) {
            EventType.GET_WIFI_DETAILS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Fetching wifi details")
                    }
                    EventStatus.SUCCESS -> {
                        if (event.data == null) {
                            Timber.d("No previous wifi data found")
                            checkForWifiConnections()
                        } else {
                            Timber.d("Previous wifi details found")
                            updateUi(event.data as HashMap<String, String>)
                        }
                    }
                }
            }
            EventType.SET_WIFI_DETAILS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Writing wifi credentials into SharedPreference")
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully written wifi details in SharedPreferences")
                        setResult(AppCompatActivity.RESULT_OK)
                        finish()
                    }
                }
            }
        }
    }

    private fun checkEnteredDetails() {
        if (et_ssid.text.toString().trim().isNotEmpty() ||
                et_password.text.toString().trim().isNotEmpty() ||
                sp_authenticationMode.selectedItemPosition == 0 ||
                sp_encryptionType.selectedItemPosition == 0
        ) {
            if (et_password.text.toString().length >= 8)
                saveWifiDetails()
            else
                showToast(getString(R.string.toast_error_password_should_be_minimum_8_characters))
        } else {
            showToast(getString(R.string.toast_error_enter_all_details))
        }
    }

    private fun saveWifiDetails() {
        viewModel.setWifiDetails(
                et_ssid.text.toString(),
                et_password.text.toString(),
                getAuthenticationType(),
                getEncryptionType()
        )
    }

    private fun getEncryptionType(): String {
        when (sp_encryptionType.selectedItem) {
            "TKIP" -> {
                return Constants.TKIP
            }
            "AES" -> {
                return Constants.AES
            }
            "No Encryption" -> {
                return Constants.NO_ENCRYPTION
            }
        }
        return Constants.NO_ENCRYPTION
    }

    private fun getAuthenticationType(): String {
        when (sp_authenticationMode.selectedItem) {
            "WPA2-PSK-SHA256" -> {
                return Constants.WPA2_PSK_SHA256
            }
            "WPA2-CCKM" -> {
                return Constants.WPA2_CCKM
            }
            "WPA-CCKM" -> {
                return Constants.WPA_CCKM
            }
            "WPA2-PSK" -> {
                return Constants.WPA2_PSK
            }
            "WPA-PSK" -> {
                return Constants.WPA_PSK
            }
            "WPA2" -> {
                return Constants.WPA2
            }
            "WPA" -> {
                return Constants.WPA
            }
            "No Authentication" -> {
                return Constants.NO_AUTHENTICATION
            }
        }

        return Constants.NO_AUTHENTICATION
    }


    private fun initSpinners() {
        val authentionMode = resources.getStringArray(R.array.array_authentication_modes)
        val authenticationList = ArrayList(Arrays.asList(*authentionMode))
        // Setting up the spinner
        val encryptionTypes = resources.getStringArray(R.array.array_encryption_modes)
        val encryptionList = ArrayList(Arrays.asList(*encryptionTypes))
        val authenticationAdapter = object : ArrayAdapter<String>(
                this,
                R.layout.template_spinner_item, authenticationList
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                    position: Int, convertView: View?,
                    parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(Color.BLACK)
                }
                return view
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                view.setPadding(6, view.paddingTop, view.paddingRight, view.paddingBottom)
                return view
            }
        }
        val encryptionAdapter = object : ArrayAdapter<String>(
                this,
                R.layout.template_spinner_item, encryptionList
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                    position: Int, convertView: View?,
                    parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(Color.BLACK)
                }
                return view
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                view.setPadding(6, view.paddingTop, view.paddingRight, view.paddingBottom)
                return view
            }

        }
        sp_authenticationMode.adapter = authenticationAdapter
        sp_encryptionType.adapter = encryptionAdapter

        var onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                (parent.getChildAt(0) as TextView).setTextColor(Color.WHITE)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        sp_authenticationMode.setSelection(4)
        sp_encryptionType.setSelection(2)
        sp_authenticationMode.onItemSelectedListener = onItemSelectedListener
        sp_encryptionType.onItemSelectedListener = onItemSelectedListener
    }

    private fun updateUi(wifiDetails: HashMap<String, String>) {
        et_ssid.text = Editable.Factory.getInstance().newEditable(wifiDetails[BUNDLE_SSID])
        et_password.text = Editable.Factory.getInstance().newEditable(wifiDetails[BUNDLE_PASSWORD])

        sp_authenticationMode.setSelection(
                (sp_authenticationMode.adapter as ArrayAdapter<String>).getPosition(
                        getAuthenticationModePosition(wifiDetails[BUNDLE_AUTHENTICATION_MODE]!!)
                )
        )

        sp_encryptionType.setSelection(
                (sp_encryptionType.adapter as ArrayAdapter<String>).getPosition(
                        getEncryptionTypePosition(wifiDetails[BUNDLE_ENCRYPTION_TYPE]!!)
                )
        )
    }

    private fun getEncryptionTypePosition(encryptionType: String): String {
        when (encryptionType) {
            Constants.TKIP -> {
                return "TKIP"
            }
            Constants.AES -> {
                return "AES"
            }
            Constants.NO_ENCRYPTION -> {
                return "No Encryption"
            }
        }
        return "No Encryption"
    }

    private fun getAuthenticationModePosition(authenticationMode: String): String {
        when (authenticationMode) {
            Constants.WPA2_PSK_SHA256 -> {
                return "WPA2-PSK-SHA256"
            }
            Constants.WPA2_CCKM -> {
                return "WPA2-CCKM"
            }
            Constants.WPA_CCKM -> {
                return "WPA-CCKM"
            }
            Constants.WPA2_PSK -> {
                return "WPA2-PSK"
            }
            Constants.WPA_PSK -> {
                return "WPA-PSK"
            }
            Constants.WPA2 -> {
                return "WPA2"
            }
            Constants.WPA -> {
                return "WPA"
            }
            Constants.NO_AUTHENTICATION -> {
                return "No Authentication"
            }
        }

        return "No Authentication"
    }

}
