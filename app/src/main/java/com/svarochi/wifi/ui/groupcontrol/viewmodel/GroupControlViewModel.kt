package com.svarochi.wifi.ui.groupcontrol.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.ui.groupcontrol.repository.GroupControlRepository
import io.reactivex.disposables.CompositeDisposable
import com.svarochi.wifi.ui.groupcontrol.GroupControlImpl

class GroupControlViewModel(internal var repository: GroupControlRepository) : BaseViewModel(), GroupControlImpl {
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver(disposables, repository, response)
    }

    override fun onCleared() {
        disposables.dispose()
        disposables = CompositeDisposable()
        repository.dispose()
    }

    override fun turnOnGroup(groupId: Long) {
        response.value = Event(EventType.TURN_ON_GROUP, EventStatus.LOADING, null, null)
        repository.turnOnGroup(groupId)
    }

    override fun turnOffGroup(groupId: Long) {
        response.value = Event(EventType.TURN_OFF_GROUP, EventStatus.LOADING, null, null)
        repository.turnOffGroup(groupId)
    }

    override fun setGroupScene(groupId: Long, sceneType: SceneType) {
        response.value = Event(EventType.SET_GROUP_SCENE, EventStatus.LOADING, null, null)
        repository.setGroupScene(groupId, sceneType)
    }

    override fun setGroupDaylight(
            groupId: Long,
            daylightValue: Int
    ) {
        response.value = Event(EventType.SET_GROUP_DAYLIGHT, EventStatus.LOADING, null, null)
        repository.setGroupDaylight(groupId, daylightValue)
    }

    override fun setGroupBrightness(
            groupId: Long,
            brightnessValue: Int
    ) {
        response.value = Event(EventType.SET_GROUP_BRIGHTNESS, EventStatus.LOADING, null, null)
        repository.setGroupBrightness(groupId, brightnessValue)
    }

    override fun setGroupColor(
            groupId: Long,
            r_value: String,
            g_value: String,
            b_value: String
    ) {
        response.value = Event(EventType.SET_GROUP_COLOR, EventStatus.LOADING, null, null)
        repository.setGroupColor(groupId, r_value, g_value, b_value)
    }

    fun getResponse() = response

    fun getLightsOfGroup(groupId: Long, networkId: Long) {
        repository.getLightsOfGroup(groupId, networkId)
    }

    override fun addLightToGroup(macId: String, groupId: Long) {
        response.value = Event(EventType.ADD_LIGHT_TO_GROUP, EventStatus.LOADING, null, null)
        repository.addLightToGroup(macId, groupId)
    }

    override fun removeLightFromGroup(macId: String, groupId: Long) {
        response.value = Event(EventType.REMOVE_LIGHT_FROM_GROUP, EventStatus.LOADING, null, null)
        repository.removeLightFromGroup(macId, groupId)
    }

    fun getGroupLiveData(groupId: Long): LiveData<Group> {
        return repository.getGroupLiveData(groupId)
    }

    override fun editGroupName(groupId: Long, name: String) {
        response.value = Event(EventType.EDIT_GROUP_NAME, EventStatus.LOADING, null, null)
        repository.editGroupName(groupId, name)
    }

    fun turnOffDynamicScene(groupId: Long) {
        response.value = Event(EventType.TURN_OFF_DYNAMIC_SCENE, EventStatus.LOADING, null, null)
        repository.turnOffDynamicScene(groupId)
    }
}