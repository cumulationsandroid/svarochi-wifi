package com.svarochi.wifi.ui.projects.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.projects.repository.ProjectsRepository
import io.reactivex.disposables.CompositeDisposable
import com.svarochi.wifi.ui.projects.ProjectsImpl


class ProjectsViewModel(var projectsRepository: ProjectsRepository) : BaseViewModel(), ProjectsImpl {

    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        // Observe project list of roomRepository
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, projectsRepository, response)
    }

    override fun onCleared() {
        disposables.dispose()
        projectsRepository.dispose()
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    override fun getProjects() {
        response.value = Event(EventType.GET_PROJECTS, EventStatus.LOADING, null, null)
        projectsRepository.getProjects()
    }

    override fun addNewProject(name: String) {
        response.postValue(Event(EventType.ADD_NEW_PROJECT, EventStatus.LOADING, null, null))
        projectsRepository.addNewProject(name)
    }

    override fun editProjectName(projectId: Long, name: String) {
        response.value = Event(EventType.EDIT_PROJECT_NAME, EventStatus.LOADING, null, null)
        projectsRepository.editProjectName(projectId, name)
    }

    override fun deleteProject(projectId: Long) {
        response.value = Event(EventType.DELETE_PROJECT, EventStatus.LOADING, null, null)
        projectsRepository.deleteProject(projectId)
    }

    fun getProjectsLiveData(): LiveData<List<Project>> {
        return projectsRepository.getProjectsLiveData()
    }

    fun switchedProfile() {
        projectsRepository.switchedProfile()
    }

    fun stopUdpBroadcasts(){
        projectsRepository.stopUdpBroadcasts()
    }

    fun getNetworksCount(projectId: Long): Int {
        return projectsRepository.getNetworksCount(projectId)
    }
}