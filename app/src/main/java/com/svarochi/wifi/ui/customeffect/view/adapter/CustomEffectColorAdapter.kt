package com.svarochi.wifi.ui.customeffect.view.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.ui.customeffect.view.activity.CustomEffectActivity

class CustomEffectColorAdapter(var context: CustomEffectActivity, var colorList: ArrayList<Int>) :
        androidx.recyclerview.widget.RecyclerView.Adapter<CustomEffectColorAdapter.CustomEffectColorViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): CustomEffectColorViewHolder {
        var mainView = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_wifi_custom_effect_color, viewGroup, false)

        return CustomEffectColorViewHolder(mainView)
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    override fun onBindViewHolder(viewHolder: CustomEffectColorViewHolder, position: Int) {
        viewHolder.setColor(colorList.get(position))
    }

    inner class CustomEffectColorViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var colorImage: AppCompatImageView = itemView.findViewById(R.id.iv_color)

        fun setColor(color: Int) {
            colorImage.setColorFilter(Color.argb(255, Color.red(color), Color.green(color), Color.blue(color)))
        }

        init {
            colorImage.setOnClickListener {
                if (colorImage.background == null) {
                    colorImage.background = context.resources.getDrawable(R.drawable.custom_effect_color_selected, null)
                } else {
                    colorImage.background = null
                }
            }
        }
    }

}