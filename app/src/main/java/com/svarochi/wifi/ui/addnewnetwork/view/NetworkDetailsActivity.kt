package com.svarochi.wifi.ui.addnewnetwork.view

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_EDIT_NETWORK
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NETWORK
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NETWORK_NAMES
import com.svarochi.wifi.common.Constants.Companion.CONTAINS_DEVICES
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.addnewnetwork.viewmodel.NetworkDetailsViewModel
import kotlinx.android.synthetic.main.activity_network_details.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class NetworkDetailsActivity : WifiBaseActivity(), View.OnClickListener, ServiceConnectionCallback {
    private var isEditingNetwork = false
    private var network: Network? = null
    private var networkNames = ArrayList<String>()
    private var TAG = "NetworkDetailsActivity View --->"
    private var viewModel: NetworkDetailsViewModel? = null
    private var locationManager: LocationManager? = null
    private var wifiManager: WifiManager? = null
    private var dialog: Dialog? = null
    private var isReceiverRegistered: Boolean = false
    private var shouldScanWifi: Boolean = false
    private var GPS_PERMISSION = 123

    override fun serviceConnected() {
        init()
    }

    private fun init() {
        initViewModel()
        initOnClickListeners()
        initManagers()
        initViews()
    }

    private fun initViews() {
        dialog = Dialog(this@NetworkDetailsActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setCanceledOnTouchOutside(false)
    }

    private fun initManagers() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(NetworkDetailsViewModel::class.java)
        viewModel!!.getResponse().observe(this, Observer { t -> processResponse(t) })
    }

    private var wifiReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            var resultList = wifiManager!!.scanResults
            Timber.d("$TAG Got Wifi scan results")
            if (resultList != null) {
                showAvailableWifiNetworksInDialog(resultList)
            } else {
                Timber.d("$TAG No nearby wifi networks available")
            }
        }
    }

    private fun showAvailableWifiNetworksInDialog(resultList: List<ScanResult>) {
        if (dialog != null &&
                dialog!!.isShowing) {

            var loadingLayout = dialog!!.findViewById<LinearLayout>(R.id.ll_loadingWifiNetworks)
            var connectionsLayout = dialog!!.findViewById<LinearLayout>(R.id.ll_wifiConnections)
            var wifiNetworksList = dialog!!.findViewById<RecyclerView>(R.id.rv_wifiNetworks)

            wifiNetworksList.layoutManager = LinearLayoutManager(this)
            wifiNetworksList.adapter = WifiNetworksAdapter(this, resultList)

            loadingLayout.visibility = View.GONE
            connectionsLayout.visibility = View.VISIBLE

        }
    }

    private fun processResponse(event: Event?) {
        if (event != null) {
            when (event.type) {
                EventType.ADD_NEW_NETWORK -> {
                    when (event.status) {
                        EventStatus.LOADING -> {
                            Timber.d("$TAG Adding new network")
                            showLoadingPopup(true, getString(R.string.dialog_adding_new_network))
                        }
                        EventStatus.SUCCESS -> {
                            Timber.d("$TAG Successfully added new network")
                            showToast(getString(R.string.toast_successfully_added_new_network))
                            showLoadingPopup(false, null)
                            setResult(Activity.RESULT_OK)
                            finish()
                        }
                        EventStatus.FAILURE -> {
                            showLoadingPopup(false, null)
                            if (event.data != null) {
                                if (event.data is Int) {
                                    if (event.data!! as Int == NO_INTERNET_AVAILABLE) {
                                        Timber.d("Failed to add new network - No Internet available")
                                        showToast(getString(R.string.no_internet))
                                    }
                                }
                            } else {
                                showToast(getString(R.string.toast_failed_to_add_new_network))
                                Timber.d("$TAG Failed to add new network")
                            }
                        }
                    }
                }
                EventType.EDIT_NETWORK -> {
                    when (event.status) {
                        EventStatus.LOADING -> {
                            Timber.d("$TAG Editing network(${network!!.id})")
                            showLoadingPopup(true, getString(R.string.dialog_editing_network))
                        }
                        EventStatus.SUCCESS -> {
                            Timber.d("$TAG Successfully edited network(${network!!.id})")
                            showToast(getString(R.string.toast_successfully_edited_network))
                            showLoadingPopup(false, null)
                            setResult(Activity.RESULT_OK)
                            finish()
                        }
                        EventStatus.FAILURE -> {
                            showLoadingPopup(false, null)
                            showToast(getString(R.string.toast_failed_to_edit_network))

                            if (event.data != null) {
                                if (event.data is Int) {
                                    if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                        Timber.d("No Internet available")
                                        showToast(getString(R.string.no_internet))
                                    }
                                }
                            } else {
                                Timber.d("$TAG Failed to edit network(${network!!.id})")
                            }
                        }
                    }
                }

                EventType.DELETE_NETWORK -> {
                    when (event.status) {
                        EventStatus.LOADING -> {
                            Timber.d("$TAG Deleting network(${network!!.id})")
                            showLoadingPopup(true, getString(R.string.popup_deleting_network))
                        }
                        EventStatus.SUCCESS -> {
                            Timber.d("$TAG Successfully deleted network(${network!!.id})")
                            showLoadingPopup(false, null)
                            setResult(Activity.RESULT_OK)
                            finish()
                        }
                        EventStatus.FAILURE -> {
                            showLoadingPopup(false, null)
                            if (event.data != null) {
                                if (event.data is Int) {
                                    if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                        Timber.d("No Internet available")
                                        showToast(getString(R.string.no_internet))
                                        return
                                    }
                                    if(event.data as Int == CONTAINS_DEVICES){
                                        Timber.d("Network contains devices")
                                        showToast(getString(R.string.toast_please_unpair_the_associated_lamps))
                                        return
                                    }
                                }
                            } else {
                                Timber.d("$TAG Failed to delete network(${network!!.id})")
                            }

                            showToast(getString(R.string.toast_failed_to_delete_network))
                        }
                    }
                }
            }
        } else {
            // Should not occur
            Timber.d("$TAG Event is null")
        }

    }

    private fun initOnClickListeners() {
        bt_saveNetwork.setOnClickListener(this)
        iv_back.setOnClickListener(this)
        tv_ssid.setOnClickListener(this)
        iv_deleteNetwork.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                bt_saveNetwork.id -> {
                    checkEnteredDetails()
                }

                iv_back.id -> {
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                }
                tv_ssid.id -> {
                    scanWifi()
                }
                iv_deleteNetwork.id -> {
                    showDeleteNetworkConfirmation()
                }
            }
        } else {
            // Should not occur
            Timber.d("$TAG Clicked view is null")
        }
    }

    private fun showDeleteNetworkConfirmation() {
        if (dialog == null) {
            dialog = Dialog(this)
        }
        dialog!!.setContentView(R.layout.dialog_delete_network_confirmation)
        dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        dialog!!.show()
        val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
        val okButton: Button = dialog!!.findViewById(R.id.bt_okButton)

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }

        okButton.setOnClickListener {
            dialog!!.dismiss()
            deleteNetwork()
        }
    }

    private fun deleteNetwork() {
        if (network != null && isEditingNetwork && viewModel != null) {
            if (viewModel!!.getLightsCount(network!!.id) == 0){
                viewModel!!.deleteNetwork(network!!.id, network!!.projectId)
            }else{
                showToast(getString(R.string.toast_please_unpair_the_associated_lamps))
            }
        } else {
            // Should not occur
            Timber.d("Network/ViewModel is null")
        }
    }

    private fun openAvailableWifiNetworksPopup() {
        if (dialog == null) {
            dialog = Dialog(this@NetworkDetailsActivity)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setCancelable(true)
            dialog!!.setCanceledOnTouchOutside(false)
        }

        dialog!!.setContentView(R.layout.dialog_wifi_connections)
        var loadingLayout = dialog!!.findViewById<LinearLayout>(R.id.ll_loadingWifiNetworks)
        var connectionsLayout = dialog!!.findViewById<LinearLayout>(R.id.ll_wifiConnections)
        var cancelButton = dialog!!.findViewById<Button>(R.id.bt_cancelButton)
        dialog!!.show()

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }

        loadingLayout.visibility = View.VISIBLE
        connectionsLayout.visibility = View.GONE

        if (wifiManager != null) {
            wifiManager!!.startScan()
        }
    }

    private fun scanWifi() {
        shouldScanWifi = true
        if (isReceiverRegistered.not()) {
            registerWifiReceiver()
        }
        if (wifiManager!!.isWifiEnabled.not()) {
            wifiManager!!.isWifiEnabled = true
        }

        if (locationManager != null) {
            if ((ContextCompat.checkSelfPermission(
                            this@NetworkDetailsActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED) &&
                    (ContextCompat.checkSelfPermission(
                            this@NetworkDetailsActivity,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED) &&
                    (locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER))) {

                openAvailableWifiNetworksPopup()


            } else {
                checkLocationPermission()
            }
        }
    }

    private fun registerWifiReceiver() {
        if (isReceiverRegistered.not() && wifiManager != null) {
            Timber.d("$TAG Registered wifi receiver")
            isReceiverRegistered = true
            registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        }
    }

    private fun checkEnteredDetails() {
        if (et_networkName.text.trim().isNotEmpty() &&
                et_password.text.toString().trim().isNotEmpty() &&
                sp_authenticationMode.selectedItemPosition != 0 &&
                sp_encryptionType.selectedItemPosition != 0
        ) {
            if (et_password.text.toString().length < 8) {
                showToast(getString(R.string.toast_error_password_should_be_minimum_8_characters))
                return
            }

            if (networkNames.contains(et_networkName.text.toString())) {
                showToast(getString(R.string.toast_network_by_that_name_already_exists))
                return
            }

            if (tv_ssid.text.toString().trim().isEmpty() ||
                    tv_ssid.text.toString().equals("<unknown ssid>") ||
                    tv_ssid.text.toString().equals(getString(R.string._dash))) {
                showToast(getString(R.string.toast_please_select_a_valid_ssid))
                return
            }

            saveNetwork()
        } else {
            showToast(getString(R.string.toast_error_enter_all_details))
        }
    }

    private fun saveNetwork() {
        if (isEditingNetwork) {
            if (viewModel != null) {
                if (network != null) {
                    if (GeneralUtils.isInternetAvailable(this)) {
                        viewModel!!.editNetwork(network!!.id,
                                et_networkName.text.toString(),
                                tv_ssid.text.toString(),
                                et_password.text.toString(),
                                getAuthenticationType(),
                                getEncryptionType())
                    } else {
                        showToast(getString(R.string.toast_no_internet_available))
                    }
                } else {
                    // Should not occur
                    Timber.d("$TAG Editing Network is null")
                }
            } else {
                // Should not occur
                Timber.d("$TAG ViewModel is null")
            }
        } else {
            if (viewModel != null) {
                if (GeneralUtils.isInternetAvailable(this)) {
                    viewModel!!.addNewNetwork(
                            et_networkName.text.toString(),
                            tv_ssid.text.toString(),
                            et_password.text.toString(),
                            getAuthenticationType(),
                            getEncryptionType()
                    )
                } else {
                    showToast(getString(R.string.toast_no_internet_available))
                }
            } else {
                // Should not occur
                Timber.d("$TAG ViewModel is null")
            }
        }
    }

    private fun getEncryptionType(): String {
        when (sp_encryptionType.selectedItem) {
            "TKIP" -> {
                return Constants.TKIP
            }
            "AES" -> {
                return Constants.AES
            }
            "No Encryption" -> {
                return Constants.NO_ENCRYPTION
            }
        }
        return Constants.NO_ENCRYPTION
    }

    private fun getAuthenticationType(): String {
        when (sp_authenticationMode.selectedItem) {
            "WPA2-PSK-SHA256" -> {
                return Constants.WPA2_PSK_SHA256
            }
            "WPA2-CCKM" -> {
                return Constants.WPA2_CCKM
            }
            "WPA-CCKM" -> {
                return Constants.WPA_CCKM
            }
            "WPA2-PSK" -> {
                return Constants.WPA2_PSK
            }
            "WPA-PSK" -> {
                return Constants.WPA_PSK
            }
            "WPA2" -> {
                return Constants.WPA2
            }
            "WPA" -> {
                return Constants.WPA
            }
            "No Authentication" -> {
                return Constants.NO_AUTHENTICATION
            }
        }

        return Constants.NO_AUTHENTICATION
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_network_details)
        requestToBindService(this)

        initSpinners()

        isEditingNetwork = intent.getBooleanExtra(BUNDLE_EDIT_NETWORK, false)
        networkNames = intent.getStringArrayListExtra(BUNDLE_NETWORK_NAMES)

        if (isEditingNetwork) {
            tv_headerText.text = getString(R.string.label_edit_network)

//            tv_ssid.isEnabled = false
            tv_ssid.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

            if (intent.extras != null) {
                network = intent.getSerializableExtra(BUNDLE_NETWORK) as Network

                networkNames.remove(network!!.name)

                Timber.d("$TAG Editing network(${network!!.id}")

                updateUi()

            } else {
                // Should not occur
                Timber.d("$TAG Bundle network is null")
            }

            iv_deleteNetwork.visibility = View.VISIBLE
        } else {
            tv_headerText.text = getString(R.string.label_add_new_network)

            Timber.d("$TAG Adding new network")

            displayCurrentSsid()

            iv_deleteNetwork.visibility = View.GONE
        }

    }

    private fun initSpinners() {
        val authentionMode = resources.getStringArray(R.array.array_authentication_modes)
        val authenticationList = ArrayList(Arrays.asList(*authentionMode))
        // Setting up the spinner
        val encryptionTypes = resources.getStringArray(R.array.array_encryption_modes)
        val encryptionList = ArrayList(Arrays.asList(*encryptionTypes))
        val authenticationAdapter = object : ArrayAdapter<String>(
                this,
                R.layout.template_spinner_item, authenticationList
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                    position: Int, convertView: View?,
                    parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(Color.BLACK)
                }
                return view
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                view.setPadding(6, view.paddingTop, view.paddingRight, view.paddingBottom)
                return view
            }
        }
        val encryptionAdapter = object : ArrayAdapter<String>(
                this,
                R.layout.template_spinner_item, encryptionList
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                    position: Int, convertView: View?,
                    parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(Color.BLACK)
                }
                return view
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                view.setPadding(6, view.paddingTop, view.paddingRight, view.paddingBottom)
                return view
            }

        }
        sp_authenticationMode.adapter = authenticationAdapter
        sp_encryptionType.adapter = encryptionAdapter

        var onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                (parent.getChildAt(0) as TextView).setTextColor(Color.WHITE)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        sp_authenticationMode.setSelection(4)
        sp_encryptionType.setSelection(2)
        sp_authenticationMode.onItemSelectedListener = onItemSelectedListener
        sp_encryptionType.onItemSelectedListener = onItemSelectedListener
    }

    private fun updateUi() {
        if (network != null) {
            et_networkName.text = Editable.Factory.getInstance().newEditable(network!!.name)
            tv_ssid.text = network!!.wifiSsid
            et_password.text = Editable.Factory.getInstance().newEditable(network!!.wifiPassword)


            sp_authenticationMode.setSelection(
                    (sp_authenticationMode.adapter as ArrayAdapter<String>).getPosition(
                            getAuthenticationModePosition(network!!.wifiAuthenticationMode)
                    )
            )

            sp_encryptionType.setSelection(
                    (sp_encryptionType.adapter as ArrayAdapter<String>).getPosition(
                            getEncryptionTypePosition(network!!.wifiEncryptionType)
                    )
            )
        }
    }

    private fun getEncryptionTypePosition(encryptionType: String): String {
        when (encryptionType) {
            Constants.TKIP -> {
                return "TKIP"
            }
            Constants.AES -> {
                return "AES"
            }
            Constants.NO_ENCRYPTION -> {
                return "No Encryption"
            }
        }
        return "No Encryption"
    }

    private fun getAuthenticationModePosition(authenticationMode: String): String {
        when (authenticationMode) {
            Constants.WPA2_PSK_SHA256 -> {
                return "WPA2-PSK-SHA256"
            }
            Constants.WPA2_CCKM -> {
                return "WPA2-CCKM"
            }
            Constants.WPA_CCKM -> {
                return "WPA-CCKM"
            }
            Constants.WPA2_PSK -> {
                return "WPA2-PSK"
            }
            Constants.WPA_PSK -> {
                return "WPA-PSK"
            }
            Constants.WPA2 -> {
                return "WPA2"
            }
            Constants.WPA -> {
                return "WPA"
            }
            Constants.NO_AUTHENTICATION -> {
                return "No Authentication"
            }
        }

        return "No Authentication"
    }

    private fun displayCurrentSsid() {
        shouldScanWifi = false
        if (locationManager != null) {
            if ((ContextCompat.checkSelfPermission(
                            this@NetworkDetailsActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED) &&
                    (ContextCompat.checkSelfPermission(
                            this@NetworkDetailsActivity,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED) &&
                    (locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER))) {

                if (wifiManager != null &&
                        wifiManager!!.connectionInfo != null &&
                        wifiManager!!.connectionInfo.ssid != null) {

                    tv_ssid.text = wifiManager!!.connectionInfo.ssid.replace("\"", "")
                } else {
                    Timber.d("$TAG Wifi connection information is null")
                    tv_ssid.text = Editable.Factory.getInstance().newEditable(getString(R.string._dash))
                }
            } else {
                Timber.d("$TAG Permissions not granted or GPS not enabled")
                tv_ssid.text = getString(R.string._dash)
                checkLocationPermission()
            }
        } else {
            Timber.d("$TAG Location manager is null")
            tv_ssid.text = getString(R.string._dash)
        }
    }

    private fun checkLocationPermission() {
        Timber.d("$TAG Checking location permission")
        if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            Timber.d("$TAG Location permission not granted")
            Timber.d("$TAG SDK Version - ${Build.VERSION.SDK_INT}")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Timber.d("$TAG Requesting location permission")
                requestPermissions(
                        arrayOf(
                                android.Manifest.permission.ACCESS_FINE_LOCATION,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION
                        ), Constants.ACCESS_LOCATION_PERMISSIONS_REQUEST
                )
            } else {
                // Should not occur
                Timber.d("$TAG Location permission not granted")
            }
        } else {
            checkGps()
        }
    }

    private fun checkGps() {
        Timber.d("$TAG Checking GPS provider")
        if (locationManager != null) {
            if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                displayLocationSettingsRequest(this)
            } else {
                if (shouldScanWifi) {
                    scanWifi()
                } else {
                    displayCurrentSsid()
                }
            }
        } else {
            Timber.d("$TAG Location manager is null")
        }
    }

    private fun displayLocationSettingsRequest(context: Context) {
        val googleApiClient = GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = (10000 / 2).toLong()

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)


        val task = LocationServices.getSettingsClient(context).checkLocationSettings(builder.build())
        task.addOnCompleteListener(object : OnCompleteListener<LocationSettingsResponse> {
            @Override
            override fun onComplete(task: Task<LocationSettingsResponse>) {
                try {
                    var response: LocationSettingsResponse = task.getResult(ApiException::class.java)!!
                    // All location settings are satisfied. The client can initialize location
                    // requests here.

                } catch (exception: ApiException) {
                    when (exception.statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                var resolvable: ResolvableApiException = exception as ResolvableApiException
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        this@NetworkDetailsActivity,
                                        GPS_PERMISSION)
                            } catch (exception: Exception) {
                            }
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                        }
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            GPS_PERMISSION -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        if (shouldScanWifi) {
                            scanWifi()
                        } else {
                            displayCurrentSsid()
                        }
                    }
                    Activity.RESULT_CANCELED -> {

                    }
                }
            }
        }
    }

    fun selectedWifiNetwork(scanResult: ScanResult) {
        if (scanResult.SSID != null) {
            tv_ssid.text = scanResult.SSID
        }

        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
    }


}
