package com.svarochi.wifi.ui.projects.view.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.ui.userSession.activities.ChangePasswordActivity
import com.svarochi.wifi.ui.userSession.activities.HelpActivity
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.ui.alexa.AlexaFragment
import com.svarochi.wifi.preference.Preferences
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PROJECT_ID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PROJECT_NAME
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.common.Constants.Companion.MAX_PROJECTS
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.networks.view.activity.NetworksActivity
import com.svarochi.wifi.ui.projects.view.adapter.ProjectsAdapter
import com.svarochi.wifi.ui.projects.viewmodel.ProjectsViewModel
import com.svarochi.wifi.ui.switchprofile.view.SwitchProfileActivity
import com.svarochi.wifi.ui.userinvitations.view.UserInvitationsActivity
import com.willblaschko.android.alexa.callbacks.ImplAsyncCallback
import kotlinx.android.synthetic.main.activity_wifi_projects.*
import timber.log.Timber

class ProjectsActivity : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener {
    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                iv_options.id -> {
                    showOptionsMenu(v)
                }
            }
        } else {
            // Should not occur
            Timber.d("View is null")
        }
    }

    override fun serviceConnected() {

        init()
    }

    private val SWITCH_PROFILE: Int = 123
    lateinit var viewModel: ProjectsViewModel
    private var dialog: Dialog? = null
    private var projectsCount = 0
    private var preference: SharedPreference? = null
    private var projectsList = ArrayList<Project>()
    private var isSelfProfile = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wifi_projects)
        requestToBindService(this)
    }

    private fun init() {
        if (preference == null) {
            preference = SharedPreference(this)
        }
        isSelfProfile = preference!!.getCurrentProfile().equals(CURRENT_PROFILE_SELF)
        preference!!.setProjectId(-1)
        preference!!.setNetworkId(-1)
        initViewModel()
        initDialogs()
        initViews()

    }

    private fun showOptionsMenu(view: View) {
        var popup = PopupMenu(this, view)
        popup.menuInflater.inflate(R.menu.options_projects, popup.menu)

        // Show invite user only its the self profile
        popup.menu.findItem(R.id.mi_inviteUser).isVisible = isSelfProfile

        popup.setOnMenuItemClickListener(optionMenuItemClickListener)
        popup.show()

    }

    private var optionMenuItemClickListener = object : PopupMenu.OnMenuItemClickListener {
        override fun onMenuItemClick(item: MenuItem?): Boolean {
            if (item != null) {
                when (item.itemId) {
                    R.id.mi_inviteUser -> {
                        showUserInvitationsScreen()
                    }
                    R.id.mi_switchProfile -> {
                        showSwitchProfileScreen()
                    }
                    R.id.mi_enableAlexa -> {
                        /***** Implementation for Alexa and FCM *****/
                        if (item.getTitle().toString().equals(getString(R.string.enable_alexa), ignoreCase = true)) {
                            Preferences.getInstance(this@ProjectsActivity).isAlexaEnabled = true
                            //Disable alexa
                            item.setTitle(getString(R.string.disable_alexa))
//                            mBinding.ivAlexa.setTag("Enable")
//                            mBinding.ivAlexa.setVisibility(View.GONE)
                        } else {
                            Preferences.getInstance(this@ProjectsActivity).isAlexaEnabled = false
                            //Enable alexa
                            item.setTitle(getString(R.string.enable_alexa))
//                            mBinding.ivAlexa.setTag("Disable")
//                            mBinding.ivAlexa.setVisibility(View.GONE)
                            AlexaFragment.signOut(this@ProjectsActivity, object : ImplAsyncCallback<Boolean, Throwable>() {
                                override fun success(result: Boolean?) {
                                    Timber.e("Alexa sign out completed")
                                }
                            })
                        }
                        /********************************************/
                    }

                    R.id.mi_help -> {
                        startActivity(Intent(this@ProjectsActivity, HelpActivity::class.java))
                    }
                    R.id.mi_privacyPolicy -> {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(com.bizbrolly.Constants.PRIVACY_POLICY)))
                    }
                    R.id.mi_terms_and_conditions -> {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(com.bizbrolly.Constants.TERMS_AND_CONDITIONS)))
                    }
                    R.id.mi_changePassword -> {
                        startActivity(Intent(this@ProjectsActivity, ChangePasswordActivity::class.java))
                    }
                    R.id.mi_logout -> {
                        showLogoutConfirmationDialog()
                    }
                }

            } else {
                // Should not occur
                Timber.d("View is null")
                return false
            }
            return false
        }
    }

    private fun showLogoutConfirmationDialog() {
        if (dialog == null) {
            initDialogs()
        }
        dialog!!.setContentView(R.layout.dialog_logout_confirmation)
        var okButton = dialog!!.findViewById<Button>(R.id.bt_okButton)
        var cancelButton = dialog!!.findViewById<Button>(R.id.bt_cancelButton)

        okButton.setOnClickListener {
            dialog!!.dismiss()
            logout()
        }

        cancelButton.setOnClickListener {
            dialog!!.cancel()
        }

        dialog!!.show()
    }

    private fun logout() {
        showLoadingPopup(true, getString(R.string.dialog_logging_out))
        AlexaFragment.signOut(this, object : ImplAsyncCallback<Boolean, Throwable>() {
            override fun success(result: Boolean?) {
                Timber.d("Alexa sign out completed")
                runOnUiThread {
                    showLoadingPopup(false, null)
                    Preferences.getInstance(this@ProjectsActivity).clearPreferences()
                    Preferences.getInstance(this@ProjectsActivity).setIsWelcomeVisited(true)
//                    AppDatabase.clearDatabase()


                    /***** Implementation for Wifi Module  */
                    // The below line is commented from BLE version
                    //onBackPressed();

                    (application as SvarochiApplication).wifiDatabase.clearAllTables()
                    val preference = SharedPreference(applicationContext)
                    preference.clearPreferences()
                    finishAffinity()
                    /******************************************/
                }
            }
        })
    }

    private fun showSwitchProfileScreen() {
        startActivityForResult(Intent(this, SwitchProfileActivity::class.java), SWITCH_PROFILE)
    }


    private fun showUserInvitationsScreen() {
        startActivity(Intent(this, UserInvitationsActivity::class.java))
    }


    private fun saveProjectIdToPreference(projectId: Long) {
        if (projectId != (-1).toLong()) {
            Timber.d("Saving project_id($projectId) to preferecnce")
            preference!!.setProjectId(projectId)
        } else {
            Timber.d("Project Id is not set")
        }
    }

    private fun initViews() {
        //viewModel.getProjects()

        initProjectsRecyclerView()

        if (isSelfProfile) {
            tv_addUptoProjects.text = getString(R.string.label_you_can_add_upto) + MAX_PROJECTS +
                    getString(R.string.label_projects)
        } else {
            tv_addUptoProjects.visibility = View.GONE
            tv_remainingProjects.visibility = View.GONE
        }


        resetSavedProjectId()
        initClickListeners()
        observeProjectsDb()

    }

    private fun initDialogs() {
        dialog = Dialog(this@ProjectsActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    private fun initClickListeners() {
        iv_options.setOnClickListener(this)
    }

    private fun observeProjectsDb() {
        Timber.d("Observing the projects table")
        viewModel.getProjectsLiveData().observe(this, Observer {
            if (it != null) {
                Timber.d("Change in project database")
                Utilities.sortProjects(it)
                updateProjectsRv(it)
                updateProjectCount(it.size)

            }
        })
    }

    private fun updateProjectsRv(updatedProjectsList: List<Project>) {
        projectsList.clear()
        for (updatedProject in updatedProjectsList) {
            projectsList.add(updatedProject)
        }

        if (projectsList.size < MAX_PROJECTS && isSelfProfile) {
            projectsList.add(Project(-1, ""))
        }

        rv_projectRv.adapter!!.notifyDataSetChanged()
        rv_projectRv.scrollToPosition(updatedProjectsList.size - 1)

    }

    private fun initViewModel() {
        viewModel = ViewModelProviders
                .of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(ProjectsViewModel::class.java)


        viewModel.getResponse().observe(this, Observer { t -> processResponse(t) })

        viewModel.stopUdpBroadcasts()
    }


    private fun addNewProject(name: String) {
        viewModel.addNewProject(name)
    }


    private fun processResponse(event: Event?) {
        when (event!!.type) {
            EventType.GET_PROJECTS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Fetching projects....")
                    }
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.SUCCESS -> {
                        /* Timber.d("${(event.data!! as List<Project>).size} projects found.")
                         initProjectsRecyclerView((event.data!! as List<Project>))
                         projectsCount = (event.data!! as List<Project>).size
                         updateProjectCount()*/
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                    }
                }
            }
            EventType.EDIT_PROJECT_NAME -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Editing project name")
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully updated project")
                        dialog!!.dismiss()
                    }
                    EventStatus.FAILURE -> {
                        if ((event.data != null)) {
                            if (event.data is Int) {
                                if ((event.data!! as Int) == ALREADY_EXISTS) {
                                    showToast(getString(R.string.toast_project_by_same_name_already_exists))
                                }

                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    showToast(getString(R.string.no_internet))
                                }
                            }
                        }
                    }
                }
            }
            EventType.DELETE_PROJECT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Deleting project...")
                    }

                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully deleted project")
                        dialog!!.dismiss()
                        showToast(getString(R.string.toast_deleted_project))

                    }

                    EventStatus.FAILURE -> {
                        if ((event.data != null)) {
                            if (event.data is Int) {
                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    Timber.d("No Internet available")
                                    showToast(getString(R.string.no_internet))
                                }

                                if (event.data as Int == Constants.CONTAINS_NETWORKS) {
                                    Timber.d("Project contains networks")
                                    showToast(getString(R.string.toast_please_delete_networks))
                                    return
                                }
                            }
                        }
                        showToast(getString(R.string.toast_failed_to_delete_the_project))
                    }
                }
            }


            EventType.ADD_NEW_PROJECT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        showPopup(true, getString(R.string.dialog_adding_project_to_server))
                    }
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.SUCCESS -> {
                        showPopup(false, null)
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                    }
                    EventStatus.FAILURE -> {
                        if (event.data != null) {
                            if ((event.data!! is Int)) {
                                if ((event.data!! as Int) == ALREADY_EXISTS) {
                                    showToast(getString(R.string.toast_project_by_same_name_already_exists))
                                    showPopup(false, null)
                                }

                                if ((event.data!! as Int) == NO_INTERNET_AVAILABLE) {
                                    showToast(getString(R.string.no_internet))
                                    showPopup(false, null)
                                }
                            }
                        } else {
                            showToast(getString(R.string.toast_failed_to_add_new_project))
                            showPopup(false, null)
                        }
                    }
                }
            }
        }
    }

    private fun updateProjectCount(size: Int) {
        projectsCount = size
        tv_remainingProjects.text =
                getString(R.string.label_add) +
                        (MAX_PROJECTS - projectsCount) +
                        getString(R.string.label_more_projects)

        if (projectsCount >= MAX_PROJECTS) {
            tv_remainingProjects.visibility = View.GONE
        } else {
            tv_remainingProjects.visibility = View.VISIBLE
        }
    }


    private fun initProjectsRecyclerView() {
        rv_projectRv.layoutManager = androidx.recyclerview.widget.GridLayoutManager(this, 2)
        rv_projectRv.adapter = ProjectsAdapter(projectsList, isSelfProfile, this)
    }


    fun addNewProject() {
        if (projectsCount < MAX_PROJECTS) {
            if (dialog == null) {
                initDialogs()
            }

            dialog!!.setContentView(R.layout.dialog_add_new_project)
            dialog!!.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

            dialog!!.show()
            val addButton: Button = dialog!!.findViewById(R.id.bt_addButton)
            val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
            val nameField: EditText = dialog!!.findViewById(R.id.et_name)
            nameField.requestFocus()

            addButton.setOnClickListener {
                if (nameField.text.toString().trim().isNotEmpty()) {

                    addNewProject(nameField.text.toString())
                    nameField.text.clear()
                    nameField.clearFocus()
                    dialog!!.dismiss()

                } else {
                    showToast(getString(R.string.toast_please_enter_project_name))
                }
            }

            cancelButton.setOnClickListener {
                val nameField: EditText = dialog!!.findViewById(R.id.et_name)
                nameField.text.clear()
                dialog!!.dismiss()
            }
        } else {
            showToast(getString(R.string.toast_max_projects_reached))
        }

    }

    fun openNetworksActivity(projectId: Long?, projectName: String) {
        saveProjectIdToPreference(projectId!!)
        preference!!.setNetworkId(-1)
        Timber.d("Show networks of project - $projectName")
        startActivity(
                Intent(this@ProjectsActivity, NetworksActivity::class.java)
                        .putExtra(BUNDLE_PROJECT_ID, projectId)
                        .putExtra(BUNDLE_PROJECT_NAME, projectName)
        )

    }

    fun editProjectName(project: Project) {
        Timber.d("Edit project - ${project.name}")

        // TODO - EDIT PROJECT
        dialog!!.setContentView(R.layout.dialog_edit_project)
        dialog!!.show()

        val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
        val renameProjectText: TextView = dialog!!.findViewById(R.id.tv_renameProject)
        val deleteProjectText: TextView = dialog!!.findViewById(R.id.tv_deleteProject)

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }

        renameProjectText.setOnClickListener {
            dialog!!.dismiss()
            showRenameDialog(project.id!!, project.name)
        }

        deleteProjectText.setOnClickListener {
            dialog!!.dismiss()
            showDeleteProjectConfirmation(project.id!!)
        }

    }

    private fun showDeleteProjectConfirmation(projectId: Long) {
        if (dialog == null) {
            dialog = Dialog(this)
        }
        dialog!!.setContentView(R.layout.dialog_delete_project_confirmation)
        dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        dialog!!.show()
        val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
        val okButton: Button = dialog!!.findViewById(R.id.bt_okButton)

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }

        okButton.setOnClickListener {
            dialog!!.dismiss()
            deleteProject(projectId)
        }
    }

    private fun showRenameDialog(projectId: Long, name: String) {
        dialog!!.setContentView(R.layout.dialog_rename_project)
        dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        dialog!!.show()
        val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
        val okButton: Button = dialog!!.findViewById(R.id.bt_okButton)
        val nameField: EditText = dialog!!.findViewById(R.id.et_name)
        nameField.text = Editable.Factory.getInstance().newEditable(name)
        nameField.requestFocus()

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }

        okButton.setOnClickListener {
            if (nameField.text.toString().trim().isNotEmpty()) {
                if (isItSameProject(projectId, nameField.text.toString())) {
                    dialog!!.dismiss()
                    return@setOnClickListener
                }
                editProjectName(projectId, nameField.text.toString())
            } else {
                showToast(getString(R.string.toast_please_enter_project_name))
            }
        }

    }

    private fun isItSameProject(projectId: Long, newName: String?): Boolean {
        return (rv_projectRv.adapter as ProjectsAdapter).isItSameProject(projectId, newName)
    }

    private fun deleteProject(projectId: Long) {
        if (viewModel.getNetworksCount(projectId) == 0) {
            viewModel.deleteProject(projectId)
        } else {
            showToast(getString(R.string.toast_please_delete_networks))
        }
    }

    private fun editProjectName(projectId: Long, name: String) {
        viewModel.editProjectName(projectId, name)
    }

    private fun resetSavedProjectId() {
        Timber.d("Reseting project_id to -1")
        preference!!.setProjectId(-1)
    }

    private fun requestServiceConnection() {
        Timber.d("Requesting service connection")
        requestToBindService(this)
    }

    private fun showPopup(show: Boolean, message: String?) {
        showLoadingPopup(show, message)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            SWITCH_PROFILE -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (viewModel != null) {
                        viewModel.switchedProfile()
                        init()
                    }
                }
            }
        }
    }

}
