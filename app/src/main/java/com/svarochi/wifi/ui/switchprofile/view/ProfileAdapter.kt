package com.svarochi.wifi.ui.switchprofile.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.model.common.CustomProfile

class ProfileAdapter(var context: SwitchProfileActivity, var profilesList: ArrayList<CustomProfile>) : RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_user_profile, parent, false)
        return ProfileViewHolder(view)
    }

    override fun getItemCount(): Int {
        return profilesList.size
    }

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        holder.bind(profilesList[position])
    }

    fun setProfileSelected(currentProfileId: String) {
        for (customProfile in profilesList) {
            if (customProfile.profile.id.equals(currentProfileId)) {
                customProfile.isSelected = true
                break
            }
        }
        notifyDataSetChanged()
    }

    inner class ProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profileName = itemView.findViewById<TextView>(R.id.tv_profileName)

        fun bind(customProfile: CustomProfile) {
            profileName.text = customProfile.profile.profileName

            if (customProfile.isSelected) {
                profileName.setTextColor(ContextCompat.getColor(context, R.color.aquaBlue))
            } else {
                profileName.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
        }

        init {
            profileName.setOnClickListener {
                var position = adapterPosition
                context.selectProfile(profilesList[position].profile.id, profilesList[position].profile.profileName!!)
            }
        }
    }

}