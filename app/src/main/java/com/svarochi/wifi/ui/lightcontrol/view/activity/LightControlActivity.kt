package com.svarochi.wifi.ui.lightcontrol.view.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.Html
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.ScreenUtils
import com.jakewharton.rxbinding2.widget.RxSeekBar
import com.jakewharton.rxbinding2.widget.SeekBarProgressChangeEvent
import com.jakewharton.rxbinding2.widget.SeekBarStartChangeEvent
import com.jakewharton.rxbinding2.widget.SeekBarStopChangeEvent
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.colorpicker.WifiColorPickerDialog
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_LIGHT_MACID
import com.svarochi.wifi.common.Constants.Companion.CAMERA
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.common.Constants.Companion.LIGHT_SYNCED
import com.svarochi.wifi.common.Constants.Companion.MQTT_TRANSITION_DELAY
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Constants.Companion.NO_TCP_CONNECTION
import com.svarochi.wifi.common.Constants.Companion.PHOTO_GALLERY
import com.svarochi.wifi.common.Constants.Companion.TCP_TRANSITION_DELAY
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.common.Utilities.Companion.convertHexStringToInt
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.LampType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.customeffect.view.activity.CustomEffectActivity
import com.svarochi.wifi.ui.diagnostics.view.DiagnosticsScreen
import com.svarochi.wifi.ui.lightcontrol.view.fragment.ImagePickerDialogFragment
import com.svarochi.wifi.ui.lightcontrol.viewmodel.LightControlViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_light_control.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue
import kotlin.math.roundToInt


@Suppress("UNNECESSARY_NOT_NULL_ASSERTION")
class LightControlActivity : WifiBaseActivity(), View.OnClickListener, ImagePickerDialogFragment.ColorPickerInterface,
        ServiceConnectionCallback {
    override fun serviceConnected() {
        initViewModel()
        initViews()
        initListeners()
    }

    private fun sendInitialCommands() {
        if (light != null) {
            if (isInit) {
                isInit = false
                if (light!!.firmwareVersion.isNullOrEmpty()) {
                    viewModel.getFirmwareVersion(light!!.macId)
                }
            }
        } else {
            Timber.d("Light is null")
        }
    }

    private var disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var viewModel: LightControlViewModel
    private var dialog: Dialog? = null
    private var mColorPickerDialog: WifiColorPickerDialog? = null
    // private var requestingTurnOn: Boolean = false
    private var imagePickerFragment: ImagePickerDialogFragment? = null
    //  private lateinit var customLight: CustomLight
    private lateinit var macId: String
    private var isObservingLight = false
    private var light: Light? = null
    private var isSelfProfile = true
    private var isInit = true
    private var isLocallyAvailable = false

    val MAX = 100
    val MIN = 10
    val STEP = 1

    /*** For delay in color picker ***/
    var lastColorSentTime: Long = 0
    val colorPickerDelaySubject: PublishSubject<String> = PublishSubject.create()
    var lastSentColor = "000000"
    /*********************************/

    override fun onClick(v: View?) {
        when (v!!.id) {
            iv_back.id -> {
                /*setResult(
                    Activity.RESULT_OK, Intent()
                        .putExtra(BUNDLE_LIGHT, customLight)
                )*/
                finish()
            }
            ll_rainbow.id -> {
                setScene(SceneType.RAINBOW)
            }
            tv_study.id -> {

                setScene(SceneType.STUDY)
            }
            ll_energise.id -> {
                setScene(SceneType.ENERGISE)
            }
            tv_aqua.id -> {
                setScene(SceneType.AQUA)
            }
            ll_candleLight.id -> {
                setScene(SceneType.CANDLE_LIGHT)
            }
            tv_moonLight.id -> {
                setScene(SceneType.MOON_LIGHT)
            }
            ll_partyMode.id -> {
                setScene(SceneType.PARTY_MODE)
            }
            ll_sunset.id -> {
                setScene(SceneType.SUNSET)
            }
            tv_meditation.id -> {
                setScene(SceneType.MEDITATION)
            }
            ll_colorBlast.id -> {
                setScene(SceneType.COLOR_BLAST)
            }
            tv_colorPalette.id -> {
                showColorPalette()
            }
            tv_photoGallery.id -> {
                openPhotoGallery()
            }
            tv_camera.id -> {
                openCamera()
            }
            ll_wc_candleLight.id -> {
                setScene(SceneType.CANDLE_LIGHT)
            }
            iv_editLightName.id -> {

                showEditLightNameDialog()

            }
            iv_options.id -> {
                showOptionsMenu(v)
            }
        }
    }

    private fun openDiagnosticsScreen() {
        if (light != null) {
            startActivity(
                    Intent(this, DiagnosticsScreen::class.java)
                            .putExtra(BUNDLE_LIGHT_MACID, light!!.macId)
            )
        } else {
            // Should not occur
            Timber.d("Light variable is nul")
        }
    }

    private fun showEditLightNameDialog() {
        if (dialog != null) {
            if (light != null) {
                if (light!!.isSynced == LIGHT_SYNCED) {
                    dialog!!.setContentView(R.layout.dialog_edit_light_name)
                    dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

                    val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
                    val okButton: Button = dialog!!.findViewById(R.id.bt_okButton)
                    val nameField: EditText = dialog!!.findViewById(R.id.et_name)
                    nameField.text = Editable.Factory.getInstance().newEditable(light!!.name)
                    nameField.requestFocus()

                    dialog!!.show()
                    okButton.setOnClickListener {
                        if (nameField.text.trim().isNotEmpty()) {
                            dialog!!.dismiss()
                            editLightName(light!!.macId!!, nameField.text.toString())
                        } else {
                            Timber.d("Edit light name - No name entered")
                            showToast(getString(R.string.toast_name_cannot_be_empty))
                        }
                    }

                    cancelButton.setOnClickListener {
                        dialog!!.dismiss()
                    }
                } else {
                    showToast(getString(R.string.toast_light_unsynced_light_name))
                }
            } else {
                // Should not occur
                Timber.d("Light value not initialised")
            }

        } else {
            // Should not occur
            Timber.d("Dialog not initialised")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_light_control)
        requestToBindService(this)
    }


    private fun initViews() {
        //customLight = intent.getSerializableExtra(BUNDLE_LIGHT) as CustomLight
        var preference = SharedPreference(this)
        isSelfProfile = preference.getCurrentProfile().equals(CURRENT_PROFILE_SELF)

        if (isSelfProfile) {
            iv_editLightName.visibility = View.VISIBLE
            ll_nameTypeLayout.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0.4f
            )
        } else {
            iv_editLightName.visibility = View.GONE

            ll_nameTypeLayout.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0.2f
            )
        }

        macId = intent.getStringExtra(BUNDLE_LIGHT_MACID)
        /*tv_lightHeaderName.text = customLight.light.name
        tv_lightName.text = customLight.light.name*/

        dialog = Dialog(this@LightControlActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.dialog_loading)
        dialog!!.setCanceledOnTouchOutside(false)


        initColorPickerDialog()
        observeLight(macId)

        listenToBrightnessChangeEvents()
        listenToDaylightChangeEvents()
    }


    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun observeLight(macId: String) {
        if (isObservingLight) {
            viewModel.getLightLiveData(macId).removeObservers(this)
        }
        isObservingLight = true
        viewModel.getLightLiveData(macId).observe(this, Observer {
            if (it != null) {
                Timber.d("Change in light($macId)")
                light = it!!
                updateUi(it)
            } else {
                Timber.d("Light($macId) doesnt exist")
            }
        })
    }

    private fun updateUi(light: Light) {

        when (light.lampType) {
            LampType.NONE.value -> {
                hideAllControls()
            }
            LampType.BRIGHT_AND_DIM.value -> {
                ivLightType.setImageResource(R.drawable.icon_bright_dim)
                showBrightAndDimLampControls()
            }
            LampType.WARM_AND_COOL.value -> {
                ivLightType.setImageResource(R.drawable.icon_warm_cool)
                showWarmAndCoolLampControls()
            }
            LampType.COLOR_AND_DAYLIGHT.value -> {
                ivLightType.setImageResource(R.drawable.icon_color_daylight)
                showColorLampControls()
            }
            else -> {
                ivLightType.setImageResource(R.drawable.icon_unpaired)
                hideAllControls()
            }
        }

        tv_lightHeaderName.text = light.name
        tv_lightName.text = light.name

        when (light.lampType) {
            LampType.COLOR_AND_DAYLIGHT.value -> {
                tv_lightTypeName.text =
                        Html.fromHtml("${getString(R.string.label_color_and_daylight)}<sup><small>${getString(R.string.label_plus)}</small></sup>")
            }
            LampType.WARM_AND_COOL.value -> {
                tv_lightTypeName.text = getString(R.string.label_warm_and_cool)
            }
            LampType.BRIGHT_AND_DIM.value -> {
                tv_lightTypeName.text = getString(R.string.label_bright_and_dim)
            }
            else -> {
                tv_lightTypeName.text = getString(R.string._dash)
            }
        }

        cb_onOffLight.isChecked = light.onOffValue
        //sb_brightness.progress = light.brightnessValue

        /*if (customLight.isLocallyAvailable()) {
            tv_locallyAvailable.visibility = View.VISIBLE
        } else {
            tv_locallyAvailable.visibility = View.INVISIBLE
        }*/


        var daylightValue = calculatePercentage(convertHexStringToInt(light.cValue).toFloat())
        sb_daylightControl.progress = daylightValue.toInt()

        sb_brightness.progress = calculateProgress((light.brightnessValue.toFloat()).roundToInt(), MIN, MAX, STEP)

        when (light.sceneValue) {
            SceneType.CANDLE_LIGHT.value -> {
                ll_candleLight.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_energise.background = null
                ll_colorBlast.background = null
                ll_sunset.background = null
                ll_partyMode.background = null
            }
            SceneType.ENERGISE.value -> {
                ll_energise.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_colorBlast.background = null
                ll_sunset.background = null
                ll_partyMode.background = null
            }
            SceneType.SUNSET.value -> {
                ll_sunset.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_colorBlast.background = null
                ll_energise.background = null
                ll_partyMode.background = null
            }
            SceneType.COLOR_BLAST.value -> {
                ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_partyMode.background = null
            }
            SceneType.PARTY_MODE.value -> {
                ll_partyMode.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_colorBlast.background = null
            }
            SceneType.RAINBOW.value -> {
                ll_rainbow.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_partyMode.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_colorBlast.background = null
            }
            else -> {
                ll_rainbow.background = null
                ll_partyMode.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_colorBlast.background = null
            }
        }

        /*if ((daylightValue).roundToInt() < 10) {
            sb_daylightControl.progress = (10)
        } else {
            //sb_daylightControl.progress = ((daylightValue).roundToInt())
        }*/

        tv_lightFwVersion.visibility = View.VISIBLE
        tv_lightFwVersion.text = getString(R.string.label_firmware_version_) + light.firmwareVersion

        sendInitialCommands()
    }

    private fun showColorLampControls() {
        ll_warmAndCoolEffectsControl.visibility = View.GONE

        ll_allEffectsControl.visibility = View.VISIBLE
        ll_colorControl.visibility = View.VISIBLE
        ll_nameLayout.visibility = View.VISIBLE
        ll_brightnessControl.visibility = View.VISIBLE
        ll_daylightControl.visibility = View.VISIBLE
    }

    private fun showWarmAndCoolLampControls() {
        ll_allEffectsControl.visibility = View.GONE
        ll_colorControl.visibility = View.GONE
        ll_warmAndCoolEffectsControl.visibility = View.GONE

        ll_nameLayout.visibility = View.VISIBLE
        ll_brightnessControl.visibility = View.VISIBLE
        ll_daylightControl.visibility = View.VISIBLE
    }

    private fun showBrightAndDimLampControls() {
        ll_allEffectsControl.visibility = View.GONE
        ll_warmAndCoolEffectsControl.visibility = View.GONE
        ll_daylightControl.visibility = View.GONE
        ll_colorControl.visibility = View.GONE

        ll_nameLayout.visibility = View.VISIBLE
        ll_brightnessControl.visibility = View.VISIBLE
    }

    private fun hideAllControls() {
        ll_allEffectsControl.visibility = View.GONE
        ll_warmAndCoolEffectsControl.visibility = View.GONE
        ll_nameLayout.visibility = View.GONE
        ll_brightnessControl.visibility = View.GONE
        ll_daylightControl.visibility = View.GONE
    }

    private fun initColorPickerDialog() {
        if (mColorPickerDialog == null) {
            mColorPickerDialog = WifiColorPickerDialog.createColorPickerDialog(this)
            mColorPickerDialog!!.hideColorComponentsInfo()
            mColorPickerDialog!!.hideOpacityBar()
            mColorPickerDialog!!.hideHexaDecimalValue()
            mColorPickerDialog!!.hidePreviewBox()
            mColorPickerDialog!!.setOnColorPickedListener(onColorPickedListener)
        } else {
            Timber.d("Color picker dialog already initialised")
        }
    }



    private fun calculateProgress(value: Int, MIN: Int, MAX: Int, STEP: Int): Int {
        return 100 * (value - MIN) / (MAX - MIN)
    }

    private fun calculatePercentage(value: Float): Float {
        return (value / 255) * 100
    }

    private var daylightSeekChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            if (seekBar != null) {

                setDaylight(seekBar.progress)

            } else {
                Timber.d("Seekbar isnt there ")
            }
        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            /*if (fromUser) {
                var value = Math.round((progress.toDouble()) * (MAX - MIN) / 100)
                var displayValue = (value.toInt() + MIN) / STEP * STEP
                Timber.d("Light Daylight - $displayValue")
                //Toast.makeText(this@LightControlActivity, displayValue.toString(), Toast.LENGTH_SHORT).show()

                if((displayValue % 5) == 0){
                    setDaylight(displayValue)
                }

            } else {
                Timber.d("Daylight value not from user")
            }*/

        }
    }


    private fun initListeners() {
        iv_back.setOnClickListener(this)
        iv_editLightName.setOnClickListener(this)
        tv_aqua.setOnClickListener(this)
        ll_candleLight.setOnClickListener(this)
        tv_moonLight.setOnClickListener(this)
        ll_rainbow.setOnClickListener(this)
        ll_sunset.setOnClickListener(this)
        ll_partyMode.setOnClickListener(this)
        tv_meditation.setOnClickListener(this)
        ll_colorBlast.setOnClickListener(this)
        ll_energise.setOnClickListener(this)
        tv_study.setOnClickListener(this)
        tv_colorPalette.setOnClickListener(this)
        tv_photoGallery.setOnClickListener(this)
        tv_camera.setOnClickListener(this)
        ll_wc_candleLight.setOnClickListener(this)
        tv_lightName.setOnClickListener(this)
        iv_options.setOnClickListener(this)

        //sb_brightness.setOnSeekBarChangeListener(brightnessSeekChangeListener)
        //sb_daylightControl.setOnSeekBarChangeListener(daylightSeekChangeListener)
        cb_onOffLight.setOnClickListener {
            if (cb_onOffLight.isChecked) {
                cb_onOffLight.isChecked = cb_onOffLight.isChecked.not()
                //requestingTurnOn = true
                turnOnLight()
            } else {
                cb_onOffLight.isChecked = cb_onOffLight.isChecked.not()
                //requestingTurnOn = false
                turnOffLight()
            }
        }

        ivLightType.setOnLongClickListener {
            if (isSelfProfile) {
                openDeleteLampDialog()
            }
            true
        }
    }


    private fun listenToBrightnessChangeEvents() {
        disposables.add(RxSeekBar.changeEvents(sb_brightness)
                .flatMap {
                    when (it) {
                        is SeekBarProgressChangeEvent -> {
                            if (it.fromUser()) {
                                var value = Math.round((it.progress().toDouble()) * (MAX - MIN) / 100)
                                var displayValue = (value.toInt() + MIN) / STEP * STEP
                                tvBubbleIntensityValue.text = "$displayValue%"
                                rlBubbleIntensity.x = (getIntensitySliderThumbPos() - ScreenUtils.dpToPx(13)).toFloat()
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Start")
                            rlBubbleIntensity.visibility = View.VISIBLE
                        }
                        is SeekBarStopChangeEvent -> {
                            rlBubbleIntensity.visibility = View.GONE
                        }
                    }
                    Observable.just(it)
                }
                .debounce(if (isLocallyAvailable) TCP_TRANSITION_DELAY.toLong() else MQTT_TRANSITION_DELAY.toLong(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekBarChangeEvent ->
                    when (seekBarChangeEvent) {
                        is SeekBarProgressChangeEvent -> {
                            Timber.d("Change Events - Progress from user = ${seekBarChangeEvent.fromUser()}")
                            Timber.d("Change Events - Progress = ${seekBarChangeEvent.progress()}")

                            if (seekBarChangeEvent.fromUser()) {
                                var value = Math.round((seekBarChangeEvent.progress().toDouble()) * (MAX - MIN) / 100)
                                var displayValue = (value.toInt() + MIN) / STEP * STEP
                                setLightBrightness(displayValue)
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Start")
                        }
                        is SeekBarStopChangeEvent -> {
                            Timber.d("Change Events - Stop = ${seekBarChangeEvent.view().progress}")
                            Timber.d("Change Events - Stop")


                            var value = Math.round((seekBarChangeEvent.view().progress.toDouble()) * (MAX - MIN) / 100)
                            var displayValue = (value.toInt() + MIN) / STEP * STEP
                            setLightBrightness(displayValue)

                            getLightStatus()
                        }
                    }
                }
        )
    }

    private fun listenToDaylightChangeEvents() {
        disposables.add(RxSeekBar.changeEvents(sb_daylightControl)
                .debounce(if (isLocallyAvailable) TCP_TRANSITION_DELAY.toLong() else MQTT_TRANSITION_DELAY.toLong(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekBarChangeEvent ->
                    when (seekBarChangeEvent) {
                        is SeekBarProgressChangeEvent -> {
                            Timber.d("Change Events - Daylight Progress from user = ${seekBarChangeEvent.fromUser()}")
                            Timber.d("Change Events - Daylight Progress = ${seekBarChangeEvent.progress()}")

                            if (seekBarChangeEvent.fromUser()) {
                                setDaylight(seekBarChangeEvent.progress())
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Daylight Start")
                        }
                        is SeekBarStopChangeEvent -> {
                            Timber.d("Change Events - Daylight Stop = ${seekBarChangeEvent.view().progress}")
                            Timber.d("Change Events - Daylight Stop")

                            setDaylight(seekBarChangeEvent.view().progress)

                            getLightStatus()
                        }
                    }
                }
        )
    }

    private fun getIntensitySliderThumbPos(): Int {
        val width = (sb_brightness.width
                - sb_brightness.paddingLeft
                - sb_brightness.paddingRight)
        return sb_brightness.paddingLeft + width * sb_brightness.progress / sb_brightness.max + sb_brightness.thumb.intrinsicWidth / 2
    }

    private fun getLightStatus() {
        Handler().postDelayed({
            if (viewModel != null) {
                viewModel!!.getLightStatus(macId)
            }
        }, 1500)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(LightControlViewModel::class.java)
        viewModel.getResponse().observe(this, Observer { t -> processResponse(t) })
    }

    private fun processResponse(event: Event?) {
        when (event!!.type) {

            EventType.CONNECTION -> {
                when (event.status) {
                    EventStatus.FAILURE -> {
                        showPopup(false, null)
                        if (event.data != null && event.data is String) {
                            Timber.d("$macId is not locally available")
                            isLocallyAvailable = false

                            disposables.clear()
                            disposables = CompositeDisposable()
                            listenToBrightnessChangeEvents()
                            listenToDaylightChangeEvents()
                        }
                    }
                    EventStatus.SUCCESS -> {
                        if (event.data != null && event.data is String) {
                            Timber.d("$macId is locally available")
                            isLocallyAvailable = true

                            disposables.clear()
                            disposables = CompositeDisposable()
                            listenToBrightnessChangeEvents()
                            listenToDaylightChangeEvents()
                        }
                    }
                }
            }
            EventType.SET_LIGHT_RGB -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting color to light - ${macId}")
                        showPopup(true, "Setting light color")
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Color successfully set - ${macId}")
                        showPopup(false, null)
                        //updateLight(event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light)
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set color - ${macId}")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //updateUi()
                        //observeLight(macId)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set color - ${macId}")
                        showPopup(false, null)
                        //updateUi()
                        //observeLight(macId)
                    }
                }
            }
            EventType.SET_LIGHT_PRESET_SCENE -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting light scene - ${macId}")
                        showPopup(true, getString(R.string.popup_setting_light_scene))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Scene successfully reset - ${macId}")
                        showPopup(false, null)
                        //updateLight(event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light)
                        //showLightControlPage(true)
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set scene - ${macId}")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //updateUi()
                        observeLight(macId)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set scene - ${macId}")
                        //showToast(getString(R.string.toast_failed_to_set_scene))
                        showPopup(false, null)
                        //updateUi()
                        observeLight(macId)
                    }
                }
            }
            EventType.TURN_ON_LIGHT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning on light - ${macId}")
                        showPopup(true, getString(R.string.popup_turning_on_light))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully turned on light- ${macId}")
                        showPopup(false, null)
                        //updateLight(event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light)
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                        Timber.d("Failed to turn on light- ${macId}")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_failed_to_turn_on_light))
                        //updateUi()
                        //observeLight(macId)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn on light- ${macId}")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_failed_to_turn_on_light))
                        //updateUi()
                        //observeLight(macId)
                    }
                }
            }
            EventType.TURN_OFF_LIGHT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning off light - ${macId}")
                        showPopup(true, getString(R.string.popup_turning_off_light))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully turned off light- ${macId}")
                        showPopup(false, null)
                        //updateLight(event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light)
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                        Timber.d("Failed to turn off light- ${macId}")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_failed_to_turn_off_light))
                        //updateUi()
                        //observeLight(macId)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn off light - ${macId}")
                        showPopup(false, null)
                        //(getString(R.string.toast_failed_to_turn_off_light))
                        //updateUi()
                        //observeLight(macId)
                    }
                }
            }
            EventType.SET_LIGHT_BRIGHTNESS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting brightness of light - ${macId}")
                        showPopup(true, getString(R.string.popup_setting_brightness))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Brightness successfully set - ${macId}")
                        showPopup(false, null)
                        //updateLight(event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light)
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set brightness - ${macId}")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //updateUi()
                        //observeLight(macId)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set brightness - ${macId}")
                        showPopup(false, null)
                        //updateUi()
                        //observeLight(macId)
                    }
                }
            }
            EventType.SET_LIGHT_DAYLIGHT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting daylight of light - ${macId}")
                        showPopup(true, getString(R.string.popup_setting_daylight))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Daylight successfully set - ${macId}")
                        showPopup(false, null)
                        //updateLight(event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light)

                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set daylight - ${macId}")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //updateUi()
                        //observeLight(macId)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set daylight - ${macId}")
                        showPopup(false, null)
                        //updateUi()
                        //observeLight(macId)
                    }
                }
            }
            EventType.EDIT_LIGHT_NAME -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Editing name of light - ${macId}")
                        showPopup(true, getString(R.string.popup_editing_light_name))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully edited light name - ${macId}")
                        showPopup(false, null)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to edit light name - ${macId}")
                        showPopup(false, null)

                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data == ALREADY_EXISTS) {
                                    showToast(getString(R.string.toast_light_by_that_name_already_exists))
                                }

                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    showToast(getString(R.string.no_internet))
                                }
                            }
                        }
                    }
                }
            }

            EventType.DELETE_LAMP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Deleting light")
                        showLoadingPopup(true, getString(R.string.dialog_deleting_light))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully deleted the light")
                        showLoadingPopup(false, getString(R.string.dialog_deleting_light))
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to delete light")
                        showLoadingPopup(false, null)

                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    Timber.d("No internet available")
                                    showToast(getString(R.string.no_internet))
                                }

                                if (event.data as Int == NO_TCP_CONNECTION) {
                                    Timber.d("No Tco connection with device")
                                    showToast(getString(R.string.toast_device_not_reachable))
                                }
                            }
                        } else {
                            showToast(getString(R.string.toast_failed_to_delete_the_lamp))
                        }
                    }
                }
            }
        }
    }

    private fun updateLight(light: Light) {
        //customLight.light = light
        //updateUi()

    }

    private fun showLightControlPage(show: Boolean) {
        if (show)
            sv_lightControlPage.visibility = View.VISIBLE
        else
            sv_lightControlPage.visibility = View.GONE
    }

    private fun showPopup(show: Boolean, message: String?) {
        //showLoadingPopup(show, message)
    }


    private fun setLightColor(color: String, isForced: Boolean) {
        // Sample input - 00FF00
        if (color.equals(lastSentColor).not()) {
            if (isForced) {
                Timber.d("Color - $color sent to device $macId")
                lastSentColor = color
                viewModel.setLampColor(
                        macId,
                        r_value = color.substring(0, 2),
                        g_value = color.substring(2, 4),
                        b_value = color.substring(4)
                )
            } else {
                var currentTime = System.currentTimeMillis()
                val delay = if (isLocallyAvailable) TCP_TRANSITION_DELAY.toLong() else MQTT_TRANSITION_DELAY.toLong()
                if ((currentTime - lastColorSentTime) >= delay) {
                    Timber.d("Color - $color sent to device $macId")
                    lastColorSentTime = currentTime
                    lastSentColor = color
                    viewModel.setLampColor(
                            macId,
                            r_value = color.substring(0, 2),
                            g_value = color.substring(2, 4),
                            b_value = color.substring(4)
                    )
                }
            }
        }
    }

    private fun setLightBrightness(value: Int) {
        Timber.d("View : Setting brightness to light ${macId}")
        cb_onOffLight.isChecked = true

        viewModel.setLampBrightness(
                macId,
                value
        )
    }

    private fun setDaylight(value: Int) {
        Timber.d("View : Setting daylight to light ${macId}")

        cb_onOffLight.isChecked = true

        viewModel.setDaylight(
                macId,
                value
        )
    }

    private fun turnOffLight() {
        viewModel.turnOffLight(
                macId
        )
    }

    private fun turnOnLight() {
        viewModel.turnOnLight(
                macId
        )
    }

    private fun setScene(sceneType: SceneType) {
        when (sceneType) {
            SceneType.ENERGISE -> {
                ll_energise.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                viewModel.setScene(
                        macId,
                        sceneType
                )
            }
            SceneType.CANDLE_LIGHT -> {
                ll_candleLight.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_energise.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                viewModel.setScene(
                        macId,
                        sceneType
                )
            }
            SceneType.SUNSET -> {
                ll_sunset.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                viewModel.setScene(
                        macId,
                        sceneType
                )
            }
            SceneType.COLOR_BLAST -> {
                ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_partyMode.background = null
                // TODO - Dynamic scene feature can be turned off

                if (light != null) {
                    if (light!!.sceneValue.equals(SceneType.COLOR_BLAST.value)) {
                        // TODO - Color blast is already set so we need to turn off
                        ll_colorBlast.background = null
                        Timber.d("Color blast is already set, turning off color blast")
                        viewModel.turnOffDynamicScene(light!!.macId)

                    } else {
                        // TODO - Color blast is not set so we need to turn on
                        ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                        Timber.d("Color blast is not set, setting color blast scene")

                        viewModel.setScene(
                                macId,
                                sceneType
                        )
                    }
                } else {
                    // Should not occur
                    Timber.d("Light value is null")
                }
            }
            SceneType.PARTY_MODE -> {
                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null

                if (light != null) {
                    if (light!!.sceneValue.equals(SceneType.PARTY_MODE.value)) {
                        // Party mode is already set so we need to turn off
                        ll_partyMode.background = null
                        Timber.d("Party mode is already set, turning off Party mode")
                        viewModel.turnOffDynamicScene(light!!.macId)

                    } else {
                        // Party mode is not set so we need to turn on
                        ll_partyMode.setBackgroundResource(R.drawable.today_tomorrow_selected)
                        Timber.d("Party mode is not set, setting color Party mode")

                        viewModel.setScene(
                                macId,
                                sceneType
                        )
                    }
                } else {
                    // Should not occur
                    Timber.d("Light value is null")
                }
            }
            SceneType.RAINBOW -> {
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null

                if (light != null) {
                    if (light!!.sceneValue.equals(SceneType.RAINBOW.value)) {
                        // Rainbow is already set so we need to turn off
                        ll_rainbow.background = null
                        Timber.d("Rainbow is already set, turning off Rainbow")
                        viewModel.turnOffDynamicScene(light!!.macId)

                    } else {
                        // Rainbow is not set so we need to turn on
                        ll_rainbow.setBackgroundResource(R.drawable.today_tomorrow_selected)
                        Timber.d("Rainbow is not set, setting color Rainbow")

                        viewModel.setScene(
                                macId,
                                sceneType
                        )
                    }
                } else {
                    // Should not occur
                    Timber.d("Light value is null")
                }
            }
        }
    }

    private fun showColorPalette() {
        if (mColorPickerDialog == null) {
            initColorPickerDialog()
        }
        lastSentColor = "000000"
        mColorPickerDialog!!.show()
    }

    private fun openPhotoGallery() {
        var fm = supportFragmentManager
        imagePickerFragment = ImagePickerDialogFragment.newInstance(PHOTO_GALLERY.absoluteValue)
        lastSentColor = "000000"
        imagePickerFragment!!.show(fm, "image_picker_fragment")
    }


    private var onColorPickedListener = object : WifiColorPickerDialog.OnColorPickedListener {
        override fun onForceColorPicked(color: Int, hexVal: String?) {
            Timber.d("Color picked from color palette - ${Utilities.convertColorToHexString(color)}")
            setLightColor(Utilities.convertColorToHexString(color), true)
        }

        override fun onColorPicked(color: Int, hexVal: String) {
            Timber.d("Color picked from color palette - ${Utilities.convertColorToHexString(color)}")
            setLightColor(Utilities.convertColorToHexString(color), false)

        }
    }


    override fun onColorSelected(color: String) {
        // From Photo gallery
        Timber.d("User selected from gallery - $color color")
        setLightColor(color, false)
//        colorPickerDelaySubject.onNext(color)
    }

    override fun onForceColorSelected(color: String) {
        // From Photo gallery
        Timber.d("User selected from gallery - $color color")
        setLightColor(color, true)
//        colorPickerDelaySubject.onNext(color)
    }

    private fun openCamera() {
        var fm = supportFragmentManager
        imagePickerFragment = ImagePickerDialogFragment.newInstance(CAMERA.absoluteValue)
        lastSentColor = "000000"
        imagePickerFragment!!.show(fm, "image_picker_fragment")
    }

    private fun editLightName(macId: String, name: String) {
        viewModel.editLightName(macId, name)
    }

    private fun showOptionsMenu(view: View) {
        var popup = PopupMenu(this, view)
        popup.menuInflater.inflate(R.menu.options_light_control, popup.menu)
        popup.setOnMenuItemClickListener(optionMenuItemClickListener)

        popup.show()
    }

    private val optionMenuItemClickListener = object : PopupMenu.OnMenuItemClickListener {
        override fun onMenuItemClick(item: MenuItem?): Boolean {
            when (item!!.itemId) {
                R.id.mi_diagonisticsScreen -> {
                    openDiagnosticsScreen()
                }
                /*R.id.mi_customEffect -> {
                    openCustomEffectScreen()
                }*/
            }
            return false
        }

    }

    private fun openCustomEffectScreen() {
        startActivity(Intent(this, CustomEffectActivity::class.java))
    }

    fun openDeleteLampDialog() {
        if (dialog != null) {
            if (light != null) {
                dialog!!.setContentView(R.layout.dialog_delete_lamp_confirmation)
                var cancelButton = dialog!!.findViewById<Button>(R.id.bt_cancelButton)
                var okButton = dialog!!.findViewById<Button>(R.id.bt_okButton)
                dialog!!.show()

                cancelButton.setOnClickListener {
                    dialog!!.dismiss()
                }

                okButton.setOnClickListener {
                    dialog!!.dismiss()
                    if (light!!.isSynced == LIGHT_SYNCED) {
                        deleteLamp(macId)
                    } else {
                        deleteUnsyncedLamp(macId)
                    }
                }
            }
        }
    }

    private fun deleteLamp(macId: String) {
        Timber.d("Confirmed to delete $macId")
        if (viewModel != null) {
            viewModel!!.deleteLamp(macId)
        } else {
            Timber.d("View model is null")
        }
    }

    private fun deleteUnsyncedLamp(macId: String) {
        Timber.d("Confirmed to delete $macId")
        if (viewModel != null) {
            viewModel!!.deleteUnsyncedLamp(macId)
        } else {
            Timber.d("View model is null")
        }
    }

}

