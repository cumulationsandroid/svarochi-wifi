package com.svarochi.wifi.ui.userSession.activities

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView

import com.akkipedia.skeleton.utils.GeneralUtils
import com.bizbrolly.WebServiceRequests
import com.bizbrolly.entities.SetSecurityKeyResponse

import bizbrolly.svarochiapp.R
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.common.CommonUtils
import com.svarochi.wifi.common.DialogUtils
import kotlinx.android.synthetic.main.activity_set_secret_code.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SetSecretCodeActivity : BaseActivity() {
    // private String otp = "";
    private var email: String? = null
    private var phoneNumber: String? = null

    private var editorActionListener: TextView.OnEditorActionListener = TextView.OnEditorActionListener { v, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_DONE || event.keyCode == KeyEvent.KEYCODE_ENTER) {
//            requestSetSecretCode(getRoot())
            requestSetSecretCode(window.decorView.rootView)
        }
        false
    }

    // variable to track event time
    private var mLastClickTime: Long = 0
    private var deleteKeyListener: View.OnKeyListener = View.OnKeyListener { v, keyCode, event ->
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTime < 100) {
            return@OnKeyListener false
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            when (v.id) {
                R.id.etOtp1 -> {
                    Log.e(TAG, "etOtp1")
                    when {
                        etOtp4.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp4.setText("")
                            }
                            etOtp4.requestFocus()
                        }
                        etOtp3.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp3.setText("")
                            }
                            etOtp3.requestFocus()
                        }
                        etOtp2.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp2.setText("")
                            }
                            etOtp2.requestFocus()
                        }
                        etOtp1.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp1.setText("")
                            }
                            etOtp1.requestFocus()
                        }
                    }
                }
                R.id.etOtp2 -> {
                    Log.e(TAG, "etOtp2")
                    if (etOtp2.text?.length!! == 0) {
                        etOtp1.setText("")
                    }
                    etOtp1.requestFocus()
                }
                R.id.etOtp3 -> {
                    Log.e(TAG, "etOtp3")
                    if (etOtp3.text?.length!! == 0) {
                        etOtp2.setText("")
                    }
                    etOtp2.requestFocus()
                }
                R.id.etOtp4 -> {
                    Log.e(TAG, "etOtp4")
                    if (etOtp4.text?.length!! == 0) {
                        etOtp3.setText("")
                    }
                    etOtp3.requestFocus()
                }
            }
        }
        false
    }

    private var otpTextWatcher: TextWatcher = object : TextWatcher {

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            if (etOtp1.text.hashCode() == s.hashCode()) {
                if (etOtp1.length() > 0) {
                    if (etOtp2.length() > 0) {
                        if (etOtp3.length() > 0) {
                            if (etOtp4.length() > 0) {
                                //otp = etOtp1.getText().toString() + etOtp2.getText().toString() + etOtp3.getText().toString() + etOtp4.getText().toString();
                                //requestVerifyToken();
                                etOtp1.transformationMethod = PasswordTransformationMethod.getInstance()
                                etOtp4.requestFocus()
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp3.requestFocus()
                        }
                    } else {
                        etOtp2.requestFocus()
                    }
                } else {
                    etOtp1.requestFocus()
                }
            } else if (etOtp2.text.hashCode() == s.hashCode()) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        if (etOtp3.length() > 0) {
                            if (etOtp4.length() > 0) {
                                //otp = etOtp1.getText().toString() + etOtp2.getText().toString() + etOtp3.getText().toString() + etOtp4.getText().toString();
                                //requestVerifyToken();
                                etOtp2.transformationMethod = PasswordTransformationMethod.getInstance()
                                etOtp4.requestFocus()
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp3.requestFocus()
                        }
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else if (etOtp3.text.hashCode() == s.hashCode()) {
                if (etOtp3.length() > 0) {
                    if (etOtp2.length() > 0) {
                        if (etOtp1.length() > 0) {
                            if (etOtp4.length() > 0) {
                                //otp = etOtp1.getText().toString() + etOtp2.getText().toString() + etOtp3.getText().toString() + etOtp4.getText().toString();
                                //requestVerifyToken();
                                etOtp3.transformationMethod = PasswordTransformationMethod.getInstance()
                                etOtp4.requestFocus()
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp1.requestFocus()
                        }
                    } else {
                        if (etOtp1.length() > 0) {
                            etOtp2.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    }
                } else {
                    if (etOtp2.length() > 0) {
                        if (etOtp1.length() > 0) {
                            etOtp3.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    } else {
                        if (etOtp1.length() > 0) {
                            etOtp2.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    }
                }
            } else if (etOtp4.text.hashCode() == s.hashCode()) {
                if (etOtp4.length() > 0) {
                    if (etOtp3.length() > 0) {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp4.transformationMethod = PasswordTransformationMethod.getInstance()
                                etOtp1.requestFocus()
                                //otp = etOtp1.getText().toString() + etOtp2.getText().toString() + etOtp3.getText().toString() + etOtp4.getText().toString();
                                //requestVerifyToken();
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    } else {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp3.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    }
                } else {
                    if (etOtp3.length() > 0) {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp4.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    } else {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp3.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_secret_code)
        Log.e(TAG, "SetSecretCodeActivity onCreate")
        init()
    }

    private fun init() {
        getBundleData()
        setListeners()
    }

    private fun getBundleData() {
        if (intent != null && intent.extras != null) {
            val bundle = intent.extras
            if (bundle!!.containsKey("Email")) {
                email = bundle.getString("Email")
            }
            if (bundle.containsKey("Phone")) {
                phoneNumber = bundle.getString("Phone")
            }
        }
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        etOtp1.setOnEditorActionListener(editorActionListener)
        etOtp2.setOnEditorActionListener(editorActionListener)
        etOtp3.setOnEditorActionListener(editorActionListener)
        etOtp4.setOnEditorActionListener(editorActionListener)

        etOtp1.setOnKeyListener(deleteKeyListener)
        etOtp2.setOnKeyListener(deleteKeyListener)
        etOtp3.setOnKeyListener(deleteKeyListener)
        etOtp4.setOnKeyListener(deleteKeyListener)

        etOtp1.addTextChangedListener(otpTextWatcher)
        etOtp2.addTextChangedListener(otpTextWatcher)
        etOtp3.addTextChangedListener(otpTextWatcher)
        etOtp4.addTextChangedListener(otpTextWatcher)
    }

    private fun validateSecretCode(): String {
        var secretCode = ""
        if (etOtp4.length() > 0) {
            if (etOtp3.length() > 0) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        secretCode = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        etOtp3.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            }
        } else {
            if (etOtp3.length() > 0) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {

                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        etOtp3.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            }
        }

        return secretCode
    }

    fun requestSetSecretCode(view: View) {
        val secretCode = validateSecretCode()
        if (TextUtils.isEmpty(secretCode) || secretCode.length < 4) {
            showToast(getString(R.string.secret_code_should_be_digit_long))
            return
        }
        etOtp4.requestFocus()
        if (!GeneralUtils.isInternetAvailable(this)) {
            showToast(getString(R.string.no_internet))
            return
        }
        CommonUtils.hideSoftKeyboard(this@SetSecretCodeActivity)
        showProgressDialog()
        WebServiceRequests.getInstance().setSecurityKey(email, phoneNumber, secretCode.trim { it <= ' ' },
                object : Callback<SetSecurityKeyResponse> {
                    override fun onResponse(call: Call<SetSecurityKeyResponse>, response: Response<SetSecurityKeyResponse>) {
                        Log.e(TAG, "response SetSecretCode")
                        hideProgressDialog()
                        if (response?.body() != null) {
                            if (response.body()!!.getDBDetailsResult.isResult) {
                                DialogUtils.showDefaultAlertMessage(this@SetSecretCodeActivity, "", "You have setup secret code successfully. Try to login with secret code.", getString(R.string.ok)) {
                                    val intent = Intent(this@SetSecretCodeActivity, LoginActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                    val bundle = Bundle()
                                    bundle.putBoolean(LoginActivity.BUNDLE_IS_SLAVE_USER, false)
                                    intent.putExtras(bundle)
                                    startActivity(intent)
                                    finish()
                                }
                            } else {
                                if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorDetails != null) {
                                    showToast(response.body()!!.getDBDetailsResult.errorDetail.errorDetails)
                                } else {
                                    showToast(getString(R.string.something_went_wrong))
                                }
                            }
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }

                    override fun onFailure(call: Call<SetSecurityKeyResponse>, t: Throwable) {
                        hideProgressDialog()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                }
        )
    }

    companion object {
        private val TAG = SetSecretCodeActivity::class.java.simpleName
    }
}
