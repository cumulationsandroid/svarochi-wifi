package com.svarochi.wifi.ui.customeffect.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bizbrolly.svarochiapp.R

class CustomEffectTransitionSpeed : androidx.fragment.app.Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_custom_effect_transition_speed, container, false)
    }


}
