package com.svarochi.wifi.ui.userSession;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

/**
 * Created by Akash on 15/11/16.
 */

public class BaseActivity extends AppCompatActivity {
    private boolean contentViewSet = false;
    DrawerLayout drawerLayout;
    //region Methods for progress dialog
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivityAnimation();
    }

    public void startActivityAnimation() {
        overridePendingTransition(com.bizbrolly.skeleton.R.anim.enter_slide_in, com.bizbrolly.skeleton.R.anim.enter_slide_out);
    }

    public final void showProgressDialog(final String title, final String message) {
        runOnUiThread(() -> {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(BaseActivity.this);
            }
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(com.bizbrolly.skeleton.R.anim.exit_slide_in, com.bizbrolly.skeleton.R.anim.exit_slide_out);
    }

    public final void hideProgressDialog() {
        runOnUiThread(() -> {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        });
    }

    //endregion

    /**
     * Making sure setContentWithNavigationDrawer() is called before setContentView().
     */
    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        contentViewSet = true;
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        contentViewSet = true;
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        contentViewSet = true;
    }
    //endregion

    public void showToast(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public final void showProgressDialog() {
        showProgressDialog("Loading", "Please wait...");
    }
}
