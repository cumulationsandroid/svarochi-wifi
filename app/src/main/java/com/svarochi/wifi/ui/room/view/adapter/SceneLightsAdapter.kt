package com.svarochi.wifi.ui.room.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.model.api.common.CustomSceneDevice
import com.svarochi.wifi.ui.room.view.fragment.ScenesFragment

class SceneLightsAdapter(
        var lightsList: ArrayList<CustomSceneDevice>,
        var context: ScenesFragment
) :
        androidx.recyclerview.widget.RecyclerView.Adapter<SceneLightsAdapter.AddNewSceneLightsViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): AddNewSceneLightsViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_new_scene_light, viewGroup, false)
        return AddNewSceneLightsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lightsList.size
    }

    override fun onBindViewHolder(viewHolder: AddNewSceneLightsViewHolder, positiion: Int) {
        viewHolder.lightName.text = lightsList.get(positiion).light_name
        viewHolder.selectLightCheckBox.isChecked = lightsList.get(positiion).is_selected

        if (lightsList.get(positiion).is_selected) {
            viewHolder.setLight.visibility = View.VISIBLE
        } else {
            viewHolder.setLight.visibility = View.INVISIBLE
        }
    }


    inner class AddNewSceneLightsViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var lightName: TextView = itemView.findViewById(R.id.tv_lightName)
        var selectLightCheckBox: CheckBox = itemView.findViewById(R.id.cb_selectLightCheckBox)
        var setLight: Button = itemView.findViewById(R.id.bt_setLight)

        init {
            selectLightCheckBox.setOnClickListener {
                if (lightsList.get(adapterPosition).is_synced == Constants.LIGHT_SYNCED) {
                    lightsList.get(adapterPosition).is_selected = selectLightCheckBox.isChecked
                    if (selectLightCheckBox.isChecked) {
                        setLight.visibility = View.VISIBLE
                    } else {
                        setLight.visibility = View.INVISIBLE
                    }
                } else {
                    context.showToast(context.getString(R.string.toast_light_unsynced_group_scene))
                    selectLightCheckBox.isChecked = selectLightCheckBox.isChecked.not()
                }
            }

            setLight.setOnClickListener {
                val selectedPosition = adapterPosition
                context.openSetSceneLightScreen(lightsList.get(selectedPosition), selectedPosition)
            }
        }
    }

    fun getSelectedLights(): List<CustomSceneDevice> {
        var selectedLights = ArrayList<CustomSceneDevice>()

        lightsList.forEach {
            if (it.is_selected) {
                selectedLights.add(it)
            }
        }

        return selectedLights
    }

    fun updateSceneDevice(updatedSceneDevice: CustomSceneDevice, position: Int) {
        lightsList[position] = updatedSceneDevice
    }
}