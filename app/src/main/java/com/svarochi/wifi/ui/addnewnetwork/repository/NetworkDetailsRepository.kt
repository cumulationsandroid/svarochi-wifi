package com.svarochi.wifi.ui.addnewnetwork.repository

import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils.isInternetAvailable
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants.Companion.CONTAINS_DEVICES
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities.Companion.getErrorMessage
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.model.api.response.AddNetworkResponse
import com.svarochi.wifi.model.api.response.DeleteNetworkResponse
import com.svarochi.wifi.model.api.response.EditNetworkResponse
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber


class NetworkDetailsRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl {
    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var sharedPreference: SharedPreference? = null
    private var projectId: Long = -1
    private var userId: String = ""
    private var TAG = "NetworkDetailsRepository --->"

    init {
        initCommunicationResponseObserver()
        fetchIdsFromSharedPreference()
    }

    override fun initCommunicationResponseObserver() {
        // No implementation as not communicating with devices
    }

    private fun fetchIdsFromSharedPreference() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        projectId = sharedPreference!!.getProjectId()
        userId = sharedPreference!!.getUserId()
    }

    override fun getCommunicationResponseObserver() = responseObservable

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    fun addNewNetwork(name: String, ssid: String, password: String, authenticationMode: String, encryptionType: String) {
        if (isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser
                            .addNetwork(userId, name, projectId.toInt(), ssid, password, authenticationMode, encryptionType)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<AddNetworkResponse>>() {
                                override fun onComplete() {
                                    Timber.d("$TAG Completed Add Network Api")
                                }

                                override fun onNext(response: Response<AddNetworkResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            if (response.body()!!.status.equals("success")) {

                                                if (response.body()!!.network_id != null) {
                                                    Timber.d("$TAG Successfully added network into server")
                                                    addNetworkInLocalDb(response.body()!!.network_id!!.toLong(), name, ssid, password, authenticationMode, encryptionType)
                                                } else {
                                                    Timber.d("$TAG Something went wrong while adding new network to server")
                                                    var errorMessage = getErrorMessage(response)
                                                    if (errorMessage != null) {
                                                        Timber.d(errorMessage)
                                                    } else {
                                                        Timber.d("$TAG No Error message received from server")
                                                    }
                                                    responseObservable.onNext(Event(EventType.ADD_NEW_NETWORK, EventStatus.FAILURE, null, null))
                                                }

                                            } else {
                                                Timber.d("$TAG Something went wrong while adding new network to server")
                                                var errorMessage = getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.d(errorMessage)
                                                } else {
                                                    Timber.d("$TAG No Error message received from server")
                                                }
                                                responseObservable.onNext(Event(EventType.ADD_NEW_NETWORK, EventStatus.FAILURE, null, null))
                                            }
                                        } else {
                                            Timber.d("$TAG No response body received from server")
                                        }
                                    } else {
                                        Timber.d("$TAG Something went wrong while adding new network to server")
                                        var errorMessage = getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.d(errorMessage)
                                        } else {
                                            Timber.d("$TAG No Error message received from server")
                                        }
                                        responseObservable.onNext(Event(EventType.ADD_NEW_NETWORK, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    error.printStackTrace()
                                    Timber.d("$TAG Something went wrong while adding new network to server")
                                    responseObservable.onNext(Event(EventType.ADD_NEW_NETWORK, EventStatus.FAILURE, null, null))
                                }
                            })
            )
        } else {
            Timber.d("No Internet available")
            responseObservable.onNext(Event(EventType.ADD_NEW_NETWORK, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    fun editNetwork(networkId: Long, name: String, ssid: String, password: String, authenticationMode: String, encryptionType: String) {
        if (isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser
                            .editNetwork(userId, projectId.toInt(), networkId.toInt(), name, ssid, password, authenticationMode, encryptionType)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<EditNetworkResponse>>() {
                                override fun onComplete() {
                                    Timber.d("$TAG Completed Edit Network Api")
                                }

                                override fun onNext(response: Response<EditNetworkResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            if (response.body()!!.status.equals("success")) {

                                                if (response.body()!!.network_id != null) {
                                                    Timber.d("$TAG Successfully edited network in server")
                                                    updateNetworkInLocalDb(response.body()!!.network_id!!.toLong(), name, ssid, password, authenticationMode, encryptionType)
                                                } else {
                                                    Timber.d("$TAG Something went wrong while editing new network in server")
                                                    var errorMessage = getErrorMessage(response)
                                                    if (errorMessage != null) {
                                                        Timber.d(errorMessage)
                                                    } else {
                                                        Timber.d("$TAG No error message received from server")
                                                    }
                                                    responseObservable.onNext(Event(EventType.EDIT_NETWORK, EventStatus.FAILURE, null, null))
                                                }

                                            } else {
                                                Timber.d("$TAG Something went wrong while editing new network in server")
                                                var errorMessage = getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.d(errorMessage)
                                                } else {
                                                    Timber.d("$TAG No Error message received from server")
                                                }
                                                responseObservable.onNext(Event(EventType.EDIT_NETWORK, EventStatus.FAILURE, null, null))
                                            }
                                        } else {
                                            Timber.d("$TAG No response body received from server")
                                        }
                                    } else {
                                        Timber.d("$TAG Something went wrong while editing new network in server")
                                        var errorMessage = getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.d(errorMessage)
                                        } else {
                                            Timber.d("$TAG No Error message received from server")
                                        }
                                        responseObservable.onNext(Event(EventType.EDIT_NETWORK, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    error.printStackTrace()
                                    Timber.d("$TAG Something went wrong while editing new network in server")
                                    responseObservable.onNext(Event(EventType.EDIT_NETWORK, EventStatus.FAILURE, null, null))
                                }
                            })
            )
        } else {
            Timber.d("No Internet available")
            responseObservable.onNext(Event(EventType.EDIT_NETWORK, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun addNetworkInLocalDb(networkId: Long, name: String, ssid: String, password: String, authenticationMode: String, encryptionType: String) {
        var networkToAdd = Network(networkId, name, projectId, ssid, password, authenticationMode, encryptionType)
        application.wifiLocalDatasource.addNewNetwork(networkToAdd)

        Timber.d("Successfully added network($name) in local db")
        responseObservable.onNext(Event(EventType.ADD_NEW_NETWORK, EventStatus.SUCCESS, null, null))
    }

    private fun updateNetworkInLocalDb(networkId: Long, name: String, ssid: String, password: String, authenticationMode: String, encryptionType: String) {
        var networkToAdd = Network(networkId, name, projectId, ssid, password, authenticationMode, encryptionType)
        application.wifiLocalDatasource.addNewNetwork(networkToAdd)

        Timber.d("Successfully updated network($name) in local db")
        responseObservable.onNext(Event(EventType.EDIT_NETWORK, EventStatus.SUCCESS, null, null))
    }

    fun getLightsCount(networkId: Long): Int {
        return application.wifiLocalDatasource.getLights(networkId).size
    }

    fun deleteNetwork(networkId: Long, projectId: Long) {
        if (isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser
                            .deleteNetwork(userId, networkId.toInt(), projectId.toInt())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<DeleteNetworkResponse>>() {
                                override fun onComplete() {
                                    Timber.d("$TAG Completed Delete Network Api")
                                }

                                override fun onNext(response: Response<DeleteNetworkResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            if (response.body()!!.status.equals("success", true)) {
                                                deleteNetworkFromLocalDb(networkId)
                                            } else {
                                                if (response.body()?.description != null) {
                                                    if (response.body()?.description.equals("Network contains devices", true)) {
                                                        responseObservable.onNext(Event(EventType.DELETE_NETWORK, EventStatus.FAILURE, CONTAINS_DEVICES, null))
                                                        return
                                                    }
                                                }
                                                Timber.d("$TAG Something went wrong while deleting network in server")
                                                var errorMessage = getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.d(errorMessage)
                                                } else {
                                                    Timber.d("$TAG No Error message received from server")
                                                }
                                                responseObservable.onNext(Event(EventType.DELETE_NETWORK, EventStatus.FAILURE, null, null))
                                            }

                                        } else {
                                            Timber.d("$TAG No response body received from server")
                                        }
                                    } else {
                                        Timber.d("$TAG Something went wrong while deleting network in server")
                                        var errorMessage = getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.d(errorMessage)
                                        } else {
                                            Timber.d("$TAG No Error message received from server")
                                        }
                                        responseObservable.onNext(Event(EventType.DELETE_NETWORK, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    error.printStackTrace()
                                    Timber.d("$TAG Something went wrong while editing new network in server")
                                    responseObservable.onNext(Event(EventType.DELETE_NETWORK, EventStatus.FAILURE, null, null))
                                }
                            })
            )
        } else {
            responseObservable.onNext(Event(EventType.DELETE_NETWORK, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun deleteNetworkFromLocalDb(networkId: Long) {
        application.wifiLocalDatasource.deleteNetwork(networkId)
        Timber.d("$TAG Successfully deleted network($networkId) in local db")
        responseObservable.onNext(Event(EventType.DELETE_NETWORK, EventStatus.SUCCESS, null, null))
    }
}