package com.svarochi.wifi.ui.diagnostics.viewmodel

import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.ui.diagnostics.repository.DiagnosticsRepository
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

class DiagnosticsViewModel(var repository: DiagnosticsRepository) : BaseViewModel() {

    private var disposables: CompositeDisposable = CompositeDisposable()
    var response: MutableLiveData<Event> = MutableLiveData()
    private var TAG = "DiagnosticsViewModel"

    init {
        initResponseObserver(disposables, repository, response)
        Timber.d("$TAG - Observing DiagnosticsRespository response")
    }

    override fun onCleared() {
        Timber.d("$TAG - Disposing observers")
        disposables.dispose()
        disposables = CompositeDisposable()
        repository.dispose()
    }

    fun stopUdpBroadcast() {
        Timber.d("$TAG - Requesting to stop UDP broadcast")
        repository.stopUdpBroadcast()
    }

    fun startUdpBroadcast(macId: String) {
        Timber.d("$TAG - Requesting to start UDP broadcast")
        repository.startUdpBroadcast(macId)
    }

    fun checkIfDeviceIsAvailableViaMqtt(macId: String) {
        Timber.d("$TAG - Requesting to check if device is available via MQTT")
        repository.checkIfDeviceIsAvailableViaMqtt(macId)
    }

}