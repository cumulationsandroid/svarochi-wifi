package com.svarochi.wifi.ui.switchprofile.repository

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.Profile
import com.svarochi.wifi.database.entity.ProfileProject
import com.svarochi.wifi.model.api.response.ApiProfile
import com.svarochi.wifi.model.api.response.GetUserProfilesResponse
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class SwitchProfileRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl {

    private val responseObservable = PublishSubject.create<Event>()
    private var disposables = CompositeDisposable()
    private var sharedWithUserId: String = ""
    private var sharedPreference: SharedPreference? = null
    private var TAG = "SwitchProfileRepository ---->"

    init {
        fetchFromPreference()
        callGetUserProfilesApi()
    }

    private fun callGetUserProfilesApi() {
        disposables.add(
                ApiRequestParser.getUserProfiles(sharedWithUserId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetUserProfilesResponse>>() {
                            override fun onComplete() {
                                Timber.d("$TAG Completed get user profiles api")
                            }

                            override fun onNext(response: Response<GetUserProfilesResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("$TAG Get User Profiles API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            if (response.body()!!.records != null) {
                                                Timber.d("$TAG Successfully received user profiles from server")
                                                storeProfilesInLocalDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("$TAG No user profiles received from server")
                                            }
                                        }
                                    } else {
                                        Timber.d("$TAG Something went wrong while fetching user profiles from server")
                                        Timber.d("$TAG No response got for user profiles from server")

                                    }
                                } else {
                                    Timber.d("$TAG Something went wrong while fetching user profiles from server")
                                    Timber.d("$TAG Response code - ${response.code()}")
                                    Timber.d("$TAG Response body - ${response.body()}")
                                    Timber.d("$TAG Response errorBody - ${response.errorBody()}")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.d("$TAG Response error message - $errorMessage")
                                    }

                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("$TAG Something went wrong while fetching user profiles from server")
                                Timber.e(error)
                                error.printStackTrace()
                            }

                        })
        )
    }

    private fun storeProfilesInLocalDb(apiProfileList: ArrayList<ApiProfile>) {
        /**
         * 1. Clear all the profiles which are shared with $sharedWithUserId
         * 2. Add all the profiles in db
         * 3. Since profiles are removed, the corresponding ProfileProject table will be cleared
         * 4. Add the new profileProjects into ProfileProject table
         * */
        Timber.d("$TAG Cleared all the profiles shared with $sharedWithUserId")
        application.wifiLocalDatasource.clearProfilesSharedWith(sharedWithUserId)

        for (apiProfile in apiProfileList) {
            var profileToAdd = Profile(apiProfile.profile_id, apiProfile.profile_name, apiProfile.shared_by, apiProfile.shared_with_data.user_id, Converters.toString(apiProfile.shared_with_data))
            application.wifiLocalDatasource.addNewProfile(profileToAdd)
            Timber.d("$TAG Added profiles into profiles table")

            var profileProjectList = ArrayList<ProfileProject>()

            for (sharingProject in apiProfile.projects) {
                for (sharingNetwork in sharingProject.networks) {
                    var profileProjectToAdd = ProfileProject()

                    profileProjectToAdd.profileId = apiProfile.profile_id
                    profileProjectToAdd.projectId = sharingProject.project_id
                    profileProjectToAdd.networkId = sharingNetwork

                    profileProjectList.add(profileProjectToAdd)
                }
            }

            application.wifiLocalDatasource.addProfileProjects(profileProjectList)
            Timber.d("$TAG Added ProfileProjects into ProfileProjects table")

            Timber.d("$TAG Successfully added user profiles into local db")
        }
    }

    private fun fetchFromPreference() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        sharedWithUserId = sharedPreference!!.getUserId()
    }

    override fun initCommunicationResponseObserver() {
        // No implementation as not communicating with the devices
    }

    override fun getCommunicationResponseObserver(): PublishSubject<Event> = responseObservable

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    fun getSharedWithProfilesLiveData(): LiveData<List<Profile>> {
        return application.wifiLocalDatasource.getProfilesSharedWith(sharedWithUserId)
    }
}