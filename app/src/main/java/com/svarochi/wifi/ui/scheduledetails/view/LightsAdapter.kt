package com.svarochi.wifi.ui.scheduledetails.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants.Companion.LIGHT_SYNCED
import com.svarochi.wifi.model.common.CustomLight

class LightsAdapter(var lightList: ArrayList<CustomLight>, var context: ScheduleActivity) :
        androidx.recyclerview.widget.RecyclerView.Adapter<LightsAdapter.LightViewHolder>() {

    val selectedIds = ArrayList<String>()
    val selectedLightNames = ArrayList<String>()
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): LightViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_new_group_light, viewGroup, false)
        return LightViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lightList.size
    }

    override fun onBindViewHolder(viewHolder: LightViewHolder, position: Int) {
        viewHolder.name.text = lightList[position].light.name
        viewHolder.checkBox.isChecked = lightList[position].isSelected()

        if (lightList[position].isSelected()) {
            if (selectedIds.contains(lightList[position].light.macId).not()) {
                selectedIds.add(lightList[position].light.macId)
                selectedLightNames.add(lightList[position].light.name)
            }
        } else {
            if (selectedIds.contains(lightList[position].light.macId)) {
                selectedIds.remove(lightList[position].light.macId)
                selectedLightNames.remove(lightList[position].light.name)
            }
        }
    }

    inner class LightViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.tv_lightName)
        var checkBox: CheckBox = itemView.findViewById(R.id.cb_selectLightCheckBox)
        var setLightButton: Button = itemView.findViewById(R.id.bt_setLight)


        init {
            setLightButton.visibility = View.GONE
            checkBox.setOnClickListener {
                var position = adapterPosition

                if (lightList[position].light.isSynced == LIGHT_SYNCED) {
                    lightList[position].setSelected(checkBox.isChecked)

                    if (lightList[position].isSelected()) {
                        if (selectedIds.contains(lightList[position].light.macId).not()) {
                            selectedIds.add(lightList[position].light.macId)
                            selectedLightNames.add(lightList[position].light.name)
                        }
                    } else {
                        if (selectedIds.contains(lightList[position].light.macId)) {
                            selectedIds.remove(lightList[position].light.macId)
                            selectedLightNames.remove(lightList[position].light.name)
                        }
                    }
                } else {
                    context.showToast(context.getString(R.string.toast_light_unsynced_schedule))
                    checkBox.isChecked = checkBox.isChecked.not()
                }

            }
        }
    }
}