package com.svarochi.wifi.ui.switchprofile.view

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.database.entity.Profile
import com.svarochi.wifi.model.common.CustomProfile
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.switchprofile.viewmodel.SwitchProfileViewModel
import kotlinx.android.synthetic.main.activity_wifi_switch_profile.*
import timber.log.Timber

class SwitchProfileActivity : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener {
    override fun onClick(clickedView: View?) {
        if (clickedView != null) {
            when (clickedView.id) {
                iv_back.id -> {
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                }
            }
        } else {
            Timber.d("$TAG Clicked view is null")
        }
    }

    override fun serviceConnected() {
        init()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wifi_switch_profile)
        requestToBindService(this)
    }

    private val currentProfileId by lazy {
        preference.getCurrentProfile()
    }
    private var viewModel: SwitchProfileViewModel? = null
    private var TAG = "SwitchProfileActivity ---->"
    private val preference by lazy {
        SharedPreference(this)
    }
    private var profilesList = ArrayList<CustomProfile>()

    private fun init() {
        initViewModel()
        initViews()
        observeSharedWithProfiles()
        initClickListeners()
    }

    private fun initViews() {
        var selfProfile = CustomProfile(Profile(CURRENT_PROFILE_SELF, "Self", "", "",""))
        profilesList.add(selfProfile)
        rv_profiles.layoutManager = LinearLayoutManager(this)
        rv_profiles.adapter = ProfileAdapter(this, profilesList)
    }

    private fun initClickListeners() {
        iv_back.setOnClickListener(this)
    }

    private fun observeSharedWithProfiles() {
        if (viewModel != null) {
            viewModel!!.getSharedWithProfiles().observe(this, Observer {
                if (it != null) {
                    updateProfilesRv(it)
                } else {
                    Timber.d("$TAG User profiles is null")
                }
            })
        } else {
            Timber.d("$TAG View model is null")
        }
    }

    private fun updateProfilesRv(userProfiles: List<Profile>) {
        profilesList.clear()
        var selfProfile = CustomProfile(Profile(CURRENT_PROFILE_SELF, "Self", "", "",""))
        profilesList.add(selfProfile)
        for (profile in userProfiles) {
            profilesList.add(CustomProfile(profile))
        }
        (rv_profiles.adapter as ProfileAdapter).setProfileSelected(currentProfileId)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory).get(SwitchProfileViewModel::class.java)
    }

    fun selectProfile(profileId: String, profileName: String) {
        if (currentProfileId.equals(profileId).not()) {
            saveProfileInPreference(profileId)
            Timber.d("$TAG $profileName($profileId) is selected")
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }

    private fun saveProfileInPreference(profileId: String) {
        preference.setCurrentProfile(profileId)
    }

}
