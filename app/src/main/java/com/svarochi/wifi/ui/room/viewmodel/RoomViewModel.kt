package com.svarochi.wifi.ui.room.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.database.entity.Scene
import com.svarochi.wifi.database.entity.SceneLight
import com.svarochi.wifi.model.api.common.CustomSceneDevice
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.room.GroupsImpl
import com.svarochi.wifi.ui.room.LightsImpl
import com.svarochi.wifi.ui.room.ScenesImpl
import com.svarochi.wifi.ui.room.repository.RoomRepository
import io.reactivex.disposables.CompositeDisposable

class RoomViewModel(var roomRepository: RoomRepository) : BaseViewModel(),
        LightsImpl, ScenesImpl, GroupsImpl {

    private var disposables = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()
    private var isObservingResponse = false

    init {
        // Observe light list of roomRepository
        initResponseObserver()
        response.value = Event(EventType.INIT, EventStatus.DEFAULT, null, null)
    }

    override fun onCleared() {
        isObservingResponse = false
        disposables.dispose()
        disposables = CompositeDisposable()
//        roomRepository.stopBroadcast()
        //roomRepository.disconnectClients()
    }

    private fun initResponseObserver() {
        if (!isObservingResponse) {
            isObservingResponse = true
            initResponseObserver(disposables, roomRepository, response)
        }
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    fun startBroadcast() {
        //response.postValue(Event(EventType.UDP_BROADCAST, EventStatus.LOADING, null, null))
        roomRepository.startBroadcast()
    }

    fun stopBroadcast() {
        roomRepository.stopBroadcast()
    }

    fun initObservers() {
        roomRepository.initObservers()
        initResponseObserver()
    }

    /*Lights*/
    override fun getLights(networkId: Long) {
        response.postValue(Event(EventType.GET_LIGHTS, EventStatus.LOADING, null, null))
        roomRepository.getLights(networkId)
    }

    override fun setWifiCredentials(
            lampName: String,
            lampSsid: String,
            ssidToConfigure: String,
            password: String,
            authenticationMode: String,
            encryptionType: String
    ) {
        response.postValue(Event(EventType.SET_WIFI, EventStatus.LOADING, null, null))
        roomRepository.setWifiCredentials(lampName, lampSsid, ssidToConfigure, password, authenticationMode, encryptionType)
    }

    override fun turnOnLight(macId: String?) {
        response.postValue(Event(EventType.TURN_ON_LIGHT, EventStatus.LOADING, null, null))
        roomRepository.turnOnLight(macId)
    }

    override fun turnOffLight(macId: String?) {
        response.postValue(Event(EventType.TURN_OFF_LIGHT, EventStatus.LOADING, null, null))
        roomRepository.turnOffLight(macId)
    }

    /*Scenes*/
    override fun getScenes(networkId: Long) {
        response.value = Event(EventType.GET_SCENES, EventStatus.LOADING, null, null)
        roomRepository.getScenes(networkId)
    }

    override fun addNewScene(name: String, lightList: List<CustomSceneDevice>) {
        response.value = Event(EventType.ADD_NEW_SCENE, EventStatus.LOADING, null, null)
        roomRepository.addNewScene(name, lightList)
    }

    override fun checkIfSceneExists(name: String) {
        response.value = Event(EventType.CHECK_IF_SCENE_EXISTS, EventStatus.LOADING, null, null)
        roomRepository.checkIfSceneExists(name)
    }

    override fun editSceneName(sceneId: Long, name: String) {
        response.value = Event(EventType.EDIT_SCENE_NAME, EventStatus.LOADING, null, null)
        roomRepository.editSceneName(sceneId, name)
    }

    override fun turnOnScene(sceneId: Long) {
        response.value = Event(EventType.TURN_ON_SCENE, EventStatus.LOADING, null, null)
        roomRepository.turnOnScene(sceneId)
    }

    override fun turnOffScene(sceneId: Long) {
        response.value = Event(EventType.TURN_OFF_SCENE, EventStatus.LOADING, null, null)
        roomRepository.turnOffScene(sceneId)
    }

    fun getAllLightsOfNetwork(networkId: Long): List<CustomLight> {
        return roomRepository.getAllLightsOfNetwork(networkId)
    }

    /*Used only while editing scene*/
    override fun getLightsAndMarkThoseOfScene(networkId: Long, sceneId: Long) {
        response.value = Event(EventType.GET_LIGHTS_OF_SCENE, EventStatus.LOADING, null, null)
        roomRepository.getLightsAndMarkThoseOfScene(networkId, sceneId)
    }

    override fun editSceneLightsList(sceneId: Long, lightList: List<CustomSceneDevice>) {
        response.value = Event(EventType.EDIT_LIGHTS_OF_SCENE, EventStatus.LOADING, null, null)
        roomRepository.editSceneLightsList(sceneId, lightList)
    }

    fun getSceneLightsOfScene(sceneId: Long): List<SceneLight> {
        return roomRepository.getSceneLightsOfScene(sceneId)
    }

    /*Groups*/
    override fun getGroups(networkId: Long) {
        response.value = Event(EventType.GET_GROUPS, EventStatus.LOADING, null, null)
        roomRepository.getGroups(networkId)
    }

    override fun addNewGroup(name: String, lightList: ArrayList<Light>) {
        response.value = Event(EventType.ADD_NEW_GROUP, EventStatus.LOADING, null, null)
        roomRepository.addNewGroup(name, lightList)
    }

    override fun checkIfGroupExists(name: String, networkId: Long) {
        response.value = Event(EventType.CHECK_IF_GROUP_EXISTS, EventStatus.LOADING, null, null)
        roomRepository.checkIfGroupExists(name, networkId)
    }

    override fun editGroupName(groupId: Long, name: String) {
        response.value = Event(EventType.EDIT_GROUP_NAME, EventStatus.LOADING, null, null)
        roomRepository.editGroupName(groupId, name)
    }

    override fun turnOnGroup(groupId: Long) {
//        response.value = Event(EventType.TURN_ON_GROUP, EventStatus.LOADING, null, null)
        roomRepository.turnOnGroup(groupId)
    }

    override fun turnOffGroup(groupId: Long) {
//        response.value = Event(EventType.TURN_OFF_GROUP, EventStatus.LOADING, null, null)
        roomRepository.turnOffGroup(groupId)
    }

    override fun getLightsAndMarkThoseOfGroup(networkId: Long, groupId: Long) {
        response.value = Event(EventType.GET_LIGHTS_OF_GROUP, EventStatus.LOADING, null, null)
        roomRepository.getLightsAndMarkThoseOfGroup(networkId, groupId)
    }

    override fun editGroupLightsList(groupId: Long, lightList: ArrayList<String>) {
        response.value = Event(EventType.EDIT_LIGHTS_OF_GROUP, EventStatus.LOADING, null, null)
        roomRepository.editGroupLightsList(groupId, lightList)
    }

    override fun addLightToGroup(macId: String, groupId: Long) {
        response.value = Event(EventType.ADD_LIGHT_TO_GROUP, EventStatus.LOADING, null, null)
        roomRepository.addLightToGroup(macId, groupId)
    }

    override fun removeLightFromGroup(macId: String, groupId: Long) {
        response.value = Event(EventType.REMOVE_LIGHT_FROM_GROUP, EventStatus.LOADING, null, null)
        roomRepository.removeLightFromGroup(macId, groupId)
    }

    fun getLightsLiveData(networkId: Long, projectId: Long): LiveData<List<Light>> {
        return roomRepository.getLightsLiveData(networkId, projectId)
    }

    fun getGroupsLiveData(networkId: Long, projectId: Long): LiveData<List<Group>> {
        return roomRepository.getGroupsLiveData(networkId, projectId)
    }

    fun getScenesLiveData(networkId: Long, projectId: Long): LiveData<List<Scene>> {
        return roomRepository.getScenesLiveData(networkId, projectId)
    }

    fun deleteGroup(groupId: Long) {
        response.value = Event(EventType.DELETE_GROUP, EventStatus.LOADING, null, null)
        roomRepository.deleteGroup(groupId)
    }

    fun deleteScene(sceneId: Long) {
        response.value = Event(EventType.DELETE_SCENE, EventStatus.LOADING, null, null)
        roomRepository.deleteScene(sceneId)
    }

    fun isGroupContainingLights(groupId: Long): Boolean {
        return roomRepository.isGroupContainingLights(groupId)
    }

    fun isSceneContainingLights(sceneid: Long): Boolean {
        return roomRepository.isSceneContainingLights(sceneid)
    }

    fun editLightName(macId: String, newName: String) {
        response.value = Event(EventType.EDIT_LIGHT_NAME, EventStatus.LOADING, null, null)

        roomRepository.editLightName(macId, newName)
    }

    fun deleteLamp(macId: String) {
        response.value = Event(EventType.DELETE_LAMP, EventStatus.LOADING, null, null)
        roomRepository.deleteLamp(macId)
    }

    fun deleteUnsyncedLamp(macId: String) {
        response.value = Event(EventType.DELETE_LAMP, EventStatus.LOADING, null, null)
        roomRepository.deleteUnSyncedLamp(macId)
    }

}
