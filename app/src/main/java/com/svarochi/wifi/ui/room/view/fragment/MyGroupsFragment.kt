package com.svarochi.wifi.ui.room.view.fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_GROUP_ID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_GROUP_NAMES
import com.svarochi.wifi.common.Constants.Companion.MAX_GROUPS
import com.svarochi.wifi.common.Constants.Companion.MAX_LIGHTS_IN_GROUP
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.groupcontrol.view.GroupControlActivity
import com.svarochi.wifi.ui.room.view.activity.RoomActivity
import com.svarochi.wifi.ui.room.view.adapter.GroupsAdapter
import com.svarochi.wifi.ui.room.view.adapter.GroupsLightsAdapter
import kotlinx.android.synthetic.main.fragment_my_groups.*
import timber.log.Timber

class MyGroupsFragment : androidx.fragment.app.Fragment() {

    private var isSelfProfile: Boolean = true
    private var networkId: Long = 0
    private var projectId: Long = 0
    private var groupsDialog: Dialog? = null
    private lateinit var activityContext: RoomActivity
    private var dialogLightList: ArrayList<CustomLight>? = null
    private val GROUP_CONTROL: Int = 0
    private var groupsList: ArrayList<Group> = ArrayList()
    private var isObservingGroupsTable = false


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_my_groups, container, false)

        initViews()

        return view
    }

    private fun initViews() {
        if (activityContext != null) {
            if (activityContext!!.network == null) {
                Timber.d("MyGroups Activity's network variable is null")
                activityContext!!.initBundleData()
            }
            networkId = activityContext!!.network!!.id
            projectId = activityContext!!.network!!.projectId
            isSelfProfile = activityContext.isSelfProfile
            groupsDialog = Dialog(activityContext)
            groupsDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            groupsDialog!!.setCancelable(true)
            groupsDialog!!.setCanceledOnTouchOutside(false)
            groupsDialog!!.setContentView(R.layout.dialog_add_new_scene)
            groupsDialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

            observeGroupsTable()
        } else {
            Timber.d("Activity context is null")
        }
    }

    private fun observeGroupsTable() {
        if (isObservingGroupsTable) {
            activityContext!!.viewModel!!.getGroupsLiveData(networkId, projectId).removeObservers(this)
        }
        if (activityContext != null) {
            if (activityContext!!.viewModel == null) {
                activityContext.initViewModel()
            }
            isObservingGroupsTable = true
            Timber.d("Observing all the groups of network($networkId)")
            activityContext!!.viewModel!!.getGroupsLiveData(networkId, projectId).observe(this, Observer<List<Group>> {
                if (it != null) {
                    Timber.d("Change in groups of network($networkId)")
                    Utilities.sortGroups(it)
                    groupsCount = it.size
                    updateGroupsRv(it)
                } else {
                    Timber.d("No groups got for network($networkId)")
                }
            })
        }
    }

    private fun showPopup(show: Boolean, message: String?) {
        if (activityContext != null) {
            activityContext.showLoadingPopup(show, message)
        } else {
            Timber.d("ActivityContext not initialised")
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activityContext = context as RoomActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initGroupsRv()
    }

    companion object {
        @JvmStatic
        fun newInstance(): MyGroupsFragment = MyGroupsFragment()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (!isVisibleToUser) {
            try {
                removeObservers()
            } catch (e: Exception) {
            }
        } else {
            try {
                //getGroups()
                initObservers()
            } catch (e: Exception) {
            }
        }
    }

    private fun getGroups() {
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.getGroups(networkId)
        }
    }

    private fun checkIfGroupNameExists(name: String) {
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.checkIfGroupExists(name, networkId)
        }
    }

    private var responseObserver = Observer<Event> { event -> processResponse(event) }

    private var groupsCount: Int = 0

    private fun processResponse(event: Event?) {
        when (event!!.type) {

            EventType.CONNECTION -> {
                when (event.status) {
                    EventStatus.FAILURE -> {
                        var macId = event.data as String
                        showPopup(false, null)
                        //activityContext.showToast(lightName + getString(R.string.toast_is_not_reachable))
                        Timber.d("Connection lost with device - ${macId}")
                    }
                }
            }

            EventType.GET_GROUPS -> {
                when (event.status) {
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.LOADING -> {
                        Timber.d("Fetching groups of network - $networkId")
                        showPopup(true, getString(R.string.popup_fetching_groups))
                    }
                    EventStatus.SUCCESS -> {
                        showPopup(false, null)
                        Timber.d("Got ${(event.data!! as List<Group>).size} groups of network - $networkId")

                        /*var responseGroupsList = ArrayList((event.data!! as List<Group>))

                        groupsCount = responseGroupsList.size

                        updateGroupsRv(responseGroupsList)*/
                    }
                    EventStatus.ERROR -> {
                        showPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_to_fetch_groups))
                        Timber.e(event.error)
                    }
                }
            }
            EventType.ADD_NEW_GROUP -> {
                when (event.status) {
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.LOADING -> {
                        Timber.d("Adding new group")
                        showPopup(true, getString(R.string.popup_creating_new_group))
                    }
                    EventStatus.SUCCESS -> {
                        showPopup(false, null)
                        Timber.d("Added new group")
                        groupsDialog!!.dismiss()
                        //getGroups()
                    }
                    EventStatus.ERROR -> {
                        showPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_to_add_new_group))
                        Timber.e(event.error)
                    }
                    EventStatus.FAILURE -> {
                        showPopup(false, null)
                        if (event.data != null) {
                            if ((event.data as Int) == Constants.ALREADY_EXISTS) {
                                Timber.d("Group by that already exists")
                                activityContext.showToast(
                                        getString(R.string.toast_group_by_that_name_already_exists)
                                )
                            } else if ((event.data as Int) == Constants.NO_INTERNET_AVAILABLE) {
                                activityContext.showToast(
                                        getString(R.string.no_internet)
                                )
                            } else {
                                groupsDialog!!.dismiss()
                                activityContext.showToast(getString(R.string.toast_failed_to_add_new_group))
                            }
                        } else {
                            groupsDialog!!.dismiss()
                            activityContext.showToast(getString(R.string.toast_failed_to_add_new_group))
                        }
                    }
                }
            }
            EventType.CHECK_IF_GROUP_EXISTS -> {
                when (event.status) {

                    EventStatus.SUCCESS -> {
                        if ((event.data is Int)) {
                            Timber.d("Group by that already exists")
                            activityContext.showToast(getString(R.string.toast_group_by_that_name_already_exists))
                        } else if ((event.data is String)) {
                            showSelectLightsDialog((event.data as String), null)
                        }
                    }
                }
            }

            EventType.GET_LIGHTS -> {
                when (event.status) {
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.LOADING -> {
                        Timber.d("Fetching lights of network - $networkId")
                        //showPopup(true, getString(R.string.popup_fetching_lights))
                    }
                    EventStatus.SUCCESS -> {
                        showPopup(false, null)
                        var responseLightsList = (event.data!! as ArrayList<CustomLight>)
                        Timber.d("Got ${responseLightsList.size} lights of network - $networkId")

                        initLightsRv(responseLightsList)
                    }
                    EventStatus.ERROR -> {
                        showPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_to_fetch_lights))
                        Timber.e(event.error)
                    }
                }
            }
            EventType.TURN_ON_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning on group")
                        showPopup(true, getString(R.string.popup_turning_on_group))
                    }
                    EventStatus.SUCCESS -> {
                        var group = event.data as Group
                        Timber.d("Successfully turned on group(${group.name})")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_successfully_turned_on_group) + group.name)
//                        updateGroup(group)

                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                        Timber.d("Failed to turn on group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_failed_to_turn_on_group))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn on group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_failed_to_turn_on_group))
                    }
                }
            }
            EventType.TURN_OFF_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning off group")
                        showPopup(true, getString(R.string.popup_turning_off_group))
                    }
                    EventStatus.SUCCESS -> {
                        var group = event.data as Group
                        Timber.d("Successfully turned off group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_successfully_turned_off_group) + group.name)
//                        updateGroup(group)
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                        Timber.d("Failed to turn off group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_failed_to_turn_off_group))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn off group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_failed_to_turn_off_group))
                    }
                }
            }
            EventType.GET_LIGHTS_OF_GROUP -> {
                when (event.status) {
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.LOADING -> {
                        Timber.d("Fetching lights of group")
                        showPopup(true, getString(R.string.popup_fetching_lights))
                    }
                    EventStatus.SUCCESS -> {
                        showPopup(false, null)
                        Timber.d("Got ${(event.data!! as List<CustomLight>).size} lights of network - $networkId")

                        var responseLightsList =
                                (event.data!! as ArrayList<CustomLight>)

                        initEditGroupLightsRv(responseLightsList)
                    }
                    EventStatus.ERROR -> {
                        showPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_to_fetch_lights))
                        Timber.e(event.error)
                    }
                }
            }
            EventType.EDIT_LIGHTS_OF_GROUP -> {
                when (event.status) {

                    EventStatus.LOADING -> {
                        Timber.d("Editing group")
                        showPopup(true, getString(R.string.popup_editing_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully updated group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_successfully_edited_group))
                        groupsDialog!!.dismiss()
                        //getGroups()
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to edit lights of group")
                        showPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_edit_group))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to edit lights of group")
                        showPopup(false, null)

                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    activityContext.showToast(getString(R.string.no_internet))
                                }
                            }
                        } else {
                            activityContext.showToast(getString(R.string.toast_failed_edit_group))
                        }
                    }
                }
            }
            EventType.ADD_LIGHT_TO_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Adding light to group")
                        showPopup(true, getString(R.string.popup_adding_light_to_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully added light to group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_successfully_added_light_to_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to add light to group")
                        showPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_add_light_to_group))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to add light to group")
                        showPopup(false, null)
                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    activityContext.showToast(getString(R.string.no_internet))
                                }
                            }
                        } else {
                            activityContext.showToast(getString(R.string.toast_failed_add_light_to_group))
                        }
                    }
                }
            }
            EventType.REMOVE_LIGHT_FROM_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Removing light from group")
                        showPopup(true, getString(R.string.popup_removing_light_from_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully removed light from group")
                        showPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_successfully_removed_light_from_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to remove light from group")
                        showPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_remove_light_from_group))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to remove light from group")
                        showPopup(false, null)

                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    activityContext.showToast(getString(R.string.no_internet))
                                }
                            }
                        } else {
                            activityContext.showToast(getString(R.string.toast_failed_remove_light_from_group))
                        }
                    }
                }
            }

            EventType.DELETE_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Deleting group")
                        activityContext.showLoadingPopup(true, getString(R.string.dialog_deleting_group))
                    }
                    EventStatus.SUCCESS -> {
                        activityContext.showLoadingPopup(false, null)
                        Timber.d("Successfully deleted the group")
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to delete group")
                        activityContext.showLoadingPopup(false, null)

                        if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                            activityContext.showToast(getString(R.string.no_internet))
                        } else {
                            activityContext.showToast(getString(R.string.toast_failed_to_delete_the_group))
                        }

                    }
                }
            }
        }
    }

    private fun updateGroupsRv(responseGroupsList: List<Group>) {
        groupsList.clear()
        for (group in responseGroupsList) {
            groupsList.add(group)
        }

        if (groupsList.size < MAX_GROUPS && isSelfProfile) {
            groupsList.add(Group(-1, "", -1, -1))
        }
        if (rv_groupsRv.adapter != null) {
            rv_groupsRv.adapter?.notifyDataSetChanged()
        } else {
            Timber.d("Adapter not set for groups rv")
        }
    }

    fun showAddGroupDialog() {
        if (groupsCount < Constants.MAX_GROUPS) {
            if (activityContext.lightsCount > 0) {
                groupsDialog!!.setContentView(R.layout.dialog_add_new_group)
                val addButton: Button = groupsDialog!!.findViewById(R.id.bt_addButton)
                val cancelButton: Button = groupsDialog!!.findViewById(R.id.bt_cancelButton)
                val nameField: EditText = groupsDialog!!.findViewById(R.id.et_groupName)

                nameField.requestFocus()
                addButton.setOnClickListener {

                    if (nameField.text.trim().isNotEmpty()) {
                        checkIfGroupNameExists(nameField.text.toString())
                    } else {
                        activityContext.showToast(getString(R.string.toast_error_please_enter_name))
                    }
                }

                cancelButton.setOnClickListener {
                    groupsDialog!!.dismiss()
                }
                groupsDialog!!.show()
            } else {
                activityContext.showToast(
                        getString(R.string.toast_need_to_add_atleast_one_light_before_creating_group)
                )
            }
        } else {
            activityContext.showToast(getString(R.string.toast_maximum_groups_added))
        }
    }

    fun turnOnGroup(groupId: Long) {
        activityContext.viewModel!!.turnOnGroup(groupId)
    }

    fun turnOffGroup(groupId: Long) {
        activityContext.viewModel!!.turnOffGroup(groupId)
    }

    fun showSelectLightsDialog(groupName: String?, groupId: Long?) {
        groupsDialog!!.setContentView(R.layout.dialog_add_new_group_lights)
        val addButton: Button = groupsDialog!!.findViewById(R.id.bt_addButton)
        val cancelButton: Button = groupsDialog!!.findViewById(R.id.bt_cancelButton)
        val groupNameView: TextView = groupsDialog!!.findViewById(R.id.tv_groupName)

        if (groupName != null) {
            groupNameView.text = groupName
        } else {
            groupNameView.text = getString(R.string.all_lights)
        }

        groupsDialog!!.show()

        if (groupId == null) {
            // If group id is not provided that means we have to create a new group
            addButton.setOnClickListener {
                createGroup(groupName!!)
            }

            cancelButton.setOnClickListener {
                groupsDialog!!.dismiss()
            }
            if (activityContext.viewModel != null) {
                activityContext.viewModel!!.getLights(networkId)
            }
        } else {
            // If group id is provided that means we already have a group which needs to be edited
            addButton.setOnClickListener {
                editGroupLights(groupId)
            }

            cancelButton.setOnClickListener {
                groupsDialog!!.dismiss()
            }
            if (activityContext.viewModel != null) {
                activityContext.viewModel!!.getLightsAndMarkThoseOfGroup(networkId, groupId)
            }
        }
    }

    private fun editGroupLights(groupId: Long) {
        var lightList = ArrayList<String>()
        for (customLight in dialogLightList!!) {
            if (customLight.isSelected()) {

                lightList.add(customLight.light.macId)
            }
        }
        Timber.d("Selected number of lights - ${lightList.size}")
        if (lightList.isNotEmpty()) {
            if (lightList.size <= MAX_LIGHTS_IN_GROUP) {
                if (activityContext.viewModel != null) {
                    activityContext.viewModel!!.editGroupLightsList(groupId, lightList)
                } else {
                    Timber.d("View model is not initialised")
                }
            } else {
                showToast("Cannot add more than $MAX_LIGHTS_IN_GROUP to a group")
            }
        } else if (lightList.isEmpty()) {
            activityContext.showToast(getString(R.string.toast_atleast_one_light_needs_to_selected))
        }
    }

    fun editGroupLightsList(groupName:String , groupId: Long) {
        showSelectLightsDialog(groupName, groupId)
    }

    private fun createGroup(groupName: String) {
        var lightList = ArrayList<Light>()
        for (customLight in dialogLightList!!) {
            if (customLight.isSelected()) {
                lightList.add(customLight.light)
            }
        }
        Timber.d("Selected number of lights - ${lightList.size}")
        if (lightList.isNotEmpty()) {
            if (lightList.size <= MAX_LIGHTS_IN_GROUP) {

                if (activityContext.viewModel != null) {
                    activityContext.viewModel!!.addNewGroup(groupName, lightList)
                } else {
                    Timber.d("View model is not initialised")
                }
            } else {
                Timber.d("View model is not initialised")
                showToast("Cannot add more than $MAX_LIGHTS_IN_GROUP to a group")
            }
        } else {
            activityContext.showToast(getString(R.string.toast_atleast_one_light_needs_to_selected))
        }
    }

    private fun initEditGroupLightsRv(lightsList: ArrayList<CustomLight>) {
        var lightsRv: androidx.recyclerview.widget.RecyclerView = groupsDialog!!.findViewById(R.id.rv_lightsRv)
        lightsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)

        dialogLightList = lightsList
        lightsRv.adapter = GroupsLightsAdapter(dialogLightList!!, this)
    }

    private fun initLightsRv(lightsList: ArrayList<CustomLight>) {
        var lightsRv: androidx.recyclerview.widget.RecyclerView = groupsDialog!!.findViewById(R.id.rv_lightsRv)
        lightsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)

        dialogLightList = lightsList
        lightsRv.adapter = GroupsLightsAdapter(dialogLightList!!, this)
    }

    private fun initGroupsRv() {
        rv_groupsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activityContext)
        rv_groupsRv.adapter = GroupsAdapter(groupsList, isSelfProfile, this)
    }


    private fun initObservers() {
        Timber.d("GroupsFragment - Initialising observers")
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.getResponse().observe(this, responseObserver)
        } else {
            Timber.d("View model is not initialised")
        }
    }

    private fun removeObservers() {
        Timber.d("GroupsFragment - Removing observers")
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.getResponse().removeObserver(responseObserver)
        } else {
            Timber.d("View model is not initialised")
        }
    }

    fun openGroupControlScreen(groupId: Long) {
        var groupNames = getGroupNames(groupId)
        startActivity(
                Intent(activityContext, GroupControlActivity::class.java)
                        .putExtra(BUNDLE_GROUP_ID, groupId)
                        .putExtra(BUNDLE_GROUP_NAMES, groupNames)
        )
    }

    private fun getGroupNames(groupId: Long): ArrayList<String> {
        var groupNames = ArrayList<String>()
        groupsList.forEach {
            if (it.id != groupId) {
                groupNames.add(it.name)
            }
        }
        return groupNames
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        when (requestCode) {
//            GROUP_CONTROL -> {
//                if (resultCode == RESULT_OK) {
//                    updateGroup((data!!.getSerializableExtra(BUNDLE_GROUP) as Group))
//                }
//            }
//        }
    }

    private fun updateGroup(updatedGroup: Group) {
        (rv_groupsRv.adapter as GroupsAdapter).updateGroup(updatedGroup)
    }

    private fun deleteGroup(groupId: Long) {
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.deleteGroup(groupId)
        } else {
            Timber.d("ViewModel is null")
        }
    }

    fun showDeleteGroupConfirmationDialog(groupId: Long): Boolean {
        if (activityContext.dialog != null) {
            activityContext.dialog!!.setContentView(R.layout.dialog_delete_group_confirmation)
            var cancelButton = activityContext.dialog!!.findViewById<Button>(R.id.bt_cancelButton)
            var okButton = activityContext.dialog!!.findViewById<Button>(R.id.bt_okButton)
            activityContext.dialog!!.show()

            cancelButton.setOnClickListener {
                activityContext.dialog!!.dismiss()
            }

            okButton.setOnClickListener {
                activityContext.dialog!!.dismiss()
                deleteGroup(groupId)
            }
        }
        return true
    }

    fun isGroupContainingLights(groupId: Long): Boolean {
        if (activityContext != null && activityContext.viewModel != null) {
            return activityContext.viewModel!!.isGroupContainingLights(groupId)
        } else {
            Timber.d("Activity context is null")
            return false
        }
    }

    fun showToast(message: String) {
        if (activityContext != null && activityContext.viewModel != null) {
            activityContext.showToast(message)
        } else {
            Timber.d("Activity context is null")
        }
    }
}
