package com.svarochi.wifi.ui.audio.viewmodel

import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.audio.AudioImpl
import com.svarochi.wifi.ui.audio.repository.AudioRepository
import io.reactivex.disposables.CompositeDisposable

class AudioViewModel(var repository: AudioRepository) : BaseViewModel(), AudioImpl {
    private var disposables = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()
    private var isObservingResponse = false

    fun getResponse() = response

    init {
        initResponseObserver()
    }

    override fun getLights(networkId: Long) {
        response.value = Event(EventType.GET_LIGHTS, EventStatus.LOADING, null, null)
        repository.getLights(networkId)
    }

    private fun initResponseObserver() {
        if (!isObservingResponse) {
            isObservingResponse = true
            initResponseObserver(disposables, repository, response)
        }
    }

    override fun onCleared() {
        isObservingResponse = false
        disposables.dispose()
        disposables = CompositeDisposable()
        repository.dispose()
        resetAudioFlag()
    }

    override fun getGroups(networkId: Long) {
        response.value = Event(EventType.GET_GROUPS, EventStatus.LOADING, null, null)
        repository.getGroups(networkId)
    }

    fun setLightRgbWithBrightness(macId: String, r_value: Int, g_value: Int, b_value: Int, brightnessValue: Int) {
        repository.setLightRgbWithBrightness(macId, r_value, g_value, b_value, brightnessValue)
    }

    fun setGroupRgbWithBrightness(groupId: Long, r_value: Int, g_value: Int, b_value: Int, brightnessValue: Int) {
        repository.setGroupRgbWithBrightness(groupId, r_value, g_value, b_value, brightnessValue)
    }

    fun getDevicesOfGroup(groupId: Long) {
        repository.getDevicesOfGroup(groupId)
    }

    fun resetAudioFlag() {
        repository.resetAudioFlag()
    }

    fun setAudioFlag() {
        repository.setAudioFlag()
    }
}