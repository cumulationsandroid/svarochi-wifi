package com.svarochi.wifi.ui.setscenelight

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel

import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.communication.SceneType
import io.reactivex.disposables.CompositeDisposable

class SetSceneLightViewModel(private var repository: SetSceneLightRepository) : BaseViewModel() {
    private var disposables: CompositeDisposable = CompositeDisposable()
    var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }

    override fun onCleared() {
        disposables.clear()
    }

    fun setDaylight(macId: String, value: Int) {
        repository.setDaylight(macId, value)
    }

    fun setLampBrightness(macId: String, value: Int) {
        repository.setLampBrightness(macId, value)
    }

    fun turnOffLight(macId: String) {
        repository.turnOffLight(macId)
    }

    fun turnOnLight(macId: String) {
        repository.turnOnLight(macId)
    }

    fun getLight(macId: String): LiveData<Light> {
        return repository.getLight(macId)
    }


    fun turnOffDynamicScene(macId: String) {
        repository.turnOffDynamicScene(macId)
    }

    fun setLampColor(
            macId: String,
            r_value: String,
            g_value: String,
            b_value: String
    ) {
        repository.setLampColor(macId, r_value, g_value, b_value)
    }

    fun setScene(macId: String, sceneType: SceneType) {
        repository.setScene(macId, sceneType)
    }

}