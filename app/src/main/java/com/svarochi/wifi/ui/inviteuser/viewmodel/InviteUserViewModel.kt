package com.svarochi.wifi.ui.inviteuser.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.database.entity.ProfileProject
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.model.api.common.SharedWithData
import com.svarochi.wifi.model.common.SharingProject
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.inviteuser.repository.InviteUserRepository
import io.reactivex.disposables.CompositeDisposable

class InviteUserViewModel(var repository: InviteUserRepository) : BaseViewModel() {


    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }


    override fun onCleared() {
        disposables.clear()
        repository.dispose()
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    fun getAllProjects(): LiveData<List<Project>> {
        return repository.getAllProjects()
    }

    fun getNetworksOfProject(projectId: Long): List<Network> {
        return repository.getNetworksOfProject(projectId)
    }

    fun getProjectNetworksOfProfile(profileId: String): LiveData<List<ProfileProject>> {
        return repository.getProfileProjectsOfProfile(profileId)
    }

    fun updateProfile(profileId: String, profileName: String, sharedWithData: SharedWithData, sharedBy: String, projectsList: ArrayList<SharingProject>) {
        response.value = Event(EventType.UPDATE_PROFILE, EventStatus.LOADING, null, null)
        repository.updateProfile(profileId, profileName, sharedWithData, sharedBy, projectsList)
    }

    fun createProfile(profileName: String, sharedWithData: SharedWithData, projectsList: ArrayList<SharingProject>) {
        response.value = Event(EventType.CREATE_PROFILE, EventStatus.LOADING, null, null)
        repository.createProfile(profileName, sharedWithData, projectsList)
    }
}