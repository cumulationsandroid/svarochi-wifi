package com.svarochi.wifi.ui.inviteuser.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.model.common.CustomNetwork
import com.svarochi.wifi.model.common.CustomProject


class SharingProfileAdapter(var context: InviteUserScreen, var childNetworksList: ArrayList<ArrayList<CustomNetwork>>, var groupProjectNames: ArrayList<CustomProject>)
    : BaseExpandableListAdapter() {

    override fun getGroup(groupPosition: Int): Any {
        return groupProjectNames.get(groupPosition)
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return childNetworksList.get(groupPosition).size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return childNetworksList.get(groupPosition).get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return groupProjectNames.size
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_sharing_profile_network, parent, false)

        }
        var networkName = view!!.findViewById(R.id.tv_networkName) as TextView
        var networkSelectBox = view.findViewById(R.id.cb_networkCheckbox) as CheckBox
        networkName.text = (getChild(groupPosition, childPosition) as CustomNetwork).network.name

        networkSelectBox.isChecked = childNetworksList.get(groupPosition).get(childPosition).selected

        networkSelectBox.setOnClickListener {
            childNetworksList.get(groupPosition).get(childPosition).selected = childNetworksList.get(groupPosition).get(childPosition).selected.not()
        }

        return view
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_sharing_profile_project, parent, false)

        }
        var projectName = view!!.findViewById(R.id.tv_projectName) as TextView
        var arrow = view!!.findViewById(R.id.iv_projectArrow) as ImageView
        projectName.text = groupProjectNames[groupPosition].projectName

        if (groupProjectNames[groupPosition].isExpanded) {
            arrow.setImageResource(R.drawable.ic_arrow_up)
        } else {
            arrow.setImageResource(R.drawable.ic_arrow_down)
        }

        return view
    }

    fun setGroupExpanded(groupPosition: Int) {
        groupProjectNames[groupPosition].isExpanded = true
        notifyDataSetChanged()
    }

    fun setGroupCollapsed(groupPosition: Int) {
        groupProjectNames[groupPosition].isExpanded = false
        notifyDataSetChanged()
    }
}