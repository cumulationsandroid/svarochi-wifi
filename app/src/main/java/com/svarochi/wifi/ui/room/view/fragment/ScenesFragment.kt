package com.svarochi.wifi.ui.room.view.fragment


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.*
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_CUSTOM_SCENE_DEVICE
import com.svarochi.wifi.common.Constants.Companion.MAX_LIGHTS_IN_SCENE
import com.svarochi.wifi.common.Constants.Companion.MAX_SCENES
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Scene
import com.svarochi.wifi.database.entity.SceneLight
import com.svarochi.wifi.model.api.common.CustomSceneDevice
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.room.view.activity.RoomActivity
import com.svarochi.wifi.ui.room.view.adapter.SceneLightsAdapter
import com.svarochi.wifi.ui.room.view.adapter.ScenesAdapter
import com.svarochi.wifi.ui.setscenelight.SetSceneLightActivity
import kotlinx.android.synthetic.main.fragment_wifi_scenes.*
import timber.log.Timber

class ScenesFragment : androidx.fragment.app.Fragment() {

    private var isSelfProfile: Boolean = true
    private lateinit var activityContext: RoomActivity
    private var networkId: Long = 0
    private var projectId: Long = 0
    private val SET_SCENE_LIGHT = 123
    private var scenesCount = 0
    private var sceneDialog: Dialog? = null
    private var sceneLightsAdapter: SceneLightsAdapter? = null
    private var scenesList = ArrayList<Scene>()
    private var allLights: List<CustomLight>? = null
    private var lightNameMap = HashMap<String, String>()
    private var lightPosition = 0

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_wifi_scenes, container, false)

        initViews()

        return view
    }


    private fun initObservers() {
        Timber.d("SceneFragment - Initialising observers")
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.getResponse().observe(this, responseObserver)
        } else {
            Timber.d("View model is not initialised")
        }
    }

    private fun removeObservers() {
        Timber.d("SceneFragment - Removing observers")
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.getResponse().removeObserver(responseObserver)
        } else {
            Timber.d("View model is not initialised")
        }
    }

    private var responseObserver = Observer<Event> { event -> processResponse(event) }

    private fun processResponse(event: Event?) {
        when (event!!.type) {
            EventType.ADD_NEW_SCENE -> {
                when (event.status) {
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.LOADING -> {
                        Timber.d("Adding new scene")
                        activityContext?.showLoadingPopup(true, getString(R.string.popup_creating_new_scene))
                    }
                    EventStatus.SUCCESS -> {
                        activityContext?.showLoadingPopup(false, null)
                        Timber.d("Added new scene")
                        sceneDialog!!.dismiss()
                        //getScenes()
                    }
                    EventStatus.ERROR -> {
                        activityContext?.showLoadingPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_to_add_new_scene))
                        Timber.e(event.error)
                    }
                    EventStatus.FAILURE -> {
                        activityContext?.showLoadingPopup(false, null)
                        if (event.data != null) {
                            if ((event.data as Int) == ALREADY_EXISTS) {
                                Timber.d("Scene by that already exists")
                                activityContext.showToast(getString(R.string.toast_scene_by_that_name_already_exists))
                            } else if ((event.data as Int) == NO_INTERNET_AVAILABLE) {
                                activityContext.showToast(getString(R.string.no_internet))
                            } else {
                                Timber.d("Failed to add new scene")
                                activityContext.showToast(getString(R.string.toast_failed_to_add_new_scene))
                                sceneDialog!!.dismiss()
                            }
                        } else {
                            activityContext?.showLoadingPopup(false, null)
                            activityContext.showToast(getString(R.string.toast_failed_to_add_new_scene))
                            sceneDialog!!.dismiss()
                        }
                    }
                }
            }

            EventType.EDIT_LIGHTS_OF_SCENE -> {
                when (event.status) {

                    EventStatus.LOADING -> {
                        Timber.d("Editing scene")
                        activityContext?.showLoadingPopup(true, getString(R.string.popup_editing_scene))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully updated scene")
                        activityContext?.showLoadingPopup(false, null)
                        //activityContext.showToast(getString(R.string.toast_successfully_edited_scenes))
                        sceneDialog!!.dismiss()
//                       getScenes()
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to edit lights of scene")
                        activityContext?.showLoadingPopup(false, null)
                        activityContext.showToast(getString(R.string.toast_failed_edit_scene))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to edit lights of scene")
                        activityContext?.showLoadingPopup(false, null)

                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    Timber.d("No internet available")
                                    activityContext.showToast(getString(R.string.no_internet))
                                }
                            }
                        } else {
                            activityContext.showToast(getString(R.string.toast_failed_edit_scene))
                        }
                    }
                }
            }

            EventType.DELETE_SCENE -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Deleting scene")
                        activityContext.showLoadingPopup(true, getString(R.string.dialog_deleting_scene))
                    }
                    EventStatus.SUCCESS -> {
                        activityContext.showLoadingPopup(false, null)
                        Timber.d("Successfully deleted the scene")
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to delete scene")
                        activityContext.showLoadingPopup(false, null)

                        if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                            activityContext.showToast(getString(R.string.no_internet))
                        } else {
                            activityContext.showToast(getString(R.string.toast_failed_to_delete_the_scene))
                        }

                    }
                }
            }
            EventType.EDIT_SCENE_NAME -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Editing scene name")
                        activityContext.showLoadingPopup(true, getString(R.string.popup_editing_scene))
                    }
                    EventStatus.SUCCESS -> {
                        activityContext.showLoadingPopup(false, null)
                        Timber.d("Successfully edited scene name")
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to edit scene name")
                        activityContext.showLoadingPopup(false, null)
                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == NO_INTERNET_AVAILABLE) {
                                    activityContext.showToast(getString(R.string.no_internet))
                                }
                            }
                        } else {
                            activityContext.showToast(getString(R.string.toast_failed_to_edit_scene))
                        }
                    }
                }
            }
        }
    }

    private fun initEditSceneLightsRv(lightsList: List<SceneLight>) {
        var customSceneDeviceList = ArrayList<CustomSceneDevice>()
        val sceneLightMap = getSceneWithLightMap(lightsList)
        if (allLights == null) {
            allLights = getAllLights()
            initLightNameMap()
        }

        allLights!!.forEach {
            if (sceneLightMap.containsKey(it.light.macId)) {
                // Mark this - take scene light states
                val sceneLight = sceneLightMap[it.light.macId]!!
                val lightName = getLightName(sceneLight.macId)

                val customSceneDevice = CustomSceneDevice(
                        Utilities.convertHexStringToInt(sceneLight.bValue),
                        sceneLight.brightnessValue,
                        Utilities.getDaylightValue(sceneLight.cValue),
                        Utilities.convertHexStringToInt(sceneLight.gValue),
                        sceneLight.macId,
                        Utilities.convertHexStringToInt(sceneLight.rValue),
                        Utilities.getLampOnOffStatus(sceneLight.onOffValue),
                        Utilities.convertHexStringToInt(sceneLight.sceneValue),
                        lightName,
                        true, Constants.LIGHT_SYNCED
                )
                customSceneDeviceList.add(customSceneDevice)

            } else {
                // UnMark this - set lamp states

                val lightName = getLightName(it.light.macId)

                val customSceneDevice = CustomSceneDevice(
                        Utilities.convertHexStringToInt(it.light.bValue),
                        it.light.brightnessValue,
                        Utilities.getDaylightValue(it.light.cValue),
                        Utilities.convertHexStringToInt(it.light.gValue),
                        it.light.macId,
                        Utilities.convertHexStringToInt(it.light.rValue),
                        Utilities.getLampOnOffStatus(it.light.onOffValue),
                        Utilities.convertHexStringToInt(it.light.sceneValue),
                        lightName,
                        false, Constants.LIGHT_SYNCED
                )
                customSceneDeviceList.add(customSceneDevice)
            }
        }

        var lightsRv: RecyclerView = sceneDialog!!.findViewById(R.id.rv_lightsRv)
        lightsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)

        sceneLightsAdapter = SceneLightsAdapter(customSceneDeviceList, this)
        lightsRv.adapter = sceneLightsAdapter
    }

    private fun getSceneWithLightMap(lightsList: List<SceneLight>): HashMap<String, SceneLight> {
        val map = HashMap<String, SceneLight>()
        lightsList.forEach {
            map[it.macId] = it
        }
        return map
    }


    private fun getLightName(macId: String): String {
        if (activityContext.viewModel == null) {
            activityContext.initViewModel()
        }
        if (allLights == null) {
            allLights = getAllLights()
            initLightNameMap()
        }
        if (lightNameMap.containsKey(macId)) {
            return lightNameMap[macId]!!
        }
        return macId
    }

    private fun initLightNameMap() {
        if (allLights == null) {
            allLights = getAllLights()
        }
        lightNameMap.clear()

        for (customLight in allLights!!) {
            lightNameMap.put(customLight.light.macId, customLight.light.name)
        }
    }

    private fun updateScenesRv(responseScenesList: List<Scene>) {
        scenesList.clear()
        for (scene in responseScenesList) {
            scenesList.add(scene)
        }

        if (scenesList.size < MAX_SCENES && isSelfProfile) {
            scenesList.add(Scene(-1, "", -1))
        }

        if (rv_scenesRv.adapter != null) {
            rv_scenesRv.adapter!!.notifyDataSetChanged()
        } else {
            Timber.d("Scenes adapter not set")
        }
    }

    private fun initLightsRv(lightsList: List<CustomLight>) {
        var customSceneDeviceList = ArrayList<CustomSceneDevice>()
        lightsList.forEach {
            val customSceneDevice = CustomSceneDevice(
                    0,
                    100,
                    100,
                    0,
                    it.light.macId,
                    0,
                    "ON",
                    Utilities.convertHexStringToInt(it.light.sceneValue),
                    it.light.name,
                    false, it.light.isSynced
            )
            customSceneDeviceList.add(customSceneDevice)
        }

        var lightsRv: RecyclerView = sceneDialog!!.findViewById(R.id.rv_lightsRv)
        lightsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)

        sceneLightsAdapter = SceneLightsAdapter(customSceneDeviceList, this)
        lightsRv.adapter = sceneLightsAdapter
    }

    private fun initScenesRv() {
        rv_scenesRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        rv_scenesRv.adapter = ScenesAdapter(scenesList, isSelfProfile, this)
    }


    private fun initViews() {
        if (activityContext != null) {
            if (activityContext!!.network == null) {
                Timber.d("MyScenes Activity's network variable is null")
                activityContext!!.initBundleData()
            }
            networkId = activityContext!!.network!!.id
            projectId = activityContext!!.network!!.projectId
            isSelfProfile = activityContext.isSelfProfile
            sceneDialog = Dialog(activityContext)
            sceneDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            sceneDialog!!.setCancelable(true)
            sceneDialog!!.setCanceledOnTouchOutside(false)
            sceneDialog!!.setContentView(R.layout.dialog_add_new_scene)
            sceneDialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

            observeScenesTable()
        } else {
            Timber.d("Activity context is null")
        }
    }

    private fun observeScenesTable() {
        Timber.d("Observing all the scenes of network($networkId)")
        activityContext!!.viewModel!!.getScenesLiveData(networkId, projectId).removeObservers(this)
        activityContext!!.viewModel!!.getScenesLiveData(networkId, projectId).observe(this, Observer<List<Scene>> {
            if (it != null) {
                Timber.d("Change in scenes of network($networkId)")
                Utilities.sortScenes(it)
                scenesCount = it.size
                updateScenesRv(it)
            } else {
                Timber.d("No scenes got for network($networkId)")
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initScenesRv()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activityContext = context as RoomActivity
    }

    fun editSceneName(sceneId: Long, oldName: String) {
        // Show dialog
        if (sceneDialog != null) {

            sceneDialog!!.setContentView(R.layout.dialog_edit_scene_name)
            sceneDialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

            val okButton: Button = sceneDialog!!.findViewById(R.id.bt_okButton)
            val cancelButton: Button = sceneDialog!!.findViewById(R.id.bt_cancelButton)
            val nameField: EditText = sceneDialog!!.findViewById(R.id.et_name)

            nameField.text = Editable.Factory.getInstance().newEditable(oldName)
            nameField.requestFocus()

            okButton.setOnClickListener {

                if (nameField.text.trim().isNotEmpty()) {
                    if (isSceneNameAlreadyExisting(nameField.text.toString(), sceneId).not()) {
                        sceneDialog!!.dismiss()
                        editSceneNameInServer(sceneId, nameField.text.toString())
                    } else {
                        Timber.d("Scene name already exists")
                        activityContext.showToast(getString(R.string.toast_scene_by_that_name_already_exists))
                    }
                } else {
                    activityContext.showToast(getString(R.string.toast_error_please_enter_name))
                }
            }

            cancelButton.setOnClickListener {
                sceneDialog!!.dismiss()
            }
            sceneDialog!!.show()

        } else {
            // Should not occur
            Timber.d("Scene dialog not initialised")
        }

    }

    private fun editSceneNameInServer(sceneId: Long, newName: String) {
        if (activityContext != null) {
            if (activityContext!!.viewModel != null) {

                activityContext!!.viewModel!!.editSceneName(sceneId, newName)
            } else {
                // Should not occur
                Timber.d("Activity context view model is null")
            }
        } else {
            // Should not occur
            Timber.d("Activity context is null")
        }
    }

    private fun isSceneNameAlreadyExisting(newName: String, sceneId: Long?): Boolean {
        if (rv_scenesRv.adapter != null) {
            for (scene in (rv_scenesRv.adapter as ScenesAdapter).scenesList) {
                if (sceneId != null) {
                    if (scene.name.equals(newName) && scene.id != sceneId) {
                        return true
                    }
                } else {
                    if (scene.name.equals(newName)) {
                        return true
                    }
                }
            }
            return false
        } else {
            // Should not occur
            Timber.d("Scenes adapter not initialised")
            return true
        }
    }

    fun editSceneLights(sceneId: Long?) {
        // Show dialog
        showSelectLightsDialog(null, sceneId)
    }

    fun turnOnScene(sceneId: Long?) {
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.turnOnScene(sceneId!!)
        } else {
            Timber.d("View model is not initialised")
        }
    }

    fun turnOffScene(sceneId: Long) {
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.turnOffScene(sceneId)
        } else {
            Timber.d("View model is not initialised")
        }
    }

    fun showAddSceneDialog() {
        if (scenesCount < MAX_SCENES) {
            if (activityContext!!.lightsCount > 0) {

                sceneDialog!!.setContentView(R.layout.dialog_add_new_scene)

                val addButton: Button = sceneDialog!!.findViewById(R.id.bt_addButton)
                val cancelButton: Button = sceneDialog!!.findViewById(R.id.bt_cancelButton)
                val nameField: EditText = sceneDialog!!.findViewById(R.id.et_sceneName)
                nameField.requestFocus()
                addButton.setOnClickListener {

                    if (nameField.text.isNotEmpty()) {
                        if (isSceneNameAlreadyExisting(nameField.text.toString(), null).not()) {
                            showSelectLightsDialog(nameField.text.toString(), null)
                        } else {
                            activityContext.showToast(getString(R.string.toast_scene_by_that_name_already_exists))
                        }
                    } else {
                        activityContext.showToast(getString(R.string.toast_error_please_enter_name))
                    }
                }

                cancelButton.setOnClickListener {
                    sceneDialog!!.dismiss()
                }
                sceneDialog!!.show()
            } else {
                activityContext.showToast(
                        getString(R.string.toast_need_to_add_atleast_one_light_before_creating_scene)
                )
            }
        } else {
            activityContext.showToast(getString(R.string.toast_maximum_scenes_added))
        }
    }


    private fun showSelectLightsDialog(sceneName: String?, sceneId: Long?) {
        sceneDialog!!.setContentView(R.layout.dialog_add_new_scene_lights)
        val addButton: Button = sceneDialog!!.findViewById(R.id.bt_addButton)
        val cancelButton: Button = sceneDialog!!.findViewById(R.id.bt_cancelButton)
        val lightsRv: RecyclerView = sceneDialog!!.findViewById(R.id.rv_lightsRv)
        sceneDialog!!.show()

        if (sceneId == null) {
            // If scene id is not provided that means we have to create a new scene
            addButton.setOnClickListener {
                val selectedLights = (lightsRv.adapter as SceneLightsAdapter).getSelectedLights()
                createScene(sceneName!!, selectedLights)
            }

            cancelButton.setOnClickListener {
                sceneDialog!!.setContentView(R.layout.dialog_add_new_scene)
                sceneDialog!!.dismiss()
            }
            if (allLights == null) {
                allLights = getAllLights()
                initLightNameMap()
            }
            initLightsRv(allLights!!)
        } else {
            // If scene id is provided that means we already have a scene which needs to be edited
            addButton.setOnClickListener {
                val selectedLights = (lightsRv.adapter as SceneLightsAdapter).getSelectedLights()
                editScene(sceneId, selectedLights)
            }

            cancelButton.setOnClickListener {
                sceneDialog!!.dismiss()
            }
            if (activityContext.viewModel != null) {
                val sceneLights = activityContext.viewModel!!.getSceneLightsOfScene(sceneId)
                initEditSceneLightsRv(sceneLights)
            } else {
                Timber.d("View model is not initialised")
            }
        }
    }

    private fun editScene(sceneId: Long, lightsList: List<CustomSceneDevice>) {

        Timber.d("Selected number of lights - ${lightsList.size}")
        if (lightsList.isNotEmpty()) {
            if (lightsList.size <= MAX_LIGHTS_IN_SCENE) {
                if (activityContext.viewModel != null) {
                    activityContext.viewModel!!.editSceneLightsList(sceneId, lightsList)
                } else {
                    Timber.d("View model is not initialised")
                }
            } else {
                showToast("Cannot add more than $MAX_LIGHTS_IN_SCENE to a scene")
            }

        } else {
            activityContext.showToast(getString(R.string.toast_atleast_one_light_needs_to_selected))
        }
    }

    private fun createScene(sceneName: String, lightsList: List<CustomSceneDevice>) {
        Timber.d("Selected number of lights - ${lightsList.size}")
        if (lightsList.isNotEmpty()) {
            if (lightsList.size <= MAX_LIGHTS_IN_SCENE) {
                if (activityContext.viewModel != null) {
                    sceneDialog?.dismiss()
                    activityContext.viewModel!!.addNewScene(sceneName, lightsList)
                } else {
                    Timber.d("View model is not initialised")
                }
            } else {
                showToast("Cannot add more than $MAX_LIGHTS_IN_SCENE to a scene")
            }
        } else {
            activityContext.showToast(getString(R.string.toast_atleast_one_light_needs_to_selected))
        }
    }

    fun openSetSceneLightScreen(sceneDevice: CustomSceneDevice, position: Int) {
        Timber.d("Opening light control screen of light - ${sceneDevice.mac_id}")
        lightPosition = position

        startActivityForResult(
                Intent(activity, SetSceneLightActivity::class.java)
                        .putExtra(BUNDLE_CUSTOM_SCENE_DEVICE, sceneDevice),
                SET_SCENE_LIGHT
        )
    }


    companion object {
        @JvmStatic
        fun newInstance() = ScenesFragment()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (!isVisibleToUser) {
            try {
                removeObservers()
            } catch (e: Exception) {
            }
        } else {
            try {
                //getScenes()
                initObservers()
            } catch (e: Exception) {
            }
        }
    }

    private fun deleteScene(sceneId: Long) {
        if (activityContext.viewModel != null) {
            activityContext.viewModel!!.deleteScene(sceneId)
        } else {
            Timber.d("ViewModel is null")
        }
    }

    fun showDeleteSceneConfirmationDialog(sceneId: Long): Boolean {
        if (activityContext.dialog != null) {
            activityContext.dialog!!.setContentView(R.layout.dialog_delete_scene_confirmation)
            var cancelButton = activityContext.dialog!!.findViewById<Button>(R.id.bt_cancelButton)
            var okButton = activityContext.dialog!!.findViewById<Button>(R.id.bt_okButton)
            activityContext.dialog!!.show()

            cancelButton.setOnClickListener {
                activityContext.dialog!!.dismiss()
            }

            okButton.setOnClickListener {
                activityContext.dialog!!.dismiss()
                deleteScene(sceneId)
            }
        }
        return true
    }

    fun isSceneContainingLights(sceneId: Long): Boolean {
        if (activityContext != null && activityContext.viewModel != null) {
            return activityContext.viewModel!!.isSceneContainingLights(sceneId)
        } else {
            Timber.d("Activity context is null")
            return false
        }
    }

    fun showToast(message: String) {
        if (activityContext != null && activityContext.viewModel != null) {
            activityContext.showToast(message)
        } else {
            Timber.d("Activity context is null")
        }
    }

    private fun getAllLights(): List<CustomLight> {
        if (activityContext.viewModel == null) {
            activityContext.initViewModel()
        }

        return activityContext.viewModel?.getAllLightsOfNetwork(networkId)!!

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SET_SCENE_LIGHT && resultCode == Activity.RESULT_OK && data != null && data.extras != null) {
            val updatedSceneDevice = data.getSerializableExtra(BUNDLE_CUSTOM_SCENE_DEVICE) as CustomSceneDevice
            updateSceneDevice(updatedSceneDevice)
        } else {
            lightPosition = 0
        }
    }

    private fun updateSceneDevice(updatedSceneDevice: CustomSceneDevice) {
        if (sceneDialog != null) {
            val lightsRv: RecyclerView = sceneDialog!!.findViewById(R.id.rv_lightsRv)
            (lightsRv.adapter as SceneLightsAdapter).updateSceneDevice(updatedSceneDevice, lightPosition)
        }
    }

}
