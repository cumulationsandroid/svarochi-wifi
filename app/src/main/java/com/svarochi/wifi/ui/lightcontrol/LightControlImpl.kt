package com.svarochi.wifi.ui.lightcontrol

import com.svarochi.wifi.model.communication.SceneType

interface LightControlImpl {

    fun setLampColor(
            macId: String,
            r_value: String,
            g_value: String,
            b_value: String
    )

    fun setScene(macId: String, sceneType: SceneType)

    fun turnOnLight(macId: String)

    fun turnOffLight(macId: String)

    fun setLampBrightness(
            macId: String,
            brightnessValue: Int
    )

    fun setDaylight(
            macId: String,
            daylightValue: Int
    )

    fun editLightName(
            macId: String,
            newName: String
    )

    fun deleteLamp(macId: String)


}