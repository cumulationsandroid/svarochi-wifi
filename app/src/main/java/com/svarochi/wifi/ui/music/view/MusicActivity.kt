package com.svarochi.wifi.ui.music.view

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.audio.music.MusicVisualizer
import com.svarochi.wifi.audio.music.callback.MusicCallback
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SELECTED_IDS
import com.svarochi.wifi.common.Constants.Companion.RECORD_PERMISSION
import com.svarochi.wifi.common.Utilities.Companion.MilliSecondsToTimerHMS
import com.svarochi.wifi.model.common.Song
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.ui.music.viewmodel.MusicViewModel
import kotlinx.android.synthetic.main.activity_music.*
import timber.log.Timber


class MusicActivity : WifiBaseActivity(), View.OnClickListener, MusicCallback, ServiceConnectionCallback {
    override fun serviceConnected() {
        init()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            iv_back.id -> {
                closeActivity()
            }

            ib_play.id -> {
                playPause()
            }

            ib_previous.id -> {
                playPreviousSong()
            }

            ib_next.id -> {
                playNextSong()
            }
        }
    }

    private fun playNextSong() {
        if (isConnectedToInternet) {
            if (currentSongPosition < songsList.size - 1) {
                currentSongPosition += 1
            } else {
                currentSongPosition = 0
            }
            playSong(currentSongPosition)
        } else {
            showToast(getString(R.string.toast_no_network_connection))
        }
    }

    private fun playPreviousSong() {
        if (isConnectedToInternet) {
            if (currentSongPosition > 0) {
                currentSongPosition -= 1
                playSong(currentSongPosition)
            }
        } else {
            showToast(getString(R.string.toast_no_network_connection))
        }
    }

    private fun playPause() {
        if (isConnectedToInternet) {
            if (mediaPlayer.isPlaying) {
                pauseSong()
            } else {
                if (currentSongPosition != -1) {
                    playPreviouslyPausedSong()
                } else {
                    currentSongPosition = 0
                    playSong(currentSongPosition)
                }
            }
        } else {
            showToast(getString(R.string.toast_no_network_connection))
        }
    }

    private fun playPreviouslyPausedSong() {
        mediaPlayer.start()
        setVisualizer()
        ib_play.setImageResource(R.drawable.ic_wifi_pause)
    }

    private fun pauseSong() {
        releaseVisualizer()
        mediaPlayer.pause()
        ib_play.setImageResource(R.drawable.ic_play)
    }

    private fun closeActivity() {
        finish()
    }

    private var songsList = ArrayList<Song>()
    private lateinit var viewModel: MusicViewModel
    private lateinit var connectivityManager: ConnectivityManager
    private var selectedIds = ArrayList<Any>()
    private var mediaPlayer: MediaPlayer = MediaPlayer()
    private var isConnectedToInternet: Boolean = false
    private var isNetworkChangeRegistered: Boolean = false
    private var currentSongPosition = -1
    private var visualizer = MusicVisualizer()

    private var mediaPlayerCallback = object : MediaPlayer.OnCompletionListener {
        override fun onCompletion(mp: MediaPlayer?) {
            if (currentSongPosition < songsList.size - 1) {
                currentSongPosition += 1
                playSong(currentSongPosition)
            } else {
                // play first song
                playSong(0)
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)
        requestToBindService(this)
    }

    private fun init() {
        initViewModelAndObserver()
        initViews()
        initClickListeners()
        setAudioFlag()
    }

    private fun setAudioFlag() {
        viewModel.setAudioFlag()
    }

    private fun reqisterNetworkCallback() {
        if (isNetworkChangeRegistered.not()) {
            isNetworkChangeRegistered = true
            var builder = NetworkRequest.Builder().build()
            connectivityManager.registerNetworkCallback(builder, networkChangeCallback)
            Timber.d("Network change callback registered")
        }
    }

    override fun onStart() {
        super.onStart()
        reqisterNetworkCallback()
    }


    private var networkChangeCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            Timber.d("OnAvailable")
            isConnectedToInternet = true
        }

        override fun onLost(network: Network?) {
            isConnectedToInternet = false
            stopSong()
        }
    }


    private fun initViews() {
        connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        rv_songs.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        rv_songs.adapter = MusicListAdapter(this)
        selectedIds = intent.getSerializableExtra(BUNDLE_SELECTED_IDS) as ArrayList<Any>
        getSongsFromDevice()
    }

    private fun initClickListeners() {
        iv_back.setOnClickListener(this)
        ib_play.setOnClickListener(this)
        ib_previous.setOnClickListener(this)
        ib_next.setOnClickListener(this)
    }

    private fun initViewModelAndObserver() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(MusicViewModel::class.java)
        viewModel.getResponse().observe(this, Observer { t -> processResponse(t) })
    }

    private fun processResponse(event: Event?) {

    }

    fun getSongsFromDevice() {
        if ((ContextCompat.checkSelfPermission(
                        this@MusicActivity,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED)
        ) {
            Timber.d("No read external storage permission")
            Timber.d("SDK Version - ${Build.VERSION.SDK_INT}")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Timber.d("Requesting read external storage permission")
                requestPermissions(
                        arrayOf(
                                android.Manifest.permission.READ_EXTERNAL_STORAGE
                        ), Constants.READ_EXTERNAL_STORAGE_PERMISSION
                )
            } else {
                Timber.d("Permission not granted")
            }

        } else {
            val selection =
                    MediaStore.Audio.Media.IS_MUSIC + " != 0" + " AND " + MediaStore.Audio.Media.MIME_TYPE + "= 'audio/mpeg'"

            val projection = arrayOf(
                    MediaStore.Audio.Media._ID, // 0
                    MediaStore.Audio.Media.TITLE, // 1
                    MediaStore.Audio.Media.ARTIST, // 2
                    MediaStore.Audio.Media.DATA, // 3
                    MediaStore.Audio.Media.DISPLAY_NAME // 4
            )

            val musicResolver = contentResolver
            val musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            val musicCursor = musicResolver.query(musicUri, projection, selection, null, null)

            if (musicCursor != null && musicCursor.moveToFirst()) {
                do {
                    var id = musicCursor.getLong(0)
                    var path = musicCursor.getString(3)
                    var title = musicCursor.getString(4)
                    var artist = musicCursor.getString(2)

                    var song = Song(id, path, title, artist)

                    songsList.add(song)
                } while (musicCursor.moveToNext())

                addSongsToRv()
            }
            if (musicCursor != null)
                musicCursor.close()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            RECORD_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Timber.d("Record permission granted")
                    if (currentSongPosition == -1) {
                        currentSongPosition++
                    }
                    playSong(currentSongPosition)
                } else {
                    Timber.d("Record permission not granted")
                }
            }
        }
    }

    private fun addSongsToRv() {
        (rv_songs.adapter as MusicListAdapter).addSongs(songsList)
    }

    fun playSong(position: Int) {
        if (position != -1) {
            if (isConnectedToInternet) {
                currentSongPosition = position
                var song = getSongAtPosition(position)
                playSong(song)
            } else {
                showToast(getString(R.string.toast_no_network_connection))
            }
        }
    }

    private fun playSong(song: Song) {
        if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.RECORD_AUDIO
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            Timber.d("Record audio permission not granted")
            Timber.d("SDK Version - ${Build.VERSION.SDK_INT}")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Timber.d("Requesting audio permission")
                requestPermissions(
                        arrayOf(android.Manifest.permission.RECORD_AUDIO), Constants.RECORD_PERMISSION
                )
            } else {
                // Should not occur
                Timber.d("Record audio permission not granted")
            }
        } else {
            try {
                mediaPlayer.release()
                mediaPlayer = MediaPlayer()
                mediaPlayer.setDataSource(song.path)
                mediaPlayer.setOnCompletionListener(mediaPlayerCallback)
                mediaPlayer.setOnPreparedListener(MediaPlayer.OnPreparedListener {

                    // Starting music
                    mediaPlayer.start()

                    releaseVisualizer()
                    setVisualizer()

                    // Displaying Song title
                    tv_songName.text = song.title

                    // set Progress bar values
                    sb_songSeekBar.progress = 0

                    // Updating progress bar and time duration
                    getDurationTimer()
                    getSeekBarStatus()
                    mediaPlayer.setOnPreparedListener(null)

                    // Set Play image
                    ib_play.setImageResource(R.drawable.ic_wifi_pause)
                })
                mediaPlayer.prepareAsync()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getDurationTimer() {
        tv_songEndTime.text = MilliSecondsToTimerHMS(mediaPlayer.duration)
    }

    fun getSeekBarStatus() {
        Thread(Runnable {
            if (mediaPlayer == null || !mediaPlayer.isPlaying)
                return@Runnable
            var currentPosition = 0
            val total = mediaPlayer.getDuration()
            sb_songSeekBar.max = total
            while (mediaPlayer != null && currentPosition < total) {
                try {
                    Thread.sleep(100)
                    if (mediaPlayer == null)
                        return@Runnable
                    currentPosition = mediaPlayer.currentPosition
                } catch (e: Exception) {
                    return@Runnable
                }

                sb_songSeekBar.progress = currentPosition
            }
        }).start()


        sb_songSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, ProgressValue: Int, fromUser: Boolean) {
                if (fromUser) {
                    mediaPlayer.seekTo(ProgressValue)//if user drags the seekbar, it gets the position and updates in textView.
                }
                tv_songCurrentTime.text = MilliSecondsToTimerHMS(ProgressValue)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })

    }

    private fun getSongAtPosition(position: Int): Song {
        return songsList[position]
    }

    private fun setVisualizer() {
        if (mediaPlayer != null) {
            visualizer.setPlayer(mediaPlayer, mediaPlayer.audioSessionId)
            visualizer.setCallback(this)
            visualizer.setFFTVisualizer()
        }
    }

    override fun setRgbIntensity(rgb: IntArray, intensity: Int) {
        sendRgbValuesToDevices(rgb, intensity)
    }

    private fun sendRgbValuesToDevices(result: IntArray, intensity: Int) {
        for (device in selectedIds) {
            if (device is String) {
                // Selected id is a Mac id
                setLightRgb(device, result[0], result[1], result[2], intensity)

            } else if (device is Long) {
                // Selected id is a group id
                setGroupRgb(device, result[0], result[1], result[2], intensity)
            }
        }
    }

    private fun setGroupRgb(groupId: Long, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        if(brightness_value > 100){
            viewModel.setGroupRgbWithBrightness(groupId, r_value, g_value, b_value, 100)
        }else{
            viewModel.setGroupRgbWithBrightness(groupId, r_value, g_value, b_value, brightness_value)
        }
    }

    private fun setLightRgb(macId: String, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        if(brightness_value > 100){
            viewModel.setLightRgbWithBrightness(macId, r_value, g_value, b_value, 100)
        }else {
            viewModel.setLightRgbWithBrightness(macId, r_value, g_value, b_value, brightness_value)
        }
    }

    private fun releaseVisualizer() {
        if (visualizer != null) {
            visualizer.release()
        }
    }

    override fun onStop() {
        stopSong()
        super.onStop()
    }

    private fun stopSong() {
        if (mediaPlayer.isPlaying) {
            Timber.d("Stopping song")
            pauseSong()
        } else {
            Timber.d("No song is being played")
        }
    }

}
