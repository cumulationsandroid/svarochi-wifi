package com.svarochi.wifi.ui.setscenelight

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.database.LocalDataSource
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.Command
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.service.communication.CommunicationService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class SetSceneLightRepository(var application: SvarochiApplication) : BaseRepository() {

    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var communicationService: CommunicationService = application.wifiCommunicationService
    private var localDataSource: LocalDataSource = application.wifiLocalDatasource

    init {
        // Observe the responseObservable in service
        initCommunicationResponseObserver()
    }

    override fun initCommunicationResponseObserver() {
        communicationService.getResponseObservable()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Timber.d("Listening to responseObservable of service...")
                    disposables.add(it!!)
                }
                .subscribe(object : DisposableObserver<Event>() {
                    override fun onComplete() {
                    }

                    override fun onNext(event: Event) {
                        responseObservable.onNext(event)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                        //responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.ERROR, null, null))
                    }
                })
    }

    override fun getCommunicationResponseObserver() = responseObservable


    fun setDaylight(
            macId: String,
            daylightValue: Int
    ) {
        var command = Command(EventType.SET_LIGHT_DAYLIGHT, macId, daylightValue)
        communicationService.sendCommand(command)
    }

    fun setScene(macId: String, sceneType: SceneType) {
        var command = Command(EventType.SET_LIGHT_PRESET_SCENE, macId, sceneType)
        communicationService.sendCommand(command)
    }

    fun setLampColor(
            macId: String,
            r_value: String,
            g_value: String,
            b_value: String
    ) {
        var rgbPayload = HashMap<String, String>()
        rgbPayload[Constants.BUNDLE_R_VALUE] = r_value
        rgbPayload[Constants.BUNDLE_G_VALUE] = g_value
        rgbPayload[Constants.BUNDLE_B_VALUE] = b_value

        var command = Command(EventType.SET_LIGHT_RGB, macId, rgbPayload)
        communicationService.sendCommand(command)
    }

    fun turnOnLight(macId: String) {
        var command = Command(EventType.TURN_ON_LIGHT, macId, null)
        communicationService.sendCommand(command)
    }

    fun turnOffLight(macId: String) {
        var command = Command(EventType.TURN_OFF_LIGHT, macId, null)
        communicationService.sendCommand(command)
    }

    fun setLampBrightness(
            macId: String,
            brightnessValue: Int
    ) {
        var command = Command(EventType.SET_LIGHT_BRIGHTNESS, macId, brightnessValue)
        communicationService.sendCommand(command)
    }


    fun getLight(macId: String): LiveData<Light> {
        return localDataSource.getLightLiveData(macId)
    }


    fun turnOffDynamicScene(macId: String) {
        /**
         * To turn off a dynamic we have to turn on the lamp with previous saved RGBWC values
         * */
        setLightSceneToDefault(macId)
        turnOnLight(macId)
    }

    private fun setLightSceneToDefault(macId: String) {
        localDataSource.setLightSceneToDefault(macId)
    }
}