package com.svarochi.wifi.ui.userinvitations.view

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.Profile
import com.svarochi.wifi.model.api.common.SharedWithData
import com.svarochi.wifi.model.api.response.CheckUserResponse
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.inviteuser.view.InviteUserScreen
import com.svarochi.wifi.ui.userinvitations.viewmodel.UserInvitationsViewModel
import kotlinx.android.synthetic.main.activity_user_invitations.*
import timber.log.Timber
import java.io.Serializable

class UserInvitationsActivity : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener {
    private var TAG = "UserInvitationsActivity --->"
    private var viewModel: UserInvitationsViewModel? = null
    private var dialog: Dialog? = null
    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                iv_back.id -> {
                    finish()
                }
                fab_inviteNewUser.id -> {
                    inviteNewUser()
                }
            }
        } else {
            // Should not occur
            Timber.d("$TAG Clicked view is null")
        }
    }

    override fun serviceConnected() {
        init()
    }

    private fun init() {
        initViewModel()
        initViews()
        observeProfilesTable()
        initClickListeners()
    }

    private fun observeProfilesTable() {
        if (viewModel != null) {
            viewModel!!.getSharedProfilesLiveData().observe(this, Observer { t -> updateInvitationsList(t) })
        } else {
            // Should not occur
            Timber.d("$TAG View model is null")
        }
    }

    private fun updateInvitationsList(profiles: List<Profile>?) {
        if (profiles.isNullOrEmpty().not()) {
            tv_noInvitationsText.visibility = View.GONE
            rv_invitedUsers.visibility = View.VISIBLE

            val sharedProfiles = ArrayList<SharedProfile>()
            profiles!!.forEach {
                val sharedProfile = SharedProfile(it.id, it.profileName, it.sharedBy, Converters.toSharedWithData(it.sharedWithData))
                sharedProfiles.add(sharedProfile)
            }

            if (rv_invitedUsers.adapter != null) {
                (rv_invitedUsers.adapter as UserInvitationsAdapter).addProfiles(sharedProfiles)
            } else {
                rv_invitedUsers.adapter = UserInvitationsAdapter(this)
                (rv_invitedUsers.adapter as UserInvitationsAdapter).addProfiles(sharedProfiles)
            }
        } else {
            Timber.d("Received empty profile list from db")
            tv_noInvitationsText.visibility = View.VISIBLE
            rv_invitedUsers.visibility = View.GONE
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory).get(UserInvitationsViewModel::class.java)
        viewModel!!.getResponse().observe(this, Observer { t -> processResponse(t) })
    }

    private fun processResponse(event: Event?) {
        if (event != null) {
            when (event.type) {
                EventType.CHECK_USER -> {
                    when (event.status) {
                        EventStatus.LOADING -> {
                            Timber.d("$TAG validating user email/phone number")
                            showLoadingPopup(true, getString(R.string.dialog_validating_user_email_phone_number))
                        }
                        EventStatus.SUCCESS -> {
                            if (event.data != null) {
                                if (event.data is CheckUserResponse) {
                                    if ((event.data as CheckUserResponse).status as Boolean) {
                                        showLoadingPopup(false, null)
                                        userExists((event.data as CheckUserResponse).user_id!!)
                                    } else {
                                        showLoadingPopup(false, null)
                                        showToast(getString(R.string.toast_entered_email_phone_is_not_associated_to_any_user))
                                    }
                                } else {
                                    Timber.d("$TAG Something went wrong while checking if user exists")
                                    Timber.d("$TAG Invalid data received")
                                    showLoadingPopup(false, null)
                                    showToast(getString(R.string.toast_failed_to_validate_user_email_phone))
                                }
                            } else {
                                Timber.d("$TAG Something went wrong while checking if user exists")
                                Timber.d("$TAG No data data received")
                                showLoadingPopup(false, null)
                                showToast(getString(R.string.toast_failed_to_validate_user_email_phone))
                            }
                        }
                        EventStatus.FAILURE -> {
                            Timber.d("$TAG Something went wrong while checking if user exists")
                            showLoadingPopup(false, null)

                            if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                                showToast(getString(R.string.no_internet))
                            } else {
                                showToast(getString(R.string.toast_failed_to_validate_user_email_phone))
                            }
                        }
                    }
                }
            }
        } else {
            // Should not occur
            Timber.d("$TAG event is null")
        }

    }

    private fun userExists(userId: String) {
        if (dialog != null) {
            var emailPhoneValue = dialog!!.findViewById<EditText>(R.id.et_userEmailPhone).text.toString()
            var profileNameValue = dialog!!.findViewById<EditText>(R.id.et_profileName).text.toString()
            dialog!!.dismiss()

            Timber.d("$TAG Invitee email - $emailPhoneValue")
            Timber.d("$TAG Invite profile name - $profileNameValue")

            // TODO - Show invite user screen and send user email, profile name to invite user screen
            openInviteNewUserScreen(emailPhoneValue, userId, profileNameValue)

        } else {
            // Should not occur
            Timber.d("$TAG dialog is null")
        }
    }

    private fun openInviteNewUserScreen(emailPhoneValue: String, userId: String, profileNameValue: String) {
        startActivity(
                Intent(this, InviteUserScreen::class.java)
                        .putExtra(Constants.BUNDLE_EDIT_PROFILE, false)
                        .putExtra(Constants.BUNDLE_SHARED_WITH, emailPhoneValue)
                        .putExtra(Constants.BUNDLE_SHARED_WITH_ID, userId)
                        .putExtra(Constants.BUNDLE_PROFILE_NAME, profileNameValue)
        )
    }

    private fun openEditInvitationScreen(profile: SharedProfile) {
        startActivity(
                Intent(this, InviteUserScreen::class.java)
                        .putExtra(Constants.BUNDLE_EDIT_PROFILE, true)
                        .putExtra(Constants.BUNDLE_PROFILE, profile)
        )
    }


    private fun initViews() {
        rv_invitedUsers.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_invitedUsers.adapter = UserInvitationsAdapter(this)

    }

    private fun initClickListeners() {
        iv_back.setOnClickListener(this)
        fab_inviteNewUser.setOnClickListener(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_invitations)
        requestToBindService(this)
    }

    fun editInvitation(profile: SharedProfile) {
        openEditInvitationScreen(profile)
    }

    private fun inviteNewUser() {
        if (dialog == null) {
            dialog = Dialog(this)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setCancelable(true)
            dialog!!.setCanceledOnTouchOutside(false)
            dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        }

        dialog!!.setContentView(R.layout.dialog_invite_user)
        var emailPhoneField = dialog!!.findViewById<EditText>(R.id.et_userEmailPhone)
        var profileNameField = dialog!!.findViewById<EditText>(R.id.et_profileName)
        var cancelButton = dialog!!.findViewById<Button>(R.id.bt_cancel)
        var inviteButton = dialog!!.findViewById<Button>(R.id.bt_invite)

        emailPhoneField.requestFocus()

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }

        inviteButton.setOnClickListener {
            if (profileNameField.text != null && emailPhoneField.text != null) {
                if (profileNameField.text.trim().isNotEmpty() &&
                        emailPhoneField.text.trim().isNotEmpty()) {
                    checkIfUserEmailPhoneExists(emailPhoneField.text.toString())
                } else {
                    if ((profileNameField.text.trim().isEmpty())) {
                        showToast(getString(R.string.toast_enter_profile_name))
                    }

                    if ((emailPhoneField.text.trim().isEmpty())) {
                        showToast(getString(R.string.toast_invitee_email_phone))
                    }
                }
            } else {
                showToast(getString(R.string.toast_please_enter_all_the_data))
            }
        }

        dialog!!.show()
    }

    private fun checkIfUserEmailPhoneExists(userEmailPhone: String?) {
        if (userEmailPhone != null) {
            if (viewModel != null) {
                viewModel!!.checkIfUserExists(userEmailPhone)
            } else {
                // Should not occur
                Timber.d("$TAG View model is null")
            }
        } else {
            // Should not occur
            Timber.d("$TAG Validating email phone is null")
        }
    }
}

data class SharedProfile(val id: String, val profileName: String, val sharedBy: String, val sharedWithData: SharedWithData) : Serializable
