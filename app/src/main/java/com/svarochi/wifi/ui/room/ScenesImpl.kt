package com.svarochi.wifi.ui.room

import com.svarochi.wifi.model.api.common.CustomSceneDevice

interface ScenesImpl {

    fun getScenes(networkId: Long)

    fun addNewScene(name: String, lightList: List<CustomSceneDevice>)

    fun checkIfSceneExists(name: String)

    fun editSceneName(sceneId: Long, name: String)

    fun turnOnScene(sceneId: Long)

    fun turnOffScene(sceneId: Long)

    fun getLightsAndMarkThoseOfScene(networkId: Long, sceneId: Long) // For editing existing scene

    fun editSceneLightsList(sceneId: Long, lightList: List<CustomSceneDevice>)


    // fun getLights(networkId: Long)-> This functionality is already implemented by TestLightsImpl

}