package com.svarochi.wifi.ui.userinvitations.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Profile
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.userinvitations.repository.UserInvitationsRepository
import io.reactivex.disposables.CompositeDisposable

class UserInvitationsViewModel(var repository: UserInvitationsRepository) : BaseViewModel() {

    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }


    override fun onCleared() {
        disposables.clear()
        repository.dispose()
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    fun getSharedProfilesLiveData(): LiveData<List<Profile>> {
        return repository.getSharedProfilesLiveData()
    }
    fun checkIfUserExists(userEmailPhone: String) {
        response.value = Event(EventType.CHECK_USER, EventStatus.LOADING, null, null)
        repository.checkIfUserExists(userEmailPhone)
    }
}