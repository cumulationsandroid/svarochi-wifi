package com.svarochi.wifi.ui.audio.view

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.audio.recorder.callback.AudioRecorderCallback
import com.svarochi.wifi.audio.recorder.core.WifiAudioRecorder
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NETWORK_ID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SELECTED_IDS
import com.svarochi.wifi.common.Constants.Companion.READ_EXTERNAL_STORAGE_PERMISSION
import com.svarochi.wifi.common.Utilities.Companion.WaveLengthToRGB
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.database.entity.GroupLight
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.audio.view.adapter.AudioDevicesAdapter
import com.svarochi.wifi.ui.audio.viewmodel.AudioViewModel
import com.svarochi.wifi.ui.music.view.MusicActivity
import kotlinx.android.synthetic.main.activity_audio.*
import timber.log.Timber

class AudioActivity : WifiBaseActivity(), View.OnClickListener, ServiceConnectionCallback {
    override fun serviceConnected() {
        initViewModelAndObserver()
        initViews()
        initClickListeners()
        setAudioFlag()
    }

    private fun setAudioFlag() {
        viewModel.setAudioFlag()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            ib_micButton.id -> {
                record()
            }
            iv_back.id -> {
                closeActivity()
            }
            ib_musicButton.id -> {
                checkReadExternalStoragePermission()
            }
        }
    }

    private fun checkReadExternalStoragePermission() {
        stopRecording()
        if ((ContextCompat.checkSelfPermission(
                        this@AudioActivity,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED)
        ) {
            Timber.d("No read external storage permission")
            Timber.d("SDK Version - ${Build.VERSION.SDK_INT}")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Timber.d("Requesting read external storage permission")
                requestPermissions(
                        arrayOf(
                                android.Manifest.permission.READ_EXTERNAL_STORAGE
                        ), Constants.READ_EXTERNAL_STORAGE_PERMISSION
                )
            } else {
                // Should not occur
                Timber.d("Read external storage permission is not granted")
            }
        } else {
            checkRecordPermission()
        }
    }

    private fun checkRecordPermission() {
        if ((ContextCompat.checkSelfPermission(
                        this@AudioActivity,
                        Manifest.permission.RECORD_AUDIO
                ) != PackageManager.PERMISSION_GRANTED)
        ) {
            Timber.d("Record audio permission not granted")
            Timber.d("SDK Version - ${Build.VERSION.SDK_INT}")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Timber.d("Requesting audio permission")
                requestPermissions(
                        arrayOf(
                                android.Manifest.permission.RECORD_AUDIO
                        ), Constants.RECORD_PERMISSION
                )
            } else {
                // Should not occur
                Timber.d("Record audio permission not granted")
            }
        } else {
            openMusicActivity()
        }
    }

    private fun openMusicActivity() {
        if (((ContextCompat.checkSelfPermission(
                        this@AudioActivity,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED) && ((ContextCompat.checkSelfPermission(
                        this@AudioActivity,
                        Manifest.permission.RECORD_AUDIO
                ) == PackageManager.PERMISSION_GRANTED)))
        ) {
            if (selectedIds.isNotEmpty()) {
                stopRecording()
                startActivity(Intent(this, MusicActivity::class.java).putExtra(BUNDLE_SELECTED_IDS, selectedIds))
            } else {
                showToast(getString(R.string.toast_atleast_one_light_must_be_selected))
                Toast.makeText(this, getString(R.string.toast_atleast_one_light_must_be_selected), Toast.LENGTH_SHORT)
                        .show()
            }
        } else {
            checkReadExternalStoragePermission()
        }
    }

    private fun closeActivity() {
        finish()
    }

    private lateinit var viewModel: AudioViewModel
    private var networkId: Long = 0
    private var devicesCount = 0
    private var selectedIds = ArrayList<Any>()

    private lateinit var connectivityManager: ConnectivityManager
    private var isConnectedToInternet: Boolean = false
    private var isNetworkChangeRegistered: Boolean = false
    private var groupDevicesMap = HashMap<Long, ArrayList<String>>()

    // Audio - Recording
    private var isAudioRecording = false
    private var audioRecorder: WifiAudioRecorder? = null
    private val audioRecorderCallback = object : AudioRecorderCallback {
        override fun onBufferAvailable(buffer: ByteArray) {
            if (!isAudioRecording)
                return
            var iTByting = 0
            var iTAvByting = 0
            var intensity = 0
            var result = IntArray(0)
            if (buffer != null && buffer.isNotEmpty()) {
                try {
                    val byting = buffer[0].toInt()
                    intensity = if (byting == 0) 0 else byting + 128
                    for (bByting in buffer) {
                        val sByting = bByting.toString() + ""
                        var iByting = Integer.parseInt(sByting)
                        iTByting = iTByting + iByting
                        iByting = Math.abs(iByting)
                        iTAvByting = iTAvByting + iByting
                        result = WaveLengthToRGB(Math.abs(iTByting / 6.5))
                        result[2] = result[2] / 255 * 75
                    }
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                }

            }
            if (intensity == 0) {
                result = intArrayOf(0, 0, 0)
            }

            sendRgbValuesToDevices(result, intensity)
        }
    }

    private fun sendRgbValuesToDevices(result: IntArray, brightnessValue: Int) {
        for (device in selectedIds) {
            if (device is String) {
                // Selected id is a Mac id
                setLightRgb(device, result[0], result[1], result[2], brightnessValue)

            } else if (device is Long) {
                // Selected id is a group id
                setGroupRgb(device, result[0], result[1], result[2], brightnessValue)
            }
        }
    }

    private fun setGroupRgb(groupId: Long, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        if(brightness_value > 100){
            viewModel.setGroupRgbWithBrightness(groupId, r_value, g_value, b_value, 100)
        }else{
            viewModel.setGroupRgbWithBrightness(groupId, r_value, g_value, b_value, brightness_value)
        }
    }

    private fun setLightRgb(macId: String, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        if(brightness_value > 100){
            viewModel.setLightRgbWithBrightness(macId, r_value, g_value, b_value, 100)
        }else {
            viewModel.setLightRgbWithBrightness(macId, r_value, g_value, b_value, brightness_value)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)
        requestToBindService(this)
    }

    private fun initClickListeners() {
        ib_micButton.setOnClickListener(this)
        iv_back.setOnClickListener(this)
        ib_musicButton.setOnClickListener(this)
    }

    private fun initViews() {
        connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        networkId = intent.getLongExtra(BUNDLE_NETWORK_ID, 0)
        rv_lightsAndGroups.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        rv_lightsAndGroups.adapter = AudioDevicesAdapter(this)
        getLights()
    }

    private fun getGroups() {
        viewModel.getGroups(networkId)
    }


    private fun getLights() {
        viewModel.getLights(networkId)
    }

    override fun onStart() {
        super.onStart()
        reqisterNetworkCallback()
    }

    private var networkChangeCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            Timber.d("OnAvailable")
            isConnectedToInternet = true
        }

        override fun onLost(network: Network?) {
            isConnectedToInternet = false
            stopRecording()
        }
    }

    private fun reqisterNetworkCallback() {
        if (isNetworkChangeRegistered.not()) {
            isNetworkChangeRegistered = true
            var builder = NetworkRequest.Builder().build()
            connectivityManager.registerNetworkCallback(builder, networkChangeCallback)
            Timber.d("Network change callback registered")
        }
    }

    private fun initViewModelAndObserver() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(AudioViewModel::class.java)
        viewModel.getResponse().observe(this, Observer<Event> { t -> processResponse(t) })
    }

    private fun processResponse(event: Event?) {
        when (event!!.type) {
            EventType.GET_LIGHTS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Fetching lights of network($networkId)")
                    }
                    EventStatus.SUCCESS -> {
                        var lights = event.data as List<Light>
                        Timber.d("Successfully got ${lights.size} lights")

                        addLightsToRv(lights)
                    }
                }
            }

            EventType.GET_GROUPS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Fetching groups of network($networkId)")
                    }
                    EventStatus.SUCCESS -> {
                        var groups = event.data as List<Group>
                        Timber.d("Successfully got ${groups.size} groups")

                        addGroupsToRv(groups)
                        getGroupDevices(groups)
                    }
                }
            }

            EventType.GET_GROUP_DEVICES -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Fetching devices of group")
                    }
                    EventStatus.SUCCESS -> {
                        var groupLights = event.data as List<GroupLight>
                        addLightsToGroupDevicesMap(groupLights)
                    }
                }
            }
        }
    }

    private fun addLightsToGroupDevicesMap(groupLights: List<GroupLight>) {
        for (groupLight in groupLights) {
            if (groupDevicesMap.containsKey(groupLight.groupId)) {
                var devicesList = groupDevicesMap[groupLight.groupId]
                devicesList!!.add(groupLight.macId)
                groupDevicesMap[groupLight.groupId] = devicesList

                Timber.d("Stored ${devicesList.size} lights of group(${groupLight.groupId})")
            } else {
                var devicesList = ArrayList<String>()
                devicesList.add(groupLight.macId)
                groupDevicesMap[groupLight.groupId] = devicesList

                Timber.d("Stored ${devicesList.size} lights of group(${groupLight.groupId}")
            }
        }
    }

    private fun getGroupDevices(groupsList: List<Group>) {
        for (group in groupsList) {
            viewModel.getDevicesOfGroup(group.id!!)
        }
    }

    private fun addLightsToRv(lights: List<Light>) {
        devicesCount += lights.size
        (rv_lightsAndGroups.adapter as AudioDevicesAdapter).addToList(lights)
        //(rv_lightsAndGroups.adapter as AudioDevicesAdapter).notifyDataSetChanged()
        getGroups()
    }

    private fun addGroupsToRv(groups: List<Group>) {
        devicesCount += groups.size
        (rv_lightsAndGroups.adapter as AudioDevicesAdapter).addToList(groups)
        //(rv_lightsAndGroups.adapter as AudioDevicesAdapter).notifyDataSetChanged()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.RECORD_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Timber.d("Record audio permission granted")
                    startRecording()
                } else {
                    Timber.d("Record audio permission not granted")
                }
            }

            READ_EXTERNAL_STORAGE_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Timber.d("Record audio permission granted")
                    checkRecordPermission()
                } else {
                    Timber.d("Record audio permission not granted")
                }
            }
        }
    }

    fun deviceSelected(id: Any) {
        if (id is Long) {
            // Group is selected
            var groupLights = groupDevicesMap[id]
            var shouldSelectGroup = true
            if (groupLights.isNullOrEmpty().not()) {
                for (light in groupLights!!) {
                    if (selectedIds.contains(light)) {
                        // Dont select the group
                        // Group contains a selected light
                        shouldSelectGroup = false
                        break
                    }
                }

                if (shouldSelectGroup) {
                    selectedIds.addAll(groupLights)
                    Timber.d("Added lights of Group($id) into selected id list")
                } else {
                    Timber.d("Group($id) contains a selected light")
                    //showToast(this, getString(R.string.toast_group_contains_a_selected_light))
                    Toast.makeText(this, getString(R.string.toast_group_contains_a_selected_light), Toast.LENGTH_SHORT)
                            .show()
                    resetCheckbox(id)
                }
            } else {
                Timber.d("No lights in the selected group")
            }
        } else if (id is String) {
            // Light is selected

            if (selectedIds.contains(id).not()) {
                selectedIds.add(id)
                Timber.d("Added light($id) into selected id list")
            } else {
                Timber.d("Light($id) is already selected")
                //showToast(this, getString(R.string.toast_light_is_already_selected_or_present_in_another_group))
                Toast.makeText(
                        this,
                        getString(R.string.toast_light_is_already_selected_or_present_in_another_group),
                        Toast.LENGTH_SHORT
                ).show()
                resetCheckbox(id)
            }
        }
    }

    private fun resetCheckbox(id: Any) {
        if (rv_lightsAndGroups.adapter != null) {
            (rv_lightsAndGroups.adapter as AudioDevicesAdapter).resetCheckStatus(id)
        } else {
            Timber.d("List doesnt have adapter")
        }
    }

    fun deviceUnSelected(id: Any) {
        if (!isAudioRecording) {
            if (id is Long) {
                // Group is unselected
                if (groupDevicesMap[id].isNullOrEmpty().not()) {
                    selectedIds.removeAll(groupDevicesMap[id]!!)
                    Timber.d("Removed lights of group($id) from selected id list")
                } else {
                    // Should not occur
                    Timber.d("No lights present of the group($id)")
                }
            } else if (id is String) {
                // Light is unselected
                if (selectedIds.contains(id)) {
                    selectedIds.remove(id)
                    Timber.d("Light($id) removed from selected id list")
                } else {
                    Timber.d("Light($id) doesnt selected id list")
                }
            }
        } else {
            Timber.d("Stop recording and then unselect the device")
            showToast(getString(R.string.toast_stop_recording_and_then_unselect_the_device))
            Toast.makeText(
                    this,
                    getString(R.string.toast_stop_recording_and_then_unselect_the_device),
                    Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun record() {
        if (devicesCount > 0) {
            if (isAudioRecording) {
                stopRecording()
            } else {
                startRecording()
            }
        } else {
            Timber.d("No devices available")
        }
    }

    private fun startRecording() {
        if (isConnectedToInternet) {
            if ((ContextCompat.checkSelfPermission(
                            this@AudioActivity,
                            Manifest.permission.RECORD_AUDIO
                    ) != PackageManager.PERMISSION_GRANTED)
            ) {
                Timber.d("Record audio permission not granted")
                Timber.d("SDK Version - ${Build.VERSION.SDK_INT}")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Timber.d("Requesting audio permission")
                    requestPermissions(
                            arrayOf(
                                    android.Manifest.permission.RECORD_AUDIO
                            ), Constants.RECORD_PERMISSION
                    )
                } else {
                    // Should not occur
                    Timber.d("Record audio permission not granted")
                }
            } else {
                if (!isAudioRecording && selectedIds.isNotEmpty()) {
                    if (audioRecorder != null) {
                        audioRecorder!!.stop()
                        audioRecorder = null
                    }

                    audioRecorder = WifiAudioRecorder(audioRecorderCallback)
                    audioRecorder!!.start()
                    isAudioRecording = true
                    setRecordFlagToAdapter(true)

                    ib_micButton.setImageResource(R.drawable.ic_mic_off)

                    Timber.d("Audio recording started")
                } else {
                    if (isAudioRecording)
                        Timber.d("Device already recording audio")

                    if (selectedIds.isEmpty()) {
                        Timber.d("No devices selected to start recording")
                        showToast(getString(R.string.toast_atleast_one_light_must_be_selected))
                        Toast.makeText(
                                this,
                                getString(R.string.toast_atleast_one_light_must_be_selected),
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        } else {
            showToast(getString(R.string.toast_no_network_connection))
            Toast.makeText(this, getString(R.string.toast_no_network_connection), Toast.LENGTH_SHORT).show()
        }
    }

    private fun setRecordFlagToAdapter(flag: Boolean) {
        (rv_lightsAndGroups.adapter as AudioDevicesAdapter).setRecordingStatus(flag)
    }

    private fun stopRecording() {
        if (isAudioRecording) {
            if (audioRecorder != null) {
                audioRecorder!!.stop()
            }
            isAudioRecording = false
            setRecordFlagToAdapter(false)

            ib_micButton.setImageResource(R.drawable.ic_mic_on)
            Timber.d("Audio recording stopped")
        } else {
            Timber.d("Device isnt recording audio")
        }
    }

    override fun onStop() {
        stopRecording()
        super.onStop()
    }

    override fun onPause() {
        stopRecording()
        super.onPause()
    }

    override fun onDestroy() {
        viewModel.resetAudioFlag()
        super.onDestroy()
    }

}
