package com.svarochi.wifi.ui.diagnostics.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_LIGHT_MACID
import com.svarochi.wifi.common.Constants.Companion.SVAROCHI_LAMP_IDENTIFIER
import com.svarochi.wifi.model.common.LightStatusResponse
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.diagnostics.viewmodel.DiagnosticsViewModel
import kotlinx.android.synthetic.main.activity_diagnostics_screen.*
import timber.log.Timber


class DiagnosticsScreen : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener {

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                iv_backButton.id -> {
                    finish()
                }
                bt_retest.id -> {
                    retest()
                }
            }
        } else {
            Timber.d("${TAG} - OnClick view is null")
        }
    }

    private var isBroadcasting: Boolean = false
    private var mqttRequestSent: Boolean = false
    private var isInternetAvailable: Boolean = false
    private var isConnectedToWifi: Boolean = false

    var connectivityManager: ConnectivityManager? = null
    var wifiManager: WifiManager? = null
    private var TAG = "DiagnosticsScreenActivity"
    private var deviceAvailableViaMqtt = false
    private var viewModel: DiagnosticsViewModel? = null
    private var macId: String? = null
    private var isNetworkCallbackRegisted = false

    private var networkChangeCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            Timber.d("$TAG - Mobile Data Available")

            if (connectivityManager != null && wifiManager != null) {
                Timber.d("$TAG - Connected to mobile data")
                isInternetAvailable = true
                Handler().postDelayed({
                    checkIfDeviceAvailableViaMqtt()
                }, 1500)
            } else {
                // Should not occur
                Timber.d("$TAG - WifiManager or ConnectivityManager is null")
            }
        }

        override fun onLost(network: Network?) {
            Timber.d("$TAG - Mobile Data Lost")
            isInternetAvailable = false
            setDeviceUnavailable()
        }
    }

    private var wifiChangeCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            Timber.d("$TAG - Wifi Available")
            if (connectivityManager != null && wifiManager != null) {
                Timber.d("$TAG - Connected to wifi")
                if ((!wifiManager!!.connectionInfo.ssid.isNullOrEmpty() &&
                                !wifiManager!!.connectionInfo.ssid.contains(SVAROCHI_LAMP_IDENTIFIER, false) &&
                                !wifiManager!!.connectionInfo.ssid.contains("<unknown ssid>", true)) ||
                        (connectivityManager!!.getNetworkInfo(network) != null &&
                                !connectivityManager!!.getNetworkInfo(network).extraInfo.isNullOrEmpty() &&
                                !connectivityManager!!.getNetworkInfo(network).extraInfo.contains(
                                        SVAROCHI_LAMP_IDENTIFIER,
                                        false
                                ))
                ) {
                    Timber.d("$TAG - Successfully connected to non svarochi wifi network - ${wifiManager!!.connectionInfo.ssid}")
                    isConnectedToWifi = true
                    Handler().postDelayed({
                        checkIfDeviceAvailableViaMqtt()
                    }, 1500)
                } else {
                    Timber.d("$TAG - Successfully connected to svarochi or unknown ssid wifi network - ${wifiManager!!.connectionInfo.ssid}")
                    isConnectedToWifi = false

                }
            } else {
                // Should not occur
                Timber.d("$TAG - WifiManager or ConnectivityManager is null")
            }
        }

        override fun onLost(network: Network?) {
            Timber.d("$TAG - Wifi Lost")
            isConnectedToWifi = false
            setDeviceUnavailable()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diagnostics_screen)
        requestToBindService(this)

        if (intent.extras != null) {
            macId = intent.getStringExtra(BUNDLE_LIGHT_MACID)
        }
    }

    override fun onStart() {
        startObserveInternetConnectivityChange()
        super.onStart()
    }

    override fun onStop() {
        stopObservingInternetConnectivity()
        super.onStop()
    }

    override fun serviceConnected() {
        init()
    }

    private fun startObserveInternetConnectivityChange() {
        var mobileInternetBuilder = NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR).build()
        var wifiBuilder = NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build()
        if (connectivityManager != null) {
            connectivityManager!!.registerNetworkCallback(mobileInternetBuilder, networkChangeCallback)
            connectivityManager!!.registerNetworkCallback(wifiBuilder, wifiChangeCallback)
            Timber.d("$TAG - Network change callback registered")
            isNetworkCallbackRegisted = true
        } else {
            // Should not occur
            Timber.d("$TAG - ConnectivityManager is null, couldnot register network callback")
        }
    }

    private fun stopObservingInternetConnectivity() {
        if (connectivityManager != null) {
            connectivityManager!!.unregisterNetworkCallback(networkChangeCallback)
            connectivityManager!!.unregisterNetworkCallback(wifiChangeCallback)
            Timber.d("$TAG - Network change callback unregistered")
            isNetworkCallbackRegisted = false
        } else {
            // Should not occur
            Timber.d("$TAG - ConnectivityManager is null, couldnot unregister network callback")
        }
    }

    private fun init() {
        initManagers()
        initViewModel()
        initObservers()
        initClickListeners()
        if (isNetworkCallbackRegisted) {
            checkIfDeviceAvailableViaMqtt()
        } else {
            if (GeneralUtils.isInternetAvailable(this)) {
                showMqttLoading()
            } else {
                showMqttNotAvailable()
            }
        }
    }

    private fun initClickListeners() {
        iv_backButton.setOnClickListener(this)
        bt_retest.setOnClickListener(this)
    }

    private fun initObservers() {
        if (viewModel != null) {
            viewModel!!.response.observe(this, Observer { t -> processResponse(t) })
            Timber.d("$TAG - ViewModel is null, reinitialising ViewModel")
        } else {
            // Should not occur
            Timber.d("$TAG - ViewModel is null, reinitialising ViewModel")
            initViewModel()
        }
    }

    private fun processResponse(event: Event?) {
        if (event != null) {
            when (event.type) {
                EventType.GET_LIGHT_STATUS -> {
                    when (event.status) {
                        EventStatus.SUCCESS -> {
                            if (event.data != null) {
                                if (event.data is LightStatusResponse) {

                                    if ((event.data as LightStatusResponse).macId.equals(macId)) {
                                        if ((event.data as LightStatusResponse).isFromTcp) {
                                            Timber.d("$TAG - $macId responded to GET_LIGHT_STATUS command via TCP")
                                            updateSignalStrengthText((event.data as LightStatusResponse).rssiValue)
                                            setDeviceAvailableViaTcp()
                                        } else {
                                            Timber.d("$TAG - $macId responded to GET_LIGHT_STATUS command via MQTT")
                                            mqttRequestSent = false

                                            updateSignalStrengthText((event.data as LightStatusResponse).rssiValue)
                                            setDeviceAvailableViaMqtt()
                                        }
                                    } else {
                                        // Should not occur
                                        Timber.d("$TAG - Unused HashMap response is received in GET_LIGHT_STATUS success response")
                                        Timber.d("$TAG - Some other device has responeded to GET_LIGHT_STATUS success packet")
                                    }
                                } else {
                                    // Should not occur
                                    Timber.d("$TAG - Non HashMap response is received in GET_LIGHT_STATUS response")
                                }
                            } else {
                                // Should not occur
                                Timber.d("$TAG - No data received in GET_LIGHT_STATUS response")
                            }
                        }

                        EventStatus.FAILURE -> {
                            Timber.d("$TAG - GET_LIGHT_STATUS command failed")
                            if (event.data != null) {
                                if (event.data is HashMap<*, *>) {

                                    if ((event.data as HashMap<String, String>).containsKey(macId)) {
                                        if ((event.data as HashMap<String, String>)[macId].equals("TCP")) {
                                        } else if ((event.data as HashMap<String, String>)[macId].equals("MQTT")) {
                                            mqttRequestSent = false
                                            showMqttNotAvailable()
                                        }
                                    } else {
                                        // Should not occur
                                        Timber.d("$TAG - Unused HashMap response is received in GET_LIGHT_STATUS failure response")
                                        Timber.d("$TAG - Some other device has responeded to GET_LIGHT_STATUS packet")
                                    }
                                } else {
                                    // Should not occur
                                    Timber.d("$TAG - Non HashMap response is received in GET_LIGHT_STATUS response")
                                }
                            } else {
                                Timber.d("$TAG - No data received in GET_LIGHT_STATUS failure packet")
                            }
                        }
                    }
                }

                EventType.MQTT_CONNECTION -> {
                    when (event.status) {
                        EventStatus.FAILURE -> {
                            showMqttNotAvailable()
                            mqttRequestSent = false
                        }
                    }
                }
            }

        } else {
            // Should not occur
            Timber.d("$TAG - Event value is null")
        }

    }

    private fun updateSignalStrengthText(rssiValue: Int) {
        if (rssiValue >= 0 && rssiValue <= 20) {
            tv_signalStrength.text = getString(R.string.label_very_low)
            return
        }

        if (rssiValue > 20 && rssiValue <= 30) {
            tv_signalStrength.text = getString(R.string.label_low)
            return
        }

        if (rssiValue > 30 && rssiValue <= 45) {
            tv_signalStrength.text = getString(R.string.label_high)
            return
        }

        if (rssiValue > 45) {
            tv_signalStrength.text = getString(R.string.label_very_high)
            return
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(DiagnosticsViewModel::class.java)
    }

    private fun initManagers() {
        wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    private fun checkIfDeviceAvailableViaMqtt() {
        Timber.d("$TAG - Device state, available via MQTT = $deviceAvailableViaMqtt")
        if ((isInternetAvailable || isConnectedToWifi) && mqttRequestSent.not()) {
            if (viewModel != null) {
                if (macId != null) {
                    // TODO - UI changes - Show loader for MQTT
                    showMqttLoading()
                    mqttRequestSent = true
                    viewModel!!.checkIfDeviceIsAvailableViaMqtt(macId!!)
                } else {
                    // Should not occur
                    Timber.d("$TAG - Macid is null")
                }
            } else {
                // Should not occur
                Timber.d("$TAG - ViewModel is null, reinitialising ViewModel")
                initViewModel()
            }
        } else {
            Timber.d("$TAG No internet available")
            showMqttNotAvailable()
        }

    }

    private fun setDeviceUnavailable() {
        deviceAvailableViaMqtt = false
        Timber.d("$TAG - Device state available via MQTT = $deviceAvailableViaMqtt")

        // TODO - UI changes - Show invalid sign for both
        showErrorForTcpAndMqtt()
    }

    private fun setDeviceAvailableViaMqtt() {
        deviceAvailableViaMqtt = true
        Timber.d("$TAG - Device state available via MQTT = $deviceAvailableViaMqtt")

        // TODO - UI changes - Show available for MQTT
        showMqttAvailable()
    }

    private fun setDeviceAvailableViaTcp() {
        /*deviceAvailableViaTcp = true
        Timber.d("$TAG - Device state available via MQTT = $deviceAvailableViaMqtt")

        // TODO - UI changes - Show available for TCP
        showTcpAvailable()*/
    }


    private fun showMqttLoading() {
        runOnUiThread {
            tv_errorMqttConnection.visibility = GONE
            iv_deviceStateIcon.visibility = GONE
            pb_loading.visibility = VISIBLE
            tv_errorMqttConnection.visibility = GONE
            tv_signalStrength.text = getString(R.string._dash)

            iv_bulbIcon.setImageResource(R.drawable.ic_bulb_grey)
        }
    }


    private fun showErrorForTcpAndMqtt() {
        showMqttNotAvailable()
    }

    private fun showMqttNotAvailable() {
        runOnUiThread {
            pb_loading.visibility = GONE
            iv_deviceStateIcon.visibility = VISIBLE
            iv_deviceStateIcon.setImageResource(R.drawable.ic_warning)
            tv_errorMqttConnection.visibility = VISIBLE
            tv_signalStrength.text = getString(R.string._dash)

            iv_bulbIcon.setImageResource(R.drawable.ic_bulb_grey)
        }
    }

    private fun showMqttAvailable() {
        runOnUiThread {
            pb_loading.visibility = GONE
            tv_errorMqttConnection.visibility = GONE
            iv_deviceStateIcon.visibility = VISIBLE
            iv_deviceStateIcon.setImageResource(R.drawable.ic_tick_green)

            if (deviceAvailableViaMqtt) {
                iv_bulbIcon.setImageResource(R.drawable.ic_bulb_green)
            } else {
                iv_bulbIcon.setImageResource(R.drawable.ic_bulb_grey)
            }
        }
    }


    private fun retest() {
        if (isInternetAvailable || isConnectedToWifi) {
            if (mqttRequestSent.not()) {
                checkIfDeviceAvailableViaMqtt()
                Timber.d("$TAG - Phone connected to Internet, hence performing retest for MQTT")
            } else {
                Timber.d("$TAG - MQTT request already sent, awaiting response")
            }

            /*if (isConnectedToWifi) {
                if (isBroadcasting.not()) {
                    startUdpBroadcasting()
                    Timber.d("$TAG - Phone connected to Wifi Internet, hence performing retest for TCP")
                } else {
                    Timber.d("$TAG - Already performing UDP broadcast")
                }
            } else {
                Timber.d("$TAG - Phone not connected to Wifi, hence cannot retest for TCP")
            }*/
        } else {
            Timber.d("$TAG - No internet is available, hence no retest can be performed")
            showToast(getString(R.string.no_internet))
            showMqttNotAvailable()
        }
    }

}
