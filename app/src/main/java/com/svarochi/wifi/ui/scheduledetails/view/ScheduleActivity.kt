package com.svarochi.wifi.ui.scheduledetails.view

import android.app.Activity
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.Window
import android.widget.Button
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_ACTION_DETAILS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SCHEDULE_ID
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.common.Utilities.Companion.convertHexStringToInt
import com.svarochi.wifi.common.Utilities.Companion.convertToUTC
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.Schedule
import com.svarochi.wifi.model.api.common.EndAction
import com.svarochi.wifi.model.api.common.StartAction
import com.svarochi.wifi.model.common.CustomGroup
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.Scene
import com.svarochi.wifi.ui.scheduleaction.view.SetActionActivity
import com.svarochi.wifi.ui.scheduledetails.viewmodel.ScheduleDetailsViewModel
import kotlinx.android.synthetic.main.activity_schedule.*
import timber.log.Timber
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet
import kotlin.math.absoluteValue


class ScheduleActivity : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener {
    private var selectedMacIds = ArrayList<String>()
    private var selectedGroupIds = ArrayList<Long>()
    private var groupsList: ArrayList<CustomGroup>? = null
    private var lightsList: ArrayList<CustomLight>? = null
    private var viewModel: ScheduleDetailsViewModel? = null
    private var dialog: Dialog? = null
    private var startAction: ActionDetails? = null
    private var endAction: ActionDetails? = null
    private val SET_START_ACTION = 1
    private val SET_END_ACTION = 2
    private var timePickerDialog: TimePickerDialog? = null
    private var isSettingStartTime = false
    private var selectedStartHour: Int = -1
    private var selectedStartMinute: Int = -1
    private var selectedEndHour: Int = -1
    private var selectedEndMinute: Int = -1
    private var scheduleId: Long = -1
    private var schedule: Schedule? = null

    private var timePickerListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                setTimeText(hourOfDay, minute)
            }

    private fun setTimeText(hourOfDay: Int, minute: Int) {
        var hourReceived = hourOfDay
        var amPmText = "AM"
        Timber.d("Selected Time - Hour($hourReceived) Minute($minute) in 24hour format")
        if (hourReceived > 12) {
            hourReceived = (12 - hourOfDay).absoluteValue
            amPmText = "PM"
        } else if (hourReceived == 12) {
            amPmText = "PM"
        } else {
            amPmText = "AM"
        }
        Timber.d("Selected Time - Hour($hourReceived) Minute($minute) $amPmText in 12hour format")

        if (isSettingStartTime) {
            // Received start time

            selectedStartHour = hourOfDay
            selectedStartMinute = minute
            selectedEndHour = -1
            selectedEndMinute = -1
            isSettingStartTime = false

            tv_startTime.text =
                    appendOIfNeeded(hourReceived) + getString(R.string._colon) +
                            appendOIfNeeded(minute) + getString(R.string._space) + amPmText

//            tv_endTime.text = getString(R.string._dash_)

            tv_endTime.text = ""

            Utilities.scrollToView(sv_parent, ll_selectGroups)
        } else {
            // Received end time
            if (((hourOfDay < selectedStartHour) || ((hourOfDay == selectedStartHour) && (minute < selectedStartMinute)))) {
                Timber.d("End time cant be less than start time")
                showToast(getString(R.string.toast_end_time_must_be_more_than_start_time))
                return
            }

            if ((hourOfDay == selectedStartHour && minute == selectedStartMinute)) {
                Timber.d("End time cannot be same as start time")
                showToast(getString(R.string.toast_end_time_cannot_be_same_as_start_time))
                return
            }

            selectedEndHour = hourOfDay
            selectedEndMinute = minute
            isSettingStartTime = false

            tv_endTime.text = getString(R.string._space) +
                    appendOIfNeeded(hourReceived) + getString(R.string._colon) +
                    appendOIfNeeded(minute) + getString(R.string._space) + amPmText

            Utilities.scrollToView(sv_parent, ll_selectGroups)
        }
    }


    private fun appendOIfNeeded(value: Int): String {
        return String.format("%02d", value)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                iv_back.id -> {
                    finish()
                }
                /*iv_delete.id -> {
                    deleteSchedule()
                }*/
                bt_saveButton.id -> {
                    saveButton()
                }
                ll_selectLights.id -> {
                    showSelectLightsDialog()
                }
                ll_selectGroups.id -> {
                    showSelectGroupsDialog()
                }
                ll_selectStartAction.id -> {
                    showActionScreen(true)
                }
                ll_selectEndAction.id -> {
                    showActionScreen(false)
                }
                ll_selectStartTime.id -> {
                    isSettingStartTime = true
                    if (selectedStartHour != -1 && selectedStartMinute != -1) {
                        timePickerDialog!!.updateTime(selectedStartHour, selectedStartMinute)
                    }
                    timePickerDialog!!.show()
                }
                ll_selectEndTime.id -> {
                    if (selectedStartHour != -1 && selectedStartMinute != -1) {
                        if (selectedEndHour != -1 && selectedEndMinute != -1) {
                            timePickerDialog!!.updateTime(selectedEndHour, selectedEndMinute)
                        }
                        timePickerDialog!!.show()

                    } else {
                        Timber.d("No start time is selected")
                        showToast(getString(R.string.toast_please_select_start_time))
                    }
                }
            }
        }
    }

    private fun saveButton() {
        if (et_eventName.text == null || et_eventName.text.toString().trim().isEmpty()) {
            showToast(getString(R.string.please_enter_valid_event_name))
            return
        }

        if ((rv_days.adapter as DaySelectionAdapter).getSelectedDays().isEmpty()) {
            showToast(getString(R.string.please_select_days))
            return
        }

        if (selectedStartHour == -1 && selectedStartMinute == -1) {
            showToast(getString(R.string.please_select_start_time))
            return
        }

        if (selectedEndHour == -1 && selectedEndMinute == -1) {
            showToast(getString(R.string.please_select_end_time))
            return
        }

        if (selectedMacIds.isEmpty() && selectedGroupIds.isEmpty()) {
            showToast(getString(R.string.please_select_lights_or_groups))
            return
        }

        if (scheduleId != (-1).toLong() && schedule != null) {
            if (viewModel!!.isScheduleNameTaken(et_eventName.text.toString(), scheduleId)) {
                showToast(getString(R.string.schedule_with_that_name_already_exists))
                return
            }
        } else {
            if (viewModel!!.isScheduleNameTaken(et_eventName.text.toString())) {
                showToast(getString(R.string.schedule_with_that_name_already_exists))
                return
            }
        }

        if (startAction == null) {
            showToast(getString(R.string.toast_please_select_start_action))
            return
        }

        if (endAction == null) {
            showToast(getString(R.string.toast_please_select_end_action))
            return
        }

        val selectedDaysList = (rv_days.adapter as DaySelectionAdapter).getSelectedDays()
        val tempSelectedStartTime = appendOIfNeeded(selectedStartHour) + ":" + appendOIfNeeded(selectedStartMinute) + ":00"
        val tempSelectedEndTime = appendOIfNeeded(selectedEndHour) + ":" + appendOIfNeeded(selectedEndMinute) + ":00"

        val selectedStartTime = convertToUTC(tempSelectedStartTime)
        val selectedEndTime = convertToUTC(tempSelectedEndTime)

        val startPower: String
        if (startAction!!.onOffValue!!) startPower = "ON" else startPower = "OFF"

        val configuredStartAction = StartAction(
                startPower,
                Utilities.convertHexStringToInt(startAction!!.rValue!!),
                Utilities.convertHexStringToInt(startAction!!.gValue!!),
                Utilities.convertHexStringToInt(startAction!!.bValue!!),
                startAction!!.brightnessValue!!,
                Utilities.convertHexStringToInt(startAction!!.wValue!!),
                Utilities.convertHexStringToInt(startAction!!.cValue!!),
                startAction!!.sceneValue
        )

        val endPower: String
        if (endAction!!.onOffValue!!) endPower = "ON" else endPower = "OFF"

        val configuredEndAction = EndAction(
                endPower,
                Utilities.convertHexStringToInt(endAction!!.rValue!!),
                Utilities.convertHexStringToInt(endAction!!.gValue!!),
                Utilities.convertHexStringToInt(endAction!!.bValue!!),
                endAction!!.brightnessValue!!,
                Utilities.convertHexStringToInt(endAction!!.wValue!!),
                Utilities.convertHexStringToInt(endAction!!.cValue!!),
                endAction!!.sceneValue
        )

        val groupIds = ArrayList<Int>()
        selectedGroupIds.forEach {
            groupIds.add(it.toInt())
        }

        if (scheduleId != (-1).toLong() && schedule != null) {
            // Edit schedule
            viewModel!!.editSchedule(scheduleId, et_eventName.text.toString(), selectedDaysList, selectedStartTime, configuredStartAction, selectedEndTime, configuredEndAction, selectedMacIds, groupIds)

        } else {
            // New schedule
            viewModel!!.addNewSchedule(et_eventName.text.toString(), selectedDaysList, selectedStartTime, configuredStartAction, selectedEndTime, configuredEndAction, selectedMacIds, groupIds)
        }

    }

    private fun showActionScreen(showStartAction: Boolean) {
        val macIds = HashSet<String>()
        macIds.addAll(selectedMacIds)

        selectedGroupIds.forEach { groupId ->
            viewModel!!.getGroupLights(groupId).forEach {
                macIds.add(it.macId)
            }
        }
        if (selectedMacIds.isNotEmpty() || selectedGroupIds.isNotEmpty()) {
            if (showStartAction) {
                if (startAction == null) {
                    var tempAction = ActionDetails()
                    tempAction.rValue = "00"
                    tempAction.gValue = "00"
                    tempAction.bValue = "00"
                    tempAction.wValue = "00"
                    tempAction.cValue = "FF"
                    tempAction.brightnessValue = 100
                    tempAction.sceneValue = Scene.DEFAULT.value
                    tempAction.onOffValue = true
                    tempAction.selectedMacIds = macIds

                    startActivityForResult(Intent(this, SetActionActivity::class.java)
                            .putExtra(BUNDLE_ACTION_DETAILS, tempAction), SET_START_ACTION)
                } else {
                    startActivityForResult(Intent(this, SetActionActivity::class.java)
                            .putExtra(BUNDLE_ACTION_DETAILS, startAction!!), SET_START_ACTION)
                }
            } else {
                if (endAction == null) {
                    var tempAction = ActionDetails()
                    tempAction.rValue = "00"
                    tempAction.gValue = "00"
                    tempAction.bValue = "00"
                    tempAction.wValue = "00"
                    tempAction.cValue = "FF"
                    tempAction.brightnessValue = 100
                    tempAction.sceneValue = Scene.DEFAULT.value
                    tempAction.onOffValue = true
                    tempAction.selectedMacIds = macIds

                    startActivityForResult(Intent(this, SetActionActivity::class.java)
                            .putExtra(BUNDLE_ACTION_DETAILS, tempAction), SET_END_ACTION)
                } else {
                    startActivityForResult(Intent(this, SetActionActivity::class.java)
                            .putExtra(BUNDLE_ACTION_DETAILS, endAction!!), SET_END_ACTION)
                }
            }
        } else {
            showToast(getString(R.string.no_lights_or_groups_selected))
            Utilities.scrollToView(sv_parent, ll_selectGroups)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SET_START_ACTION && resultCode == Activity.RESULT_OK && data != null && data.extras != null) {
            startAction = data.getSerializableExtra(BUNDLE_ACTION_DETAILS) as ActionDetails
            setStartActionUi()
        } else if (requestCode == SET_END_ACTION && resultCode == Activity.RESULT_OK && data != null && data.extras != null) {
            endAction = data.getSerializableExtra(BUNDLE_ACTION_DETAILS) as ActionDetails
            setEndActionUi()
        }
    }

    private fun showSelectGroupsDialog() {
        initGroupsRvOfDialog()
    }

    private fun showSelectLightsDialog() {
        initLightsRvOfDialog()
    }

    override fun serviceConnected() {
        init()

    }

    private fun initCommonDialog() {
        dialog = Dialog(this@ScheduleActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setCanceledOnTouchOutside(false)
    }

    private fun getAllGroups() {
        groupsList = viewModel!!.getAllGroups()
    }

    private fun getAllLights() {
        lightsList = viewModel!!.getAllLights()
    }

    private fun initViewModelAndObservers() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(ScheduleDetailsViewModel::class.java)

        viewModel!!.response.observe(this, androidx.lifecycle.Observer { processResponse(it!!) })
    }

    private fun processResponse(event: Event) {
        when (event.type) {
            EventType.ADD_NEW_SCHEDULE -> {
                when (event.status) {
                    EventStatus.FAILURE -> {
                        showLoadingPopup(false, null)
                        if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                            showToast(getString(R.string.toast_no_internet_available))
                            return
                        }
                        showToast(getString(R.string.failed_to_add_new_schedule))
                    }
                    EventStatus.SUCCESS -> {
                        showLoadingPopup(false, null)
                        finish()
                    }
                    EventStatus.LOADING -> {
                        showLoadingPopup(true, getString(R.string.adding_new_schedule))
                    }
                }
            }
            EventType.EDIT_SCHEDULE -> {
                when (event.status) {
                    EventStatus.FAILURE -> {
                        showLoadingPopup(false, null)
                        if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                            showToast(getString(R.string.toast_no_internet_available))
                            return
                        }
                        showToast(getString(R.string.failed_to_edit_schedule))
                    }
                    EventStatus.SUCCESS -> {
                        showLoadingPopup(false, null)
                        finish()
                    }
                    EventStatus.LOADING -> {
                        showLoadingPopup(true, getString(R.string.editing_schedule))
                    }
                }
            }

            EventType.DELETE_SCHEDULE -> {
                when (event.status) {
                    EventStatus.FAILURE -> {
                        showLoadingPopup(false, null)
                        if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                            showToast(getString(R.string.toast_no_internet_available))
                            return
                        }
                        showToast(getString(R.string.failed_to_delete_schedule))
                    }
                    EventStatus.SUCCESS -> {
                        showLoadingPopup(false, null)
                        finish()
                    }
                    EventStatus.LOADING -> {
                        showLoadingPopup(true, getString(R.string.deleting_schedule))
                    }
                }
            }
        }
    }

    private fun initLightsRvOfDialog() {

        if (dialog != null) {
            dialog!!.setContentView(R.layout.dialog_light_list)

            val selectButton: Button = dialog!!.findViewById(R.id.bt_selectButton)
            val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
            var lightsRv: androidx.recyclerview.widget.RecyclerView = dialog!!.findViewById(R.id.rv_lightsRv)


            for (light in lightsList!!) {
                light.setSelected(selectedMacIds.contains(light.light.macId))
            }

            lightsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            lightsRv.adapter = LightsAdapter(lightsList!!, this)

            dialog!!.show()

            selectButton.setOnClickListener {
                if (lightsList!!.isNotEmpty()) {
                    selectedMacIds.clear()
                    for (lightId in (lightsRv.adapter as LightsAdapter).selectedIds) {
                        Timber.d("Selected id - $lightId")
                        selectedMacIds.add(lightId)
                    }
                    Timber.d("Selected ids count - ${selectedMacIds.size}")

                    showSelectedLightNames((lightsRv.adapter as LightsAdapter).selectedLightNames)
                    dialog!!.dismiss()
                } else {
                    Timber.d("No lights available to select")
                }
            }

            cancelButton.setOnClickListener {
                dialog!!.dismiss()
            }

        } else {
            initCommonDialog()
        }
    }

    private fun initGroupsRvOfDialog() {
        if (dialog != null) {
            dialog!!.setContentView(R.layout.dialog_group_list)

            val selectButton: Button = dialog!!.findViewById(R.id.bt_selectButton)
            val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
            var groupsRv: androidx.recyclerview.widget.RecyclerView = dialog!!.findViewById(R.id.rv_groupsRv)

            for (group in groupsList!!) {
                group.isSelected = selectedGroupIds.contains(group.group.id)
            }

            groupsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            groupsRv.adapter = GroupsAdapter(groupsList!!, this)

            dialog!!.show()

            selectButton.setOnClickListener {
                if (groupsList!!.isNotEmpty()) {
                    selectedGroupIds.clear()
                    for (groupId in (groupsRv.adapter as GroupsAdapter).selectedIds) {
                        Timber.d("Selected id - $groupId")
                        selectedGroupIds.add(groupId)
                    }

                    showSelectedGroupNames((groupsRv.adapter as GroupsAdapter).selectedGroupNames)
                    Timber.d("Selected ids count - ${selectedGroupIds.size}")
                    dialog!!.dismiss()

                } else {
                    Timber.d("No groups available to select")
                }
            }

            cancelButton.setOnClickListener {
                dialog!!.dismiss()
            }

        } else {
            initCommonDialog()
        }
    }

    private fun showSelectedGroupNames(selectedNames: ArrayList<String>) {
        if (selectedNames.isNotEmpty()) {
            tv_groupNames.visibility = View.VISIBLE
            tv_groupNames.text = selectedNames[0]

            if(selectedNames.size - 1 != 0){
                fl_groupCount.visibility = View.VISIBLE
                tv_groupCount.text = "+${selectedNames.size - 1}"
            }else{
                fl_groupCount.visibility = View.GONE
            }

            return
        } else {
            tv_groupNames.visibility = View.GONE
            fl_groupCount.visibility = View.GONE
            return
        }
    }

    private fun showSelectedLightNames(selectedNames: ArrayList<String>) {
        if (selectedNames.isNotEmpty()) {
            tv_lightNames.visibility = View.VISIBLE
            tv_lightNames.text = selectedNames[0]

            if(selectedNames.size - 1 != 0){
                fl_lightCount.visibility = View.VISIBLE
                tv_lightCount.text = "+${selectedNames.size - 1}"
            }else{
                fl_lightCount.visibility = View.GONE
            }
            return
        } else {
            tv_lightNames.visibility = View.GONE
            fl_lightCount.visibility = View.GONE
            return
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
        requestToBindService(this)
    }

    fun init() {
        initClickListeners()
        initViewModelAndObservers()
        initBundleData()
        getScheduleDetails()
        getAllLights()
        getAllGroups()
        updateUi()
        initCommonDialog()
        initTimePicker()
    }


    private fun getScheduleDetails() {
        if (scheduleId != (-1).toLong()) {
            schedule = viewModel!!.getSchedule(scheduleId)
        }
    }

    private fun updateUi() {
        initDaysRv()

        if (scheduleId != (-1).toLong() && schedule != null) {
            et_eventName.text = Editable.Factory.getInstance().newEditable(schedule!!.name)

            (rv_days.adapter as DaySelectionAdapter).setSelectedDays(Converters.toStringArrayList(schedule!!.days))

            selectedStartHour = schedule!!.startTime.substring(0, 2).toInt()
            selectedStartMinute = schedule!!.startTime.substring(3, 5).toInt()

            selectedEndHour = schedule!!.endTime.substring(0, 2).toInt()
            selectedEndMinute = schedule!!.endTime.substring(3, 5).toInt()

            tv_startTime.text = Utilities.getTime(schedule!!.startTime)
            tv_endTime.text = Utilities.getTime(schedule!!.endTime)

            selectedMacIds.clear()
            Converters.toStringArrayList(schedule!!.devices).forEach {
                selectedMacIds.add(it)
            }
            selectedGroupIds.clear()
            Converters.toIntArrayList(schedule!!.groups).forEach {
                selectedGroupIds.add(it.toLong())
            }

            setLightNames()
            setGroupNames()
            setStartAction(Converters.toStartAction(schedule!!.startAction))
            setEndAction(Converters.toEndAction(schedule!!.endAction))

            setStartActionUi()
            setEndActionUi()

            val macIds = HashSet<String>()
            macIds.addAll(selectedMacIds)

            selectedGroupIds.forEach { groupId ->
                viewModel!!.getGroupLights(groupId).forEach {
                    macIds.add(it.macId)
                }
            }

            startAction!!.selectedMacIds = macIds
            endAction!!.selectedMacIds = macIds

//            iv_delete.visibility = View.GONE
        }
    }

    private fun setStartAction(action: StartAction) {
        startAction = ActionDetails()
        startAction!!.rValue = Utilities.convertIntToHexString(action.start_red)
        startAction!!.gValue = Utilities.convertIntToHexString(action.start_green)
        startAction!!.bValue = Utilities.convertIntToHexString(action.start_blue)
        startAction!!.wValue = Utilities.convertIntToHexString(action.start_warm)
        startAction!!.cValue = Utilities.convertIntToHexString(action.start_cool)
        startAction!!.brightnessValue = action.start_brightness
        startAction!!.sceneValue = action.start_scene
        startAction!!.onOffValue = action.start_power.equals("ON")
    }

    private fun setEndAction(action: EndAction) {
        endAction = ActionDetails()
        endAction!!.rValue = Utilities.convertIntToHexString(action.end_red)
        endAction!!.gValue = Utilities.convertIntToHexString(action.end_green)
        endAction!!.bValue = Utilities.convertIntToHexString(action.end_blue)
        endAction!!.wValue = Utilities.convertIntToHexString(action.end_warm)
        endAction!!.cValue = Utilities.convertIntToHexString(action.end_cool)
        endAction!!.brightnessValue = action.end_brightness
        endAction!!.sceneValue = action.end_scene
        endAction!!.onOffValue = action.end_power.equals("ON")
    }

    private fun setLightNames() {
        lightsList!!.forEach {
            if (selectedMacIds.contains(it.light.macId)) {
                tv_lightNames.visibility = View.VISIBLE
                tv_lightNames.text = it.light.name

                if(selectedMacIds.size - 1 != 0){
                    fl_lightCount.visibility = View.VISIBLE
                    tv_lightCount.text = "+${selectedMacIds.size - 1}"
                }else{
                    fl_lightCount.visibility = View.GONE
                }

                return
            }
        }
    }

    private fun setGroupNames() {
        groupsList!!.forEach {
            if (selectedGroupIds.contains(it.group.id)) {
                tv_groupNames.visibility = View.VISIBLE
                tv_groupNames.text = it.group.name

                if(selectedGroupIds.size - 1 != 0){
                    fl_groupCount.visibility = View.VISIBLE
                    tv_groupCount.text = "+${selectedGroupIds.size - 1}"
                }else{
                    fl_groupCount.visibility = View.GONE
                }
                return
            }
        }
    }

    private fun initBundleData() {
        if (intent.extras != null) {
            scheduleId = intent.getLongExtra(BUNDLE_SCHEDULE_ID, -1)
        }
    }

    private fun initTimePicker() {
        timePickerDialog = TimePickerDialog(
                this,
                timePickerListener,
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                Calendar.getInstance().get(Calendar.MINUTE),
                false
        )
        timePickerDialog!!.setOnCancelListener {
            isSettingStartTime = false
        }
    }

    private fun initDaysRv() {
        rv_days.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_days.adapter = DaySelectionAdapter(this)
    }

    private fun deleteSchedule() {
        if (scheduleId != (-1).toLong()) {
            viewModel!!.deleteSchedule(scheduleId)
        }
    }

    private fun initClickListeners() {
        ll_selectLights.setOnClickListener(this)
        ll_selectGroups.setOnClickListener(this)
        ll_selectEndAction.setOnClickListener(this)
        ll_selectStartAction.setOnClickListener(this)
        ll_selectStartTime.setOnClickListener(this)
        ll_selectEndTime.setOnClickListener(this)
        bt_saveButton.setOnClickListener(this)
        iv_back.setOnClickListener(this)
//        iv_delete.setOnClickListener(this)
    }


    private fun setStartActionUi() {
        if (startAction != null) {
            tv_startAction.visibility = View.VISIBLE
            iv_startActionColor.visibility = View.GONE

            if (startAction?.onOffValue!!.not()) {
                tv_startAction.text = getString(R.string.off)
                return
            }

            if (startAction?.sceneValue != Scene.DEFAULT.value) {
                when (startAction?.sceneValue) {
                    Scene.RAINBOW.value -> {
                        tv_startAction.text = getString(R.string.label_rainbow)
                    }
                    Scene.PARTY_MODE.value -> {
                        tv_startAction.text = getString(R.string.label_party)
                    }
                    Scene.COLOR_BLAST.value -> {
                        tv_startAction.text = getString(R.string.label_color_blast)
                    }
                    Scene.CANDLE_LIGHT.value -> {
                        tv_startAction.text = getString(R.string.label_candle_light)
                    }
                    Scene.SUNSET.value -> {
                        tv_startAction.text = getString(R.string.label_sunset)
                    }
                    Scene.ENERGISE.value -> {
                        tv_startAction.text = getString(R.string.label_energise)
                    }
                    else -> {
                        tv_startAction.text = ""
                    }
                }
                return
            }

            if (startAction?.rValue.equals("00").not() ||
                    startAction?.gValue.equals("00").not() ||
                    startAction?.bValue.equals("00").not()) {
                tv_startAction.visibility = View.GONE
                iv_startActionColor.visibility = View.VISIBLE

                val fillColor = Color.parseColor("#${startAction?.rValue}${startAction?.gValue}${startAction?.bValue}")
                val background = GradientDrawable()
                background.setColor(fillColor)
                background.shape = GradientDrawable.OVAL
                iv_startActionColor.background = background

                return
            }

            if (startAction?.wValue.equals("00").not() ||
                    startAction?.cValue.equals("00").not()) {
                iv_startActionColor.visibility = View.VISIBLE

                tv_startAction.text = getString(R.string.label_daylight)

                var daylightValue = calculatePercentage(convertHexStringToInt(startAction?.cValue!!).toFloat())
                if (daylightValue > 50) {
                    val fillColor = Color.parseColor("#B9E5FB")
                    val background = GradientDrawable()
                    background.setColor(fillColor)
                    background.shape = GradientDrawable.OVAL
                    iv_startActionColor.background = background

                    return
                }else{
                    val fillColor = Color.parseColor("#FBAF17")
                    val background = GradientDrawable()
                    background.setColor(fillColor)
                    background.shape = GradientDrawable.OVAL
                    iv_startActionColor.background = background

                    return
                }
            }

            if (startAction?.brightnessValue != null) {
                tv_startAction.text = getString(R.string.brightness_openbrac) + startAction?.brightnessValue + getString(R.string.percent_closebrac)
                return
            }

        } else {
            tv_startAction.text = ""
        }
    }

    private fun setEndActionUi() {
        if (endAction != null) {
            tv_endAction.visibility = View.VISIBLE
            iv_endActionColor.visibility = View.GONE

            if (endAction?.onOffValue!!.not()) {
                tv_endAction.text = getString(R.string.off)
                return
            }

            if (endAction?.sceneValue != Scene.DEFAULT.value) {
                when (endAction?.sceneValue) {
                    Scene.RAINBOW.value -> {
                        tv_endAction.text = getString(R.string.label_rainbow)
                    }
                    Scene.PARTY_MODE.value -> {
                        tv_endAction.text = getString(R.string.label_party)
                    }
                    Scene.COLOR_BLAST.value -> {
                        tv_endAction.text = getString(R.string.label_color_blast)
                    }
                    Scene.CANDLE_LIGHT.value -> {
                        tv_endAction.text = getString(R.string.label_candle_light)
                    }
                    Scene.SUNSET.value -> {
                        tv_endAction.text = getString(R.string.label_sunset)
                    }
                    Scene.ENERGISE.value -> {
                        tv_endAction.text = getString(R.string.label_energise)
                    }
                    else -> {
                        tv_endAction.text = ""
                    }
                }
                return
            }

            if (endAction?.rValue.equals("00").not() ||
                    endAction?.gValue.equals("00").not() ||
                    endAction?.bValue.equals("00").not()) {
                tv_endAction.visibility = View.GONE
                iv_endActionColor.visibility = View.VISIBLE

                val fillColor = Color.parseColor("#${endAction?.rValue}${endAction?.gValue}${endAction?.bValue}")
                val background = GradientDrawable()
                background.setColor(fillColor)
                background.shape = GradientDrawable.OVAL
                iv_endActionColor.background = background
                return
            }

            if (endAction?.wValue.equals("00").not() ||
                    endAction?.cValue.equals("00").not()) {
                iv_endActionColor.visibility = View.VISIBLE

                tv_endAction.text = getString(R.string.label_daylight)

                var daylightValue = calculatePercentage(convertHexStringToInt(endAction?.cValue!!).toFloat())
                if (daylightValue > 50) {
                    val fillColor = Color.parseColor("#B9E5FB")
                    val background = GradientDrawable()
                    background.setColor(fillColor)
                    background.shape = GradientDrawable.OVAL
                    iv_endActionColor.background = background

                    return
                }else{
                    val fillColor = Color.parseColor("#FBAF17")
                    val background = GradientDrawable()
                    background.setColor(fillColor)
                    background.shape = GradientDrawable.OVAL
                    iv_endActionColor.background = background

                    return
                }
            }

            if (endAction?.brightnessValue != null) {
                tv_endAction.text = getString(R.string.brightness_openbrac) + endAction?.brightnessValue + getString(R.string.percent_closebrac)
                return
            }

            tv_endAction.text = ""

        } else {
            tv_endAction.text = ""
        }
    }

    private fun calculatePercentage(value: Float): Float {
        return (value / 255) * 100
    }
}

class ActionDetails : Serializable {
    var rValue: String? = null
    var gValue: String? = null
    var bValue: String? = null
    var wValue: String? = null
    var cValue: String? = null
    var sceneValue: Int = Scene.DEFAULT.value
    var brightnessValue: Int? = null
    var onOffValue: Boolean? = null
    var selectedMacIds: HashSet<String>? = null
}
