package com.svarochi.wifi.ui.groupcontrol.view

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.SeekBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.ScreenUtils
import com.jakewharton.rxbinding2.widget.RxSeekBar
import com.jakewharton.rxbinding2.widget.SeekBarProgressChangeEvent
import com.jakewharton.rxbinding2.widget.SeekBarStartChangeEvent
import com.jakewharton.rxbinding2.widget.SeekBarStopChangeEvent
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.colorpicker.WifiColorPickerDialog
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_GROUP_ID
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_ALL
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_BRIGHTNESS_AND_COLOR
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_BRIGHTNESS_AND_DAYLIGHT
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_BRIGHTNESS_ONLY
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_COLOR_ONLY
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_DAYLIGHT_AND_COLOR
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_DAYLIGHT_ONLY
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_NONE
import com.svarochi.wifi.common.Constants.Companion.GROUP_TRANSITION_DELAY
import com.svarochi.wifi.common.Constants.Companion.MAX_LIGHTS_IN_GROUP
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.groupcontrol.viewmodel.GroupControlViewModel
import com.svarochi.wifi.ui.lightcontrol.view.fragment.ImagePickerDialogFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_group_control.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue


class GroupControlActivity : WifiBaseActivity(), View.OnClickListener, ImagePickerDialogFragment.ColorPickerInterface,
        ServiceConnectionCallback {
    override fun serviceConnected() {
        initViewModel()
        initViews()
        initListeners()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            iv_back.id -> {
                //setResult(Activity.RESULT_OK, Intent().putExtra(BUNDLE_GROUP, group))
                finish()
            }
            ll_rainbow.id -> {
                setScene(SceneType.RAINBOW)
            }
            tv_study.id -> {
                setScene(SceneType.STUDY)
            }
            ll_energise.id -> {
                setScene(SceneType.ENERGISE)
            }
            tv_aqua.id -> {
                setScene(SceneType.AQUA)
            }
            ll_candleLight.id -> {
                setScene(SceneType.CANDLE_LIGHT)
            }
            ll_wc_candleLight.id -> {
                setScene(SceneType.CANDLE_LIGHT)
            }
            tv_moonLight.id -> {
                setScene(SceneType.MOON_LIGHT)
            }
            ll_partyMode.id -> {
                setScene(SceneType.PARTY_MODE)
            }
            ll_sunset.id -> {
                setScene(SceneType.SUNSET)
            }
            tv_meditation.id -> {
                setScene(SceneType.MEDITATION)
            }
            ll_colorBlast.id -> {
                setScene(SceneType.COLOR_BLAST)
            }
            tv_colorPalette.id -> {
                showColorPalette()
            }
            tv_photoGallery.id -> {
                openPhotoGallery()
            }
            tv_camera.id -> {
                openCamera()
            }
            iv_editGroupName.id -> {
                showEditGroupNameDialog()
            }
        }
    }

    private fun showEditGroupNameDialog() {
        if (dialog != null) {
            if (group != null) {
                dialog!!.setContentView(R.layout.dialog_edit_group_name)
                dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

                val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
                val okButton: Button = dialog!!.findViewById(R.id.bt_okButton)
                val nameField: EditText = dialog!!.findViewById(R.id.et_name)
                nameField.text = Editable.Factory.getInstance().newEditable(group!!.name)
                nameField.requestFocus()

                okButton.setOnClickListener {
                    if (nameField.text.trim().isNotEmpty()) {
                        dialog!!.dismiss()
                        if (nameField.text.equals(group!!.name).not() && isNameAlreadyExisting(nameField.text.toString()).not())
                            editGroupName(group!!.id!!, nameField.text.toString())
                    } else {
                        Timber.d("Edit network name - No name entered")
                        showToast(getString(R.string.toast_name_cannot_be_empty))
                    }
                }

                cancelButton.setOnClickListener {
                    dialog!!.dismiss()
                }

                dialog!!.show()

            } else {
                // Should not occur
                Timber.d("Group value not initialised")
            }

        } else {
            // Should not occur
            Timber.d("Dialog not initialised")
        }
    }

    private fun isNameAlreadyExisting(newName: String): Boolean {
        groupNames.forEach {
            if (it.equals(newName)) {
                return true
            }
        }
        return false
    }

    private fun editGroupName(groupId: Long, name: String) {
        viewModel.editGroupName(groupId, name)
    }

    private fun setScene(sceneType: SceneType) {
        if (cb_onOffGroup.isChecked.not()) {
            cb_onOffGroup.isChecked = true
        }
        when (sceneType) {
            SceneType.ENERGISE -> {

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null


                ll_energise.setBackgroundResource(R.drawable.today_tomorrow_selected)
                viewModel.setGroupScene(
                        groupId,
                        sceneType
                )

            }
            SceneType.CANDLE_LIGHT -> {

                ll_rainbow.background = null
                ll_energise.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null


                ll_candleLight.setBackgroundResource(R.drawable.today_tomorrow_selected)
                viewModel.setGroupScene(
                        groupId,
                        sceneType
                )

            }
            SceneType.SUNSET -> {

                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null


                ll_sunset.setBackgroundResource(R.drawable.today_tomorrow_selected)

                viewModel.setGroupScene(
                        groupId,
                        sceneType
                )
            }
            SceneType.COLOR_BLAST -> {

                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_partyMode.background = null
                // TODO - Dynamic scene feature can be turned off


                if (ll_colorBlast.background != null) {
                    // TODO - Color blast is already set so we need to turn off
                    ll_colorBlast.background = null
                    Timber.d("Color blast is already set, turning off color blast")
                    viewModel.turnOffDynamicScene(groupId)

                } else {
                    // TODO - Color blast is not set so we need to turn on
                    ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                    Timber.d("Color blast is not set, setting color blast scene")

                    viewModel.setGroupScene(
                            groupId,
                            sceneType
                    )
                }

            }
            SceneType.PARTY_MODE -> {
                ll_rainbow.background = null
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null


                if (ll_partyMode.background != null) {
                    // Party mode is already set so we need to turn off
                    ll_partyMode.background = null
                    Timber.d("Party mode is already set, turning off Party mode")
                    viewModel.turnOffDynamicScene(groupId)

                } else {
                    // Party mode is not set so we need to turn on
                    ll_partyMode.setBackgroundResource(R.drawable.today_tomorrow_selected)
                    Timber.d("Party mode is not set, setting color Party mode")

                    viewModel.setGroupScene(
                            groupId,
                            sceneType
                    )
                }
            }
            SceneType.RAINBOW -> {
                ll_energise.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_partyMode.background = null


                if (ll_rainbow.background != null) {
                    // Rainbow is already set so we need to turn off
                    ll_rainbow.background = null
                    Timber.d("Rainbow is already set, turning off Rainbow")
                    viewModel.turnOffDynamicScene(groupId)

                } else {
                    // Rainbow is not set so we need to turn on
                    ll_rainbow.setBackgroundResource(R.drawable.today_tomorrow_selected)
                    Timber.d("Rainbow is not set, setting color Rainbow")

                    viewModel.setGroupScene(
                            groupId,
                            sceneType
                    )
                }

            }
        }
    }

    private fun resetScene() {
        ll_energise.background = null
        ll_candleLight.background = null
        ll_sunset.background = null
        ll_colorBlast.background = null
        ll_partyMode.background = null
        ll_rainbow.background = null
    }

    private var isSelfProfile: Boolean = true
    private lateinit var viewModel: GroupControlViewModel
    private var dialog: Dialog? = null
    private var groupId: Long = 0
    private var networkId: Long = 0
    private var lightsList: ArrayList<CustomLight>? = null
    private var mColorPickerDialog: WifiColorPickerDialog? = null
    private var group: Group? = null
    private val disposables: CompositeDisposable = CompositeDisposable()
    private var groupNames: ArrayList<String> = ArrayList()

    val MAX = 100
    val MIN = 10
    val STEP = 1

    /*** For delay in color picker ***/
    var lastColorSentTime: Long = 0
    var lastSentColor = "000000"
    /*********************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_control)
        requestToBindService(this)
    }

    private fun initListeners() {
        iv_back.setOnClickListener(this)
        iv_editGroupName.setOnClickListener(this)
        tv_aqua.setOnClickListener(this)
        ll_candleLight.setOnClickListener(this)
        tv_moonLight.setOnClickListener(this)
        ll_rainbow.setOnClickListener(this)
        ll_sunset.setOnClickListener(this)
        ll_partyMode.setOnClickListener(this)
        tv_meditation.setOnClickListener(this)
        ll_energise.setOnClickListener(this)
        tv_study.setOnClickListener(this)
        ll_colorBlast.setOnClickListener(this)
        tv_colorPalette.setOnClickListener(this)
        tv_photoGallery.setOnClickListener(this)
        tv_camera.setOnClickListener(this)
        ll_wc_candleLight.setOnClickListener(this)

        //sb_brightness.setOnSeekBarChangeListener(brightnessSeekChangeListener)
        //sb_daylightControl.setOnSeekBarChangeListener(daylightSeekChangeListener)
        cb_onOffGroup.setOnClickListener {
            if (cb_onOffGroup.isChecked) {
                turnOnGroup()
            } else {
                turnOffGroup()
            }
        }
    }

    private var daylightSeekChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            if (seekBar != null) {
                setGroupDaylight(seekBar.progress)
            } else {
                Timber.d("Daylight seekbar not initialised")
            }

        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

        }
    }

    private var brightnessSeekChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            if (seekBar != null) {
                var value = Math.round((seekBar.progress.toDouble()) * (MAX - MIN) / 100)
                var displayValue = (value.toInt() + MIN) / STEP * STEP

                Timber.d("Group Brightness - ${displayValue}")
                setGroupBrightness(displayValue)
            } else {
                Timber.d("Seekbar not present")
            }
        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        }
    }


    private fun setGroupDaylight(progress: Int) {
        if (cb_onOffGroup.isChecked.not()) {
            cb_onOffGroup.isChecked = true
        }
        viewModel.setGroupDaylight(groupId, progress)
    }

    private fun setGroupBrightness(progress: Int) {
        if (cb_onOffGroup.isChecked.not()) {
            cb_onOffGroup.isChecked = true
        }
        viewModel.setGroupBrightness(groupId, progress)
    }

    private fun showColorPalette() {
        if (mColorPickerDialog == null) {
            initColorPicker()
        }
        lastSentColor = "000000"
        mColorPickerDialog!!.show()
    }

    /* private var colorPickerListener = object : ColorPickerPopup.ColorPickerObserver() {
         override fun onColorPicked(color: Int) {
             Timber.d("User selected from palette - ${Utilities.convertColorToHexString(color)} color")
             //setGroupColor(Utilities.convertColorToHexString(color))
         }
     }*/

    private fun openPhotoGallery() {
        var fm = getSupportFragmentManager()
        var imagePickerFragment = ImagePickerDialogFragment.newInstance(Constants.PHOTO_GALLERY.absoluteValue)
        lastSentColor = "000000"
        imagePickerFragment.show(fm, "image_picker_fragment")
    }

    private fun openCamera() {
        var fm = getSupportFragmentManager()
        var imagePickerFragment = ImagePickerDialogFragment.newInstance(Constants.CAMERA.absoluteValue)
        lastSentColor = "000000"
        imagePickerFragment.show(fm, "image_picker_fragment")

    }

    override fun onColorSelected(color: String) {
        // From Photo gallery
        Timber.d("User selected from gallery - $color color")
        setGroupColor(color, false)
    }

    override fun onForceColorSelected(color: String) {
        // From Photo gallery
        Timber.d("User selected from gallery - $color color")
        setGroupColor(color, true)
    }


    private fun setGroupColor(color: String, isForced: Boolean) {
        // Sample input - FFFF00

        if (cb_onOffGroup.isChecked.not()) {
            cb_onOffGroup.isChecked = true
        }

        if (lastSentColor.equals(color).not()) {
            if (isForced) {
                Timber.d("Color - $color sent to group $groupId")
                lastSentColor = color
                viewModel.setGroupColor(
                        groupId,
                        r_value = color.substring(0, 2),
                        g_value = color.substring(2, 4),
                        b_value = color.substring(4)
                )
            } else {
                var currentTime = System.currentTimeMillis()
                if ((currentTime - lastColorSentTime) >= GROUP_TRANSITION_DELAY) {
                    lastColorSentTime = currentTime
                    Timber.d("Color - $color sent to group $groupId")
                    lastSentColor = color
                    viewModel.setGroupColor(
                            groupId,
                            r_value = color.substring(0, 2),
                            g_value = color.substring(2, 4),
                            b_value = color.substring(4)
                    )
                }
            }
        }
    }


    private fun turnOffGroup() {
        resetScene()
        viewModel.turnOffGroup(groupId)
    }

    private fun turnOnGroup() {
        viewModel.turnOnGroup(groupId)
    }

    private fun initViews() {
        groupId = intent.getLongExtra(BUNDLE_GROUP_ID, 0)
        groupNames = intent.getStringArrayListExtra(Constants.BUNDLE_GROUP_NAMES)

        var preference = SharedPreference(this)
        isSelfProfile = preference.getCurrentProfile().equals(Constants.CURRENT_PROFILE_SELF)

        if (isSelfProfile) {
            iv_editGroupName.visibility = VISIBLE
        } else {
            iv_editGroupName.visibility = GONE
        }

        observeGroupTable()
        initColorPicker()

        rv_groupLightsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        dialog = Dialog(this@GroupControlActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.dialog_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        //observeGroupLightsTable()

        listenToBrightnessChangeEvents()
        listenToDaylightChangeEvents()
    }

    private fun observeGroupTable() {
        if (groupId != 0.toLong()) {
            viewModel.getGroupLiveData(groupId).observe(this, Observer {
                if (it != null) {
                    group = it!!
                    updateUi(it)
                    if (networkId != 0.toLong()) {
                        getLightsAndMarkThoseOfGroup()
                    }
                } else {
                    Timber.d("No group exists in DB for groupId($groupId)")
                }
            })

        } else {
            Timber.d("Group Id is 0")
        }
    }

    private fun initColorPicker() {
        if (mColorPickerDialog == null) {
            mColorPickerDialog = WifiColorPickerDialog.createColorPickerDialog(this)
            mColorPickerDialog!!.hideColorComponentsInfo()
            mColorPickerDialog!!.hideOpacityBar()
            mColorPickerDialog!!.hideHexaDecimalValue()
            mColorPickerDialog!!.hidePreviewBox()
            mColorPickerDialog!!.setOnColorPickedListener(onColorPickedListener)

        } else {
            Timber.d("Color picker already initialised")
        }
    }

    private var onColorPickedListener = object : WifiColorPickerDialog.OnColorPickedListener {
        override fun onForceColorPicked(color: Int, hexVal: String?) {
            Timber.d("Color picked from color palette - ${Utilities.convertColorToHexString(color)}")
            setGroupColor(Utilities.convertColorToHexString(color), true)
        }

        override fun onColorPicked(color: Int, hexVal: String) {
            Timber.d("Color picked from color palette - ${Utilities.convertColorToHexString(color)}")
            setGroupColor(Utilities.convertColorToHexString(color), false)
        }

    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(GroupControlViewModel::class.java)
        viewModel.getResponse().observe(this, Observer { t -> processResponse(t) })
    }

    private fun processResponse(event: Event?) {
        when (event!!.type) {

            EventType.CONNECTION -> {
                when (event.status) {
                    /* EventStatus.LOADING -> {
                         Timber.d("Connecting to group - ${group.id}")
                         showPopup(true, getString(R.string.label_connecting_to_light))
                     }
                     EventStatus.SUCCESS -> {
                         Timber.d("Successful connection")
                         showPopup(false, null)
                     }
                     EventStatus.ERROR -> {
                         Timber.d("Failed to connect to some lights of group(${group.id})")
                         Timber.e(event.error)
                         showPopup(false, null)
                         //showLightControlPage(false)
                     }*/
                    EventStatus.FAILURE -> {
                        var macId = event.data as String
                        showPopup(false, null)
                        //showToast(lightName + getString(R.string.toast_is_not_reachable))
                        Timber.d("Connection lost with device - ${macId}")
                    }
                    /*EventStatus.WIFI -> {
                        if (!(event.data as Boolean)) {
                            showPopup(false, null)
                            showToast(this, getString(R.string.toast_some_devices_not_reachable))
                        }
                    }*/
                }
            }
            EventType.SET_GROUP_COLOR -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting color to group($groupId)")
//                        showPopup(true, getString(R.string.popup_setting_color_to_lights))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Color successfully set to group($groupId)")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_successfully_set_color_to_lights_of_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set color to group($groupId)")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_color_couldnt_be_set))
                        //showLightControlPage(false)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set color to group($groupId)")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_color_couldnt_be_set))
                        //showLightControlPage(false)
                    }
                }
            }
            EventType.SET_GROUP_SCENE -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting scene to group($groupId)")
//                        showPopup(true, getString(R.string.popup_setting_scene_to_lights))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully set scene to group($groupId)")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_successfully_set_scene_to_lights_of_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set scene to group(${groupId})")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set scene to group(${groupId})")
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                }
            }
            EventType.GET_LIGHTS_OF_GROUP -> {
                when (event.status) {
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.LOADING -> {
                        Timber.d("Fetching lights of group")
//                        showPopup(true, getString(R.string.popup_fetching_lights))
                    }
                    EventStatus.SUCCESS -> {
                        showPopup(false, null)

                        var responseLightsList =
                                (event.data!! as ArrayList<CustomLight>)

                        lightsList = responseLightsList

                        initGroupLightsRv(responseLightsList)
                    }
                    EventStatus.ERROR -> {
                        showPopup(false, null)
                        showToast(getString(R.string.toast_failed_to_fetch_lights))
                        Timber.e(event.error)
                    }
                }
            }
            EventType.TURN_ON_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning on group(${groupId})")
//                        showPopup(true, getString(R.string.popup_turning_on_lights_of_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully turned on group(${groupId})")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_successfully_turned_on_the_lights_of_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to turn on group(${groupId})")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn on group(${groupId})")
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                }
            }
            EventType.TURN_OFF_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning off group(${groupId})")
//                        showPopup(true, getString(R.string.popup_turning_off_lights_of_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully turned off group(${groupId})")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_successfully_turned_off_the_lights_of_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to turn off group(${groupId})")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn off group(${groupId})")
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                }
            }
            EventType.SET_GROUP_BRIGHTNESS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting brightness of group(${groupId})")
                        //showPopup(true, getString(R.string.popup_setting_brightness_to_lights_of_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully turned on group(${groupId})")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_successfully_set_brightness_to_lights_of_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set brightness of group(${groupId})")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set brightness of group(${groupId})")
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                }
            }
            EventType.SET_GROUP_DAYLIGHT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Setting daylight of group(${groupId})")
                        //showPopup(true, getString(R.string.popup_setting_daylight_to_lights_of_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully set daylight of group(${groupId})")
                        showPopup(false, null)
                        //showToast(getString(R.string.toast_successfully_set_daylight_to_lights_of_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set daylight of group(${groupId})")
                        Timber.e(event.error)
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set daylight of group(${groupId})")
                        showPopup(false, null)
                        //showLightControlPage(false)
                    }
                }
            }
            EventType.ADD_LIGHT_TO_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Adding light to group")
                        showPopup(true, getString(R.string.popup_adding_light_to_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully added light to group")
                        showPopup(false, null)
                        showToast(getString(R.string.toast_successfully_added_light_to_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to add light to group")
                        showPopup(false, null)
                        showToast(getString(R.string.toast_failed_add_light_to_group))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to add light to group")
                        showPopup(false, null)
                        showToast(getString(R.string.toast_failed_add_light_to_group))
                        if (event.data != null) {
                            if (event.data is String) {
                                resetCheckboxOfMacId(event.data as String)
                            }

                            if (event.data is HashMap<*, *>) {
                                showToast(getString(R.string.no_internet))
                                resetCheckboxOfMacId((event.data as HashMap<Int, String>)[NO_INTERNET_AVAILABLE]!!)
                            }
                        }
                    }
                }
            }
            EventType.REMOVE_LIGHT_FROM_GROUP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Removing light from group")
                        showPopup(true, getString(R.string.popup_removing_light_from_group))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully removed light from group")
                        showPopup(false, null)
                        showToast(getString(R.string.toast_successfully_removed_light_from_group))
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to remove light from group")
                        showPopup(false, null)
                        showToast(getString(R.string.toast_failed_remove_light_from_group))
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to remove light from group")
                        showPopup(false, null)
                        showToast(getString(R.string.toast_failed_remove_light_from_group))
                        if (event.data != null) {
                            if (event.data is String) {
                                resetCheckboxOfMacId(event.data as String)
                            }

                            if (event.data is HashMap<*, *>) {
                                showToast(getString(R.string.no_internet))
                                resetCheckboxOfMacId((event.data as HashMap<Int, String>)[NO_INTERNET_AVAILABLE]!!)
                            }
                        }
                    }
                }
            }
            EventType.EDIT_GROUP_NAME -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Editing group name")
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully edited group name")
                        showPopup(false, null)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to edit name of the group")
                        showPopup(false, null)
                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == ALREADY_EXISTS) {
                                    showToast(getString(R.string.toast_network_by_that_name_already_exists))
                                }
                            }

                            if (event.data is HashMap<*, *>) {
                                showToast(getString(R.string.no_internet))
                                resetCheckboxOfMacId((event.data as HashMap<Int, String>)[NO_INTERNET_AVAILABLE]!!)
                            }
                        }
                    }
                }
            }
        }
    }


    private fun resetCheckboxOfMacId(macId: String) {
        if (rv_groupLightsRv.adapter != null) {
            (rv_groupLightsRv.adapter as GroupControlLightsAdapter).resetCheckBoxOfMacId(macId)

        }
    }

    private fun updateUi(group: Group) {
        tv_groupHeaderName.text = group.name
        tv_groupName.text = group.name
        networkId = group.networkId

        when (group.groupTag) {

            GROUP_TAG_COLOR_ONLY -> {
                showColorTypeControls()
            }
            GROUP_TAG_BRIGHTNESS_ONLY -> {
                showBrightAndDimControls()
            }
            GROUP_TAG_DAYLIGHT_ONLY -> {
                showWarmAndCoolControls()
            }
            GROUP_TAG_BRIGHTNESS_AND_COLOR -> {
                showBrightAndDimControls()
            }
            GROUP_TAG_BRIGHTNESS_AND_DAYLIGHT -> {
                showBrightAndDimControls()
            }
            GROUP_TAG_DAYLIGHT_AND_COLOR -> {
                showWarmAndCoolControls()
            }
            GROUP_TAG_ALL -> {
                showBrightAndDimControls()
            }
            GROUP_TAG_NONE -> {
                hideAllControls()
            }
            else -> {
                hideAllControls()
            }
        }

        /*cb_onOffGroup.isChecked = group.onOffValue
        sb_brightness.setProgress(group.brightnessValue.toFloat())

        var daylightValue = calculatePercentage(convertHexStringToInt(group.cValue).toFloat())
        if ((daylightValue).roundToInt() < 10) {
            sb_daylightControl.setProgress((10).toFloat())
        } else {
            sb_daylightControl.setProgress(((daylightValue).roundToInt()).toFloat())
        }*/
    }

    private fun showWarmAndCoolControls() {
        ll_colorControl.visibility = GONE
        ll_allEffectsControl.visibility = GONE
        ll_warmAndCoolEffectsControl.visibility = GONE

        ll_nameLayout.visibility = VISIBLE
        ll_brightnessControl.visibility = VISIBLE
        ll_allLightsControl.visibility = VISIBLE
        ll_daylightControl.visibility = VISIBLE
    }

    private fun showBrightAndDimControls() {
        ll_warmAndCoolEffectsControl.visibility = GONE
        ll_daylightControl.visibility = GONE
        ll_colorControl.visibility = GONE
        ll_allEffectsControl.visibility = GONE

        ll_nameLayout.visibility = VISIBLE
        ll_brightnessControl.visibility = VISIBLE
        ll_allLightsControl.visibility = VISIBLE
    }

    private fun showColorTypeControls() {
        ll_warmAndCoolEffectsControl.visibility = GONE

        ll_nameLayout.visibility = VISIBLE
        ll_brightnessControl.visibility = VISIBLE
        ll_daylightControl.visibility = VISIBLE
        ll_colorControl.visibility = VISIBLE
        ll_allEffectsControl.visibility = VISIBLE
        ll_allLightsControl.visibility = VISIBLE
    }

    private fun hideAllControls() {
        ll_nameLayout.visibility = GONE
        ll_brightnessControl.visibility = GONE
        ll_daylightControl.visibility = GONE
        ll_colorControl.visibility = GONE
        ll_allEffectsControl.visibility = GONE
        ll_warmAndCoolEffectsControl.visibility = GONE
        ll_allLightsControl.visibility = GONE

    }

    private fun calculatePercentage(value: Float): Float {
        return (value / 255) * 100
    }

    private fun getLightsAndMarkThoseOfGroup() {
        viewModel.getLightsOfGroup(groupId, networkId)
    }

    private fun initGroupLightsRv(lightsList: ArrayList<CustomLight>) {
        rv_groupLightsRv.adapter = GroupControlLightsAdapter(lightsList, isSelfProfile, this)
    }

    private fun showPopup(show: Boolean, message: String?) {
        showLoadingPopup(show, message)
    }

    fun addLightToGroup(macId: String) {
        var totalSelectedLights = 0
        if (lightsList != null) {
            for (light in lightsList!!) {
                if (light.isSelected()) {
                    totalSelectedLights++
                }
            }
            if (totalSelectedLights <= MAX_LIGHTS_IN_GROUP) {
                viewModel.addLightToGroup(macId, groupId)
            } else {
                showToast("Cannot add more than $MAX_LIGHTS_IN_GROUP to a group")
                resetCheckboxOfMacId(macId)
            }
        } else {
            Timber.d("The lights list is null")
        }
    }

    fun removeLightFromGroup(macId: String) {
        if (isSelectedLightListEmpty().not()) {
            viewModel.removeLightFromGroup(macId, groupId)
        } else {
            showToast(getString(R.string.toast_atleast_one_light_must_be_selected))
            resetCheckboxOfMacId(macId)
        }
    }

    private fun isSelectedLightListEmpty(): Boolean {
        if (lightsList != null) {
            for (light in lightsList!!) {
                if (light.isSelected()) {
                    return false
                }
            }
            return true
        } else {
            return true
        }
    }

    override fun onBackPressed() {
        //setResult(Activity.RESULT_OK, Intent().putExtra(BUNDLE_GROUP, groupId))
        finish()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun listenToBrightnessChangeEvents() {
        disposables.add(RxSeekBar.changeEvents(sb_brightness)
                .flatMap {
                    when (it) {
                        is SeekBarProgressChangeEvent -> {
                            if (it.fromUser()) {
                                var value = Math.round((it.progress().toDouble()) * (MAX - MIN) / 100)
                                var displayValue = (value.toInt() + MIN) / STEP * STEP
                                tvBubbleIntensityValue.text = "$displayValue%"
                                rlBubbleIntensity.setX((getIntensitySliderThumbPos() - ScreenUtils.dpToPx(13)).toFloat())
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Start")
                            rlBubbleIntensity.visibility = View.VISIBLE
                        }
                        is SeekBarStopChangeEvent -> {
                            rlBubbleIntensity.visibility = View.GONE
                        }
                    }
                    Observable.just(it)
                }
                .debounce(Constants.GROUP_TRANSITION_DELAY.toLong(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekBarChangeEvent ->
                    when (seekBarChangeEvent) {
                        is SeekBarProgressChangeEvent -> {
                            Timber.d("Change Events - Progress from user = ${seekBarChangeEvent.fromUser()}")
                            Timber.d("Change Events - Progress = ${seekBarChangeEvent.progress()}")

                            if (seekBarChangeEvent.fromUser()) {
                                var value = Math.round((seekBarChangeEvent.progress().toDouble()) * (MAX - MIN) / 100)
                                var displayValue = (value.toInt() + MIN) / STEP * STEP
                                setGroupBrightness(displayValue)
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Start")
                        }
                        is SeekBarStopChangeEvent -> {
                            Timber.d("Change Events - Stop = ${seekBarChangeEvent.view().progress}")
                            Timber.d("Change Events - Stop")

                            var value = Math.round((seekBarChangeEvent.view().progress.toDouble()) * (MAX - MIN) / 100)
                            var displayValue = (value.toInt() + MIN) / STEP * STEP
                            setGroupBrightness(displayValue)
                        }
                    }
                }
        )
    }

    private fun getIntensitySliderThumbPos(): Int {
        val width = (sb_brightness.getWidth()
                - sb_brightness.getPaddingLeft()
                - sb_brightness.getPaddingRight())
        return sb_brightness.getPaddingLeft() + width * sb_brightness.getProgress() / sb_brightness.getMax() + sb_brightness.getThumb().getIntrinsicWidth() / 2
    }

    private fun listenToDaylightChangeEvents() {
        disposables.add(RxSeekBar.changeEvents(sb_daylightControl)
                .debounce(Constants.GROUP_TRANSITION_DELAY.toLong(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekBarChangeEvent ->
                    when (seekBarChangeEvent) {
                        is SeekBarProgressChangeEvent -> {
                            Timber.d("Change Events - Daylight Progress from user = ${seekBarChangeEvent.fromUser()}")
                            Timber.d("Change Events - Daylight Progress = ${seekBarChangeEvent.progress()}")

                            if (seekBarChangeEvent.fromUser()) {
                                setGroupDaylight(seekBarChangeEvent.progress())
                            }
                        }
                        is SeekBarStartChangeEvent -> {
                            Timber.d("Change Events - Daylight Start")
                        }
                        is SeekBarStopChangeEvent -> {
                            Timber.d("Change Events - Daylight Stop = ${seekBarChangeEvent.view().progress}")
                            Timber.d("Change Events - Daylight Stop")

                            setGroupDaylight(seekBarChangeEvent.view().progress)
                        }
                    }
                }
        )
    }

}
