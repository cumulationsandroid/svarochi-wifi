package com.svarochi.wifi.ui.userinvitations.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.Profile
import com.svarochi.wifi.database.entity.ProfileProject
import com.svarochi.wifi.model.api.response.ApiProfile
import com.svarochi.wifi.model.api.response.CheckUserResponse
import com.svarochi.wifi.model.api.response.GetSharedProfilesResponse
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class UserInvitationsRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl {
    private var userId: String? = null
    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var sharedPreference: SharedPreference? = null
    private var TAG = "UserInvitationsRepository ---->"

    init {
        initCommunicationResponseObserver()
        fetchIdsFromPreference()
        fetchUserInvitationsFromServer()
    }

    private fun fetchUserInvitationsFromServer() {
        disposables.add(
                ApiRequestParser.getSharedProfiles(userId!!)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetSharedProfilesResponse>>() {
                            override fun onComplete() {
                                Timber.d("$TAG Get Shared profiles api completed")
                            }

                            override fun onNext(response: Response<GetSharedProfilesResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("$TAG Response body - ${response.body()}")
                                        if (response.body()!!.status.equals("success", true)) {
                                            if (response.body()!!.records != null) {
                                                Timber.d("$TAG Successfully got shared profiles of ($userId) from server")
                                                storeSharedProfilesInLocalDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("$TAG Something went wrong while getting shared profiles of ($userId) from server")
                                                Timber.d("$TAG No records received")
                                            }
                                        } else {
                                            Timber.d("$TAG Something went wrong while getting shared profiles of ($userId) from server")
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.d("$TAG Error message - ${errorMessage}")
                                            } else {
                                                Timber.d("$TAG No Error message received")
                                            }
                                        }

                                    } else {
                                        Timber.d("$TAG Something went wrong while getting shared profiles of ($userId) from server")
                                        Timber.d("$TAG No response body received")
                                    }
                                } else {
                                    Timber.d("$TAG Something went wrong while getting shared profiles of ($userId) from server")
                                    Timber.d("$TAG Response code - ${response.code()}")
                                    Timber.d("$TAG Response body - ${response.body()}")
                                    Timber.d("$TAG Response error body - ${response.errorBody()}")
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("$TAG Something went wrong while getting shared profiles of ($userId) from server")
                                Timber.e(error)
                                error.printStackTrace()
                            }

                        })
        )
    }

    private fun storeSharedProfilesInLocalDb(apiProfileList: ArrayList<ApiProfile>) {
        /**
         * 1. Clear all the profiles which are shared by $userId
         * 2. Add all the profiles in db
         * 3. Since profiles are removed, the corresponding ProfileProject table will be cleared
         * 4. Add the new profileProjects into ProfileProject table
         * */
        application.wifiLocalDatasource.clearProfilesSharedBy(userId!!)
        Timber.d("$TAG Cleared all the profiles shared by $userId")

        for(apiProfile in apiProfileList) {

            var profileToAdd = Profile(apiProfile.profile_id, apiProfile.profile_name, apiProfile.shared_by,apiProfile.shared_with_data.user_id, Converters.toString(apiProfile.shared_with_data))
            application.wifiLocalDatasource.addNewProfile(profileToAdd)
            Timber.d("$TAG Added profiles into profiles table")

            var profileProjectList = ArrayList<ProfileProject>()

            for (sharingProject in apiProfile.projects) {
                for (sharingNetwork in sharingProject.networks) {
                    var profileProjectToAdd = ProfileProject()

                    profileProjectToAdd.profileId = apiProfile.profile_id
                    profileProjectToAdd.projectId = sharingProject.project_id
                    profileProjectToAdd.networkId = sharingNetwork

                    profileProjectList.add(profileProjectToAdd)
                }
            }

            application.wifiLocalDatasource.addProfileProjects(profileProjectList)
            Timber.d("$TAG Added ProfileProjects into ProfileProjects table")

            Timber.d("$TAG Successfully added shared profiles in local db")
        }
    }

    private fun fetchIdsFromPreference() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        userId = sharedPreference!!.getUserId()
    }

    override fun initCommunicationResponseObserver() {
        // No implementation as not communicating with device
    }

    override fun getCommunicationResponseObserver(): PublishSubject<Event> = responseObservable

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    fun getSharedProfilesLiveData(): LiveData<List<Profile>> {
        if (userId != null) {
            return application.wifiLocalDatasource.getProfilesSharedBy(userId!!)
        } else {
            // Should not occur
            Timber.d("$TAG userId is null")
            return MutableLiveData<List<Profile>>()
        }
    }

    fun checkIfUserExists(newUserId: String) {
        if(GeneralUtils.isInternetAvailable(application)) {
            disposables.add(ApiRequestParser.checkUser(newUserId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableObserver<Response<CheckUserResponse>>() {
                        override fun onComplete() {
                            Timber.d("$TAG Completed check user api")
                        }

                        override fun onNext(response: Response<CheckUserResponse>) {
                            if (response.isSuccessful) {
                                if (response.body() != null) {
                                    Timber.d("$TAG Check user API response body - ${response.body().toString()}")
                                    if (response.body()!!.status is Boolean) {
                                        if (response.body()!!.status as Boolean) {
                                            Timber.d("$TAG User($newUserId) exists in the server")
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.CHECK_USER,
                                                            EventStatus.SUCCESS,
                                                            (response.body() as CheckUserResponse),
                                                            null
                                                    )
                                            )
                                        } else {
                                            Timber.d("$TAG User($newUserId) doesnt exist in the server")

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.CHECK_USER,
                                                            EventStatus.SUCCESS,
                                                            false,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("$TAG Something went wrong while checking user in server")
                                        responseObservable.onNext(
                                                Event(
                                                        EventType.CHECK_USER,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }
                                } else {
                                    Timber.d("$TAG Something went wrong while checking user in server")
                                    Timber.d("$TAG No response got from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.CHECK_USER,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                            } else {
                                Timber.d("$TAG Something went wrong while checking user in server")

                                var errorMessage = Utilities.getErrorMessage(response)
                                if (errorMessage != null) {
                                    Timber.e(errorMessage)
                                } else {
                                    Timber.e("$TAG No error message received")
                                }

                                responseObservable.onNext(
                                        Event(
                                                EventType.CHECK_USER,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }
                        }

                        override fun onError(error: Throwable) {
                            Timber.d("$TAG Something went wrong while checking user in server")
                            error.printStackTrace()
                            Timber.e(error)

                            responseObservable.onNext(Event(EventType.CHECK_USER, EventStatus.FAILURE, null, null))
                        }

                    }))
        }else{
            responseObservable.onNext(Event(EventType.CHECK_USER, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }
}