package com.svarochi.wifi.ui.inviteuser.view

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_EDIT_PROFILE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PROFILE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PROFILE_NAME
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SHARED_WITH
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SHARED_WITH_ID
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.database.entity.ProfileProject
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.model.api.common.SharedWithData
import com.svarochi.wifi.model.common.CustomNetwork
import com.svarochi.wifi.model.common.CustomProject
import com.svarochi.wifi.model.common.SharingProject
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.inviteuser.viewmodel.InviteUserViewModel
import com.svarochi.wifi.ui.userinvitations.view.SharedProfile
import kotlinx.android.synthetic.main.activity_invite_user_screen.*
import timber.log.Timber

class InviteUserScreen : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener {
    private var TAG = "InviteUserScreen --->"
    private var viewModel: InviteUserViewModel? = null
    private var childNetworkList = ArrayList<ArrayList<CustomNetwork>>()
    private var groupProjectList = ArrayList<CustomProject>()
    private var sharedWithEmailPhone: String? = null // For ui purposes only
    private var sharedWithUserId: String? = null
    private var isEditProfile = false
    private var profileName: String? = null
    private var profile: SharedProfile? = null
    private var dialog: Dialog? = null
    private var sharedWithData: SharedWithData? = null

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                iv_back.id -> {
                    finish()
                }

                iv_done.id -> {
                    saveProfile()
                }
                iv_editProfileName.id -> {
                    editProfileName()
                }
            }
        } else {
            Timber.d("$TAG clicked view is null")
        }
    }

    private fun saveProfile() {
        if (isEditProfile) {
            if (profile != null && viewModel != null) {

                var projectNetworkMap = HashMap<Long, ArrayList<Long>>()
                var projectsList = ArrayList<SharingProject>()
                for (projectNetworks in childNetworkList) {
                    for (customNetwork in projectNetworks) {
                        if (customNetwork.selected) {
                            var networkIdList = projectNetworkMap.get(customNetwork.network.projectId)
                            if (networkIdList == null) {
                                networkIdList = ArrayList()
                            }
                            networkIdList.add(customNetwork.network.id)
                            projectNetworkMap[customNetwork.network.projectId] = networkIdList
                        }
                    }
                }

                for (key in projectNetworkMap.keys) {
                    var newSharingProject = SharingProject(key, projectNetworkMap[key]!!)
                    projectsList.add(newSharingProject)
                }
                viewModel!!.updateProfile(profile!!.id, profileName!!, sharedWithData!!, profile!!.sharedBy, projectsList)
            } else {
                Timber.d("$TAG profile is null")
            }
        } else {
            if (viewModel != null) {

                var projectNetworkMap = HashMap<Long, ArrayList<Long>>()
                var projectsList = ArrayList<SharingProject>()
                for (projectNetworks in childNetworkList) {
                    for (customNetwork in projectNetworks) {
                        if (customNetwork.selected) {
                            var networkIdList = projectNetworkMap.get(customNetwork.network.projectId)
                            if (networkIdList == null) {
                                networkIdList = ArrayList()
                            }
                            networkIdList.add(customNetwork.network.id)
                            projectNetworkMap[customNetwork.network.projectId] = networkIdList
                        }
                    }
                }


                for (key in projectNetworkMap.keys) {
                    var newSharingProject = SharingProject(key, projectNetworkMap[key]!!)
                    projectsList.add(newSharingProject)
                }

                viewModel!!.createProfile(profileName!!, sharedWithData!!, projectsList)
            } else {
                Timber.d("$TAG ViewModel is null")
            }
        }
    }

    override fun serviceConnected() {
        init()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite_user_screen)
        requestToBindService(this)
    }

    private fun init() {
        initViewModel()
        initClickListeners()
        initBundleData()
    }

    private fun initBundleData() {
        if (intent.extras != null) {
            isEditProfile = intent.getBooleanExtra(BUNDLE_EDIT_PROFILE, false)

            if (isEditProfile.not()) {
                Timber.d("$TAG Creating new profile")
                sharedWithEmailPhone = intent.getStringExtra(BUNDLE_SHARED_WITH)
                sharedWithUserId = intent.getStringExtra(BUNDLE_SHARED_WITH_ID)
                profileName = intent.getStringExtra(BUNDLE_PROFILE_NAME)

                var email: String? = null
                var phone: String? = null

                if (Utilities.isEmail(sharedWithEmailPhone!!)) {
                    email = sharedWithEmailPhone!!
                } else {
                    phone = sharedWithEmailPhone!!
                }
                sharedWithData = SharedWithData(sharedWithUserId!!, email, phone)

            } else {
                profile = intent.getSerializableExtra(BUNDLE_PROFILE) as SharedProfile
                Timber.d("$TAG Editing profile(${profile!!.profileName})")


                sharedWithData = profile!!.sharedWithData
                sharedWithEmailPhone = ""
                if (sharedWithData?.email.isNullOrEmpty().not()) {
                    sharedWithEmailPhone = sharedWithData?.email
                }

                if (sharedWithEmailPhone.isNullOrEmpty() && sharedWithData?.phone.isNullOrEmpty().not()) {
                    sharedWithEmailPhone = sharedWithData?.phone
                }

                sharedWithUserId = sharedWithData?.user_id!!
                profileName = profile!!.profileName
            }
            updateUi()
        }
    }

    private fun updateUi() {
        getAllProjects()

        tv_profileName.text = getString(R.string.label_sharing_profile_) + profileName!!
        tv_sharedWithEmail.text = sharedWithEmailPhone!!
    }

    private fun fetchProjectNetworksOfProfile() {
        if (viewModel != null) {
            if (profile != null) {
                viewModel!!.getProjectNetworksOfProfile(profile!!.id).observe(this, Observer { t -> markNetworksOfProfile(t) })

            } else {
                // Should not occur
                Timber.d("$TAG Editing profile and profile is null")
            }
        } else {
            Timber.d("$TAG view model is null")
        }
    }

    private fun markNetworksOfProfile(profileProjectsList: List<ProfileProject>?) {
        if (profileProjectsList != null) {
            var networkIds = ArrayList<Long>()

            for (profileProject in profileProjectsList) {
                networkIds.add(profileProject.networkId)
            }

            for (project in childNetworkList) {
                for (customNetwork in project) {
                    if (networkIds.contains(customNetwork.network.id)) {
                        customNetwork.selected = true
                    }
                }
            }
        } else {
            Timber.d("$TAG no projects received for profile(${profile!!.profileName}")
        }
    }

    private fun initViews() {
        Timber.d("$TAG Projects size = ${groupProjectList.size}")
        Timber.d("$TAG Total Networks size = ${childNetworkList.size}")
        elv_projectsAndNetworks.setAdapter(SharingProfileAdapter(this, childNetworkList, groupProjectList))
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory).get(InviteUserViewModel::class.java)

        viewModel!!.getResponse().observe(this, Observer<Event> { event -> processResponse(event) })
    }

    private fun processResponse(event: Event?) {
        if (event != null) {
            when (event.type) {
                EventType.CREATE_PROFILE -> {
                    when (event.status) {
                        EventStatus.LOADING -> {
                            Timber.d("$TAG Creating profile")
                            showLoadingPopup(true, getString(R.string.dialog_creating_profile))
                        }
                        EventStatus.SUCCESS -> {
                            Timber.d("$TAG Successfully created profile")
                            showLoadingPopup(false, null)

                            finish()
                        }
                        EventStatus.FAILURE -> {
                            Timber.d("$TAG Failed to create profile")
                            showLoadingPopup(false, null)

                            if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                                showToast(getString(R.string.no_internet))
                            }
                        }
                    }
                }

                EventType.UPDATE_PROFILE -> {
                    when (event.status) {
                        EventStatus.LOADING -> {
                            Timber.d("$TAG Updating profile")
                            showLoadingPopup(true, getString(R.string.dialog_updating_profile))
                        }
                        EventStatus.SUCCESS -> {
                            Timber.d("$TAG Successfully updated profile")
                            showLoadingPopup(false, null)

                            finish()
                        }
                        EventStatus.FAILURE -> {
                            Timber.d("$TAG Failed to update profile")
                            showLoadingPopup(false, null)

                            if (event.data != null && event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                                showToast(getString(R.string.no_internet))
                            }
                        }
                    }
                }
            }

        } else {
            Timber.d("$TAG Event is null")
        }

    }

    private fun initClickListeners() {
        iv_back.setOnClickListener(this)
        iv_done.setOnClickListener(this)
        iv_editProfileName.setOnClickListener(this)

        elv_projectsAndNetworks.setOnGroupExpandListener {
            setGroupExpanded(it)
        }

        elv_projectsAndNetworks.setOnGroupCollapseListener {
            setGroupCollapsed(it)
        }
    }

    private fun setGroupCollapsed(groupPosition: Int) {
        (elv_projectsAndNetworks.expandableListAdapter as SharingProfileAdapter).setGroupCollapsed(groupPosition)
    }

    private fun setGroupExpanded(groupPosition: Int) {
        (elv_projectsAndNetworks.expandableListAdapter as SharingProfileAdapter).setGroupExpanded(groupPosition)

    }

    private fun getAllProjects() {
        if (viewModel != null) {
            viewModel!!.getAllProjects().observe(this, Observer { t ->
                updateListWithAllProjects(t)
            })
        } else {
            Timber.d("$TAG view model is null")
        }
    }

    private fun updateListWithAllProjects(allProjects: List<Project>?) {
        Timber.d("$TAG All projects size = ${allProjects!!.size}")

        Utilities.sortProjects(allProjects)
        childNetworkList.clear()
        groupProjectList.clear()

        if (allProjects != null) {
            for (project in allProjects) {
                var customProject = CustomProject(project.name)
                groupProjectList.add(customProject)

                var networksList = ArrayList<CustomNetwork>()
                var networks = getNetworksOfProject(project.id)
                for (network in networks) {
                    var customNetwork = CustomNetwork(network)
                    networksList.add(customNetwork)
                }
                Timber.d("Project(${project.name}), networks size = ${networksList.size}")
                childNetworkList.add(networksList)
            }
        }

        fetchProjectNetworksOfProfile()
        initViews()
    }


    private fun getNetworksOfProject(id: Long): List<Network> {
        if (viewModel != null) {
            return viewModel!!.getNetworksOfProject(id)
        } else {
            Timber.d("$TAG view model is null")
            return emptyList()
        }
    }

    private fun editProfileName() {
        if (dialog == null) {
            dialog = Dialog(this)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setCancelable(true)
            dialog!!.setCanceledOnTouchOutside(false)
            dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        }
        dialog!!.setContentView(R.layout.dialog_edit_sharing_profile_name)

        var profileNameField = dialog!!.findViewById<EditText>(R.id.et_profileName)
        var okButton = dialog!!.findViewById<Button>(R.id.bt_okButton)
        var cancelButton = dialog!!.findViewById<Button>(R.id.bt_cancelButton)
        profileNameField.text = Editable.Factory.getInstance().newEditable(profileName)
        profileNameField.requestFocus()

        okButton.setOnClickListener {
            if (profileNameField.text.trim().isNotEmpty()) {
                profileName = profileNameField.text.toString()
                tv_profileName.text = getString(R.string.label_sharing_profile_) + profileName
                dialog!!.dismiss()
            } else {
                showToast(getString(R.string.toast_please_enter_profile_name))
            }
        }

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }

        dialog!!.show()

    }

}
