package com.svarochi.wifi.ui.projects.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants.Companion.PROJECT_ITEM
import com.svarochi.wifi.common.Constants.Companion.MAX_PROJECTS
import com.svarochi.wifi.common.Constants.Companion.ADD_PROJECT_ITEM
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.ui.projects.view.activity.ProjectsActivity

class ProjectsAdapter(var projectsList: ArrayList<Project>, var isSelfProfile: Boolean, open var context: ProjectsActivity) :
        androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        var mainView: View
        if (viewType == ADD_PROJECT_ITEM) {
            mainView = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.item_add_new_project, viewGroup, false)


            return AddNewProjectViewHolder(mainView)
        }
        mainView = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_wifi_project, viewGroup, false)

        return ProjectsViewHolder(mainView)
    }

    override fun getItemCount(): Int {
        return projectsList.size

    }

    override fun onBindViewHolder(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (isSelfProfile) {
            if (projectsList.size < MAX_PROJECTS) {
                if (position != itemCount - 1) {
                    (viewHolder as ProjectsViewHolder).title.text = projectsList.get(position).name
                }
            } else {
                (viewHolder as ProjectsViewHolder).title.text = projectsList.get(position).name
            }
        } else {
            (viewHolder as ProjectsViewHolder).title.text = projectsList.get(position).name

            (viewHolder).edit.visibility = View.GONE

        }
    }


    override fun getItemViewType(position: Int): Int {
       if(projectsList[position].id == (-1).toLong()){
           return ADD_PROJECT_ITEM
       }
        return PROJECT_ITEM
    }

    fun updateProject(updatedProject: Project) {
        for (i in 0..projectsList.size) {
            if (projectsList[i].id == updatedProject.id) {
                projectsList.set(i, updatedProject)
                break
            }
        }
        notifyDataSetChanged()
    }

    fun removeProject(projectId: Long) {
        for (i in 0..projectsList.size) {
            if (projectsList[i].id == projectId) {
                projectsList.remove(projectsList[i])
                break
            }
        }
        notifyDataSetChanged()
    }

    fun isItSameProject(projectId: Long, newName: String?): Boolean {
        for (project in projectsList) {
            if(project.id == projectId && newName!!.equals(project.name)){
                return true
            }
        }
        return false
    }

    inner class ProjectsViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.tv_projectTitle)
        var layout: ConstraintLayout = itemView.findViewById(R.id.cl_projectLayout)
        var edit: ImageView = itemView.findViewById(R.id.iv_edit)

        init {
            layout.setOnClickListener {
                context.openNetworksActivity(
                        projectsList.get(adapterPosition).id,
                        projectsList.get(adapterPosition).name
                )
            }

            edit.setOnClickListener {
                context.editProjectName(projectsList.get(adapterPosition))
            }
        }
    }

    inner class AddNewProjectViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        init {
            var layout: ConstraintLayout = itemView.findViewById(R.id.cl_addNewProjectLayout)
            layout.setOnClickListener {
                context.addNewProject()
            }
        }
    }

}