package com.svarochi.wifi.ui.room

interface LightsImpl {

    fun getLights(networkId: Long)

    fun turnOnLight(macId: String?)

    fun turnOffLight(macId: String?)

    fun setWifiCredentials(
            lampName: String,
            lampSsid: String,
            ssidToConfigure: String,
            password: String,
            authenticationMode: String,
            encryptionType: String
    )

}