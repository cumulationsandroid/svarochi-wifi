package com.svarochi.wifi.ui.networks.view.activity

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_EDIT_NETWORK
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NETWORK
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NETWORK_NAMES
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PROJECT_ID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PROJECT_NAME
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.common.Constants.Companion.MAX_NETWORKS
import com.svarochi.wifi.common.Constants.Companion.REQUEST_LOCATION_SETTINGS
import com.svarochi.wifi.common.Constants.Companion.SET_WIFI_CREDENTIALS
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.addnewnetwork.view.NetworkDetailsActivity
import com.svarochi.wifi.ui.networks.view.adapter.NetworksAdapter
import com.svarochi.wifi.ui.networks.viewmodel.NetworksViewModel
import com.svarochi.wifi.ui.room.view.activity.RoomActivity
import com.svarochi.wifi.ui.setwifidetails.view.SetWifiDetailsActivity
import kotlinx.android.synthetic.main.activity_networks.*
import timber.log.Timber

class NetworksActivity : WifiBaseActivity(), View.OnClickListener, ServiceConnectionCallback {
    override fun serviceConnected() {
        initViewModel()
        initViews()
        initClickListeners()
        initManagers()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            ll_addNewRoom.id -> {
                addNewNetwork()
            }
            iv_back.id -> {
                finish()
            }
            iv_options.id -> {
                showOptionsMenu(v)
            }
        }
    }

    lateinit var viewModel: NetworksViewModel
    private var dialog: Dialog? = null
    private lateinit var locationManager: LocationManager
    private var networksCount = 0
    private var pendingNetwork: Network? = null
    private lateinit var projectName: String
    private var projectId: Long = -1
    private var networksList = ArrayList<Network>()
    private lateinit var preference: SharedPreference
    private var isSelfProfile = true
    private val NEW_EDIT_NETWORK = 123


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_networks)
        requestToBindService(this)
    }

    private fun initManagers() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private val optionMenuItemClickListener = object : PopupMenu.OnMenuItemClickListener {
        override fun onMenuItemClick(item: MenuItem?): Boolean {
            when (item!!.itemId) {
            }
            return false
        }
    }

    private fun showSetWifiDetailsActivity() {
        startActivityForResult(Intent(this, SetWifiDetailsActivity::class.java), SET_WIFI_CREDENTIALS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            SET_WIFI_CREDENTIALS -> {
                if (requestCode == RESULT_OK) {
                    openNetwork(pendingNetwork!!)
                }
            }

            REQUEST_LOCATION_SETTINGS -> {
                if (resultCode == RESULT_OK) {
                    checkWifiCredentials()
                }
            }

        }
    }

    private fun showOptionsMenu(view: View) {
        var popup = PopupMenu(this, view)
        popup.menuInflater.inflate(R.menu.options_networks, popup.menu)
        popup.setOnMenuItemClickListener(optionMenuItemClickListener)
        popup.show()
    }

    private fun initClickListeners() {
        ll_addNewRoom.setOnClickListener(this)
        iv_back.setOnClickListener(this)
        iv_options.setOnClickListener(this)
    }

    private fun initViews() {
        preference = SharedPreference(this)
        isSelfProfile = preference.getCurrentProfile().equals(CURRENT_PROFILE_SELF)

        projectId = intent.getLongExtra(BUNDLE_PROJECT_ID, 0)
        projectName = intent.getStringExtra(BUNDLE_PROJECT_NAME)
        //viewModel.getNetworks(intent.getLongExtra(BUNDLE_PROJECT_ID, 0))

        initNetworksRv()

        if (isSelfProfile) {
            tv_addUptoRooms.text = getString(R.string.label_you_can_add_upto) + MAX_NETWORKS + getString(R.string._space) +
                    getString(R.string.networks)
        } else {
            ll_bottomTextLayout.visibility = View.GONE

            ll_addNewRoom.visibility = View.GONE
        }
        tv_projectName.text = intent.getStringExtra(BUNDLE_PROJECT_NAME)

        dialog = Dialog(this@NetworksActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        saveProjectIdToPreference()
        observeNetworksTable()
    }

    private fun observeNetworksTable() {
        Timber.d("Observing the networks table")
        viewModel.getNetworksLiveData(projectId).observe(this, Observer {
            if (it != null) {
                Timber.d("Change in networks database")
                Utilities.sortNetworks(it)
                updateNetworksRv(it)
                updateNetworksCount(it.size)
            }
        })
    }

    private fun updateNetworksCount(size: Int) {
        networksCount = size
        tv_remainingRooms.text = getString(R.string.label_add) + (MAX_NETWORKS - networksCount) +
                getString(R.string.label_more_networks)

        if (networksCount >= MAX_NETWORKS) {
            ll_addNewRoom.visibility = View.GONE
        } else {
            ll_addNewRoom.visibility = View.VISIBLE
        }
    }

    private fun updateNetworksRv(updatedNetworksList: List<Network>) {
        networksList.clear()
        for (updatedNetwork in updatedNetworksList) {
            networksList.add(updatedNetwork)
        }
        rv_networkRv.adapter!!.notifyDataSetChanged()

        /*  if(updatedNetworksList.isNotEmpty()){
              isInit = false
          }
          if(updatedNetworksList.isNotEmpty() && !isInit) {
              rv_networkRv.scrollToPosition(networksList.size - 1)
          }*/
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(NetworksViewModel::class.java)
        viewModel.getResponse().observe(this, Observer { t -> processResponse(t) })
    }

    private fun processResponse(event: Event?) {
        when (event!!.type) {
            EventType.GET_NETWORKS -> {
                when (event.status) {
                    EventStatus.DEFAULT -> {
                    }
                    EventStatus.LOADING -> {
                        Timber.d("Fetching networks from DB ....")
                    }
                    EventStatus.SUCCESS -> {
                        /* Timber.d(
                             "Got ${(event.data!! as List<Network>).size} networks from DB for project id - ${intent.getLongExtra(
                                 "project_id",
                                 0
                             )}"
                         )
                         initNetworksRv((event.data!! as List<Network>))
                         updateNetworksCount((event.data!! as List<Network>).size)*/
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                    }
                }
            }
        }
    }

    private fun isNetworkAlreadyExisting(newName: String): Boolean {
        if (rv_networkRv.adapter != null) {
            if ((rv_networkRv.adapter as NetworksAdapter).networksList != null) {

                if ((rv_networkRv.adapter as NetworksAdapter).networksList.isEmpty()) {
                    return false
                } else {
                    for (network in (rv_networkRv.adapter as NetworksAdapter).networksList) {
                        if (network.name.equals(newName)) {
                            return true
                        }
                    }
                    return false
                }
            } else {
                // Should not occur
                Timber.d("Network list not initiated")
                return true
            }
        } else {
            // Should not occur
            Timber.d("Network list not initiated")
            return true
        }
    }


    private fun initNetworksRv() {
        rv_networkRv.layoutManager = LinearLayoutManager(this@NetworksActivity)
        rv_networkRv.adapter =
                NetworksAdapter(networksList, isSelfProfile, this@NetworksActivity)
    }

    fun addNewNetwork() {
        if (networksCount < MAX_NETWORKS) {
            openNetworkDetailsScreen(null)
        } else {
            showToast(getString(R.string.toast_maximum_rooms_are_added))
        }
    }

    fun editNetwork(network: Network) {
        Timber.d("Edit network - ${network.name}")
        openNetworkDetailsScreen(network)
    }

    private fun openNetworkDetailsScreen(network: Network?) {

        var networksNameList = ArrayList<String>()
        for (scannedNetwork in (rv_networkRv.adapter as NetworksAdapter).networksList) {
            networksNameList.add(scannedNetwork.name)
        }

        if (network != null) {
            var networksNameList = ArrayList<String>()
            for (scannedNetwork in (rv_networkRv.adapter as NetworksAdapter).networksList) {
                networksNameList.add(scannedNetwork.name)
            }
            startActivity(Intent(this, NetworkDetailsActivity::class.java)
                    .putExtra(BUNDLE_EDIT_NETWORK, true)
                    .putExtra(BUNDLE_NETWORK, network)
                    .putExtra(BUNDLE_NETWORK_NAMES, networksNameList))
        } else {
            startActivity(Intent(this, NetworkDetailsActivity::class.java)
                    .putExtra(BUNDLE_EDIT_NETWORK, false)
                    .putExtra(BUNDLE_NETWORK_NAMES, networksNameList))
        }
    }

    fun openNetwork(network: Network) {
        Timber.d("Open ${network.name}")
        pendingNetwork = network
        if ((ContextCompat.checkSelfPermission(
                        this@NetworksActivity,
                        Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(
                        this@NetworksActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(
                        this@NetworksActivity,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED) &&
                (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        ) {
            pendingNetwork = null
            saveNetworkIdToPreference(network.id)
            startActivity(
                    Intent(this@NetworksActivity, RoomActivity::class.java)
                            .putExtra(BUNDLE_NETWORK, network)
            )
        } else
            checkCameraPermission()
    }

    private fun saveNetworkIdToPreference(networkId: Long?) {
        if (networkId != null) {
            if (networkId != (-1).toLong()) {
                Timber.d("Saving networkId($networkId) to preferecnce")
                preference.setNetworkId(networkId)
            } else {
                Timber.d("Network Id is not set")
            }
        }
    }

    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this@NetworksActivity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED
        ) {
            Timber.d("Camera permission not granted")
            Timber.d("SDK Version - ${Build.VERSION.SDK_INT}")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Timber.d("Requesting camera permission")
                requestPermissions(
                        arrayOf(
                                android.Manifest.permission.CAMERA
                        ), Constants.ACCESS_CAMERA_PERMISSIONS_REQUEST
                )
            } else {
                // Should not occur
                Timber.d("Camera permission not granted")
            }
        } else {
            checkLocationPermisionAndGps()
        }
    }

    private fun checkLocationPermisionAndGps() {
        if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            Timber.d("Location permission not granted")
            Timber.d("SDK Version - ${Build.VERSION.SDK_INT}")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Timber.d("Requesting location permission")
                requestPermissions(
                        arrayOf(
                                android.Manifest.permission.ACCESS_FINE_LOCATION,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION
                        ), Constants.ACCESS_LOCATION_PERMISSIONS_REQUEST
                )
            } else {
                // Should not occur
                Timber.d("Location permission not granted")
            }
        } else {
            checkGps()
        }
    }


    private fun checkGps() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(this)
        } else {
            openNetwork(pendingNetwork!!)
        }
    }

    private fun checkWifiCredentials() {
        /*if (preference.getSsid().isNullOrEmpty()) {
            showSetWifiDetailsDialog()
        } else {
            openNetwork(pendingNetwork!!)
        }*/
        openNetwork(pendingNetwork!!)
    }

    private fun showSetWifiDetailsDialog() {
        dialog!!.setContentView(R.layout.dialog_set_wifi_credentials)
        dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        val okButton: Button = dialog!!.findViewById(R.id.bt_okButton)
        val cancelButton: Button = dialog!!.findViewById(R.id.bt_cancelButton)
        dialog!!.show()
        okButton.setOnClickListener {
            dialog!!.dismiss()
            showSetWifiDetailsActivity()
        }

        cancelButton.setOnClickListener {
            dialog!!.dismiss()
        }
    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.ACCESS_CAMERA_PERMISSIONS_REQUEST -> {
                // If networkRequest is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkLocationPermisionAndGps()
                } else {
                    showToast(getString(R.string.toast_kindly_provide_the_requested_permissions))
                    checkLocationPermisionAndGps()
                }
                return
            }
            Constants.ACCESS_LOCATION_PERMISSIONS_REQUEST -> {
                // If networkRequest is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkGps()
                } else {
                    showToast(getString(R.string.toast_kindly_provide_the_requested_permissions))
                    checkGps()
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might networkRequest.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun displayLocationSettingsRequest(context: Context?) {
        val googleApiClient = GoogleApiClient.Builder(context!!)
                .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = (10000 / 2).toLong()

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    Timber.d("All location settings are satisfied.")
                    checkWifiCredentials()
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Timber.d(
                            "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                    )
                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        startIntentSenderForResult(
                                status.resolution.intentSender,
                                Constants.REQUEST_LOCATION_SETTINGS,
                                null,
                                0,
                                0,
                                0,
                                null
                        )

                    } catch (e: Exception) {
                        Timber.d("PendingIntent unable to execute networkRequest.")
                    }

                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Timber.d(
                        "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                )
            }
        }
    }


    private fun showPopup(show: Boolean, message: String?) {
        showLoadingPopup(show, message)
    }


    private fun saveProjectIdToPreference() {
        if (projectId != (-1).toLong()) {
            Timber.d("Saving project_id($projectId) to preferecnce")
            preference.setProjectId(projectId)
        } else {
            Timber.d("Project Id is not set")
        }
    }
}
