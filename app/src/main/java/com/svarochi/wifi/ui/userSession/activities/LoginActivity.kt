package com.svarochi.wifi.ui.userSession.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import bizbrolly.svarochiapp.R
import com.akkipedia.skeleton.utils.GeneralUtils
import com.bizbrolly.WebServiceRequests
import com.bizbrolly.entities.ForgotPasswordResponse
import com.bizbrolly.entities.GetDbDetailsResponse
import com.bizbrolly.entities.GetUserDetailsResponse
import com.bizbrolly.entities.SendTokenResponse
import com.svarochi.wifi.common.*
import com.svarochi.wifi.common.FontManager.FONT_REGULAR
import com.svarochi.wifi.preference.Preferences
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.projects.view.activity.ProjectsActivity
import com.svarochi.wifi.ui.userSession.BaseActivity
import it.sephiroth.android.library.tooltip.Tooltip
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.inflate_phone_number.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity() {

    private var mForgotPasswordDialog: AlertDialog? = null
    private var isSlaveUser: Boolean = false

    private var mSecretCodeToolTip: Tooltip.TooltipView? = null

    private val mEmailPhoneTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            try {
                if (mSecretCodeToolTip != null && mSecretCodeToolTip!!.isShown) {
                    Tooltip.remove(this@LoginActivity, 101)
                    etEmailPhone.post { etEmailPhone.removeTextChangedListener(this) }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init() {
        setBundleData()
        setListeners()
        setDefaults()
    }

    private fun setBundleData() {
        if (intent != null && intent.extras != null) {
            val bundle = intent.extras
            if (bundle!!.containsKey(BUNDLE_IS_SLAVE_USER)) {
                isSlaveUser = bundle.getBoolean(BUNDLE_IS_SLAVE_USER)
            }
        }
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        etPassword.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateLogin()
            }
            false
        }
        if (!isSlaveUser) {
            etEmailPhone.addTextChangedListener(mEmailPhoneTextWatcher)
        }
    }

    private fun setDefaults() {
        //Rahul 2019/05/09 Hiding secret code feature for 3.3 onwards
        /* llSecretCode.setVisibility(isSlaveUser ? View.GONE : View.VISIBLE);
        if (!isSlaveUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showSecretCodeToolTip();
                }
            }, 1000);
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_CODE_VERIFY_TOKEN) {

        }
    }

    fun actionSecretCodeHint(view: View) {
        DialogUtils.showDefaultAlertMessage(this, "", getString(R.string.secret_code_hint), getString(R.string.ok), null)
    }

    fun actionForgotPasswordSecretCode(view: View) {
        showForgotPasswordSecretCodeDialog()
    }

    fun actionLogin(view: View) {
        validateLogin()
    }

    fun actionPasswordHint(view: View) {
        if (ivPasswordHint.tag != null && "close".equals(ivPasswordHint.getTag().toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(etPassword.text.toString().trim())) {
                etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                ivPasswordHint.setImageResource(R.drawable.icon_eye)
                ivPasswordHint.tag = "open"
                etPassword.setSelection(etPassword.text?.length!!)
            }
        } else {
            etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            ivPasswordHint.setImageResource(R.drawable.icon_closed_eye)
            ivPasswordHint.tag = "close"
            etPassword.setSelection(etPassword.text?.length!!)
        }
    }

    private fun validateLogin() {
        val emailPhone = etEmailPhone.text.toString().trim()
        val secretCode = etSecretCode.text.toString().trim()
        val password = etPassword.text.toString().trim()
        if (TextUtils.isEmpty(emailPhone)) {
            showToast(getString(R.string.please_enter_email_or_phone_number))
            return
        } else if (!emailPhone.matches("\\d+".toRegex()) && !CommonUtils.isValidEmail(emailPhone)) {
            showToast(getString(R.string.please_enter_valid_email))
            return
        } else if (emailPhone.matches("\\d+".toRegex()) && emailPhone.length < 10) {
            showToast(getString(R.string.please_enter_correct_phone_number))
            return
        } /*else if (!isSlaveUser && TextUtils.isEmpty(secretCode)) {
            showToast(getString(R.string.please_enter_secret_code));
            return;
        }*/
        else if (TextUtils.isEmpty(password)) {
            showToast(getString(R.string.please_enter_password))
            return
        } else if (password.length < 6) {
            showToast(getString(R.string.password_must_have_atleast_six_characters))
            return
        } else if (!password.matches(".*\\d+.*".toRegex())) {
            showToast(getString(R.string.password_must_have_atleast_one_number))
            return
        } else {
            CommonUtils.hideSoftKeyboard(this@LoginActivity)
            requestRestoreNetwork()
        }
    }

    private fun requestRestoreNetwork() {
        if (!GeneralUtils.isInternetAvailable(this)) {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show()
            return
        }
        CommonUtils.hideSoftKeyboard(this@LoginActivity)
        showProgressDialog(getString(R.string.restoring_settings_from_cloud), getString(R.string.do_not_close_restoring) + "\n" + getString(R.string.please_wait_))
        val emailPhone = etEmailPhone.text.toString().trim()
        val secretCode = etSecretCode.text.toString().trim()
        val password = etPassword.text.toString().trim()
        WebServiceRequests.getInstance().getDbDetails(emailPhone, secretCode, password,
                object : Callback<GetDbDetailsResponse> {
                    override fun onResponse(call: Call<GetDbDetailsResponse>, response: Response<GetDbDetailsResponse>) {
                        try {
                            if (response?.body() != null && response.body()!!.getDBDetailsResult != null) {
                                val userVersion = response.body()!!.getDBDetailsResult.userVersion
                                val versionName = response.body()!!.getDBDetailsResult.versionName

                                if (!TextUtils.isEmpty(versionName) && versionName.equals("V1", ignoreCase = true) && !isSlaveUser) {
                                    hideProgressDialog()
                                    //Old UI user - V1
                                    if (!emailPhone.matches("\\d+".toRegex()) && !CommonUtils.isValidEmail(emailPhone)) {
                                        showToast(getString(R.string.please_enter_valid_email))
                                        return
                                    }
                                    //Set secret code dialog
                                    showSecretCodeSetupDialog(emailPhone, password)
                                } else {
                                    //New UI user - V2
                                    if (response.body()!!.getDBDetailsResult.result) {
                                        //Success
                                        if (response.body()!!.getDBDetailsResult.data != null) {
                                            try {
                                                val dataEntity = response.body()!!.getDBDetailsResult.data

                                                //This logic is put to manage old user and new UI user for the secret code
                                                if (!isSlaveUser && dataEntity != null && !TextUtils.isEmpty(dataEntity.securityKey) && TextUtils.isEmpty(secretCode)) {
                                                    hideProgressDialog()
                                                    showToast(getString(R.string.please_enter_secret_code))
                                                    return
                                                }

                                                val dbString = dataEntity!!.dbScript

                                                Thread(Runnable {
                                                    try {
//                                                        AppDatabase.clearDatabase()
                                                        if (dbString != null && dbString.isNotEmpty()) {
//                                                            DbScriptHelper().parseScript(isSlaveUser, this@LoginActivity, dbString)
                                                        } else {
                                                            showToast(getString(R.string.no_database_found_for_this_user))
                                                        }

                                                        runOnUiThread {
                                                            hideProgressDialog()
                                                            Preferences.getInstance(this@LoginActivity).setUserMail(if (!TextUtils.isEmpty(dataEntity.email)) dataEntity.email else ""/*emailPhone.contains("@") ? emailPhone : ""*/)
                                                            Preferences.getInstance(this@LoginActivity).phone = if (!TextUtils.isEmpty(dataEntity.phoneNumber)) dataEntity.phoneNumber else ""
                                                            Preferences.getInstance(this@LoginActivity).secretCode = if (isSlaveUser) "" else secretCode
                                                            Preferences.getInstance(this@LoginActivity).networkPassword = password
                                                            Preferences.getInstance(this@LoginActivity).isPasswordSaved = true
                                                            Preferences.getInstance(this@LoginActivity).setGroupId(dataEntity.bulbId)
                                                            Preferences.getInstance(this@LoginActivity).isSlaveUser = isSlaveUser
                                                            Preferences.getInstance(this@LoginActivity).putInt(Preferences.PREF_USER_ID, dataEntity.id)

                                                            /**** Implementation for Wifi Module  */

                                                            val wifiPreference = SharedPreference(this@LoginActivity)
                                                            wifiPreference.setUserId(dataEntity.id.toString())

                                                            /** */
                                                            //Rahul; 2019/04/23 Doing so to not do any azure maintenance work after login/register as there is not need of data push if we are coming from login or register
                                                            Preferences.getInstance(this@LoginActivity).putBoolean(Preferences.PREF_ALREADY_AZURE_MAINTAINED, true)
                                                            DialogUtils.showDefaultAlertMessage(this@LoginActivity, "", getString(R.string.login_successful), getString(R.string.ok)) {
                                                                /**** Implementation for Wifi Module  */
                                                                // Below is the previous implementation which is commented, as an intermediate screen is added to either open BLE projects or Wifi Projects
                                                                //Intent intent = new Intent(LoginActivity.this, ProjectsActivity.class);

                                                                val intent = Intent(this@LoginActivity, ProjectsActivity::class.java)
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                                                startActivity(intent)
                                                                finish()
                                                            }
                                                        }
                                                    } catch (e: Exception) {
                                                        e.printStackTrace()
                                                        hideProgressDialog()
                                                    }
                                                }).start()
                                            } catch (e: Exception) {
                                                e.printStackTrace()
                                                hideProgressDialog()
                                                showToast(getString(R.string.something_went_wrong))
                                            }

                                        } else {
                                            hideProgressDialog()
                                            showToast(getString(R.string.no_database_found_for_this_user))
                                        }
                                    } else {
                                        hideProgressDialog()
                                        //Failure
                                        if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorDetails != null) {
                                            if ("Please Enter Correct Security Key".equals(response.body()!!.getDBDetailsResult.errorDetail.errorDetails, ignoreCase = true)) {
                                                DialogUtils.showDefaultAlertMessage(this@LoginActivity, "", "Please enter correct Secret Code, in-case you do not have keep it empty.", getString(R.string.ok), null)
                                            } else if ("Please first push the data to cloud to Sync your network.".equals(response.body()!!.getDBDetailsResult.errorDetail.errorDetails, ignoreCase = true)) {
                                                if (response.body()!!.getDBDetailsResult.data != null) {
                                                    try {
                                                        val dataEntity = response.body()!!.getDBDetailsResult.data

                                                        //This logic is put to manage old user and new UI user for the secret code
                                                        if (!isSlaveUser && dataEntity != null && !TextUtils.isEmpty(dataEntity.securityKey) && TextUtils.isEmpty(secretCode)) {
                                                            hideProgressDialog()
                                                            showToast(getString(R.string.please_enter_secret_code))
                                                            return
                                                        }

                                                        val dbString = dataEntity!!.dbScript
                                                        Thread(Runnable {
                                                            try {
//                                                                AppDatabase.clearDatabase()
                                                                if (dbString != null && dbString.isNotEmpty()) {
//                                                                    DbScriptHelper().parseScript(isSlaveUser, this@LoginActivity, dbString)
                                                                } else {
                                                                    showToast(getString(R.string.no_database_found_for_this_user))
                                                                }

                                                                runOnUiThread {
                                                                    hideProgressDialog()
                                                                    Preferences.getInstance(this@LoginActivity).setUserMail(if (!TextUtils.isEmpty(dataEntity.email)) dataEntity.email else ""/*emailPhone.contains("@") ? emailPhone : ""*/)
                                                                    Preferences.getInstance(this@LoginActivity).phone = if (!TextUtils.isEmpty(dataEntity.phoneNumber)) dataEntity.phoneNumber else ""
                                                                    Preferences.getInstance(this@LoginActivity).secretCode = if (isSlaveUser) "" else secretCode
                                                                    Preferences.getInstance(this@LoginActivity).networkPassword = password
                                                                    Preferences.getInstance(this@LoginActivity).isPasswordSaved = true
                                                                    Preferences.getInstance(this@LoginActivity).setGroupId(dataEntity.bulbId)
                                                                    Preferences.getInstance(this@LoginActivity).isSlaveUser = isSlaveUser
                                                                    Preferences.getInstance(this@LoginActivity).putInt(Preferences.PREF_USER_ID, dataEntity.id)
                                                                    //Preferences.getInstance(LoginActivity.this).putBoolean(Preferences.PREF_ALREADY_AZURE_MAINTAINED, true);
                                                                    //Not managing azure server maintainece variable as user is still with old history in ModifiedDate column, so let user push all whatever he/she has wrong/correct
                                                                    DialogUtils.showDefaultAlertMessage(this@LoginActivity, "", getString(R.string.login_successful), getString(R.string.ok)) {
                                                                        /**** Implementation for Wifi Module  */

                                                                        // Below is the previous implementation which is commented, as an intermediate screen is added to either open BLE projects or Wifi Projects
                                                                        //Intent intent = new Intent(LoginActivity.this, ProjectsActivity.class);

                                                                        val intent = Intent(this@LoginActivity, ProjectsActivity::class.java)
                                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                                                        startActivity(intent)
                                                                        finish()
                                                                    }
                                                                }
                                                            } catch (e: Exception) {
                                                                e.printStackTrace()
                                                                hideProgressDialog()
                                                            }
                                                        }).start()
                                                    } catch (e: Exception) {
                                                        e.printStackTrace()
                                                        hideProgressDialog()
                                                        showToast(getString(R.string.something_went_wrong))
                                                    }

                                                } else {
                                                    hideProgressDialog()
                                                    showToast(getString(R.string.no_database_found_for_this_user))
                                                }
                                            } else {
                                                showToast(response.body()!!.getDBDetailsResult.errorDetail.errorDetails)
                                            }
                                        }
                                    }
                                }
                            } else {
                                hideProgressDialog()
                                showToast(getString(R.string.something_went_wrong))
                            }
                        } catch (e: Exception) {
                            hideProgressDialog()
                            e.printStackTrace()
                        }

                    }

                    override fun onFailure(call: Call<GetDbDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                }
        )
    }

    private fun showForgotPasswordSecretCodeDialog() {
        try {
            val layout = LinearLayout(this)
            layout.orientation = LinearLayout.VERTICAL
            val etEmailMobile = EditText(this)
            etEmailMobile.hint = getString(R.string.enter_email_id_or_mobile_no)
            etEmailMobile.maxLines = 1
            etEmailMobile.setLines(1)
            val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            layoutParams.setMargins(resources.getDimensionPixelSize(R.dimen.dp_20), 0, resources.getDimensionPixelSize(R.dimen.dp_20), 0)
            etEmailMobile.layoutParams = layoutParams
            layout.addView(etEmailMobile)
            mForgotPasswordDialog = AlertDialog.Builder(this)
                    .setView(layout)
                    .setTitle(getString(R.string.forgot_password))
                    .setPositiveButton(getString(R.string.reset_password), null)
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
            mForgotPasswordDialog!!.setOnShowListener { dialog ->
                val positiveButton = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
                positiveButton.setOnClickListener {
                    val emailPhone = etEmailMobile.text.toString().trim { it <= ' ' }
                    if (TextUtils.isEmpty(emailPhone)) {
                        showToast(getString(R.string.please_enter_email_or_phone_number))
                    } else if (!emailPhone.matches("\\d+".toRegex()) && !CommonUtils.isValidEmail(emailPhone)) {
                        showToast(getString(R.string.please_enter_valid_email))
                    } else if (emailPhone.matches("\\d+".toRegex()) && emailPhone.length < 10) {
                        showToast(getString(R.string.please_enter_correct_phone_number))
                    } /*else if (!emailPhone.equals(Preferences.getInstance(LoginActivity.this).getEmail())) {
                                showToast(getString(R.string.entered_email_is_not_connected_with_logged_in_profile));
                            }*/
                    else {
                        CommonUtils.hideSoftKeyboard(this@LoginActivity)
                        requestUserDetails(if (!emailPhone.matches("\\d+".toRegex())) emailPhone else "", if (emailPhone.matches("\\d+".toRegex())) emailPhone else "")
                    }
                }
            }
            mForgotPasswordDialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showSecretCodeSetupDialog(email: String, password: String) {
        DialogUtils.showDefaultAlert(this@LoginActivity, getString(R.string.set_secret_code), getString(R.string.it_seems_you_are_an_old_app_user_), getString(R.string.using_email_), getString(R.string.using_mobile_number_),
                View.OnClickListener {
                    //Using email
                    requestSendToken(email, "", password)
                }, View.OnClickListener {
            //Using mobile number + adding mobile number to the account
            showPhoneNumberDialog(email, password)
        }, true, 16f)
    }

    private fun showPhoneNumberDialog(email: String, password: String) {
        try {
            val view = LayoutInflater.from(this).inflate(R.layout.inflate_phone_number, null, false)
            etPhone.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    tvPhoneCode.setTextColor(ContextCompat.getColor(this@LoginActivity, if (s.isNotEmpty()) R.color.black else R.color.grey))
                }

                override fun afterTextChanged(s: Editable) {

                }
            })
            val dialog = AlertDialog.Builder(this)
                    .setView(view)
                    .setTitle("Set Mobile Number")
                    .setPositiveButton(getString(R.string.ok)) { dialog, which ->
                        val phone = etPhone.getText()!!.toString().trim { it <= ' ' }
                        if (TextUtils.isEmpty(phone)) {
                            showToast(getString(R.string.please_enter_phone_number))
                        } else if (phone.matches("\\d+".toRegex()) && phone.length < 10) {
                            showToast(getString(R.string.please_enter_correct_phone_number))
                        } else if (phone.startsWith("+91")) {
                            showToast(getString(R.string.please_enter_phone_number_without_country_code))
                        } else {
                            dialog.dismiss()
                            CommonUtils.hideSoftKeyboard(this@LoginActivity)
                            requestSendToken(email, phone, password)
                        }
                    }
                    .setNegativeButton(getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                        showSecretCodeSetupDialog(email, password)
                    }
                    .setCancelable(false)
                    .create()
            dialog.setOnShowListener {
                CommonUtils.showKeyBoard(this@LoginActivity)
                etPhone.requestFocus()
            }
            dialog.setOnDismissListener { CommonUtils.hideSoftKeyboard(this@LoginActivity) }
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun requestForgotPassword(emailPhone: String) {
        if (!GeneralUtils.isInternetAvailable(this@LoginActivity)) {
            showToast(getString(R.string.no_internet))
            return
        }
        showProgressDialog()
        WebServiceRequests.getInstance().forgotPassword(emailPhone, object : Callback<ForgotPasswordResponse> {
            override fun onResponse(call: Call<ForgotPasswordResponse>, response: Response<ForgotPasswordResponse>) {
                hideProgressDialog()
                if (mForgotPasswordDialog != null && mForgotPasswordDialog!!.isShowing) {
                    mForgotPasswordDialog!!.dismiss()
                }
                if (response?.body() != null && response.body()!!.getDBDetailsResult != null) {
                    if (response.isSuccessful && response.body()!!.getDBDetailsResult.isResult) {
                        DialogUtils.showDefaultAlertMessage(this@LoginActivity, "", getString(R.string.password_secret_code_sent_to_the_registered_email_mobile), getString(R.string.ok), null)
                    } else {
                        if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorDetails != null) {
                            showToast(response.body()!!.getDBDetailsResult.errorDetail.errorDetails)
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                } else {
                    showToast(getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                hideProgressDialog()
                if (mForgotPasswordDialog != null && mForgotPasswordDialog!!.isShowing) {
                    mForgotPasswordDialog!!.dismiss()
                }
                if (t.message?.contains("Unable to resolve host")!!) {
                    showToast(getString(R.string.no_internet))
                } else {
                    showToast(getString(R.string.something_went_wrong))
                }
            }
        })
    }

    private fun requestSendToken(email: String, phoneNumber: String, password: String) {
        if (!GeneralUtils.isInternetAvailable(this@LoginActivity)) {
            showToast(getString(R.string.no_internet))
            return
        }
        showProgressDialog()
        WebServiceRequests.getInstance().sendToken(email, phoneNumber, password, object : Callback<SendTokenResponse> {
            override fun onResponse(call: Call<SendTokenResponse>, response: Response<SendTokenResponse>) {
                CommonUtils.hideSoftKeyboard(this@LoginActivity)
                hideProgressDialog()
                if (response?.body() != null && response.body()!!.getDBDetailsResult != null) {
                    if (response.isSuccessful && response.body()!!.getDBDetailsResult.isResult) {
                        var message = ""
                        if (!TextUtils.isEmpty(email) && TextUtils.isEmpty(phoneNumber)) {
                            message = getString(R.string.otp_has_been_sent_to_this_email, email)
                        } else if (TextUtils.isEmpty(email) && !TextUtils.isEmpty(phoneNumber)) {
                            message = getString(R.string.otp_has_been_sent_to_this_phone, phoneNumber)
                        } else if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(phoneNumber)) {
                            message = getString(R.string.otp_has_been_sent_to_this_email_phone, email, phoneNumber)
                        }

                        DialogUtils.showDefaultAlertMessage(this@LoginActivity, "", message, getString(R.string.ok)) {
                            val intent = Intent(this@LoginActivity, VerifyTokenActivity::class.java)
                            val bundle = Bundle()
                            bundle.putString("Email", email)
                            bundle.putString("Phone", phoneNumber)
                            bundle.putString("Password", password)
                            intent.putExtras(bundle)
                            startActivity(intent)
                        }
                    } else {
                        if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorMessage != null) {
                            //Showing phone number dialog again if phone number related error shown
                            if (response.body()!!.getDBDetailsResult.errorDetail.errorMessage.contains("Please enter different phone number")) {
                                showToast(response.body()!!.getDBDetailsResult.errorDetail.errorMessage + " " +
                                        response.body()!!.getDBDetailsResult.errorDetail.errorDetails)
                                showPhoneNumberDialog(email, password)
                                return
                            }
                            showToast(response.body()!!.getDBDetailsResult.errorDetail.errorMessage)
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                } else {
                    showToast(getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<SendTokenResponse>, t: Throwable) {
                hideProgressDialog()
                if (mForgotPasswordDialog != null && mForgotPasswordDialog!!.isShowing) {
                    mForgotPasswordDialog!!.dismiss()
                }
                if (t.message?.contains("Unable to resolve host")!!) {
                    showToast(getString(R.string.no_internet))
                } else {
                    showToast(getString(R.string.something_went_wrong))
                }
            }
        })
    }

    private fun requestUserDetails(email: String, phone: String) {
        if (!GeneralUtils.isInternetAvailable(this)) {
            showToast(getString(R.string.no_internet))
            return
        }
        showProgressDialog("Validating User", getString(R.string.please_wait_))
        WebServiceRequests.getInstance().getUserDetails(email, phone, object : Callback<GetUserDetailsResponse> {
            override fun onResponse(call: Call<GetUserDetailsResponse>, response: Response<GetUserDetailsResponse>) {
                hideProgressDialog()
                if (response?.body() != null && response.body()!!.userDetailsResponseBean != null) {
                    val userDetailsResponse = response.body()!!.userDetailsResponseBean
                    if (userDetailsResponse != null) {
                        val statusCode = userDetailsResponse.responseStatusCode
                        val status = userDetailsResponse.responseStatus
                        if (statusCode == 200 && status.equals("success", ignoreCase = true)) {
                            Log.e(TAG, "requestUserDetails onResponse")
                            val intent = Intent(this@LoginActivity, ResetPasswordActivity::class.java)
                            val bundle = Bundle()
                            bundle.putString("EmailOrPhoneNumber", if (!TextUtils.isEmpty(email)) email else phone)
                            intent.putExtras(bundle)
                            startActivity(intent)
                        } else {
                            showToast("User doesn't exists")
                        }
                    }
                }
            }

            override fun onFailure(call: Call<GetUserDetailsResponse>, t: Throwable) {
                Log.e(TAG, "requestMyUserDetails onFailure")
                hideProgressDialog()
                if (t.message?.contains("Unable to resolve host")!!) {
                    showToast(getString(R.string.no_internet))
                } else {
                    showToast(getString(R.string.something_went_wrong))
                }
            }
        })
    }

    companion object {
        private val TAG = LoginActivity::class.java.simpleName
        const val REQ_CODE_VERIFY_TOKEN = 201
        const val BUNDLE_IS_SLAVE_USER = "is_slave_user"
    }
}
