package com.svarochi.wifi.ui.userinvitations.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.database.entity.Profile

class UserInvitationsAdapter(var context: UserInvitationsActivity) : RecyclerView.Adapter<UserInvitationsAdapter.UserInvitationViewHolder>() {
    var invitationsList = ArrayList<SharedProfile>()
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): UserInvitationViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_user_invitation, viewGroup, false)

        return UserInvitationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return invitationsList.size
    }

    override fun onBindViewHolder(viewHolder: UserInvitationViewHolder, position: Int) {
        viewHolder.bind(invitationsList.get(position))
    }

    inner class UserInvitationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var invitationText = itemView.findViewById<TextView>(R.id.tv_invitationText)
        var editInvitation = itemView.findViewById<ImageView>(R.id.iv_editInvitation)

         fun bind(profile: SharedProfile) {
             var sharedWithEmailPhone = ""

             if(profile.sharedWithData.email.isNullOrEmpty().not()){
                 sharedWithEmailPhone = profile.sharedWithData.email!!
             }

             if(sharedWithEmailPhone.isEmpty() && profile.sharedWithData.phone.isNullOrEmpty().not()){
                 sharedWithEmailPhone = profile.sharedWithData.phone!!
             }

             invitationText.text = profile.profileName + context.getString(R.string.label_is_shared_as_guest_to_) + sharedWithEmailPhone
         }

         init {
             editInvitation.setOnClickListener {
                 var position = adapterPosition
                 context.editInvitation(invitationsList.get(position))
             }
         }
    }

     fun addProfiles(profiles: List<SharedProfile>) {
         invitationsList.clear()
         invitationsList.addAll(profiles)
         notifyDataSetChanged()
     }
}