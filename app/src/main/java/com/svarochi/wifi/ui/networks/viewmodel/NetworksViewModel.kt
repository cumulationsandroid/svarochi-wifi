package com.svarochi.wifi.ui.networks.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.networks.repository.NetworksRepository
import io.reactivex.disposables.CompositeDisposable

class NetworksViewModel(var networksRepository: NetworksRepository) : BaseViewModel() {

    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        // Observe network list of roomRepository
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, networksRepository, response)
    }


    override fun onCleared() {
        disposables.dispose()
        networksRepository.dispose()
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }


    fun getNetworksLiveData(project_id: Long): LiveData<List<Network>> {
        return networksRepository.getNetworksLiveData(project_id)
    }

}