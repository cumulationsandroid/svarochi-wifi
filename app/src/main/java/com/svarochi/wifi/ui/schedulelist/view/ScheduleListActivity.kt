package com.svarochi.wifi.ui.schedulelist.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_EDIT_SCHEDULE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_OPEN_SCHEDULE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SCHEDULE_ID
import com.svarochi.wifi.common.Constants.Companion.MAX_SCHEDULES
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.database.entity.Schedule
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.ui.scheduledetails.view.ScheduleActivity
import com.svarochi.wifi.ui.schedulelist.viewmodel.ScheduleListViewModel
import kotlinx.android.synthetic.main.activity_schedule_list.*
import timber.log.Timber

class ScheduleListActivity : WifiBaseActivity(), View.OnClickListener, ServiceConnectionCallback {

    private var schedulesList = ArrayList<Schedule>()
    private var viewModel: ScheduleListViewModel? = null

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                bt_scheduleNewEvent.id -> {
                    openNewScheduleActivity()
                }

                iv_back.id -> {
                    finish()
                }
            }
        }
    }

    private fun openNewScheduleActivity() {
        if (schedulesList.size < MAX_SCHEDULES) {
            startActivityForResult(
                    Intent(this, ScheduleActivity::class.java)
                            .putExtra(BUNDLE_EDIT_SCHEDULE, false)
                            .putExtra(BUNDLE_SCHEDULE_ID, 0),
                    BUNDLE_OPEN_SCHEDULE
            )
        } else {
            Timber.d("Max schedules($MAX_SCHEDULES) are added")
            showToast(getString(R.string.toast_max_schedules_are_added))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_list)
        requestToBindService(this)
    }

    override fun serviceConnected() {
        init()
    }

    private fun init() {
        initViewModel()
        initClickListeners()
        initSchedulesRv()
        observeSchedulesTable()
    }

    private fun initViewModel() {
        if (viewModel == null) {
            viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                    .get(ScheduleListViewModel::class.java)

            viewModel!!.response.observe(this, Observer {
                processResponse(it!!)
            })

        }
    }

    private fun processResponse(event: Event) {
        when (event.type) {
            EventType.GET_SCHEDULES -> {
                if (event.status == EventStatus.FAILURE) {
                    if (event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                        showToast(getString(R.string.toast_no_internet_available))
                        return
                    }
                    showToast(getString(R.string.failed_to_fetch_schedules))
                }
            }

            EventType.DELETE_SCHEDULE -> {
                if (event.status == EventStatus.LOADING) {
                    showLoadingPopup(true, getString(R.string.deleting_schedule))
                    return
                }
                if (event.status == EventStatus.SUCCESS) {
                    showLoadingPopup(false, null)
                    return
                }

                if (event.status == EventStatus.FAILURE) {
                    if (event.data is Int && event.data == NO_INTERNET_AVAILABLE) {
                        showToast(getString(R.string.toast_no_internet_available))
                        return
                    }
                    showToast(getString(R.string.failed_to_fetch_schedules))
                }
            }
        }
    }

    private fun observeSchedulesTable() {
        if (viewModel == null) {
            initViewModel()
        }
        viewModel!!.getSchedules().observe(this, Observer { dbSchedules ->
            if (dbSchedules != null) {
                if (dbSchedules.isNotEmpty()) {
                    rv_scheduledEventsRv.visibility = View.VISIBLE
                    ll_noSchedules.visibility = View.GONE
                    updateSchedulesRv(dbSchedules)
                }else{
                    rv_scheduledEventsRv.visibility = View.GONE
                    ll_noSchedules.visibility = View.VISIBLE
                }
            }
        })
    }


    private fun updateSchedulesRv(schedules: List<Schedule>) {
        schedulesList.clear()
        for (schedule in schedules) {
            schedulesList.add(schedule)
        }
        if (rv_scheduledEventsRv.adapter != null) {
            rv_scheduledEventsRv.adapter!!.notifyDataSetChanged()
        } else {
            Timber.d("Schedules RV not initialised")
            initSchedulesRv()
        }
    }

    private fun initSchedulesRv() {
        rv_scheduledEventsRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        rv_scheduledEventsRv.adapter = ScheduleListAdapter(schedulesList, this)

    }

    private fun initClickListeners() {
        bt_scheduleNewEvent.setOnClickListener(this)
        iv_back.setOnClickListener(this)
    }

    fun openSchedule(schedule: Schedule) {
        startActivityForResult(
                Intent(this, ScheduleActivity::class.java)
                        .putExtra(BUNDLE_SCHEDULE_ID, schedule.id),
                BUNDLE_OPEN_SCHEDULE
        )
    }

    private fun getSchedules() {
        if (viewModel != null) {
            Timber.d("$javaClass Fetching schedules from API")
            viewModel!!.getSchedules()
        } else {
            // Should not occur
            Timber.d("ViewModel is null")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            BUNDLE_OPEN_SCHEDULE -> {
                if (resultCode == Activity.RESULT_OK) {
                    getSchedules()
                }
            }
        }
    }

    fun deleteSchedule(scheduleId: Long) {
        viewModel!!.deleteSchedule(scheduleId)
    }
}
