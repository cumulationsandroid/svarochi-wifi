package com.svarochi.wifi.ui.userSession.activities

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager

import com.akkipedia.skeleton.utils.GeneralUtils
import com.bizbrolly.WebServiceRequests
import com.bizbrolly.entities.CommonResponse

import bizbrolly.svarochiapp.R
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.preference.Preferences
import kotlinx.android.synthetic.main.activity_change_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        init()
    }

    private fun init() {
        setListeners()
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        et_confirm_password.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateChangePassword()
            }
            false
        }
    }

    fun actionOldPasswordHint(view: View) {
        if (iv_old_password_hint.tag != null && "close".equals(iv_new_password_hint.tag.toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(et_old_password.text.toString().trim())) {
                et_old_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                iv_old_password_hint.setImageResource(R.drawable.icon_eye)
                iv_old_password_hint.tag = "open"
                et_old_password.setSelection(et_old_password.text?.length!!)
            }
        } else {
            et_old_password.transformationMethod = PasswordTransformationMethod.getInstance()
            iv_old_password_hint.setImageResource(R.drawable.icon_closed_eye)
            iv_old_password_hint.tag = "close"
            et_old_password.setSelection(et_old_password.text?.length!!)
        }
    }

    fun actionNewPasswordHint(view: View) {
        if (iv_new_password_hint.tag != null && "close".equals(iv_new_password_hint.tag.toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(et_new_password.text.toString().trim())) {
                et_new_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                iv_new_password_hint.setImageResource(R.drawable.icon_eye)
                iv_new_password_hint.tag = "open"
                et_new_password.setSelection(et_new_password.text?.length!!)
            }
        } else {
            et_new_password.transformationMethod = PasswordTransformationMethod.getInstance()
            iv_new_password_hint.setImageResource(R.drawable.icon_closed_eye)
            iv_new_password_hint.tag = "close"
            et_new_password.setSelection(et_new_password.text?.length!!)
        }
    }

    fun actionConfirmPasswordHint(view: View) {
        if (iv_confirm_password_hint.tag != null && "close".equals(iv_confirm_password_hint.tag.toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(et_confirm_password.text.toString().trim())) {
                et_confirm_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                iv_confirm_password_hint.setImageResource(R.drawable.icon_eye)
                iv_confirm_password_hint.tag = "open"
                et_confirm_password.setSelection(et_confirm_password.text?.length!!)
            }
        } else {
            et_confirm_password.transformationMethod = PasswordTransformationMethod.getInstance()
            iv_confirm_password_hint.setImageResource(R.drawable.icon_closed_eye)
            iv_confirm_password_hint.tag = "close"
            et_confirm_password.setSelection(et_confirm_password.text?.length!!)
        }
    }

    fun actionChangePassword(view: View) {
        validateChangePassword()
    }

    private fun validateChangePassword() {
        val oldPassword = et_old_password.text.toString().trim()
        val newPassword = et_new_password.text.toString().trim()
        val confirmPassword = et_confirm_password.text.toString().trim()

        if (TextUtils.isEmpty(oldPassword)) {
            showToast(getString(R.string.please_enter_old_password))
            return
        } else if (TextUtils.isEmpty(newPassword)) {
            showToast(getString(R.string.please_enter_new_password))
            return
        } else if (newPassword.length < 6) {
            showToast(getString(R.string.password_must_have_atleast_six_characters))
            return
        } else if (!newPassword.matches(".*\\d+.*".toRegex())) {
            showToast(getString(R.string.password_must_have_atleast_one_number))
            return
        } else if (TextUtils.isEmpty(confirmPassword)) {
            showToast(getString(R.string.please_enter_confirm_password))
            return
        } else if (newPassword != confirmPassword) {
            showToast(getString(R.string.new_password_and_confirm_password_should_not_mismatch))
            return
        } else {
            hideSoftKeyboard(this)
            requestChangePassword()
        }
    }

    private fun requestChangePassword() {
        if (!GeneralUtils.isInternetAvailable(this)) {
            showToast(getString(R.string.no_internet))
            return
        }
        showProgressDialog("Changing password", getString(R.string.please_wait_))
        val oldPassword = et_old_password.text.toString().trim()
        val newPassword = et_new_password.text.toString().trim()

        WebServiceRequests.getInstance().changePassword(Preferences.getInstance(this).getInt(Preferences.PREF_USER_ID), oldPassword, newPassword,
                object : Callback<CommonResponse> {
                    override fun onResponse(call: Call<CommonResponse>, response: Response<CommonResponse>) {
                        hideProgressDialog()
                        if (response.body() != null && response.body()!!.dbDetailsResult != null) {
                            if (response.body()!!.dbDetailsResult.isResult) {
                                Preferences.getInstance(this@ChangePasswordActivity).networkPassword = newPassword
                                showToast("Password changed successfully")
                                finish()
                            } else {
                                if (response.body()!!.dbDetailsResult.errorDetail != null && response.body()!!.dbDetailsResult.errorDetail.errorDetails != null) {
                                    if (response.body()!!.dbDetailsResult.errorDetail.errorDetails.equals("Invalid UserId or Password", ignoreCase = true)) {
                                        showToast("Please enter correct old password.")
                                        //showToast("Something went wrong! Please logout and login again.");
                                        //SvarochiApplication.requestMyUserDetails(ChangePasswordActivity.this, true);
                                    } else {
                                        showToast(response.body()!!.dbDetailsResult.errorDetail.errorDetails)
                                    }
                                } else {
                                    showToast(getString(R.string.something_went_wrong))
                                }
                            }
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }

                    override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                        hideProgressDialog()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else
                            showToast(getString(R.string.something_went_wrong))
                    }
                }
        )
    }

    companion object {

        /**
         * close opened soft keyboard.
         *
         * @param mActivity context
         */
        fun hideSoftKeyboard(mActivity: Activity) {
            try {
                val view = mActivity.currentFocus
                if (view != null) {
                    val inputManager = mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputManager.hideSoftInputFromWindow(view.windowToken, 0)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
}
