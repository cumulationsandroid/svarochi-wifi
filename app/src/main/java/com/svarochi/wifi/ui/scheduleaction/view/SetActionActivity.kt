package com.svarochi.wifi.ui.scheduleaction.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.SeekBar
import androidx.lifecycle.ViewModelProviders
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.ScreenUtils
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.base.WifiBaseActivity
import com.svarochi.wifi.colorpicker.WifiColorPickerDialog
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_ACTION_DETAILS
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.common.Utilities.Companion.convertHexStringToInt
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.communication.Scene
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.ui.lightcontrol.view.fragment.ImagePickerDialogFragment
import com.svarochi.wifi.ui.scheduleaction.viewholder.ScheduleActionViewModel
import com.svarochi.wifi.ui.scheduledetails.view.ActionDetails
import kotlinx.android.synthetic.main.activity_set_action.*
import timber.log.Timber
import kotlin.math.absoluteValue
import kotlin.math.roundToInt

class SetActionActivity : WifiBaseActivity(), ServiceConnectionCallback, View.OnClickListener, ImagePickerDialogFragment.ColorPickerInterface {
    private var viewModel: ScheduleActionViewModel? = null
    private var mColorPickerDialog: WifiColorPickerDialog? = null
    private var imagePickerFragment: ImagePickerDialogFragment? = null
    private var action: ActionDetails? = null
    val MAX = 100
    val MIN = 10
    val STEP = 1

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                bt_saveButton.id -> {
                    saveButton()
                }
                iv_back.id -> {
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                }
                R.id.bt_preview -> {
                    previewAction()
                }
                R.id.ll_candleLight -> {
                    setScene(SceneType.CANDLE_LIGHT)
                }
                R.id.ll_energise -> {
                    setScene(SceneType.ENERGISE)
                }
                R.id.ll_sunset -> {
                    setScene(SceneType.SUNSET)
                }
                ll_colorBlast.id -> {
                    setScene(SceneType.COLOR_BLAST)
                }
                ll_rainbow.id -> {
                    setScene(SceneType.RAINBOW)
                }
                ll_partyMode.id -> {
                    setScene(SceneType.PARTY_MODE)
                }
                tv_colorPalette.id -> {
                    showColorPalette()
                }
                tv_photoGallery.id -> {
                    openPhotoGallery()
                }
                tv_camera.id -> {
                    openCamera()
                }
            }
        }
    }

    private fun saveButton() {
        setResult(Activity.RESULT_OK, Intent().putExtra(BUNDLE_ACTION_DETAILS, action!!))
        finish()
    }

    override fun onColorSelected(color: String) {
        setColor(color)
    }

    override fun onForceColorSelected(color: String) {
        setColor(color)
    }


    private fun setColor(color: String) {
        action!!.rValue = color.substring(0, 2)
        action!!.gValue = color.substring(2, 4)
        action!!.bValue = color.substring(4)
        action!!.wValue = "00"
        action!!.cValue = "00"
        action!!.sceneValue = Scene.DEFAULT.value

        setOnAction()
        resetSceneBg()
    }

    private fun previewAction() {
        if (action!!.sceneValue == Scene.RAINBOW.value ||
                action!!.sceneValue == Scene.COLOR_BLAST.value ||
                action!!.sceneValue == Scene.PARTY_MODE.value) {
            showToast(getString(R.string.toast_cannot_preview_dynamic_scene))
            return
        }
        disableUi()
        var countDownTimer = object : CountDownTimer(6000, 1000) {
            override fun onFinish() {
                enableUi()
                bt_preview.text = getString(R.string.preview)
            }

            override fun onTick(millisUntilFinished: Long) {
                bt_preview.text = "Preview for (${(millisUntilFinished / 1000).toInt()}) seconds"
            }

        }
        countDownTimer.start()
        var value = Math.round((sb_brightness.progress.toDouble()) * (MAX - MIN) / 100)
        var displayValue = (value.toInt() + MIN) / STEP * STEP
        action!!.brightnessValue = displayValue

        action!!.onOffValue = cb_onOffLight.isChecked

        var event = viewModel!!.showPreview(action!!)
        if (event.status == EventStatus.FAILURE) {
            countDownTimer.cancel()
            showToast("Failed to preview")
            enableUi()
        }

    }

    override fun serviceConnected() {
        init()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_action)
        requestToBindService(this)
    }

    private fun init() {
        initClickListeners()
        initViewModelAndObservers()
        initBundleData()
        updateUi()
    }

    private fun updateUi() {
        cb_onOffLight.isChecked = action!!.onOffValue!!

        var daylightValue = calculatePercentage(convertHexStringToInt(action!!.cValue!!).toFloat())
        sb_daylightControl.progress = daylightValue.toInt()

        sb_brightness.progress = calculateProgress((action!!.brightnessValue!!.toFloat()).roundToInt(), MIN, MAX, STEP)

        if (action!!.sceneValue != Scene.DEFAULT.value) {
            setSceneUi(getSceneValue(action!!.sceneValue))
        }
    }

    private fun getSceneValue(value: Int): Scene {
        Scene.values().iterator().forEach {
            if (it.value == value) {
                return it
            }
        }
        return Scene.DEFAULT
    }

    private fun calculateProgress(value: Int, MIN: Int, MAX: Int, STEP: Int): Int {
        return 100 * (value - MIN) / (MAX - MIN)
    }

    private fun calculatePercentage(value: Float): Float {
        return (value / 255) * 100
    }

    private fun initBundleData() {
        if (intent.extras != null) {
            action = intent.getSerializableExtra(BUNDLE_ACTION_DETAILS) as ActionDetails
        } else {
            // Should not occur
            Timber.e("No bundle data found")
        }
    }

    private fun initViewModelAndObservers() {
        viewModel = ViewModelProviders.of(this, (application as SvarochiApplication).wifiViewModelFactory)
                .get(ScheduleActionViewModel::class.java)
    }

    private fun initClickListeners() {
        bt_preview.setOnClickListener(this)
        ll_candleLight.setOnClickListener(this)
        ll_sunset.setOnClickListener(this)
        ll_energise.setOnClickListener(this)
        ll_colorBlast.setOnClickListener(this)
        ll_partyMode.setOnClickListener(this)
        ll_rainbow.setOnClickListener(this)
        tv_colorPalette.setOnClickListener(this)
        tv_photoGallery.setOnClickListener(this)
        tv_camera.setOnClickListener(this)
        iv_back.setOnClickListener(this)
        bt_saveButton.setOnClickListener(this)


        sb_daylightControl.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                setDayLight(seekBar!!.progress)
            }

        })

        sb_brightness.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                var value = Math.round((progress.toDouble()) * (MAX - MIN) / 100)
                var displayValue = (value.toInt() + MIN) / STEP * STEP
                tvBubbleIntensityValue.text = "$displayValue%"
                rlBubbleIntensity.setX((getIntensitySliderThumbPos() - ScreenUtils.dpToPx(13)).toFloat())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                rlBubbleIntensity.visibility = View.VISIBLE
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                rlBubbleIntensity.visibility = View.GONE
                action!!.brightnessValue = seekBar!!.progress
                setOnAction()
            }

        })

        cb_onOffLight.setOnClickListener { action!!.onOffValue = cb_onOffLight.isChecked }
    }

    private fun getIntensitySliderThumbPos(): Int {
        val width = (sb_brightness.getWidth()
                - sb_brightness.getPaddingLeft()
                - sb_brightness.getPaddingRight())
        return sb_brightness.getPaddingLeft() + width * sb_brightness.getProgress() / sb_brightness.getMax() + sb_brightness.getThumb().getIntrinsicWidth() / 2
    }


    private fun setDayLight(progress: Int) {
        var w_value = Utilities.convertIntToHexString((255 * (1 - (progress * 0.01))).toInt())
        var c_value = Utilities.convertIntToHexString((255 * (progress * 0.01)).toInt())

        action!!.rValue = "00"
        action!!.gValue = "00"
        action!!.bValue = "00"
        action!!.wValue = w_value
        action!!.cValue = c_value
        action!!.sceneValue = Scene.DEFAULT.value

        setOnAction()
        resetSceneBg()
    }

    private fun openPhotoGallery() {
        var fm = supportFragmentManager
        imagePickerFragment = ImagePickerDialogFragment.newInstance(Constants.PHOTO_GALLERY.absoluteValue)
        imagePickerFragment!!.show(fm, "image_picker_fragment")
    }

    private fun resetSceneBg() {
        ll_candleLight.background = null
        ll_sunset.background = null
        ll_energise.background = null
        ll_rainbow.background = null
        ll_colorBlast.background = null
        ll_partyMode.background = null
    }

    private fun setSceneUi(sceneType: Scene) {
        setOnAction()
        when (sceneType) {
            Scene.ENERGISE -> {
                ll_energise.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_colorBlast.background = null
                ll_sunset.background = null
                ll_partyMode.background = null

            }
            Scene.CANDLE_LIGHT -> {
                ll_candleLight.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_sunset.background = null
                ll_colorBlast.background = null
                ll_energise.background = null
                ll_partyMode.background = null

            }
            Scene.SUNSET -> {
                ll_sunset.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_colorBlast.background = null
                ll_energise.background = null
                ll_partyMode.background = null

            }
            Scene.COLOR_BLAST -> {
                ll_colorBlast.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_partyMode.background = null


            }
            Scene.PARTY_MODE -> {
                ll_partyMode.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_rainbow.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_colorBlast.background = null

            }
            Scene.RAINBOW -> {
                ll_rainbow.setBackgroundResource(R.drawable.today_tomorrow_selected)

                ll_colorBlast.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_partyMode.background = null
            }
            else -> {
                ll_colorBlast.background = null
                ll_candleLight.background = null
                ll_sunset.background = null
                ll_energise.background = null
                ll_partyMode.background = null
                ll_rainbow.background = null
            }
        }
    }

    private fun setOnAction() {
        cb_onOffLight.isChecked = true
        action!!.onOffValue = true
    }

    private fun setScene(sceneType: SceneType) {
        when (sceneType) {
            SceneType.ENERGISE -> {
                if (action!!.sceneValue == Scene.ENERGISE.value) {
                    resetSelectedScene()
                } else {
                    setSceneUi(Scene.ENERGISE)

                    action!!.rValue = "8B"
                    action!!.gValue = "28"
                    action!!.bValue = "00"
                    action!!.wValue = "00"
                    action!!.cValue = "4B"
                    action!!.sceneValue = Scene.ENERGISE.value
                }
            }
            SceneType.CANDLE_LIGHT -> {
                if (action!!.sceneValue == Scene.CANDLE_LIGHT.value) {
                    resetSelectedScene()
                } else {
                    setSceneUi(Scene.CANDLE_LIGHT)

                    action!!.rValue = "56"
                    action!!.gValue = "1A"
                    action!!.bValue = "00"
                    action!!.wValue = "00"
                    action!!.cValue = "06"
                    action!!.sceneValue = Scene.CANDLE_LIGHT.value
                }
            }
            SceneType.SUNSET -> {
                if (action!!.sceneValue == Scene.SUNSET.value) {
                    resetSelectedScene()
                } else {
                    setSceneUi(Scene.SUNSET)

                    action!!.rValue = "AF"
                    action!!.gValue = "17"
                    action!!.bValue = "00"
                    action!!.wValue = "00"
                    action!!.cValue = "00"
                    action!!.sceneValue = Scene.SUNSET.value
                }
            }
            SceneType.COLOR_BLAST -> {
                if (action!!.sceneValue == Scene.COLOR_BLAST.value) {
                    resetSelectedScene()
                } else {
                    setSceneUi(Scene.COLOR_BLAST)

                    action!!.rValue = "00"
                    action!!.gValue = "00"
                    action!!.bValue = "00"
                    action!!.wValue = "00"
                    action!!.cValue = "00"
                    action!!.sceneValue = Scene.COLOR_BLAST.value
                }
            }
            SceneType.PARTY_MODE -> {
                if (action!!.sceneValue == Scene.PARTY_MODE.value) {
                    resetSelectedScene()
                } else {
                    setSceneUi(Scene.PARTY_MODE)

                    action!!.rValue = "00"
                    action!!.gValue = "00"
                    action!!.bValue = "00"
                    action!!.wValue = "00"
                    action!!.cValue = "00"
                    action!!.sceneValue = Scene.PARTY_MODE.value
                }
            }
            SceneType.RAINBOW -> {
                if (action!!.sceneValue == Scene.RAINBOW.value) {
                    resetSelectedScene()
                } else {
                    setSceneUi(Scene.RAINBOW)

                    action!!.rValue = "00"
                    action!!.gValue = "00"
                    action!!.bValue = "00"
                    action!!.wValue = "00"
                    action!!.cValue = "00"
                    action!!.sceneValue = Scene.RAINBOW.value
                }
            }
        }
    }

    private fun resetSelectedScene() {
        ll_energise.background = null
        ll_rainbow.background = null
        ll_candleLight.background = null
        ll_colorBlast.background = null
        ll_sunset.background = null
        ll_partyMode.background = null

        action!!.rValue = "00"
        action!!.gValue = "00"
        action!!.bValue = "00"
        action!!.wValue = "FF"
        action!!.cValue = "00"
        action!!.sceneValue = Scene.DEFAULT.value

        updateUi()
    }

    private fun showColorPalette() {
        if (mColorPickerDialog == null) {
            initColorPickerDialog()
        }
        mColorPickerDialog!!.show()
    }

    private fun openCamera() {
        var fm = supportFragmentManager
        imagePickerFragment = ImagePickerDialogFragment.newInstance(Constants.CAMERA.absoluteValue)
        imagePickerFragment!!.show(fm, "image_picker_fragment")
    }

    private fun initColorPickerDialog() {
        if (mColorPickerDialog == null) {
            mColorPickerDialog = WifiColorPickerDialog.createColorPickerDialog(this)
            mColorPickerDialog!!.hideColorComponentsInfo()
            mColorPickerDialog!!.hideOpacityBar()
            mColorPickerDialog!!.hideHexaDecimalValue()
            mColorPickerDialog!!.hidePreviewBox()
            mColorPickerDialog!!.setOnColorPickedListener(onColorPickedListener)
        } else {
            Timber.d("Color picker dialog already initialised")
        }
    }

    private var onColorPickedListener = object : WifiColorPickerDialog.OnColorPickedListener {
        override fun onForceColorPicked(color: Int, hexVal: String?) {
            setColor(Utilities.convertColorToHexString(color))
        }

        override fun onColorPicked(color: Int, hexVal: String) {
            Timber.d("Color picked from color palette - ${Utilities.convertColorToHexString(color)}")
            setColor(Utilities.convertColorToHexString(color))
        }
    }


    private fun disableUi() {
        cb_onOffLight.isEnabled = false
        sb_brightness.isEnabled = false
        sb_daylightControl.isEnabled = false
        tv_photoGallery.isEnabled = false
        tv_camera.isEnabled = false
        tv_colorPalette.isEnabled = false
        ll_candleLight.isEnabled = false
        ll_energise.isEnabled = false
        ll_sunset.isEnabled = false
        ll_colorBlast.isEnabled = false
        ll_rainbow.isEnabled = false
        ll_partyMode.isEnabled = false
        bt_saveButton.isEnabled = false
        iv_back.isEnabled = false
        bt_preview.isEnabled = false
    }

    private fun enableUi() {
        cb_onOffLight.isEnabled = true
        sb_brightness.isEnabled = true
        sb_daylightControl.isEnabled = true
        tv_photoGallery.isEnabled = true
        tv_camera.isEnabled = true
        tv_colorPalette.isEnabled = true
        ll_candleLight.isEnabled = true
        ll_energise.isEnabled = true
        ll_sunset.isEnabled = true
        ll_colorBlast.isEnabled = true
        ll_rainbow.isEnabled = true
        ll_partyMode.isEnabled = true
        bt_saveButton.isEnabled = true
        iv_back.isEnabled = true
        bt_preview.isEnabled = true
    }

}
