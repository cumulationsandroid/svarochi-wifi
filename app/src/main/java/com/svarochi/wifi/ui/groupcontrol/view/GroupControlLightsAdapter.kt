package com.svarochi.wifi.ui.groupcontrol.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Constants.Companion.LIGHT_SYNCED
import com.svarochi.wifi.model.common.CustomLight
import timber.log.Timber

class GroupControlLightsAdapter(var lightsList: List<CustomLight>,var isSelfProfile:Boolean, var context: GroupControlActivity) :
        androidx.recyclerview.widget.RecyclerView.Adapter<GroupControlLightsAdapter.GroupControlViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): GroupControlViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_group_control_light, viewGroup, false)
        return GroupControlViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lightsList.size
    }

    override fun onBindViewHolder(viewHolder: GroupControlViewHolder, position: Int) {
        viewHolder.lightName.text = lightsList.get(position).light.name

        viewHolder.lightCheckBox.isChecked = lightsList.get(position).isSelected()
    }

    fun resetCheckBoxOfMacId(macId: String) {
        for (light in lightsList) {
            if (light.light.macId.equals(macId)) {
                light.setSelected(!light.isSelected())
                break
            }
        }
        notifyDataSetChanged()
    }

    inner class GroupControlViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var lightName: TextView = itemView.findViewById(R.id.tv_lightName)
        var lightCheckBox: CheckBox = itemView.findViewById(R.id.cb_selectLightCheckBox)

        init {
            lightCheckBox.setOnClickListener {
                if(isSelfProfile) {
                    if(lightsList.get(adapterPosition).light.isSynced == LIGHT_SYNCED) {
                        if (lightCheckBox.isChecked) {
                            // Add light to group
                            lightsList.get(adapterPosition).setSelected(true)
                            context.addLightToGroup(lightsList.get(adapterPosition).light.macId)
                        } else {
                            // Remove light from group
                            lightsList.get(adapterPosition).setSelected(false)
                            context.removeLightFromGroup(lightsList.get(adapterPosition).light.macId)
                        }
                    }else{
                        lightCheckBox.isChecked = lightCheckBox.isChecked.not()
                        context.showToast(context.getString(R.string.toast_light_unsynced_group_scene))
                        Timber.d("Light(${lightsList.get(adapterPosition).light.macId}) is not synced")
                    }
                }else{
                    lightCheckBox.isChecked = lightCheckBox.isChecked.not()
                    Timber.d("User is in guest mode hence cannot add or remove lights of group")
                }
            }
        }
    }
}