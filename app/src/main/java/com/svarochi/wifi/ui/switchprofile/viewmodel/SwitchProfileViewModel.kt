package com.svarochi.wifi.ui.switchprofile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.database.entity.Profile
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.ui.switchprofile.repository.SwitchProfileRepository
import io.reactivex.disposables.CompositeDisposable

class SwitchProfileViewModel(var repository: SwitchProfileRepository) : BaseViewModel() {

    private var disposables: CompositeDisposable = CompositeDisposable()
    private var response: MutableLiveData<Event> = MutableLiveData()

    init {
        initResponseObserver()
    }

    private fun initResponseObserver() {
        initResponseObserver(disposables, repository, response)
    }

    override fun onCleared() {
        disposables.clear()
        repository.dispose()
    }

    fun getResponse(): MutableLiveData<Event> {
        return response
    }

    fun getSharedWithProfiles(): LiveData<List<Profile>> {
        return repository.getSharedWithProfilesLiveData()
    }
}