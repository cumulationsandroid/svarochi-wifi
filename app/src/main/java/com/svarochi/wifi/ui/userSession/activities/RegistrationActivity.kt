package com.svarochi.wifi.ui.userSession.activities

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import bizbrolly.svarochiapp.R
import com.akkipedia.skeleton.utils.GeneralUtils
import com.bizbrolly.Constants
import com.bizbrolly.WebServiceRequests
import com.bizbrolly.entities.AddDetailsResponse
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.common.CommonUtils
import com.svarochi.wifi.common.DialogUtils
import com.svarochi.wifi.preference.Preferences
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NAVIGATE_FROM
import kotlinx.android.synthetic.main.activity_registration.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegistrationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        init()
    }

    private fun init() {
        setListeners()
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                tvPhoneCode.setTextColor(ContextCompat.getColor(this@RegistrationActivity, if (s.length > 0) R.color.white else R.color.grey))
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        etConfirmPassword.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateRegistration()
            }
            false
        }

        val termsAndConditionsClick = object : ClickableSpan() {
            override fun onClick(widget: View) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TERMS_AND_CONDITIONS)))
            }
        }
        val privacyPolicyClick = object : ClickableSpan() {
            override fun onClick(widget: View) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.PRIVACY_POLICY)))
            }
        }
        val styledAgreeCheckString = SpannableString(getString(R.string.i_agree_to_the_terms_amp_conditions_and_privacy_policy))
        styledAgreeCheckString.setSpan(termsAndConditionsClick, 15, 33, 0)
        styledAgreeCheckString.setSpan(privacyPolicyClick, 38, 52, 0)
        // this step is mandated for the url and clickable styles.
        tv_agree.movementMethod = LinkMovementMethod.getInstance()
        styledAgreeCheckString.setSpan(UnderlineSpan(), 15, 33, 0)
        styledAgreeCheckString.setSpan(UnderlineSpan(), 38, 52, 0)
        styledAgreeCheckString.setSpan(ForegroundColorSpan(Color.WHITE), 15, 33, 0)
        styledAgreeCheckString.setSpan(ForegroundColorSpan(Color.WHITE), 38, 52, 0)
        tv_agree.text = styledAgreeCheckString
    }

    fun actionEmailHint(view: View) {
        DialogUtils.showDefaultAlertMessage(this, "", getString(R.string.email_hint), getString(R.string.ok), null)
    }

    fun actionSecretCodeHint(view: View) {
        DialogUtils.showDefaultAlertMessage(this, "", getString(R.string.secret_code_hint), getString(R.string.ok), null)
    }

    fun actionPasswordHint(view: View) {
        if (ivPasswordHint.tag != null && "close".equals(ivPasswordHint.tag.toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(etPassword.text.toString().trim())) {
                etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                ivPasswordHint.setImageResource(R.drawable.icon_eye)
                ivPasswordHint.tag = "open"
                etPassword.setSelection(etPassword.text?.length!!)
            }
        } else {
            etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            ivPasswordHint.setImageResource(R.drawable.icon_closed_eye)
            ivPasswordHint.tag = "close"
            etPassword.setSelection(etPassword.text?.length!!)
        }
    }

    fun actionConfirmPasswordHint(view: View) {
        if (ivConfirmPasswordHint.tag != null && "close".equals(ivConfirmPasswordHint.tag.toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(etConfirmPassword.text.toString().trim())) {
                etConfirmPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                ivConfirmPasswordHint.setImageResource(R.drawable.icon_eye)
                ivConfirmPasswordHint.tag = "open"
                etConfirmPassword.setSelection(etConfirmPassword.text?.length!!)
            }
        } else {
            etConfirmPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            ivConfirmPasswordHint.setImageResource(R.drawable.icon_closed_eye)
            ivConfirmPasswordHint.tag = "close"
            etConfirmPassword.setSelection(etConfirmPassword.text?.length!!)
        }
    }

    fun actionRegistration(view: View) {
        validateRegistration()
    }

    private fun validateRegistration() {
        val email = etEmail.text.toString().trim()
        val phone = etPhone.text.toString().trim()
        //Rahul; 2019/05/07 Removing use of secret code
        val secretCode = etSecretCode.text.toString().trim()
        val password = etPassword.text.toString().trim()
        val confirmPassword = etConfirmPassword.text.toString().trim()

        if (TextUtils.isEmpty(email) && TextUtils.isEmpty(phone)) {
            showToast(getString(R.string.please_enter_email_or_phone_number))
            return
        } else if (!TextUtils.isEmpty(email) && !CommonUtils.isValidEmail(email)) {
            showToast(getString(R.string.please_enter_valid_email))
            return
        } else if (!TextUtils.isEmpty(phone) && phone.length < 10) {
            showToast(getString(R.string.please_enter_correct_phone_number))
            return
        } else if (!TextUtils.isEmpty(phone) && phone.startsWith("+91")) {
            showToast(getString(R.string.please_enter_phone_number_without_country_code))
            return
        } /*else if (TextUtils.isEmpty(secretCode)) {
            showToast(getString(R.string.please_enter_secret_code));
            return;
        } */
        else if (TextUtils.isEmpty(password)) {
            showToast(getString(R.string.please_enter_password))
            return
        } else if (password.length < 6) {
            showToast(getString(R.string.password_must_have_atleast_six_characters))
            return
        } else if (!password.matches(".*\\d+.*".toRegex())) {
            showToast(getString(R.string.password_must_have_atleast_one_number))
            return
        } else if (TextUtils.isEmpty(confirmPassword)) {
            showToast(getString(R.string.please_enter_confirm_password))
            return
        } else if (password != confirmPassword) {
            showToast(getString(R.string.password_and_confirm_password_should_not_mismatch))
            return
        } else if (!cb_agree.isChecked) {
            showToast("Please read and accept to the Terms & Conditions and Privacy Policy")
            return
        } else {
            CommonUtils.hideSoftKeyboard(this@RegistrationActivity)
            requestRegistration()
        }
    }

    private fun requestRegistration() {
        if (!GeneralUtils.isInternetAvailable(this)) {
            showToast(getString(R.string.no_internet))
            return
        }
        showProgressDialog()
        val email = etEmail.text.toString().trim()
        val phone = etPhone.text.toString().trim()
        val secretCode = etSecretCode.text.toString().trim()
        val password = etPassword.text.toString().trim()

        WebServiceRequests.getInstance().createUser(email, phone, secretCode, password,
                object : Callback<AddDetailsResponse> {
                    override fun onResponse(call: Call<AddDetailsResponse>, response: Response<AddDetailsResponse>) {
                        try {
                            Log.e(TAG, "response createUser: " + response.body()!!.toString())
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        hideProgressDialog()
                        if (response?.body() != null && response.body()!!.addDetailsResult != null) {
                            if (response.body()!!.addDetailsResult.isResult) {
                                Preferences.getInstance(this@RegistrationActivity).setUserMail(email)
                                Preferences.getInstance(this@RegistrationActivity).phone = phone
                                Preferences.getInstance(this@RegistrationActivity).secretCode = secretCode
                                Preferences.getInstance(this@RegistrationActivity).networkPassword = password
                                Preferences.getInstance(this@RegistrationActivity).isPasswordSaved = true
                                Preferences.getInstance(this@RegistrationActivity).isOTPWaiting = true
                                Preferences.getInstance(this@RegistrationActivity).setGroupId("0")
                                if (!TextUtils.isEmpty(email) && TextUtils.isEmpty(phone)) {
                                    showToast(getString(R.string.otp_has_been_sent_to_this_email, email))
                                } else if (TextUtils.isEmpty(email) && !TextUtils.isEmpty(phone)) {
                                    showToast(getString(R.string.otp_has_been_sent_to_this_phone, phone))
                                } else if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(phone)) {
                                    showToast(getString(R.string.otp_has_been_sent_to_this_email_phone, email, phone))
                                }

                                startActivity(Intent(this@RegistrationActivity, OTPActivity::class.java))
                            } else {
                                if (response.body()!!.addDetailsResult.errorDetail != null && response.body()!!.addDetailsResult.errorDetail.errorDetails != null) {
                                    showToast(response.body()!!.addDetailsResult.errorDetail.errorDetails)
                                } else {
                                    showToast(getString(R.string.something_went_wrong))
                                }
                            }
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }

                    override fun onFailure(call: Call<AddDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else
                            showToast(getString(R.string.something_went_wrong))
                    }
                }
        )
    }

    override fun onBackPressed() {
        if (intent != null
                && intent.getStringExtra(BUNDLE_NAVIGATE_FROM) != null
                && intent.getStringExtra(BUNDLE_NAVIGATE_FROM).equals(OTPActivity::class.java.simpleName, ignoreCase = true)) {
            startActivity(Intent(this, NewAccountActivity::class.java))
            finish()
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        private val TAG = RegistrationActivity::class.java.simpleName
    }
}
