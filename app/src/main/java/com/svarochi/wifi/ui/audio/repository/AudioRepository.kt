package com.svarochi.wifi.ui.audio.repository

import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Utilities.Companion.convertIntToHexString
import com.svarochi.wifi.database.LocalDataSource
import com.svarochi.wifi.database.entity.GroupLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.Command
import com.svarochi.wifi.service.communication.CommunicationService
import com.svarochi.wifi.ui.audio.AudioImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class AudioRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl, AudioImpl {

    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var communicationService: CommunicationService = application.wifiCommunicationService
    private var localDataSource: LocalDataSource = application.wifiLocalDatasource
    private var isObservingResponse = false

    init {
        initCommunicationResponseObserver()

    }

    override fun initCommunicationResponseObserver() {
        if (!isObservingResponse) {
            isObservingResponse = true

            Timber.d("Initialised response observer")
            communicationService.getResponseObservable()
                    .observeOn(Schedulers.io())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        Timber.d("Listening to responseObservable of service...")
                        disposables.add(it!!)
                    }
                    .subscribe(object : DisposableObserver<Event>() {
                        override fun onComplete() {
                        }

                        override fun onNext(event: Event) {
                            responseObservable.onNext(event)
                        }

                        override fun onError(e: Throwable) {
                            Timber.e(e)
                            //responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.ERROR, null, e))
                        }
                    })
        }
    }

    override fun getCommunicationResponseObserver() = responseObservable

    override fun dispose() {
        isObservingResponse = false
        Timber.d("Clearing all observers")
        disposables = disposeDisposables(disposables)
    }

    override fun getLights(networkId: Long) {
        Timber.d("Fetching lights of network($networkId)")
        var lights = localDataSource.getLights(networkId)
        responseObservable.onNext(Event(EventType.GET_LIGHTS, EventStatus.SUCCESS, lights, null))
    }

    override fun getGroups(networkId: Long) {
        Timber.d("Fetching groups of network($networkId)")
        var groups = localDataSource.getGroups(networkId)
        responseObservable.onNext(Event(EventType.GET_GROUPS, EventStatus.SUCCESS, groups, null))
    }

    /*override fun setGroupRgb(groupId: Long, r_value: Int, g_value: Int, b_value: Int) {
        var lights = getLightsOfGroup(groupId)

        for (light in lights) {
            setLightRgb(light.macId, r_value, g_value, b_value)
        }
    }*/

    private fun getLightsOfGroup(groupId: Long): List<GroupLight> {
        Timber.d("Fetching lights of group($groupId)")
        return localDataSource.getLightsOfGroup(groupId)
    }

    /*override fun setLightRgb(macId: String, r_value: Int, g_value: Int, b_value: Int) {
        Timber.d("Setting RGB values to light($macId)")
        var rgbPayload = HashMap<String, String>()
        rgbPayload[Constants.BUNDLE_R_VALUE] = convertIntToHexString(r_value)
        rgbPayload[Constants.BUNDLE_G_VALUE] = convertIntToHexString(g_value)
        rgbPayload[Constants.BUNDLE_B_VALUE] = convertIntToHexString(b_value)

        var command = Command(EventType.SET_LIGHT_RGB, macId, rgbPayload)
        communicationService.sendCommand(command)
    }*/

    fun getDevicesOfGroup(groupId: Long) {
        var lights = getLightsOfGroup(groupId)
        responseObservable.onNext(Event(EventType.GET_GROUP_DEVICES, EventStatus.SUCCESS, lights, null))
    }

     fun setGroupRgbWithBrightness(groupId: Long, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        var lights: List<GroupLight>? = null

        if (lights == null) {
            lights = getLightsOfGroup(groupId)
        }

        for (light in lights) {
            setLightRgbWithBrightness(light.macId, r_value, g_value, b_value, brightness_value)
        }
    }

     fun setLightRgbWithBrightness(macId: String, r_value: Int, g_value: Int, b_value: Int, brightness_value: Int) {
        Timber.d("Setting RGB values to light($macId)")
        var rgbPayload = HashMap<String, Any>()
        rgbPayload[Constants.BUNDLE_R_VALUE] = convertIntToHexString(r_value)
        rgbPayload[Constants.BUNDLE_G_VALUE] = convertIntToHexString(g_value)
        rgbPayload[Constants.BUNDLE_B_VALUE] = convertIntToHexString(b_value)
        rgbPayload[Constants.BUNDLE_BRIGHTNESS_VALUE] = brightness_value

        var command = Command(EventType.SET_LIGHT_RGB_BRIGHTNESS, macId, rgbPayload)
        communicationService.sendCommand(command)
    }

    fun resetAudioFlag() {
        communicationService.isMusicActivity(false)
    }

    fun setAudioFlag(){
        communicationService.isMusicActivity(true)

    }
}