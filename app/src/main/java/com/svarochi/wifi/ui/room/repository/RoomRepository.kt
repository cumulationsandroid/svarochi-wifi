package com.svarochi.wifi.ui.room.repository

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_API_DAYLIGHT
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_API_DIMMABLE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_API_ON
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_AUTHENTICATION_MODE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_ENCRYPTION_TYPE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_LAMP_SSID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PASSWORD
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SSID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SSID_TO_CONFIGURE
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.common.Constants.Companion.DEFAULT_MAC_ID
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_ALL
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_BRIGHTNESS_AND_COLOR
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_BRIGHTNESS_AND_DAYLIGHT
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_BRIGHTNESS_ONLY
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_COLOR_ONLY
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_DAYLIGHT_AND_COLOR
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_DAYLIGHT_ONLY
import com.svarochi.wifi.common.Constants.Companion.GROUP_TAG_NONE
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.common.Utilities.Companion.convertHexStringToInt
import com.svarochi.wifi.common.Utilities.Companion.convertIntToHexString
import com.svarochi.wifi.database.LocalDataSource
import com.svarochi.wifi.database.entity.*
import com.svarochi.wifi.database.entity.Group
import com.svarochi.wifi.model.api.common.CustomSceneDevice
import com.svarochi.wifi.model.api.common.UpdateGroupType
import com.svarochi.wifi.model.api.common.UpdateSceneType
import com.svarochi.wifi.model.api.request.DeviceState
import com.svarochi.wifi.model.api.request.SceneDevice
import com.svarochi.wifi.model.api.response.*
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.Command
import com.svarochi.wifi.model.communication.LampType
import com.svarochi.wifi.model.communication.SceneType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.service.communication.CommunicationService
import com.svarochi.wifi.ui.room.GroupsImpl
import com.svarochi.wifi.ui.room.LightsImpl
import com.svarochi.wifi.ui.room.ScenesImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class RoomRepository(var application: SvarochiApplication) : BaseRepository(),
        BaseRepositoryImpl, LightsImpl, ScenesImpl, GroupsImpl {
    private var routerBroadcastAddress: String? = null
    private var broadcastDisposable: Disposable? = null
    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var broadcastFlag: Boolean = false
    private var isObservingConnectionStatus = false
    private var isObservingResponse = false
    private var networkId: Long = -1
    private var projectId: Long = -1
    private var userId: String = ""
    private var communicationService: CommunicationService = application.wifiCommunicationService
    private var localDataSource: LocalDataSource = application.wifiLocalDatasource
    private var sharedPreference: SharedPreference? = null
    private var isSelfProfile = true
    private var profileId: String? = null

    private val pendingLightMapType = object : TypeToken<HashMap<String, String>>() {
    }.type
    private val mapper = ObjectMapper() // jackson's objectmapper


    init {
        fetchIdsFromPreferences()
        callApis()
    }

    private fun callApis() {
        getLightsFromServer()
    }

    private fun fetchIdsFromPreferences() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        projectId = sharedPreference!!.getProjectId()
        networkId = sharedPreference!!.getNetworkId()
        userId = sharedPreference!!.getUserId()
        profileId = sharedPreference!!.getCurrentProfile()
        isSelfProfile = profileId.equals(CURRENT_PROFILE_SELF)
    }

    override fun initCommunicationResponseObserver() {
        if (!isObservingResponse) {
            isObservingResponse = true

            Timber.d("Initialised response observer")
            communicationService!!.getResponseObservable()
                    .observeOn(Schedulers.io())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        Timber.d("Listening to responseObservable of service...")
                        disposables.add(it!!)
                    }
                    .subscribe(object : DisposableObserver<Event>() {
                        override fun onComplete() {
                        }

                        override fun onNext(event: Event) {
                            responseObservable.onNext(event)
                        }

                        override fun onError(e: Throwable) {
                            Timber.e(e)
                            //responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.ERROR, null, e))
                        }
                    })
        }
    }

    override fun getCommunicationResponseObserver() = responseObservable

    fun initObservers() {
        initCommunicationResponseObserver()
    }

    fun disconnectClients() {
        communicationService!!.disconnectClients()
    }

    override fun dispose() {
        isObservingConnectionStatus = false
        isObservingResponse = false
        Timber.d("Clearing all observers")
        disposables = disposeDisposables(disposables)
    }

    fun startBroadcast() {
        communicationService!!.startBroadcast()
    }

    fun stopBroadcast() {
        Timber.d("UDP broadcasting stopped")
        communicationService!!.stopBroadcast()
        disposeBroadcastDisposable()
        broadcastFlag = false
    }

    private fun disposeBroadcastDisposable() {
        if (broadcastFlag) {
            broadcastDisposable!!.dispose()
        }
    }

    /*Lights*/
    override fun getLights(networkId: Long) {
        var lightList = localDataSource.getLights(networkId)
        var customLightList = ArrayList<CustomLight>()
        for (light in lightList) {
            var customLight = CustomLight(light)
            customLight.setLocallyAvailable(communicationService!!.isDeviceLocallyAvailable(light.macId))
            customLightList.add(customLight)
        }
        Timber.d("Sending ${lightList.size} lights from DB...")
        responseObservable.onNext(Event(EventType.GET_LIGHTS, EventStatus.SUCCESS, customLightList, null))
    }

    override fun turnOnLight(macId: String?) {
        var command = Command(EventType.TURN_ON_LIGHT, macId!!, null)
        communicationService!!.sendCommand(command)
    }

    override fun turnOffLight(macId: String?) {
        var command = Command(EventType.TURN_OFF_LIGHT, macId!!, null)
        communicationService!!.sendCommand(command)
    }

    override fun setWifiCredentials(
            lampName: String,
            lampSsid: String,
            ssidToConfigure: String,
            password: String,
            authenticationMode: String,
            encryptionType: String) {

        val updatedLampSsid = lampSsid.replace("\"","")
        var map = HashMap<String, Any>()
        map[BUNDLE_SSID] = lampName
        map[BUNDLE_LAMP_SSID] = updatedLampSsid
        map[BUNDLE_SSID_TO_CONFIGURE] = ssidToConfigure
        map[BUNDLE_PASSWORD] = password
        map[BUNDLE_AUTHENTICATION_MODE] = authenticationMode
        map[BUNDLE_ENCRYPTION_TYPE] = encryptionType

        var command = Command(EventType.SET_WIFI, DEFAULT_MAC_ID, map)
        communicationService.sendCommand(command)
    }

    /*Scenes*/
    override fun getScenes(networkId: Long) {
        Timber.d("Fetching scenes of network - $networkId")
        var sceneList = localDataSource.getScenes(networkId)
        responseObservable.onNext(Event(EventType.GET_SCENES, EventStatus.SUCCESS, sceneList, null))
    }

    override fun addNewScene(name: String, lightList: List<CustomSceneDevice>) {
        insertNewSceneIntoServer(name, lightList)
    }

    private fun insertNewSceneIntoServer(name: String, lightsList: List<CustomSceneDevice>) {
        /**
         * Steps involved -
         * 1. Call addscene api
         * 2. From the success response of the addscene api parse the sceneId
         * 3. Add the devices with sceneId to SceneLight table
         *
         * */
        if (GeneralUtils.isInternetAvailable(application)) {
            var sceneDevices = ArrayList<SceneDevice>()
            for (light in lightsList) {
                val deviceStateToAdd = SceneDevice(
                        light.mac_id,
                        light.blue,
                        light.brightness,
                        light.daylight_control,
                        light.green,
                        light.red,
                        light.status,
                        light.scene_value
                )
                sceneDevices.add(deviceStateToAdd)
            }
            disposables.add(
                    ApiRequestParser.addScene(userId, sceneDevices.toList(), name, networkId.toInt(), projectId.toInt())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<AddSceneResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed Add Scene api")
                                }

                                override fun onNext(response: Response<AddSceneResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("Add Scene API response body - ${response.toString()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully added scene into server")
                                                if (response.body()!!.scene_id != null) {
                                                    insertNewSceneIntoDb(
                                                            name,
                                                            response.body()!!.scene_id!!.toLong(),
                                                            lightsList
                                                    )
                                                } else {
                                                    Timber.d("No sceneId received from server")
                                                }
                                            }
                                        } else {
                                            Timber.d("Something went wrong while adding scene into server")
                                            Timber.d("No response got from server")
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_NEW_SCENE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("Something went wrong while adding scene into server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                        responseObservable.onNext(Event(EventType.ADD_NEW_SCENE, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while adding scene into server")
                                    Timber.e(error)
                                    responseObservable.onNext(Event(EventType.ADD_NEW_SCENE, EventStatus.FAILURE, null, null))
                                }

                            })
            )
        } else {
            Timber.d("No Internet available")
            responseObservable.onNext(Event(EventType.ADD_NEW_SCENE, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun getDaylightValue(cValue: String): Int {
        var cValueInInt = convertHexStringToInt(cValue)
        return ((cValueInInt / 255) * 100)
    }

    private fun insertNewSceneIntoDb(name: String, sceneId: Long, lightsList: List<CustomSceneDevice>) {
        Timber.d("Adding scenes of network - $networkId")
        var scene = Scene(sceneId, name, networkId)
        localDataSource.addScene(scene)
        Timber.d("Added scene($sceneId) to network($networkId)")

        Timber.d("Adding scene and lights to SceneLight table")
        for (customSceneDevice in lightsList) {
            Timber.d("Adding scene - $sceneId, light - ${customSceneDevice.mac_id}")
            var sceneLight = SceneLight()

            sceneLight.setSceneId(sceneId)
            sceneLight.setMacId(customSceneDevice.mac_id)
            sceneLight.setrValue(convertIntToHexString(customSceneDevice.red))
            sceneLight.setgValue(convertIntToHexString(customSceneDevice.green))
            sceneLight.setbValue(convertIntToHexString(customSceneDevice.blue))
            sceneLight.setwValue(getWValue(customSceneDevice.daylight_control))
            sceneLight.setcValue(getCValue(customSceneDevice.daylight_control))
            sceneLight.setBrightnessValue(customSceneDevice.brightness)
            sceneLight.setOnOffValue(getLampOnOffStatus(customSceneDevice.status))
            sceneLight.setSceneValue(convertIntToHexString(customSceneDevice.scene_value))

            localDataSource.addLightToScene(sceneLight)
        }
        Timber.d("Added scene and lights to network - $networkId")

        responseObservable.onNext(Event(EventType.ADD_NEW_SCENE, EventStatus.SUCCESS, null, null))
    }

    override fun editSceneName(sceneId: Long, name: String) {
        callUpdateSceneApi(sceneId, name, UpdateSceneType.UPDATE_NAME, null)
    }

    override fun turnOnScene(sceneId: Long) {
        var command = Command(EventType.TURN_ON_SCENE, sceneId, null)
        communicationService!!.sendCommand(command)
        responseObservable.onNext(Event(EventType.TURN_ON_SCENE, EventStatus.SUCCESS, null, null))
    }

    override fun turnOffScene(sceneId: Long) {
        var command = Command(EventType.TURN_OFF_SCENE, sceneId, null)
        communicationService!!.sendCommand(command)
        responseObservable.onNext(Event(EventType.TURN_OFF_SCENE, EventStatus.SUCCESS, null, null))
    }

    /*Used only while editing a scene*/
    override fun getLightsAndMarkThoseOfScene(networkId: Long, sceneId: Long) {
        Timber.d("Fetching lights of network - $networkId")
        var lightList = localDataSource.getLights(networkId)
        Timber.d("Got ${lightList.size} lights of network - $networkId")

        var customLightList = ArrayList<CustomLight>(lightList.size)

        for (light in lightList) {
            var customLight = CustomLight(light)
            Timber.d("Checking if ${light.macId} belongs to scene")
            var queryList = localDataSource.getLightsFromScenesOfMacId(sceneId, light.macId)

            if (queryList.isEmpty()) {
                Timber.d("Light ${light.macId} doesn't belong to scene")
                customLight.setSelected(false)
            } else {
                Timber.d("Light ${light.macId} belongs to scene")
                customLight.setSelected(true)
            }
            customLightList.add(customLight)
        }

        responseObservable.onNext(
                Event(
                        EventType.GET_LIGHTS_OF_SCENE,
                        EventStatus.SUCCESS,
                        customLightList,
                        null
                )
        )
    }

    override fun checkIfSceneExists(name: String) {
        Timber.d("Checking if scene - $name already exists")
        var queryList = localDataSource.getScenesOfName(name)

        if (queryList.isNotEmpty()) {
            Timber.d("Scene by name - $name already exists")
            responseObservable.onNext(
                    Event(
                            EventType.CHECK_IF_SCENE_EXISTS,
                            EventStatus.SUCCESS,
                            ALREADY_EXISTS,
                            null
                    )
            )
        } else {
            Timber.d("Scene by name - $name doesn't exists")
            responseObservable.onNext(
                    Event(
                            EventType.CHECK_IF_SCENE_EXISTS,
                            EventStatus.SUCCESS,
                            name,
                            null
                    )
            )
        }
    }

    override fun editSceneLightsList(sceneId: Long, lightList: List<CustomSceneDevice>) {
        var scene = localDataSource.getLatestScene(sceneId)
        callUpdateSceneApi(sceneId, scene.name, UpdateSceneType.EDIT_LIGHTS, lightList)
    }


    private fun callUpdateSceneApi(
            sceneId: Long,
            sceneName: String,
            requestType: UpdateSceneType,
            lightsList: List<CustomSceneDevice>?
    ) {

        if (GeneralUtils.isInternetAvailable(application)) {
            var deviceStatesList = ArrayList<DeviceState>()
            if (requestType == UpdateSceneType.EDIT_LIGHTS && lightsList != null) {
                for (light in lightsList!!) {
                    val deviceStateToAdd = DeviceState(
                            light.mac_id,
                            light.blue,
                            light.brightness,
                            light.daylight_control,
                            light.green,
                            light.red,
                            sceneId.toInt(),
                            light.status,
                            light.scene_value
                    )
                    deviceStatesList.add(deviceStateToAdd)
                }
            } else {
                var sceneLights = application.wifiLocalDatasource.getLightsOfScene(sceneId)
                for (light in sceneLights) {
                    val deviceStateToAdd = DeviceState(
                            light.macId,
                            convertHexStringToInt(light.bValue),
                            light.brightnessValue,
                            getDaylightValue(light.cValue),
                            convertHexStringToInt(light.gValue),
                            convertHexStringToInt(light.rValue),
                            sceneId.toInt(),
                            getLampOnOffStatus(light.onOffValue),
                            convertHexStringToInt(light.sceneValue)
                    )
                    deviceStatesList.add(deviceStateToAdd)
                }
            }
            disposables.add(
                    ApiRequestParser.updateScene(
                            userId,
                            deviceStatesList.toList(),
                            sceneId.toInt(),
                            sceneName,
                            networkId.toInt(),
                            projectId.toInt()
                    )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<UpdateSceneResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed Update Scene api")
                                }

                                override fun onNext(response: Response<UpdateSceneResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("Update Scene API response body - ${response.toString()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully updated scene in server")
                                                if (response.body()!!.scene_id != null) {
                                                    when (requestType) {
                                                        UpdateSceneType.UPDATE_NAME -> {
                                                            editSceneNameInDb(sceneId, sceneName)
                                                        }
                                                        UpdateSceneType.EDIT_LIGHTS -> {
                                                            editLightsOfSceneDb(response.body()!!.scene_id!!.toLong(), lightsList!!)
                                                        }
                                                    }
                                                } else {
                                                    Timber.d("No sceneId received from server")
                                                }
                                            }
                                        } else {
                                            Timber.d("Something went wrong while updating scene in server")
                                            Timber.d("No response got from server")

                                            when (requestType) {
                                                UpdateSceneType.UPDATE_NAME -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.EDIT_SCENE_NAME,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }
                                                UpdateSceneType.EDIT_LIGHTS -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.EDIT_LIGHTS_OF_SCENE,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while updating scene in server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                        when (requestType) {
                                            UpdateSceneType.UPDATE_NAME -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_SCENE_NAME,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                            UpdateSceneType.EDIT_LIGHTS -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_LIGHTS_OF_SCENE,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        }
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while adding group into server")
                                    Timber.e(error)
                                    when (requestType) {
                                        UpdateSceneType.UPDATE_NAME -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_SCENE_NAME,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                        UpdateSceneType.EDIT_LIGHTS -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_LIGHTS_OF_SCENE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    }
                                }

                            })
            )
        } else {
            when (requestType) {
                UpdateSceneType.UPDATE_NAME -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.EDIT_SCENE_NAME,
                                    EventStatus.FAILURE,
                                    NO_INTERNET_AVAILABLE,
                                    null
                            )
                    )
                }
                UpdateSceneType.EDIT_LIGHTS -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.EDIT_LIGHTS_OF_SCENE,
                                    EventStatus.FAILURE,
                                    NO_INTERNET_AVAILABLE,
                                    null
                            )
                    )
                }
            }
        }
    }

    private fun editSceneNameInDb(sceneId: Long, sceneName: String) {
        localDataSource.updateSceneName(sceneId, sceneName)
        responseObservable.onNext(Event(EventType.EDIT_SCENE_NAME, EventStatus.SUCCESS, null, null))
    }

    private fun editLightsOfSceneDb(sceneId: Long, macIdList: List<CustomSceneDevice>) {
        Timber.d("Removing scenes of scene id - $sceneId")
        localDataSource.deleteScenes(sceneId)

        Timber.d("Adding scene and lights to SceneLight table")
        for (customSceneDevice in macIdList) {
            Timber.d("Adding scene - $sceneId, light - ${customSceneDevice.mac_id}")
            var sceneLight = SceneLight()

            sceneLight.setSceneId(sceneId)
            sceneLight.setMacId(customSceneDevice.mac_id)
            sceneLight.setrValue(convertIntToHexString(customSceneDevice.red))
            sceneLight.setgValue(convertIntToHexString(customSceneDevice.green))
            sceneLight.setbValue(convertIntToHexString(customSceneDevice.blue))
            sceneLight.setwValue(getWValue(customSceneDevice.daylight_control))
            sceneLight.setcValue(getCValue(customSceneDevice.daylight_control))
            sceneLight.setBrightnessValue(customSceneDevice.brightness)
            sceneLight.setOnOffValue(getLampOnOffStatus(customSceneDevice.status))
            sceneLight.setSceneValue(convertIntToHexString(customSceneDevice.scene_value))

            localDataSource.addLightToScene(sceneLight)
        }
        Timber.d("Edited scene id - $sceneId")

        responseObservable.onNext(Event(EventType.EDIT_LIGHTS_OF_SCENE, EventStatus.SUCCESS, null, null))
    }

    /*Groups*/
    override fun getGroups(networkId: Long) {
        Timber.d("Fetching groups of network - $networkId")
        var groupList = localDataSource.getGroups(networkId)
        responseObservable.onNext(Event(EventType.GET_GROUPS, EventStatus.SUCCESS, groupList, null))
    }

    override fun addNewGroup(name: String, lightList: ArrayList<Light>) {
        insertGroupIntoServer(name, lightList)
    }

    private fun insertNewGroupIntoDb(name: String, groupId: Long, networkId: Long, lightList: ArrayList<Light>) {
        /**
         * Steps involved -
         * 1. Call addgroup api
         * 2. From the success response of the addgroup api parse the groupId
         * 3. Add the devices with groupId to GroupLight table
         *
         * */
        Timber.d("Adding new group($name) to network($networkId)")
        var group = Group(groupId, name, networkId, GROUP_TAG_NONE)
        localDataSource.addGroup(group)

        Timber.d("Adding group and lights to GroupLight table")
        for (light in lightList) {
            Timber.d("Adding group($groupId), light(${light.macId})")
            var groupLight = GroupLight()
            groupLight.setGroupId(groupId)
            groupLight.setMacId(light.macId)

            localDataSource.addLightToGroup(groupLight)
        }
        Timber.d("Added group and lights to network - $networkId")

        updateGroupTag(groupId)

        responseObservable.onNext(Event(EventType.ADD_NEW_GROUP, EventStatus.SUCCESS, null, null))

    }

    private fun insertGroupIntoServer(name: String, lightList: ArrayList<Light>) {
        /**
         * Steps involved -
         * 1. Call addgroup api
         * 2. From the success response of the addgroup api parse the groupId
         * 3. Add the devices with groupId to GroupLight table
         *
         * */

        if (GeneralUtils.isInternetAvailable(application)) {
            var macIds = ArrayList<String>()
            for (light in lightList) {
                macIds.add(light.macId)
            }

            disposables.add(
                    ApiRequestParser.addGroup(userId, macIds.toList(), name, networkId.toInt(), projectId.toInt())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<AddGroupResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed Add Group api")
                                }

                                override fun onNext(response: Response<AddGroupResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("Add Group API response body - ${response.toString()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully added group into server")
                                                if (response.body()!!.group_id != null) {
                                                    insertNewGroupIntoDb(
                                                            name,
                                                            response.body()!!.group_id!!.toLong(),
                                                            networkId,
                                                            lightList
                                                    )
                                                } else {
                                                    Timber.d("No groupId received from server")
                                                }
                                            }
                                        } else {
                                            Timber.d("Something went wrong while adding group into server")
                                            Timber.d("No response got from server")
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_NEW_GROUP,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("Something went wrong while adding group into server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                        responseObservable.onNext(Event(EventType.ADD_NEW_GROUP, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while adding group into server")
                                    Timber.e(error)
                                    responseObservable.onNext(Event(EventType.ADD_NEW_GROUP, EventStatus.FAILURE, null, null))
                                }

                            })
            )
        } else {
            responseObservable.onNext(Event(EventType.ADD_NEW_GROUP, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    override fun checkIfGroupExists(name: String, networkId: Long) {
        Timber.d("Checking if group($name) already exists")
        var queryList = localDataSource.getGroupsOfName(name, networkId)

        if (queryList.isNotEmpty()) {
            Timber.d("Group by name - $name already exists")
            responseObservable.onNext(
                    Event(
                            EventType.CHECK_IF_GROUP_EXISTS,
                            EventStatus.SUCCESS,
                            ALREADY_EXISTS,
                            null
                    )
            )
        } else {
            Timber.d("Group by name - $name doesn't exists")
            responseObservable.onNext(
                    Event(
                            EventType.CHECK_IF_GROUP_EXISTS,
                            EventStatus.SUCCESS,
                            name,
                            null
                    )
            )
        }
    }

    override fun editGroupName(groupId: Long, name: String) {
        Timber.d("Changing group($groupId) to name - $name")
        localDataSource.editGroupName(groupId, name)
        Timber.d("Changed group($groupId) to name - $name")

        responseObservable.onNext(Event(EventType.EDIT_GROUP_NAME, EventStatus.SUCCESS, null, null))
    }

    override fun turnOnGroup(groupId: Long) {
//        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            group.rValue,
            group.gValue,
            group.bValue,
            group.wValue,
            group.cValue,
            group.sceneValue,
            group.brightnessValue,
            true
        )*/

        var command = Command(EventType.TURN_ON_GROUP, groupId, null)
        communicationService!!.sendCommand(command)

    }

    override fun turnOffGroup(groupId: Long) {
        var group = getLatestGroup(groupId)
        /*updateGroupStatus(
            groupId,
            group.rValue,
            group.gValue,
            group.bValue,
            group.wValue,
            group.cValue,
            group.sceneValue,
            group.brightnessValue,
            false
        )*/

        var command = Command(EventType.TURN_OFF_GROUP, groupId, null)
        communicationService!!.sendCommand(command)
    }

    /*private fun updateGroupStatus(
        groupId: Long,
        rValue: String,
        gValue: String,
        bValue: String,
        wValue: String,
        cValue: String,
        sceneValue: String,
        brightnessValue: Int,
        onOffStatus: Boolean
    ) {
        localDataSource.updateGroupStatus(
            groupId,
            rValue,
            gValue,
            bValue,
            wValue,
            cValue,
            sceneValue,
            brightnessValue,
            onOffStatus
        )
    }*/

    private fun getLatestGroup(groupId: Long): Group {
        return localDataSource.getLatestGroup(groupId)
    }

    override fun getLightsAndMarkThoseOfGroup(networkId: Long, groupId: Long) {
        Timber.d("Fetching lights of network - $networkId")
        var lightList = localDataSource.getLights(networkId)
        Timber.d("Got ${lightList.size} lights of network - $networkId")

        var customLightList = ArrayList<CustomLight>(lightList.size)

        for (light in lightList) {
            var customLight = CustomLight(light)
            Timber.d("Checking if ${light.macId} belongs to group")
            var queryList = localDataSource.getLightsFromGroupsOfMacId(groupId, light.macId)

            if (queryList.isEmpty()) {
                Timber.d("Light ${light.macId} doesn't belong to group")
                customLight.setSelected(false)
            } else {
                Timber.d("Light ${light.macId} belongs to group")
                customLight.setSelected(true)
            }
            customLightList.add(customLight)
        }

        responseObservable.onNext(
                Event(
                        EventType.GET_LIGHTS_OF_GROUP,
                        EventStatus.SUCCESS,
                        customLightList,
                        null
                )
        )
    }

    override fun editGroupLightsList(groupId: Long, lightMacIdList: ArrayList<String>) {
        var lightsListToSend = ArrayList<String>()

        for (lightMacId in lightMacIdList) {
            lightsListToSend.add(lightMacId)
        }

        Timber.d("Fetching group($groupId) name")
        var group = localDataSource.getLatestGroup(groupId)

        callUpdateGroupApi(lightsListToSend.toList(), groupId, group.name, UpdateGroupType.EDIT_LIGHTS)
    }

    override fun addLightToGroup(macId: String, groupId: Long) {
        Timber.d("Fetching lights of group($groupId)")
        var lightsList = getLightsOfGroup(groupId)
        var lightsListToSend = ArrayList<String>()

        for (light in lightsList) {
            lightsListToSend.add(light.macId)
        }
        if (lightsListToSend.contains(macId).not()) {
            lightsListToSend.add(macId)

            Timber.d("Fetching group($groupId) name")
            var group = localDataSource.getLatestGroup(groupId)

            callUpdateGroupApi(lightsListToSend.toList(), groupId, group.name, UpdateGroupType.ADD_LIGHT)
        } else {
            responseObservable.onNext(
                    Event(
                            EventType.ADD_LIGHT_TO_GROUP,
                            EventStatus.SUCCESS,
                            null,
                            null
                    )
            )
        }
    }

    private fun updateGroupLightsList(devices: List<String>, groupId: Long, requestType: UpdateGroupType) {
        Timber.d("Removing all lights of group($groupId)")
        localDataSource.clearLightsOfGroup(groupId)

        var groupLightsList = ArrayList<GroupLight>()
        for (lightMacId in devices) {

            var groupLightToAdd = GroupLight()
            groupLightToAdd.setGroupId(groupId)
            groupLightToAdd.setMacId(lightMacId)

            groupLightsList.add(groupLightToAdd)
        }

        localDataSource.insertGroupLights(groupLightsList)

        updateGroupTag(groupId)

        when (requestType) {
            UpdateGroupType.ADD_LIGHT -> {
                responseObservable.onNext(
                        Event(
                                EventType.ADD_LIGHT_TO_GROUP,
                                EventStatus.SUCCESS,
                                null,
                                null
                        )
                )
            }
            UpdateGroupType.REMOVE_LIGHT -> {
                responseObservable.onNext(
                        Event(
                                EventType.REMOVE_LIGHT_FROM_GROUP,
                                EventStatus.SUCCESS,
                                null,
                                null
                        )
                )
            }
            UpdateGroupType.EDIT_LIGHTS -> {
                responseObservable.onNext(
                        Event(
                                EventType.EDIT_LIGHTS_OF_GROUP,
                                EventStatus.SUCCESS,
                                null,
                                null
                        )
                )
            }
        }
    }

    private fun callUpdateGroupApi(macIds: List<String>, groupId: Long, name: String, requestType: UpdateGroupType) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.updateGroup(
                            userId,
                            macIds,
                            groupId.toInt(),
                            name,
                            networkId.toInt(),
                            projectId.toInt()
                    )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<UpdateGroupResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed Update group api")
                                }

                                override fun onNext(response: Response<UpdateGroupResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("Update Group API response body - ${response.toString()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully updated group in server")
                                                if (response.body()!!.group_id != null) {
                                                    when (requestType) {
                                                        UpdateGroupType.ADD_LIGHT -> {
                                                            updateGroupLightsList(macIds, groupId, requestType)
                                                        }

                                                        UpdateGroupType.REMOVE_LIGHT -> {
                                                            updateGroupLightsList(macIds, groupId, requestType)
                                                        }

                                                        UpdateGroupType.UPDATE_NAME -> {

                                                        }

                                                        UpdateGroupType.EDIT_LIGHTS -> {
                                                            updateGroupLightsList(macIds, groupId, requestType)
                                                        }
                                                    }
                                                } else {
                                                    Timber.d("No groupId received from server")
                                                }
                                            }
                                        } else {
                                            Timber.d("Something went wrong while updating group in server")
                                            Timber.d("No response got from server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            when (requestType) {
                                                UpdateGroupType.ADD_LIGHT -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.ADD_LIGHT_TO_GROUP,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }

                                                UpdateGroupType.REMOVE_LIGHT -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.REMOVE_LIGHT_FROM_GROUP,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }

                                                UpdateGroupType.EDIT_LIGHTS -> {
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.EDIT_LIGHTS_OF_GROUP,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }

                                                UpdateGroupType.UPDATE_NAME -> {

                                                }
                                            }

                                        }
                                    } else {
                                        Timber.d("Something went wrong while updating group into server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                        when (requestType) {
                                            UpdateGroupType.ADD_LIGHT -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.ADD_LIGHT_TO_GROUP,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }

                                            UpdateGroupType.REMOVE_LIGHT -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.REMOVE_LIGHT_FROM_GROUP,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                            UpdateGroupType.EDIT_LIGHTS -> {
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_LIGHTS_OF_GROUP,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }

                                            UpdateGroupType.UPDATE_NAME -> {

                                            }
                                        }
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while adding group into server")
                                    Timber.e(error)
                                    when (requestType) {
                                        UpdateGroupType.ADD_LIGHT -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_LIGHT_TO_GROUP,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }

                                        UpdateGroupType.REMOVE_LIGHT -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.REMOVE_LIGHT_FROM_GROUP,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                        UpdateGroupType.EDIT_LIGHTS -> {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_LIGHTS_OF_GROUP,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }

                                        UpdateGroupType.UPDATE_NAME -> {

                                        }
                                    }
                                }

                            })
            )
        } else {
            when (requestType) {
                UpdateGroupType.ADD_LIGHT -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.ADD_LIGHT_TO_GROUP,
                                    EventStatus.FAILURE,
                                    NO_INTERNET_AVAILABLE,
                                    null
                            )
                    )
                }

                UpdateGroupType.REMOVE_LIGHT -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.REMOVE_LIGHT_FROM_GROUP,
                                    EventStatus.FAILURE,
                                    NO_INTERNET_AVAILABLE,
                                    null
                            )
                    )
                }

                UpdateGroupType.EDIT_LIGHTS -> {
                    responseObservable.onNext(
                            Event(
                                    EventType.EDIT_LIGHTS_OF_GROUP,
                                    EventStatus.FAILURE,
                                    NO_INTERNET_AVAILABLE,
                                    null
                            )
                    )
                }
            }
        }
    }

    override fun removeLightFromGroup(macId: String, groupId: Long) {
        Timber.d("Fetching lights of group($groupId)")
        var lightsList = getLightsOfGroup(groupId)
        var lightsListToSend = ArrayList<String>()

        for (light in lightsList) {
            lightsListToSend.add(light.macId)
        }
        if (lightsListToSend.contains(macId)) {
            lightsListToSend.remove(macId)

            Timber.d("Fetching group($groupId) name")
            var group = localDataSource.getLatestGroup(groupId)


            callUpdateGroupApi(lightsListToSend.toList(), groupId, group.name, UpdateGroupType.REMOVE_LIGHT)

        } else {
            responseObservable.onNext(
                    Event(
                            EventType.REMOVE_LIGHT_FROM_GROUP,
                            EventStatus.SUCCESS,
                            null,
                            null
                    )
            )
        }
    }

    private fun getLightOfMacId(macId: String): Light? {
        Timber.d("Fetching light lampType of device($macId)")
        return localDataSource.getLightOfMacId(macId)
    }

    private fun getLightsOfGroup(groupId: Long): List<GroupLight> {
        Timber.d("Fetching lights of group($groupId)")
        return localDataSource.getLightsOfGroup(groupId)
    }

    private fun updateGroupTag(groupId: Long) {
        Timber.d("Calculating groupTag of the group($groupId)")
        var groupLights = getLightsOfGroup(groupId)
        var brightnessTypeCount = 0
        var daylightTypeCount = 0
        var colorTypeCount = 0
        var groupTag = 0

        for (groupLight in groupLights) {
            var light = getLightOfMacId(groupLight.macId)
            when (light!!.lampType) {
                LampType.BRIGHT_AND_DIM.value -> {
                    brightnessTypeCount++
                }
                LampType.WARM_AND_COOL.value -> {
                    daylightTypeCount++
                }
                LampType.COLOR_AND_DAYLIGHT.value -> {
                    colorTypeCount++
                }
            }
        }

        if ((brightnessTypeCount > 0) && (daylightTypeCount == 0) && (colorTypeCount == 0)) {
            groupTag = GROUP_TAG_BRIGHTNESS_ONLY
        } else if ((brightnessTypeCount == 0) && (daylightTypeCount > 0) && (colorTypeCount == 0)) {
            groupTag = GROUP_TAG_DAYLIGHT_ONLY
        } else if ((brightnessTypeCount == 0) && (daylightTypeCount == 0) && (colorTypeCount > 0)) {
            groupTag = GROUP_TAG_COLOR_ONLY
        } else if ((brightnessTypeCount > 0) && (daylightTypeCount == 0) && (colorTypeCount > 0)) {
            groupTag = GROUP_TAG_BRIGHTNESS_AND_COLOR
        } else if ((brightnessTypeCount > 0) && (daylightTypeCount > 0) && (colorTypeCount == 0)) {
            groupTag = GROUP_TAG_BRIGHTNESS_AND_DAYLIGHT
        } else if ((brightnessTypeCount == 0) && (daylightTypeCount > 0) && (colorTypeCount > 0)) {
            groupTag = GROUP_TAG_DAYLIGHT_AND_COLOR
        } else if ((brightnessTypeCount > 0) && (daylightTypeCount > 0) && (colorTypeCount > 0)) {
            groupTag = GROUP_TAG_ALL
        } else {
            groupTag = GROUP_TAG_NONE
        }

        setGroupTag(groupId, groupTag)
    }

    private fun setGroupTag(groupId: Long, groupTag: Int) {
        Timber.d("Setting group groupTag($groupTag) to group($groupId)")
        localDataSource.setGroupTag(groupId, groupTag)
    }

    fun getLightsLiveData(networkId: Long, projectId: Long): LiveData<List<Light>> {
        return localDataSource.getLightsLiveData(networkId)
    }

    private fun getLightsFromServer() {
        if (isSelfProfile) {
            getPendingDevicesFromPreference()
            removeDeletedLights()
            getSelfLightsFromServer()
            communicationService.addPendingDeviceIntoServer()
        } else {
            if (profileId != null) {
                getSharedLightsFromServer()
            } else {
                Timber.d("Profile id is null, hence cannot fetch shared devices")
            }
        }
    }

    private fun removeDeletedLights() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        var networkId = sharedPreference?.getNetworkId()!!
        var projectId = sharedPreference?.getProjectId()!!

        val devicesToBeDeletedMap = sharedPreference?.getAllDevicesToBeDeleted()

        if (devicesToBeDeletedMap == null) {
            Timber.d("No devices to be deleted from local db")
            return
        }
        devicesToBeDeletedMap.forEach {
            var lightToBeDeletedMap = Gson().fromJson<HashMap<String, Any>>(it.value, pendingLightMapType)

            val lightToBeDeleted = mapper.convertValue(lightToBeDeletedMap, Light::class.java)

            if (lightToBeDeleted.networkId == networkId && lightToBeDeleted.projectId == projectId) {
                localDataSource.deleteLight(lightToBeDeleted.macId)
                Timber.d("Light(${it.key}) deleted from the local db")
            } else {
                Timber.d("Deleted light(${it.key}) belongs to different project/network")
            }
        }
    }

    private fun getSharedLightsFromServer() {
        disposables.add(
                ApiRequestParser.getSharedDevices(profileId!!, projectId.toInt(), networkId.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetDevicesResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed get shared devices api")
                            }

                            override fun onNext(response: Response<GetDevicesResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Shared Devices API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Successfully received shared devices from server")
                                            if (response.body()!!.records != null) {
                                                storeLightsIntoDb(response.body()!!.records!!)
                                                // To be done to get the latest state of devices using mosquito mqtt
//                                                subscribeToLights()

                                                // To be done to get the latest state of devices using azure mqtt
                                                getAllLightStatus(networkId)
                                            } else {
                                                Timber.d("No records for shared devices found from server")
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching shared devices from server")
                                        Timber.d("No response got for shared devices from server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching shared devices from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching shared devices from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun getSelfLightsFromServer() {
        disposables.add(
                ApiRequestParser.getDevices(userId, projectId.toInt(), networkId.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetDevicesResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed get devices api")
                            }

                            override fun onNext(response: Response<GetDevicesResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Devices API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Successfully received devices from server")
                                            if (response.body()!!.records != null) {
                                                storeLightsIntoDb(response.body()!!.records!!)

                                                // To be done to get the latest state of devices using mosquito mqtt
//                                                subscribeToLights()

                                                // To be done to get the latest state of devices using azure mqtt
                                                getAllLightStatus(networkId)

                                            } else {
                                                Timber.d("No records for devices found from server")
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching devices from server")
                                        Timber.d("No response got for devices from server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching devices from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching devices from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun getPendingDevicesFromPreference() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        val pendingDevicesMap = sharedPreference?.getAllPendingDevices()
        if (pendingDevicesMap == null) {
            Timber.d("No pending devices to add into local db")
            return
        }
        pendingDevicesMap.forEach {
            var pendingLightMap = Gson().fromJson<HashMap<String, Any>>(it.value, pendingLightMapType)

            val pendingLight = mapper.convertValue(pendingLightMap, Light::class.java)

            localDataSource.addNewLight(pendingLight)
            Timber.d("Pending light(${it.key}) is added into the local db")
        }

    }

    // For azure mqtt
    private fun getAllLightStatus(networkId: Long) {
        communicationService.getLightStatusOfNetwork(networkId)
    }

    private fun subscribeToLights() {
//        communicationService.subscribeToDevices()
    }


    private fun getGroupsFromServer() {
        if (isSelfProfile) {
            getSelfGroupsFromServer()
        } else {
            Timber.d("User is in guest mode hence no groups are fetched from server")
        }
    }

    private fun getSelfGroupsFromServer() {
        disposables.add(
                ApiRequestParser.getGroups(userId, networkId.toInt(), projectId.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetGroupsResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed get groups api")
                            }

                            override fun onNext(response: Response<GetGroupsResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Groups API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Successfully received groups from server")
                                            if (response.body()!!.records != null) {
                                                storeGroupsIntoDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("No records for groups found from server")
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching groups from server")
                                        Timber.d("No response got for groups from server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching groups from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching groups from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun storeGroupsIntoDb(records: List<GroupWithDevices>) {
        var groupsList = parseGroupsResponse(records)
        var groupLightsList = parseGroupsResponseForGroupLight(records)
        application.wifiLocalDatasource.clearGroupsOfNetwork(networkId)
        application.wifiLocalDatasource.insertGroups(groupsList)
        application.wifiLocalDatasource.insertGroupLights(groupLightsList)
        for (group in groupsList) {
            updateGroupTag(group.id!!)
        }

        /** Steps involved -
         * 1. Clear groups table. (This inturn deletes all the other tables which contains groupId as foreign key)
         * 2. Fill in the groups table
         * 3. Fill in the groupsLight table for each device in the group
         * */
    }

    private fun parseGroupsResponseForGroupLight(records: List<GroupWithDevices>): List<GroupLight> {
        var groupLightsList = ArrayList<GroupLight>()

        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        var networkId = sharedPreference?.getNetworkId()!!
        var projectId = sharedPreference?.getNetworkId()!!

        for (group in records) {
            if (group.devices != null) {
                for (deviceId in group.devices) {
                    if (sharedPreference!!.deviceExistsInToBeDeleted(deviceId, networkId, projectId).not()) {

                        var groupLightToAdd = GroupLight()
                        groupLightToAdd.setGroupId(group.group_id.toLong())
                        groupLightToAdd.setMacId(deviceId)

                        groupLightsList.add(groupLightToAdd)
                    }
                }
            }
        }

        return groupLightsList.toList()
    }

    private fun storeScenesIntoDb(records: List<GetScenesRecord>) {
        var scenesList = parseScenesResponse(records)
        var sceneLightsList = parseScenesResponseForSceneLightTable(records)

        application.wifiLocalDatasource.clearScenesOfNetwork(networkId)
        application.wifiLocalDatasource.insertScenes(scenesList)
        application.wifiLocalDatasource.insertSceneLights(sceneLightsList)
        /** Steps involved -
         * 1. For every group clear all the lights of group in GroupLight table
         * 2. Clear all the groups of network id
         * 3. For every group got from api add it into db
         * 4. For the devices of the group, add them into GroupLight table
         * */
    }

    private fun parseScenesResponse(records: List<GetScenesRecord>): List<Scene> {
        var scenesList = ArrayList<Scene>()

        for (scene in records) {
            var sceneToAdd = Scene(scene.scene_id.toLong(), scene.scene_name, scene.network_id.toLong())
            scenesList.add(sceneToAdd)
        }
        return scenesList.toList()
    }

    private fun parseScenesResponseForSceneLightTable(records: List<GetScenesRecord>): List<SceneLight> {
        var scenesList = ArrayList<SceneLight>()

        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        var networkId = sharedPreference?.getNetworkId()!!
        var projectId = sharedPreference?.getNetworkId()!!

        for (scene in records) {
            if (scene.devices != null) {
                for (sceneDevice in scene.devices) {
                    if (!sharedPreference!!.deviceExistsInToBeDeleted(sceneDevice.mac_id, networkId, projectId)) {
                        var sceneLight = SceneLight()
                        sceneLight.setSceneId(scene.scene_id.toLong())
                        sceneLight.setMacId(sceneDevice.mac_id)
                        sceneLight.setrValue(convertIntToHexString(sceneDevice.red))
                        sceneLight.setgValue(convertIntToHexString(sceneDevice.green))
                        sceneLight.setbValue(convertIntToHexString(sceneDevice.blue))
                        sceneLight.setwValue(getWValue(sceneDevice.daylight_control))
                        sceneLight.setcValue(getCValue(sceneDevice.daylight_control))
                        sceneLight.setBrightnessValue(sceneDevice.brightness)
                        sceneLight.setOnOffValue(getLampOnOffStatus(sceneDevice.status))
                        sceneLight.setSceneValue(convertIntToHexString(sceneDevice.scene_value))
                        scenesList.add(sceneLight)
                    }
                }
            }
        }
        return scenesList.toList()
    }

    private fun parseGroupsResponse(records: List<GroupWithDevices>): List<Group> {
        var groupsList = ArrayList<Group>()

        for (group in records) {
            var groupToAdd = Group(group.group_id.toLong(), group.group_name, group.network_id.toLong(), GROUP_TAG_NONE)
            groupsList.add(groupToAdd)
        }

        return groupsList.toList()
    }

    private fun getScenesFromServer() {
        if (isSelfProfile) {
            getSelfScenesFromServer()
        } else {
            Timber.d("User is in guest mode hence no scenes are fetched from server")
        }
    }

    private fun getSelfScenesFromServer() {
        disposables.add(
                ApiRequestParser.getScenes(userId, networkId.toInt(), projectId.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetScenesResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed get scenes api")
                            }

                            override fun onNext(response: Response<GetScenesResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Scenes API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Successfully received scenes from server")
                                            if (response.body()!!.records != null) {
                                                storeScenesIntoDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("No records for scenes found from server")
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching scenes from server")
                                        Timber.d("No response got for scenes from server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching scenes from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching scenes from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun storeLightsIntoDb(lights: List<Device>) {
        var lightsListToStoreInDb = parseLightsResponse(lights)
//        application.wifiLocalDatasource.clearLightsOfNetwork(networkId)
        application.wifiLocalDatasource.insertLights(lightsListToStoreInDb)
        getGroupsFromServer()
        getScenesFromServer()
    }

    private fun parseLightsResponse(devices: List<Device>): List<Light> {
        Timber.d("Parsing the get devices API response")
        var lightsList = ArrayList<Light>()

        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        var networkId = sharedPreference?.getNetworkId()!!
        var projectId = sharedPreference?.getNetworkId()!!

        for (device in devices) {
            if (!sharedPreference!!.deviceExistsInToBeDeleted(device.mac_id, networkId, projectId)) {
                var lightToAdd = Light(
                        device.mac_id,
                        device.device_name,
                        device.network_id.toLong(),
                        device.project_id.toLong(),
                        getLampType(device.device_type),
                        convertIntToHexString(device.red),
                        convertIntToHexString(device.green),
                        convertIntToHexString(device.blue),
                        getWValue(device.daylight_control),
                        getCValue(device.daylight_control),
                        SceneType.DEFAULT.value,
                        device.brightness,
                        getLampOnOffStatus(device.status),
                        device.rssi_value,
                        1,
                        device.firmware
                )

                lightsList.add(lightToAdd)
            }
        }
        return lightsList.toList()
    }

    private fun getCValue(daylight_control: Int): String {
        return convertIntToHexString(((daylight_control * 0.01) * 255).toInt())
    }

    private fun getWValue(daylight_control: Int): String {
        return convertIntToHexString((((100 - daylight_control) * 0.01) * 255).toInt())
    }

    private fun getLampOnOffStatus(status: String): Boolean {
        if (status.equals(BUNDLE_API_ON)) {
            return true
        }
        return false
    }

    private fun getLampOnOffStatus(status: Boolean): String {
        if (status) {
            return "ON"
        }
        return "OFF"
    }

    private fun getLampType(type: String): String {
        if (type.equals(BUNDLE_API_DIMMABLE)) {
            return LampType.BRIGHT_AND_DIM.value
        } else if (type.equals(BUNDLE_API_DAYLIGHT)) {
            return LampType.WARM_AND_COOL.value
        } else {
            return LampType.COLOR_AND_DAYLIGHT.value
        }
    }

    fun getGroupsLiveData(networkId: Long, projectId: Long): LiveData<List<Group>> {
        return application.wifiLocalDatasource.getGroupsLiveData(networkId)
    }

    fun getScenesLiveData(networkId: Long, projectId: Long): LiveData<List<Scene>> {
        return application.wifiLocalDatasource.getScenesLiveData(networkId)
    }

    /*  override fun serviceConnectionEstablished(service: CommunicationService) {
          this.communicationService = service
      }

      override fun serviceConnectionLost() {
          this.communicationService = null
      }*/

    private fun deleteLampFromServer(macId: String) {
        /*disposables.add(
                ApiRequestParser.deleteLamp(userId, macId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<DeleteLightResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed delete light api")
                            }

                            override fun onNext(response: Response<DeleteLightResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Delete light API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Successfully delete light from server")
                                            removeLightFromDb(macId)
                                        } else {
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.DELETE_LAMP,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("Something went wrong while deleting light from server")
                                        Timber.d("No response got from server")

                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }

                                        responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, null, null))
                                    }
                                } else {
                                    Timber.d("Something went wrong while deleting light from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }
                                    responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, null, null))
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching scenes from server")
                                Timber.e(error)
                                responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, null, null))
                            }

                        })
        )*/
    }


    fun deleteGroup(groupId: Long) {
        /**
         * Delete group -
         * 1. Delete group from server
         * 2. Delete group from local db
         *
         * */

        deleteGroupFromServer(groupId)
    }

    private fun deleteGroupFromServer(groupId: Long) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.deleteGroup(userId, networkId.toInt(), projectId.toInt(), groupId.toInt())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<DeleteGroupResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed delete group api")
                                }

                                override fun onNext(response: Response<DeleteGroupResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("Delete Group API response body - ${response.body()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully deleted group from server")
                                                deleteGroupFromDb(groupId)
                                            } else {
                                                Timber.d("Something went wrong while deleting group from server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.DELETE_GROUP,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("Something went wrong while deleting group from server")
                                            Timber.d("No response got from server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.DELETE_GROUP,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("Something went wrong while deleting group from server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                        responseObservable.onNext(Event(EventType.DELETE_GROUP, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while deleting group from server")
                                    Timber.e(error)
                                    responseObservable.onNext(Event(EventType.DELETE_GROUP, EventStatus.FAILURE, null, null))
                                }

                            })
            )
        } else {
            Timber.d("No Internet Available")
            responseObservable.onNext(Event(EventType.DELETE_GROUP, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun deleteGroupFromDb(groupId: Long) {
        application.wifiLocalDatasource.deleteGroup(groupId)
        Timber.d("Successfully deleted group from local database")
        responseObservable.onNext(Event(EventType.DELETE_GROUP, EventStatus.SUCCESS, null, null))
    }

    fun deleteScene(sceneId: Long) {
        /**
         * Delete Scene -
         * 1. Delete scene from server
         * 2. Delete scene from local db
         *
         * */

        deleteSceneFromServer(sceneId)
    }

    private fun deleteSceneFromServer(sceneId: Long) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.deleteScene(userId, networkId.toInt(), projectId.toInt(), sceneId.toInt())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<DeleteSceneResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed delete scene api")
                                }

                                override fun onNext(response: Response<DeleteSceneResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("Delete Scene API response body - ${response.body()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully deleted scene from server")
                                                deleteSceneFromDb(sceneId)
                                            } else {
                                                Timber.d("Something went wrong while deleting scene from server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.DELETE_SCENE,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("Something went wrong while deleting scene from server")
                                            Timber.d("No response got from server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.DELETE_SCENE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("Something went wrong while deleting scene from server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                        responseObservable.onNext(Event(EventType.DELETE_SCENE, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while deleting scene from server")
                                    Timber.e(error)
                                    responseObservable.onNext(Event(EventType.DELETE_SCENE, EventStatus.FAILURE, null, null))
                                }

                            })
            )
        } else {
            responseObservable.onNext(Event(EventType.DELETE_SCENE, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun deleteSceneFromDb(sceneId: Long) {
        application.wifiLocalDatasource.deleteScene(sceneId)
        Timber.d("Successfully deleted scene from local db")
        responseObservable.onNext(Event(EventType.DELETE_SCENE, EventStatus.SUCCESS, null, null))
    }

    fun isGroupContainingLights(groupId: Long): Boolean {
        return application.wifiLocalDatasource.getLightsOfGroup(groupId).isNotEmpty()
    }

    fun isSceneContainingLights(sceneId: Long): Boolean {
        return application.wifiLocalDatasource.getLightsOfScene(sceneId).isNotEmpty()
    }

    fun editLightName(macId: String, newName: String) {
        if (GeneralUtils.isInternetAvailable(application)) {
            if (isLightNameAlreadyExisting(newName).not()) {
                disposables.add(
                        ApiRequestParser.editLightName(userId, projectId.toInt(), networkId.toInt(), macId, newName)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(object : DisposableObserver<Response<EditLightNameResponse>>() {
                                    override fun onComplete() {
                                        Timber.d("Completed edit light name api")
                                    }

                                    override fun onNext(response: Response<EditLightNameResponse>) {
                                        if (response.isSuccessful) {
                                            if (response.body() != null) {
                                                Timber.d("Edit Light Name API response body - ${response.toString()}")
                                                if (response.body()!!.status.contains("success", true)) {
                                                    Timber.d("Successfully edited light name in server")
                                                    updateLightNameInDb(macId, newName)
                                                } else {
                                                    Timber.d("Something went wrong while editing light name in server")

                                                    var errorMessage = Utilities.getErrorMessage(response)
                                                    if (errorMessage != null) {
                                                        Timber.e(errorMessage)
                                                    } else {
                                                        Timber.e("No error message received")
                                                    }

                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.EDIT_LIGHT_NAME,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }
                                            } else {
                                                Timber.d("Something went wrong while editing light name in server")
                                                Timber.d("No response got from server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_LIGHT_NAME,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("Something went wrong while editing light name in server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_LIGHT_NAME,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    }

                                    override fun onError(error: Throwable) {
                                        Timber.d("Something went wrong while editing light name in server")
                                        error.printStackTrace()
                                        Timber.e(error)

                                        responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.FAILURE, null, null))
                                    }

                                })
                )
            } else {
                Timber.d("Light by name($newName) already exists")
                responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.FAILURE, ALREADY_EXISTS, null))
            }
        } else {
            Timber.d("No Internet available")
            responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun isLightNameAlreadyExisting(newName: String): Boolean {
        var lightsList = localDataSource.getLights(networkId)
        for (light in lightsList) {
            if (light.name.equals(newName)) {
                return true
            }
        }
        return false
    }

    private fun updateLightNameInDb(macId: String, newName: String) {
        localDataSource.updateLightName(macId, newName)
        Timber.d("Successfully edited light name in db")
        responseObservable.onNext(Event(EventType.EDIT_LIGHT_NAME, EventStatus.SUCCESS, null, null))
    }

    fun deleteLamp(macId: String) {
        /**
         * An active internet is needed to update server
         *
         * Delete lamp -
         * 1. Send RESET command to device via TCP
         * 2. Call delete API
         * 3. On Success of the API remove the lamp from local database
         *
         * */
        if (application.wifiCommunicationService.isDeviceLocallyAvailable(macId).not()) {
            responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, Constants.NO_TCP_CONNECTION, null))
        } else if (GeneralUtils.isInternetAvailable(application).not()) {
            responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        } else {
            sendResetCommandToDevice(macId)
        }
    }

    fun deleteUnSyncedLamp(macId: String) {
        /**
         *
         * No need of active internet as the lamp is not present in server
         * No need to Call delete API as device is not synced
         *
         * Delete lamp -
         * 1. Send RESET command to device via TCP
         * 2. On Success of the API remove the lamp from local database
         *
         * */
        if (application.wifiCommunicationService.isDeviceLocallyAvailable(macId).not()) {
            responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, Constants.NO_TCP_CONNECTION, null))
        } else {
            sendResetCommandToDevice(macId)
        }
    }


    private fun sendResetCommandToDevice(macId: String) {
        var command = Command(EventType.DELETE_LAMP, macId, null)
        communicationService.sendCommand(command)
    }


    fun getAllLightsOfNetwork(networkId: Long): List<CustomLight> {
        var lightList = localDataSource.getLights(networkId)
        var customLightList = ArrayList<CustomLight>()
        for (light in lightList) {
            var customLight = CustomLight(light)
            customLight.setLocallyAvailable(communicationService!!.isDeviceLocallyAvailable(light.macId))
            customLightList.add(customLight)
        }
        return customLightList
    }

    fun getSceneLightsOfScene(sceneId: Long): List<SceneLight> {
        return localDataSource.getLightsOfScene(sceneId)
    }

}