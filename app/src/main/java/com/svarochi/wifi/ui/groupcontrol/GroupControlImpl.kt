package com.svarochi.wifi.ui.groupcontrol

import com.svarochi.wifi.model.communication.SceneType

interface GroupControlImpl {

    fun turnOnGroup(groupId: Long)

    fun turnOffGroup(groupId: Long)

    fun setGroupScene(groupId: Long, sceneType: SceneType)

    fun setGroupDaylight(
            groupId: Long,
            daylightValue: Int
    )

    fun setGroupBrightness(
            groupId: Long,
            brightnessValue: Int
    )

    fun setGroupColor(
            groupId: Long,
            r_value: String,
            g_value: String,
            b_value: String
    )

    fun addLightToGroup(macId: String, groupId: Long)

    fun removeLightFromGroup(macId: String, groupId: Long)

    fun editGroupName(groupId: Long, name: String)
}