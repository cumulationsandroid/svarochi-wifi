package com.svarochi.wifi.ui.room.view.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.os.Bundle
import android.provider.Settings.ACTION_WIFI_SETTINGS
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.ServiceConnectionCallback
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS_WITH_ANOTHER_USER
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_LIGHT_MACID
import com.svarochi.wifi.common.Constants.Companion.CONNECTING_TO_SVAROCHI_TCP
import com.svarochi.wifi.common.Constants.Companion.INITIAL
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.lightcontrol.view.activity.LightControlActivity
import com.svarochi.wifi.ui.room.view.activity.RoomActivity
import com.svarochi.wifi.ui.room.view.adapter.LightsAdapter
import kotlinx.android.synthetic.main.fragment_ble_lights.*
import timber.log.Timber


class BleLightsFragment : androidx.fragment.app.Fragment(), View.OnClickListener, ServiceConnectionCallback {
    override fun serviceConnected() {
        initObservers()
        initViews()
        initClickListeners()
    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                fab_configureNewDevice.id -> {
                    configureNewDevice()
                }
            }
        } else {
            Timber.d("$TAG Clicked view is null")
        }
    }

    private var isSelfProfile: Boolean = true
    private var wifiManager: WifiManager? = null
    private lateinit var connectivityManager: ConnectivityManager
    private var activityContext: RoomActivity? = null
    private var requestingMacId: String = ""
    private var isReceiverRegistered: Boolean = false
    private var networkId: Long = 0
    private var projectId: Long = 0
    private val OPEN_SETTINGS: Int = 1
    private var currentWifiScanCount: Int = 0
    private var isObservingLightsTable = false
    private var TAG: String = "BleLightsFragment --->"
    private var OPEN_LAMP_SETTINGS = 3
    private var isLightNameDialogShowing = false

    var devicesList = ArrayList<Any>()

    private var isInit = true

    /*private var wifiReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            var resultList = wifiManager!!.scanResults
            var addedToList = false
            Timber.d("Got Wifi scan results")
            for (result in resultList) {
                //Timber.d(result.SSID + " - " + result.capabilities)
                if (result.SSID.contains(SVAROCHI_LAMP_IDENTIFIER, true)) {
                    if (doesListContainSsid(result.SSID).not()) {
                        addedToList = true
                        devicesList.add(CustomSsid(result.SSID))
                    }
                }
            }
            if (addedToList)
                updateLightsRv()
            scanWifi()
        }
    }*/

    /*private fun doesListContainSsid(ssid: String?): Boolean {
        var isContaining = false
        for (device in devicesList) {
            if (device is CustomSsid) {
                if (device.ssid.equals(ssid)) {
                    isContaining = true
                    return isContaining
                }
            } else if (device is CustomLight) {
                if (device.light.name.contains(ssid!!, true)) {
                    isContaining = true
                    return isContaining
                }
            }
        }
        return isContaining
    }*/

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_ble_lights, container, false)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activityContext = context as RoomActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activityContext != null) {
            activityContext!!.requestToBindService(this)
        } else {
            if (activity != null) {
                (activity as RoomActivity).requestToBindService(this)
            }
        }
    }


    private fun initClickListeners() {
        fab_configureNewDevice.setOnClickListener(this)
    }

    private fun initViews() {
        if (activityContext != null) {
            if (activityContext!!.network == null) {
                Timber.d("BleFragment Activity's network variable is null")
                activityContext!!.initBundleData()
            }
            networkId = activityContext!!.network!!.id
            projectId = activityContext!!.network!!.projectId
            var preference = SharedPreference(activityContext!!)
            isSelfProfile = preference.getCurrentProfile().equals(Constants.CURRENT_PROFILE_SELF)
            wifiManager = activityContext!!.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            connectivityManager = activity!!.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            if (isSelfProfile.not()) {
                fab_configureNewDevice.visibility = View.GONE
            } else {
                fab_configureNewDevice.visibility = View.VISIBLE
            }

            initLightsRv()
            observeLightsTable()
        } else {
            Timber.d("Activity context is null")
        }
    }


    private fun observeLightsTable() {
        if (activityContext != null) {
            if (activityContext!!.viewModel == null) {
                Timber.d("BleFragment Activity's viewmodel is null")
                activityContext!!.initViewModel()
            }

            if (isObservingLightsTable) {
                activityContext!!.viewModel!!.getLightsLiveData(networkId, projectId).removeObservers(this)
            }
            isObservingLightsTable = true
            isInit = true
            Timber.d("Observing all the lights of network($networkId)")
            activityContext!!.viewModel!!.getLightsLiveData(networkId, projectId).observe(this, Observer<List<Light>> {
                if (it != null) {
                    Timber.d("Change in lights of network($networkId), lights size = ${it.size}")
                    activityContext!!.lightsCount = it.size
                    updateLightsRv(it)
                } else {
                    Timber.d("No lights got for network($networkId)")
                }
            })
        } else {
            Timber.d("BleFragment Activity Context is null")
        }
    }

    private fun initLightsRv() {
        rv_devicesRv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activityContext!!)
        rv_devicesRv.adapter = LightsAdapter(devicesList, isSelfProfile, this)
    }

    private fun initObservers() {
        if (activityContext != null) {
            if (activityContext!!.viewModel != null) {
                activityContext!!.viewModel!!.getResponse().observe(this, Observer { t -> processResponse(t) })
            } else {
                Timber.d("View model is not initialised")
            }
        } else {
            Timber.d("Activity context is not initialised")
        }
    }

    private fun processResponse(event: Event?) {
        when (event!!.type) {
            EventType.CONNECTION -> {
                when (event.status) {
                    EventStatus.FAILURE -> {
                        if (event.data != null && event.data is String) {
                            var macId = event.data as String
                            showPopup(false, null)
                            //activityContext!!.showToast(lightName + getString(R.string.toast_is_not_reachable))
                            Timber.d("Connection lost with device - ${macId}")
//                            observeLightsTable()
                            if (Constants.LOCAL_AVAILABLE) {
                                setLocallyAvailable(macId, false)
                            }
                            if (activityContext!!.shouldWriteWifiCredentials) {
                                Timber.d("DEVICE SETUP STATE - Failed to write wifi credentials")
                                setCurrentState(INITIAL)
                                activityContext!!.shouldWriteWifiCredentials = false
                            }
                        }
                    }
                    EventStatus.SUCCESS -> {
                        if (event.data != null && event.data is String) {
                            var macId = event.data as String
//                            showPopup(false, null)
                            //activityContext!!.showToast(lightName + getString(R.string.toast_is_not_reachable))
                            Timber.d("Connection success with device - ${macId}")
//                            observeLightsTable()
                            if (Constants.LOCAL_AVAILABLE) {
                                setLocallyAvailable(macId, true)
                            }
                        }
                    }
                }
            }

            EventType.SET_WIFI -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        showPopup(true, getString(R.string.label_setting_wifi_credentials))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully written wifi credentials")
                        activityContext!!.shouldWriteWifiCredentials = false
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to set wifi to device")
                        showPopup(false, null)
                        activityContext!!.shouldWriteWifiCredentials = false
                        activityContext!!.showToast(getString(R.string.toast_failed_to_set_wifi_credentials))
                        /*Toast.makeText(
                                activityContext!!,
                                getString(R.string.toast_failed_to_set_wifi_credentials),
                                Toast.LENGTH_SHORT
                        ).show()*/
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to set wifi to device")
                        showPopup(false, null)
                        activityContext!!.shouldWriteWifiCredentials = false
                        activityContext!!.showToast(getString(R.string.toast_failed_to_set_wifi_credentials))
                        /*Toast.makeText(
                                activityContext!!,
                                getString(R.string.toast_failed_to_set_wifi_credentials),
                                Toast.LENGTH_SHORT
                        ).show()*/
                    }
                }
            }

            EventType.ADD_NEW_LIGHT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        showPopup(true, getString(R.string.label_adding_device_to_server))
                    }
                    EventStatus.SUCCESS -> {
                        showPopup(false, null)
                        if (event.data != null) {
                            if (event.data is String) {
                                activityContext!!.showToast(
                                        (event.data as String) +
                                                getString(R.string._space) +
                                                getString(R.string.toast_is_successfully_configured)
                                )
                            }
                        }
                        Timber.d("DEVICE SETUP STATE - Successfully written wifi credentials")
                        setCurrentState(INITIAL)
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to add device to server")
                        showPopup(false, null)
                        observeLightsTable()
                        activityContext!!.showToast(getString(R.string.toast_failed_to_add_device_to_server))

                        /*Toast.makeText(
                                activityContext!!,
                                getString(R.string.toast_failed_to_add_device_to_server),
                                Toast.LENGTH_SHORT
                        ).show()*/

                        if (event.data != null) {
                            if (event.data is Int) {
                                if ((event.data as Int) == ALREADY_EXISTS_WITH_ANOTHER_USER) {
                                    /*Toast.makeText(
                                            activityContext!!,
                                            getString(R.string.toast_device_already_exists_with_another_user),
                                            Toast.LENGTH_SHORT
                                    ).show()*/
                                    showAlreadyExistsPopup()
                                }
                            }
                        }

                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to add device to server")
                        showPopup(false, null)
                        observeLightsTable()
                        activityContext!!.showToast(getString(R.string.toast_failed_to_add_device_to_server))
                        /*Toast.makeText(
                                activityContext!!,
                                getString(R.string.toast_failed_to_add_device_to_server),
                                Toast.LENGTH_SHORT
                        ).show()*/
                    }
                }
            }


            EventType.TURN_ON_LIGHT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning on light")
                        //showPopup(true, getString(R.string.popup_turning_on_light))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully turned on light- $requestingMacId")
                        showPopup(false, null)
                        /*updateLightList(
                            requestingMacId,
                            event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light
                        )*/
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                        Timber.d("Failed to turn on light- $requestingMacId")
                        showPopup(false, null)
                        //activityContext!!.showToast(getString(R.string.toast_failed_to_turn_on_light))
                        observeLightsTable()
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn on light- $requestingMacId")
                        showPopup(false, null)
                        //activityContext!!.showToast(getString(R.string.toast_failed_to_turn_on_light))
                        observeLightsTable()
                    }
                }
            }
            EventType.TURN_OFF_LIGHT -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Turning off light - $requestingMacId")
                        // showPopup( true, getString(R.string.popup_turning_off_light))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully turned off light- $requestingMacId")
                        showPopup(false, null)
                        /*updateLightList(
                            requestingMacId,
                            event.data!! as wifi.svarochi.com.wifisvarochi.database.entity.Light
                        )*/
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
                        Timber.d("Failed to turnoff light- $requestingMacId")
                        showPopup(false, null)
                        //activityContext!!.showToast(getString(R.string.toast_failed_to_turn_off_light))
                        /*updateLightsRv()*/
                        observeLightsTable()
                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to turn off light - $requestingMacId")
                        showPopup(false, null)
                        //activityContext!!.showToast(getString(R.string.toast_failed_to_turn_off_light))
                        //updateLightsRv()
                        observeLightsTable()
                    }
                }
            }
            EventType.GET_LIGHT -> {
                when (event.status) {
                    EventStatus.SUCCESS -> {
                        if ((event.data is Light)) {
                            var customLight = CustomLight(event.data as Light)
                            addDeviceToList(customLight)
                        } else {
                            Timber.e("Device is configured to wifi but not saved in local db")
                        }
                    }
                }
            }
            EventType.GET_LIGHTS -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Fetching lights from local db of network($networkId)")
                        //activityContext!!.viewModel.getMyLights(networkId)
                    }

                    EventStatus.SUCCESS -> {
                        var lightsList = event.data as ArrayList<CustomLight>
                        Timber.d("Got ${lightsList.size} lights of network($networkId)")
                        activityContext!!.lightsCount = lightsList.size

                        if (lightsList.isNotEmpty()) {
//                            activityContext!!.shouldBroadcast(true)
                        }

                        devicesList.clear()
                        devicesList.addAll(lightsList)
                        updateLightsRv()
                    }
                }
            }

            EventType.UDP_BROADCAST -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                    }
                    EventStatus.SUCCESS -> {
                        var macId = (event.data!! as String)

                        setLightAsLocallyAvailable(macId, true)
                    }

                    EventStatus.FAILURE -> {
                        var macId = (event.data!! as String)

                        setLightAsLocallyAvailable(macId, false)
                    }
                    EventStatus.ERROR -> {
                        Timber.e(event.error)
//                        activityContext!!.shouldBroadcast(true)
                        //activityContext!!.showToast(getString(R.string.toast_device_not_reachable))
                    }
                }
            }

            EventType.EDIT_LIGHT_NAME -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        if (activityContext != null) {
                            activityContext!!.showLoadingPopup(true, getString(R.string.popup_editing_light_name))
                        }
                    }
                    EventStatus.SUCCESS->{
                        if (activityContext != null) {
                            activityContext!!.showLoadingPopup(false, null)
                        }
                    }

                    EventStatus.FAILURE -> {
                        Timber.d("Failed to edit light name")
                        if (event.data != null && event.data is Int && (event.data as Int) == ALREADY_EXISTS) {
                            activityContext!!.showToast(getString(R.string.toast_light_by_that_name_already_exists))
                            return
                        }
                        if (activityContext != null) {
                            activityContext!!.showToast(getString(R.string.failed_to_edit_light_name))
                        }
                    }
                    EventStatus.ERROR -> {
                        Timber.d("Failed to edit light name")
                        if (activityContext != null) {
                            activityContext!!.showToast(getString(R.string.failed_to_edit_light_name))
                        }
                    }
                }
            }
            EventType.DELETE_LAMP -> {
                when (event.status) {
                    EventStatus.LOADING -> {
                        Timber.d("Deleting light")
                        activityContext?.showLoadingPopup(true, getString(R.string.dialog_deleting_light))
                    }
                    EventStatus.SUCCESS -> {
                        Timber.d("Successfully deleted the light")
                        activityContext?.showLoadingPopup(false, getString(R.string.dialog_deleting_light))
                        if (activityContext != null && activityContext!!.network != null) {
                            activityContext!!.viewModel!!.getLights(activityContext!!.network!!.id)
                        }

                    }
                    EventStatus.FAILURE -> {
                        Timber.d("Failed to delete light")
                        activityContext?.showLoadingPopup(false, null)

                        if (event.data != null) {
                            if (event.data is Int) {
                                if (event.data as Int == Constants.NO_INTERNET_AVAILABLE) {
                                    Timber.d("No internet available")
                                    activityContext?.showToast(getString(R.string.no_internet))
                                }

                                if (event.data as Int == Constants.NO_TCP_CONNECTION) {
                                    Timber.d("No Tco connection with device")
                                    activityContext?.showToast(getString(R.string.toast_device_not_reachable))
                                }
                            }
                        } else {
                            activityContext?.showToast(getString(R.string.toast_failed_to_delete_the_lamp))
                        }
                    }
                }
            }
        }
    }

    private fun showAlreadyExistsPopup() {
        if (activityContext != null && activityContext!!.dialog != null) {
            activityContext!!.dialog!!.setContentView(R.layout.dialog_lamp_already_exists)
            var okButton = activityContext!!.dialog!!.findViewById<Button>(R.id.bt_okButton)

            okButton.setOnClickListener {
                activityContext!!.dialog!!.dismiss()
            }

            activityContext!!.dialog!!.show()
        } else {
            Timber.d("Activity context is null")
        }
    }

    private fun setLocallyAvailable(macId: String, flag: Boolean) {
        for (device in devicesList) {
            if (device is CustomLight) {
                if (device.light.macId.equals(macId)) {
                    device.setLocallyAvailable(flag)
                    break
                }
            }
        }
    }

    /*private fun refreshPage() {
        //getLights()
        if (activityContext != null) {
            activityContext!!.runOnUiThread {
                var ssidListToRemove = ArrayList<CustomSsid>()
                for (device in devicesList) {
                    if (device is CustomSsid) {
                        ssidListToRemove.add(device)
                    }
                }

                devicesList.removeAll(ssidListToRemove)

                rv_devicesRv.adapter!!.notifyDataSetChanged()
                currentWifiScanCount = 0
                scanWifi()

                activityContext!!.shouldBroadcast(true)
            }
        }
    }*/

    private fun updateLightsRv() {
        rv_devicesRv.adapter!!.notifyDataSetChanged()
    }

    private fun updateLightsRv(lightsList: List<Light>) {
        for (light in lightsList) {
            var deviceExists = false
            for (device in devicesList) {
                if (device is CustomLight) {
                    if (device.light.macId.equals(light.macId)) {
                        deviceExists = true
                        device.light = light
                        if (isInit && Constants.LOCAL_AVAILABLE) {
                            device.setLocallyAvailable(isDeviceLocallyAvailable(light.macId))
                        }
                    }
                }
            }
            if (!deviceExists) {
                var newLight = CustomLight(light)
                if (isInit && Constants.LOCAL_AVAILABLE) {
                    newLight.setLocallyAvailable(isDeviceLocallyAvailable(light.macId))
                }
                devicesList.add(newLight)
            }
        }
        if(lightsList.isNotEmpty()) {
            isInit = false
        }
        rv_devicesRv.adapter!!.notifyDataSetChanged()
    }

    private fun isDeviceLocallyAvailable(macId: String): Boolean {
        if (activityContext != null) {
            return (activityContext!!.application as SvarochiApplication).wifiCommunicationService.isDeviceLocallyAvailable(macId)
        }
        return false
    }

    private fun addDeviceToList(customLight: CustomLight) {
        var contains = false
        var position = -1
        for (device in devicesList) {
            position++
            if (device is CustomLight) {
                if (device.light.macId.equals(customLight.light.macId)) {
                    contains = true
                    break
                }
            }
        }

        if (!contains) {
            customLight.setLocallyAvailable(true)
            devicesList.add(customLight)
        } else {
            (devicesList[position] as CustomLight).setLocallyAvailable(true)
        }

        if (rv_devicesRv.adapter == null) {
            rv_devicesRv.adapter = LightsAdapter(devicesList, isSelfProfile, this)
        } else {
            (rv_devicesRv.adapter as LightsAdapter).updateList(devicesList)
        }
    }


    private fun updateLightList(macId: String, light: Light) {
        (rv_devicesRv.adapter as LightsAdapter).updateLight(macId, light)
    }

    private fun setLightAsLocallyAvailable(macId: String, flag: Boolean) {
        (rv_devicesRv.adapter as LightsAdapter).setLocallyAvailable(macId, flag)
    }

    /*private fun registerReceiver() {
        if (isReceiverRegistered.not() && activityContext != null && wifiManager != null) {
            Timber.d("Registered wifi receiver")
            isReceiverRegistered = true
            activity!!.registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        }
    }

    private fun unRegisterReceiver() {
        if (isReceiverRegistered) {
            Timber.d("Unregistered wifi receiver")
            isReceiverRegistered = false
            activity!!.unregisterReceiver(wifiReceiver)
        }
    }*/

/*    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if (isVisibleToUser) {
        } else {
            unRegisterReceiver()
        }
    }

    override fun onPause() {
        super.onPause()
        if (isReceiverRegistered)
            unRegisterReceiver()
    }*/

    private fun showPopup(show: Boolean, message: String?) {
        if (activityContext != null) {
            activityContext!!.showLoadingPopup(show, message)
        } else {
            Timber.d("No activitycontext")
        }
    }

    /*private fun scanWifi() {
        if (activityContext != null) {
            if (activityContext!!.isSelfProfile) {
                Timber.d("Scanning for wifi")
                registerReceiver()
                if (wifiManager!!.isWifiEnabled.not()) {
                    wifiManager!!.isWifiEnabled = true
                }
                if (wifiManager != null && (currentWifiScanCount < MAX_WIFI_SCAN_COUNT)) {
                    currentWifiScanCount++
                    wifiManager!!.startScan()
                }
            } else {
                Timber.d("User is in guest mode hence not scanning for new devices")
            }
        } else {
            Timber.d("Activity context is null")
        }
    }*/

    /*private fun connectToWifi(ssid: String) {
        setCurrentState(CONNECTING_TO_SVAROCHI_WIFI)
        activityContext!!.shouldWriteWifiCredentials = true
        activityContext!!.pendingSsid = ssid


        if (wifiManager!!.connectionInfo != null &&
                wifiManager!!.connectionInfo.ssid != null &&
                wifiManager!!.connectionInfo.ssid.contains(ssid, true).not()
        ) {
            showPopup(true, getString(R.string.dialog_connecting_to_) + ssid)
            Timber.d("Connecting to wifi - $ssid")

            *//*var config = getExistingConfiguration(ssid)
            var networkId = 0

            if (wifiManager!!.isWifiEnabled.not()) {
                wifiManager!!.isWifiEnabled = true
            }

            if (config != null) {
                networkId = config.networkId
                if (wifiManager!!.removeNetwork(networkId)) {
                    Timber.d("Successfully removed the saved wifi config - ${config.SSID}")
                    wifiManager!!.saveConfiguration()
                    config = null
                } else {
                    Timber.d("Failed to remove config - ${config.SSID}")
                }
            } else {
                Timber.d("Config not saved - ${ssid}")
            }

            if (config == null) {
                Timber.d("New wifi config is created with ssid - $ssid")
                config = createWifiConfuguration(ssid)
                networkId = wifiManager!!.addNetwork(config)
            }

            Timber.d("Network id - $networkId")
            if (networkId != -1) {
                wifiManager!!.disconnect()
                var enableNetworkResult = wifiManager!!.enableNetwork(networkId, true)
                if (enableNetworkResult) {
                    Timber.d("Successfully enabled wifi config - ${config!!.SSID} and reconnect initialised")
                } else {
                    Timber.d("Failed to enable wifi config - ${config!!.SSID}")
                }
                wifiManager!!.reconnect()
            } else {
                Timber.d("Failed to connect to $ssid")
                showPopup(false, null)
                //activityContext!!.showToast(getString(R.string.toast_could_not_connect_to_device_hotspot))
                phoneFailedToConnectToHotspot(ssid)
            }*//*

            startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
        } else {
            writeWifiCredentials(ssid)
        }
    }*/

    private fun createWifiConfuguration(ssid: String): WifiConfiguration? {
        var config = WifiConfiguration()

        config.SSID = "\"" + ssid + "\""
        config.status = WifiConfiguration.Status.ENABLED
        config.priority = 10000

        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
        config.allowedProtocols.set(WifiConfiguration.Protocol.RSN)
        config.allowedProtocols.set(WifiConfiguration.Protocol.WPA)
        config.allowedAuthAlgorithms.clear()
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP)
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP)
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40)
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104)
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP)
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP)

        return config
    }

    private fun getExistingConfiguration(ssid: String): WifiConfiguration? {
        val configuredNetworks = wifiManager!!.configuredNetworks
        if (configuredNetworks != null) {
            for (existingConfig in configuredNetworks) {
                if (existingConfig.SSID.equals(ssid)) {
                    return existingConfig
                }
            }
        }
        return null
    }

    private fun writeWifiCredentials(name: String, lampSsid:String) {
        showPopup(false, null)
        setCurrentState(CONNECTING_TO_SVAROCHI_TCP)
        if (activityContext != null) {
            if (activityContext!!.viewModel != null) {
                if (activityContext!!.network != null) {
                    activityContext!!.viewModel!!.setWifiCredentials(
                            name,
                            lampSsid,
                            activityContext!!.network!!.wifiSsid,
                            activityContext!!.network!!.wifiPassword,
                            activityContext!!.network!!.wifiAuthenticationMode,
                            activityContext!!.network!!.wifiEncryptionType
                    )
                } else {
                    Timber.d("Activity context network is null")
                }
            } else {
                Timber.d("View model is not initialised")
            }
        } else {
            Timber.d("Activity context is not initialised")
        }
    }

    private fun configureNewDevice() {
        if (activityContext != null) {
            activityContext!!.configureNewDevice()

        } else {
            Timber.d("$TAG Failed to configure new device activity context is null")
        }
    }

    fun configureDevice(ssid: String) {
//        connectToWifi(ssid)
    }

    fun turnOnLight(macId: String) {
        requestingMacId = macId
        if (activityContext != null) {
            if (activityContext!!.viewModel != null) {
                activityContext!!.viewModel!!.turnOnLight(macId)
            } else {
                Timber.d("View model is not initialised")
            }
        } else {
            Timber.d("Activity context is not initialised")
        }

    }

    fun turnOffLight(macId: String) {
        requestingMacId = macId
        if (activityContext != null) {
            if (activityContext!!.viewModel != null) {
                activityContext!!.viewModel!!.turnOffLight(macId)
            } else {
                Timber.d("View model is not initialised")
            }
        } else {
            Timber.d("Activity context is not initialised")
        }
    }

    fun openLampControlScreen(light: CustomLight) {
        startActivityForResult(
                Intent(activityContext!!, LightControlActivity::class.java)
                        .putExtra(BUNDLE_LIGHT_MACID, light.light.macId), OPEN_LAMP_SETTINGS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            /*LIGHT_CONTROL -> {
                if (resultCode == RESULT_OK) {
                    updateLightList(
                        (data!!.getSerializableExtra(BUNDLE_LIGHT) as CustomLight).light.macId,
                        (data.getSerializableExtra(BUNDLE_LIGHT) as CustomLight).light
                    )
                }
            }*/
           /* OPEN_SETTINGS -> {
                activityContext!!.dialog!!.dismiss()
                if (wifiManager != null && wifiManager!!.connectionInfo != null && wifiManager!!.connectionInfo!!.ssid != null) {
                    if (wifiManager?.connectionInfo?.ssid!!.contains(
                                    Constants.SVAROCHI_LAMP_IDENTIFIER,
                                    false
                            )
                    ) {
                        showLightNameDialog(wifiManager!!.connectionInfo.ssid)
                    }
                }
            }*/

            OPEN_LAMP_SETTINGS -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (activityContext != null && activityContext!!.network != null) {
                        activityContext!!.viewModel!!.getLights(activityContext!!.network!!.id)
                    }
                }
            }
        }
    }

    fun phoneFailedToConnectToHotspot() {
        Timber.d("DEVICE SETUP STATE - Failed to connect to svarochi hotspot")
        setCurrentState(INITIAL)
        showPopup(false, null)
        showConnectToDeviceHotspotDialog()
    }

    private fun showConnectToDeviceHotspotDialog() {
        activityContext!!.runOnUiThread {
            activityContext!!.dialog!!.setContentView(R.layout.dialog_connect_to_hotspot)
            var message = activityContext!!.dialog!!.findViewById<TextView>(R.id.tv_message)
            var cancelButton = activityContext!!.dialog!!.findViewById<Button>(R.id.bt_cancelButton)
            var settingsButton = activityContext!!.dialog!!.findViewById<Button>(R.id.bt_settingsButton)

            message.text =
                    getString(R.string.label_phone_could_not_connect_to_device_hotspot) + getString(R.string.label_connect_to_device_hotspot_in_the_settings_screen)

            cancelButton.setOnClickListener {
                activityContext!!.shouldWriteWifiCredentials = false
                activityContext!!.dialog!!.dismiss()
            }

            settingsButton.setOnClickListener {
                activityContext!!.dialog!!.dismiss()
//                openSettingsScreen()
                activityContext?.configureNewDevice()
            }

            activityContext!!.dialog!!.show()
        }

    }

//    private fun openSettingsScreen() {
//        startActivityForResult(Intent(ACTION_WIFI_SETTINGS), OPEN_SETTINGS)
//    }

    private fun setCurrentState(value: String) {
        activityContext!!.DEVICE_SETUP_STATE = value
        Timber.d("DEVICE SETUP STATE - ${activityContext!!.DEVICE_SETUP_STATE}")
    }

    fun showLightNameDialog(ssid: String) {
        if (activityContext != null && activityContext!!.dialog != null && isLightNameDialogShowing.not()) {
            activityContext!!.runOnUiThread {
                isLightNameDialogShowing = true

                activityContext!!.dialog!!.setContentView(R.layout.dialog_enter_lamp_name)
                var okButton = activityContext!!.dialog!!.findViewById<Button>(R.id.bt_ok)
                var cancelButton = activityContext!!.dialog!!.findViewById<Button>(R.id.bt_cancelButton)
                var nameField = activityContext!!.dialog!!.findViewById<EditText>(R.id.et_lightName)
                var nameText = activityContext!!.dialog!!.findViewById<TextView>(R.id.tv_enterNameText)
                activityContext!!.dialog!!.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                var formatedSsid = ssid.replace("\"", "")
                formatedSsid = formatedSsid.replace("\"", "")

                nameText.text = nameText.text.toString() + getString(R.string._space) + formatedSsid


                okButton.setOnClickListener {
                    if (nameField.text.trim().isEmpty()) {
                        activityContext!!.showToast(getString(R.string.toast_please_enter_light_name))
                    } else {
                        activityContext!!.dialog!!.dismiss()
                        isLightNameDialogShowing = false
                        val name = Utilities.capitalizeWords(nameField.text.toString())
                        writeWifiCredentials(name, ssid)
                    }
                }

                cancelButton.setOnClickListener {
                    activityContext!!.dialog!!.dismiss()
                    isLightNameDialogShowing = false
                }
                activityContext!!.dialog!!.show()
            }
        } else {
            Timber.d("Activity context is null")
            isLightNameDialogShowing = false
        }
    }

    fun showRenameLightDialog(light: Light) {
        if (activityContext != null) {
            if (activityContext!!.dialog == null) {
                activityContext!!.initDialogs()
            }

            if (light.isSynced == Constants.LIGHT_SYNCED) {
                activityContext!!.dialog!!.setContentView(R.layout.dialog_edit_light_name)
                activityContext!!.dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

                val cancelButton: Button = activityContext!!.dialog!!.findViewById(R.id.bt_cancelButton)
                val okButton: Button = activityContext!!.dialog!!.findViewById(R.id.bt_okButton)
                val nameField: EditText = activityContext!!.dialog!!.findViewById(R.id.et_name)
                nameField.text = Editable.Factory.getInstance().newEditable(light!!.name)
                nameField.requestFocus()

                activityContext!!.dialog!!.show()
                okButton.setOnClickListener {
                    if (nameField.text.trim().isNotEmpty()) {
                        activityContext!!.dialog!!.dismiss()
                        editLightName(light!!.macId!!, nameField.text.toString())
                    } else {
                        Timber.d("Edit light name - No name entered")
                        activityContext!!.showToast(getString(R.string.toast_name_cannot_be_empty))
                    }
                }

                cancelButton.setOnClickListener {
                    activityContext!!.dialog!!.dismiss()
                }
            } else {
                activityContext!!.showToast(getString(R.string.toast_light_unsynced_light_name))
            }
        } else {
            // Should not occur
            Timber.d("Light value not initialised")
        }
    }

    private fun editLightName(macId: String, name: String) {
        if (activityContext != null) {
            if (activityContext!!.viewModel == null) {
                activityContext!!.initViewModel()
            }
            activityContext!!.viewModel!!.editLightName(macId, name)
        }
    }

    fun openDeleteLampDialog(light: Light) {
        if (activityContext != null) {
            if (activityContext!!.dialog == null) {
                activityContext!!.initDialogs()
            }


            activityContext!!.dialog!!.setContentView(R.layout.dialog_delete_lamp_confirmation)
            activityContext!!.dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            var cancelButton = activityContext!!.dialog!!.findViewById<Button>(R.id.bt_cancelButton)
            var okButton = activityContext!!.dialog!!.findViewById<Button>(R.id.bt_okButton)
            activityContext!!.dialog!!.show()

            cancelButton.setOnClickListener {
                activityContext!!.dialog!!.dismiss()
            }

            okButton.setOnClickListener {
                activityContext!!.dialog!!.dismiss()
                if (light.isSynced == Constants.LIGHT_SYNCED) {
                    deleteLamp(light.macId)
                } else {
                    deleteUnsyncedLamp(light.macId)
                }
            }

        }
    }

    private fun deleteLamp(macId: String) {
        Timber.d("Confirmed to delete $macId")
        if (activityContext != null) {
            if (activityContext!!.viewModel == null) {
                activityContext!!.initViewModel()
            }
            activityContext!!.viewModel!!.deleteLamp(macId)
        }
    }

    private fun deleteUnsyncedLamp(macId: String) {
        Timber.d("Confirmed to delete $macId")
        if (activityContext != null) {
            if (activityContext!!.viewModel == null) {
                activityContext!!.initViewModel()
            }
            activityContext!!.viewModel!!.deleteUnsyncedLamp(macId)
        }
    }
}
