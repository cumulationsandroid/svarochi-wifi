package com.svarochi.wifi.ui.setwifidetails.repository

import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.setwifidetails.SetWifiDetailsImpl
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class SetWifiDetailsRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl, SetWifiDetailsImpl {

    private var preference = SharedPreference(application)

    override fun initCommunicationResponseObserver() {
        // No implementations as this isn't using service
    }

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    private val responseObservable = PublishSubject.create<Event>()
    private var disposables = CompositeDisposable()

    override fun getCommunicationResponseObserver() = responseObservable

    override fun getWifiDetails() {
        Timber.d("Getting wifi details")
        var map = HashMap<String, String>()
/*
        if (preference.getSsid().isEmpty()) {
            responseObservable.onNext(Event(EventType.GET_WIFI_DETAILS, EventStatus.SUCCESS, null, null))
        } else {
            map[BUNDLE_SSID] = preference.getSsid()
            map[BUNDLE_PASSWORD] = preference.getPassPhrase()
            map[BUNDLE_AUTHENTICATION_MODE] = preference.getAuthenticationMode()
            map[BUNDLE_ENCRYPTION_TYPE] = preference.getEncryptionType()

            responseObservable.onNext(Event(EventType.GET_WIFI_DETAILS, EventStatus.SUCCESS, map, null))

        }*/

    }

    override fun setWifiDetails(ssid: String, password: String, authenticationMode: String, encryptionType: String) {
      /*  Timber.d("Setting wifi details")
        preference.setSsid(ssid)
        preference.setPassPhrase(password)
        preference.setAuthenticationMode(authenticationMode)
        preference.setEncryptionType(encryptionType)

        responseObservable.onNext(Event(EventType.SET_WIFI_DETAILS, EventStatus.SUCCESS, null, null))*/
    }


}