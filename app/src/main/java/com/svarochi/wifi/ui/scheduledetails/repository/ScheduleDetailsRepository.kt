package com.svarochi.wifi.ui.scheduledetails.repository

import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.GroupLight
import com.svarochi.wifi.database.entity.Schedule
import com.svarochi.wifi.model.api.common.EndAction
import com.svarochi.wifi.model.api.common.StartAction
import com.svarochi.wifi.model.api.response.AddScheduleResponse
import com.svarochi.wifi.model.api.response.DeleteScheduleResponse
import com.svarochi.wifi.model.api.response.EditScheduleResponse
import com.svarochi.wifi.model.common.CustomGroup
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class ScheduleDetailsRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl {
    private val responseObservable = PublishSubject.create<Event>()
    private var disposables = CompositeDisposable()
    private var sharedPreference: SharedPreference? = null
    private var TAG = "ScheduleDetailsRepository"

    // Values
    private var projectId: Long = -1
    private var networkId: Long = -1
    private var userId: String = ""

    init {
        fetchIdsFromPreferences()
    }

    private fun fetchIdsFromPreferences() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        projectId = sharedPreference!!.getProjectId()
        networkId = sharedPreference!!.getNetworkId()
        userId = sharedPreference!!.getUserId()
    }

    override fun initCommunicationResponseObserver() {
    }

    override fun getCommunicationResponseObserver(): PublishSubject<Event> = responseObservable

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    fun deleteSchedule(scheduleId: Int) {
        disposables.add(
                ApiRequestParser.deleteSchedule(
                        userId,
                        networkId.toInt(),
                        projectId.toInt(),
                        scheduleId
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<DeleteScheduleResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed Delete Schedule Api")
                            }

                            override fun onNext(response: Response<DeleteScheduleResponse>) {
                                if (response.isSuccessful) {

                                    if (response.body() != null) {
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.d("Received success response while deleting schedule in server")
                                            deleteScheduleFromLocalDb(scheduleId)

                                        } else {
                                            Timber.d("Something went wrong while deleting schedule in server")
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.d("Response error message - $errorMessage")
                                            }
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.DELETE_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }

                                    } else {
                                        Timber.d("Something went wrong while deleting schedule in server")
                                        Timber.d("No response body received from server")
                                        responseObservable.onNext(
                                                Event(
                                                        EventType.DELETE_SCHEDULE,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }

                                } else {
                                    Timber.d("Something went wrong while deleting schedule in server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.d("Response error message - $errorMessage")
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.DELETE_SCHEDULE,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                            }

                            override fun onError(e: Throwable) {
                                Timber.d("Something went wrong while deleting schedule in server")
                                Timber.e(e)
                                e.printStackTrace()
                                responseObservable.onNext(Event(EventType.DELETE_SCHEDULE, EventStatus.FAILURE, null, null))
                            }
                        }
                        )
        )
    }

    fun addNewSchedule(
            scheduleName: String,
            days: ArrayList<String>,
            startTime: String,
            startAction: StartAction,
            endTime: String,
            endAction: EndAction,
            devices: ArrayList<String>,
            groups: ArrayList<Int>

    ) {
        if (GeneralUtils.isInternetAvailable(application)) {

            disposables.add(
                    ApiRequestParser.addNewSchedule(
                            userId,
                            projectId.toInt(),
                            networkId.toInt(),
                            scheduleName,
                            days,
                            startTime,
                            startAction,
                            endTime,
                            endAction,
                            devices,
                            groups
                    )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<AddScheduleResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed Add New Schedule Api")
                                }

                                override fun onNext(response: Response<AddScheduleResponse>) {
                                    if (response.isSuccessful) {

                                        if (response.body() != null) {
                                            if (response.body()!!.status.contains("success", true)) {

                                                Timber.d("Received success response while adding new schedule in server")
                                                addNewScheduleInLocalDb(
                                                        response.body()?.schedule_id!!,
                                                        scheduleName,
                                                        days,
                                                        Utilities.utcToLocal(startTime),
                                                        startAction,
                                                        Utilities.utcToLocal(endTime),
                                                        endAction,
                                                        devices,
                                                        groups
                                                )

                                            } else {
                                                Timber.d("Something went wrong while adding new schedule to server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.d("Response error message - $errorMessage")
                                                }
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.ADD_NEW_SCHEDULE,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }

                                        } else {
                                            Timber.d("Something went wrong while adding new schedule to server")
                                            Timber.d("No response body received from server")
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_NEW_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }

                                    } else {
                                        Timber.d("Something went wrong while adding new schedule to server")
                                        if (response.code() == 409) {
                                            Timber.d("Schedule by that name already exists")
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_NEW_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            ALREADY_EXISTS,
                                                            null
                                                    )
                                            )
                                        } else {
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.d("Respone error message - $errorMessage")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_NEW_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    }
                                }

                                override fun onError(e: Throwable) {
                                    Timber.d("Something went wrong while adding new schedule to server")
                                    Timber.e(e)
                                    e.printStackTrace()
                                    responseObservable.onNext(Event(EventType.ADD_NEW_SCHEDULE, EventStatus.FAILURE, null, null))
                                }
                            }
                            ))
        } else {
            responseObservable.onNext(
                    Event(
                            EventType.ADD_NEW_SCHEDULE,
                            EventStatus.FAILURE,
                            NO_INTERNET_AVAILABLE,
                            null
                    )
            )
        }
    }

    private fun addNewScheduleInLocalDb(scheduleId: Int, scheduleName: String, days: ArrayList<String>, startTime: String, startAction: StartAction, endTime: String, endAction: EndAction, devices: ArrayList<String>, groups: ArrayList<Int>) {

        var newSchedule = Schedule(
                scheduleId.toLong(),
                scheduleName,
                networkId,
                Converters.fromStringArrayList(days),
                startTime,
                Converters.fromAction(startAction),
                endTime,
                Converters.fromAction(endAction),
                Converters.fromStringArrayList(devices),
                Converters.fromIntArrayList(groups))

        application.wifiLocalDatasource.addNewSchedule(newSchedule)

        responseObservable.onNext(
                Event(
                        EventType.ADD_NEW_SCHEDULE,
                        EventStatus.SUCCESS,
                        null,
                        null
                )
        )
    }

    private fun editScheduleInLocalDb(scheduleId: Long, scheduleName: String, days: ArrayList<String>, startTime: String, startAction: StartAction, endTime: String, endAction: EndAction, devices: ArrayList<String>, groups: ArrayList<Int>) {

        var newSchedule = Schedule(
                scheduleId,
                scheduleName,
                networkId,
                Converters.fromStringArrayList(days),
                startTime,
                Converters.fromAction(startAction),
                endTime,
                Converters.fromAction(endAction),
                Converters.fromStringArrayList(devices),
                Converters.fromIntArrayList(groups))

        application.wifiLocalDatasource.editSchedule(newSchedule)

        responseObservable.onNext(
                Event(
                        EventType.EDIT_SCHEDULE,
                        EventStatus.SUCCESS,
                        null,
                        null
                )
        )
    }

    private fun deleteScheduleFromLocalDb(scheduleId: Int) {
        application.wifiLocalDatasource.deleteSchedule(scheduleId.toLong())
        responseObservable.onNext(
                Event(
                        EventType.DELETE_SCHEDULE,
                        EventStatus.SUCCESS,
                        null,
                        null
                )
        )
    }

    fun editSchedule(
            scheduleId: Long,
            scheduleName: String,
            days: ArrayList<String>,
            startTime: String,
            startAction: StartAction,
            endTime: String,
            endAction: EndAction,
            devices: ArrayList<String>,
            groups: ArrayList<Int>
    ) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.editSchedule(
                            userId,
                            projectId.toInt(),
                            networkId.toInt(),
                            scheduleId,
                            scheduleName,
                            days,
                            startTime,
                            startAction,
                            endTime,
                            endAction,
                            devices,
                            groups
                    )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<EditScheduleResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed Edit Schedule Api")
                                }

                                override fun onNext(response: Response<EditScheduleResponse>) {
                                    if (response.isSuccessful) {

                                        if (response.body() != null) {
                                            if (response.body()!!.status.contains("success", true)) {

                                                Timber.d("Received success response while editing schedule in server")
                                                editScheduleInLocalDb(
                                                        scheduleId,
                                                        scheduleName,
                                                        days,
                                                        Utilities.utcToLocal(startTime),
                                                        startAction,
                                                        Utilities.utcToLocal(endTime),
                                                        endAction,
                                                        devices,
                                                        groups
                                                )

                                            } else {
                                                Timber.d("Something went wrong while editing schedule in server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.d("Response error message - $errorMessage")
                                                }
                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_SCHEDULE,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }

                                        } else {
                                            Timber.d("Something went wrong while editing schedule in server")
                                            Timber.d("No response body received from server")
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }

                                    } else {
                                        Timber.d("Something went wrong while editing schedule in server")
                                        if (response.code() == 409) {
                                            Timber.d("Schedule by that name already exists")
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            ALREADY_EXISTS,
                                                            null
                                                    )
                                            )
                                        } else {
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.d("Respone error message - $errorMessage")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_SCHEDULE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    }
                                }

                                override fun onError(e: Throwable) {
                                    Timber.d("Something went wrong while editing schedule in server")
                                    Timber.e(e)
                                    e.printStackTrace()
                                    responseObservable.onNext(Event(EventType.EDIT_SCHEDULE, EventStatus.FAILURE, null, null))
                                }
                            }
                            ))
        } else {
            Timber.d("No internet available")
            responseObservable.onNext(Event(EventType.EDIT_SCHEDULE, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }

    }


    fun getAllGroups(): ArrayList<CustomGroup> {
        var groupsList = ArrayList<CustomGroup>()
        application.wifiLocalDatasource.getGroups(networkId).forEach {
            groupsList.add(CustomGroup(it))
        }
        return groupsList
    }

    fun getAllLights(): ArrayList<CustomLight> {
        var lightsList = ArrayList<CustomLight>()
        application.wifiLocalDatasource.getLights(networkId).forEach {
            lightsList.add(CustomLight(it))
        }
        return lightsList
    }

    fun getGroupLights(groupId: Long): ArrayList<GroupLight> {
        var groupLightsList = ArrayList<GroupLight>()
        groupLightsList.addAll(application.wifiLocalDatasource.getLightsOfGroup(groupId))
        return groupLightsList
    }

    fun getSchedule(scheduleId: Long): Schedule {
        return application.wifiLocalDatasource.getSchedule(scheduleId)
    }

    fun isScheduleNameTaken(name: String, scheduleId: Long): Boolean {
        return application.wifiLocalDatasource.getScheduleWithName(name, networkId, scheduleId).isNotEmpty()
    }

    fun isScheduleNameTaken(name: String): Boolean {
        return application.wifiLocalDatasource.getScheduleWithName(name, networkId).isNotEmpty()
    }
}

