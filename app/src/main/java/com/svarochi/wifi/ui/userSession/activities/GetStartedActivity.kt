package com.svarochi.wifi.ui.userSession.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import bizbrolly.svarochiapp.R
import com.akkipedia.skeleton.utils.GeneralUtils
import com.facebook.stetho.Stetho
import com.svarochi.wifi.common.CommonUtils
import com.svarochi.wifi.common.DialogUtils
import com.svarochi.wifi.preference.Preferences
import com.svarochi.wifi.preference.Preferences.PREF_RECOMMEND_ONE_USER
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_NAVIGATE_FROM
import com.svarochi.wifi.ui.projects.view.activity.ProjectsActivity
import kotlinx.android.synthetic.main.activity_get_started.*

class GetStartedActivity : Activity() {

    private var mSplashHandler: Handler? = null
    private var mSplashRunnable: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(com.bizbrolly.skeleton.R.anim.enter_slide_in, com.bizbrolly.skeleton.R.anim.enter_slide_out)
        setContentView(R.layout.activity_get_started)
        init()
    }

    override fun onResume() {
        Log.e(TAG, "onResume")
        super.onResume()

        if (!Preferences.getInstance(this).getBoolean(PREF_RECOMMEND_ONE_USER)) {
            Preferences.getInstance(this).putBoolean(PREF_RECOMMEND_ONE_USER, true)
            DialogUtils.showDefaultAlertMessage(this, "", getString(R.string.recommended_to_use_one_user_), getString(R.string.got_it), View.OnClickListener {
                if (mSplashHandler != null && mSplashRunnable != null) {
                    mSplashHandler!!.postDelayed(mSplashRunnable, 2000)
                }
            })
        } else {
            if (mSplashHandler != null && mSplashRunnable != null) {
                mSplashHandler!!.postDelayed(mSplashRunnable, 2000)
            }
        }
    }

    override fun onPause() {
        Log.e(TAG, "onPause")
        if (mSplashHandler != null && mSplashRunnable != null) {
            mSplashHandler!!.removeCallbacks(mSplashRunnable)
        }
        super.onPause()
    }

    override fun onDestroy() {
        Log.e(TAG, "onDestroy")
        if (mSplashHandler != null && mSplashRunnable != null) {
            mSplashHandler!!.removeCallbacks(mSplashRunnable)
        }
        overridePendingTransition(com.bizbrolly.skeleton.R.anim.exit_slide_in, com.bizbrolly.skeleton.R.anim.exit_slide_out)
        super.onDestroy()
    }

    private fun init() {
        try {
            Stetho.initializeWithDefaults(this)

            if (!Preferences.getInstance(this).isPasswordSaved) {
                btnGetStarted.visibility = View.VISIBLE
                ivHelp.visibility = View.VISIBLE
            } else {
                btnGetStarted.visibility = View.INVISIBLE
                ivHelp.visibility = View.INVISIBLE
                mSplashHandler = Handler()
                mSplashRunnable = Runnable { navigateNext() }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun actionHelp(view: View) {
        CommonUtils.viewBrowser(this, Constants.SVAROCHI_PAGE)
    }

    fun actionGetStarted(view: View) {
        navigateNext()
    }

    private fun navigateNext() {
        if (Preferences.getInstance(this).isOTPWaiting) {
            val intent = Intent(this, OTPActivity::class.java)
            intent.putExtra(BUNDLE_NAVIGATE_FROM, "SplashActivity")
            startActivity(intent)
        } else {
            if (Preferences.getInstance(this).isPasswordSaved) {
                Preferences.getInstance(this).putBoolean(Preferences.PREF_ALREADY_LOGGED_IN, true)
                /* Implementation for Wifi Module */
                openWifiProjectsScreen()
            } else {
                startActivity(Intent(this, NewAccountActivity::class.java))
            }
        }
        finish()
    }

    private fun openWifiProjectsScreen() {
        startActivity(Intent(this, ProjectsActivity::class.java))
    }

    companion object {
        private val TAG = GetStartedActivity::class.java.simpleName
    }
}
