package com.svarochi.wifi.ui.projects.repository

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.preference.Preferences
import com.akkipedia.skeleton.utils.GeneralUtils
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS
import com.svarochi.wifi.common.Constants.Companion.CONTAINS_NETWORKS
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.model.api.response.*
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.projects.ProjectsImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class ProjectsRepository(var application: SvarochiApplication) : BaseRepository(),
        BaseRepositoryImpl, ProjectsImpl {


    private val responseObservable = PublishSubject.create<Event>()
    private var disposables = CompositeDisposable()
    private var userId: String = ""
    private var sharedPreference: SharedPreference? = null


    override fun initCommunicationResponseObserver() {
        // No implementations as its not using service
    }

    init {
        fetchIdsFromPreferences()
        callApis()
    }

    fun stopUdpBroadcasts() {
        application.wifiCommunicationService.stopBroadcast()
    }

    private fun callApis() {
        var currentProfile = sharedPreference!!.getCurrentProfile()
        if (currentProfile.equals(CURRENT_PROFILE_SELF)) {
            getProjectsFromApi()
        } else {
            getSharedProjectsFromApi(currentProfile)
        }
    }

    private fun getSharedProjectsFromApi(profileId: String) {
        disposables.add(
                ApiRequestParser.getSharedProjects(profileId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetSharedProjectsResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed Shared Projects api")
                            }

                            override fun onNext(response: Response<GetSharedProjectsResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Shared Projects API response body - ${response.body()}")

                                        if (response.body()!!.status.contains("success", true)) {
                                            if (response.body()!!.records != null) {
                                                Timber.d("Successfully received shared projects from server")
                                                storeProjectsIntoDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("No records for shared projects found from server")
                                            }
                                        } else {
                                            Timber.d("Something went wrong while fetching shared projects from server")
                                            if (response.body()!!.description != null) {
                                                Timber.d("Server message - ${response.body()!!.description}")
                                            } else {
                                                Timber.d("Server message - **NO ERROR MESSAGE RECEIVED**")
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching shared projects from server")
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching shared projects from server")
                                    Timber.d("Response code - ${response.code()}")
                                    Timber.d("Response body - ${response.body()}")
                                    Timber.d("Response errorBody - ${response.errorBody()}")
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching shared projects from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun fetchIdsFromPreferences() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        var prefUserId: String = sharedPreference!!.getUserId()
        if (prefUserId.isEmpty()) {
            var blePreference = Preferences.getInstance(application)
            sharedPreference!!.setUserId(blePreference.getInt(Preferences.PREF_USER_ID, 0).toString())
            userId = sharedPreference!!.getUserId()
        }
        userId = sharedPreference!!.getUserId()

    }

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    override fun getCommunicationResponseObserver() = responseObservable

    override fun getProjects() {

        var projectList = application.wifiLocalDatasource.getProjects()
        Timber.d("Got ${projectList.size} projects from DB ...")
        responseObservable.onNext(Event(EventType.GET_PROJECTS, EventStatus.SUCCESS, projectList, null))
    }

    fun getProjectsLiveData(): LiveData<List<Project>> {
        return application.wifiLocalDatasource.getProjectsLiveData()
    }

    override fun addNewProject(name: String) {
        if (isProjectExisting(name).not()) {
            addProjectToServer(name)
        } else {
            responseObservable.onNext(Event(EventType.ADD_NEW_PROJECT, EventStatus.FAILURE, ALREADY_EXISTS, null))
        }
    }

    private fun addProjectToServer(name: String) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.addProject(userId, name)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<AddProjectResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed add project api")
                                }

                                override fun onNext(response: Response<AddProjectResponse>) {
                                    if (response.isSuccessful) {

                                        if (response.body() != null) {

                                            if (response.body()!!.status.contains("success", true)) {

                                                if (response.body()!!.project_id != null) {

                                                    addProjectIntoDb(response.body()!!.project_id!!.toLong(), name)

                                                } else {
                                                    Timber.d("Something went wrong while adding project($name) from server")
                                                    Timber.d("No project_id received")
                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.ADD_NEW_PROJECT,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }
                                            } else {
                                                Timber.d("Something went wrong while adding project($name) from server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.ADD_NEW_PROJECT,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("Something went wrong while adding project($name) from server")
                                            Timber.d("No response received from server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_NEW_PROJECT,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("Something went wrong while adding project($name) into server")
                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }

                                        responseObservable.onNext(Event(EventType.ADD_NEW_PROJECT, EventStatus.FAILURE, null, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while adding project($name) from server")
                                    error.printStackTrace()
                                    responseObservable.onNext(Event(EventType.ADD_NEW_PROJECT, EventStatus.FAILURE, null, null))
                                }

                            })
            )
        } else {
            responseObservable.onNext(Event(EventType.ADD_NEW_PROJECT, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun isProjectExisting(name: String): Boolean {
        var queryResponse = application.wifiLocalDatasource.getProjectsOfName(name)
        Timber.d("Returning the rows having project name - $name")

        if (queryResponse.isEmpty()) {
            Timber.d("Project $name doesn't exist")
            return false
        } else {
            Timber.d("Project $name already exists")
            return true
        }
    }

    private fun addProjectIntoDb(projectId: Long, projectName: String) {
        var project = Project(projectId, projectName)
        application.wifiLocalDatasource.addNewProject(project)
        Timber.d("Added project - ${project.name}")
        responseObservable.onNext(Event(EventType.ADD_NEW_PROJECT, EventStatus.SUCCESS, null, null))
    }

    override fun editProjectName(projectId: Long, name: String) {
        if (GeneralUtils.isInternetAvailable(application)) {
            if (isProjectExisting(name).not()) {
                disposables.add(
                        ApiRequestParser.editProject(userId, projectId.toInt(), name)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(object : DisposableObserver<Response<EditProjectResponse>>() {
                                    override fun onComplete() {
                                        Timber.d("Completed projects api")
                                    }

                                    override fun onNext(response: Response<EditProjectResponse>) {
                                        if (response.isSuccessful) {
                                            if (response.body() != null) {
                                                Timber.d("Edit Project API response body - ${response.toString()}")
                                                if (response.body()!!.status.contains("success", true)) {
                                                    Timber.d("Successfully edited project in server")
                                                    updateProjectNameInDb(projectId, name)
                                                }
                                            } else {
                                                Timber.d("Something went wrong while editing project in server")
                                                Timber.d("No response got from server")
                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.EDIT_PROJECT_NAME,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("Something went wrong while editing project in server")
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.EDIT_PROJECT_NAME,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    }

                                    override fun onError(error: Throwable) {
                                        Timber.d("Something went wrong while editing project in server")
                                        Timber.e(error)
                                        error.printStackTrace()
                                        responseObservable.onNext(
                                                Event(
                                                        EventType.EDIT_PROJECT_NAME,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }

                                })

                )
            } else {
                responseObservable.onNext(Event(EventType.EDIT_PROJECT_NAME, EventStatus.FAILURE, ALREADY_EXISTS, null))
            }
        } else {
            Timber.d("No internet available")
            responseObservable.onNext(Event(EventType.EDIT_PROJECT_NAME, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun updateProjectNameInDb(projectId: Long, name: String) {
        application.wifiLocalDatasource.editProjectName(projectId, name)
        Timber.d("Edited project($projectId) with new name - $name in db")

        responseObservable.onNext(Event(EventType.EDIT_PROJECT_NAME, EventStatus.SUCCESS, null, null))
    }

    override fun deleteProject(projectId: Long) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.deleteProject(userId, projectId.toInt())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<DeleteProjectResponse>>() {
                                override fun onComplete() {
                                    Timber.d("Completed projects api")
                                }

                                override fun onNext(response: Response<DeleteProjectResponse>) {
                                    Timber.d("Delete Project API response body - ${response.toString()}")
                                    if (response.isSuccessful) {
                                        if (response.body()?.status!!.contains("success", true)) {
                                            Timber.d("Successfully deleted project in server")
                                            deleteProjectFromLocalDb(projectId)
                                        } else {
                                            if(response.body()?.description != null){
                                                if(response.body()?.description.equals("Project contains networks")){
                                                    responseObservable.onNext(Event(EventType.DELETE_PROJECT, EventStatus.FAILURE, CONTAINS_NETWORKS, null))
                                                    return
                                                }
                                            }
                                            Timber.d("Something went wrong while deleting project in server")
                                            if (response.body() != null && response.body()?.description != null) {
                                                Timber.d("Server message - ${response.body()?.description}")
                                            } else {
                                                Timber.d("Server message - **NO ERROR MESSAGE RECEIVED**")
                                            }
                                            responseObservable.onNext(Event(EventType.DELETE_PROJECT, EventStatus.FAILURE, projectId, null))
                                        }
                                    } else {
                                        Timber.d("Something went wrong while deleting project in server")
                                        if (response.body() != null && response.body()?.description != null) {
                                            Timber.d("Server message - ${response.body()?.description}")
                                        } else {
                                            Timber.d("Server message - **NO ERROR MESSAGE RECEIVED**")
                                        }
                                        responseObservable.onNext(Event(EventType.DELETE_PROJECT, EventStatus.FAILURE, projectId, null))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while deleting project in server")
                                    Timber.e(error)
                                    responseObservable.onNext(Event(EventType.DELETE_PROJECT, EventStatus.FAILURE, projectId, null))
                                }

                            })
            )
        } else {
            responseObservable.onNext(Event(EventType.DELETE_PROJECT, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun deleteProjectFromLocalDb(projectId: Long) {
        application.wifiLocalDatasource.deleteProject(projectId)
        Timber.d("Successfully deleted project($projectId) in db")

        responseObservable.onNext(Event(EventType.DELETE_PROJECT, EventStatus.SUCCESS, projectId, null))
    }

    private fun getProjectsFromApi() {
        disposables.add(
                ApiRequestParser.getProjects(userId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<GetProjectsResponse>() {
                            override fun onComplete() {
                                Timber.d("Completed projects api")
                            }

                            override fun onNext(response: GetProjectsResponse) {
                                Timber.d("Get Projects API response body - ${response.toString()}")
                                if (response.status.contains("success", true)) {
                                    Timber.d("Successfully received projects from server")
                                    if (response.records != null) {
                                        storeProjectsIntoDb(response.records)
                                    } else {
                                        Timber.d("No records for projects found from server")
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching projects from server")
                                    if (response.description != null) {
                                        Timber.d("Server message - ${response.description}")
                                    } else {
                                        Timber.d("Server message - **NO ERROR MESSAGE RECEIVED**")
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching projects from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun storeProjectsIntoDb(projects: List<com.svarochi.wifi.model.api.response.Project>) {
        var projectListToStoreInDb = parseProjectsResponse(projects)
        application.wifiLocalDatasource.insertProjects(projectListToStoreInDb)

        for (project in projects) {
            var networksList = ArrayList<Network>()
            if (project.networks != null) {
                for (network in project.networks) {
                    var networkToAdd = Network(network.network_id.toLong(), project.project_id.toLong(), network.network_name)
                    networksList.add(networkToAdd)
                }
                application.wifiLocalDatasource.insertNetworks(networksList)
            }
            Timber.d("Successfully stored networks of project(${project.project_name} in local db")
        }
    }


    private fun parseProjectsResponse(projects: List<com.svarochi.wifi.model.api.response.Project>)
            : List<Project> {
        var projectsList = ArrayList<Project>()
        for (project in projects) {
            var projectToAdd = Project(project.project_id.toLong(), project.project_name)
            projectsList.add(projectToAdd)


        }
        return projectsList.toList()
    }

    private fun test() {
        /** Steps involved -
         * 1. Fetch data from DB
         *       1.1. onCreate of activity observe the whole db / onCreate of activity ask the repository to observe the db
         *              and emit the changes to view
         * 2. Emit the data
         *       2.1. onCreate of activity observe the db / onCreate of activity ask the repository to observe the db
         *              and emit the changes to view
         * 3. Make Api call
         * 4. Parse response
         * 5. Store in DB
         * 6. Emit the data
         *       6.1. onCreate of activity observe the db / onCreate of activity ask the repository to observe the db
         *              and emit the changes to view
         */


    }

    fun switchedProfile() {
        application.wifiLocalDatasource.clearProjectsTable()
        Timber.d("Cleared projects table")
        callApis()
    }

    fun getNetworksCount(projectId: Long): Int {
        return application.wifiLocalDatasource.getNetworks(projectId).size
    }

}