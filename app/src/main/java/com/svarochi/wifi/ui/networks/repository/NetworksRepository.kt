package com.svarochi.wifi.ui.networks.repository

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.LocalDataSource
import com.svarochi.wifi.model.api.response.GetNetworksResponse
import com.svarochi.wifi.model.api.response.Network
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.ui.networks.NetworksImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class NetworksRepository(var application: SvarochiApplication) : BaseRepository(),
        BaseRepositoryImpl, NetworksImpl {

    private var localDataSource: LocalDataSource = application.wifiLocalDatasource

    private var userId: String = ""
    private var projectId: Long = -1
    private val responseObservable = PublishSubject.create<Event>()
    private var disposables = CompositeDisposable()
    private var profileId: String? = null
    private var isSelfProfile = true
    override fun initCommunicationResponseObserver() {
        // No implementations as this isn't using service
    }

    private var sharedPreference: SharedPreference? = null

    init {
        fetchIdsFromPreferences()
        callApis()
    }

    private fun callApis() {
        getNetworksFromApi()
    }

    private fun fetchIdsFromPreferences() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        projectId = sharedPreference!!.getProjectId()
        userId = sharedPreference!!.getUserId()

        profileId = sharedPreference!!.getCurrentProfile()
        isSelfProfile = profileId!!.equals(CURRENT_PROFILE_SELF)
    }

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    private fun getNetworksFromApi() {
        if (isSelfProfile){
            getSelfNetworksFromServer()
        }else{
            if(profileId!=null) {
                getGuestNetworksFromServer()
            }else{
                Timber.d("Profile id is null hence cannot fetch the shared networks from server")
            }
        }

    }

    private fun getGuestNetworksFromServer() {
        disposables.add(
                ApiRequestParser.getSharedNetworks(profileId!!, projectId.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetNetworksResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed get shared networks api")
                            }

                            override fun onNext(response: Response<GetNetworksResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Shared Networks API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            if (response.body()!!.records != null) {
                                                Timber.d("Successfully received shared networks from server")
                                                storeNetworksIntoDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("No records for shared networks found from server")
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching shared networks from server")
                                        Timber.d("No response got for shared networks from server")

                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching shared networks from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching shared networks from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun getSelfNetworksFromServer() {
        disposables.add(
                ApiRequestParser.getNetworks(userId, projectId.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<Response<GetNetworksResponse>>() {
                            override fun onComplete() {
                                Timber.d("Completed networks api")
                            }

                            override fun onNext(response: Response<GetNetworksResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.d("Get Networks API response body - ${response.toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            if (response.body()!!.records != null) {
                                                Timber.d("Successfully received networks from server")
                                                storeNetworksIntoDb(response.body()!!.records!!)
                                            } else {
                                                Timber.d("No records for networks found from server")
                                            }
                                        }
                                    } else {
                                        Timber.d("Something went wrong while fetching networks from server")
                                        Timber.d("No response got for networks from server")

                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }
                                    }
                                } else {
                                    Timber.d("Something went wrong while fetching networks from server")
                                    var errorMessage = Utilities.getErrorMessage(response)
                                    if (errorMessage != null) {
                                        Timber.e(errorMessage)
                                    } else {
                                        Timber.e("No error message received")
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.d("Something went wrong while fetching networks from server")
                                Timber.e(error)
                            }

                        })
        )
    }

    private fun storeNetworksIntoDb(records: List<Network>) {
        var networkListToStoreInDb = parseNetworksResponse(records)
        application.wifiLocalDatasource.insertNetworks(networkListToStoreInDb)

        Timber.d("Successfully added new networks(${records.size}) into local db")
    }

    private fun parseNetworksResponse(networks: List<Network>)
            : List<com.svarochi.wifi.database.entity.Network> {
        var networksList = ArrayList<com.svarochi.wifi.database.entity.Network>()
        for (network in networks) {
            var networkToAdd =
                    com.svarochi.wifi.database.entity.Network(
                            network.network_id.toLong(),
                            network.network_name,
                            network.project_id.toLong(),
                            network.ssid,
                            network.password,
                            network.authentication_mode,
                            network.encryption_type)
            networksList.add(networkToAdd)
        }
        return networksList.toList()
    }

    override fun getCommunicationResponseObserver() = responseObservable

    fun getNetworksLiveData(projectId: Long): LiveData<List<com.svarochi.wifi.database.entity.Network>> {
        return application.wifiLocalDatasource.getNetworksLiveData(projectId)
    }

}