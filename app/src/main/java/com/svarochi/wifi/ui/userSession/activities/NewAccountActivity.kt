package com.svarochi.wifi.ui.userSession.activities

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View

import com.akkipedia.skeleton.permissions.PermissionManager
import com.svarochi.wifi.common.Constants

import bizbrolly.svarochiapp.R
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.common.CommonUtils

/**
 * Created by Akash on 24/04/17.
 */

class NewAccountActivity : BaseActivity() {
    private var permissionManager: PermissionManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_account)
        Log.e(TAG, "NewAccountActivity onCreate")
        initPermissions()
    }

    fun actionRegister(view: View) {
        startActivity(Intent(this@NewAccountActivity, RegistrationActivity::class.java))
    }

    fun actionLogin(view: View) {
        //startActivity(new Intent(NewAccountActivity.this, AlreadyRegisteredActivity.class));
        actionLoginMaster(view)
    }

    fun actionLoginMaster(view: View) {
        val intent = Intent(this@NewAccountActivity, LoginActivity::class.java)
        val bundle = Bundle()
        bundle.putBoolean(LoginActivity.BUNDLE_IS_SLAVE_USER, false)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun actionBrowseSvarochi(view: View) {
        CommonUtils.viewBrowser(this@NewAccountActivity, Constants.SVAROCHI_PAGE)
    }

    private fun initPermissions() {
        val permissionListener = PermissionManager.PermissionListener { allGranted, grantedPermissions, rejectedPermissions ->
            if (!allGranted) {
                showToast(getString(R.string.please_provide_all_the_permissions_))
                //Rahul: 2017/12/26 Stopping application finish in case of deny the permission where we are already showing a toast and the user click again for the process it will again ask for the permission.
                //finish();
                return@PermissionListener
            }

            //Rahul; 2019/07/10 Stopping for looking th Bluwtooth prmission on start of the application only,
            // as we do not need any location or bluetooth permission for login or registration flow
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    initGps();
                else {
                    initBluetooth();
                }*/
        }
        permissionManager = PermissionManager.Builder(permissionListener)
                .with(this)
                .addLocationPermissions()
                .addPermission(Manifest.permission.BLUETOOTH)
                .addPermission(Manifest.permission.BLUETOOTH_ADMIN)
                .addPermission(Manifest.permission.CAMERA)
                .addPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .addPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .addPermission(Manifest.permission.RECORD_AUDIO)
                .addCameraPermissions()
                .build()
        permissionManager!!.checkAndAskPermissions()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (permissionManager != null) {
            permissionManager!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    companion object {
        private val TAG = NewAccountActivity::class.java.simpleName
    }
}
