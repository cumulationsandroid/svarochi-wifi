package com.svarochi.wifi.ui.room.view.adapter

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.Utilities.Companion.getLampType
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.common.CustomLight
import com.svarochi.wifi.model.common.CustomSsid
import com.svarochi.wifi.model.communication.LampType
import com.svarochi.wifi.ui.room.view.fragment.BleLightsFragment
import timber.log.Timber

class LightsAdapter(var devicesList: ArrayList<Any>, var isSelfProfile: Boolean, var fragmentContext: BleLightsFragment) :
        androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val FREE_DEVICE: Int = 0
    private val CONFIGURED_DEVICE: Int = 1

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        if (viewType == FREE_DEVICE) {
            return FreeDeviceViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(
                            R.layout.item_free_device,
                            viewGroup,
                            false
                    )
            )
        }
        return ConfiguredDeviceViewHolder(
                LayoutInflater.from(viewGroup.context).inflate(
                        R.layout.item_configured_device,
                        viewGroup,
                        false
                )
        )
    }

    override fun getItemCount(): Int {
        return devicesList.size
    }

    override fun onBindViewHolder(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (viewHolder.itemViewType == FREE_DEVICE) {
            (viewHolder as FreeDeviceViewHolder).deviceName.text = (devicesList[position] as CustomSsid).ssid
        } else {
            (viewHolder as ConfiguredDeviceViewHolder).configuredDeviceName.text =
                    (devicesList[position] as CustomLight).light.name

            viewHolder.onOffCheckBox.isChecked = (devicesList[position] as CustomLight).light.onOffValue

            when (getLampType((devicesList[position] as CustomLight).light.lampType)) {

                LampType.BRIGHT_AND_DIM -> {
                    viewHolder.lampImage.setImageResource(R.drawable.icon_bright_dim)
                    viewHolder.lampTypeText.text = fragmentContext.getString(R.string.label_bright_and_dim)
                }
                LampType.WARM_AND_COOL -> {
                    viewHolder.lampImage.setImageResource(R.drawable.icon_warm_cool)
                    viewHolder.lampTypeText.text = fragmentContext.getString(R.string.label_warm_and_cool)
                }
                LampType.COLOR_AND_DAYLIGHT -> {
                    viewHolder.lampImage.setImageResource(R.drawable.icon_color_daylight)
                    viewHolder.lampTypeText.text = Html.fromHtml("${fragmentContext.getString(R.string.label_color_and_daylight)}<sup><small>${fragmentContext.getString(R.string.label_plus)}</small></sup>")
                }
                else->{
                    viewHolder.lampImage.setImageResource(R.drawable.icon_unpaired)
                    viewHolder.lampTypeText.text = "-"
                }
            }

            if ((devicesList[position] as CustomLight).isLocallyAvailable()) {
                viewHolder.isLocallyAvailText.visibility = View.VISIBLE
            } else {
                viewHolder.isLocallyAvailText.visibility = View.GONE
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        if (devicesList[position] is CustomSsid) {
            return FREE_DEVICE
        } else {
            return CONFIGURED_DEVICE
        }
    }

    fun updateList(newDevicesList: ArrayList<Any>) {
        for (device in newDevicesList) {
            if (!devicesList.contains(device)) {
                devicesList.add(device)
            }
        }
        notifyDataSetChanged()
    }


    fun setLocallyAvailable(macId: String, flag: Boolean) {
        for (device in devicesList) {
            if (device is CustomLight) {
                if (device.light.macId.equals(macId)) {
                    device.setLocallyAvailable(flag)
                    break
                }
            }
        }
        notifyDataSetChanged()
    }

    fun updateLight(macId: String, light: Light) {
        for (device in devicesList) {
            if (device is CustomLight) {
                if (device.light.macId.equals(macId)) {
                    device.light = light
                    break
                }
            }
        }
        notifyDataSetChanged()
    }

    inner class FreeDeviceViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        var deviceName = view.findViewById<TextView>(R.id.tv_freeDeviceNmae)
        var configureDevice = view.findViewById<ImageView>(R.id.iv_confiureFreeDevice)

        init {
            configureDevice.setOnClickListener {
                fragmentContext.configureDevice((devicesList[adapterPosition] as CustomSsid).ssid)
            }
        }
    }

    inner class ConfiguredDeviceViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        var configuredDeviceName = view.findViewById<TextView>(R.id.tv_deviceName)
        var isLocallyAvailText = view.findViewById<TextView>(R.id.tv_locallyAvailable)
        var onOffCheckBox = view.findViewById<CheckBox>(R.id.cb_onOffLight)
        var deviceControl = view.findViewById<ImageView>(R.id.iv_settings)
        var lampImage = view.findViewById<ImageView>(R.id.iv_deviceTypeImage)
        var lampTypeText = view.findViewById<TextView>(R.id.tv_deviceTypeName)

        init {
            onOffCheckBox.setOnClickListener {
                var selectedPosition = adapterPosition
                if (selectedPosition != -1) {
                    if (onOffCheckBox.isChecked) {
                        // Turn on light
                        onOffCheckBox.isChecked = onOffCheckBox.isChecked.not()
                        fragmentContext.turnOnLight((devicesList[selectedPosition] as CustomLight).light.macId)

                        /*// Device not locally available
                    showToast(fragmentContext.context!!, fragmentContext.getString(R.string.toast_light_needs_to_be_connected_locally))
                    onOffCheckBox.isChecked = onOffCheckBox.isChecked.not()*/

                    } else {

                        onOffCheckBox.isChecked = onOffCheckBox.isChecked.not()
                        fragmentContext.turnOffLight((devicesList[selectedPosition] as CustomLight).light.macId)

                        /*// Device not locally available
                    showToast(fragmentContext.context!!, fragmentContext.getString(R.string.toast_light_needs_to_be_connected_locally))
                    onOffCheckBox.isChecked = onOffCheckBox.isChecked.not()*/
                    }
                }
            }

            deviceControl.setOnClickListener {
                var selectedPosition = adapterPosition
                if (selectedPosition != -1) {
                    fragmentContext.openLampControlScreen((devicesList[selectedPosition] as CustomLight))
                }
            }

            lampImage.setOnClickListener {
                var selectedPosition = adapterPosition
                if (selectedPosition != -1) {
                    fragmentContext.showRenameLightDialog((devicesList[selectedPosition] as CustomLight).light)
                }
            }

            lampImage.setOnLongClickListener {
                var selectedPosition = adapterPosition
                if (selectedPosition != -1) {
                    if(isSelfProfile) {
                        fragmentContext.openDeleteLampDialog((devicesList[selectedPosition] as CustomLight).light)
                    }
                }
                true
            }


        }

    }
}