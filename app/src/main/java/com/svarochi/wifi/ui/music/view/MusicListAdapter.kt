package com.svarochi.wifi.ui.music.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.model.common.Song

class MusicListAdapter(var context: MusicActivity) : androidx.recyclerview.widget.RecyclerView.Adapter<MusicListAdapter.SongViewHolder>() {

    private var songsList = ArrayList<Song>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SongViewHolder {
        return SongViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_song, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return songsList.size
    }

    override fun onBindViewHolder(viewHolder: SongViewHolder, position: Int) {
        viewHolder.songTitle.text = songsList[position].title
        viewHolder.songArtist.text = songsList[position].artist
    }

    fun addSongs(songsList: ArrayList<Song>) {
        this.songsList = songsList
    }


    inner class SongViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var songTitle = itemView.findViewById<TextView>(R.id.tv_songTitle)
        var songArtist = itemView.findViewById<TextView>(R.id.tv_artistName)
        var songLayout = itemView.findViewById<LinearLayout>(R.id.ll_songLayout)

        init {
            songLayout.setOnClickListener {
                var position = adapterPosition
                context.playSong(position)
            }
        }
    }

}