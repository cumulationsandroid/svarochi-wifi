package com.svarochi.wifi.ui.userSession.activities

import android.os.Bundle
import android.text.TextUtils
import android.view.View

import bizbrolly.svarochiapp.R
import com.svarochi.wifi.common.AppUtils
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.common.CommonUtils
import com.svarochi.wifi.preference.Preferences
import com.svarochi.wifi.common.Constants
import kotlinx.android.synthetic.main.activity_help.*

class HelpActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        init()
    }

    private fun init() {
        setListeners()
        setDefaults()
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setDefaults() {
        val userEmail = Preferences.getInstance(this).email
        val userPhone = Preferences.getInstance(this).phone
        tvUserProfile.text = userEmail + (if (!(TextUtils.isEmpty(userEmail) || TextUtils.isEmpty(userPhone))) " / " else "") + userPhone
        tvUserProfileType.text = getString(R.string.as_a_master_operator_profile, if (Preferences.getInstance(this).isSlaveUser) getString(R.string.operator) else getString(R.string.master))
        tvAppVersion.text = getString(R.string.version) + AppUtils.getAppVerionName(this@HelpActivity)
    }

    fun actionTutorialYoutube(view: View) {
        CommonUtils.watchYoutubeVideo(this, Constants.YOUTUBE)
    }

    fun actionTutorialHowItWorks(view: View) {
        CommonUtils.watchYoutubeVideo(this, Constants.YOUTUBE_HOW_IT_WORKS)
    }

    fun actionTutorialSupport(view: View) {
        CommonUtils.viewBrowser(this, getString(R.string.svarochi_support))
    }

    fun actionHardReset(view: View) {
//        startActivity(Intent(this@HelpActivity, HardResetActivity::class.java))
    }

    fun actionContactByEmail(view: View) {
        CommonUtils.openEmail(this, getString(R.string.contact_email))
    }

    fun actionContactByPhone(view: View) {
        CommonUtils.openDialer(this, getString(R.string.toll_free))
    }

    fun actionContactByWebsite(view: View) {
        CommonUtils.viewBrowser(this, getString(R.string.contact_website))
    }
}
