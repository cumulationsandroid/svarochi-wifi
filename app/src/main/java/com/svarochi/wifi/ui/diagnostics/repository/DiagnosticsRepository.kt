package com.svarochi.wifi.ui.diagnostics.repository

import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.service.communication.CommunicationService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Predicate
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DiagnosticsRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl {
    /**
     * udpBroadcast - Used to check know whether UDP broadcasting is taking place
     * */
    private var udpBroadcast: Boolean = false
    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var communicationService: CommunicationService = application.wifiCommunicationService
    private var TAG = "DiagnosticsRepository"
    private var broadcastDisposable: Disposable? = null

    init {
        // Observe the responseObservable in service
        initCommunicationResponseObserver()
    }

    override fun initCommunicationResponseObserver() {
        communicationService.getResponseObservable()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Timber.d("$TAG - Listening to responseObservable of service...")
                    disposables.add(it!!)
                }
                .subscribe(object : DisposableObserver<Event>() {
                    override fun onComplete() {
                    }

                    override fun onNext(event: Event) {
                        responseObservable.onNext(event)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                    }
                })
    }

    override fun getCommunicationResponseObserver(): PublishSubject<Event> = responseObservable

    override fun dispose() {
        Timber.d("$TAG - Disposing observers")
        disposables = disposeDisposables(disposables)
    }

    fun startUdpBroadcast(macId: String) {
        if (communicationService.isDeviceLocallyAvailable(macId)) {
            Timber.d("$TAG - $macId is locally available")

            communicationService.getLightStatusFromDevice(macId, true)
        } else {
            Timber.d("$TAG - $macId is not locally available, so performing UDP broadcast")
            getRouterBroadcastAddress()
        }
    }

    fun stopUdpBroadcast() {
        udpBroadcast = false
        Timber.d("$TAG - UDP broadcasting stopped")
        communicationService.stopBroadcast()
        disposeBroadcastDisposable()
    }

    private fun disposeBroadcastDisposable() {
        if (broadcastDisposable != null) {
            Timber.d("$TAG - Disposing broadcasting disposable ")
            broadcastDisposable!!.dispose()
        } else {
            // Should not occur
            Timber.d("$TAG - Broadcast disposable is null")
        }
    }

    private fun getRouterBroadcastAddress() {
        Utilities.findRouterBCastAddress()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Timber.d("$TAG - Finding router broadcast address")
                }
                .subscribe(object : DisposableObserver<String>() {
                    override fun onComplete() {
                    }

                    override fun onNext(bcastAddress: String) {
                        Timber.d("$TAG - Found Router BCAST ADDRESS - $bcastAddress")
                        performBroadcast(bcastAddress)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                        responseObservable.onNext(Event(EventType.UDP_BROADCAST, EventStatus.ERROR, null, e))
                    }
                })
    }

    private fun performBroadcast(routerBcastAddress: String) {
        var broadcastFlag = true
        Observable
                .interval(0, Constants.BROADCAST_TIMEOUT, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .takeWhile(object : Predicate<Long> {
                    override fun test(t: Long): Boolean {
                        return broadcastFlag
                    }
                })
                .doOnSubscribe {
                    // Using a separate disposable to clear broadcast observer
                    broadcastDisposable = it
                }
                .subscribe(object : DisposableObserver<Long>() {
                    override fun onComplete() {
                        stopUdpBroadcast()
                        responseObservable.onNext(Event(EventType.UDP_BROADCAST, EventStatus.COMPLETE, null, null))
                    }

                    override fun onNext(t: Long) {
                        udpBroadcast = true
                        communicationService.sendBroadcast(
                                routerBcastAddress,
                                Constants.DEFAULT_BCAST_PORT,
                                null
                        )
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                        udpBroadcast = false
                    }

                })
    }

    fun checkIfDeviceIsAvailableViaMqtt(macId: String) {
        Timber.d("$TAG - Checking if $macId is available via MQTT")
        communicationService.checkIfDeviceIsAvailableViaMqtt(macId)
    }

}