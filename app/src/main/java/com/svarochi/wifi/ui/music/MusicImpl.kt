package com.svarochi.wifi.ui.music

interface MusicImpl {
    fun setGroupRgbWithBrightness(groupId: Long, r_value: Int, g_value: Int, b_value: Int, brightness_value:Int)

    fun setLightRgbWithBrightness(macId: String, r_value: Int, g_value: Int, b_value: Int, brightness_value:Int)
}