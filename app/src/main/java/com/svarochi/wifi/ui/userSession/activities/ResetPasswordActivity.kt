package com.svarochi.wifi.ui.userSession.activities

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.EditorInfo
import bizbrolly.svarochiapp.R
import com.akkipedia.skeleton.utils.GeneralUtils
import com.bizbrolly.WebServiceRequests
import com.bizbrolly.entities.CommonResponse
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.common.CommonUtils
import com.svarochi.wifi.common.DialogUtils
import kotlinx.android.synthetic.main.activity_reset_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResetPasswordActivity : BaseActivity() {
    private var emailOrPhoneNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        init()
    }

    private fun init() {
        setBundleData()
        setListeners()
    }

    private fun setBundleData() {
        if (intent != null && intent.extras != null) {
            val bundle = intent.extras
            if (bundle!!.containsKey("EmailOrPhoneNumber")) {
                emailOrPhoneNumber = bundle.getString("EmailOrPhoneNumber")
            }
        }
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        et_confirm_password.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateResetPassword()
            }
            false
        }
    }

    fun actionNewPasswordHint(view: View) {
        if (iv_new_password_hint.tag != null && "close".equals(iv_new_password_hint.tag.toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(et_new_password.text.toString().trim())) {
                et_new_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                iv_new_password_hint.setImageResource(R.drawable.icon_eye)
                iv_new_password_hint.tag = "open"
                et_new_password.setSelection(et_new_password.text?.length!!)
            }
        } else {
            et_new_password.transformationMethod = PasswordTransformationMethod.getInstance()
            iv_new_password_hint.setImageResource(R.drawable.icon_closed_eye)
            iv_new_password_hint.tag = "close"
            et_new_password.setSelection(et_new_password.text?.length!!)
        }
    }

    fun actionConfirmPasswordHint(view: View) {
        if (iv_confirm_password_hint.tag != null && "close".equals(iv_confirm_password_hint.tag.toString(), ignoreCase = true)) {
            if (!TextUtils.isEmpty(et_confirm_password.text.toString().trim())) {
                et_confirm_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                iv_confirm_password_hint.setImageResource(R.drawable.icon_eye)
                iv_confirm_password_hint.tag = "open"
                et_confirm_password.setSelection(et_confirm_password.text?.length!!)
            }
        } else {
            et_confirm_password.transformationMethod = PasswordTransformationMethod.getInstance()
            iv_confirm_password_hint.setImageResource(R.drawable.icon_closed_eye)
            iv_confirm_password_hint.tag = "close"
            et_confirm_password.setSelection(et_confirm_password.text?.length!!)
        }
    }

    fun actionResetPassword(view: View) {
        validateResetPassword()
    }

    private fun validateResetPassword() {
        val newPassword = et_new_password.text.toString().trim()
        val confirmPassword = et_confirm_password.text.toString().trim()

        if (TextUtils.isEmpty(newPassword)) {
            showToast(getString(R.string.please_enter_new_password))
            return
        } else if (newPassword.length < 6) {
            showToast(getString(R.string.password_must_have_atleast_six_characters))
            return
        } else if (!newPassword.matches(".*\\d+.*".toRegex())) {
            showToast(getString(R.string.password_must_have_atleast_one_number))
            return
        } else if (TextUtils.isEmpty(confirmPassword)) {
            showToast(getString(R.string.please_enter_confirm_password))
            return
        } else if (newPassword != confirmPassword) {
            showToast(getString(R.string.password_and_confirm_password_should_not_mismatch))
            return
        } else {
            CommonUtils.hideSoftKeyboard(this)
            requestSendOtp()
        }
    }

    private fun requestSendOtp() {
        if (!GeneralUtils.isInternetAvailable(this@ResetPasswordActivity)) {
            showToast(getString(R.string.no_internet))
            return
        }
        showProgressDialog()
        WebServiceRequests.getInstance().sendOtp(emailOrPhoneNumber, 2, object : Callback<CommonResponse> {
            override fun onResponse(call: Call<CommonResponse>, response: Response<CommonResponse>) {
                CommonUtils.hideSoftKeyboard(this@ResetPasswordActivity)
                hideProgressDialog()
                if (response?.body() != null && response.body()!!.dbDetailsResult != null) {
                    if (response.isSuccessful && response.body()!!.dbDetailsResult.isResult) {
                        var message = "An OTP has been sent"
                        if (!TextUtils.isEmpty(emailOrPhoneNumber) && !emailOrPhoneNumber!!.matches("\\d+".toRegex())) {
                            message = getString(R.string.otp_has_been_sent_to_this_email, emailOrPhoneNumber)
                        } else if (!TextUtils.isEmpty(emailOrPhoneNumber) && emailOrPhoneNumber!!.matches("\\d+".toRegex())) {
                            message = getString(R.string.otp_has_been_sent_to_this_phone, emailOrPhoneNumber)
                        }

                        DialogUtils.showDefaultAlertMessage(this@ResetPasswordActivity, "", message, getString(R.string.ok)) {
                            val intent = Intent(this@ResetPasswordActivity, VerifyTokenActivity::class.java)
                            val bundle = Bundle()
                            bundle.putString("Action", "ResetPassword")
                            bundle.putString("EmailOrPhoneNumber", emailOrPhoneNumber)
                            bundle.putString("NewPassword", et_new_password.getText().toString().trim())
                            intent.putExtras(bundle)
                            startActivity(intent)
                        }
                    } else {
                        if (response.body()!!.dbDetailsResult.errorDetail != null && response.body()!!.dbDetailsResult.errorDetail.errorDetails != null) {
                            //Showing phone number dialog again if phone number related error shown
                            showToast(response.body()!!.dbDetailsResult.errorDetail.errorDetails)
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                } else {
                    showToast(getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                hideProgressDialog()
                if (t.message?.contains("Unable to resolve host")!!) {
                    showToast(getString(R.string.no_internet))
                } else {
                    showToast(getString(R.string.something_went_wrong))
                }
            }
        })
    }
}
