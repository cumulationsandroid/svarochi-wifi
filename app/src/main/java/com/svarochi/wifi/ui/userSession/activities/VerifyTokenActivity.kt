package com.svarochi.wifi.ui.userSession.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View

import com.akkipedia.skeleton.utils.GeneralUtils
import com.bizbrolly.WebServiceRequests
import com.bizbrolly.entities.CommonResponse
import com.bizbrolly.entities.SendTokenResponse
import com.bizbrolly.entities.VerifyTokenResponse

import bizbrolly.svarochiapp.R
import com.svarochi.wifi.ui.userSession.BaseActivity
import com.svarochi.wifi.common.CommonUtils
import kotlinx.android.synthetic.main.activity_verify_token.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerifyTokenActivity : BaseActivity() {
    private var otp = ""
    private var email: String? = null
    private var phoneNumber: String? = null
    private var password: String? = null
    private var emailOrPhoneNumber: String? = null
    private var action: String? = ""
    private var resendCount = 0

    // variable to track event time
    private var mLastClickTime: Long = 0
    private var deleteKeyListener: View.OnKeyListener = View.OnKeyListener { v, keyCode, event ->
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTime < 100) {
            return@OnKeyListener false
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            when (v.id) {
                R.id.etOtp1 -> {
                    Log.e(TAG, "etOtp1")
                    when {
                        etOtp4.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp4.setText("")
                            }
                            etOtp4.requestFocus()
                        }
                        etOtp3.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp3.setText("")
                            }
                            etOtp3.requestFocus()
                        }
                        etOtp2.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp2.setText("")
                            }
                            etOtp2.requestFocus()
                        }
                        etOtp1.text?.length!! > 0 -> {
                            if (etOtp1.text?.length!! == 0) {
                                etOtp1.setText("")
                            }
                            etOtp1.requestFocus()
                        }
                    }
                }
                R.id.etOtp2 -> {
                    Log.e(TAG, "etOtp2")
                    if (etOtp2.text?.length!! == 0) {
                        etOtp1.setText("")
                    }
                    etOtp1.requestFocus()
                }
                R.id.etOtp3 -> {
                    Log.e(TAG, "etOtp3")
                    if (etOtp3.text?.length!! == 0) {
                        etOtp2.setText("")
                    }
                    etOtp2.requestFocus()
                }
                R.id.etOtp4 -> {
                    Log.e(TAG, "etOtp4")
                    if (etOtp4.text?.length!! == 0) {
                        etOtp3.setText("")
                    }
                    etOtp3.requestFocus()
                }
            }
        }
        false
    }

    private var otpTextWatcher: TextWatcher = object : TextWatcher {

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            Log.e(TAG, "onTextChanged")
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            Log.e(TAG, "beforeTextChanged")
        }

        override fun afterTextChanged(s: Editable) {
            Log.e(TAG, "afterTextChanged")
            if (etOtp1.text.hashCode() == s.hashCode()) {
                //etOtp1
                if (etOtp1.length() > 0) {
                    if (etOtp2.length() > 0) {
                        if (etOtp3.length() > 0) {
                            if (etOtp4.length() > 0) {
                                Log.e(TAG, "etOtp1")
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                if (!TextUtils.isEmpty(action) && action!!.equals("ResetPassword", ignoreCase = true)) {
                                    requestResetPassword()
                                } else {
                                    requestVerifyToken()
                                }
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp3.requestFocus()
                        }
                    } else {
                        etOtp2.requestFocus()
                    }
                } else {
                    etOtp1.requestFocus()
                }
            } else if (etOtp2.text.hashCode() == s.hashCode()) {
                //etOtp2
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        if (etOtp3.length() > 0) {
                            if (etOtp4.length() > 0) {
                                Log.e(TAG, "etOtp2")
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                if (!TextUtils.isEmpty(action) && action!!.equals("ResetPassword", ignoreCase = true)) {
                                    requestResetPassword()
                                } else {
                                    requestVerifyToken()
                                }
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp3.requestFocus()
                        }
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else if (etOtp3.text.hashCode() == s.hashCode()) {
                //etOtp3
                if (etOtp3.length() > 0) {
                    if (etOtp2.length() > 0) {
                        if (etOtp1.length() > 0) {
                            if (etOtp4.length() > 0) {
                                Log.e(TAG, "etOtp3")
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                if (!TextUtils.isEmpty(action) && action!!.equals("ResetPassword", ignoreCase = true)) {
                                    requestResetPassword()
                                } else {
                                    requestVerifyToken()
                                }
                            } else {
                                etOtp4.requestFocus()
                            }
                        } else {
                            etOtp1.requestFocus()
                        }
                    } else {
                        if (etOtp1.length() > 0) {
                            etOtp2.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    }
                } else {
                    if (etOtp2.length() > 0) {
                        if (etOtp1.length() > 0) {
                            etOtp3.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    } else {
                        if (etOtp1.length() > 0) {
                            etOtp2.requestFocus()
                        } else {
                            etOtp1.requestFocus()
                        }
                    }
                }
            } else if (etOtp4.text.hashCode() == s.hashCode()) {
                //etOtp4
                if (etOtp4.length() > 0) {
                    if (etOtp3.length() > 0) {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                Log.e(TAG, "etOtp4")
                                otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                                if (!TextUtils.isEmpty(action) && action!!.equals("ResetPassword", ignoreCase = true)) {
                                    requestResetPassword()
                                } else {
                                    requestVerifyToken()
                                }
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    } else {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp3.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    }
                } else {
                    if (etOtp3.length() > 0) {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                Log.e(TAG, "etOtp4 else")
                                etOtp4.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    } else {
                        if (etOtp2.length() > 0) {
                            if (etOtp1.length() > 0) {
                                etOtp3.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        } else {
                            if (etOtp1.length() > 0) {
                                etOtp2.requestFocus()
                            } else {
                                etOtp1.requestFocus()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_token)
        Log.e(TAG, "VerifyTokenActivity onCreate")
        init()
    }

    private fun init() {
        setBundleData()
        setListeners()
    }

    private fun setBundleData() {
        if (intent != null && intent.extras != null) {
            val bundle = intent.extras
            if (bundle!!.containsKey("Action") && bundle.getString("Action")!!.equals("ResetPassword", ignoreCase = true)) {
                //Reset Password
                action = bundle.getString("Action")
                if (bundle.containsKey("EmailOrPhoneNumber")) {
                    emailOrPhoneNumber = bundle.getString("EmailOrPhoneNumber")
                }
                if (bundle.containsKey("NewPassword")) {
                    password = bundle.getString("NewPassword")
                }
            } else {
                //Action for secret code
                if (bundle.containsKey("Email")) {
                    email = bundle.getString("Email")
                }
                if (bundle.containsKey("Phone")) {
                    phoneNumber = bundle.getString("Phone")
                }
                if (bundle.containsKey("Password")) {
                    password = bundle.getString("Password")
                }
            }
        }
    }

    private fun setListeners() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        etOtp1.setOnKeyListener(deleteKeyListener)
        etOtp2.setOnKeyListener(deleteKeyListener)
        etOtp3.setOnKeyListener(deleteKeyListener)
        etOtp4.setOnKeyListener(deleteKeyListener)

        etOtp1.addTextChangedListener(otpTextWatcher)
        etOtp2.addTextChangedListener(otpTextWatcher)
        etOtp3.addTextChangedListener(otpTextWatcher)
        etOtp4.addTextChangedListener(otpTextWatcher)

        etOtp1.requestFocus()
    }

    private fun validateOTP(): String {
        var otp = ""
        if (etOtp4.length() > 0) {
            if (etOtp3.length() > 0) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        otp = etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        etOtp3.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            }
        } else {
            if (etOtp3.length() > 0) {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {

                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            } else {
                if (etOtp2.length() > 0) {
                    if (etOtp1.length() > 0) {
                        etOtp3.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                } else {
                    if (etOtp1.length() > 0) {
                        etOtp2.requestFocus()
                    } else {
                        etOtp1.requestFocus()
                    }
                }
            }
        }

        return otp
    }

    fun actionResendOTP(view: View) {
        if (resendCount >= 3) {
            showToast(getString(R.string.you_have_reached_to_the_resend_otp_limit))
            return
        }
        if (!GeneralUtils.isInternetAvailable(this@VerifyTokenActivity)) {
            showToast(getString(R.string.no_internet))
            return
        }
        clearOTP()
        showProgressDialog(getString(R.string.resending_otp), getString(R.string.please_wait_))
        ++resendCount

        if (!TextUtils.isEmpty(action) && action!!.equals("ResetPassword", ignoreCase = true)) {
            WebServiceRequests.getInstance().sendOtp(emailOrPhoneNumber, 2, object : Callback<CommonResponse> {
                override fun onResponse(call: Call<CommonResponse>, response: Response<CommonResponse>) {
                    CommonUtils.hideSoftKeyboard(this@VerifyTokenActivity)
                    hideProgressDialog()
                    if (resendCount == 3) {
                        //llResendOtp.setEnabled(false);
                        llResendOtp.alpha = 0.5f
                    }
                    if (response?.body() != null && response.body()!!.dbDetailsResult != null) {
                        if (response.isSuccessful && response.body()!!.dbDetailsResult.isResult) {
                            var message = "OTP has been sent"
                            if (!TextUtils.isEmpty(emailOrPhoneNumber) && !emailOrPhoneNumber!!.matches("\\d+".toRegex())) {
                                message = getString(R.string.otp_has_been_sent_to_this_email, emailOrPhoneNumber)
                            } else if (!TextUtils.isEmpty(emailOrPhoneNumber) && !emailOrPhoneNumber!!.matches("\\d+".toRegex())) {
                                message = getString(R.string.otp_has_been_sent_to_this_phone, emailOrPhoneNumber)
                            }
                            showToast(message)
                        } else {
                            if (response.body()!!.dbDetailsResult.errorDetail != null && response.body()!!.dbDetailsResult.errorDetail.errorDetails != null) {
                                showToast(response.body()!!.dbDetailsResult.errorDetail.errorDetails)
                            } else {
                                showToast(getString(R.string.something_went_wrong))
                            }
                        }
                    } else {
                        showToast(getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                    hideProgressDialog()
                    if (t.message?.contains("Unable to resolve host")!!) {
                        showToast(getString(R.string.no_internet))
                    } else {
                        showToast(getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            WebServiceRequests.getInstance().sendToken(email, phoneNumber, password, object : Callback<SendTokenResponse> {
                override fun onResponse(call: Call<SendTokenResponse>, response: Response<SendTokenResponse>) {
                    if (resendCount == 3) {
                        //llResendOtp.setEnabled(false);
                        llResendOtp.alpha = 0.5f
                    }
                    hideProgressDialog()
                    if (response?.body() != null && response.body()!!.getDBDetailsResult != null) {
                        if (response.isSuccessful && response.body()!!.getDBDetailsResult.isResult) {
                            if (!TextUtils.isEmpty(email) && TextUtils.isEmpty(phoneNumber)) {
                                showToast(getString(R.string.otp_has_been_sent_to_this_email, email))
                            } else if (TextUtils.isEmpty(email) && !TextUtils.isEmpty(phoneNumber)) {
                                showToast(getString(R.string.otp_has_been_sent_to_this_phone, phoneNumber))
                            } else if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(phoneNumber)) {
                                showToast(getString(R.string.otp_has_been_sent_to_this_email_phone, email, phoneNumber))
                            }
                        } else {
                            if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorMessage != null) {
                                showToast(response.body()!!.getDBDetailsResult.errorDetail.errorMessage)
                            } else {
                                showToast(getString(R.string.something_went_wrong))
                            }
                        }
                    } else {
                        showToast(getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<SendTokenResponse>, t: Throwable) {
                    hideProgressDialog()
                    if (t.message?.contains("Unable to resolve host")!!) {
                        showToast(getString(R.string.no_internet))
                    } else {
                        showToast(getString(R.string.something_went_wrong))
                    }
                }
            })
        }
    }

    private fun requestVerifyToken() {
        Log.e(TAG, "requestVerifyToken")
        val OTP = validateOTP()
        if (TextUtils.isEmpty(OTP) || OTP.length < 4) {
            showToast(getString(R.string.invalid_otp))
            return
        }
        etOtp4.requestFocus()
        if (!GeneralUtils.isInternetAvailable(this)) {
            showToast(getString(R.string.no_internet))
            return
        }
        CommonUtils.hideSoftKeyboard(this@VerifyTokenActivity)
        showProgressDialog(getString(R.string.validating_otp), getString(R.string.please_wait_))
        WebServiceRequests.getInstance().verifyToken(email, phoneNumber, otp.trim { it <= ' ' },
                object : Callback<VerifyTokenResponse> {
                    override fun onResponse(call: Call<VerifyTokenResponse>, response: Response<VerifyTokenResponse>) {
                        Log.e(TAG, "response verifyToken")
                        hideProgressDialog()
                        if (response.body() != null) {
                            if (response.body()!!.getDBDetailsResult.isResult) {
                                val intent = Intent(this@VerifyTokenActivity, SetSecretCodeActivity::class.java)
                                val bundle = Bundle()
                                bundle.putString("Email", email)
                                bundle.putString("Phone", phoneNumber)
                                intent.putExtras(bundle)
                                startActivity(intent)
                                finish()
                            } else {
                                if (response.body()!!.getDBDetailsResult.errorDetail != null && response.body()!!.getDBDetailsResult.errorDetail.errorDetails != null) {
                                    showToast(response.body()!!.getDBDetailsResult.errorDetail.errorDetails)
                                } else {
                                    showToast(getString(R.string.something_went_wrong))
                                }
                                clearOTP()
                            }
                        } else {
                            clearOTP()
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }

                    override fun onFailure(call: Call<VerifyTokenResponse>, t: Throwable) {
                        hideProgressDialog()
                        clearOTP()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                }
        )
    }

    private fun requestResetPassword() {
        Log.e(TAG, "requestResetPassword")
        val OTP = validateOTP()
        if (TextUtils.isEmpty(OTP) || OTP.length < 4) {
            showToast(getString(R.string.invalid_otp))
            return
        }
        etOtp4.requestFocus()
        if (!GeneralUtils.isInternetAvailable(this)) {
            showToast(getString(R.string.no_internet))
            return
        }
        CommonUtils.hideSoftKeyboard(this@VerifyTokenActivity)
        showProgressDialog(getString(R.string.validating_otp), getString(R.string.please_wait_))
        WebServiceRequests.getInstance().resetPassword(emailOrPhoneNumber, otp.trim { it <= ' ' }, password,
                object : Callback<CommonResponse> {
                    override fun onResponse(call: Call<CommonResponse>, response: Response<CommonResponse>) {
                        Log.e(TAG, "response resetPassword")
                        hideProgressDialog()
                        if (response?.body() != null && response.body()!!.dbDetailsResult != null) {
                            if (response.body()!!.dbDetailsResult.isResult) {
                                showToast(getString(R.string.password_updated_successfully))
                                val intent = Intent(this@VerifyTokenActivity, LoginActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                val bundle = Bundle()
                                bundle.putBoolean(LoginActivity.BUNDLE_IS_SLAVE_USER, false)
                                intent.putExtras(bundle)
                                startActivity(intent)
                                finish()
                            } else {
                                if (response.body()!!.dbDetailsResult.errorDetail != null && response.body()!!.dbDetailsResult.errorDetail.errorDetails != null) {
                                    showToast(response.body()!!.dbDetailsResult.errorDetail.errorDetails)
                                } else {
                                    showToast(getString(R.string.something_went_wrong))
                                }
                                clearOTP()
                            }
                        } else {
                            clearOTP()
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }

                    override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                        hideProgressDialog()
                        clearOTP()
                        if (t.message?.contains("Unable to resolve host")!!) {
                            showToast(getString(R.string.no_internet))
                        } else {
                            showToast(getString(R.string.something_went_wrong))
                        }
                    }
                }
        )
    }

    private fun clearOTP() {
        try {
            etOtp4.setText("")
            etOtp3.setText("")
            etOtp2.setText("")
            etOtp1.setText("")
            Handler().postDelayed({
                etOtp1.requestFocus()
                CommonUtils.showKeyBoard(this@VerifyTokenActivity)
                CommonUtils.showKeyBoard(this@VerifyTokenActivity, etOtp1)
            }, 400)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {
        private val TAG = VerifyTokenActivity::class.java.simpleName
    }
}
