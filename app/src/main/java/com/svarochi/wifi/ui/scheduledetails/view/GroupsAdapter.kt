package com.svarochi.wifi.ui.scheduledetails.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.model.common.CustomGroup

class GroupsAdapter(var groupsList: ArrayList<CustomGroup>, var context: ScheduleActivity) :
        androidx.recyclerview.widget.RecyclerView.Adapter<GroupsAdapter.GroupViewHolder>() {

    var selectedIds = ArrayList<Long>()
    var selectedGroupNames = ArrayList<String>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): GroupViewHolder {
        var view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_new_group_light, viewGroup, false)
        return GroupViewHolder(view)
    }

    override fun getItemCount(): Int {
        return groupsList.size
    }

    override fun onBindViewHolder(viewHolder: GroupViewHolder, position: Int) {
        viewHolder.name.text = groupsList[position].group.name
        viewHolder.checkBox.isChecked = groupsList[position].isSelected

        if (groupsList[position].isSelected) {
            if (selectedIds.contains(groupsList[position].group.id!!).not()) {
                selectedIds.add(groupsList[position].group.id!!)
                selectedGroupNames.add(groupsList[position].group.name)
            }
        } else {
            if (selectedIds.contains(groupsList[position].group.id!!)) {
                selectedIds.remove(groupsList[position].group.id!!)
                selectedGroupNames.remove(groupsList[position].group.name)
            }
        }
    }

    inner class GroupViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.tv_lightName)
        var checkBox: CheckBox = itemView.findViewById(R.id.cb_selectLightCheckBox)
        var setLightButton: Button = itemView.findViewById(R.id.bt_setLight)


        init {
            checkBox.setOnClickListener {
                var position = adapterPosition
                groupsList[position].isSelected = checkBox.isChecked

                if (groupsList[position].isSelected) {
                    if (selectedIds.contains(groupsList[position].group.id!!).not()) {
                        selectedIds.add(groupsList[position].group.id!!)
                        selectedGroupNames.add(groupsList[position].group.name)
                    }
                } else {
                    if (selectedIds.contains(groupsList[position].group.id!!)) {
                        selectedIds.remove(groupsList[position].group.id!!)
                        selectedGroupNames.remove(groupsList[position].group.name)
                    }
                }
            }

            setLightButton.visibility = View.GONE
        }
    }
}