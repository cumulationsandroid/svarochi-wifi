package com.svarochi.wifi.ui.scheduledetails.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import bizbrolly.svarochiapp.R


class DaySelectionAdapter(var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dayList = inflateDays()
    var SELECTED_DAY = 1
    var UNSELECTED_DAY = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == SELECTED_DAY) {
            var view = LayoutInflater.from(parent.context).inflate(R.layout.item_selected_day, parent, false)
            return SelectedDayViewHolder(view)
        }
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_unselected_day, parent, false)
        return UnSelectedDayViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        if (dayList[position].isSelected) {
            return SELECTED_DAY
        }
        return UNSELECTED_DAY
    }

    override fun getItemCount(): Int {
        return 7
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (dayList[position].isSelected) {
            (holder as SelectedDayViewHolder).bind(dayList[position])
        } else {
            (holder as UnSelectedDayViewHolder).bind(dayList[position])
        }
    }


    inner class SelectedDayViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var dayName: TextView

        init {
            dayName = view.findViewById(R.id.tv_dayName)

            dayName.setOnClickListener {
                var selectedPosition = adapterPosition
                dayList[selectedPosition].isSelected = false
                notifyDataSetChanged()
            }
        }

        fun bind(day: Day) {
            dayName.text = day.name
        }
    }

    inner class UnSelectedDayViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var dayName: TextView

        init {
            dayName = view.findViewById(R.id.tv_dayName)

            dayName.setOnClickListener {
                var selectedPosition = adapterPosition
                dayList[selectedPosition].isSelected = true
                notifyDataSetChanged()
            }
        }

        fun bind(day: Day) {
            dayName.text = day.name
        }
    }

    private fun inflateDays(): ArrayList<Day> {
        var dayNamesList = context.resources.getStringArray(R.array.array_day_names)
        val dayIdList = listOf("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday")
        var dayList = ArrayList<Day>()
        var i = 0
        dayNamesList.forEach {
            var newDay = Day(dayIdList[i++], it, false)
            dayList.add(newDay)
        }
        return dayList
    }

    fun getSelectedDays(): ArrayList<String> {
        var selectedDays = ArrayList<String>()
        dayList.forEach {
            if (it.isSelected) {
                selectedDays.add(it.id)
            }
        }
        return selectedDays
    }

    fun setSelectedDays(selectedDayList: ArrayList<String>) {
        dayList.forEach {
            if (selectedDayList.contains(it.id)) {
                it.isSelected = true
            }
        }
        notifyDataSetChanged()
    }
}