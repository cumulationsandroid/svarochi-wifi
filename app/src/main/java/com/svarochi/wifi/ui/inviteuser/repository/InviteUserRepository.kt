package com.svarochi.wifi.ui.inviteuser.repository

import androidx.lifecycle.LiveData
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.svarochi.wifi.base.BaseRepository
import com.svarochi.wifi.base.BaseRepositoryImpl
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.database.Converters
import com.svarochi.wifi.database.entity.Network
import com.svarochi.wifi.database.entity.Profile
import com.svarochi.wifi.database.entity.ProfileProject
import com.svarochi.wifi.database.entity.Project
import com.svarochi.wifi.model.api.common.SharedWithData
import com.svarochi.wifi.model.api.response.CreateProfileResponse
import com.svarochi.wifi.model.api.response.UpdateProfileResponse
import com.svarochi.wifi.model.common.SharingProject
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import retrofit2.Response
import timber.log.Timber

class InviteUserRepository(var application: SvarochiApplication) : BaseRepository(), BaseRepositoryImpl {
    private var sharedByUserId: String? = null
    private var responseObservable = PublishSubject.create<Event>()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var sharedPreference: SharedPreference? = null
    private var TAG = "UserInvitationsRepository ---->"

    init {
        initCommunicationResponseObserver()
        fetchIdsFromPreference()
    }

    private fun fetchIdsFromPreference() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        sharedByUserId = sharedPreference!!.getUserId()
    }

    override fun initCommunicationResponseObserver() {
        // No implementation as not communicating with device
    }

    override fun getCommunicationResponseObserver(): PublishSubject<Event> = responseObservable

    override fun dispose() {
        disposables = disposeDisposables(disposables)
    }

    fun getAllProjects(): LiveData<List<Project>> {
        return application.wifiLocalDatasource.getAllProjects()
    }

    fun getNetworksOfProject(projectId: Long): List<Network> {
        return application.wifiLocalDatasource.getNetworks(projectId)
    }

    fun getProfileProjectsOfProfile(profileId: String): LiveData<List<ProfileProject>> {
        return application.wifiLocalDatasource.getProfileProjectsOfProfile(profileId)
    }

    fun updateProfile(profileId: String, profileName: String, sharedWithData: SharedWithData, sharedBy: String, projectsList: ArrayList<SharingProject>) {
        updateProfileInServer(profileId, profileName, sharedWithData, sharedBy, projectsList)
    }

    private fun updateProfileInServer(profileId: String, profileName: String, sharedWithData: SharedWithData, sharedBy: String, projectsList: ArrayList<SharingProject>) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.updateProfile(
                            profileId,
                            profileName,
                            sharedBy,
                            sharedWithData.user_id,
                            projectsList)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<UpdateProfileResponse>>() {
                                override fun onComplete() {
                                    Timber.d("$TAG Completed update profile api")
                                }

                                override fun onNext(response: Response<UpdateProfileResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("$TAG Update Profile API response body - ${response.toString()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                Timber.d("Successfully updated profile($profileId) in server")
                                                updateProfileInLocalDb(profileId, profileName, sharedBy, sharedWithData, projectsList)
                                            } else {
                                                Timber.d("Something went wrong while updating profile($profileId) in server")

                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.UPDATE_PROFILE,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("Something went wrong while updating profile($profileId) in server")
                                            Timber.d("No response got from server")
                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("No error message received")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.UPDATE_PROFILE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.d("Something went wrong while updating profile($profileId) in server")

                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("No error message received")
                                        }

                                        responseObservable.onNext(
                                                Event(
                                                        EventType.UPDATE_PROFILE,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("Something went wrong while updating profile($profileId) in server")
                                    error.printStackTrace()
                                    Timber.e(error)

                                    responseObservable.onNext(Event(EventType.UPDATE_PROFILE, EventStatus.FAILURE, null, null))
                                }

                            })
            )
        } else {
            responseObservable.onNext(Event(EventType.UPDATE_PROFILE, EventStatus.FAILURE, NO_INTERNET_AVAILABLE, null))
        }
    }

    private fun updateProfileInLocalDb(profileId: String, profileName: String, sharedBy: String, sharedWithData: SharedWithData, projectsList: ArrayList<SharingProject>) {
        application.wifiLocalDatasource.updateProfile(profileId, profileName, sharedBy, sharedWithData.user_id, Converters.toString(sharedWithData))
        Timber.d("Successfully updated profile($profileId) in local db")

        application.wifiLocalDatasource.clearProfileProjectsOfProfile(profileId)
        Timber.d("Deleted all projects of updated profile($profileId) in local db")

        var profileProjectsToAdd = ArrayList<ProfileProject>()

        for (sharingProject in projectsList) {
            for (networkId in sharingProject.networks) {
                var profileProjectToAdd = ProfileProject()
                profileProjectToAdd.profileId = profileId
                profileProjectToAdd.projectId = sharingProject.project_id
                profileProjectToAdd.networkId = networkId
                profileProjectsToAdd.add(profileProjectToAdd)
            }
        }

        application.wifiLocalDatasource.addProfileProjects(profileProjectsToAdd)
        Timber.d("Successfully added all updated projects of profile($profileId) in local db")

        responseObservable.onNext(Event(EventType.UPDATE_PROFILE, EventStatus.SUCCESS, null, null))
    }

    fun createProfile(profileName: String, sharedWithData: SharedWithData, projectsList: ArrayList<SharingProject>) {
        addProfileInServer(profileName, sharedWithData, projectsList)
    }

    private fun addProfileInServer(profileName: String, sharedWithData: SharedWithData, projectsList: ArrayList<SharingProject>) {
        if (GeneralUtils.isInternetAvailable(application)) {
            disposables.add(
                    ApiRequestParser.createProfile(
                            profileName,
                            sharedByUserId!!,
                            sharedWithData.user_id,
                            projectsList)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(object : DisposableObserver<Response<CreateProfileResponse>>() {
                                override fun onComplete() {
                                    Timber.d("$TAG Completed create profile api")
                                }

                                override fun onNext(response: Response<CreateProfileResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body() != null) {
                                            Timber.d("$TAG Create Profile API response body - ${response.toString()}")
                                            if (response.body()!!.status.contains("success", true)) {
                                                if (response.body()!!.profile_id != null) {
                                                    Timber.d("$TAG Successfully created profile($profileName) in server")
                                                    addProfileInLocalDb(response.body()!!.profile_id!!, profileName, sharedByUserId!!, sharedWithData, projectsList)
                                                } else {
                                                    Timber.d("$TAG Something went wrong while creating profile($profileName) in server")
                                                    Timber.d("$TAG No profile id received from server")

                                                    var errorMessage = Utilities.getErrorMessage(response)
                                                    if (errorMessage != null) {
                                                        Timber.e(errorMessage)
                                                    } else {
                                                        Timber.e("$TAG No error message received")
                                                    }

                                                    responseObservable.onNext(
                                                            Event(
                                                                    EventType.CREATE_PROFILE,
                                                                    EventStatus.FAILURE,
                                                                    null,
                                                                    null
                                                            )
                                                    )
                                                }
                                            } else {
                                                Timber.d("$TAG Something went wrong while creating profile($profileName) in server")

                                                var errorMessage = Utilities.getErrorMessage(response)
                                                if (errorMessage != null) {
                                                    Timber.e(errorMessage)
                                                } else {
                                                    Timber.e("$TAG No error message received")
                                                }

                                                responseObservable.onNext(
                                                        Event(
                                                                EventType.CREATE_PROFILE,
                                                                EventStatus.FAILURE,
                                                                null,
                                                                null
                                                        )
                                                )
                                            }
                                        } else {
                                            Timber.d("$TAG Something went wrong while creating profile($profileName) in server")
                                            Timber.d("$TAG No response got from server")

                                            var errorMessage = Utilities.getErrorMessage(response)
                                            if (errorMessage != null) {
                                                Timber.e(errorMessage)
                                            } else {
                                                Timber.e("$TAG No error message received")
                                            }

                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.CREATE_PROFILE,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )

                                        }
                                    } else {
                                        Timber.d("$TAG Something went wrong while creating profile($profileName) in server")

                                        var errorMessage = Utilities.getErrorMessage(response)
                                        if (errorMessage != null) {
                                            Timber.e(errorMessage)
                                        } else {
                                            Timber.e("$TAG No error message received")
                                        }

                                        responseObservable.onNext(
                                                Event(
                                                        EventType.CREATE_PROFILE,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    Timber.d("$TAG Something went wrong while creating profile($profileName) in server")
                                    error.printStackTrace()
                                    Timber.e(error)
                                    responseObservable.onNext(
                                            Event(
                                                    EventType.CREATE_PROFILE,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }

                            })
            )
        } else {
            responseObservable.onNext(
                    Event(
                            EventType.CREATE_PROFILE,
                            EventStatus.FAILURE,
                            NO_INTERNET_AVAILABLE,
                            null
                    )
            )
        }
    }

    private fun addProfileInLocalDb(profileId: String, profileName: String, sharedByUserId: String, sharedWithData: SharedWithData, projectsList: ArrayList<SharingProject>) {
        var profileToAdd = Profile(profileId, profileName, sharedByUserId, sharedWithData.user_id, Converters.toString(sharedWithData))
        application.wifiLocalDatasource.addNewProfile(profileToAdd)
        Timber.d("Successfully created profile($profileId) in local db")

        var profileProjectsToAdd = ArrayList<ProfileProject>()

        for (sharingProject in projectsList) {
            for (networkId in sharingProject.networks) {
                var profileProjectToAdd = ProfileProject()
                profileProjectToAdd.profileId = profileId
                profileProjectToAdd.projectId = sharingProject.project_id
                profileProjectToAdd.networkId = networkId
                profileProjectsToAdd.add(profileProjectToAdd)
            }
        }

        application.wifiLocalDatasource.addProfileProjects(profileProjectsToAdd)

        Timber.d("Successfully added all new projects of profile($profileId) in local db")
        responseObservable.onNext(Event(EventType.CREATE_PROFILE, EventStatus.SUCCESS, null, null))

    }


}