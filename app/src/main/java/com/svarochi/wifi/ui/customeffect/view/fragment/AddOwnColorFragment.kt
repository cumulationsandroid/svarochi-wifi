package com.svarochi.wifi.ui.customeffect.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import bizbrolly.svarochiapp.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.svarochi.wifi.common.Constants.Companion.ITEM_CAMERA
import com.svarochi.wifi.common.Constants.Companion.ITEM_CANCEL
import com.svarochi.wifi.common.Constants.Companion.ITEM_COLOR_PALETTE
import com.svarochi.wifi.common.Constants.Companion.ITEM_PHOTO_GALLERY
import com.svarochi.wifi.ui.customeffect.view.callback.OnBottomSheetItemClickListener
import kotlinx.android.synthetic.main.bottom_sheet_custom_effect_color.*
import timber.log.Timber


class AddOwnColorFragment(var clickCallback: OnBottomSheetItemClickListener) : BottomSheetDialogFragment(),
        View.OnClickListener {
    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                ll_camera.id -> {
                    clickCallback.onBottomSheetItemClick(ITEM_CAMERA)
                }
                ll_photo_gallery.id -> {
                    clickCallback.onBottomSheetItemClick(ITEM_PHOTO_GALLERY)
                }
                ll_color_palette.id -> {
                    clickCallback.onBottomSheetItemClick(ITEM_COLOR_PALETTE)
                }
                ll_cancel.id -> {
                    clickCallback.onBottomSheetItemClick(ITEM_CANCEL)
                }
            }
        } else {
            Timber.d("View is null")
        }
    }

    companion object {
        fun newInstance(clickCallback: OnBottomSheetItemClickListener): AddOwnColorFragment {
            return AddOwnColorFragment(clickCallback)
        }
    }


    @Nullable
    override fun onCreateView(
            inflater: LayoutInflater,
            @Nullable container: ViewGroup?,
            @Nullable savedInstanceState: Bundle?
    ): View? {

// get the views and attach the listener

        return inflater.inflate(
                R.layout.bottom_sheet_custom_effect_color, container,
                false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        initClickListeners()
    }

    private fun initClickListeners() {
        ll_camera.setOnClickListener(this)
        ll_photo_gallery.setOnClickListener(this)
        ll_color_palette.setOnClickListener(this)
        ll_cancel.setOnClickListener(this)
    }
}