package com.svarochi.wifi.ui.scheduleaction.viewholder

import com.svarochi.wifi.base.BaseViewModel
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.ui.scheduleaction.repository.ScheduleActionRepository
import com.svarochi.wifi.ui.scheduledetails.view.ActionDetails

class ScheduleActionViewModel(var repository: ScheduleActionRepository) : BaseViewModel()  {

    fun showPreview(action: ActionDetails): Event {
        return repository.showPreview(action)
    }
}