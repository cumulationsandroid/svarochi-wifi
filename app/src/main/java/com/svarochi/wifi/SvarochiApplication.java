package com.svarochi.wifi;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import androidx.room.Room;

import com.akkipedia.skeleton.permissions.PermissionManager;
import com.akkipedia.skeleton.utils.GeneralUtils;
import com.bizbrolly.WebServiceRequests;
import com.bizbrolly.entities.GetUserDetailsResponse;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.splunk.mint.Mint;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import com.svarochi.wifi.base.ServiceConnectionCallback;
import com.svarochi.wifi.preference.Preferences;
import com.svarochi.wifi.common.Constants;
import com.svarochi.wifi.database.LocalDataSource;
import com.svarochi.wifi.database.WifiModuleDatabase;
import com.svarochi.wifi.factory.ViewModelFactory;
import com.svarochi.wifi.logging.TimberReleaseTree;
import com.svarochi.wifi.preference.SharedPreference;
import com.svarochi.wifi.service.communication.CommunicationService;
import com.willblaschko.android.alexa.AlexaManager;

import bizbrolly.svarochiapp.BuildConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

//import com.bugfender.sdk.Bugfender;

public class SvarochiApplication extends MultiDexApplication implements LifecycleObserver {
    public static final String TAG = SvarochiApplication.class.getSimpleName();

    //public static MeshService mService;
    //public static MeshApiMessageHandler meshApiMessageHandler;
    public static int mDisassociateUuidHash;
    public static boolean isDBMigrating = false;

    public static Bus bus;
//    private static BluetoothHelper bluetoothHelper;
    private static PermissionManager permissionManager;

    /****** Implementation for Alexa and FCM ******/
    private static AlexaManager alexaManager;

    /**********************************************/


    /******* Implementation for Wifi Module *******/
    public WifiModuleDatabase wifiDatabase;
    public LocalDataSource wifiLocalDatasource;
    public ViewModelFactory wifiViewModelFactory;
    private ServiceConnectionCallback wifiServiceConnectionCallback = null;
    public CommunicationService wifiCommunicationService = null;
    public Boolean appInBackground = false;
    Intent serviceIntent;
    boolean isServiceStartedAndBound = false;


    /**********************************************/

    public static SvarochiApplication get(Context context) {
        return (SvarochiApplication) context.getApplicationContext();
    }

    /****** Implementation for Alexa and FCM ******/
    public static AlexaManager getAlexaManager() {
        return alexaManager;
    }
    /**********************************************/

    /**
     * Requesting this API to get self user id
     * Reason for doing this because in old app there was no logic setup for logged in or registered in user's user id
     */
    public static void requestMyUserDetails(Context context, boolean noCheckForCurrentUserId) {
        if (Preferences.getInstance(context).getInt(Preferences.PREF_USER_ID) <= 0 || noCheckForCurrentUserId) {
            if (!GeneralUtils.isInternetAvailable(context)) {
                return;
            }
            String email = Preferences.getInstance(context).getEmail();
            String phone = Preferences.getInstance(context).getPhone();
            WebServiceRequests.getInstance().getUserDetails(email, phone, new Callback<GetUserDetailsResponse>() {
                @Override
                public void onResponse(Call<GetUserDetailsResponse> call, Response<GetUserDetailsResponse> response) {
                    if (response != null && response.body() != null && response.body().getUserDetailsResponseBean() != null) {
                        GetUserDetailsResponse.GetUserDetailsResponseBean userDetailsResponse = response.body().getUserDetailsResponseBean();
                        if (userDetailsResponse != null) {
                            int statusCode = userDetailsResponse.getResponseStatusCode();
                            String status = userDetailsResponse.getResponseStatus();
                            if (statusCode == 200 && status.equalsIgnoreCase("success")) {
                                Log.e(TAG, "requestMyUserDetails onResponse");
                                GetUserDetailsResponse.ResponseDataResponseBean userDetails = userDetailsResponse.getResponseData();
                                String savedEmail = Preferences.getInstance(context).getEmail();
                                String savedPhone = Preferences.getInstance(context).getPhone();
                                if (userDetails != null) {
                                    String email = TextUtils.isEmpty(userDetails.getEmail()) ? "" : userDetails.getEmail();
                                    String phone = TextUtils.isEmpty(userDetails.getPhone_Number()) ? "" : userDetails.getPhone_Number();
                                    if (email.equalsIgnoreCase(savedEmail) || phone.equalsIgnoreCase(savedPhone)) {
                                        //Logged in users email id or phone number is same then only update the user id
                                        Preferences.getInstance(context).putInt(Preferences.PREF_USER_ID, userDetails.getId());

                                        /**** Implementation for Wifi Module *****/

                                        SharedPreference wifiPreference = new SharedPreference(context);
                                        wifiPreference.setUserId(String.valueOf(userDetails.getId()));
                                        /*****************************************/
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetUserDetailsResponse> call, Throwable t) {
                    Log.e(TAG, "requestMyUserDetails onFailure");
                }
            });
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initSplunkCrashAnalytics();
        //initBugfender();
        //Log.e(TAG, "DebugDB "+DebugDB.getAddressLog());
//        BluetoothHelper.init(getApplicationContext(), true);
        //uncaughtExceptionHandler();
        // Bus must be created before we do anything else as other components want to post on it.
        bus = new Bus(ThreadEnforcer.ANY);

        /****** Implementation for Alexa and FCM ******/
        FirebaseApp.initializeApp(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        alexaManager = AlexaManager.getInstance(this);
        /**********************************************/

        /****** Implementation for Wifi Module ******/

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        Room.databaseBuilder(this, WifiModuleDatabase.class, "asd")
                .allowMainThreadQueries()
                .build();

        // Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new TimberReleaseTree());
        }

        // Database
        wifiDatabase = Room.databaseBuilder(this, WifiModuleDatabase.class, Constants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        // LocalDataSource
        wifiLocalDatasource = new LocalDataSource(wifiDatabase);

        // ViewModel Factory
        wifiViewModelFactory = new ViewModelFactory(this);
        /**********************************************/
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static void initBus() {
        // Bus must be created before we do anything else as other components want to post on it.
        bus = new Bus(ThreadEnforcer.ANY);
    }

    private void initSplunkCrashAnalytics() {
        //Mint.initAndStartSession(this, "61d3c6f0");
        //Disable network monitoring
        Mint.disableNetworkMonitoring();
        //Initialize the SDK to use the MINT Backend to transport data
        //Will start a new session if one is not active
        Mint.initAndStartSession(get(this), "70f3c36f");//ibahn analytics
        Mint.enableLogging(true);
    }

    private void uncaughtExceptionHandler() {
        Thread.UncaughtExceptionHandler crashHandler = new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                // make sure we die, otherwise the app will hang ...
                e.printStackTrace();
                if (e instanceof Exception) {
                    Mint.logException((Exception) e);
                    Mint.flush();
                }
                System.exit(0);
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        };
        Thread.setDefaultUncaughtExceptionHandler(crashHandler);
    }

    /******* Implementation for Wifi Module ********/

    ServiceConnection communicationServiceCallback = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Timber.d("Communication service connected");
            wifiCommunicationService = ((CommunicationService.LocalBinder) service).getService();
            if (wifiServiceConnectionCallback != null) {
                wifiServiceConnectionCallback.serviceConnected();
            }
            isServiceStartedAndBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Timber.d("Communication service disconnected");
            wifiCommunicationService = null;
            isServiceStartedAndBound = false;
        }
    };

    public void bindCommunicationService(ServiceConnectionCallback serviceConnectionCallback) {
        this.wifiServiceConnectionCallback = serviceConnectionCallback;
        if (wifiCommunicationService == null) {
            Timber.d("Binding communication service");
            if (!appInBackground) {
                if(serviceIntent == null){
                    serviceIntent = new Intent(this, CommunicationService.class);
                }
                startService(serviceIntent);
                bindService(serviceIntent, communicationServiceCallback, Context.BIND_AUTO_CREATE);
            } else {
                Timber.d("App in background, hence communication service is not bound");
            }
        } else {
            Timber.d("Communication service already bound");
            if (serviceConnectionCallback != null) {
                serviceConnectionCallback.serviceConnected();
            }
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onMoveToBackground() {
        appInBackground = true;
        if (wifiCommunicationService != null) {
            wifiCommunicationService.onMoveToBackground();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onMoveToForeground() {
        appInBackground = false;
        if (wifiCommunicationService != null) {
            wifiCommunicationService.onMoveToForeground();
        }
    }

    public void unbindCommunicationService() {
        if (isServiceStartedAndBound) {
            isServiceStartedAndBound = false;
            unbindService(communicationServiceCallback);
            stopService(serviceIntent);
            wifiCommunicationService = null;
            serviceIntent = null;
        }
    }

}