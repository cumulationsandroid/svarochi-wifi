package com.svarochi.wifi.service.communication

import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Binder
import android.os.CountDownTimer
import android.os.Handler
import android.os.IBinder
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ProcessLifecycleOwner
import com.svarochi.wifi.SvarochiApplication
import com.akkipedia.skeleton.utils.GeneralUtils
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.microsoft.azure.sdk.iot.service.devicetwin.MethodResult
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.ALREADY_EXISTS_WITH_ANOTHER_USER
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_API_COLOR
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_API_DAYLIGHT
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_API_DIMMABLE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_AUTHENTICATION_MODE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_BRIGHTNESS_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_B_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_C_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_ENCRYPTION_TYPE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_G_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_LAMP_SSID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_ON_OFF_STATUS
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_PASSWORD
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_R_VALUE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SCHEDULE_ID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SSID
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_SSID_TO_CONFIGURE
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_START_TIME
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_STOP_TIME
import com.svarochi.wifi.common.Constants.Companion.BUNDLE_W_VALUE
import com.svarochi.wifi.common.Constants.Companion.DEFAULT_LAMP_IP
import com.svarochi.wifi.common.Constants.Companion.DEFAULT_LAMP_PORT
import com.svarochi.wifi.common.Constants.Companion.DEFAULT_MAC_ID
import com.svarochi.wifi.common.Constants.Companion.JSON_BRIGHTNESS_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_B_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_C_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_FIRMWARE_VERSION
import com.svarochi.wifi.common.Constants.Companion.JSON_FRAME_NUM
import com.svarochi.wifi.common.Constants.Companion.JSON_G_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_MAC_ID
import com.svarochi.wifi.common.Constants.Companion.JSON_ON_OFF_STATUS
import com.svarochi.wifi.common.Constants.Companion.JSON_PACKET_ID
import com.svarochi.wifi.common.Constants.Companion.JSON_RSSI
import com.svarochi.wifi.common.Constants.Companion.JSON_R_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_SCENE_NUM
import com.svarochi.wifi.common.Constants.Companion.JSON_STATUS
import com.svarochi.wifi.common.Constants.Companion.JSON_W_VALUE
import com.svarochi.wifi.common.Constants.Companion.LAMP_TCP_DISCONNECTION
import com.svarochi.wifi.common.Constants.Companion.LIGHT_SYNCED
import com.svarochi.wifi.common.Constants.Companion.MAX_TCP_RETRY
import com.svarochi.wifi.common.Constants.Companion.MQTT_CONNECTION
import com.svarochi.wifi.common.Constants.Companion.MQTT_TIMEOUT
import com.svarochi.wifi.common.Constants.Companion.NO_INTERNET_AVAILABLE
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_BRIGHTNESS_RESET_VALUE_MQTT
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_BRIGHTNESS_RESET_VALUE_TCP
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_RESET_END_TIME_MQTT
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_RESET_END_TIME_TCP
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_RESET_START_TIME_MQTT
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_RESET_START_TIME_TCP
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_RESET_VALUE_MQTT
import com.svarochi.wifi.common.Constants.Companion.SCHEDULE_RESET_VALUE_TCP
import com.svarochi.wifi.common.Constants.Companion.SVAROCHI_LAMP_IDENTIFIER
import com.svarochi.wifi.common.Constants.Companion.TCP_CONNECTION
import com.svarochi.wifi.common.Constants.Companion.TCP_TIMEOUT
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.common.Utilities.Companion.convertBaseRequestToMqttJson
import com.svarochi.wifi.common.Utilities.Companion.getErrorMessage
import com.svarochi.wifi.common.Utilities.Companion.getSceneType
import com.svarochi.wifi.database.LocalDataSource
import com.svarochi.wifi.database.entity.GroupLight
import com.svarochi.wifi.database.entity.Light
import com.svarochi.wifi.model.api.response.AddDeviceResponse
import com.svarochi.wifi.model.api.response.DeleteLightResponse
import com.svarochi.wifi.model.common.DeviceBroadcast
import com.svarochi.wifi.model.common.LightStatus
import com.svarochi.wifi.model.common.LightStatusResponse
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType
import com.svarochi.wifi.model.communication.*
import com.svarochi.wifi.model.communication.commands.request.SetLampColorRequest
import com.svarochi.wifi.model.communication.commands.request.SetLampSceneRequest
import com.svarochi.wifi.model.communication.commands.request.SetWifiCredRequest
import com.svarochi.wifi.model.communication.commands.response.*
import com.svarochi.wifi.model.mqtt.MqttPacketType
import com.svarochi.wifi.model.mqtt.commands.*
import com.svarochi.wifi.network.ApiRequestParser
import com.svarochi.wifi.preference.SharedPreference
import com.svarochi.wifi.service.azuremqtt.AzureMqttCallBack
import com.svarochi.wifi.service.azuremqtt.AzureMqttHelper
import com.svarochi.wifi.udp.BroadcastCallback
import com.svarochi.wifi.udp.UdpClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Predicate
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import moe.codeest.rxsocketclient.RxSocketClient
import moe.codeest.rxsocketclient.SocketClient
import moe.codeest.rxsocketclient.SocketSubscriber
import moe.codeest.rxsocketclient.meta.SocketConfig
import moe.codeest.rxsocketclient.meta.ThreadStrategy
import org.json.JSONObject
import retrofit2.Response
import timber.log.Timber
import java.net.SocketException
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue


class CommunicationService : Service(), LifecycleObserver, BroadcastCallback {


    private var lastSentGetLightStatusSequenceNumberMqtt: Int = 0
    private lateinit var connectivityManager: ConnectivityManager
    private lateinit var wifiManager: WifiManager
    private lateinit var localDataSource: LocalDataSource
    private lateinit var sharedPreference: SharedPreference

    private var TAG = "CommunicationService ---->"

    /**
     * On an average we get 5 commands(SET_COLOR) per second when playing Music and Audio,
     * so the TIMER and RETRY_VIA_MQTT feature is disabled for the commands coming from Music and Audio activities.
     * Hence introduced the flag isMusicActivity
     *
     * */
    private var isMusicActivity = false

    private var isAppicationLive = true
    private val mBinder = LocalBinder()
    private var responseObservable = PublishSubject.create<Event>()
    private var deleteLampSequenceNumber: String? = null
    private var svarochiWifiNetwork: Network? = null
    private var wifiNetwork: Network? = null
    private var pendingSsid = ""
    private var disposables = CompositeDisposable()
    var connectedToWifi = false
    var connectedToMobileData = false

    /**** Mosquitto MQTT ****/
//    private var mqttManager: MqttManager? = null
//    var isMqttConnecting: Boolean = false
//    var isMqttConnected = false

//    private var mqttSubscribeListener = object : MqttOnSubscribeListener {
//        override fun onMqttSubscribeSuccess(macId: String) {
//            getLightStatusFromDevice(macId, false)
//        }
//    }

    /*private fun initMqttManager() {
        Timber.d("MQTT is connected = $isMqttConnected")
        Timber.d("MQTT is app in background = ${(application as SvarochiApplication).appInBackground}")
        Timber.d("MQTT is mqtt connecting = $isMqttConnecting")
        Timber.d("MQTT is wifi connected = $connectedToWifi")
        Timber.d("MQTT is mobile data = $connectedToMobileData")

        if ((application as SvarochiApplication).appInBackground.not() &&
                (mqttManager == null || isMqttConnected.not()) &&
                isMqttConnecting.not() &&
                (connectedToWifi || connectedToMobileData)
        ) {
            Timber.e("Initailising Mqtt Manager")
            isMqttConnecting = true
            mqttManager = MqttManager(this)
            mqttManager!!.connect()
            registerMqttCallback()
            mqttManager!!.setOnMqttSubscribeListener(mqttSubscribeListener)
        } else {
            if ((application as SvarochiApplication).appInBackground) {
                Timber.d("App is in background hence mqtt is not initialised")
            } else {

                Timber.e("MQTT already initialised")
            }
        }
//        azureMqttHelper.setCallBack(azureMqttCallBack)

    }*/

    /*private fun subscribeToAllDevices() {
        Timber.e("Subscribing to all devices")
        var lights = getAllLights()
        var pendingList = ArrayList<String>()
        if (lights.isNotEmpty()) {
            for (light in lights) {
                Timber.e("Subscribing to device(${light.macId})")
                pendingList.add(light.macId)
            }
            if (mqttManager != null) {
                mqttManager!!.updatePendingSubscriptionList(pendingList)
            }
        } else {
            Timber.e("No lights to subscribe")
        }
        if (mqttManager != null) {
            mqttManager!!.subscribeToPendingDevices()
        }
    }

    private fun registerMqttCallback() {
        mqttManager!!.mqttAndroidClient.setCallback(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String?) {
                Timber.e("MQTT callback - Connected to $serverURI")
                subscribeToAllDevices()
                isMqttConnected = true
                isMqttConnecting = false
            }

            override fun messageArrived(topic: String?, message: MqttMessage?) {
                Timber.e("MQTT callback - Received message from topic($topic)")
                if (String(message!!.payload).contains(SVAROCHI_LAMP_IDENTIFIER, false).not()) {
                    try {
                        var obj = JSONObject(String(message.payload))
                        Timber.e("MQTT message -  ${obj.toString()}")

                        parseResponse(obj, true)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun connectionLost(cause: Throwable?) {
                Timber.e("MQTT callback - Connection lost")
                isMqttConnected = false
                isMqttConnecting = false
                if (mqttManager != null) {
                    mqttManager!!.addAllSubscribedDevicesToPendingList()
                }
                if (isAppicationLive) {
                    Timber.e("Invalidating mqtt manager")
                    notifyMqttConnectionLost()
                } else {
                    Timber.e("Application not live for retrying MQTT connection")
                }
                cause?.printStackTrace()

            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {
                Timber.e("MQTT callback - Delivered message successfully")
            }
        })
    }*/

    /*private fun notifyMqttConnectionLost() {
        if (mqttManager != null && mqttManager!!.mqttAndroidClient != null) {
            mqttManager!!.mqttAndroidClient.setCallback(null)
            if (mqttManager!!.mqttAndroidClient.isConnected) {
                mqttManager!!.mqttAndroidClient.disconnect()
            }
            mqttManager = null
        }
        stopAllTimers()
        mqttManager = null
        isMqttConnected = false
        isMqttConnecting = false
        initMqttManager()
    }*/

    /*fun subscribeToDevices() {
        subscribeToAllDevices()
    }*/
    /**************/

    /**** Azure Mqtt ****/
    private val azureMqttHelper = AzureMqttHelper()
    private val azureMqttCallBack = object : AzureMqttCallBack {
        override fun onSuccess(result: MethodResult, payload: Map<String, Any>) {
            if (result.status in 200..300) {
                Timber.d("Azure mqtt success - ${result.payload}")
                var responseJson = JSONObject(result.payload.toString())
                parseResponse(responseJson, true)
            } else {
                if (payload[JSON_FRAME_NUM] != null && payload[JSON_FRAME_NUM] is Int) {
                    stopCountdownTimer(payload[JSON_MAC_ID] as String)
                }
            }
        }

        override fun onFailure(payload: Map<String, Any>, reason: String?) {
            Timber.d("Azure mqtt failed")
            if (reason != null) {
                Timber.d(reason)
            }
            if (payload[JSON_FRAME_NUM] != null && payload[JSON_FRAME_NUM] is Int) {
                stopCountdownTimer(payload[JSON_MAC_ID] as String)
                disconnectSocket(payload[JSON_MAC_ID] as String)
            }
        }

    }

    private fun setAzureMqttCallback() {
        azureMqttHelper.setCallBack(azureMqttCallBack)
    }

    /********************/


    /*** Udp Broadcast ***/
    var deviceBroadcastCountMap = HashMap<String, DeviceBroadcast>()
    var shouldBroadcast = false
    var broadcastDisposables = CompositeDisposable()
    var isAlreadyBroadcasting = false

    private val udpClient = UdpClient(this)

    override fun onBrodcastReceived(data: ByteArray) {
        if (data.isNotEmpty()) {
            var responsePacket =
                    BaseResponse(data)
            var responsePayload: BasePayload = responsePacket.getPayloadInstance()

            setMacIdWithIp(
                    (responsePayload as UdpBroadcastResponse).macId,
                    Utilities.convertToIp(responsePayload.ipAddress.replace(" ", "", true))
            )
        }
    }

    private fun stopReceivingBroadcast() {
        udpClient.stopReceivingBroadcast()
    }

    /*********************/

    /*** Flicker Implementation ***/
    val flickerSequenceNumber = Utilities.getRandomSequenceNumber().toInt()

    /*****************************/

    /***** TCP communication ******/
    private var lastSentGetLightStatusSequenceNumberTcp: Int = 0
    private var pendingSetWifiCommand = HashMap<String, BaseRequest>()
    var macIdClientMap =
            HashMap<String, SocketWrapper>() // For storing MACID -> SocketClient ---> Signifies that you can communicate with the device. Gives client object of the device
    //private var macIdIpAddressMap =
    //  HashMap<String, String>() // For storing MACID -> IpAddress ----? Signifies that the device has become locally available. Gives ipAddress of device
    //private var pendingCommand: BaseRequest? = null
    private var retryCount = 0


    private var turnOnRequestSequenceNumber: Int =
            0 // Storing the sequence number as we will not be able to distinguish between TURN_ON_LIGHT and SET_LAMP_COLOR
    private var turnOffRequestSequenceNumber: Int =
            0 // Storing the sequence number as we will not be able to distinguish between TURN_OFF_LIGHT and SET_LAMP_COLOR

    private var setBrightnessRequestSequenceNumber: Int = 0
    // Storing the sequence number as we will not be able to distinguish between SET_LIGHT_BRIGHTNESS and SET_LAMP_COLOR
    private var setDaylightRequestSequenceNumber: Int = 0
    // Storing the sequence number as we will not be able to distinguish between SET_LIGHT_DAYLIGHT and SET_LAMP_COLOR

    private var pendingLightStatusMap = HashMap<String, LightStatus>()
    private var lastSentCommandViaTcp = HashMap<String, BaseRequest>()
    private var countDownTimerMap = HashMap<String, LastSentCommand>()
    /*
        private var lastSentCommandMap = HashMap<String, LastSentCommand>()
    private var sequenceCommandMap = HashMap<Int, String>()


    private fun addToSequenceCommandMap(sequenceNumber: Int, macId: String) {
        Timber.e("Adding $sequenceNumber to sequence command map")
        sequenceCommandMap[sequenceNumber] = macId
    }

    private fun removeFromSequenceCommandMap(sequenceNumber: Int) {
        if (sequenceCommandMap.containsKey(sequenceNumber)) {
            Timber.e("Removing $sequenceNumber from sequence command map")
            sequenceCommandMap.remove(sequenceNumber)
        } else {
            Timber.e("No command with sequence number - $sequenceNumber")
        }
    }

     private fun getMacIdOfSequenceNumber(sequenceNumber: Int): String? {
        if (sequenceCommandMap.containsKey(sequenceNumber)) {
            return sequenceCommandMap[sequenceNumber]
        }
        Timber.e("No command for sequence number - $sequenceNumber")
        return null
    }

    */

    /*****************************/

    /**** TCP Retry implementation *****/
    private var tcpRetryMap = HashMap<String, Handler>()
    /***************************************/


    /**** Lamp deletion implementation *****/
    private val pendingLightMapType = object : TypeToken<HashMap<String, String>>() {
    }.type
    private val mapper = ObjectMapper() //
    /***************************************/


    private var mobileNetworkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            Timber.d("$TAG OnAvailable - Mobile Network")
            connectedToMobileData = true
//            addPendingDeviceIntoServer()
            Handler().postDelayed({
                addPendingDeviceIntoServer()
                deletePendingDevicesFromServer()
                flickerDevices()
            }, 1000)
//            notifyMqttConnectionLost()

            setAzureMqttCallback()
        }

        override fun onLost(network: Network?) {
            Timber.d("$TAG OnLost - Mobile Network")
            connectedToMobileData = false
        }
    }

    private fun flickerDevices() {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        val devicesToBeFlickered = sharedPreference.getAllDevicesToBeFlickered()

        if (devicesToBeFlickered.isNullOrEmpty()) {
            Timber.d("No devices to be flickered")
            return
        }
        devicesToBeFlickered?.forEach {
            sendFlickerCommands(it, false)
        }
    }

    private fun sendFlickerCommands(macId: String, viaTcp: Boolean) {
        val turnOnCommand1 = Command(EventType.TURN_ON_LIGHT, macId, false)
        val turnOffCommand = Command(EventType.TURN_OFF_LIGHT, macId, false)
        val turnOnCommand2 = Command(EventType.TURN_ON_LIGHT, macId, true)

        if (viaTcp.not()) {
            turnOnLight(turnOnCommand1, true, true)
            turnOffLight(turnOffCommand, true, true)
            turnOnLight(turnOnCommand2, true, true)
        } else {
            turnOnLight(turnOnCommand1, false, true)
            turnOffLight(turnOffCommand, false, true)
            turnOnLight(turnOnCommand2, false, true)
        }
    }

    private fun deletePendingDevicesFromServer() {
        var lightsToBeDeleted = sharedPreference.getAllDevicesToBeDeleted()

//        sharedPreference.deleteDeviceToBeDeleted("f8a2d606907a")
        if (lightsToBeDeleted != null) {
            if (lightsToBeDeleted.isNotEmpty()) {
                lightsToBeDeleted.forEach {

                    var lightToBeDeletedMap = Gson().fromJson<HashMap<String, Any>>(it.value, pendingLightMapType)
                    val lightToBeDeleted = mapper.convertValue(lightToBeDeletedMap, Light::class.java)

                    deleteLampFromServer(lightToBeDeleted.macId, lightToBeDeleted.projectId, lightToBeDeleted.networkId)
                }
            } else {
                Timber.e("No lights to be deleted from server")
            }
        } else {
            Timber.e("No lights to be deleted from server")
        }
    }

    private var wifiCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            Timber.e("OnAvailable - Wifi")
            connectedToWifi = true
            if (wifiManager.connectionInfo != null) {
                if (wifiManager.connectionInfo.ssid != null) {
                    Timber.e("$TAG Connected wifi network - ${wifiManager.connectionInfo.ssid}")
                    if (wifiManager.connectionInfo.ssid.contains(SVAROCHI_LAMP_IDENTIFIER, false)) {
                        Timber.e("$TAG Connected to Svarochi wifi network")
                        svarochiWifiNetwork = network!!

                        setWifiDetailsToPendingDevice(wifiManager.connectionInfo.ssid)
                    } else {
                        Timber.e("$TAG Device not connected to svarochi wifi network")
                        Timber.e("$TAG Not saving to svarochi wifi network")
                        svarochiWifiNetwork = null


                        Handler().postDelayed({
                            addPendingDeviceIntoServer()
                            deletePendingDevicesFromServer()
                            flickerDevices()
                        }, 1000)


                        setAzureMqttCallback()
                    }
                } else {
                    Timber.e("$TAG Connection Info Ssid is null")
                    Timber.e("$TAG Could not save wifi network")
                    svarochiWifiNetwork = null
                }
            } else {
                Timber.e("$TAG Wifi Manager Connection Info is null")
                Timber.e("$TAG Could not save wifi network")
                svarochiWifiNetwork = null
            }
            wifiNetwork = network!!

            if (shouldBroadcast) {
                performUdpBroadcast()
            }
        }

        override fun onLost(network: Network?) {
            Timber.e("$TAG OnLost - Wifi")
            wifiNetwork = null
            connectedToWifi = false
            stopBroadcast()
        }

        override fun onUnavailable() {
            Timber.e("$TAG UnAvailable - Wifi")
        }
    }

    fun addPendingDeviceIntoServer() {
        var unsyncedLights = localDataSource.getUnsyncedLights()

        if (unsyncedLights.isNotEmpty()) {
            unsyncedLights.forEach {
                insertDeviceIntoServer(it.name, it.macId, it.lampType, it.networkId, it.projectId)
            }
        } else {
            Timber.e("No unsynced devices to add to server")

        }
    }

    fun getResponseObservable() = responseObservable

    inner class LocalBinder : Binder() {
        fun getService(): CommunicationService = this@CommunicationService
    }

    override fun onBind(intent: Intent): IBinder {
        return mBinder
    }


    private fun registerMobileNetworkCallback() {
        var builder = NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR).build()
        connectivityManager.registerNetworkCallback(builder, mobileNetworkCallback)
        Timber.e("Mobile network change callback registered")
    }

    private fun unRegisterMobileNetworkCallback() {
        connectivityManager.unregisterNetworkCallback(mobileNetworkCallback)
        Timber.e("Mobile network change callback unregistered")
    }

    init {
        //macIdIpAddressMap.put(DEFAULT_MAC_ID, DEFAULT_LAMP_IP)
    }

    override fun onDestroy() {
        Timber.d("Service OnDestroy")
//        unRegisterMobileNetworkCallback()
//        unRegisterWifiCallback()
        stopReceivingBroadcast()
        stopBroadcast()
        clearDisposables()
        clearSockets()
        clearAllPendingCommands()
        clearAllPendingTcpRetries()
        stopSelf()
        super.onDestroy()
    }

    private fun clearAllPendingTcpRetries() {
        tcpRetryMap.forEach {
            it.value.removeCallbacksAndMessages(null)
        }
        tcpRetryMap.clear()
    }

    private fun clearSockets() {
        macIdClientMap.forEach {
            Timber.d("Disconnecting socket for ${it.key}")
            it.value.socket.disconnect()
        }
        macIdClientMap.clear()
    }

    private fun clearDisposables() {
        disposables.dispose()
        Timber.e("$TAG Disposed all disposables")
    }

    private fun connect(
            macId: String,
            ipAddress: String,
            port: Int
    ) {
        //if (macIdIpAddressMap[macId] != null) {
        Timber.e("Connecting to ${ipAddress}/$port")

        var client: SocketClient =
                RxSocketClient.create(
                        SocketConfig
                                .Builder()
                                .setIp(ipAddress)
                                .setPort(port)
                                .setThreadStrategy(ThreadStrategy.ASYNC)
                                .setTimeout(TCP_TIMEOUT)
                                .build()
                )

        if (macId.equals(DEFAULT_MAC_ID) && ipAddress.equals(DEFAULT_LAMP_IP) && port == DEFAULT_LAMP_PORT) {
            Timber.e("$TAG Creating a socket for default ip address")
            if (svarochiWifiNetwork != null) {

                svarochiWifiNetwork!!.bindSocket(client.mSocket)

                Timber.e("$TAG Default Socket(${client.mConfig.mIp}/${client.mConfig.mPort}) is bound to wifi network")
                svarochiWifiNetwork = null

                disposables.add(
                        client.connect()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(getConnectionObserver(client, macId), io.reactivex.functions.Consumer<Throwable> {
                                    it.printStackTrace()
                                })
                )
            } else {
                Timber.e("$TAG Svarochi wifi network is null")
                Timber.e("$TAG Default Socket(${client.mConfig.mIp}/${client.mConfig.mPort}) could not be bound to wifi network")
            }
        } else {
            if (wifiNetwork != null) {

                wifiNetwork!!.bindSocket(client.mSocket)

                Timber.e("$TAG Socket(${client.mConfig.mIp}/${client.mConfig.mPort}) is bound to wifi network")

                disposables.add(
                        client.connect()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(getConnectionObserver(client, macId), io.reactivex.functions.Consumer<Throwable> {
                                    it.printStackTrace()
                                })
                )

            } else {
                Timber.e("$TAG Wifi network is null")
                Timber.e("$TAG Socket(${client.mConfig.mIp}/${client.mConfig.mPort}) could not be bound to wifi network")

                macIdClientMap[macId]!!.socket.disconnect()
            }
        }

        //connectivityManager.requestNetwork()!!.bindSocket(client.mSocket)

        /* } else {
             // Trying to send command which isnt locally available

             Timber.e("$macId is not locally available")
             var lightName = getLampName(macId)
             responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, lightName, null))
         }*/
    }


    private fun getConnectionObserver(
            socketClient: SocketClient,
            macId: String
    ): SocketSubscriber {
        return object : SocketSubscriber() {
            override fun onConnected() {
                Timber.e("Successfully connected to ${socketClient.mConfig.mIp}/${socketClient.mConfig.mPort}")
                var socketWrapper = SocketWrapper(socketClient, System.currentTimeMillis())
                macIdClientMap.put(macId, socketWrapper)

                // Publish the device is connected
                /*deviceStatusMap.clear()
                deviceStatusMap.put(macId, true)
                isConnectedObservable.onNext(deviceStatusMap)*/

                /* if (pendingCommand != null) {
                     sendCommandViaTcp(macId, pendingCommand!!)
                 }*/

                /* Clearing pending tcp retries*/
                tcpRetryMap[macId]?.removeCallbacksAndMessages(null)


                if (pendingSetWifiCommand.isNotEmpty()) {
                    pendingSetWifiCommand.entries.forEach {
                        Timber.e("SET WIFI CREDENTIALS command sent via tcp - ${Utilities.toString(it.value.getByteArray())}")
                        sendCommandViaTcp(macId, it.value, false, true)
                        pendingSetWifiCommand.clear()
                    }
                    pendingSetWifiCommand.clear()
                } else {
                    Timber.e("$TAG No pending devices to send SET WIFI command")
                }

                if (isDeviceToBeFlickered(macId)) {
                    sendFlickerCommands(macId, true)
                } else {
                    getLightStatusFromDevice(macId, true)
                }


                responseObservable.onNext(
                        Event(
                                EventType.UDP_BROADCAST,
                                EventStatus.SUCCESS,
                                macId,
                                null
                        )
                )

                responseObservable.onNext(
                        Event(
                                EventType.CONNECTION,
                                EventStatus.SUCCESS,
                                macId,
                                null
                        )
                )
            }

            override fun onDisconnected(error: Exception?) {
                Timber.e("Disconnected")
                // Clear just socket map, becasue socket is useless now. But ipAddress of device can be active
                // This doesnt say that device isnt locally available
                stopCountdownTimer(macId)

                responseObservable.onNext(
                        Event(
                                EventType.UDP_BROADCAST,
                                EventStatus.FAILURE,
                                macId,
                                null
                        )
                )

                if (error != null) {
                    // Not force disconnect
                    error.printStackTrace()

                    // Have only used SocketException because - If we try sending data when WIFI is off, we get Machine not on network exception
                    if (error is SocketException &&
                            error.message.isNullOrEmpty() &&
                            error.message!!.contains("Machine is not on the network", true)
                    ) {
                        Timber.e("Machine might not be on the network, wifi ")

                    } else {
                        // retry(socketClient, macId)
                        // Retry is disabled
                    }

                    if (isMusicActivity.not()) {
                        if (lastSentCommandViaTcp.isNotEmpty()) {
                            for (command in lastSentCommandViaTcp.entries) {
                                Timber.e("Failed to send (${lastSentCommandViaTcp[command.key]!!.sequenceNumber}) via TCP, retrying to send via MQTT")
                                resendCommandViaMqtt(lastSentCommandViaTcp[command.key]!!)
                                Timber.e("Command(${lastSentCommandViaTcp[command.key]!!.sequenceNumber}) of device(${command.key})removed from last sent TCP command map")
                                lastSentCommandViaTcp.remove(command.key)
                            }
                        } else {
                            Timber.e("No last sent TCP command")
                        }
                    } else {
                        Timber.e("OnDisconnect silently fails, since its music/audio activity")
                    }

                } else {
                    // Force disconnect
                    if (isMusicActivity.not()) {
                        if (lastSentCommandViaTcp.isNotEmpty()) {
                            for (command in lastSentCommandViaTcp.entries) {
                                Timber.e("Failed to send (${lastSentCommandViaTcp[command.key]!!.sequenceNumber}) via TCP, retrying to send via MQTT")
                                resendCommandViaMqtt(lastSentCommandViaTcp[command.key]!!)
                                Timber.e("Command(${lastSentCommandViaTcp[command.key]!!.sequenceNumber}) of device(${command.key})removed from last sent TCP command map")
                                lastSentCommandViaTcp.remove(command.key)
                            }
                        } /*else {
                            Timber.e("No last sent TCP command")
                            var lightName = getLampName(macId)
                            responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, lightName, null))
                        }*/
                    } else {
                        Timber.e("OnDisconnect silently fails, since its music/audio activity")
                    }
                }

                var handler = tcpRetryMap[macId]
                if (handler == null) {
                    handler = Handler()
                    tcpRetryMap[macId] = handler
                }
                handler.removeCallbacksAndMessages(null)
                handler.postDelayed({
                    retryTcpConnection(macId)
                }, 2000)

                startBroadcast()
            }

            override fun onResponse(data: ByteArray) {
                Timber.e(Utilities.toString(data))

                //pendingCommand = null
                if (data.isNotEmpty()) {
                    Timber.e("Got response")
                    parseResponse(data, false)
                } else {
                    Timber.e("Got no response")
                }
            }
        }
    }

    private fun retryTcpConnection(macId: String) {
        if (macId != DEFAULT_MAC_ID && macIdClientMap[macId] != null && macIdClientMap[macId]!!.retryCount < MAX_TCP_RETRY) {
            macIdClientMap[macId]!!.retryCount++
            Timber.d("Retrying TCP connection with $macId - ${macIdClientMap[macId]!!.socket.mConfig.mIp!!}")
            var ipAddress: String = macIdClientMap[macId]!!.socket.mConfig.mIp!!
            var port: Int = macIdClientMap[macId]!!.socket.mConfig.mPort!!
            connect(macId, ipAddress, port)
        } else {
            if (macIdClientMap[macId] != null && macIdClientMap[macId]!!.retryCount == MAX_TCP_RETRY) {
                Timber.d("$TAG Maximum tcp connection retries made to device $macId")
            }
            macIdClientMap.remove(macId)
            startBroadcast()
            responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, macId, null))
        }
    }

    fun getLightStatusFromDevice(macId: String, viaTcp: Boolean) {
        Timber.e("Fetch device($macId) status initiated")
        if (viaTcp) {
            val request = BaseRequest(
                    macId = macId,
                    sequenceNumber = Utilities.getRandomSequenceNumber(),
                    packetType = PacketType.LIGHT_STATUS,
                    payLoad = null
            )
            Timber.e("GET_LIGHT_STATUS command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")

            lastSentGetLightStatusSequenceNumberTcp = request.sequenceNumber.toInt()
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())

        } else {
            var request = GetLightStatusRequest(
                    macId,
                    null
            )
            var requestJson = request.getRequestJson()
            Timber.e("GET_LIGHT_STATUS command for device($macId) sent via mqtt - ${requestJson.toString()}")

            lastSentGetLightStatusSequenceNumberMqtt = request.frameNum!!.toInt()
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())
        }
    }

    private fun isDeviceToBeFlickered(macId: String): Boolean {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        return sharedPreference.isDeviceToBeFlickered(macId)

    }

    private fun resendCommandViaMqtt(requestInBaseRequest: BaseRequest) {
        var requestInJson = convertBaseRequestToMqttJson(requestInBaseRequest)

        if (requestInJson == null) {
            Timber.e("Failed to form the requestJson")
            responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, requestInBaseRequest.macId, null))
        } else {
            sendCommandViaMqtt(requestInBaseRequest.macId, requestInJson, isMusicActivity.not())
        }
    }

    private fun stopCountdownTimer(macId: String, sequenceNumber: Int) {
        var lastSentCommand = countDownTimerMap[macId]

        if (lastSentCommand != null) {
            if (lastSentCommand.sequenceNumber == sequenceNumber) {
                Timber.e("Timer stopped for last request(${lastSentCommand.sequenceNumber}) sent to device($macId)")
                lastSentCommand.countDownTimer.cancel()
                countDownTimerMap.remove(macId)
            } else {
                Timber.e("Timer stop request received for previous command($sequenceNumber) of device($macId)")
            }
        } else {
            Timber.e("Timer not initiated for request device($macId)")
        }
    }

    private fun stopCountdownTimer(macId: String) {
        var lastSentCommand = countDownTimerMap[macId]

        if (lastSentCommand != null) {
            Timber.e("Timer stopped for device($macId)")
            lastSentCommand.countDownTimer.cancel()
            countDownTimerMap.remove(macId)
        } else {
            Timber.e("Timer not initiated for request device($macId)")
        }
    }

    private fun retry(socketClient: SocketClient, macId: String) {
        /* Timber.e("Retry initiated")
         if (retryCount < CONNECTION_RETRY_COUNT) {
             retryCount++
             Timber.e("Current retry count($retryCount)")
             //connect(macId, socketClient.mConfig.mPort!!)
         } else {
             // By this we can confirm that device isnt locally available
             Timber.e("Max retry counts($retryCount) reached")
             //if (!macId.equals(DEFAULT_MAC_ID))
             //macIdIpAddressMap.remove(macId)
             //pendingCommand = null
             *//*var lightName = ""
            if (macId.equals(DEFAULT_MAC_ID).not()) {
                lightName = getLampName(macId)
            } else if (pendingSsid.isNullOrEmpty().not()) {
                lightName = pendingSsid
            } else {
                lightName = "Device"
            }*//*
            //responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, lightName, null))
        }*/
    }

    fun sendBroadcast(ipAddress: String, port: Int, macId: String?) {
        var broadcastRequest: BaseRequest
        if (macId != null) {
            // UDP broadcast to a particular device
            broadcastRequest = BaseRequest(
                    macId,
                    Utilities.getRandomSequenceNumber(),
                    PacketType.GET_IP,
                    null
            )
            Timber.d("$TAG Making specific broadcast to device($macId) with router($ipAddress)")
        } else {
            // Generic UDP broadcast
            broadcastRequest = BaseRequest(
                    "000000000000",
                    Utilities.getRandomSequenceNumber(),
                    PacketType.GET_IP,
                    null
            )

            Timber.d("$TAG Making generic broadcast with router($ipAddress)")
        }

        if (macId != null) {
            udpClient.sendBroadcast(ipAddress, port, macId, broadcastRequest.getByteArray())
        } else {
            udpClient.sendBroadcast(ipAddress, port, DEFAULT_MAC_ID, broadcastRequest.getByteArray())
        }
    }

    private fun processCommand(commandPayload: Command) {
        //stopCountdownTimer()
        retryCount = 0

        var shouldSendViaMqtt = false

        if (commandPayload.id is String) {
            if (macIdClientMap[commandPayload.id as String] == null) {
                shouldSendViaMqtt = true
            } else {

                /**
                 * Will take the difference between current time and time at which the last command was sent via TCP(lastCommandSentTimestamp),
                 * if its greater than 1 minute 50 seconds then the lamp would have disconnected the socket hence we are disconnecting the socket and sending
                 * the command via MQTT
                 *
                 * */
                var differenceTime = (System.currentTimeMillis() - macIdClientMap[commandPayload.id as String]!!.lastCommandSentTimestamp)
                Timber.e("Current system time = ${System.currentTimeMillis()}")
                Timber.e("Timestamp when last command was sent via TCP = ${macIdClientMap[commandPayload.id as String]!!.lastCommandSentTimestamp}")
                Timber.e("Difference between the timestamps = ${differenceTime}")
                Timber.e("Difference between the timestamps in minutes = ${(differenceTime / 60000)}")

                if (differenceTime > LAMP_TCP_DISCONNECTION) {
                    disconnectSocket(commandPayload.id as String)
                    shouldSendViaMqtt = true
                } else {
                    shouldSendViaMqtt = false
                }
            }
        }

        when (commandPayload.commandType) {
            EventType.SET_WIFI -> {
                var map = commandPayload.payload as HashMap<String, Any>
                pendingSsid = map[BUNDLE_SSID] as String
                writeWifiCredentials(map)
            }
            EventType.TURN_ON_LIGHT -> {
                turnOnLight(commandPayload, shouldSendViaMqtt, false)
            }

            EventType.TURN_OFF_LIGHT -> {
                turnOffLight(commandPayload, shouldSendViaMqtt, false)
            }

            EventType.TURN_ON_SCENE -> {
                turnOnScene((commandPayload.id as Long))
            }

            EventType.GET_FIRMWARE_VERSION -> {
                getFirmwareVersion(commandPayload, shouldSendViaMqtt)
            }

            EventType.TURN_OFF_SCENE -> {
                turnOffScene((commandPayload.id as Long))
            }

            EventType.TURN_ON_GROUP -> {
                turnOnGroup((commandPayload.id as Long))
            }

            EventType.TURN_OFF_GROUP -> {
                turnOffGroup((commandPayload.id as Long))
            }

            EventType.SET_LIGHT_RGB -> {
                setLightRgb(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_LIGHT_BRIGHTNESS -> {
                setLightBrightness(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_LIGHT_RGB_BRIGHTNESS -> {
                setLightRgbWithBrightness(commandPayload, shouldSendViaMqtt)
            }
            EventType.ADD_NEW_SCHEDULE -> {
                addNewSchedule(commandPayload, true)
            }
            EventType.EDIT_SCHEDULE -> {
                editSchedule(commandPayload, true)
            }
            EventType.DELETE_SCHEDULE -> {
                deleteSchedule(commandPayload, true)
            }

            EventType.DELETE_LAMP -> {
                deleteLamp(commandPayload, false)
            }

            EventType.SET_LIGHT_DAYLIGHT -> {
                setLightDaylight(commandPayload, shouldSendViaMqtt)
            }

            EventType.GET_LIGHT_STATUS -> {
                getLightStatus(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_LIGHT_PRESET_SCENE -> {
                setLightPresetScene(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_GROUP_DAYLIGHT -> {
                setGroupDaylight(commandPayload)
            }

            EventType.SET_GROUP_BRIGHTNESS -> {
                setGroupBrightness(commandPayload)
            }

            EventType.SET_GROUP_SCENE -> {
                setGroupScene(commandPayload)
            }

            EventType.SET_GROUP_COLOR -> {
                setGroupColor(commandPayload)
            }
        }
    }

    private fun getFirmwareVersion(commandPayload: Command, shouldSendViaMqtt: Boolean) {
        var macId = (commandPayload.id as String)
        var sequenceNumber = Utilities.getRandomSequenceNumber().toInt()

        if (shouldSendViaMqtt) {
            // Form mqtt packet and send via that
            var request = GetFirmwareVersionRequest(
                    macId,
                    sequenceNumber.toString()
            )

            var requestJson = request.getRequestJson()
            Timber.e("GET FIRMWARE VERSION command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())

        } else {
            // Form tcp packet and send via tcp

            val request = BaseRequest(
                    macId = macId,
                    sequenceNumber = sequenceNumber.toString(),
                    packetType = PacketType.GET_FIRMWARE_VERSION,
                    payLoad = null
            )
            Timber.e("GET FIRMWARE VERSION command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun addNewSchedule(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = (command.id as String)
        var schedulePayload = (command.payload as HashMap<String, Any>)

        if (shouldSendViaMqtt) {

            var request = SetScheduleRequest(
                    macId,
                    null,
                    schedulePayload[BUNDLE_SCHEDULE_ID] as Int,
                    schedulePayload[BUNDLE_START_TIME] as Long,
                    schedulePayload[BUNDLE_STOP_TIME] as Long,
                    schedulePayload[BUNDLE_R_VALUE] as Int,
                    schedulePayload[BUNDLE_G_VALUE] as Int,
                    schedulePayload[BUNDLE_B_VALUE] as Int,
                    schedulePayload[BUNDLE_W_VALUE] as Int,
                    schedulePayload[BUNDLE_C_VALUE] as Int,
                    schedulePayload[BUNDLE_BRIGHTNESS_VALUE] as Int
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("ADD SCHEDULE command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())

        } else {

            val request = BaseRequest(
                    macId = (command.id as String),
                    sequenceNumber = Utilities.getRandomSequenceNumber(),
                    packetType = PacketType.SET_SCHEDULE,
                    payLoad = com.svarochi.wifi.model.communication.commands.request.SetScheduleRequest(
                            slot_id = (schedulePayload[BUNDLE_SCHEDULE_ID] as Int).toString(),
                            start_time = Utilities.convertLongToHexString(schedulePayload[BUNDLE_START_TIME] as Long),
                            end_time = Utilities.convertLongToHexString(schedulePayload[BUNDLE_STOP_TIME] as Long),
                            r_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_R_VALUE] as Int),
                            g_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_G_VALUE] as Int),
                            b_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_B_VALUE] as Int),
                            w_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_W_VALUE] as Int),
                            c_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_C_VALUE] as Int),
                            brightness_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_BRIGHTNESS_VALUE] as Int)
                    )
            )
            Timber.e("ADD SCHEDULE command for device(${macId}) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun editSchedule(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = (command.id as String)
        var schedulePayload = (command.payload as HashMap<String, Any>)

        if (shouldSendViaMqtt) {

            var request = SetScheduleRequest(
                    macId,
                    null,
                    schedulePayload[BUNDLE_SCHEDULE_ID] as Int,
                    schedulePayload[BUNDLE_START_TIME] as Long,
                    schedulePayload[BUNDLE_STOP_TIME] as Long,
                    schedulePayload[BUNDLE_R_VALUE] as Int,
                    schedulePayload[BUNDLE_G_VALUE] as Int,
                    schedulePayload[BUNDLE_B_VALUE] as Int,
                    schedulePayload[BUNDLE_W_VALUE] as Int,
                    schedulePayload[BUNDLE_C_VALUE] as Int,
                    schedulePayload[BUNDLE_BRIGHTNESS_VALUE] as Int
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("EDIT SCHEDULE command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())

        } else {

            val request = BaseRequest(
                    macId = (command.id as String),
                    sequenceNumber = Utilities.getRandomSequenceNumber(),
                    packetType = PacketType.SET_SCHEDULE,
                    payLoad = com.svarochi.wifi.model.communication.commands.request.SetScheduleRequest(
                            slot_id = (schedulePayload[BUNDLE_SCHEDULE_ID] as Int).toString(),
                            start_time = Utilities.convertLongToHexString(schedulePayload[BUNDLE_START_TIME] as Long),
                            end_time = Utilities.convertLongToHexString(schedulePayload[BUNDLE_STOP_TIME] as Long),
                            r_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_R_VALUE] as Int),
                            g_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_G_VALUE] as Int),
                            b_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_B_VALUE] as Int),
                            w_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_W_VALUE] as Int),
                            c_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_C_VALUE] as Int),
                            brightness_value = Utilities.convertIntToHexString(schedulePayload[BUNDLE_BRIGHTNESS_VALUE] as Int)
                    )
            )
            Timber.e("EDIT SCHEDULE command for device(${macId}) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun deleteSchedule(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = (command.id as String)
        var schedulePayload = (command.payload as HashMap<String, Any>)

        if (shouldSendViaMqtt) {

            var request = SetScheduleRequest(
                    macId,
                    null,
                    schedulePayload[BUNDLE_SCHEDULE_ID] as Int,
                    SCHEDULE_RESET_START_TIME_MQTT,
                    SCHEDULE_RESET_END_TIME_MQTT,
                    SCHEDULE_RESET_VALUE_MQTT,
                    SCHEDULE_RESET_VALUE_MQTT,
                    SCHEDULE_RESET_VALUE_MQTT,
                    SCHEDULE_RESET_VALUE_MQTT,
                    SCHEDULE_RESET_VALUE_MQTT,
                    SCHEDULE_BRIGHTNESS_RESET_VALUE_MQTT
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("DELETE SCHEDULE command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())

        } else {

            val request = BaseRequest(
                    macId = (command.id as String),
                    sequenceNumber = Utilities.getRandomSequenceNumber(),
                    packetType = PacketType.SET_SCHEDULE,
                    payLoad = com.svarochi.wifi.model.communication.commands.request.SetScheduleRequest(
                            slot_id = (schedulePayload[BUNDLE_SCHEDULE_ID] as Int).toString(),
                            start_time = SCHEDULE_RESET_START_TIME_TCP,
                            end_time = SCHEDULE_RESET_END_TIME_TCP,
                            r_value = SCHEDULE_RESET_VALUE_TCP,
                            g_value = SCHEDULE_RESET_VALUE_TCP,
                            b_value = SCHEDULE_RESET_VALUE_TCP,
                            w_value = SCHEDULE_RESET_VALUE_TCP,
                            c_value = SCHEDULE_RESET_VALUE_TCP,
                            brightness_value = SCHEDULE_BRIGHTNESS_RESET_VALUE_TCP
                    )
            )
            Timber.e("DELETE SCHEDULE command for device(${macId}) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun initiateTimer(macId: String, sequenceNumber: Int, connectionType: Int) {
        /**
         * STEPS INVOLVED -
         * 1. Stop the timer (if any) for the last command of MacId given
         * 2. Initiate start the timer for the new command and associate with the MacId
         *
         * */

        stopCountdownTimer(macId)

        if (connectionType == TCP_CONNECTION) {
            val countDownTimer = object : CountDownTimer(TCP_TIMEOUT.toLong(), 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    Timber.e("TCP timer for macId(${macId}) - $millisUntilFinished")
                }

                override fun onFinish() {
                    Timber.e("TCP timer finished for macId(${macId})")
                    countDownTimerMap.remove(macId)

                    if (deleteLampSequenceNumber != null && sequenceNumber == deleteLampSequenceNumber!!.toInt()) {
                        responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, null, null))
                    }

                    responseObservable.onNext(Event(EventType.TCP_CONNECTION, EventStatus.FAILURE, macId, null))

                    Timber.e("Disconnecting the socket for device(${macId})")
                    disconnectSocket(macId)
                }
            }
            countDownTimerMap[macId] = LastSentCommand(macId, sequenceNumber, countDownTimer)
            countDownTimerMap[macId]?.countDownTimer?.start()
        } else if (connectionType == MQTT_CONNECTION) {
            val countDownTimer = object : CountDownTimer(MQTT_TIMEOUT.toLong(), 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    Timber.e("MQTT timer for macId(${macId}) - $millisUntilFinished")
                }

                override fun onFinish() {
                    Timber.e("MQTT timer finished for macId(${macId})")
                    countDownTimerMap.remove(macId)
                    /*var macId = getMacIdOfSequenceNumber(sequenceNumber)
                removeFromSequenceCommandMap(sequenceNumber)*/

                    if (deleteLampSequenceNumber != null && sequenceNumber == deleteLampSequenceNumber!!.toInt()) {
                        responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.FAILURE, null, null))
                    }

                    responseObservable.onNext(Event(EventType.MQTT_CONNECTION, EventStatus.FAILURE, macId, null))

                    Timber.e("Disconnecting the socket for device(${macId})")
                    disconnectSocket(macId)
                    /* var lightName: String = getLampName(macId)
             responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, lightName, null))*/
                }
            }

            countDownTimerMap[macId] = LastSentCommand(macId, sequenceNumber, countDownTimer)
            countDownTimerMap[macId]?.countDownTimer?.start()
        }
    }

    private fun stopAllTimers() {
        var list = countDownTimerMap.keys.toList()
        list.forEach {
            stopCountdownTimer(it)
        }
    }

    private fun clearAllPendingCommands() {
        lastSentCommandViaTcp.clear()
        pendingSetWifiCommand.clear()
        pendingLightStatusMap.clear()
    }

    private fun turnOffLight(command: Command, shouldSendViaMqtt: Boolean, isFlickerCommand: Boolean) {
        var macId = command.id as String

        turnOffRequestSequenceNumber = Utilities.getRandomSequenceNumber().toInt()


        if (command.payload != null && command.payload is Boolean && (command.payload as Boolean)) {
            turnOffRequestSequenceNumber = flickerSequenceNumber
        }

        if (isFlickerCommand.not()) {
            var light = getLatestLight(macId)
            pendingLightStatusMap.put(
                    macId, LightStatus(
                    macId,
                    turnOffRequestSequenceNumber,
                    light!!.rValue,
                    light.gValue,
                    light.bValue,
                    light.wValue,
                    light.cValue,
                    SceneType.DEFAULT.value,
                    light.brightnessValue,
                    false,
                    light.rssiValue
            )
            )
        }

        if (shouldSendViaMqtt) {
            // Form mqtt packet and send via that

            var request = SetColourRequest(
                    macId,
                    turnOffRequestSequenceNumber.toString(),
                    "00",
                    "00",
                    "00",
                    "00",
                    "00",
                    100,
                    false
            )

            var requestJson = request.getRequestJson()
            Timber.e("TURN LIGHT OFF command for device($macId) sent via mqtt - ${requestJson.toString()}")

            if (isFlickerCommand.not()) {
                sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())
            } else {
                sendCommandViaMqtt(macId, requestJson, false)
            }

        } else {
            // Form tcp packet and send via tcp

            val request = BaseRequest(
                    macId = macId,
                    sequenceNumber = turnOffRequestSequenceNumber.toString(),
                    packetType = PacketType.SET_COLOR,
                    payLoad = SetLampColorRequest(
                            "00",
                            "00",
                            "00",
                            "00",
                            "00",
                            100,
                            false
                    )
            )
            Timber.e("TURN LIGHT OFF command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            if (isFlickerCommand.not()) {
                sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
            } else {
                sendCommandViaTcp(macId, request, false, false)
            }
        }
    }

    private fun setWifiDetailsToPendingDevice(connectedSsid: String) {
        var isWifiWritten = false
        if (pendingSetWifiCommand.isNotEmpty()) {
            pendingSetWifiCommand.entries.forEach {
                if (it.key.equals(connectedSsid, true)) {
                    isWifiWritten = true
                    connect(DEFAULT_MAC_ID, DEFAULT_LAMP_IP, DEFAULT_LAMP_PORT)
                }
            }
        }
        if(isWifiWritten.not()){
            // Phone is connected to some other Svarochi lamp
            pendingSetWifiCommand.clear()
        }
    }


    private fun sendCommandViaTcp(macId: String, request: BaseRequest, shouldSaveLastSentTcpCommand: Boolean, shouldInitiateTimer: Boolean) {
        if (macIdClientMap.containsKey(macId)) {

            if (shouldSaveLastSentTcpCommand) {
                Timber.e("Adding (${request.sequenceNumber}) for device($macId) to last sent TCP command map")
                lastSentCommandViaTcp[macId] = request
            }

            var socketClient = macIdClientMap[macId]
            socketClient!!.socket.sendData(request.getByteArray())

            if (shouldInitiateTimer) {
                initiateTimer(macId, request.sequenceNumber.toInt(), TCP_CONNECTION)
            }
        } else {
            responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, macId, null))
        }
    }


    fun disconnectClients() {
        for (client in macIdClientMap.keys) {
            if (!client.equals(DEFAULT_MAC_ID))
                macIdClientMap.get(client)!!.socket.disconnect()
        }
    }

    fun setMacIdWithIp(macId: String, ipAddress: String) {
        //macIdIpAddressMap.put(macId, ipAddress)
        /**
         * Steps involved -
         * 1. Try to create socket connection with that ipAddress
         * 2. If success add to <MacId, IpAddress> map
         * 3. On failure silent
         *
         * */
        Timber.e("$macId responded with IP($ipAddress)")
        if (!macIdClientMap.containsKey(macId)) {
            if (getLatestLight(macId) == null) {
                Timber.e("Device is not associated to user so not making TCP connection")
            } else {
                Timber.e("Device is associated to user so making TCP connection")
                connect(macId, ipAddress, DEFAULT_LAMP_PORT)
            }
        } else {
            Timber.e("TCP connection with device($macId) already exists")
            /*Timber.e("Removing the socket object from the map")
            macIdClientMap.remove(macId)

            Timber.e("Connecting to device($macId) with IP($ipAddress)")
            connect(macId, ipAddress, DEFAULT_LAMP_PORT)*/

        }
    }

    fun disconnectDefaultSocket() {
        // This disconnects default socket that is used to write wifi credentials
        // This is done as the socket object isnt valid for all the devices
        if (macIdClientMap[DEFAULT_MAC_ID] != null) {
            macIdClientMap.get(DEFAULT_MAC_ID)!!.socket.disconnect()
            Timber.e("$TAG Default Socket(${DEFAULT_LAMP_IP}/${DEFAULT_LAMP_PORT}) is disconnected")
        }
    }

    fun sendCommand(commandPayload: Command) {
        Timber.e("Received command of lampType - ${commandPayload.commandType.name}")
        processCommand(commandPayload)
    }

    private fun disconnectSocket(macId: String) {
        if (macIdClientMap[macId] != null) {
            Timber.e("Disconnecting socket of device($macId)")
            macIdClientMap[macId]!!.socket.disconnect()
        } else {
            Timber.e("No socket created for device($macId)")
        }
    }

    private fun sendCommandToDevice(commandPayload: Command) {
        //var shouldSendViaMqtt = macIdIpAddressMap[commandPayload.id as String] == null
        var shouldSendViaMqtt = macIdClientMap[commandPayload.id as String] == null


        when (commandPayload.commandType) {
            EventType.TURN_ON_LIGHT -> {
                turnOnLight(commandPayload, shouldSendViaMqtt, false)
            }

            EventType.TURN_OFF_LIGHT -> {
                turnOffLight(commandPayload, shouldSendViaMqtt, false)
            }

            EventType.SET_LIGHT_RGB -> {
                setLightRgb(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_LIGHT_BRIGHTNESS -> {
                setLightBrightness(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_LIGHT_DAYLIGHT -> {
                /*var daylightValue = (commandPayload.payload as Int)
                if ((daylightValue % 2) == 0) {
                    setLightDaylight(commandPayload, false)
                } else {
                    setLightDaylight(commandPayload, true)
                }*/
                setLightDaylight(commandPayload, shouldSendViaMqtt)
            }

            EventType.GET_LIGHT_STATUS -> {
                getLightStatus(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_LIGHT_PRESET_SCENE -> {
                setLightPresetScene(commandPayload, shouldSendViaMqtt)
            }

            EventType.SET_LIGHT_STATE -> {
                setLightState(commandPayload, shouldSendViaMqtt)
            }
        }
    }

    private fun turnOnScene(sceneId: Long) {

        // localDataSource!!.setSceneStatus(sceneId, true)
        var lightList = localDataSource!!.getLightsOfScene(sceneId)

        for (sceneLight in lightList) {
            Timber.d("Scene Value of light ${sceneLight.macId} = ${sceneLight.sceneValue}")

            if (sceneLight.sceneValue.equals(SceneType.DEFAULT.value)) {
                var scenePayload = HashMap<String, Any>()

                if (sceneLight.rValue.equals("00").not() ||
                        sceneLight.gValue.equals("00").not() ||
                        sceneLight.bValue.equals("00").not()) {
                    // Color is applied
                    scenePayload[BUNDLE_R_VALUE] = sceneLight.rValue
                    scenePayload[BUNDLE_G_VALUE] = sceneLight.gValue
                    scenePayload[BUNDLE_B_VALUE] = sceneLight.bValue
                    scenePayload[BUNDLE_W_VALUE] = "00"
                    scenePayload[BUNDLE_C_VALUE] = "00"
                    scenePayload[BUNDLE_BRIGHTNESS_VALUE] = sceneLight.brightnessValue
                    scenePayload[BUNDLE_ON_OFF_STATUS] = sceneLight.onOffValue
                } else {
                    // Warm and cool is applied
                    scenePayload[BUNDLE_R_VALUE] = "00"
                    scenePayload[BUNDLE_G_VALUE] = "00"
                    scenePayload[BUNDLE_B_VALUE] = "00"
                    scenePayload[BUNDLE_W_VALUE] = sceneLight.wValue
                    scenePayload[BUNDLE_C_VALUE] = sceneLight.cValue
                    scenePayload[BUNDLE_BRIGHTNESS_VALUE] = sceneLight.brightnessValue
                    scenePayload[BUNDLE_ON_OFF_STATUS] = sceneLight.onOffValue

                }
                var commmand = Command(EventType.SET_LIGHT_STATE, sceneLight.macId, scenePayload)
                Timber.e("Turn on scene command requested for device - ${sceneLight.macId}")
                sendCommandToDevice(commmand)
            } else {
                // Scene is applied

                var sceneType = getSceneType(sceneLight.sceneValue)
                if (sceneType != null) {
                    var commmand = Command(EventType.SET_LIGHT_PRESET_SCENE, sceneLight.macId, sceneType)
                    Timber.e("Turn on scene command requested for device with preset scene (${sceneType})- ${sceneLight.macId}")
                    sendCommandToDevice(commmand)
                }
            }
        }
    }

    private fun turnOffScene(sceneId: Long) {

        // localDataSource!!.setSceneStatus(sceneId, false)
        var lightList = localDataSource!!.getLightsOfScene(sceneId)

        for (sceneLight in lightList) {
            var commmand = Command(EventType.TURN_OFF_LIGHT, sceneLight.macId, null)
            Timber.e("Turn off scene requested for device - ${sceneLight.macId}")
            sendCommandToDevice(commmand)
        }
    }

    private fun turnOnLight(command: Command, shouldSendViaMqtt: Boolean, isFlickerCommand: Boolean) {
        var macId = command.id as String
        turnOnRequestSequenceNumber = Utilities.getRandomSequenceNumber().toInt()

        if (command.payload != null && command.payload is Boolean && (command.payload as Boolean)) {
            turnOnRequestSequenceNumber = flickerSequenceNumber
        }

        if (isFlickerCommand.not()) {
            var light = getLatestLight(macId)
            pendingLightStatusMap.put(
                    macId, LightStatus(
                    macId,
                    turnOnRequestSequenceNumber,
                    light!!.rValue,
                    light.gValue,
                    light.bValue,
                    light.wValue,
                    light.cValue,
                    light.sceneValue,
                    light.brightnessValue,
                    true,
                    light.rssiValue
            )
            )
        }


        if (shouldSendViaMqtt) {
            // Form mqtt packet and send via that
            var request = SetColourRequest(
                    macId,
                    turnOnRequestSequenceNumber.toString(),
                    "00",
                    "00",
                    "00",
                    "00",
                    "00",
                    0,
                    true
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("TURN LIGHT ON command for device($macId) sent via mqtt - ${requestJson.toString()}")

            if (isFlickerCommand.not()) {
                sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())
            } else {
                sendCommandViaMqtt(macId, requestJson, false)
            }
        } else {
            // Form tcp packet and send via tcp

            val request = BaseRequest(
                    macId = macId,
                    sequenceNumber = turnOnRequestSequenceNumber.toString(),
                    packetType = PacketType.SET_COLOR,
                    payLoad = SetLampColorRequest(
                            "00",
                            "00",
                            "00",
                            "00",
                            "00",
                            0,
                            true
                    )
            )
            Timber.e("TURN LIGHT ON command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            if (isFlickerCommand.not()) {
                sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
            } else {
                sendCommandViaTcp(macId, request, false, false)
            }
        }
    }

    private fun writeWifiCredentials(map: HashMap<String, Any>) {
        var request = BaseRequest(
                DEFAULT_MAC_ID,
                Utilities.getRandomSequenceNumber(),
                PacketType.SET_WIFI,
                SetWifiCredRequest(
                        "01",
                        map[BUNDLE_SSID_TO_CONFIGURE] as String,
                        map[BUNDLE_PASSWORD] as String,
                        map[BUNDLE_AUTHENTICATION_MODE] as String,
                        map[BUNDLE_ENCRYPTION_TYPE] as String
                )
        )
        pendingSetWifiCommand.clear()
        pendingSetWifiCommand[map[BUNDLE_LAMP_SSID] as String] = request

        Timber.e("$TAG Set Wifi Command has been added to pending command")

        if (svarochiWifiNetwork != null) {
            Timber.e("$TAG Device is already connected to svarochi wifi network")
            connect(DEFAULT_MAC_ID, DEFAULT_LAMP_IP, DEFAULT_LAMP_PORT)
        }
    }

    private fun turnOffGroup(groupId: Long) {

        var lightList = localDataSource!!.getLightsOfGroup(groupId)

        for (groupLight in lightList) {
            var commmand = Command(
                    EventType.TURN_OFF_LIGHT,
                    groupLight.macId,
                    null
            )
            Timber.e("Turn off group requested for device - ${groupLight.macId}")
            sendCommandToDevice(commmand)
        }

        responseObservable.onNext(Event(EventType.TURN_ON_GROUP, EventStatus.SUCCESS, getLatestGroup(groupId), null))
    }

    private fun turnOnGroup(groupId: Long) {

        var lightList = localDataSource!!.getLightsOfGroup(groupId)

        for (groupLight in lightList) {
            var commmand = Command(
                    EventType.TURN_ON_LIGHT,
                    groupLight.macId,
                    null
            )
            Timber.e("Turn on group requested for device - ${groupLight.macId}")
            sendCommandToDevice(commmand)
        }

        responseObservable.onNext(Event(EventType.TURN_ON_GROUP, EventStatus.SUCCESS, getLatestGroup(groupId), null))

    }

    private fun setLightRgbWithBrightness(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = (command.id as String)
        var rgb_brightness_payload = (command.payload as HashMap<String, Any>)

        var light = getLatestLight(macId)
        /*pendingLightStatusMap.put(
                macId, LightStatus(
                macId,
                rgb_brightness_payload[BUNDLE_R_VALUE]!! as String,
                rgb_brightness_payload[BUNDLE_G_VALUE]!! as String,
                rgb_brightness_payload[BUNDLE_B_VALUE]!! as String,
                "00",
                "00",
                light!!.sceneValue,
                rgb_brightness_payload[BUNDLE_BRIGHTNESS_VALUE]!! as Int,
                true,
                light.rssiValue
        )
        )*/

        if (shouldSendViaMqtt) {
            var request = SetColourRequest(
                    macId,
                    Utilities.getRandomSequenceNumber(),
                    rgb_brightness_payload[BUNDLE_R_VALUE]!! as String,
                    rgb_brightness_payload[BUNDLE_G_VALUE]!! as String,
                    rgb_brightness_payload[BUNDLE_B_VALUE]!! as String,
                    "00",
                    "00",
                    rgb_brightness_payload[BUNDLE_BRIGHTNESS_VALUE]!! as Int,
                    true
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("SET RGB with Brightness command for device($macId) sent via mqtt - ${requestJson.toString()}")

            // Since the commands are more in number, no need to initiate timer
            sendCommandViaMqtt(macId, requestJson, false)

        } else {
            val request = BaseRequest(
                    macId = (command.id as String),
                    sequenceNumber = Utilities.getRandomSequenceNumber(),
                    packetType = PacketType.SET_COLOR,
                    payLoad = SetLampColorRequest(
                            r_value = rgb_brightness_payload[BUNDLE_R_VALUE]!! as String,
                            g_value = rgb_brightness_payload[BUNDLE_G_VALUE]!! as String,
                            b_value = rgb_brightness_payload[BUNDLE_B_VALUE]!! as String,
                            w_value = "00",
                            cct_value = "00",
                            brightness_value = rgb_brightness_payload[BUNDLE_BRIGHTNESS_VALUE]!! as Int,
                            on_off_status = true
                    )
            )
            Timber.e(
                    "SET RGB with Brightness command for device(${light!!.macId}) sent via tcp - ${Utilities.toString(
                            request.getByteArray()
                    )}"
            )
            // Since the commands are more in number dont save the last sent command and no need to initiate timer also
            sendCommandViaTcp(macId, request, false, false)
        }
    }

    private fun setLightRgb(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = (command.id as String)
        var rgbPayload = (command.payload as HashMap<String, String>)
        var sequenceNumber = Utilities.getRandomSequenceNumber()

        var light = getLatestLight(macId)
        pendingLightStatusMap.put(
                macId, LightStatus(
                macId,
                sequenceNumber.toInt(),
                rgbPayload[BUNDLE_R_VALUE]!!,
                rgbPayload[BUNDLE_G_VALUE]!!,
                rgbPayload[BUNDLE_B_VALUE]!!,
                "00",
                "00",
                SceneType.DEFAULT.value,
                light!!.brightnessValue,
                true,
                light.rssiValue
        )
        )

        if (shouldSendViaMqtt) {
            var request = SetColourRequest(
                    macId.toLowerCase(),
                    sequenceNumber,
                    rgbPayload[BUNDLE_R_VALUE]!!,
                    rgbPayload[BUNDLE_G_VALUE]!!,
                    rgbPayload[BUNDLE_B_VALUE]!!,
                    "00",
                    "00",
                    0,
                    true
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("SET RGB command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, false)

        } else {
            val request = BaseRequest(
                    macId = (command.id as String),
                    sequenceNumber = sequenceNumber,
                    packetType = PacketType.SET_COLOR,
                    payLoad = SetLampColorRequest(
                            r_value = rgbPayload[BUNDLE_R_VALUE]!!,
                            g_value = rgbPayload[BUNDLE_G_VALUE]!!,
                            b_value = rgbPayload[BUNDLE_B_VALUE]!!,
                            w_value = "00",
                            cct_value = "00",
                            brightness_value = 0,
                            on_off_status = true
                    )
            )
            Timber.e("SET RGB command for device(${light.macId}) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), false)
        }
    }

    private fun setLightBrightness(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = (command.id as String)
        var brightnessPayload = (command.payload as Int)

        setBrightnessRequestSequenceNumber = Utilities.getRandomSequenceNumber().toInt()

        var light = getLatestLight(macId)
        /*pendingLightStatusMap.put(
                macId, LightStatus(
                macId,
                light!!.rValue,
                light.gValue,
                light.bValue,
                light.wValue,
                light.cValue,
                SceneType.DEFAULT.value,
                brightnessPayload,
                true,
                light.rssiValue
        )
        )*/

        if (shouldSendViaMqtt) {
            var request = SetColourRequest(
                    macId,
                    setBrightnessRequestSequenceNumber.toString(),
                    "00",
                    "00",
                    "00",
                    "00",
                    "00",
                    brightnessPayload + 100,
                    true
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("SET BRIGHTNESS command for device($macId) sent via mqtt - ${requestJson.toString()}")

            // Since the commands are more in number, no need to initiate timer
            sendCommandViaMqtt(macId, requestJson, false)
        } else {
            val request = BaseRequest(
                    macId = (command.id as String),
                    sequenceNumber = setBrightnessRequestSequenceNumber.toString(),
                    packetType = PacketType.SET_COLOR,
                    payLoad = SetLampColorRequest(
                            r_value = "00",
                            g_value = "00",
                            b_value = "00",
                            w_value = "00",
                            cct_value = "00",
                            brightness_value = brightnessPayload + 100,
                            on_off_status = true
                    )
            )
            Timber.e("SET BRIGHTNESS command for device(${light!!.macId}) sent via tcp - ${Utilities.toString(request.getByteArray())}")

            // Since the commands are more in number dont save the last sent command and no need to initiate timer also
            sendCommandViaTcp(macId, request, false, false)
        }
    }

    private fun setLightDaylight(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = (command.id as String)
        var daylightPayload = (command.payload as Int)

        setDaylightRequestSequenceNumber = Utilities.getRandomSequenceNumber().toInt()

        var light = getLatestLight((command.id as String))
        var w_value = Utilities.convertIntToHexString((255 * (1 - (daylightPayload * 0.01))).toInt())

        var c_value = Utilities.convertIntToHexString((255 * (daylightPayload * 0.01)).toInt())

        /*pendingLightStatusMap.put(
                macId, LightStatus(
                macId,
                "00",
                "00",
                "00",
                w_value,
                c_value,
                SceneType.DEFAULT.value,
                light!!.brightnessValue,
                true,
                light.rssiValue
        )
        )*/

        if (shouldSendViaMqtt) {

            var request = SetColourRequest(
                    macId,
                    setDaylightRequestSequenceNumber.toString(),
                    "00",
                    "00",
                    "00",
                    w_value,
                    c_value,
                    0,
                    true
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("SET DAYLIGHT command for device($macId) sent via mqtt - ${requestJson.toString()}")

            // Since the commands are more in number, no need to initiate timer
            sendCommandViaMqtt(macId, requestJson, false)

        } else {

            val request = BaseRequest(
                    macId = (command.id as String),
                    sequenceNumber = setDaylightRequestSequenceNumber.toString(),
                    packetType = PacketType.SET_COLOR,
                    payLoad = SetLampColorRequest(
                            r_value = "00",
                            g_value = "00",
                            b_value = "00",
                            w_value = w_value,
                            cct_value = c_value,
                            brightness_value = 0,
                            on_off_status = true
                    )
            )
            Timber.e("SET DAYLIGHT command for device(${light!!.macId}) sent via tcp - ${Utilities.toString(request.getByteArray())}")

            // Since the commands are more in number dont save the last sent command and no need to initiate timer also
            sendCommandViaTcp(macId, request, false, false)
        }
    }

    private fun setLightPresetScene(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = command.id as String
        var sceneType = command.payload as SceneType
        var light = getLatestLight(macId)

        var sequenceNumber = Utilities.getRandomSequenceNumber()

        /**
         * For Static scenes we can save the RGBWC values so that the brightness can be modified
         *
         * W value is 0 for all static scenes
         *
         * Candle light: R:56, G:1a, B:0, C:6
         * Exercise/Gym: R:8b, G:28, B:0, C:4b
         * Sunset: R:AF, G:17, B:0, C:0
         *
         * */
        pendingLightStatusMap.put(
                macId, LightStatus(
                macId,
                sequenceNumber.toInt(),
                light!!.rValue,
                light.gValue,
                light.bValue,
                light.wValue,
                light.cValue,
                sceneType.value,
                light.brightnessValue,
                true,
                light.rssiValue
        )
        )
        /*when (sceneType) {
            SceneType.CANDLE_LIGHT -> {
                pendingLightStatusMap.put(
                        macId, LightStatus(
                        macId,
                        "56",
                        "1A",
                        "00",
                        "00",
                        "06",
                        sceneType.value,
                        light!!.brightnessValue,
                        true,
                        light!!.rssiValue
                )
                )
            }
            SceneType.SUNSET -> {
                pendingLightStatusMap.put(
                        macId, LightStatus(
                        macId,
                        "AF",
                        "17",
                        "00",
                        "00",
                        "00",
                        sceneType.value,
                        light!!.brightnessValue,
                        true,
                        light!!.rssiValue
                )
                )
            }
            SceneType.ENERGISE -> {
                pendingLightStatusMap.put(
                        macId, LightStatus(
                        macId,
                        "8B",
                        "28",
                        "00",
                        "00",
                        "4B",
                        sceneType.value,
                        light!!.brightnessValue,
                        true,
                        light!!.rssiValue
                )
                )
            }
            else -> {
                pendingLightStatusMap.put(
                        macId, LightStatus(
                        macId,
                        light!!.rValue,
                        light.gValue,
                        light.bValue,
                        light.wValue,
                        light.cValue,
                        sceneType.value,
                        light.brightnessValue,
                        true,
                        light.rssiValue
                )
                )
            }
        }*/
        if (shouldSendViaMqtt) {
            var request = SetPresetSceneRequest(
                    macId,
                    sequenceNumber,
                    sceneType
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("SET PRESET_SCENE command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())
        } else {
            var request = BaseRequest(
                    macId = macId,
                    sequenceNumber = sequenceNumber,
                    packetType = PacketType.SET_SCENE,
                    payLoad = SetLampSceneRequest(
                            sceneType = sceneType
                    )
            )
            Timber.e("SET PRESET_SCENE command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun getLightStatus(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = command.id as String

        if (shouldSendViaMqtt) {
            var request = GetLightStatusRequest(
                    macId,
                    null
            )
            var requestJson = request.getRequestJson()
            Timber.e("GET_LIGHT_STATUS command for device($macId) sent via mqtt - ${requestJson.toString()}")

            lastSentGetLightStatusSequenceNumberMqtt = requestJson.getInt(JSON_FRAME_NUM)
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())
        } else {
            var request = BaseRequest(
                    macId = macId,
                    sequenceNumber = Utilities.getRandomSequenceNumber(),
                    packetType = PacketType.LIGHT_STATUS,
                    payLoad = null
            )
            lastSentGetLightStatusSequenceNumberTcp = request.sequenceNumber.toInt()

            Timber.e("GET_LIGHT_STATUS command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun setGroupColor(command: Command) {
        var groupId = command.id as Long
        var rgbPayload = command.payload as HashMap<String, String>

        var lightList = fetchGroupLights(groupId)

        for (groupLight in lightList) {
            Timber.e("Set group color requested for device - ${groupLight.macId}")
            var commmand = Command(EventType.SET_LIGHT_RGB, groupLight.macId, rgbPayload)
            sendCommandToDevice(commmand)
        }

        responseObservable.onNext(Event(EventType.SET_GROUP_COLOR, EventStatus.SUCCESS, getLatestGroup(groupId), null))
    }

    private fun setGroupBrightness(command: Command) {
        var groupId = command.id as Long
        var brightnessValue = command.payload as Int

        var lightList = fetchGroupLights(groupId)

        for (groupLight in lightList) {
            Timber.e("Set group brightness requested for device - ${groupLight.macId}")
            var commmand = Command(
                    EventType.SET_LIGHT_BRIGHTNESS,
                    groupLight.macId,
                    brightnessValue
            )
            sendCommandToDevice(commmand)
        }

        responseObservable.onNext(
                Event(
                        EventType.SET_GROUP_BRIGHTNESS,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    private fun setGroupDaylight(command: Command) {
        var groupId = command.id as Long
        var daylightValue = command.payload as Int

        var lightList = fetchGroupLights(groupId)

        for (groupLight in lightList) {
            Timber.e("Set group daylight requested for device - ${groupLight.macId}")
            var commmand = Command(
                    EventType.SET_LIGHT_DAYLIGHT,
                    groupLight.macId,
                    daylightValue
            )
            sendCommandToDevice(commmand)
        }
        responseObservable.onNext(
                Event(
                        EventType.SET_GROUP_DAYLIGHT,
                        EventStatus.SUCCESS,
                        getLatestGroup(groupId),
                        null
                )
        )
    }

    private fun setGroupScene(command: Command) {
        var groupId = command.id as Long
        var sceneTypePayload = command.payload as SceneType

        var lightList = fetchGroupLights(groupId)

        for (groupLight in lightList) {
            Timber.e("Set group preset scene requested for device - ${groupLight.macId}")
            var commmand = Command(EventType.SET_LIGHT_PRESET_SCENE, groupLight.macId, sceneTypePayload)
            sendCommandToDevice(commmand)
        }

        responseObservable.onNext(Event(EventType.SET_GROUP_SCENE, EventStatus.SUCCESS, getLatestGroup(groupId), null))
    }

    private fun setLightState(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = command.id as String
        var statePayload = command.payload as HashMap<String, Any>
        var sequenceNumber = Utilities.getRandomSequenceNumber()

        var light = getLatestLight(macId)
        pendingLightStatusMap.put(
                macId, LightStatus(
                macId,
                sequenceNumber.toInt(),
                statePayload[BUNDLE_R_VALUE] as String,
                statePayload[BUNDLE_G_VALUE] as String,
                statePayload[BUNDLE_B_VALUE] as String,
                statePayload[BUNDLE_W_VALUE] as String,
                statePayload[BUNDLE_C_VALUE] as String,
                light!!.sceneValue,
                statePayload[BUNDLE_BRIGHTNESS_VALUE] as Int,
                statePayload[BUNDLE_ON_OFF_STATUS] as Boolean,
                light.rssiValue
        )
        )

        if (shouldSendViaMqtt) {

            var request = SetColourRequest(
                    macId,
                    sequenceNumber,
                    statePayload[BUNDLE_R_VALUE] as String,
                    statePayload[BUNDLE_G_VALUE] as String,
                    statePayload[BUNDLE_B_VALUE] as String,
                    statePayload[BUNDLE_W_VALUE] as String,
                    statePayload[BUNDLE_C_VALUE] as String,
                    statePayload[BUNDLE_BRIGHTNESS_VALUE] as Int,
                    statePayload[BUNDLE_ON_OFF_STATUS] as Boolean
            )
            var requestJson: JSONObject = request.getRequestJson()
            Timber.e("SET LIGHT STATE command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())

        } else {
            var request = BaseRequest(
                    macId,
                    sequenceNumber,
                    PacketType.SET_COLOR,
                    SetLampColorRequest(
                            statePayload[BUNDLE_R_VALUE] as String,
                            statePayload[BUNDLE_G_VALUE] as String,
                            statePayload[BUNDLE_B_VALUE] as String,
                            statePayload[BUNDLE_W_VALUE] as String,
                            statePayload[BUNDLE_C_VALUE] as String,
                            statePayload[BUNDLE_BRIGHTNESS_VALUE] as Int,
                            statePayload[BUNDLE_ON_OFF_STATUS] as Boolean
                    )
            )
            Timber.e("SET LIGHT STATE command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun getLatestLight(macId: String): Light? {
        Timber.e("Fetching updated light from DB")
        return localDataSource!!.getLightOfMacId(macId)
    }

    private fun fetchGroupLights(groupId: Long): List<GroupLight> {
        Timber.e("Fetching lights of group - $groupId")
        return localDataSource!!.getLightsOfGroup(groupId)
    }

    private fun parseResponse(responsePayload: Any, isFromMqtt: Boolean) {
        if (isFromMqtt) {
            // Parse mqtt response
            var responseJson = responsePayload as JSONObject

            stopCountdownTimer((responseJson[JSON_MAC_ID] as String), responseJson[JSON_FRAME_NUM] as Int)

            if (responseJson[JSON_STATUS] as Int == 0) {
                // Success
                updateLightStatus((responseJson[JSON_MAC_ID] as String), responseJson[JSON_FRAME_NUM] as Int)

                when (responseJson[JSON_PACKET_ID] as Int) {
                    MqttPacketType.SET_COLOR.value -> {

                        when ((responseJson[JSON_FRAME_NUM] as Int)) {
                            turnOnRequestSequenceNumber -> {
                                if (turnOnRequestSequenceNumber == flickerSequenceNumber) {
                                    removeFromToBeFlickered(responseJson[JSON_MAC_ID] as String)
                                }
                                turnOnRequestSequenceNumber = 0
                                responseObservable.onNext(
                                        Event(
                                                EventType.TURN_ON_LIGHT,
                                                EventStatus.SUCCESS,
                                                getLatestLight((responseJson[JSON_MAC_ID] as String)),
                                                null
                                        )
                                )
                            }
                            turnOffRequestSequenceNumber -> {

                                if (turnOffRequestSequenceNumber == flickerSequenceNumber) {
                                    removeFromToBeFlickered(responseJson[JSON_MAC_ID] as String)
                                }

                                turnOffRequestSequenceNumber = 0
                                responseObservable.onNext(
                                        Event(
                                                EventType.TURN_OFF_LIGHT,
                                                EventStatus.SUCCESS,
                                                getLatestLight((responseJson[JSON_MAC_ID] as String)),
                                                null
                                        )
                                )
                            }
                            setDaylightRequestSequenceNumber -> {
                                setDaylightRequestSequenceNumber = 0

                                if (responseJson[JSON_PACKET_ID] == MqttPacketType.SET_COLOR.value) {
                                    Timber.d("$TAG SET COLOR Command(${responseJson[JSON_FRAME_NUM]}) MQTT response timestamp = ${System.currentTimeMillis()}")
                                }

                                responseObservable.onNext(
                                        Event(
                                                EventType.SET_LIGHT_DAYLIGHT,
                                                EventStatus.SUCCESS,
                                                getLatestLight((responseJson[JSON_MAC_ID] as String)),
                                                null
                                        )
                                )
                            }
                            setBrightnessRequestSequenceNumber -> {
                                setBrightnessRequestSequenceNumber = 0

                                if (responseJson[JSON_PACKET_ID] == MqttPacketType.SET_COLOR.value) {
                                    Timber.d("$TAG SET COLOR Command(${responseJson[JSON_FRAME_NUM]}) MQTT response timestamp = ${System.currentTimeMillis()}")
                                }

                                responseObservable.onNext(
                                        Event(
                                                EventType.SET_LIGHT_BRIGHTNESS,
                                                EventStatus.SUCCESS,
                                                getLatestLight((responseJson[JSON_MAC_ID] as String)),
                                                null
                                        )
                                )
                            }
                            else -> {
                                responseObservable.onNext(
                                        Event(
                                                EventType.SET_LIGHT_RGB,
                                                EventStatus.SUCCESS,
                                                getLatestLight((responseJson[JSON_MAC_ID] as String)),
                                                null
                                        )
                                )
                            }
                        }
                    }
                    MqttPacketType.SET_PRESET_SCENE.value -> {
                        responseObservable.onNext(
                                Event(
                                        EventType.SET_LIGHT_PRESET_SCENE,
                                        EventStatus.SUCCESS,
                                        getLatestLight((responseJson[JSON_MAC_ID] as String)),
                                        null
                                )
                        )
                    }
                    MqttPacketType.GET_FIRMWARE_VERSION.value -> {
                        updateFirmwareVersionInDb(responseJson[JSON_MAC_ID] as String, responseJson[JSON_FIRMWARE_VERSION] as Double)
                    }
                    MqttPacketType.GET_LIGHT_STATUS.value -> {
                        if (responseJson.getInt(JSON_FRAME_NUM) == lastSentGetLightStatusSequenceNumberMqtt) {
                            updateLightStatusInDb(null, null, responseJson)
                        }

                        var lightStatusResponse = LightStatusResponse((responseJson[JSON_MAC_ID] as String), false, (responseJson[JSON_RSSI] as Int))
                        responseObservable.onNext(
                                Event(
                                        EventType.GET_LIGHT_STATUS,
                                        EventStatus.SUCCESS,
                                        lightStatusResponse,
                                        null
                                )
                        )
                    }
                    MqttPacketType.SET_SCHEDULE.value -> {
                        responseObservable.onNext(
                                Event(
                                        EventType.SCHEDULE,
                                        EventStatus.SUCCESS,
                                        null,
                                        null
                                )
                        )
                    }
                    MqttPacketType.RESET.value -> {
                        responseObservable.onNext(
                                Event(
                                        EventType.DELETE_LAMP,
                                        EventStatus.SUCCESS,
                                        responseJson[JSON_MAC_ID] as String,
                                        null
                                )
                        )
                    }


                }
            } else {
                // Error
                removeFromPendingMap((responseJson[JSON_MAC_ID] as String))

                when (responseJson[JSON_PACKET_ID] as Int) {
                    MqttPacketType.SET_COLOR.value -> {

                        when (responseJson[JSON_FRAME_NUM] as Int) {
                            turnOnRequestSequenceNumber -> {
                                turnOnRequestSequenceNumber = 0
                                responseObservable.onNext(
                                        Event(
                                                EventType.TURN_ON_LIGHT,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }
                            turnOffRequestSequenceNumber -> {
                                turnOffRequestSequenceNumber = 0
                                responseObservable.onNext(
                                        Event(
                                                EventType.TURN_OFF_LIGHT,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }
                            setDaylightRequestSequenceNumber -> {
                                setDaylightRequestSequenceNumber = 0
                                responseObservable.onNext(
                                        Event(
                                                EventType.SET_LIGHT_DAYLIGHT,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }
                            setBrightnessRequestSequenceNumber -> {
                                setBrightnessRequestSequenceNumber = 0
                                responseObservable.onNext(
                                        Event(
                                                EventType.SET_LIGHT_BRIGHTNESS,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }
                            else -> {
                                responseObservable.onNext(
                                        Event(
                                                EventType.SET_LIGHT_RGB,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }

                        }

                    }
                    MqttPacketType.SET_PRESET_SCENE.value -> {
                        removeFromPendingMap(responseJson[JSON_MAC_ID] as String)
                        responseObservable.onNext(
                                Event(
                                        EventType.SET_LIGHT_PRESET_SCENE,
                                        EventStatus.FAILURE,
                                        null,
                                        null
                                )
                        )
                    }
                    MqttPacketType.SET_SCHEDULE.value -> {
                        responseObservable.onNext(
                                Event(
                                        EventType.SCHEDULE,
                                        EventStatus.FAILURE,
                                        null,
                                        null
                                )
                        )
                    }

                    MqttPacketType.RESET.value -> {
                        responseObservable.onNext(
                                Event(
                                        EventType.DELETE_LAMP,
                                        EventStatus.FAILURE,
                                        null,
                                        null
                                )
                        )
                    }
                }
            }
        } else {
            // Parse tcp response
            var responseByteArray = responsePayload as ByteArray

            if (responseByteArray.isNotEmpty()) {

                var responsePacket =
                        BaseResponse(responseByteArray)

                stopCountdownTimer(responsePacket.getMacId(), responsePacket.getSequenceNumber())

                when (responsePacket.packetType) {
                    PacketType.SET_WIFI -> {
                        var setWifiResponse = SetWifiCredResponse(
                                responsePacket.getPayloadInstance().byteArray,
                                responsePacket.getSequenceNumber(),
                                responsePacket.getMacId()
                        )
                        if (setWifiResponse.isSuccess) {
                            disconnectDefaultSocket()

                            responseObservable.onNext(
                                    Event(
                                            EventType.SET_WIFI,
                                            EventStatus.SUCCESS,
                                            null,
                                            null
                                    )
                            )

                            insertLightIntoDb(pendingSsid, responsePacket.getMacId(), setWifiResponse.lampType)
                            addDeviceAsToBeFlickered(responsePacket.getMacId())

                            // insertDeviceIntoServer(responsePacket.getMacId(), setWifiResponse.lampType)
                        } else {
                            responseObservable.onNext(
                                    Event(
                                            EventType.SET_WIFI,
                                            EventStatus.FAILURE,
                                            null,
                                            null
                                    )
                            )
                        }
                    }

                    PacketType.SET_COLOR -> {
                        var setLightColorResponse = SetLampColorResponse(
                                responsePacket.getPayloadInstance().byteArray,
                                responsePacket.getSequenceNumber()
                        )
                        if (setLightColorResponse.isSuccess) {
                            updateLightStatus(responsePacket.getMacId(), responsePacket.getSequenceNumber())
                            removeFromLastSentTcpCommandMap(responsePacket.getMacId())

                            when (responsePacket.getSequenceNumber()) {
                                turnOffRequestSequenceNumber -> {

                                    if (turnOffRequestSequenceNumber == flickerSequenceNumber) {
                                        removeFromToBeFlickered(responsePacket.getMacId())
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.TURN_OFF_LIGHT,
                                                    EventStatus.SUCCESS,
                                                    getLatestLight(responsePacket.getMacId()),
                                                    null
                                            )
                                    )
                                }
                                turnOnRequestSequenceNumber -> {

                                    if (turnOnRequestSequenceNumber == flickerSequenceNumber) {
                                        removeFromToBeFlickered(responsePacket.getMacId())
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.TURN_ON_LIGHT,
                                                    EventStatus.SUCCESS,
                                                    getLatestLight(responsePacket.getMacId()),
                                                    null
                                            )
                                    )
                                }
                                setBrightnessRequestSequenceNumber -> {

                                    if (responsePacket.packetType.value.equals(PacketType.SET_COLOR.value)) {
                                        Timber.d("$TAG SET COLOR Command(${responsePacket.getSequenceNumber()}) TCP response timestamp = ${System.currentTimeMillis()}")
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.SET_LIGHT_BRIGHTNESS,
                                                    EventStatus.SUCCESS,
                                                    getLatestLight(responsePacket.getMacId()),
                                                    null
                                            )
                                    )
                                }
                                setDaylightRequestSequenceNumber -> {

                                    if (responsePacket.packetType.value.equals(PacketType.SET_COLOR.value)) {
                                        Timber.d("$TAG SET COLOR Command(${responsePacket.getSequenceNumber()}) TCP response timestamp = ${System.currentTimeMillis()}")
                                    }

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.SET_LIGHT_DAYLIGHT,
                                                    EventStatus.SUCCESS,
                                                    getLatestLight(responsePacket.getMacId()),
                                                    null
                                            )
                                    )
                                }
                                else -> {
                                    responseObservable.onNext(
                                            Event(
                                                    EventType.SET_LIGHT_RGB,
                                                    EventStatus.SUCCESS,
                                                    getLatestLight(responsePacket.getMacId()),
                                                    null
                                            )
                                    )
                                }
                            }
                        } else {
                            removeFromPendingMap(responsePacket.getMacId())

                            when (responsePacket.getSequenceNumber()) {
                                turnOffRequestSequenceNumber -> {
                                    responseObservable.onNext(
                                            Event(
                                                    EventType.TURN_OFF_LIGHT,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                                turnOnRequestSequenceNumber -> {
                                    responseObservable.onNext(
                                            Event(
                                                    EventType.TURN_ON_LIGHT,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                                setBrightnessRequestSequenceNumber -> {
                                    responseObservable.onNext(
                                            Event(
                                                    EventType.SET_LIGHT_BRIGHTNESS,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                                setDaylightRequestSequenceNumber -> {
                                    responseObservable.onNext(
                                            Event(
                                                    EventType.SET_LIGHT_DAYLIGHT,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                                else -> {
                                    responseObservable.onNext(
                                            Event(
                                                    EventType.SET_LIGHT_RGB,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                            }
                        }
                    }
                    PacketType.SET_SCENE -> {
                        var setLightSceneResponse = SetLampSceneResponse(
                                responsePacket.getPayloadInstance().byteArray,
                                responsePacket.getSequenceNumber()
                        )
                        if (setLightSceneResponse.isSuccess) {
                            updateLightStatus(responsePacket.getMacId(), responsePacket.getSequenceNumber())
                            removeFromLastSentTcpCommandMap(responsePacket.getMacId())

                            responseObservable.onNext(
                                    Event(
                                            EventType.SET_LIGHT_PRESET_SCENE,
                                            EventStatus.SUCCESS,
                                            getLatestLight(responsePacket.getMacId()),
                                            null
                                    )
                            )

                        } else {
                            removeFromPendingMap(responsePacket.getMacId())
                            responseObservable.onNext(
                                    Event(
                                            EventType.SET_LIGHT_PRESET_SCENE,
                                            EventStatus.FAILURE,
                                            null,
                                            null
                                    )
                            )
                        }
                    }

                    PacketType.GET_FIRMWARE_VERSION -> {
                        var getFirmwareVersionResponse = responsePacket.payLoad as GetFirmwareVersionResponse
                        if (getFirmwareVersionResponse.isSuccess) {
                            updateFirmwareVersionInDb(responsePacket.getMacId(), getFirmwareVersionResponse.firmwareVersion)
                            removeFromLastSentTcpCommandMap(responsePacket.getMacId())

//                            responseObservable.onNext(
//                                    Event(
//                                            EventType.GET_FIRMWARE_VERSION,
//                                            EventStatus.SUCCESS,
//                                            null,
//                                            null
//                                    )
//                            )

                        } else {
                            removeFromPendingMap(responsePacket.getMacId())
//                            responseObservable.onNext(
//                                    Event(
//                                            EventType.GET_FIRMWARE_VERSION,
//                                            EventStatus.FAILURE,
//                                            null,
//                                            null
//                                    )
//                            )
                        }
                    }

                    PacketType.LIGHT_STATUS -> {
                        var getLightStatusResponse = GetLightStatusResponse(
                                responsePacket.getPayloadInstance().byteArray,
                                responsePacket.getSequenceNumber()
                        )
                        if (getLightStatusResponse.isSuccess) {
                            if (responsePacket.getSequenceNumber() == lastSentGetLightStatusSequenceNumberTcp) {
                                updateLightStatusInDb(responsePacket.getMacId(), getLightStatusResponse, null)
                            }
                            removeFromLastSentTcpCommandMap(responsePacket.getMacId())

                            var lightStatusResponse = LightStatusResponse(responsePacket.getMacId(), true, getLightStatusResponse.rssiValue)

                            responseObservable.onNext(
                                    Event(
                                            EventType.GET_LIGHT_STATUS,
                                            EventStatus.SUCCESS,
                                            lightStatusResponse,
                                            null
                                    )
                            )

                        } else {
                            removeFromPendingMap(responsePacket.getMacId())
                            responseObservable.onNext(
                                    Event(
                                            EventType.GET_LIGHT_STATUS,
                                            EventStatus.FAILURE,
                                            null,
                                            null
                                    )
                            )
                        }
                    }
                    PacketType.SET_SCHEDULE -> {
                        var setScheduleResponse = SetScheduleResponse(
                                responsePacket.getPayloadInstance().byteArray,
                                responsePacket.getSequenceNumber()
                        )
                        if (setScheduleResponse.isSuccess) {
                            removeFromLastSentTcpCommandMap(responsePacket.getMacId())
                            responseObservable.onNext(
                                    Event(
                                            EventType.SCHEDULE,
                                            EventStatus.SUCCESS,
                                            null,
                                            null
                                    )
                            )
                        } else {
                            responseObservable.onNext(
                                    Event(
                                            EventType.SCHEDULE,
                                            EventStatus.FAILURE,
                                            null,
                                            null
                                    )
                            )
                        }
                    }

                    PacketType.RESET_LAMP -> {
                        var resetLampResponse = ResetLampResponse(
                                responsePacket.getPayloadInstance().byteArray,
                                responsePacket.getSequenceNumber()
                        )
                        deleteLampSequenceNumber = null
                        if (resetLampResponse.isSuccess) {
                            removeFromLastSentTcpCommandMap(responsePacket.getMacId())
                            addDeviceToBeDeleted(responsePacket.getMacId())
                            removeFromToBeFlickered(responsePacket.getMacId())
                            var latestLamp = getLatestLight(responsePacket.getMacId())
                            if (latestLamp != null) {
                                deleteLightFromDb(responsePacket.getMacId())
                                if (latestLamp.isSynced == LIGHT_SYNCED) {
                                    deleteLampFromServer(latestLamp.macId, latestLamp.projectId, latestLamp.networkId)
                                } else {
                                    removeFromToBeDeleted(responsePacket.getMacId())
                                }
                            }
                        } else {
                            responseObservable.onNext(
                                    Event(
                                            EventType.DELETE_LAMP,
                                            EventStatus.FAILURE,
                                            null,
                                            null
                                    )
                            )
                        }
                    }
                }
            }
        }
    }

    private fun addDeviceAsToBeFlickered(macId: String) {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        sharedPreference.addDeviceToFlicker(macId)
    }

    private fun removeFromToBeFlickered(macId: String) {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }

        sharedPreference.removeDeviceToBeFlickered(macId)
    }

    private fun updateFirmwareVersionInDb(macId: String, firmwareVersion: Double) {
        localDataSource.updateLightFirmware(macId, firmwareVersion.toString())
    }

    private fun updateFirmwareVersionInDb(macId: String, firmwareVersion: String) {
        localDataSource.updateLightFirmware(macId, firmwareVersion)
    }

    private fun addDeviceToBeDeleted(macId: String) {
        val light = getLatestLight(macId)
        if (light != null) {
            val oMapper = ObjectMapper()
            val typeRef = object : TypeReference<HashMap<String, Any>>() {}
            val map = oMapper.convertValue<HashMap<String, Any>>(light, typeRef)
            val hashMapString = Gson().toJson(map)

            if (sharedPreference == null) {
                sharedPreference = SharedPreference(application)
            }
            sharedPreference.addDeviceToBeDeleted(macId, hashMapString)
        } else {
            // Should not occur
            Timber.d("Device($macId) doesnt exist in local db")
        }
    }

    private fun removeFromToBeDeleted(macId: String) {
        if (sharedPreference == null) {
            sharedPreference = SharedPreference(application)
        }
        sharedPreference.deleteDeviceToBeDeleted(macId)
    }

    private fun deleteLampFromServer(macId: String, projectId: Long, networkId: Long) {
        if (GeneralUtils.isInternetAvailable(application)) {
            var userId = sharedPreference.getUserId()

            disposables.add(ApiRequestParser.deleteLamp(userId, projectId.toInt(), networkId.toInt(), macId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableObserver<Response<DeleteLightResponse>>() {
                        override fun onComplete() {
                            Timber.d("$TAG Completed delete light api")
                        }

                        override fun onNext(response: Response<DeleteLightResponse>) {
                            if (response.isSuccessful) {
                                if (response.body() != null) {

                                    if (response.body()!!.status.equals("success")) {
                                        Timber.d("$TAG Successfully deleted light($macId) from the server")
                                        removeFromToBeDeleted(macId)
                                        deleteLightFromDb(macId)
                                    } else {
                                        Timber.d("$TAG Failed to remove light($macId) from server")
                                        if (response.body()!!.description != null) {
                                            Timber.d("$TAG Error response - ${response.body()!!.description}")
                                        } else {
                                            Timber.d("$TAG No error response received from server")
                                        }
                                        responseObservable.onNext(
                                                Event(
                                                        EventType.DELETE_LAMP,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }

                                } else {
                                    Timber.d("$TAG Failed to remove light($macId) from server")
                                    Timber.d("$TAG No response received from server")

                                    responseObservable.onNext(
                                            Event(
                                                    EventType.DELETE_LAMP,
                                                    EventStatus.FAILURE,
                                                    null,
                                                    null
                                            )
                                    )
                                }
                            } else {
                                Timber.d("$TAG Failed to remove the light ($macId) from the server")
                                Timber.d("Response code = ${response.code()}")
                                Timber.d("Response body = ${response.body()}")
                                Timber.d("Response errorbody = ${response.errorBody()}")

                                responseObservable.onNext(
                                        Event(
                                                EventType.DELETE_LAMP,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }
                        }

                        override fun onError(e: Throwable) {
                            Timber.d("$TAG Failed to remove the light ($macId) from the server")
                            e.printStackTrace()
                            responseObservable.onNext(
                                    Event(
                                            EventType.DELETE_LAMP,
                                            EventStatus.FAILURE,
                                            null,
                                            null
                                    )
                            )
                        }

                    })
            )
        } else {
            responseObservable.onNext(
                    Event(
                            EventType.DELETE_LAMP,
                            EventStatus.FAILURE,
                            NO_INTERNET_AVAILABLE,
                            null
                    )
            )
        }
    }

    private fun deleteLightFromDb(macId: String) {
        localDataSource.deleteLight(macId)
        Timber.d("Successfully deleted the light from local database")
        macIdClientMap.remove(macId)
        sharedPreference.removePendingDevice(macId)
        responseObservable.onNext(Event(EventType.DELETE_LAMP, EventStatus.SUCCESS, null, null))
    }

    private fun updateLightStatusInDb(
            macId: String?,
            lightStatusResponseFromTcp: GetLightStatusResponse?,
            lightStatusJsonFromMqtt: JSONObject?
    ) {
        if (lightStatusResponseFromTcp != null && macId != null && lightStatusJsonFromMqtt == null) {
            Timber.e("Received latest state of device($macId) from TCP")
            localDataSource.updateLightStatus(
                    macId,
                    lightStatusResponseFromTcp.rValue,
                    lightStatusResponseFromTcp.gValue,
                    lightStatusResponseFromTcp.bValue,
                    lightStatusResponseFromTcp.wValue,
                    lightStatusResponseFromTcp.cValue,
                    lightStatusResponseFromTcp.scnValue,
                    lightStatusResponseFromTcp.lampBrightness,
                    lightStatusResponseFromTcp.lampTurnedOn,
                    lightStatusResponseFromTcp.rssiValue
            )
            Timber.e("Updated device($macId) status in local database")
        } else if (lightStatusJsonFromMqtt != null && lightStatusResponseFromTcp == null && macId == null) {
            Timber.e("Received latest state of device(${lightStatusJsonFromMqtt[JSON_MAC_ID] as String}) from MQTT")

            localDataSource.updateLightStatus(
                    lightStatusJsonFromMqtt[JSON_MAC_ID] as String,
                    Utilities.convertIntToHexString(lightStatusJsonFromMqtt[JSON_R_VALUE] as Int),
                    Utilities.convertIntToHexString(lightStatusJsonFromMqtt[JSON_G_VALUE] as Int),
                    Utilities.convertIntToHexString(lightStatusJsonFromMqtt[JSON_B_VALUE] as Int),
                    Utilities.convertIntToHexString(lightStatusJsonFromMqtt[JSON_W_VALUE] as Int),
                    Utilities.convertIntToHexString(lightStatusJsonFromMqtt[JSON_C_VALUE] as Int),
                    Utilities.convertIntToHexString(lightStatusJsonFromMqtt[JSON_SCENE_NUM] as Int),
                    lightStatusJsonFromMqtt[JSON_BRIGHTNESS_VALUE] as Int,
                    ((lightStatusJsonFromMqtt[JSON_ON_OFF_STATUS] as Int) == 1),
                    (lightStatusJsonFromMqtt[JSON_RSSI] as Int)
            )
            Timber.e("Updated device(${lightStatusJsonFromMqtt[JSON_MAC_ID] as String}) status in local database")
        }
    }

    private fun removeFromLastSentTcpCommandMap(macId: String) {
        if (lastSentCommandViaTcp.containsKey(macId)) {
            Timber.e("Removing($macId) from last sent TCP command map")
            lastSentCommandViaTcp.remove(macId)
        } else {
            // Ideally this shouldn't be executed
            Timber.e("Command of device($macId) doesnt exist in TCP command map")
        }
    }

    private fun unBindProcessToNetwork() {
        //connectivityManager.bindProcessToNetwork(null)
    }

    private fun insertLightIntoDb(
            name: String,
            macId: String,
            lampType: LampType
    ) {
        var networkId = sharedPreference.getNetworkId()
        var projectId = sharedPreference.getProjectId()

        Timber.e("Adding light into Lights table - macId($macId), ssid($name), networkId($networkId)")
        var lampName = name
        lampName = lampName.replace("\"", "")
        var newLight = Light.newInstance(lampName, macId, networkId, projectId, lampType.value, 0) // Uncomment
        //var newLight = Light(pendingSsid, macId, pendingNetworkId, LampType.COLOR_AND_DAYLIGHT.value) // comment
        localDataSource!!.addNewLight(newLight)

        val oMapper = ObjectMapper()
        val typeRef = object : TypeReference<HashMap<String, Any>>() {}
        val map = oMapper.convertValue<HashMap<String, Any>>(newLight, typeRef)
        val hashMapString = Gson().toJson(map)
        sharedPreference.addPendingDevice(macId, hashMapString)


        responseObservable.onNext(Event(EventType.ADD_NEW_LIGHT, EventStatus.SUCCESS, name, null))
    }

    private fun updateLightStatus(macId: String, sequenceNumber: Int) {
        if (pendingLightStatusMap.get(macId) != null) {
            var pendingLightStatus = pendingLightStatusMap.get(macId)
            if (pendingLightStatus?.sequenceNumber == sequenceNumber) {
                Timber.e("Successfully updated status of device($macId)")
                localDataSource!!.updateLightStatus(
                        macId,
                        pendingLightStatus!!.r_value,
                        pendingLightStatus.g_value,
                        pendingLightStatus.b_value,
                        pendingLightStatus.w_value,
                        pendingLightStatus.c_value,
                        pendingLightStatus.scene_value,
                        pendingLightStatus.lamp_brightness,
                        pendingLightStatus.lamp_turned_on,
                        pendingLightStatus.lamp_rssi_value
                )

                pendingLightStatusMap.remove(macId)
            } else {
                Timber.e("Didnt get the success response for command(${pendingLightStatus?.sequenceNumber}) of device($macId)")
            }
        } else {
            Timber.e("Failed to update light status")
            Timber.e("$macId doesn't exist in pending map")
        }
    }

    private fun getLatestGroup(groupId: Long): com.svarochi.wifi.database.entity.Group {
        return localDataSource!!.getLatestGroup(groupId)
    }

    private fun removeFromPendingMap(macId: String) {
        if (pendingLightStatusMap.get(macId) != null) {
            Timber.e("Removing pending light status of $macId")
            pendingLightStatusMap.remove(macId)
        } else {
            Timber.e("Couldn't remove the light status of $macId")
            Timber.e("$macId doesn't exist in pending map")
        }
    }


    private fun getAllLights(): List<Light> {
        Timber.e("Fetching all lights from DB")
        return localDataSource!!.getAllLights()
    }


    private fun updateLightSyncStatus(macId: String, status: Int) {
        localDataSource.updateLightSyncStatus(macId, status)
        Timber.d("$TAG Updated light($macId) sync status($status)")
    }

    private fun removePendingDeviceFromPreference(macId: String) {
        sharedPreference.removePendingDevice(macId)
    }


    private fun insertDeviceIntoServer(
            name: String,
            macId: String,
            lampType: String,
            networkId: Long,
            projectId: Long
    ) {
        Timber.e("Adding device($macId) into server")

        // Clear pending device details
        var apiLampType = BUNDLE_API_DIMMABLE
        when (lampType) {
            LampType.BRIGHT_AND_DIM.value -> {
                apiLampType = BUNDLE_API_DIMMABLE
            }
            LampType.COLOR_AND_DAYLIGHT.value -> {
                apiLampType = BUNDLE_API_COLOR
            }
            LampType.WARM_AND_COOL.value -> {
                apiLampType = BUNDLE_API_DAYLIGHT
            }
        }
        var userId = sharedPreference.getUserId()

        if (GeneralUtils.isInternetAvailable(this)) {

        }

        Timber.d("IsInternetAvailable - ${GeneralUtils.isInternetAvailable(this)}")
        disposables.add(
                ApiRequestParser.addDevice(
                        userId,
                        name,
                        apiLampType,
                        macId,
                        networkId.toInt(),
                        projectId.toInt())
                        .retry(1)
                        .subscribeWith(object : DisposableObserver<Response<AddDeviceResponse>>() {
                            override fun onComplete() {
                                Timber.e("Completed Add Device api")
                            }

                            override fun onNext(response: Response<AddDeviceResponse>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        Timber.e("Add Device API response - ${response.toString()}")
                                        Timber.e("Add Device API response body - ${response.body().toString()}")
                                        if (response.body()!!.status.contains("success", true)) {
                                            Timber.e("Successfully added device into server")
                                            updateLightSyncStatus(macId, 1)
                                            removePendingDeviceFromPreference(macId)
                                        } else {
                                            Timber.e("OnNext - Something went wrong while adding device into server")

                                            if (getErrorMessage(response) != null) {
                                                // TODO - Handle error message
                                            } else {
                                                // TODO - Handle no error message
                                            }
                                            responseObservable.onNext(
                                                    Event(
                                                            EventType.ADD_NEW_LIGHT,
                                                            EventStatus.FAILURE,
                                                            null,
                                                            null
                                                    )
                                            )
                                        }
                                    } else {
                                        Timber.e("OnNext - Something went wrong while adding device into server")
                                        Timber.e("No response received from server")
                                        responseObservable.onNext(
                                                Event(
                                                        EventType.ADD_NEW_LIGHT,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }
                                } else {
                                    if (response.code() == 409) {
                                        Timber.e("Add Device API Response - ${response.toString()}")
                                        Timber.e("Add Device API Response body - ${response.errorBody().toString()}")
                                        Timber.e("Add Device API Response - Device($macId) exists with another user")

                                        responseObservable.onNext(
                                                Event(
                                                        EventType.ADD_NEW_LIGHT,
                                                        EventStatus.FAILURE,
                                                        ALREADY_EXISTS_WITH_ANOTHER_USER,
                                                        null
                                                )
                                        )
                                    } else {
                                        Timber.e("OnNext - Something went wrong while adding device into server")
                                        if (getErrorMessage(response) != null) {
                                            // TODO - Handle error message
                                        } else {
                                            // TODO - Handle no error message
                                        }
                                        responseObservable.onNext(
                                                Event(
                                                        EventType.ADD_NEW_LIGHT,
                                                        EventStatus.FAILURE,
                                                        null,
                                                        null
                                                )
                                        )
                                    }
                                }
                            }

                            override fun onError(error: Throwable) {
                                Timber.e("OnError - Something went wrong while adding device into server")
                                Timber.e(error)
                                responseObservable.onNext(
                                        Event(
                                                EventType.ADD_NEW_LIGHT,
                                                EventStatus.FAILURE,
                                                null,
                                                null
                                        )
                                )
                            }
                        })
        )
    }


    private fun sendCommandViaMqtt(macId: String, requestJson: JSONObject, shouldInitiateTimer: Boolean) {
        if (connectedToMobileData || connectedToWifi) {
            Timber.e("Command sent via Mqtt to the device($macId) - ${requestJson.toString()}")
            azureMqttHelper.sendCommand(requestJson)

            if (shouldInitiateTimer) {
                initiateTimer(macId, requestJson[JSON_FRAME_NUM] as Int, MQTT_CONNECTION)
            }
        } else {
            Timber.e("Device not connected to MQTT broker")
            stopCountdownTimer(macId)
            responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, macId, null))
        }
    }

    private fun getLampName(macId: String): String {
        if (macId.equals(DEFAULT_MAC_ID)) {
            if (pendingSsid != null) {
                return pendingSsid
            } else {
                return "Device"
            }
        } else {
            if (!localDataSource.getLightName(macId).isNullOrEmpty())
                return localDataSource.getLightName(macId)!!
            else
                return "Device"
        }
    }

    fun initCallbacks() {
        connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        localDataSource = (application as SvarochiApplication).wifiLocalDatasource
        sharedPreference = SharedPreference(this)
        registerWifiCallback()
        registerMobileNetworkCallback()
    }

    private fun registerWifiCallback() {
        var builder = NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build()
        connectivityManager.registerNetworkCallback(builder, wifiCallback)
        Timber.e("Network change callback registered")
    }

    private fun unRegisterWifiCallback() {
        connectivityManager.unregisterNetworkCallback(wifiCallback)
        Timber.e("Network change callback unregistered")
    }

    override fun onCreate() {
        super.onCreate()
        initCallbacks()

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        return START_NOT_STICKY
    }

    fun isDeviceLocallyAvailable(macId: String): Boolean {
        if (macIdClientMap.containsKey(macId)) {
            var differenceTime = (System.currentTimeMillis() - macIdClientMap[macId]!!.lastCommandSentTimestamp)
            Timber.e("Current system time = ${System.currentTimeMillis()}")
            Timber.e("Timestamp when last command was sent via TCP = ${macIdClientMap[macId]!!.lastCommandSentTimestamp}")
            Timber.e("Difference between the timestamps = ${differenceTime}")
            Timber.e("Difference between the timestamps in minutes = ${(differenceTime / 60000)}")

            return differenceTime < 120000
        } else {
            return false
        }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        /*if (mqttManager != null) {
            mqttManager!!.disconnect()
        }*/
        isAppicationLive = false
        onDestroy()
        stopSelf()
    }

    fun isMusicActivity(flag: Boolean) {
        Timber.d("Setting AudioFlag - $flag")
        isMusicActivity = flag
    }


    fun checkIfDeviceIsAvailableViaMqtt(macId: String) {
        if (connectedToWifi || connectedToMobileData) {
            getLightStatusFromDevice(macId, false)
        } else {
            Timber.d("$TAG Mqtt is not connected")
            var map = HashMap<String, String>()
            map[macId] = "MQTT"
            responseObservable.onNext(Event(EventType.GET_LIGHT_STATUS, EventStatus.FAILURE, map, null))
        }
    }

    private fun deleteLamp(command: Command, shouldSendViaMqtt: Boolean) {
        var macId = command.id as String
        deleteLampSequenceNumber = Utilities.getRandomSequenceNumber()

        if (shouldSendViaMqtt) {
            // Delete command will never be sent via Mqtt

            var request = ResetLampRequest(
                    macId,
                    deleteLampSequenceNumber!!
            )
            var requestJson = request.getRequestJson()
            Timber.e("RESET LIGHT command for device($macId) sent via mqtt - ${requestJson.toString()}")
            sendCommandViaMqtt(macId, requestJson, isMusicActivity.not())
        } else {
            var request = BaseRequest(
                    macId = macId,
                    sequenceNumber = deleteLampSequenceNumber!!,
                    packetType = PacketType.RESET_LAMP,
                    payLoad = null
            )
            Timber.e("RESET LIGHT command for device($macId) sent via tcp - ${Utilities.toString(request.getByteArray())}")
            sendCommandViaTcp(macId, request, isMusicActivity.not(), isMusicActivity.not())
        }
    }

    private fun getRouterBroadcastAddress() {
        Utilities.findRouterBCastAddress()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Timber.d("Finding router broadcast address")
                }
                .subscribe(object : DisposableObserver<String>() {
                    override fun onComplete() {
                    }

                    override fun onNext(bcastAddress: String) {
                        Timber.d("Found Router BCAST ADDRESS - $bcastAddress")
                        if (isAlreadyBroadcasting.not()) {
                            performGenericBroadcast(bcastAddress)
                        }
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                        responseObservable.onNext(Event(EventType.UDP_BROADCAST, EventStatus.ERROR, null, e))
                    }
                })
    }

    private fun performUdpBroadcast() {
        /**
         * Steps involved -
         * 1. Make common UDP broadcast for all the devices 3 times with a gap of 2 seconds
         * 2. Make UDP broadcast for those devices which didnt respond in the step 1 for 6 times with a gap of 2 seconds.
         * 3. Mark those devices which didnt respond after step 1 and 2 as not available.
         *
         * Note -
         * 1. broadcast only when phone wifi network and current svarochi network ssid are matching
         * 2. broadcast to only those devices that are only that are there in the current svarochi network
         * 3. broadcast continously in background
         *
         * Sample impl -
         * 1. one function to get list of lights that are not locally available
         * 2. another function which has for loop which broadcasts for each device
         * 3. above both functions are run with 2 seconds delay for 6 times
         *
         *
         * */

        if (sharedPreference != null && localDataSource != null && sharedPreference.getNetworkId() != (-1).toLong()) {
            var currentNetwork = localDataSource.getNetwork(sharedPreference.getNetworkId())[0]
            if (wifiManager != null && wifiManager.connectionInfo != null && wifiManager.connectionInfo.ssid != null) {
                var phoneWifi = wifiManager.connectionInfo.ssid.replace("\"", "", true)
                if (currentNetwork.wifiSsid.equals(phoneWifi, false) && shouldBroadcast && isAlreadyBroadcasting.not()) {
                    getRouterBroadcastAddress()
                } else {
                    if (shouldBroadcast.not()) {
                        Timber.d("No need to perform UDP broadcast")
                    } else if (isAlreadyBroadcasting) {
                        Timber.d("Already performing UDP broadcast")
                    } else if (currentNetwork.wifiSsid.equals(phoneWifi, false)) {
                        Timber.d("UDP broadcast stopped - Phone and current network not in same wifi network")
                        Timber.d("Phone network - ${phoneWifi}")
                        Timber.d("Current network ssid - ${currentNetwork.wifiSsid}")
                    } else {
                        Timber.d("Failed to perform UDP Broadcast")
                    }
                }
            } else {
                Timber.d("UDP broadcast stopped - Failed to fetch current connect wifi network")
            }
        } else {
            Timber.d("UDP broadcast stopped - Failed to fetch proper network id")
        }
    }

    private fun performGenericBroadcast(routerIpAddress: String) {

        /*for(macId in macIdClientMap.keys) {
            if (macId != DEFAULT_MAC_ID && macIdClientMap[macId] != null && macIdClientMap[macId]!!.retryCount < MAX_CONNECTION_RETRY) {
                macIdClientMap[macId]!!.retryCount++
                Timber.d("Retrying TCP connection with $macId - ${macIdClientMap[macId]!!.socket.mConfig.mIp!!}")
                var ipAddress: String = macIdClientMap[macId]!!.socket.mConfig.mIp!!
                var port: Int = macIdClientMap[macId]!!.socket.mConfig.mPort!!
                connect(macId, ipAddress, port)
            } else {
                if (macIdClientMap[macId] != null && macIdClientMap[macId]!!.retryCount == MAX_CONNECTION_RETRY) {
                    Timber.d("$TAG Maximum tcp connection retries made to device $macId")
                }
                macIdClientMap.remove(macId)
                var lightName = getLampName(macId)
                responseObservable.onNext(Event(EventType.CONNECTION, EventStatus.FAILURE, lightName, null))
            }
        }*/

        isAlreadyBroadcasting = true

        var genericBroadcastCount = 0
        var GENERIC_BROADCAST_COUNT = 3
        var GENERIC_BROADCAST_TIMEGAP: Long = 2
        var broadcastDisposable: Disposable? = null

        Observable
                .interval(0, GENERIC_BROADCAST_TIMEGAP, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .takeWhile(object : Predicate<Long> {
                    override fun test(t: Long): Boolean {
                        return (genericBroadcastCount <= GENERIC_BROADCAST_COUNT)
                    }
                })
                .doOnSubscribe {
                    broadcastDisposable = it!!
                    broadcastDisposables.add(it)
                }
                .subscribe(object : DisposableObserver<Long>() {
                    override fun onComplete() {
                        if (broadcastDisposable != null) {
                            disposeBroadcast(broadcastDisposable!!)
                        }
                        performSpecificBroadcast(routerIpAddress)
                        //responseObservable.onNext(Event(EventType.UDP_BROADCAST, EventStatus.COMPLETE, null, null))
                    }

                    override fun onNext(t: Long) {
                        genericBroadcastCount++
                        sendBroadcast(
                                routerIpAddress,
                                Constants.DEFAULT_BCAST_PORT,
                                null)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                    }

                })
    }

    private fun disposeBroadcast(broadcastDisposable: Disposable) {
        broadcastDisposable.dispose()
    }

    private fun performSpecificBroadcast(routerIpAddress: String) {
        deviceBroadcastCountMap.clear()
        var DEVICE_SPECIFIC_BCAST_DELAY: Long = 4000

        if (sharedPreference != null && localDataSource != null && sharedPreference.getNetworkId() != (-1).toLong()) {
            var lightsList = localDataSource.getLights(sharedPreference.getNetworkId())

            Handler().postDelayed({
                var macIdList = ArrayList<String>()
                for (light in lightsList) {
                    if (macIdClientMap.containsKey(light.macId).not()) {
                        macIdList.add(light.macId)
                    }
                }
                if (macIdList.isNotEmpty()) {
                    performDeviceBroadcast(routerIpAddress, macIdList)
                } else {
                    stopBroadcast()
                }
            }, DEVICE_SPECIFIC_BCAST_DELAY)
        } else {
            Timber.d("Device specific UDP broadcast stopped - Failed to fetch proper network id")
        }
    }

    private fun performDeviceBroadcast(routerIpAddress: String, macIdList: ArrayList<String>) {
        var DEVICE_BROADCAST_COUNT = 6
        var DEVICE_BROADCAST_TIMEGAP: Long = 2

        if (sharedPreference != null && localDataSource != null && sharedPreference.getNetworkId() != (-1).toLong()) {

            var currentNetwork = localDataSource.getNetwork(sharedPreference.getNetworkId())[0]
            if (wifiManager != null && wifiManager.connectionInfo != null && wifiManager.connectionInfo.ssid != null) {

                var phoneWifi = wifiManager.connectionInfo.ssid.replace("\"", "", true)
                if (currentNetwork.wifiSsid.equals(phoneWifi, false) && shouldBroadcast) {

                    for (macId in macIdList) {
                        deviceBroadcastCountMap[macId] = DeviceBroadcast(null, 0)

                        Observable
                                .interval(0, DEVICE_BROADCAST_TIMEGAP, TimeUnit.SECONDS)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .takeWhile(object : Predicate<Long> {
                                    override fun test(t: Long): Boolean {
                                        if (deviceBroadcastCountMap[macId] != null) {
                                            return (deviceBroadcastCountMap[macId]!!.count < DEVICE_BROADCAST_COUNT)
                                        } else {
                                            return false
                                        }
                                    }
                                })
                                .doOnSubscribe {
                                    if (deviceBroadcastCountMap[macId] != null) {
                                        deviceBroadcastCountMap[macId]!!.disposable = it!!
                                    }
                                    broadcastDisposables.add(it)
                                }
                                .subscribe(object : DisposableObserver<Long>() {
                                    override fun onComplete() {
                                        if (deviceBroadcastCountMap[macId] != null && deviceBroadcastCountMap[macId]!!.disposable != null) {
                                            disposeBroadcast(deviceBroadcastCountMap[macId]!!.disposable!!)

                                            Timber.d("Udp broadcast completed for device $macId - ${deviceBroadcastCountMap[macId]!!.count}")
                                            deviceBroadcastCountMap.remove(macId)
                                            macIdList.remove(macId)
                                        }

                                        if ((macIdList.size - deviceBroadcastCountMap.keys.size).absoluteValue == macIdList.size) {
                                            Timber.d("Udp broadcast completed to all devices")
                                            stopBroadcast()
                                            performUdpBroadcast()
                                        }
                                        //responseObservable.onNext(Event(EventType.UDP_BROADCAST, EventStatus.COMPLETE, null, null))
                                    }

                                    override fun onNext(t: Long) {
                                        if (macIdClientMap.containsKey(macId).not()) {
                                            if (deviceBroadcastCountMap[macId] != null) {
                                                deviceBroadcastCountMap[macId]!!.count++
                                            }

                                            sendBroadcast(
                                                    routerIpAddress,
                                                    Constants.DEFAULT_BCAST_PORT,
                                                    macId)
                                        } else {
                                            onComplete()
                                        }
                                    }

                                    override fun onError(e: Throwable) {
                                        Timber.e(e)
                                    }

                                })
                    }
                } else {
                    if (shouldBroadcast) {
                        Timber.d("No need to perform UDP broadcast")
                    } else if (isAlreadyBroadcasting) {
                        Timber.d("Already performing UDP broadcast")
                    } else if (currentNetwork.wifiSsid.equals(phoneWifi, false)) {
                        Timber.d("UDP broadcast stopped - Phone and current network not in same wifi network")
                        Timber.d("Phone network - ${phoneWifi}")
                        Timber.d("Current network ssid - ${currentNetwork.wifiSsid}")
                    } else {
                        Timber.d("Failed to perform Specific UDP Broadcast")
                    }
                }
            } else {
                Timber.d("Specific UDP broadcast stopped - Failed to fetch current connect wifi network")
            }
        } else {
            Timber.d("Specific UDP broadcast stopped - Failed to fetch proper network id")
        }
    }

    fun startBroadcast() {
        shouldBroadcast = true
        performUdpBroadcast()
    }

    fun stopBroadcast() {
        isAlreadyBroadcasting = false
        stopReceivingBroadcast()
        disposeAllBroadcasts()
        stopAllTimers()
        Timber.d("Udp Broadcast stopped")
    }


//    fun stopUdpBroadcast(macId: String?) {
//        udpClient.disposeBroadcast(macId)
//    }

    fun onMoveToBackground() {
//        stopUdpBroadcast(null)
        shouldBroadcast = false
        stopBroadcast()
        unRegisterMobileNetworkCallback()
        unRegisterWifiCallback()
    }

    private fun disposeAllBroadcasts() {
        broadcastDisposables.clear()
        broadcastDisposables = CompositeDisposable()
        deviceBroadcastCountMap.forEach {
            it.value.disposable?.dispose()
        }
        deviceBroadcastCountMap.clear()
    }

    fun onMoveToForeground() {
//        initMqttManager()
        shouldBroadcast = true
        isAlreadyBroadcasting = false
        performUdpBroadcast()
        registerMobileNetworkCallback()
        registerWifiCallback()
    }

    // For azure mqtt
    fun getLightStatusOfNetwork(networkId: Long) {
        var lights = localDataSource.getLights(networkId)
        lights.forEach {
            getLightStatusFromDevice(it.macId, false)
        }
    }


}

interface MqttOnSubscribeListener {
    fun onMqttSubscribeSuccess(macId: String)
}

data class LastSentCommand(val macId: String, val sequenceNumber: Int, val countDownTimer: CountDownTimer)