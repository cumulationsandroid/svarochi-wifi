package com.svarochi.wifi.service.mosquitomqtt

import android.content.Context
import com.svarochi.wifi.common.Constants.Companion.BROKER_DETAILS
import com.svarochi.wifi.service.communication.MqttOnSubscribeListener
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import org.json.JSONObject
import timber.log.Timber

// For Mosquito Mqtt

class MqttManager(var context: Context) {
    var mqttAndroidClient: MqttAndroidClient
    private var mqttConnectOptions: MqttConnectOptions
    private var subscribedList = ArrayList<String>()
    private var pendingSubscriptionList = ArrayList<String>()
    private var subscribeListener: MqttOnSubscribeListener? = null

    init {
        var clientId = MqttClient.generateClientId()
        //var clientId = "abcd"
        mqttAndroidClient = MqttAndroidClient(context, BROKER_DETAILS, clientId)

        mqttConnectOptions = MqttConnectOptions()
        mqttConnectOptions.isAutomaticReconnect = false
        mqttConnectOptions.isCleanSession = true
    }

    fun setOnMqttSubscribeListener(listener: MqttOnSubscribeListener) {
        this.subscribeListener = listener
    }

    fun connect() {
        Timber.d("Connecting to $BROKER_DETAILS")
        try {
            Timber.d("MQTT client id - ${mqttAndroidClient.clientId}")
            mqttAndroidClient.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Timber.d("MQTT connect - Successfully connected")
                    subscribeToPendingDevices()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Timber.d("MQTT connect - Failed to connect")
                    addAllSubscribedDevicesToPendingList()
                    exception!!.printStackTrace()
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun addAllSubscribedDevicesToPendingList() {
        Timber.d("Storing subscribed devices to pending list as connection is lost")
        if (subscribedList.isNotEmpty()) {
            for (device in subscribedList) {
                if (!pendingSubscriptionList.contains(device))
                    pendingSubscriptionList.add(device)
            }
            subscribedList.clear()
        } else {
            Timber.d("No subscribed devices")
        }
    }

    fun subscribeToDevice(macId: String) {
        if (subscribedList.contains(macId).not()) {
            val subscriptionTopicUrl = "/svarochi/lights/${macId.toLowerCase()}/hw"
            try {
                mqttAndroidClient.subscribe(subscriptionTopicUrl, 0, null, object : IMqttActionListener {
                    override fun onSuccess(asyncActionToken: IMqttToken?) {
                        Timber.d("MQTT Subscribe - Subscribed to topic($subscriptionTopicUrl)")
                        addDeviceToSubscriptionList(macId)

                        if (subscribeListener != null) {
                            subscribeListener!!.onMqttSubscribeSuccess(macId)
                        } else {
                            Timber.d("No subscribe listener")
                        }
                    }

                    override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                        Timber.d("MQTT Subscribe - Failed to subscribe to topic($subscriptionTopicUrl)")
                        removeDeviceFromSubscriptionList(macId)
                        exception?.printStackTrace()
                    }
                })

            } catch (ex: MqttException) {
                ex.printStackTrace()
            }
        } else {
            Timber.d("MQTT Subscribe - Already subscribed")
        }
    }

    private fun addDeviceToSubscriptionList(macId: String) {
        if (subscribedList.contains(macId)) {
            Timber.d("Device($macId) is already added to the subscription list")
        } else {
            Timber.d("Device($macId) added to the subscription list")
            subscribedList.add(macId)
        }
        removeFromPendingList(macId)
    }

    private fun removeFromPendingList(macId: String) {
        if (pendingSubscriptionList.contains(macId)) {
            Timber.d("Successfully removed device($macId) from the pending list")
            pendingSubscriptionList.remove(macId)
        } else {
            Timber.d("Device($macId) doesnt exist in the pending subscription list, hence couldn't be removed")
        }
    }

    private fun removeDeviceFromSubscriptionList(macId: String) {
        if (subscribedList.contains(macId)) {
            Timber.d("Successfully removed device($macId) from the subscription list")
            subscribedList.remove(macId)
        } else {
            Timber.d("Device($macId) doesnt exist in the subscription list, hence couldn't be removed")
        }
        addToPendingList(macId)
    }

    private fun addToPendingList(macId: String) {
        if (pendingSubscriptionList.contains(macId)) {
            Timber.d("Device($macId) is already added to the pending subscription list")
        } else {
            Timber.d("Device($macId) added to the pending subscription list")
            pendingSubscriptionList.add(macId)
        }
    }

    fun publish(macId: String, requestJson: JSONObject) {
        var topicToPublish = "/svarochi/lights/${macId.toLowerCase()}/sw"

        var message = MqttMessage()
        message.payload = requestJson.toString().toByteArray()

        Timber.d("Message published to topic - $topicToPublish")
        Timber.d("Message published - ${requestJson.toString()}")

        mqttAndroidClient.publish(topicToPublish, message)
    }

    fun updatePendingSubscriptionList(devicesList: ArrayList<String>) {
        pendingSubscriptionList = devicesList
    }

    fun subscribeToPendingDevices() {
        for (macId in pendingSubscriptionList) {
            subscribeToDevice(macId)
        }
    }

    fun disconnect() {
        addAllSubscribedDevicesToPendingList()
        mqttAndroidClient.disconnect()
    }
}