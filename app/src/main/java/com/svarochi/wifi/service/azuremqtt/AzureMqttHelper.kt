package com.svarochi.wifi.service.azuremqtt

import bizbrolly.svarochiapp.BuildConfig
import com.fasterxml.jackson.core.type.TypeReference
import com.google.gson.reflect.TypeToken
import com.microsoft.azure.sdk.iot.service.devicetwin.DeviceMethod
import com.microsoft.azure.sdk.iot.service.devicetwin.MethodResult
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.model.mqtt.MqttPacketType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import timber.log.Timber
import java.util.concurrent.TimeUnit


class AzureMqttHelper {

    private val responseTimeout = TimeUnit.SECONDS.toSeconds(Constants.AZURE_MQTT_TIMEOUT)
    private val connectTimeout = TimeUnit.SECONDS.toSeconds(Constants.AZURE_MQTT_TIMEOUT)
    private val connString = BuildConfig.ConnectionString
    private var methodClient: DeviceMethod? = null
    private var azureMqttCallBack: AzureMqttCallBack? = null
    private var disposables: CompositeDisposable = CompositeDisposable()
    private val typeRef = object : TypeReference<HashMap<String, Any>>() {}

    fun setCallBack(callback: AzureMqttCallBack) {
        this.azureMqttCallBack = callback
    }

    private fun sendMessage(type: MqttMessage, payload: Map<String, Any>): Observable<MethodResult> {
        return Observable.create { emitter ->
            try {
                if (methodClient == null) {
                    methodClient = DeviceMethod.createFromConnectionString(connString)
                }
                if (payload[Constants.JSON_MAC_ID] != null && payload[Constants.JSON_MAC_ID] is String) {
                    var result = methodClient?.invoke((payload[Constants.JSON_MAC_ID] as String), type.value, responseTimeout, connectTimeout, payload)

                    if (result != null) {
                        emitter.onNext(result)
                        emitter.onComplete()
                    } else {
                        var thr = Throwable("No result")
                        emitter.onError(thr)
                    }
                } else {
                    Timber.d("No macid specified")
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                emitter.onError(exception)
            }

        }
        /*
        * Observable.defer(object : Callable<Observable<String>> {
            override fun call(): Observable<String> {
                try {
                    return Observable.just(fetchData(text))
                } catch (e: IOException) {
                    return Observable.error(e)
                }

            }
        })
        * */
    }

    fun sendCommand(payloadJson: JSONObject) {
        if (payloadJson[Constants.JSON_PACKET_ID] != null && payloadJson[Constants.JSON_PACKET_ID] is Int) {
            val mapper = com.fasterxml.jackson.databind.ObjectMapper()
            try {
                val payload = mapper.readValue<HashMap<String, Any>>(payloadJson.toString(), typeRef)
//            payload[Constants.JSON_MAC_ID] = "f8a2d6068751"


                var packetId = payloadJson[Constants.JSON_PACKET_ID] as Int
                when (packetId) {
                    MqttPacketType.SET_COLOR.value -> {
                        setColor(payload)
                    }
                    MqttPacketType.GET_LIGHT_STATUS.value -> {
                        getLightStatus(payload)
                    }
                    MqttPacketType.SET_PRESET_SCENE.value -> {
                        setPresetScene(payload)
                    }
                    MqttPacketType.SET_SCHEDULE.value -> {
                        setSchedule(payload)
                    }
                    MqttPacketType.GET_FIRMWARE_VERSION.value -> {
                        getFirmwareVersion(payload)
                    }
                    else -> {
                        Timber.d("Unknown packet id")
                    }
                }
            } catch (e: Exception) {
                Timber.d("Failed to send mqtt command")
                e.printStackTrace()
            }
        } else {
            Timber.d("Invalid packet id")
        }
    }

    private fun setColor(payload: HashMap<String, Any>) {
        disposables.add(
                sendMessage(MqttMessage.SET_COLOR, payload)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<MethodResult>() {
                            override fun onComplete() {
                            }

                            override fun onNext(t: MethodResult) {
                                azureMqttCallBack?.onSuccess(t, payload)
                            }

                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                                azureMqttCallBack?.onFailure(payload, e.message)
                            }
                        }))
    }

    private fun getLightStatus(payload: HashMap<String, Any>) {
        disposables.add(
                sendMessage(MqttMessage.GET_LIGHT_STATUS, payload)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<MethodResult>() {
                            override fun onComplete() {
                            }

                            override fun onNext(t: MethodResult) {
                                azureMqttCallBack?.onSuccess(t, payload)
                            }

                            override fun onError(e: Throwable) {
                                azureMqttCallBack?.onFailure(payload, e.message)
                            }
                        }))
    }

    private fun setSchedule(payload: HashMap<String, Any>) {
        disposables.add(
                sendMessage(MqttMessage.SET_SCHEDULE, payload)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<MethodResult>() {
                            override fun onComplete() {
                            }

                            override fun onNext(t: MethodResult) {
                                azureMqttCallBack?.onSuccess(t, payload)
                            }

                            override fun onError(e: Throwable) {
                                azureMqttCallBack?.onFailure(payload, e.message)
                            }
                        }))
    }

    private fun setPresetScene(payload: HashMap<String, Any>) {
        disposables.add(
                sendMessage(MqttMessage.SET_PRESET_SCENE, payload)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<MethodResult>() {
                            override fun onComplete() {
                            }

                            override fun onNext(t: MethodResult) {
                                azureMqttCallBack?.onSuccess(t, payload)
                            }

                            override fun onError(e: Throwable) {
                                azureMqttCallBack?.onFailure(payload, e.message)
                            }
                        }))
    }

    private fun getFirmwareVersion(payload: HashMap<String, Any>) {
        disposables.add(
                sendMessage(MqttMessage.GET_FIRMWARE_VERSION, payload)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<MethodResult>() {
                            override fun onComplete() {
                            }

                            override fun onNext(t: MethodResult) {
                                azureMqttCallBack?.onSuccess(t, payload)
                            }

                            override fun onError(e: Throwable) {
                                azureMqttCallBack?.onFailure(payload, e.message)
                            }
                        }))
    }

    fun dispose() {
        disposables.clear()
    }

}

enum class MqttMessage(var value: String) {
    SET_COLOR("SetColor"),
    GET_LIGHT_STATUS("GetLightStatus"),
    SET_PRESET_SCENE("SetPresetScene"),
    SET_SCHEDULE("SetSchedule"),
    GET_FIRMWARE_VERSION("GetFirmwareVersion"),
}

interface AzureMqttCallBack {
    fun onSuccess(result: MethodResult, payload: Map<String, Any>)
    fun onFailure(payload: Map<String, Any>, reason: String?)
}