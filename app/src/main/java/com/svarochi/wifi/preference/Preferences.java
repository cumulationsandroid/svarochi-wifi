package com.svarochi.wifi.preference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by bizbrolly on 9/9/16.
 */
public class Preferences {
    private static Preferences preferences;
    private SharedPreferences sharedPreferences;
    public static final String PREF_IS_WELCOME_VISITED = "isWelcomeVisited";
    public static final String PREF_LAST_RED = "red";
    public static final String PREF_LAST_GREEN = "green";
    public static final String PREF_LAST_BLUE = "blue";
    public static final String PREF_PHONE = "phone";
    public static final String PREF_SECRET_CODE = "secret_code";
    public static final String PREF_APP_VERSION = "app_version";
    public static final String PREF_SWITCH_PROFILE = "switch_profile";
    public static final String PREF_SUB_PROFILE = "sub_profile";
    public static final String PREF_USER_ID = "user_id";//This will be always logged/registered in user's user id
    public static final String PREF_OTHER_USER_ID = "other_user_id";//This will be other user id(invitee user id not host user(Me myself)) rather than mine
    // If other_user_id value is <=0 means user is accessing self profile otherwise user is accessing shared profile
    public static final String PREF_OTHER_USER_EMAIL = "other_user_email";
    public static final String PREF_OTHER_USER_PHONE = "other_user_phone";
    public static final String PREF_ASSOCIATION_ID = "AssociationId";
    public static final String PREF_MODIFIED_DATE = "ModifiedDate";
    public static final String PREF_RECOMMEND_ONE_USER = "recommend_one_user";
    public static final String PREF_DONOT_REMIND = "donot_remind";
    public static final String PREF_APP_LAUNCH_COUNT = "app_launch_count";
    public static final String PREF_ALREADY_LOGGED_IN = "already_logged_in";
    public static final String PREF_ALREADY_AZURE_MAINTAINED = "already_azure_maintained";
    public static final String PREF_RESTORING_PROFILE = "restoring_profile";

    private enum KEYS {
        Password,
        DeviceNo,
        NetworkPassword,
        OTP,
        FetchPreviousState,
        Email,
        StateRetention,
        Slave,
        GroupId,
        IsCrashed,
        PrevousRoomId,
        AppUpdateLater,
        Alexa,
        FcmToken
    }

    public static Preferences getInstance(Context context) {
        if (preferences == null) {
            preferences = new Preferences(context);
        }
        return preferences;
    }


    private Preferences(Context context) {
        sharedPreferences = context.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
    }

    public void setPasswordSaved(boolean loggedIn) {
        sharedPreferences.edit().putBoolean(KEYS.Password.name(), loggedIn).apply();
    }

    public void setNetworkPassword(String password) {
        sharedPreferences.edit().putString(KEYS.NetworkPassword.name(), password).apply();
    }

    public String getNetworkPassword() {
        return sharedPreferences.getString(KEYS.NetworkPassword.name(), "");
    }

    public void setPreviousRoomId(int roomId) {
        sharedPreferences.edit().putInt(KEYS.PrevousRoomId.name(), roomId).apply();
    }

    public int getPreviousRoomId() {
        return sharedPreferences.getInt(KEYS.PrevousRoomId.name(), -1);
    }

    public void setIsCrashed(boolean crashed) {
        sharedPreferences.edit().putBoolean(KEYS.IsCrashed.name(), crashed).apply();
    }

    public boolean isCrashed() {
        return sharedPreferences.getBoolean(KEYS.IsCrashed.name(), false);
    }

    public void setGroupId(String groupId) {
        int group;
        if (Integer.parseInt(groupId) == 0) {
            group = 1;
        } else {
            group = Integer.parseInt(groupId) * 640;
        }
        sharedPreferences.edit().putInt(KEYS.GroupId.name(), group).apply();
    }

    public int getGroupId() {
        sharedPreferences.edit().putInt(KEYS.GroupId.name(), (sharedPreferences.getInt(KEYS.GroupId.name(), 0) + 1)).commit();
        return sharedPreferences.getInt(KEYS.GroupId.name(), 0);
    }

    public void setOTPWaiting(boolean isOTP) {
        sharedPreferences.edit().putBoolean(KEYS.OTP.name(), isOTP).apply();
    }

    public boolean isOTPWaiting() {
        return sharedPreferences.getBoolean(KEYS.OTP.name(), false);
    }

    public void setSlaveUser(boolean isSlave) {
        sharedPreferences.edit().putBoolean(KEYS.Slave.name(), isSlave).apply();
    }

    public boolean isSlaveUser() {
        return sharedPreferences.getBoolean(KEYS.Slave.name(), false);
    }

    public void setUserMail(String email) {
        sharedPreferences.edit().putString(KEYS.Email.name(), email).apply();
    }

    public String getEmail() {
        return sharedPreferences.getString(KEYS.Email.name(), "");
    }

    public int getDeviceNo() {
        sharedPreferences.edit().putInt(KEYS.DeviceNo.name(), sharedPreferences.getInt(KEYS.DeviceNo.name(), 0) + 1).commit();
        return sharedPreferences.getInt(KEYS.DeviceNo.name(), 0);
    }

    public boolean isPasswordSaved() {
        return sharedPreferences.getBoolean(KEYS.Password.name(), false);
    }

    public void fetchPreviousState(boolean isFetch) {
        sharedPreferences.edit().putBoolean(KEYS.FetchPreviousState.name(), isFetch).apply();
    }

    public boolean doesFetchPreviousState() {
        return sharedPreferences.getBoolean(KEYS.FetchPreviousState.name(), true);
    }

    public void retainPreviousState(boolean shouldRetain) {
        sharedPreferences.edit().putBoolean(KEYS.StateRetention.name(), shouldRetain).apply();
    }

    public boolean isRetainingPreviousState() {
        return sharedPreferences.getBoolean(KEYS.StateRetention.name(), true);
    }

    public void setIsWelcomeVisited(boolean isWelcomeVisited) {
        sharedPreferences.edit().putBoolean(PREF_IS_WELCOME_VISITED, isWelcomeVisited).apply();
    }

    public boolean isWelcomeVisited() {
        return sharedPreferences.getBoolean(PREF_IS_WELCOME_VISITED, false);
    }

    public void setLastRed(int red) {
        sharedPreferences.edit().putInt(PREF_LAST_RED, red).apply();
    }

    public int getLastRed() {
        return sharedPreferences.getInt(PREF_LAST_RED, -1);
    }

    public void setLastGreen(int green) {
        sharedPreferences.edit().putInt(PREF_LAST_GREEN, green).apply();
    }

    public int getLastGreen() {
        return sharedPreferences.getInt(PREF_LAST_GREEN, -1);
    }

    public void setLastBlue(int blue) {
        sharedPreferences.edit().putInt(PREF_LAST_BLUE, blue).apply();
    }

    public int getLastBlue() {
        return sharedPreferences.getInt(PREF_LAST_BLUE, -1);
    }

    public void setPhone(String phone) {
        sharedPreferences.edit().putString(PREF_PHONE, phone).apply();
    }

    public String getPhone() {
        return sharedPreferences.getString(PREF_PHONE, "");
    }

    public void setSecretCode(String secretCode) {
        sharedPreferences.edit().putString(PREF_SECRET_CODE, secretCode).apply();
    }

    public String getSecretCode() {
        return sharedPreferences.getString(PREF_SECRET_CODE, "");
    }

    public void setAppUpdateLater(boolean isAppUpdate) {
        sharedPreferences.edit().putBoolean(KEYS.AppUpdateLater.name(), isAppUpdate).apply();
    }

    public boolean isAppUpdateLater() {
        return sharedPreferences.getBoolean(KEYS.AppUpdateLater.name(), true);
    }

    public void putString(String key, String value) {
        sharedPreferences.edit().putString(key, value).commit();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public String getString(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    public void putInt(String key, int value) {
        sharedPreferences.edit().putInt(key, value).commit();
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public int getInt(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public void putBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public void clearPreferences() {
        sharedPreferences.edit().clear().commit();
    }

    /************Alexa-FCM data***********/

    public boolean isAlexaEnabled() {
        return sharedPreferences.getBoolean(KEYS.Alexa.name(), false);
    }

    public boolean setAlexaEnabled(boolean isEnabled) {
        return sharedPreferences.edit().putBoolean(KEYS.Alexa.name(), isEnabled).commit();
    }

    public String getToken() {
        return sharedPreferences.getString(KEYS.FcmToken.name(), "");
    }

    public boolean setToken(String token) {
        return sharedPreferences.edit().putString(KEYS.FcmToken.name(), token).commit();
    }

    public String getAmEmail() {
        return sharedPreferences.getString("amEmail", "");
    }

    public boolean setAmEmail(String email) {
        return sharedPreferences.edit().putString("amEmail", email).commit();
    }

    /*************************************/
}
