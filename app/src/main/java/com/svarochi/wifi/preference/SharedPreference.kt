package com.svarochi.wifi.preference

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.svarochi.wifi.common.Constants.Companion.CURRENT_PROFILE_SELF
import com.svarochi.wifi.database.entity.Light
import timber.log.Timber


class SharedPreference(context: Context) {

    private var sharedPreference: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val gson = Gson()
    private val pendingLightMapType = object : TypeToken<HashMap<String, String>>() {
    }.type
    private val mapper = ObjectMapper()

    fun getNetworkId(): Long {
        return sharedPreference
                .getLong(Keys.NETWORK_ID.value, (-1).toLong())
    }

    fun setNetworkId(networkId: Long) {
        sharedPreference
                .edit()
                .putLong(Keys.NETWORK_ID.value, networkId)
                .apply()
    }


    fun setNetworkName(name: String) {
        sharedPreference
                .edit()
                .putString(Keys.NETWORK_NAME.value, name)
                .apply()
    }

    fun getProjectId(): Long {
        return sharedPreference
                .getLong(Keys.PROJECT_ID.value, 0)
    }

    fun setProjectId(projectId: Long) {
        sharedPreference
                .edit()
                .putLong(Keys.PROJECT_ID.value, projectId)
                .apply()
    }

    fun getUserId(): String {
        return sharedPreference
                .getString(Keys.USER_ID.value, "")
    }

    fun setUserId(userId: String) {
        sharedPreference
                .edit()
                .putString(Keys.USER_ID.value, userId)
                .apply()
    }

    fun addDeviceToBeDeleted(macId: String, lightString: String) {
        var pendingLightString = sharedPreference
                .getString(Keys.DEVICES_TOBE_DELETED.value, null)

        var pendingDevicesMap: HashMap<String, String>

        pendingDevicesMap = if (pendingLightString == null) {
            HashMap()
        } else {
            gson.fromJson<HashMap<String, String>>(pendingLightString, pendingLightMapType)
        }
        pendingDevicesMap[macId] = lightString

        val mapString = gson.toJson(pendingDevicesMap)

        sharedPreference
                .edit()
                .putString(Keys.DEVICES_TOBE_DELETED.value, mapString)
                .apply()

        Timber.d("Added light($macId) to be deleted into shared preference")
    }

    fun getAllDevicesToBeDeleted(): HashMap<String, String>? {
        var devicesToBeDeletedString = sharedPreference
                .getString(Keys.DEVICES_TOBE_DELETED.value, null)

        if (devicesToBeDeletedString != null) {
            return gson.fromJson<HashMap<String, String>>(devicesToBeDeletedString, pendingLightMapType)
        }
        Timber.d("No pending lights to be deleted stored in shared preference")
        return null
    }

    fun deviceExistsInToBeDeleted(macId: String, networkId: Long, projectId: Long): Boolean {
        var devicesToBeDeletedString = sharedPreference
                .getString(Keys.DEVICES_TOBE_DELETED.value, null)

        if (devicesToBeDeletedString != null) {
            if (gson.fromJson<HashMap<String, String>>(devicesToBeDeletedString, pendingLightMapType).containsKey(macId)) {
                var lightToBeDeletedMap = gson.fromJson<HashMap<String, String>>(devicesToBeDeletedString, pendingLightMapType)[macId]
                val lightToBeDeleted = mapper.convertValue(lightToBeDeletedMap, Light::class.java)

                return (lightToBeDeleted.networkId == networkId && lightToBeDeleted.projectId == projectId)
            }
        }
        Timber.d("Device($macId) doesnt exist to be deleted list in shared preference")
        return false
    }

    fun deleteDeviceToBeDeleted(macId: String) {
        var devicesToBeDeleted = sharedPreference
                .getString(Keys.DEVICES_TOBE_DELETED.value, null)

        if (devicesToBeDeleted != null) {
            var devicesToBeDeletedMap = gson.fromJson<HashMap<String, String>>(devicesToBeDeleted, pendingLightMapType)
            if (devicesToBeDeletedMap.containsKey(macId)) {
                devicesToBeDeletedMap.remove(macId)
                val mapString = gson.toJson(devicesToBeDeletedMap)

                sharedPreference
                        .edit()
                        .putString(Keys.DEVICES_TOBE_DELETED.value, mapString)
                        .apply()

                Timber.d("Removed Device($macId) from the to be deleted list from shared preference")
            } else {
                Timber.d("Device($macId) doesnt exist in the to be deleted list of shared preference")
            }
        } else {
            Timber.d("No to be deleted lights stored in shared preference")
        }
    }

    fun addDeviceToFlicker(macId: String) {
        var devicesToBeFlickered = sharedPreference
                .getStringSet(Keys.DEVICES_TOBE_FLICKERED.value, null)

        if (devicesToBeFlickered == null) {
            devicesToBeFlickered = HashSet<String>()
        }

        val set = HashSet<String>(devicesToBeFlickered)
        set.add(macId)

        sharedPreference
                .edit()
                .putStringSet(Keys.DEVICES_TOBE_FLICKERED.value, set)
                .apply()

        Timber.d("Added Device($macId) as to be flickered in the shared preference")
    }

    fun removeDeviceToBeFlickered(macId: String) {
        var devicesToBeFlickered = sharedPreference
                .getStringSet(Keys.DEVICES_TOBE_FLICKERED.value, null)

        if (devicesToBeFlickered == null) {
            Timber.d("Device($macId) doesnt exist in to be flickered in the shared preference")
            return
        }

        val set = HashSet<String>(devicesToBeFlickered)

        if (set.contains(macId)) {
            set.remove(macId)

            sharedPreference
                    .edit()
                    .putStringSet(Keys.DEVICES_TOBE_FLICKERED.value, set)
                    .apply()

            Timber.d("Device($macId) removed from the to be flickered in the shared preference")
        } else {
            Timber.d("Device($macId) doesnt exist in to be flickered in the shared preference")
        }
    }

    fun isDeviceToBeFlickered(macId: String): Boolean {
        var devicesToBeFlickered = sharedPreference
                .getStringSet(Keys.DEVICES_TOBE_FLICKERED.value, null)

        if (devicesToBeFlickered == null) {
            Timber.d("Device($macId) doesnt exist in to be flickered in the shared preference")
            return false
        }

        return devicesToBeFlickered.contains(macId)
    }

    fun getAllDevicesToBeFlickered(): Set<String>? {
        return sharedPreference
                .getStringSet(Keys.DEVICES_TOBE_FLICKERED.value, null)
    }

    fun addPendingDevice(macId: String, value: String) {
        var pendingLightString = sharedPreference
                .getString(Keys.PENDING_LIGHT_MAP.value, null)

        var pendingDevicesMap: HashMap<String, String>

        pendingDevicesMap = if (pendingLightString == null) {
            HashMap()
        } else {
            gson.fromJson<HashMap<String, String>>(pendingLightString, pendingLightMapType)
        }
        pendingDevicesMap[macId] = value

        val mapString = gson.toJson(pendingDevicesMap)

        sharedPreference
                .edit()
                .putString(Keys.PENDING_LIGHT_MAP.value, mapString)
                .apply()

        Timber.d("Added pending light into shared preference")
    }

    fun removePendingDevice(macId: String) {
        var pendingLightString = sharedPreference
                .getString(Keys.PENDING_LIGHT_MAP.value, null)

        if (pendingLightString != null) {
            var pendingDevicesMap = gson.fromJson<HashMap<String, String>>(pendingLightString, pendingLightMapType)
            pendingDevicesMap.remove(macId)

            val mapString = gson.toJson(pendingDevicesMap)

            sharedPreference
                    .edit()
                    .putString(Keys.PENDING_LIGHT_MAP.value, mapString)
                    .apply()

            Timber.d("Removed pending light from shared preference")
        } else {
            Timber.d("No pending lights stored in shared preference")
        }
    }

    fun getAllPendingDevices(): HashMap<String, String>? {
        var pendingLightString = sharedPreference
                .getString(Keys.PENDING_LIGHT_MAP.value, null)

        if (pendingLightString != null) {
            return gson.fromJson<HashMap<String, String>>(pendingLightString, pendingLightMapType)
        }
        Timber.d("No pending lights stored in shared preference")
        return null
    }

    fun getCurrentProfile(): String {
        return sharedPreference
                .getString(Keys.CURRENT_PROFILE.value, CURRENT_PROFILE_SELF)
    }

    fun setCurrentProfile(profile: String) {
        sharedPreference
                .edit()
                .putString(Keys.CURRENT_PROFILE.value, profile)
                .apply()
    }


    fun clearPreferences() {
        sharedPreference.edit().clear().apply()
    }

}

enum class Keys(val value: String) {
    USER_ID("userId"),
    NETWORK_ID("networkId"),
    CURRENT_PROFILE("current_profile"),
    NETWORK_NAME("networkName"),
    PROJECT_ID("project_id"),
    PENDING_LIGHT_MAP("pendinglight"),
    DEVICES_TOBE_DELETED("devicesToBeDeleted"),
    DEVICES_TOBE_FLICKERED("devicesToBeFlickered")
}