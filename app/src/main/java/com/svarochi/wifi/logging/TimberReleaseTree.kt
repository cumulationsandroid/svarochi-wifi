package com.svarochi.wifi.logging

import android.util.Log
import timber.log.Timber


class TimberReleaseTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.ERROR) {
            // TODO - LOG ERROR TO CRASH_ANALYTICS
        }
    }
}
