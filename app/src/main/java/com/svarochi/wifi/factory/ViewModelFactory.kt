package com.svarochi.wifi.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.svarochi.wifi.SvarochiApplication
import com.svarochi.wifi.ui.addnewnetwork.repository.NetworkDetailsRepository
import com.svarochi.wifi.ui.addnewnetwork.viewmodel.NetworkDetailsViewModel
import com.svarochi.wifi.ui.audio.repository.AudioRepository
import com.svarochi.wifi.ui.audio.viewmodel.AudioViewModel
import com.svarochi.wifi.ui.diagnostics.repository.DiagnosticsRepository
import com.svarochi.wifi.ui.diagnostics.viewmodel.DiagnosticsViewModel
import com.svarochi.wifi.ui.groupcontrol.repository.GroupControlRepository
import com.svarochi.wifi.ui.groupcontrol.viewmodel.GroupControlViewModel
import com.svarochi.wifi.ui.inviteuser.repository.InviteUserRepository
import com.svarochi.wifi.ui.inviteuser.viewmodel.InviteUserViewModel
import com.svarochi.wifi.ui.lightcontrol.repository.LightControlRepository
import com.svarochi.wifi.ui.lightcontrol.viewmodel.LightControlViewModel
import com.svarochi.wifi.ui.music.repository.MusicRepository
import com.svarochi.wifi.ui.music.viewmodel.MusicViewModel
import com.svarochi.wifi.ui.networks.repository.NetworksRepository
import com.svarochi.wifi.ui.networks.viewmodel.NetworksViewModel
import com.svarochi.wifi.ui.scheduledetails.repository.ScheduleDetailsRepository
import com.svarochi.wifi.ui.scheduledetails.viewmodel.ScheduleDetailsViewModel
import com.svarochi.wifi.ui.projects.repository.ProjectsRepository
import com.svarochi.wifi.ui.projects.viewmodel.ProjectsViewModel
import com.svarochi.wifi.ui.room.repository.RoomRepository
import com.svarochi.wifi.ui.room.viewmodel.RoomViewModel
import com.svarochi.wifi.ui.scheduleaction.repository.ScheduleActionRepository
import com.svarochi.wifi.ui.scheduleaction.viewholder.ScheduleActionViewModel
import com.svarochi.wifi.ui.schedulelist.repository.ScheduleListRepository
import com.svarochi.wifi.ui.schedulelist.viewmodel.ScheduleListViewModel
import com.svarochi.wifi.ui.setscenelight.SetSceneLightRepository
import com.svarochi.wifi.ui.setscenelight.SetSceneLightViewModel
import com.svarochi.wifi.ui.setwifidetails.repository.SetWifiDetailsRepository
import com.svarochi.wifi.ui.setwifidetails.viewmodel.SetWifiDetailsViewModel
import com.svarochi.wifi.ui.switchprofile.repository.SwitchProfileRepository
import com.svarochi.wifi.ui.switchprofile.viewmodel.SwitchProfileViewModel
import com.svarochi.wifi.ui.userinvitations.repository.UserInvitationsRepository
import com.svarochi.wifi.ui.userinvitations.viewmodel.UserInvitationsViewModel


class ViewModelFactory(var application: SvarochiApplication) : ViewModelProvider.Factory {

    //private var dataSource: DataSource = Injection.getDataSource(application)

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(LightControlViewModel::class.java) -> LightControlViewModel(
                    LightControlRepository(application)
            ) as T
            modelClass.isAssignableFrom(NetworksViewModel::class.java) -> NetworksViewModel(
                    NetworksRepository(
                            application
                    )
            ) as T
            modelClass.isAssignableFrom(ProjectsViewModel::class.java) -> ProjectsViewModel(
                    ProjectsRepository(
                            application
                    )
            ) as T
            modelClass.isAssignableFrom(RoomViewModel::class.java) -> RoomViewModel(RoomRepository(application)) as T
            modelClass.isAssignableFrom(GroupControlViewModel::class.java) -> GroupControlViewModel(
                    GroupControlRepository(application)
            ) as T
            modelClass.isAssignableFrom(SetWifiDetailsViewModel::class.java) -> SetWifiDetailsViewModel(
                    SetWifiDetailsRepository(application)
            ) as T
            modelClass.isAssignableFrom(AudioViewModel::class.java) -> AudioViewModel(AudioRepository(application)) as T
            modelClass.isAssignableFrom(MusicViewModel::class.java) -> MusicViewModel(MusicRepository(application)) as T
            modelClass.isAssignableFrom(ScheduleDetailsViewModel::class.java) -> ScheduleDetailsViewModel(
                    ScheduleDetailsRepository(
                            application
                    )
            ) as T
            modelClass.isAssignableFrom(ScheduleListViewModel::class.java) -> ScheduleListViewModel(
                    ScheduleListRepository(application)
            ) as T
            modelClass.isAssignableFrom(DiagnosticsViewModel::class.java) -> DiagnosticsViewModel(
                    DiagnosticsRepository(application)
            ) as T
            modelClass.isAssignableFrom(NetworkDetailsViewModel::class.java) -> NetworkDetailsViewModel(
                    NetworkDetailsRepository(application)
            ) as T
            modelClass.isAssignableFrom(UserInvitationsViewModel::class.java) -> UserInvitationsViewModel(
                    UserInvitationsRepository(application)
            ) as T

            modelClass.isAssignableFrom(InviteUserViewModel::class.java) -> InviteUserViewModel(
                    InviteUserRepository(application)
            ) as T
            modelClass.isAssignableFrom(SwitchProfileViewModel::class.java) -> SwitchProfileViewModel(
                    SwitchProfileRepository(application)
            ) as T
            modelClass.isAssignableFrom(ScheduleActionViewModel::class.java) -> ScheduleActionViewModel(
                    ScheduleActionRepository(application)
            ) as T
            modelClass.isAssignableFrom(SetSceneLightViewModel::class.java) -> SetSceneLightViewModel(
                    SetSceneLightRepository(application)
            ) as T
            else -> throw IllegalArgumentException("unknown model class $modelClass")
        }
    }
}