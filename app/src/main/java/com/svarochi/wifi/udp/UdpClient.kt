package com.svarochi.wifi.udp

import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Utilities
import timber.log.Timber
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

private var socket: DatagramSocket? = null
private var receivePacket = ByteArray(20)
private const val TAG = "UdpClient"
private var shouldReceive = true


class UdpClient(private val callback: BroadcastCallback) {

    private var sendBroadcastThread: SendBroadcastThread? = null
    private var receiveBroadcastThread = ReceiveBroadcastThread(callback)

    fun sendBroadcast(ipAddress: String, port: Int, macId: String, data: ByteArray) {
        if (socket == null) {
            socket = DatagramSocket()
        }

        var broadcastPacket = BroadcastPacket(ipAddress, port, macId, data)
        sendBroadcastThread = SendBroadcastThread(broadcastPacket)
        sendBroadcastThread?.start()

        shouldReceive = true
        if (receiveBroadcastThread.isAlive.not()) {
            receiveBroadcastThread = ReceiveBroadcastThread(callback)
            receiveBroadcastThread.start()
        }
    }

    fun stopReceivingBroadcast() {
        shouldReceive = false
        socket = null
        receiveBroadcastThread = ReceiveBroadcastThread(callback)
    }
}

class SendBroadcastThread(val data: BroadcastPacket) : Thread() {
    override fun run() {
        val dgram = DatagramPacket(
                data.packet, data.packet.size,
                InetAddress.getByName(data.ipAddress), data.port
        )
        try {
            if (socket != null) {
                socket?.send(dgram)
                if (data.macId.equals(Constants.DEFAULT_MAC_ID)) {
                    Timber.d("$TAG Generic Broadcast sent at ${data.ipAddress}/${data.port}")
                } else {
                    Timber.d("$TAG Broadcast sent for ${data.macId} at ${data.ipAddress}/${data.port}")
                }
            }
        } catch (exception: IOException) {
            Timber.d("$TAG Failed to send broadcast")
            exception.printStackTrace()
            socket = null
        }
    }
}

class ReceiveBroadcastThread(val callback: BroadcastCallback) : Thread() {
    override fun run() {
        while (socket != null && shouldReceive) {
            try {
                val receiveDgram = DatagramPacket(receivePacket, receivePacket.size)
                socket?.receive(receiveDgram)

                if (receivePacket.isNotEmpty()) {
                    Timber.d("Got Ip address - ${Utilities.toString(receivePacket)}")
                    callback.onBrodcastReceived(receivePacket)
                    receivePacket = ByteArray(20)
                }

            } catch (exception: Exception) {
                Timber.d("$TAG Failed to receive broadcast")
                exception.printStackTrace()
                socket = null
            }
        }
    }

}

data class BroadcastPacket(val ipAddress: String, val port: Int, val macId: String, val packet: ByteArray)

interface BroadcastCallback {
    fun onBrodcastReceived(data: ByteArray)
}