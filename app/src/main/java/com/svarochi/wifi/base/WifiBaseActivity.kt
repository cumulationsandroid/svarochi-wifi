package com.svarochi.wifi.base

import android.app.Dialog
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import bizbrolly.svarochiapp.R
import com.svarochi.wifi.SvarochiApplication
import timber.log.Timber

abstract class WifiBaseActivity : AppCompatActivity() {
    private var dialog: Dialog? = null

    fun requestToBindService(serviceConnectionCallback: ServiceConnectionCallback) {
        (application as SvarochiApplication).bindCommunicationService(serviceConnectionCallback)
        initLoadingDialog()
    }

    private fun initLoadingDialog() {
        if (dialog == null) {
            Timber.d("Initialising loading dialog")
            dialog = Dialog(this)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setContentView(R.layout.dialog_loading)
            dialog!!.setCanceledOnTouchOutside(false)
        } else {
            Timber.d("Loading dialog already initialised")
        }
    }

    fun showLoadingPopup(show: Boolean, message: String?) {
        if (dialog != null) {
            if (show) {
                dialog!!.setContentView(R.layout.dialog_loading)
                dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
                val text = dialog!!.findViewById(R.id.tv_text) as TextView
                text.text = message!!
                dialog!!.show()
            } else {
                if (dialog!!.isShowing)
                    dialog!!.dismiss()
            }
        } else {
            Timber.d("Loading dialog not initialised")
            initLoadingDialog()
        }
    }

    open fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        if (isTaskRoot) {
            (application as SvarochiApplication).unbindCommunicationService()
        }
        super.onBackPressed()
    }
}