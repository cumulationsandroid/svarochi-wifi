package com.svarochi.wifi.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.model.common.events.EventStatus
import com.svarochi.wifi.model.common.events.EventType

abstract class BaseViewModel : ViewModel() {

    fun initResponseObserver(
            disposables: CompositeDisposable,
            repository: BaseRepository,
            response: MutableLiveData<Event>
    ) {
            disposables.add(
                repository.getCommunicationResponseObserver()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    disposables.add(it!!)
                    Timber.d("Observing for repository response...")
                }
                .subscribeWith(object : DisposableObserver<Event>() {
                    override fun onComplete() {
                    }

                    override fun onNext(commandResponse: Event) {
                        response.value = commandResponse
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e)
                        response.value = Event(EventType.CONNECTION, EventStatus.ERROR, null, e)
                    }
                })
            )
    }

}