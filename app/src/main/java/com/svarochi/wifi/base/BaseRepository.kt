package com.svarochi.wifi.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import com.svarochi.wifi.model.common.events.Event
import com.svarochi.wifi.service.communication.CommunicationService

abstract class BaseRepository {

    var service: CommunicationService? = null

    abstract fun initCommunicationResponseObserver()

    fun disposeDisposables(disposables: CompositeDisposable): CompositeDisposable {
        disposables.dispose()
        return CompositeDisposable()
    }

    abstract fun getCommunicationResponseObserver(): PublishSubject<Event>
}