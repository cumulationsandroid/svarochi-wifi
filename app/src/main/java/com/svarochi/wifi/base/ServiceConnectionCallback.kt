package com.svarochi.wifi.base

interface ServiceConnectionCallback {

    fun serviceConnected()
}