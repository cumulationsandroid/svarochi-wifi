package com.svarochi.wifi.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Profile")
public class Profile implements Serializable {

    @PrimaryKey
    @NonNull
    public String id;

    @ColumnInfo(name = "profile_name")
    public String profileName;

    @ColumnInfo(name = "shared_by")
    public String sharedBy;

    @ColumnInfo(name = "shared_with_data")
    public String sharedWithData;

    @ColumnInfo(name = "shared_with")
    public String sharedWith;

    public Profile(@NonNull String id, String profileName, String sharedBy,String sharedWith, String sharedWithData) {
        this.id = id;
        this.profileName = profileName;
        this.sharedBy = sharedBy;
        this.sharedWith = sharedWith;
        this.sharedWithData = sharedWithData;
    }

}
