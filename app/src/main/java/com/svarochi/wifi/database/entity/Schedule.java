package com.svarochi.wifi.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Network.class,
                childColumns = {"network_id"},
                onDelete = 5,
                parentColumns = {"id"}
        )},
        tableName = "Schedule"
)
public class Schedule {
    @PrimaryKey
    public Long id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "network_id")
    public Long networkId;

    @ColumnInfo(name = "days")
    public String days;

    @ColumnInfo(name = "start_time")
    public String startTime;

    @ColumnInfo(name = "start_action")
    public String startAction;

    @ColumnInfo(name = "end_time")
    public String endTime;

    @ColumnInfo(name = "end_action")
    public String endAction;

    @ColumnInfo(name = "devices")
    public String devices;

    @ColumnInfo(name = "groups")
    public String groups;

    public Schedule(Long id, String name, Long networkId, String days, String startTime, String startAction, String endTime, String endAction, String devices, String groups) {
        this.id = id;
        this.name = name;
        this.networkId = networkId;
        this.days = days;
        this.startTime = startTime;
        this.startAction = startAction;
        this.endTime = endTime;
        this.endAction = endAction;
        this.devices = devices;
        this.groups = groups;
    }
}
