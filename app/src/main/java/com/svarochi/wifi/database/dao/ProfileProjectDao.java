package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.ProfileProject;

import java.util.List;

@Dao
public interface ProfileProjectDao {
    @Insert
    void addProfileProject(ProfileProject profileProject);

    @Insert
    void addProfileProjects(List<ProfileProject> profileProjects);

    @Delete
    void deleteProfileProject(ProfileProject profileProject);

    @Query("SELECT * FROM ProfileProject WHERE profile_id=:profileId")
    LiveData<List<ProfileProject>> getProjectAndNetworksOfProfile(String profileId);

    @Query("DELETE FROM ProfileProject WHERE profile_id=:profileId")
    void clearProfileProjectsOfProfile(String profileId);
}
