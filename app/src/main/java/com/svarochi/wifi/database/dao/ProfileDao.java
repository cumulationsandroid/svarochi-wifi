package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.Profile;

import java.util.List;

@Dao
public interface ProfileDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addProfile(Profile profile);

    @Delete
    void delete(Profile profile);

    @Query("SELECT * FROM Profile WHERE shared_with=:sharedWith")
    LiveData<List<Profile>> getProfilesSharedWith(String sharedWith);

    @Query("SELECT * FROM Profile WHERE shared_by=:sharedBy")
    LiveData<List<Profile>> getProfilesSharedBy(String sharedBy);

    @Query("UPDATE Profile SET profile_name=:profileName, shared_by=:sharedBy, shared_with=:sharedWith,shared_with_data=:sharedWithData WHERE id=:profileId")
    void updateProfile(String profileId, String profileName, String sharedBy, String sharedWith, String sharedWithData);

    @Query("DELETE FROM Profile")
    void clearProfilesTable();

    @Query("DELETE FROM Profile where shared_by=:sharedBy")
    void clearProfilesSharedBy(String sharedBy);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addProfiles(List<Profile> profiles);

    @Query("DELETE FROM Profile where shared_with=:sharedWith")
    void clearProfilesSharedWith(String sharedWith);
}
