package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.Scene;

import java.util.List;

@Dao
public interface SceneDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addScene(Scene scene);

    @Query("UPDATE Scene SET name=:name WHERE id=:sceneId")
    void editSceneName(Long sceneId, String name);

    @Query("SELECT * FROM Scene WHERE network_id=:networkId")
    List<Scene> getScenes(Long networkId);

    @Query("SELECT id FROM Scene WHERE name=:name")
    long getSceneId(String name);

    @Query("SELECT * FROM Scene WHERE name=:name")
    List<Scene> getScenesOfName(String name);

    @Query("DELETE FROM Scene WHERE network_id=:networkId")
    void clearScenesOfNetwork(long networkId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertScenes(List<Scene> scenesList);

    @Query("SELECT * FROM Scene WHERE network_id=:networkId")
    LiveData<List<Scene>> getScenesLiveData(Long networkId);

    @Query("SELECT * FROM Scene WHERE id=:sceneId")
    Scene getScene(Long sceneId);

    @Query("DELETE FROM Scene WHERE id=:sceneId")
    void deleteScene(Long sceneId);
}
