package com.svarochi.wifi.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.svarochi.wifi.common.Constants;
import com.svarochi.wifi.database.LampType;
import com.svarochi.wifi.database.SceneType;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Network.class,
                childColumns = {"network_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"id"}
        ), @ForeignKey(
                entity = Project.class,
                childColumns = {"project_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"id"}
        )},
        tableName = "Light"
)
public class Light {
    @PrimaryKey
    @NonNull
    public String macId;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "network_id")
    public Long networkId;

    @ColumnInfo(name = "lamp_type")
    public String lampType;

    @ColumnInfo(name = "r_value")
    public String rValue;

    @ColumnInfo(name = "g_value")
    public String gValue;

    @ColumnInfo(name = "b_value")
    public String bValue;

    @ColumnInfo(name = "w_value")
    public String wValue;

    @ColumnInfo(name = "c_value")
    public String cValue;

    @ColumnInfo(name = "scene_value")
    public String sceneValue;

    @ColumnInfo(name = "brightness_value")
    public Integer brightnessValue;

    @ColumnInfo(name = "on_off_value")
    public Boolean onOffValue;

    @ColumnInfo(name = "rssi_value")
    public Integer rssiValue;

    @ColumnInfo(name = "synced")
    public Integer isSynced;

    @ColumnInfo(name = "project_id")
    public Long projectId;

    @ColumnInfo(name = "fw_version")
    public String firmwareVersion;

    public Light(@NonNull String macId,
                 String name,
                 Long networkId,
                 Long projectId,
                 String lampType,
                 String rValue,
                 String gValue,
                 String bValue,
                 String wValue,
                 String cValue,
                 String sceneValue,
                 Integer brightnessValue,
                 Boolean onOffValue,
                 Integer rssiValue,
                 Integer isSynced,
                 String firmwareVersion) {
        this.macId = macId;
        this.name = name;
        this.networkId = networkId;
        this.projectId = projectId;
        this.lampType = lampType;
        this.rValue = rValue;
        this.gValue = gValue;
        this.bValue = bValue;
        this.wValue = wValue;
        this.cValue = cValue;
        this.sceneValue = sceneValue;
        this.brightnessValue = brightnessValue;
        this.onOffValue = onOffValue;
        this.rssiValue = rssiValue;
        this.isSynced = isSynced;
        this.firmwareVersion = firmwareVersion;
    }

    public Light()
    {}

    public static Light newInstance(String name, String mac_id, Long network_id, Long project_id, String lamp_type, Integer syncStatus) {
        if (lamp_type.equals(LampType.BRIGHT_AND_DIM.getValue())) {
            return new Light(mac_id, name, network_id, project_id, lamp_type, "00", "00", "00", "00", "00", SceneType.DEFAULT.getValue(), 100, true, 0, syncStatus, Constants.DEFAULT_FIRMWARE_VERSION);
        } else {
            return lamp_type.equals(LampType.WARM_AND_COOL.getValue()) ? new Light(mac_id, name, network_id, project_id, lamp_type, "00", "00", "00", "00", "FF", SceneType.DEFAULT.getValue(), 100, false, 0, syncStatus, Constants.DEFAULT_FIRMWARE_VERSION)
                    : new Light(mac_id, name, network_id, project_id, lamp_type, "FF", "FF", "FF", "00", "FF", SceneType.DEFAULT.getValue(), 100, true, 0, syncStatus, Constants.DEFAULT_FIRMWARE_VERSION);
        }
    }
}