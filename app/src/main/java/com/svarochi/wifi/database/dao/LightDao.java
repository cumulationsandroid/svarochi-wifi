package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.svarochi.wifi.database.SceneType;
import com.svarochi.wifi.database.entity.Light;


import java.util.List;

@Dao
public interface LightDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNewLight(Light light);

    @Delete
    void deleteLight(Light light);

    @Query("SELECT * FROM Light WHERE network_id = :networkId")
    List<Light> getLights(Long networkId);

    @Query("SELECT * FROM Light WHERE network_id = :networkId AND name = :name")
    List<Light> getLightsOfName(String name, Long networkId);

    @Query("SELECT * FROM Light WHERE macId = :macId AND network_id = :networkId")
    List<Light> getLightsOfMacIdNetworkId(String macId, Long networkId);

    @Query("SELECT * FROM Light WHERE macId = :macId")
    Light getLightOfMacId(String macId);

    @Update
    void updateLight(Light light);

    @Query("UPDATE Light SET r_value=:rValue, g_value = :gValue,b_value = :bValue,w_value = :wValue,scene_value=:sceneValue,c_value = :cValue,brightness_value=:brightnessValue,on_off_value=:onOffStatus, rssi_value=:rssiValue, fw_version=:firmwareVersion WHERE macId = :macId")
    void updateLightStatus(String macId, String rValue, String gValue, String bValue, String wValue, String cValue, String sceneValue, Integer brightnessValue, Boolean onOffStatus, Integer rssiValue, String firmwareVersion);

    @Query("UPDATE Light SET r_value=:rValue, g_value = :gValue,b_value = :bValue,w_value = :wValue,scene_value=:sceneValue,c_value = :cValue,brightness_value=:brightnessValue,on_off_value=:onOffStatus, rssi_value=:rssiValue WHERE macId = :macId")
    void updateLightStatus(String macId, String rValue, String gValue, String bValue, String wValue, String cValue, String sceneValue, Integer brightnessValue, Boolean onOffStatus, Integer rssiValue);

    @Query("SELECT * FROM Light")
    List<Light> getAllLights();

    @Query("SELECT name FROM Light WHERE macId=:macId")
    String getLightName(String macId);

    @Query("SELECT * FROM Light WHERE network_id=:networkId")
    LiveData<List<Light>> getLightsLiveData(Long networkId);

    @Query("DELETE FROM Light WHERE network_id=:networkId")
    void clearLightsOfNetwork(Long networkId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLights(List<Light> lightsList);

    @Query("SELECT * FROM Light WHERE macId=:macId")
    LiveData<Light> getLightLiveData(String macId);

    @Query("UPDATE Light SET name=:newName WHERE macId=:macId")
    void updateLightName(String macId, String newName);

    @Query("UPDATE Light SET synced=:syncStatus WHERE macId=:macId")
    void updateSyncStatus(String macId, Integer syncStatus);

    @Query("SELECT * FROM Light WHERE synced=0")
    List<Light> getUnSyncedLights();

    @Query("UPDATE Light SET scene_value=:sceneType WHERE macId=:macId")
    void setLightSceneToDefualt(String macId, String sceneType);

    @Query("UPDATE Light SET fw_version=:firmwareVersion WHERE macId=:macId")
    void setFimwareVersion(String macId, String firmwareVersion);
}