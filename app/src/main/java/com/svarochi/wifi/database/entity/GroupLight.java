package com.svarochi.wifi.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Group.class,
                childColumns = {"group_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"id"}
        ), @ForeignKey(
                entity = Light.class,
                childColumns = {"mac_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"macId"}
        )},
        tableName = "GroupLight"
)
public class GroupLight {
    @PrimaryKey(autoGenerate = true)
    public Long id;

    @ColumnInfo(name = "group_id")
    public Long groupId;

    @ColumnInfo(name = "mac_id")
    public String macId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }
}