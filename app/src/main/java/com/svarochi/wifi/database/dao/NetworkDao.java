package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.Network;

import java.util.List;

@Dao
public interface NetworkDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNewNetwork(Network network);

    @Delete
    void deleteNetwork(Network network);

    @Query("DELETE FROM Network WHERE id = :networkId")
    void deleteNetwork(Long networkId);

    @Query("SELECT * FROM Network WHERE project_id = :projectId")
    List<Network> getNetworks(Long projectId);

    @Query("SELECT * FROM Network WHERE project_id =:projectId AND network_name =:name")
    List<Network> getNetworksOfName(String name, Long projectId);

    @Query("SELECT * FROM Network WHERE project_id=:projectId")
    LiveData<List<Network>> getNetworksLiveData(Long projectId);

    /*@Query("DELETE FROM Network WHERE project_id=:projectId")
    void clearWifiNetworks(Long projectId);*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNetworks(List<Network> networkList);

    @Query("UPDATE Network SET network_name =:name WHERE id =:networkId")
    void editNetworkName(Long networkId, String name);

    @Query("SELECT * FROM Network WHERE id = :networkId")
    List<Network> getNetwork(Long networkId);
}
