package com.svarochi.wifi.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Network.class,
                childColumns = {"network_id"},
                onDelete = 5,
                parentColumns = {"id"}
        )},
        tableName = "Scene"
)
public class Scene implements Serializable {
    @PrimaryKey
    public Long id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "network_id")
    public Long networkId;

    public Scene(Long id, String name, Long networkId) {
        this.id = id;
        this.name = name;
        this.networkId = networkId;
    }

}