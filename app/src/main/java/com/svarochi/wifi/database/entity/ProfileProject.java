package com.svarochi.wifi.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = {@ForeignKey(
        entity = Profile.class,
        childColumns = {"profile_id"},
        onDelete = ForeignKey.CASCADE,
        parentColumns = {"id"}
)})
public class ProfileProject {

    @PrimaryKey(autoGenerate = true)
    public Long id;

    @ColumnInfo(name = "profile_id")
    public String profileId;

    @ColumnInfo(name = "project_id")
    public Long projectId;

    @ColumnInfo(name = "network_id")
    public Long networkId;

    public ProfileProject(){
        super();
    }

}
