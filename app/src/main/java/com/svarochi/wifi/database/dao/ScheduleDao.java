package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.svarochi.wifi.database.entity.Schedule;

import java.util.List;

@Dao
public interface ScheduleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNewSchedule(Schedule schedule);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNewSchedules(List<Schedule> scheduleList);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateSchedule(Schedule schedule);

    @Query("SELECT * FROM Schedule WHERE network_id=:networkId")
    LiveData<List<Schedule>> getSchedules(long networkId);

    @Query("DELETE FROM Schedule")
    void clearTable();

    @Query("DELETE FROM Schedule WHERE id=:scheduleId")
    void deleteSchedule(long scheduleId);

    @Query("SELECT * FROM Schedule WHERE id=:scheduleId")
    Schedule getSchedule(long scheduleId);

    @Query("SELECT * FROM Schedule WHERE name=:name AND network_id=:networkId AND id<>:scheduleId")
    List<Schedule> getScheduleWithNameAndId(String name, long networkId, long scheduleId);

    @Query("SELECT * FROM Schedule WHERE name=:name AND network_id=:networkId")
    List<Schedule> getScheduleWithName(String name, long networkId);
}
