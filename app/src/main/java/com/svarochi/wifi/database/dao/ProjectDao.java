package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.Project;

import java.util.List;

@Dao
public interface ProjectDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNewProject(Project project);

    @Query("DELETE FROM Project WHERE id =:id")
    void deleteProject(Long id);

    @Query("SELECT * FROM Project")
    List<Project> getProjects();

    @Query("SELECT * FROM Project WHERE name =:name")
    List<Project> getProjectsOfName(String name);

    @Query("UPDATE Project SET name =:name WHERE id =:id")
    void editProjectName(Long id, String name);

    @Query("SELECT * FROM Project WHERE id =:id")
    Project getProjectOfId(Long id);

    @Query("SELECT * FROM Project")
    LiveData<List<Project>> getProjectsLiveData();

    @Query("DELETE FROM Project")
    void clearTable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProjects(List<Project> projectsList);
}
