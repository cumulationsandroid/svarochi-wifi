package com.svarochi.wifi.database

import androidx.lifecycle.LiveData
import com.svarochi.wifi.database.entity.*

interface LocalDataSourceImpl {

    // Networks
    fun addNewNetwork(network: Network)

    fun getNetworks(projectId: Long): List<Network>

    fun getNetworksOfName(name: String, type: String, projectId: Long): List<Network>

    // Projects
    fun getProjectsOfName(projectName: String): List<Project>

    fun getProjects(): List<Project>

    fun getProjectsLiveData(): LiveData<List<Project>>

    fun addNewProject(project: Project)

    fun editProjectName(projectId: Long, name: String)

    fun getProjectOfId(projectId: Long): Project

    fun deleteProject(projectId: Long)

    // CustomLight
    fun addNewLight(light: Light)

    fun getLights(networkId: Long): List<Light>

    fun getAllLights(): List<Light>

    fun getLightsOfMacIdNetworkId(macId: String, networkId: Long): List<Light>

    fun updateLight(light: Light)

    fun getLightsOfName(lampName: String, networkId: Long): List<Light>

    fun getLightOfMacId(macId: String): Light?

    fun updateLightStatus(
            macId: String,
            r_value: String?,
            g_value: String?,
            b_value: String?,
            w_value: String?,
            c_value: String?,
            sceneValue: String,
            brightness_value: Int,
            on_off_status: Boolean,
            rssiValue:Int,
            firmwareVersion:String
    )

    // Scenes
    fun addScene(scene: Scene)

    fun getScenes(networkId: Long): List<Scene>

    fun editSceneName(sceneId: Long, name: String)

    fun getScenesOfName(name: String): List<Scene>

    // Scene-CustomLight
    fun addLightToScene(sceneLight: SceneLight)

    fun getLightsFromScenesOfMacId(sceneId: Long, macId: String): List<SceneLight>

    fun getLightsOfScene(sceneId: Long): List<SceneLight>

    fun getSceneId(name: String): Long

    fun deleteScenes(sceneId: Long)

    /*fun setSceneStatus(sceneId: Long, status: Boolean)*/

    // Groups
    fun getGroups(networkId: Long): List<Group>

    fun addGroup(group: Group)

    fun editGroupName(groupId: Long, name: String)

    fun getGroupsOfName(name: String, networkId: Long): List<Group>

    fun getGroupId(name: String): Long

    fun addLightToGroup(groupLight: GroupLight)

    fun getLightsFromGroupsOfMacId(groupId: Long, macId: String): List<GroupLight>

    fun getLightsOfGroup(groupId: Long): List<GroupLight>

    fun deleteGroups(groupId: Long)

    fun deleteLight(macId: String, groupId: Long)

    fun getLatestGroup(groupId: Long): Group

    fun setGroupTag(groupId: Long, groupTag: Int)

    fun getLightName(macId: String): String?

    /*fun updateGroupStatus(
        groupId: Long,
        rValue: String?,
        gValue: String?,
        bValue: String?,
        wValue: String?,
        cValue: String?,
        sceneValue: String,
        brightnessValue: Int,
        onOffStatus: Boolean
    )*/

//    fun clearProjectsTable()

    fun insertProjects(projects: List<Project>)

    fun getNetworksLiveData(projectId: Long): LiveData<List<Network>>

    fun insertNetworks(networks: List<Network>)

//    fun clearWifiNetworksFromNetworksTable(projectId: Long)
    fun getLightsLiveData(networkId: Long): LiveData<List<Light>>
    fun clearLightsOfNetwork(networkId: Long)
    fun insertLights(lights: List<Light>)
    fun clearGroupsOfNetwork(networkId: Long)
    fun insertGroups(groupsList: List<Group>)
    fun insertGroupLights(groupLightsList: List<GroupLight>)
    fun getGroupsLiveData(networkId: Long): LiveData<List<Group>>
    fun clearScenesOfNetwork(networkId: Long)
    fun insertScenes(scenesList: List<Scene>)
    fun insertSceneLights(sceneLightsList: List<SceneLight>)
    fun getScenesLiveData(networkId: Long): LiveData<List<Scene>>
    fun clearLightsOfGroup(groupId: Long)
    fun getLatestScene(sceneId: Long): Scene
    fun getLightLiveData(macId: String): LiveData<Light>

    fun deleteLight(macId: String)
    fun getGroupLiveData(groupId: Long): LiveData<Group>
    fun editNetworkName(networkId: Long, name: String)
    fun updateLightName(macId: String, newName: String)
    fun updateGroupName(groupId: Long, groupName: String)
    fun updateSceneName(sceneId: Long, sceneName: String)

    /*fun clearSchedulesOfNetwork(networkId: Long)
    fun addSchedules(scheduleList: List<Schedule>)
    fun addSchedule(schedule: Schedule)
    fun getSchedulesLiveData(networkId: Long): LiveData<List<Schedule>>
    fun getSchedulesOfName(name: String): List<Schedule>
    fun getScheduleLiveData(scheduleId: Int): LiveData<Schedule>
    fun removeSchedule(scheduleId: Int)*/
    fun deleteScene(sceneId: Long)

    fun deleteGroup(groupId: Long)
    fun getProfilesSharedWith(userId: String): LiveData<List<Profile>>
    fun getProfilesSharedBy(userId: String): LiveData<List<Profile>>
    fun getAllProjects(): LiveData<List<Project>>
}