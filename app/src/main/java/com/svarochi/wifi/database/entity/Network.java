package com.svarochi.wifi.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Project.class,
                childColumns = {"project_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"id"}
        )},
        tableName = "Network"
)
public class Network implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public Long id;

    @ColumnInfo(name = "network_name")
    public String name;

    @ColumnInfo(name = "project_id")
    public Long projectId;

    @ColumnInfo(name = "wifi_ssid")
    public String wifiSsid;

    @ColumnInfo(name = "wifi_pw")
    public String wifiPassword;

    @ColumnInfo(name = "wifi_am")
    public String wifiAuthenticationMode;

    @ColumnInfo(name = "wifi_et")
    public String wifiEncryptionType;

    public Network(Long id, String name, Long projectId, String wifiSsid, String wifiPassword, String wifiAuthenticationMode, String wifiEncryptionType) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.wifiSsid = wifiSsid;
        this.wifiPassword = wifiPassword;
        this.wifiAuthenticationMode = wifiAuthenticationMode;
        this.wifiEncryptionType = wifiEncryptionType;
    }

    @Ignore
    public Network(Long id, Long projectId, String name) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.wifiSsid = "";
        this.wifiPassword = "";
        this.wifiAuthenticationMode = "";
        this.wifiEncryptionType = "";
    }
}
