package com.svarochi.wifi.database

enum class LampType(var value:String) {
    BRIGHT_AND_DIM("00"),
    WARM_AND_COOL("04"),
    COLOR_AND_DAYLIGHT("06"),
    NONE("ZZZ")
}