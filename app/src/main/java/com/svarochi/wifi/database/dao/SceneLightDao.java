package com.svarochi.wifi.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.SceneLight;

import java.util.List;

@Dao
public interface SceneLightDao {
    @Insert
    void addLightToScene(SceneLight sceneLight);

    @Query("SELECT * FROM SceneLight WHERE scene_id=:sceneId AND mac_id=:macId")
    List<SceneLight> getLightsFromScenesOfMacId(Long sceneId, String macId);

    @Query("SELECT * FROM SceneLight WHERE scene_id=:sceneId")
    List<SceneLight> getLightsOfScene(Long sceneId);

    @Query("DELETE FROM SceneLight WHERE scene_id=:sceneId")
    void deleteScenes(Long sceneId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSceneLights(List<SceneLight> sceneLightList);
}
