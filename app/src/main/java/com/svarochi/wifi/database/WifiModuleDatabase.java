package com.svarochi.wifi.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.svarochi.wifi.common.Constants;
import com.svarochi.wifi.database.dao.GroupDao;
import com.svarochi.wifi.database.dao.GroupLightDao;
import com.svarochi.wifi.database.dao.LightDao;
import com.svarochi.wifi.database.dao.NetworkDao;
import com.svarochi.wifi.database.dao.ProfileDao;
import com.svarochi.wifi.database.dao.ProfileProjectDao;
import com.svarochi.wifi.database.dao.ProjectDao;
import com.svarochi.wifi.database.dao.SceneDao;
import com.svarochi.wifi.database.dao.SceneLightDao;
import com.svarochi.wifi.database.dao.ScheduleDao;
import com.svarochi.wifi.database.entity.Group;
import com.svarochi.wifi.database.entity.GroupLight;
import com.svarochi.wifi.database.entity.Light;
import com.svarochi.wifi.database.entity.Network;
import com.svarochi.wifi.database.entity.Profile;
import com.svarochi.wifi.database.entity.ProfileProject;
import com.svarochi.wifi.database.entity.Project;
import com.svarochi.wifi.database.entity.Scene;
import com.svarochi.wifi.database.entity.SceneLight;
import com.svarochi.wifi.database.entity.Schedule;


@Database(
        entities = {Project.class, Network.class, Light.class, Scene.class, SceneLight.class, Group.class, GroupLight.class, Profile.class, ProfileProject.class, Schedule.class},
        version = Constants.DATABASE_VERSION
)
public abstract class WifiModuleDatabase extends RoomDatabase {
    public static void clearDatabase() {
    }

    public abstract ProjectDao projectDao();

    public abstract GroupDao groupDao();

    public abstract LightDao lightDao();

    public abstract SceneDao sceneDao();

    public abstract SceneLightDao sceneLightDao();

    public abstract NetworkDao networkDao();

    public abstract GroupLightDao groupLightDao();

    public abstract ProfileDao profileDao();

    public abstract ProfileProjectDao profileProjectsDao();

    public abstract ScheduleDao scheduleDao();


}
