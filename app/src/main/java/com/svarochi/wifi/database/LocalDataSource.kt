package com.svarochi.wifi.database

import androidx.lifecycle.LiveData
import com.svarochi.wifi.database.dao.*
import com.svarochi.wifi.database.entity.*
import com.svarochi.wifi.model.communication.SceneType
import timber.log.Timber

class LocalDataSource(appDatabase: WifiModuleDatabase) : LocalDataSourceImpl {

    private var lightsDao: LightDao = appDatabase.lightDao()
    private var scenesDao: SceneDao = appDatabase.sceneDao()
    private var scenesLightDao: SceneLightDao = appDatabase.sceneLightDao()
    private var projectsDao: ProjectDao = appDatabase.projectDao()
    private var networksDao: NetworkDao = appDatabase.networkDao()
    private var groupsDao: GroupDao = appDatabase.groupDao()
    private var groupLightDao: GroupLightDao = appDatabase.groupLightDao()
    private var profileDao: ProfileDao = appDatabase.profileDao()
    private var profileProjectsDao: ProfileProjectDao = appDatabase.profileProjectsDao()
    private var schedulesDao: ScheduleDao = appDatabase.scheduleDao()


    override fun addNewLight(light: Light) {
        light.macId = light.macId.toLowerCase()
        lightsDao.addNewLight(light)
    }

    override fun getAllLights(): List<Light> {
        return lightsDao.getAllLights()
    }

    override fun getLights(networkId: Long): List<Light> {
        return lightsDao.getLights(networkId)
    }

    override fun getLightsOfMacIdNetworkId(macId: String, networkId: Long): List<Light> {
        return lightsDao.getLightsOfMacIdNetworkId(macId, networkId)
    }

    override fun updateLight(light: Light) {
        lightsDao.updateLight(light)
    }

    override fun addNewProject(project: Project) {
        projectsDao.addNewProject(project)
    }

    override fun getProjectsOfName(projectName: String): List<Project> {
        return projectsDao.getProjectsOfName(projectName)
    }

    override fun editProjectName(projectId: Long, name: String) {
        projectsDao.editProjectName(projectId, name)
    }

    override fun getProjectOfId(projectId: Long): Project {
        return projectsDao.getProjectOfId(projectId)
    }

    override fun getProjects(): List<Project> {
        return projectsDao.getProjects()
    }

    override fun deleteProject(projectId: Long) {
        projectsDao.deleteProject(projectId)
    }

    override fun addNewNetwork(network: Network) {
        networksDao.addNewNetwork(network)
    }

    override fun getNetworks(projectId: Long): List<Network> {
        return networksDao.getNetworks(projectId)
    }

    override fun getNetworksOfName(name: String, type: String, projectId: Long): List<Network> {
        return networksDao.getNetworksOfName(name, projectId)
    }


    override fun getLightOfMacId(macId: String): Light? {
        return lightsDao.getLightOfMacId(macId)
    }

    override fun updateLightStatus(
            macId: String,
            r_value: String?,
            g_value: String?,
            b_value: String?,
            w_value: String?,
            c_value: String?,
            sceneValue: String,
            brightness_value: Int,
            on_off_status: Boolean,
            rssiValue: Int,
            firmwareVersion: String
    ) {
        lightsDao.updateLightStatus(
                macId,
                r_value,
                g_value,
                b_value,
                w_value,
                c_value,
                sceneValue,
                brightness_value,
                on_off_status,
                rssiValue,
                firmwareVersion
        )
    }

    fun updateLightStatus(
            macId: String,
            r_value: String?,
            g_value: String?,
            b_value: String?,
            w_value: String?,
            c_value: String?,
            sceneValue: String,
            brightness_value: Int,
            on_off_status: Boolean,
            rssiValue: Int
    ) {
        lightsDao.updateLightStatus(
                macId,
                r_value,
                g_value,
                b_value,
                w_value,
                c_value,
                sceneValue,
                brightness_value,
                on_off_status,
                rssiValue
        )
    }

    override fun getLightsOfName(lampName: String, networkId: Long): List<Light> {
        return lightsDao.getLightsOfName(lampName, networkId)
    }

    /*Scenes*/
    override fun addScene(scene: Scene) {
        scenesDao.addScene(scene)
    }

    override fun getScenes(networkId: Long): List<Scene> {
        return scenesDao.getScenes(networkId)
    }

    override fun editSceneName(sceneId: Long, name: String) {
        scenesDao.editSceneName(sceneId, name)
    }

    override fun getSceneId(name: String): Long {
        return scenesDao.getSceneId(name)
    }

    override fun getScenesOfName(name: String): List<Scene> {
        return scenesDao.getScenesOfName(name)
    }

    /*Scene-CustomLight*/
    override fun addLightToScene(sceneLight: SceneLight) {
        scenesLightDao.addLightToScene(sceneLight)
    }

    override fun getLightsFromScenesOfMacId(sceneId: Long, macId: String): List<SceneLight> {
        return scenesLightDao.getLightsFromScenesOfMacId(sceneId, macId)
    }

    override fun deleteScenes(sceneId: Long) {
        scenesLightDao.deleteScenes(sceneId)
    }

    override fun deleteScene(sceneId: Long) {
        scenesDao.deleteScene(sceneId)
    }

    override fun getLightsOfScene(sceneId: Long): List<SceneLight> {
        return scenesLightDao.getLightsOfScene(sceneId)
    }

    /*override fun setSceneStatus(sceneId: Long, status: Boolean) {
        scenesDao.setSceneStatus(sceneId, status)
    }*/

    /*Groups*/
    override fun getGroups(networkId: Long): List<Group> {
        return groupsDao.getGroups(networkId)
    }

    override fun addGroup(group: Group) {
        groupsDao.addNewGroup(group)
    }

    override fun editGroupName(groupId: Long, name: String) {
        groupsDao.editGroupName(name, groupId)
    }

    override fun getGroupsOfName(name: String, networkId: Long): List<Group> {
        return groupsDao.getGroupsOfName(name, networkId)
    }

    override fun getGroupId(name: String): Long {
        return groupsDao.getGroupId(name)
    }

    override fun addLightToGroup(groupLight: GroupLight) {
        groupLightDao.addLightToGroup(groupLight)
    }

    override fun getLightsFromGroupsOfMacId(groupId: Long, macId: String): List<GroupLight> {
        return groupLightDao.getLightsOfMacId(groupId, macId)
    }

    override fun getLightsOfGroup(groupId: Long): List<GroupLight> {
        return groupLightDao.getLightsOfGroup(groupId)
    }

    override fun deleteGroups(groupId: Long) {
        groupLightDao.deleteGroups(groupId)
    }

    override fun deleteLight(macId: String, groupId: Long) {
        groupLightDao.deleteLight(macId, groupId)
    }

    override fun getLatestGroup(groupId: Long): Group {
        return groupsDao.getLatestGroup(groupId)
    }

    override fun setGroupTag(groupId: Long, groupTag: Int) {
        groupsDao.setGroupTag(groupId, groupTag)
    }

    override fun getLightName(macId: String): String? {
        return lightsDao.getLightName(macId)
    }

    /*override fun updateGroupStatus(
        groupId: Long,
        rValue: String?,
        gValue: String?,
        bValue: String?,
        wValue: String?,
        cValue: String?,
        sceneValue: String,
        brightnessValue: Int,
        onOffStatus: Boolean
    ) {
        groupsDao.updateGroupStatus(groupId, rValue, gValue, bValue, wValue, cValue, sceneValue, brightnessValue, onOffStatus)
    }*/

    override fun getProjectsLiveData(): LiveData<List<Project>> {
        return projectsDao.getProjectsLiveData()
    }

    fun clearProjectsTable() {
        projectsDao.clearTable()
    }

    override fun insertProjects(projects: List<Project>) {
        projectsDao.insertProjects(projects)
    }

    override fun getNetworksLiveData(projectId: Long): LiveData<List<Network>> {
        return networksDao.getNetworksLiveData(projectId)
    }

    /*override fun clearWifiNetworksFromNetworksTable(projectId: Long) {
        networksDao.clearWifiNetworks(projectId)
    }*/

    override fun insertNetworks(networks: List<Network>) {
        networksDao.insertNetworks(networks)
    }

    override fun getLightsLiveData(networkId: Long): LiveData<List<Light>> {
        return lightsDao.getLightsLiveData(networkId)
    }

    override fun clearLightsOfNetwork(networkId: Long) {
        lightsDao.clearLightsOfNetwork(networkId)
    }

    override fun insertLights(lights: List<Light>) {
        lightsDao.insertLights(lights)
    }

    override fun clearGroupsOfNetwork(networkId: Long) {
        groupsDao.clearGroupsOfNetwork(networkId)
    }

    override fun insertGroups(groupsList: List<Group>) {
        groupsDao.insertGroups(groupsList)
    }

    override fun insertGroupLights(groupLightsList: List<GroupLight>) {
        groupLightDao.insertGroupLights(groupLightsList)
    }

    override fun getGroupsLiveData(networkId: Long): LiveData<List<Group>> {
        return groupsDao.getGroupsLiveData(networkId)
    }

    override fun clearScenesOfNetwork(networkId: Long) {
        return scenesDao.clearScenesOfNetwork(networkId)
    }

    override fun insertScenes(scenesList: List<Scene>) {
        return scenesDao.insertScenes(scenesList)
    }

    override fun insertSceneLights(sceneLightsList: List<SceneLight>) {
        return scenesLightDao.insertSceneLights(sceneLightsList)
    }

    override fun getScenesLiveData(networkId: Long): LiveData<List<Scene>> {
        return scenesDao.getScenesLiveData(networkId)
    }

    override fun clearLightsOfGroup(groupId: Long) {
        groupLightDao.deleteGroups(groupId)
    }

    override fun getLatestScene(sceneId: Long): Scene {
        return scenesDao.getScene(sceneId)
    }

    override fun getLightLiveData(macId: String): LiveData<Light> {
        return lightsDao.getLightLiveData(macId)
    }

    /*override fun clearSchedulesOfNetwork(networkId: Long) {
        Timber.d("Clearing schedules of network($networkId)")
        scheduleEventDao.deleteSchedulesOfNetwork(networkId)
    }

    override fun addSchedules(scheduleList: List<Schedule>) {
        Timber.d("Adding schedules to network")
        scheduleEventDao.addSchedules(scheduleList)
    }

    override fun addSchedule(schedule: Schedule) {
        scheduleEventDao.addSchedule(schedule)
    }

    override fun getSchedulesLiveData(networkId: Long): LiveData<List<Schedule>> {
        return scheduleEventDao.getSchedulesLiveData(networkId)
    }

    override fun getScheduleLiveData(scheduleId: Int): LiveData<Schedule> {
        return scheduleEventDao.getScheduleLiveData(scheduleId)
    }

    override fun getSchedulesOfName(name: String): List<Schedule> {
        return scheduleEventDao.getSchedulesOfName(name)
    }

    override fun removeSchedule(scheduleId: Int){
        scheduleEventDao.removeSchedule(scheduleId)
    }*/

    override fun deleteLight(macId: String) {
        var light = getLightOfMacId(macId)
        if(light != null) {
            lightsDao.deleteLight(light)
        }else{
            Timber.d("$macId doesnt exist in the local db")
        }
    }

    override fun getGroupLiveData(groupId: Long): LiveData<Group> {
        return groupsDao.getGroupLiveData(groupId)
    }

    override fun editNetworkName(networkId: Long, name: String) {
        networksDao.editNetworkName(networkId, name)
    }

    override fun updateLightName(macId: String, newName: String) {
        lightsDao.updateLightName(macId, newName)
    }

    override fun updateGroupName(groupId: Long, groupName: String) {
        groupsDao.updateGroupName(groupId, groupName)
    }

    override fun updateSceneName(sceneId: Long, sceneName: String) {
        scenesDao.editSceneName(sceneId, sceneName)
    }

    override fun deleteGroup(groupId: Long) {
        groupsDao.deleteGroup(groupId)
    }

    override fun getProfilesSharedWith(userId: String): LiveData<List<Profile>> {
        return profileDao.getProfilesSharedWith(userId)
    }

    override fun getProfilesSharedBy(userId: String): LiveData<List<Profile>> {
        return profileDao.getProfilesSharedBy(userId)
    }

    override fun getAllProjects(): LiveData<List<Project>> {
        return projectsDao.getProjectsLiveData()
    }

    fun getProfileProjectsOfProfile(profileId: String): LiveData<List<ProfileProject>> {
        return profileProjectsDao.getProjectAndNetworksOfProfile(profileId)
    }

    fun updateProfile(profileId: String, profileName: String, sharedBy: String, sharedWith: String, sharedWithData: String) {
        return profileDao.updateProfile(profileId, profileName, sharedBy, sharedWith, sharedWithData)
    }

    fun clearProfileProjectsOfProfile(profileId: String) {
        return profileProjectsDao.clearProfileProjectsOfProfile(profileId)
    }

    fun clearProfilesSharedBy(sharedBy: String) {
        return profileDao.clearProfilesSharedBy(sharedBy)
    }

    fun clearProfilesSharedWith(sharedWith: String) {
        return profileDao.clearProfilesSharedWith(sharedWith)
    }

    fun addProfiles(profiles: ArrayList<Profile>) {
        profileDao.addProfiles(profiles)
    }

    fun addProfileProject(profileProject: ProfileProject) {
        return profileProjectsDao.addProfileProject(profileProject)
    }

    fun addNewProfile(profile: Profile) {
        return profileDao.addProfile(profile)
    }

    fun addProfileProjects(profileProjectList: ArrayList<ProfileProject>) {
        return profileProjectsDao.addProfileProjects(profileProjectList)
    }

    fun deleteDevice(light: Light) {
        lightsDao.deleteLight(light)
    }

    fun getNetwork(networkId: Long): List<Network> {
        return networksDao.getNetwork(networkId)
    }

    fun getUnsyncedLights(): List<Light> {
        return lightsDao.getUnSyncedLights()
    }

    fun updateLightSyncStatus(macId: String, flag: Int) {
        return lightsDao.updateSyncStatus(macId, flag)
    }

    fun deleteNetwork(networkId: Long) {
        return networksDao.deleteNetwork(networkId)
    }

    fun addNewSchedule(schedule: Schedule) {
        return schedulesDao.addNewSchedule(schedule)
    }

    fun deleteSchedule(scheduleId: Long) {
        return schedulesDao.deleteSchedule(scheduleId)
    }

    fun editSchedule(newSchedule: Schedule) {
        return schedulesDao.updateSchedule(newSchedule)
    }

    fun getSchedule(scheduleId: Long): Schedule {
        return schedulesDao.getSchedule(scheduleId)
    }

    fun getScheduleWithName(name: String, networkId: Long, scheduleId: Long): List<Schedule> {
        return schedulesDao.getScheduleWithNameAndId(name, networkId, scheduleId)
    }

    fun getScheduleWithName(name: String, networkId: Long): List<Schedule> {
        return schedulesDao.getScheduleWithName(name, networkId)
    }

    fun getSchedules(networkId: Long): LiveData<List<Schedule>> {
        return schedulesDao.getSchedules(networkId)
    }

    fun clearSchedules() {
        schedulesDao.clearTable()
    }

    fun setLightSceneToDefault(macId: String) {
        lightsDao.setLightSceneToDefualt(macId, SceneType.DEFAULT.value)
    }

    fun updateLightFirmware(macId: String, firmwareVersion: String) {
        lightsDao.setFimwareVersion(macId, firmwareVersion)
    }
}