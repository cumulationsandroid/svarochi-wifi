package com.svarochi.wifi.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Network.class,
                childColumns = {"network_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"id"}
        )},
        tableName = "Group"
)
public class Group implements Serializable {
    @PrimaryKey
    public Long id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "network_id")
    public Long networkId;

    @ColumnInfo(name = "group_tag")
    public int groupTag;

    public Group(Long id, String name, Long networkId, int groupTag) {
        this.id = id;
        this.name = name;
        this.networkId = networkId;
        this.groupTag = groupTag;
    }
}

