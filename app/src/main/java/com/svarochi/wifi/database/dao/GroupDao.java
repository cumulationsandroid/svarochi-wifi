package com.svarochi.wifi.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.Group;

import java.util.List;

@Dao
public interface GroupDao {
    @Insert
    void addNewGroup(Group group);

    @Query("UPDATE `Group` SET name=:name WHERE id=:groupId")
    void editGroupName(String name, long groupId);

    @Query("SELECT * FROM `Group` WHERE network_id=:networkId")
    List<Group> getGroups(long networkId);

    @Query("SELECT id FROM `Group` WHERE name=:name")
    long getGroupId(String name);

    @Query("SELECT * FROM `Group` WHERE name=:groupName AND network_id=:networkId")
    List<Group> getGroupsOfName(String groupName, long networkId);

    @Query("SELECT * FROM `Group` WHERE id=:groupId")
    Group getLatestGroup(long groupId);

    @Query("UPDATE `Group` SET group_tag=:groupTag WHERE id=:groupId")
    void setGroupTag(long groupId, int groupTag);

    @Query("DELETE FROM `Group` WHERE network_id=:networkId")
    void clearGroupsOfNetwork(long networkId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertGroups(List<Group> groupsList);

    @Query("SELECT * FROM `Group` WHERE network_id=:networkId")
    LiveData<List<Group>> getGroupsLiveData(long networkId);

    @Query("SELECT * FROM `Group` WHERE id=:groupId")
    LiveData<Group> getGroupLiveData(long groupId);

    @Query("UPDATE `Group` SET name=:groupName WHERE id=:groupId")
    void updateGroupName(long groupId, String groupName);

    @Query("DELETE FROM `Group` WHERE id=:groupId")
    void deleteGroup(long groupId);
}