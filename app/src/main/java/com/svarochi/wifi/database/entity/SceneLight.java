package com.svarochi.wifi.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Scene.class,
                childColumns = {"scene_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"id"}
        ), @ForeignKey(
                entity = Light.class,
                childColumns = {"mac_id"},
                onDelete = ForeignKey.CASCADE,
                parentColumns = {"macId"}
        )},
        tableName = "SceneLight"
)
public class SceneLight {
    @PrimaryKey(autoGenerate = true)
    public Long id;

    @ColumnInfo(name = "scene_id")
    public Long sceneId;

    @ColumnInfo(name = "mac_id")
    public String macId;

    @ColumnInfo(name = "r_value")
    public String rValue;

    @ColumnInfo(name = "g_value")
    public String gValue;

    @ColumnInfo(name = "b_value")
    public String bValue;

    @ColumnInfo(name = "w_value")
    public String wValue;

    @ColumnInfo(name = "c_value")
    public String cValue;

    @ColumnInfo(name = "scene_value")
    public String sceneValue;

    public Long getSceneId() {
        return sceneId;
    }

    public void setSceneId(Long sceneId) {
        this.sceneId = sceneId;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public String getrValue() {
        return rValue;
    }

    public void setrValue(String rValue) {
        this.rValue = rValue;
    }

    public String getgValue() {
        return gValue;
    }

    public void setgValue(String gValue) {
        this.gValue = gValue;
    }

    public String getbValue() {
        return bValue;
    }

    public void setbValue(String bValue) {
        this.bValue = bValue;
    }

    public String getwValue() {
        return wValue;
    }

    public void setwValue(String wValue) {
        this.wValue = wValue;
    }

    public String getcValue() {
        return cValue;
    }

    public void setcValue(String cValue) {
        this.cValue = cValue;
    }

    public int getBrightnessValue() {
        return brightnessValue;
    }

    public void setBrightnessValue(int brightnessValue) {
        this.brightnessValue = brightnessValue;
    }

    public boolean isOnOffValue() {
        return onOffValue;
    }

    public void setOnOffValue(boolean onOffValue) {
        this.onOffValue = onOffValue;
    }

    public void setSceneValue(String sceneValue) {
        this.sceneValue = sceneValue;
    }

    public String getSceneValue() {
        return sceneValue;
    }

    @ColumnInfo(name = "brightness_value")
    public int brightnessValue;

    @ColumnInfo(name = "on_off_value")
    public boolean onOffValue;


    /*public SceneLight(Long sceneId, String macId, String rValue, String gValue, String bValue, String wValue, String cValue, int brightnessValue, boolean onOffValue) {
       *//* this.id = id;*//*
        this.sceneId = sceneId;
        this.macId = macId;
        this.rValue = rValue;
        this.gValue = gValue;
        this.bValue = bValue;
        this.wValue = wValue;
        this.cValue = cValue;
        this.brightnessValue = brightnessValue;
        this.onOffValue = onOffValue;
    }*/
}
