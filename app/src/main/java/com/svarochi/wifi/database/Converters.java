package com.svarochi.wifi.database;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.svarochi.wifi.model.api.common.Action;
import com.svarochi.wifi.model.api.common.EndAction;
import com.svarochi.wifi.model.api.common.SharedWithData;
import com.svarochi.wifi.model.api.common.StartAction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Converters {
    public static ArrayList<String> toStringArrayList(String value) {
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }


    public static ArrayList<Integer> toIntArrayList(String value) {
        Type listType = new TypeToken<ArrayList<Integer>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    public static EndAction toEndAction(String value) {
        return new Gson().fromJson(value, EndAction.class);
    }

    public static StartAction toStartAction(String value) {
        return new Gson().fromJson(value, StartAction.class);
    }

    public static SharedWithData toSharedWithData(String value) {
        return new Gson().fromJson(value, SharedWithData.class);
    }

    public static String fromStringArrayList(ArrayList<String> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }

    public static String fromStringList(List<String> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }

    public static String fromIntArrayList(ArrayList<Integer> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }

    public static String fromIntList(List<Integer> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }

    public static String fromAction(Action action) {
        Gson gson = new Gson();
        return gson.toJson(action);
    }

    public static String toString(SharedWithData sharedWithData) {
        Gson gson = new Gson();
        return gson.toJson(sharedWithData);
    }
}
