package com.svarochi.wifi.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.svarochi.wifi.database.entity.GroupLight;

import java.util.List;

@Dao
public interface GroupLightDao {
    @Insert
    void addLightToGroup(GroupLight groupLight);

    @Query("SELECT * FROM GroupLight WHERE group_id=:groupId")
    List<GroupLight> getLightsOfGroup(long groupId);

    @Query("SELECT * FROM GroupLight WHERE group_id=:groupId AND mac_id=:macId")
    List<GroupLight> getLightsOfMacId(long groupId, String macId);

    @Query("DELETE FROM GroupLight WHERE group_id=:groupId")
    void deleteGroups(long groupId);

    @Query("DELETE FROM GroupLight WHERE group_id=:groupId AND mac_id=:macId")
    void deleteLight(String macId, long groupId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertGroupLights(List<GroupLight> groupLightList);
}