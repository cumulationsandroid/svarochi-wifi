package com.svarochi.wifi.model.api.response

import com.svarochi.wifi.model.api.common.SharedWithData
import com.svarochi.wifi.model.common.SharingProject

data class GetSharedProfilesResponse(
        var status: String?,
        var description: String?,
        var records: ArrayList<ApiProfile>?
)

data class ApiProfile(
        var profile_id: String,
        var profile_name: String,
        var shared_by: String,
        var shared_with_data: SharedWithData,
        var projects: ArrayList<SharingProject>
)