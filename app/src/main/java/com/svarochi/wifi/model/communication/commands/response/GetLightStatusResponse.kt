package com.svarochi.wifi.model.communication.commands.response

import com.svarochi.wifi.common.Constants
import timber.log.Timber
import com.svarochi.wifi.model.communication.BasePayload

class GetLightStatusResponse(override var byteArray: ByteArray, override var sequenceNumber: Int) : BasePayload() {
    var isSuccess: Boolean = false
    var packetStatus: String
    var rValue: String
    var gValue: String
    var bValue: String
    var wValue: String
    var cValue: String
    var scnValue: String
    var lampType: String
    var lampBrightness: Int = 0
    var lampTurnedOn: Boolean = false
    var rssiValue: Int = 0


    init {
        val responseStringBuilder = StringBuilder()
        for (b1 in byteArray) {
            responseStringBuilder.append(String.format("%02X ", b1))
        }
        var responseString = responseStringBuilder.toString().replace(" ", "", true)
        responseString.replace(" ", "", true)

        Timber.d("Get Light Status response - $responseString")
        // 0002010304050006
        // 0123456789
        packetStatus = responseString.substring(0, 2)
        rValue = responseString.substring(2, 4)
        gValue = responseString.substring(4, 6)
        bValue = responseString.substring(6, 8)
        wValue = responseString.substring(8, 10)
        cValue = responseString.substring(10, 12)
        scnValue = responseString.substring(12, 14)
        lampType = responseString.substring(14, 16)
        lampBrightness = Integer.parseInt(responseString.substring(16, 18), 16)
        lampTurnedOn = responseString.substring(18, 20).equals("01")
        rssiValue = Integer.parseInt(responseString.substring(20, 22), 16)


        isSuccess = packetStatus.equals("00")
    }
}