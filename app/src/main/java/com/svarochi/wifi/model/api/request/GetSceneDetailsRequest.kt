package com.svarochi.wifi.model.api.request

data class GetSceneDetailsRequest(
    val network_id: Int,
    val project_id: Int,
    val scene_id: Int,
    val user_id: String
)