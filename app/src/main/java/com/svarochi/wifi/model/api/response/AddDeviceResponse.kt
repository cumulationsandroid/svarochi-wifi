package com.svarochi.wifi.model.api.response

data class AddDeviceResponse(
    val description: String?,
    val device_id: Int?,
    val status: String
)