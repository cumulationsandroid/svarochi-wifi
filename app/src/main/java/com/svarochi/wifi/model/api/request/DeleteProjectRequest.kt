package com.svarochi.wifi.model.api.request

data class DeleteProjectRequest(var project_id: Int, var user_id: String)