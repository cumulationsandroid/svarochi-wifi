package com.svarochi.wifi.model.common.events

import timber.log.Timber

open class Event(var type: EventType, var status: EventStatus, var data: Any?, var error: Throwable?) {

    companion object {
        private var eventInstance: Event? = null

        fun setEvent(type: EventType, status: EventStatus, data: Any?, error: Throwable?): Event {
            if (eventInstance != null) {
                Timber.d("used existing")
                eventInstance!!.type = type
                eventInstance!!.status = status
                eventInstance!!.data = data
                eventInstance!!.error = error
            } else {
                Timber.d("Created new")
                eventInstance = Event(type, status, data, error)
            }
            return eventInstance!!
        }

        /*fun success(lampType: EventType): Event {
            return Event(lampType, EventStatus.SUCCESS, null, null)
        }

        fun success(lampType: EventType, data: Any?): Event {
            return Event(lampType, EventStatus.SUCCESS, data, null)
        }

        fun failure(lampType: EventType): Event {
            return Event(lampType, EventStatus.FAILURE, null, null)
        }

        fun failure(lampType: EventType, data: Any?): Event {
            return Event(lampType, EventStatus.FAILURE, data, null)
        }

        fun loading(lampType: EventType): Event {
            return Event(lampType, EventStatus.LOADING, null, null)
        }

        fun loading(lampType: EventType, data: Any?): Event {
            return Event(lampType, EventStatus.FAILURE, data, null)
        }

        fun error(lampType: EventType, error: Throwable?): Event {
            return Event(lampType, EventStatus.ERROR, null, error)
        }

        fun error(lampType: EventType, error: Throwable?, data: Any?): Event {
            return Event(lampType, EventStatus.ERROR, data, error)
        }

        fun init(): Event {
            return Event(EventType.INIT, EventStatus.DEFAULT, null, null)
        }

        fun complete(lampType: EventType): Event {
            return Event(lampType, EventStatus.COMPLETE, null, null)
        }

        fun wifi(status: Boolean): Event {
            return Event(EventType.CONNECTION, EventStatus.WIFI, status, null)
        }*/
    }
}