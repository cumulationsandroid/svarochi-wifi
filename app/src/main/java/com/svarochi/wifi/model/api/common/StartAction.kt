package com.svarochi.wifi.model.api.common

import java.io.Serializable

data class StartAction(var start_power: String,
                       var start_red: Int,
                       var start_green: Int,
                       var start_blue: Int,
                       var start_brightness: Int,
                       var start_warm: Int,
                       var start_cool: Int,
                       var start_scene: Int
) : Action(), Serializable
