package com.svarochi.wifi.model.mqtt

enum class MqttPacketType(val value:Int) {
    SET_COLOR(36864),
    SET_PRESET_SCENE(36865),
    GET_LIGHT_STATUS(36873),
    SET_SCHEDULE(36866),
    RESET(36869),
    GET_FIRMWARE_VERSION(36871)
}