package com.svarochi.wifi.model.api.request

data class EditProjectRequest(
    val user_id: String,
    val project_id: Int,
    val project_name: String
)