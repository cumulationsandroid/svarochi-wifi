package com.svarochi.wifi.model.api.common

import java.io.Serializable

data class CustomSceneDevice(var blue: Int,
                             var brightness: Int,
                             var daylight_control: Int,
                             var green: Int,
                             val mac_id: String,
                             var red: Int,
                             var status: String,
                             var scene_value: Int,
                             val light_name: String,
                             var is_selected: Boolean = false,
                             val is_synced: Int = com.svarochi.wifi.common.Constants.LIGHT_UNSYNCED) : Serializable