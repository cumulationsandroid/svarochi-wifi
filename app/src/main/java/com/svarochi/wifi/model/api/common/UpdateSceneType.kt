package com.svarochi.wifi.model.api.common


enum class UpdateSceneType{
    UPDATE_NAME,
    EDIT_LIGHTS
}

