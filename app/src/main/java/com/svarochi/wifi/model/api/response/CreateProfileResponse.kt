package com.svarochi.wifi.model.api.response

data class CreateProfileResponse(
        var status: String,
        var description: String?,
        var profile_id: String?
)