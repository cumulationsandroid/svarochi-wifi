package com.svarochi.wifi.model.api.response

data class GetUserProfilesResponse(
        var status:String,
        var description:String?,
        var records:ArrayList<ApiProfile>?
)