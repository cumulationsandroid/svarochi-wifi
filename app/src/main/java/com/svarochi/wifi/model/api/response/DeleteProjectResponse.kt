package com.svarochi.wifi.model.api.response

data class DeleteProjectResponse(var description: String?, var status: String, var project_id: Int?)