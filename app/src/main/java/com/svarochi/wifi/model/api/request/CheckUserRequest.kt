package com.svarochi.wifi.model.api.request

data class CheckUserRequest(val user_id: String?, val phone:String?)