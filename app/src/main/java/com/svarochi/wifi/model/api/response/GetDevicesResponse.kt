package com.svarochi.wifi.model.api.response

data class GetDevicesResponse(
        val description:String?,
        val records: List<Device>?,
        val status: String
)

data class Device(
        val device_id: Int,
        val device_name: String,
        val device_type: String,
        val mac_id: String,
        val network_id: Int,
        val project_id: Int,
        val red: Int,
        val green: Int,
        val blue: Int,
        val brightness: Int,
        val daylight_control: Int,
        val status: String,
        val user_id: String,
        val rssi_value: Int,
        val firmware:String
)