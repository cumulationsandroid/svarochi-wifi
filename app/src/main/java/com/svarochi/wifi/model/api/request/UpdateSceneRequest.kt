package com.svarochi.wifi.model.api.request


data class UpdateSceneRequest(
        val device_states: List<DeviceState>,
        val network_id: Int,
        val project_id: Int,
        val scene_id: Int,
        val scene_name: String,
        val user_id: String
)

data class DeviceState(
        val mac_id:String,
        val blue: Int,
        val brightness: Int,
        val daylight_control: Int,
        val green: Int,
        val red: Int,
        val scene_id: Int,
        val status: String,
        val scene_value:Int
)