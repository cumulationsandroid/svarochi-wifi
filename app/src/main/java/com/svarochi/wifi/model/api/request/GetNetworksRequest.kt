package com.svarochi.wifi.model.api.request

data class GetNetworksRequest(
    val project_id: Int,
    val user_id: String
)