package com.svarochi.wifi.model.api.response

data class CheckUserResponse(
        var status: Any,
        var user_id: String?,
        var description: String?
)