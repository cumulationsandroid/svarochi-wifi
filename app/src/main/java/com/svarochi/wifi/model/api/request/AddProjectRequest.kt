package com.svarochi.wifi.model.api.request

data class AddProjectRequest(
        val user_id: String,
        val project_name: String
)