package com.svarochi.wifi.model.api.response

data class DeleteGroupResponse(
    val description:String?,
    val status:String
)