package com.svarochi.wifi.model.communication.commands.response

import com.svarochi.wifi.model.communication.BasePayload
import java.io.Serializable

class UdpBroadcastResponse(
    override var byteArray: ByteArray,
    override var sequenceNumber: Int,
    var macId: String
): BasePayload(), Serializable {

    var ipAddress = ""

    init {
        val responseString = StringBuilder()
        for (b1 in byteArray) {
            responseString.append(String.format("%02X ", b1))
        }
        ipAddress = responseString.toString()
    }

}