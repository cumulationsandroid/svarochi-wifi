package com.svarochi.wifi.model.api.response


data class GetProjectsResponse(
        val description: String?,
        val records: List<Project>?,
        val status: String
)

data class Project(
        val project_id: Int,
        val project_name: String,
        val user_id: String,
        val networks: List<Network>
)