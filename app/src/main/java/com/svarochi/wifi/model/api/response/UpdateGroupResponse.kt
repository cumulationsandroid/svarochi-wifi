package com.svarochi.wifi.model.api.response

data class UpdateGroupResponse(
    val description: String?,
    val group_id: Int?,
    val status: String
)