package com.svarochi.wifi.model.api.request

import com.svarochi.wifi.model.common.SharingProject

data class UpdateProfileRequest(
        var profile_id:String,
        var profile_name:String,
        var shared_by:String,
        var shared_with:String,
        var share_data:ArrayList<SharingProject>
)
