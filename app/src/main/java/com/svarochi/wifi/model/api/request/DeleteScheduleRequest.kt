package com.svarochi.wifi.model.api.request

data class DeleteScheduleRequest(
    val network_id: Int,
    val project_id: Int,
    val schedule_id: Int,
    val user_id: String
)