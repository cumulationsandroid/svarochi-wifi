package com.svarochi.wifi.model.common

data class CustomProject(var projectName:String, var isExpanded:Boolean = false)