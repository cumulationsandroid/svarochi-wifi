package com.svarochi.wifi.model.common

import io.reactivex.disposables.Disposable

data class DeviceBroadcast(var disposable: Disposable?, var count:Int)