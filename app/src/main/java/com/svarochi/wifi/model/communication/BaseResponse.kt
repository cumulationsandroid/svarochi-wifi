package com.svarochi.wifi.model.communication

import com.svarochi.wifi.model.communication.commands.response.*
import java.util.*

class BaseResponse(var responseArray: ByteArray) {

    private var length: String
    private var macId: String
    private var sequenceNumber: Int = 0
    lateinit var packetType: PacketType
    var payLoad: BasePayload? = null

    init {
        val responseStringBuilder = StringBuilder()
        for (b1 in responseArray) {
            responseStringBuilder.append(String.format("%02X ", b1))
        }
        var responseString = responseStringBuilder.toString().replace(" ", "", true)

        // 7e000d845dd74a6bab36373834900300
        length = responseString.substring(2, 6)
        macId = responseString.substring(6, 18).toLowerCase()
        sequenceNumber = responseString.substring(18, 26).toInt()
        when {
            responseString.substring(26, 30).equals("9003") -> packetType =
                    PacketType.SET_WIFI
            responseString.substring(26, 30).equals("4131") -> packetType =
                    PacketType.GET_IP
            responseString.substring(26, 30).equals("9000") -> packetType =
                    PacketType.SET_COLOR
            responseString.substring(26, 30).equals("9005") -> packetType =
                    PacketType.RESET_LAMP
            responseString.substring(26, 30).equals("9001") -> packetType =
                    PacketType.SET_SCENE
            responseString.substring(26, 30).equals("9009") -> packetType =
                    PacketType.LIGHT_STATUS
            responseString.substring(26, 30).equals("9007") -> packetType =
                    PacketType.GET_FIRMWARE_VERSION
        }

        when (packetType) {
            PacketType.SET_WIFI -> {
                var payloadArray: ByteArray = Arrays.copyOfRange(responseArray, 15, responseArray.size - 0) as ByteArray
                payLoad = SetWifiCredResponse(payloadArray, sequenceNumber, macId)
            }
            PacketType.GET_IP -> {
                var payloadArray: ByteArray = Arrays.copyOfRange(responseArray, 16, responseArray.size - 0) as ByteArray
                payLoad = UdpBroadcastResponse(payloadArray, sequenceNumber, macId)
            }
            PacketType.SET_COLOR -> {
                var payloadArray: ByteArray = Arrays.copyOfRange(responseArray, 15, responseArray.size - 0) as ByteArray
                payLoad = SetLampColorResponse(payloadArray, sequenceNumber)
            }
            PacketType.RESET_LAMP -> {
                var payloadArray: ByteArray = Arrays.copyOfRange(responseArray, 15, responseArray.size - 0) as ByteArray
                payLoad = ResetLampResponse(payloadArray, sequenceNumber)
            }
            PacketType.SET_SCENE -> {
                var payloadArray: ByteArray = Arrays.copyOfRange(responseArray, 15, responseArray.size - 0) as ByteArray
                payLoad = SetLampSceneResponse(payloadArray, sequenceNumber)
            }
            PacketType.LIGHT_STATUS -> {
                var payloadArray: ByteArray = Arrays.copyOfRange(responseArray, 15, responseArray.size - 0) as ByteArray
                payLoad = GetLightStatusResponse(payloadArray, sequenceNumber)
            }
            PacketType.GET_FIRMWARE_VERSION -> {
                var payloadArray: ByteArray = Arrays.copyOfRange(responseArray, 15, responseArray.size - 0) as ByteArray
                payLoad = GetFirmwareVersionResponse(payloadArray, sequenceNumber)
            }
        }

    }

    fun getPayloadInstance(): BasePayload {
        return payLoad!!
    }

    fun getSequenceNumber(): Int {
        return sequenceNumber
    }

    fun getMacId(): String {
        return macId
    }

}