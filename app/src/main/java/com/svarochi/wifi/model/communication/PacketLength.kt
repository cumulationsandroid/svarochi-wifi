package com.svarochi.wifi.model.communication

enum class PacketLength(var value: String) {
    SET_SCENE("000D"),
    SET_COLOR("0013"),
    RESET_LAMP("000D"),
    SET_WIFI("006F"),
    GET_IP("000C"),
    LIGHT_STATUS("000C"),
    SET_SCHEDULE("0018"),
    GET_FIRMWARE_VERSION("000C")
}