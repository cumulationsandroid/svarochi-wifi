package com.svarochi.wifi.model.common

open class LightStatus(
    var macId: String,
    var sequenceNumber:Int,
    var r_value: String?,
    var g_value: String?,
    var b_value: String?,
    var w_value: String?,
    var c_value: String?,
    var scene_value: String,
    var lamp_brightness: Int,
    var lamp_turned_on: Boolean,
    var lamp_rssi_value: Int
) {
}