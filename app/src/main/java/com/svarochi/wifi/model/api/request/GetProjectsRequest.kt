package com.svarochi.wifi.model.api.request

data class GetProjectsRequest(
    val user_id: String
)