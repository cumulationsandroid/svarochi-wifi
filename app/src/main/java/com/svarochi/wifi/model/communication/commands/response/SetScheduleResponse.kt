package com.svarochi.wifi.model.communication.commands.response

import com.svarochi.wifi.model.communication.BasePayload


class SetScheduleResponse(override var byteArray: ByteArray, override var sequenceNumber: Int) : BasePayload() {
    var isSuccess: Boolean = false

    init {
        val responseStringBuilder = StringBuilder()
        for (b1 in byteArray) {
            responseStringBuilder.append(String.format("%02X ", b1))
        }
        var responseString = responseStringBuilder.toString().replace(" ", "", true)
        responseString.replace(" ", "", true)
        isSuccess = responseString.equals("00")
    }

}