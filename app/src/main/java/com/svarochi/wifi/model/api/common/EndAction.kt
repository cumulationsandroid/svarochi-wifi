package com.svarochi.wifi.model.api.common

import java.io.Serializable

data class EndAction(var end_power: String,
                     var end_red: Int,
                     var end_green: Int,
                     var end_blue: Int,
                     var end_brightness: Int,
                     var end_warm: Int,
                     var end_cool: Int,
                     var end_scene: Int
):Action(),Serializable