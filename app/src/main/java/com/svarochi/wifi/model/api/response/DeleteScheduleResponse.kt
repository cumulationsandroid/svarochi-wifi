package com.svarochi.wifi.model.api.response

data class DeleteScheduleResponse(
    val description: String?,
    val status: String
)