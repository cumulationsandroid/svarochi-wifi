package com.svarochi.wifi.model.mqtt.commands

import com.svarochi.wifi.common.Constants.Companion.JSON_BRIGHTNESS_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_B_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_C_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_FRAME_NUM
import com.svarochi.wifi.common.Constants.Companion.JSON_G_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_MAC_ID
import com.svarochi.wifi.common.Constants.Companion.JSON_ON_OFF_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_PACKET_ID
import com.svarochi.wifi.common.Constants.Companion.JSON_R_VALUE
import com.svarochi.wifi.common.Constants.Companion.JSON_W_VALUE
import com.svarochi.wifi.common.Utilities
import org.json.JSONObject
import com.svarochi.wifi.model.mqtt.MqttPacketType

class SetColourRequest(
        var macId: String,
        var frameNum: String?,
        var rValue: String,
        var gValue: String,
        var bValue: String,
        var wValue: String,
        var cValue: String,
        var brightnessValue: Int,
        var onOff: Boolean
) {
    // It takes in values in Hex String and later convert to Int for json, as it would be similar to TCP model packets

    fun getRequestJson(): JSONObject {

        var requestJson = JSONObject()

        requestJson.put(JSON_PACKET_ID, MqttPacketType.SET_COLOR.value)
        requestJson.put(JSON_MAC_ID, macId)
        if (frameNum == null) {
            frameNum = Utilities.getRandomSequenceNumber()
        }
        requestJson.put(JSON_FRAME_NUM, frameNum!!.toInt())
        requestJson.put(JSON_R_VALUE, Utilities.convertHexStringToInt(rValue))
        requestJson.put(JSON_G_VALUE, Utilities.convertHexStringToInt(gValue))
        requestJson.put(JSON_B_VALUE, Utilities.convertHexStringToInt(bValue))
        requestJson.put(JSON_W_VALUE, Utilities.convertHexStringToInt(wValue))
        requestJson.put(JSON_C_VALUE, Utilities.convertHexStringToInt(cValue))
        requestJson.put(JSON_BRIGHTNESS_VALUE, brightnessValue)

        if (onOff) {
            requestJson.put(JSON_ON_OFF_VALUE, 1)
        } else {
            requestJson.put(JSON_ON_OFF_VALUE, 0)
        }

        return requestJson
    }
}