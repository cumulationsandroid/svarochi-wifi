package com.svarochi.wifi.model.api.request


data class EditNetworkRequest(
    val user_id: String,
    val project_id: Int,
    val network_id: Int,
    val network_name: String,
    val ssid: String,
    val password: String,
    val authentication_mode: String,
    val encryption_type: String
)