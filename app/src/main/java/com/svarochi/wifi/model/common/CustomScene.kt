package com.svarochi.wifi.model.common


class CustomScene(
    var scene_name: String,
    var scene_id: Long,
    var macIdList: List<String>
) {
}