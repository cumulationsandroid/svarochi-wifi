package com.svarochi.wifi.model.api.response

data class EditProjectResponse(
    val description: String?,
    val project_id:Int?,
    val status: String
)