package com.svarochi.wifi.model.api.response

data class AddGroupResponse(
    val description: String?,
    val group_id: Int?,
    val status: String
)