package com.svarochi.wifi.model.common

class Song(
    var id: Long = 0,
    var path: String,
    var title: String,
    var artist: String
) {
}