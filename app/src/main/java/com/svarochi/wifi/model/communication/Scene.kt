package com.svarochi.wifi.model.communication

enum class Scene(var value:Int) {
    CANDLE_LIGHT(3),
    SUNSET(2),
    ENERGISE(5),
    PARTY_MODE(17),
    RAINBOW(18),
    COLOR_BLAST(25),
    DEFAULT(0)
}