package com.svarochi.wifi.model.api.response

data class EditNetworkResponse(
    val description: String?,
    val network_id: Int?,
    val status: String
)