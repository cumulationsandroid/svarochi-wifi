package com.svarochi.wifi.model.communication

import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Constants.Companion.RESET_OP_VALUE
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.common.Utilities.Companion.convertIntToHexString
import com.svarochi.wifi.model.communication.commands.request.SetLampColorRequest
import com.svarochi.wifi.model.communication.commands.request.SetLampSceneRequest
import com.svarochi.wifi.model.communication.commands.request.SetScheduleRequest
import com.svarochi.wifi.model.communication.commands.request.SetWifiCredRequest

class BaseRequest(
        var macId: String,
        var sequenceNumber: String,
        var packetType: PacketType,
        var payLoad: BasePayload?
) {
    private lateinit var byteArray: ByteArray

    fun getByteArray(): ByteArray {
        var byteString = ""
        when (packetType) {
            PacketType.SET_WIFI -> {
                byteString += Constants.DELIMITER
                byteString += PacketLength.SET_WIFI.value
                byteString += macId
                byteString += sequenceNumber
                byteString += PacketType.SET_WIFI.value
                byteString += (payLoad as SetWifiCredRequest).operation
                byteString += Utilities.appendZerosToSSID(Utilities.stringToHexString(((payLoad as SetWifiCredRequest).ssid)))
                byteString += (payLoad as SetWifiCredRequest).authentication
                byteString += (payLoad as SetWifiCredRequest).encryption
                byteString += Utilities.appendZerosToPP(Utilities.stringToHexString(((payLoad as SetWifiCredRequest).password)))

                byteArray = Utilities.stringToHexByteArray(byteString, 114)
            }
            PacketType.GET_IP -> {
                byteString += Constants.DELIMITER
                byteString += PacketLength.GET_IP.value
                byteString += macId
                byteString += sequenceNumber
                byteString += PacketType.GET_IP.value

                byteArray = Utilities.stringToHexByteArray(byteString, 15)
            }
            PacketType.SET_COLOR -> {
                // 7e 0013 845dd7696379 36373834 9000 ff 0f 2a 3b 4c 64 01

                byteString += Constants.DELIMITER // 0
                byteString += PacketLength.SET_COLOR.value // 1 - 2
                byteString += macId // 3 - 8
                byteString += sequenceNumber // 9 - 12
                byteString += PacketType.SET_COLOR.value // 13 - 14
                byteString += (payLoad as SetLampColorRequest).r_value // 15
                byteString += (payLoad as SetLampColorRequest).g_value // 16
                byteString += (payLoad as SetLampColorRequest).b_value // 17
                byteString += (payLoad as SetLampColorRequest).w_value // 18
                byteString += (payLoad as SetLampColorRequest).cct_value // 19
                byteString += convertIntToHexString((payLoad as SetLampColorRequest).brightness_value) // 20
                if ((payLoad as SetLampColorRequest).on_off_status) {
                    byteString += "01" // 21
                } else {
                    byteString += "00" // 21
                }

                byteArray = Utilities.stringToHexByteArray(byteString, 22)

                //byteArray = Utilities.stringToHexByteArray("7e0013845dd7696379363738349000ff0f2a3b4c6401", 22)
            }
            PacketType.RESET_LAMP -> {
                byteString += Constants.DELIMITER
                byteString += PacketLength.RESET_LAMP.value
                byteString += macId
                byteString += sequenceNumber
                byteString += PacketType.RESET_LAMP.value
                byteString += RESET_OP_VALUE

                byteArray = Utilities.stringToHexByteArray(byteString, 16)
            }
            PacketType.SET_SCENE -> {
                byteString += Constants.DELIMITER
                byteString += PacketLength.SET_SCENE.value
                byteString += macId
                byteString += sequenceNumber
                byteString += PacketType.SET_SCENE.value
                byteString += (payLoad as SetLampSceneRequest).sceneType.value

                byteArray = Utilities.stringToHexByteArray(byteString, 16)
            }
            PacketType.LIGHT_STATUS -> {
                byteString += Constants.DELIMITER
                byteString += PacketLength.LIGHT_STATUS.value
                byteString += macId
                byteString += sequenceNumber
                byteString += PacketType.LIGHT_STATUS.value

                byteArray = Utilities.stringToHexByteArray(byteString, 15)
            }
            PacketType.SET_SCHEDULE -> {
                byteString += Constants.DELIMITER // 0
                byteString += PacketLength.SET_SCHEDULE.value // 1 - 2
                byteString += macId // 3 - 8
                byteString += sequenceNumber // 9 - 12
                byteString += PacketType.SET_SCHEDULE.value // 13 - 14
                byteString += (payLoad as SetScheduleRequest).slot_id // 15
                byteString += (payLoad as SetScheduleRequest).start_time.substring(0, 2) // 16 - 17
                byteString += (payLoad as SetScheduleRequest).start_time.substring(2, 4) // 17 - 18
                byteString += (payLoad as SetScheduleRequest).start_time.substring(4, 6) // 18 - 19
                byteString += (payLoad as SetScheduleRequest).end_time.substring(0, 2) // 19 - 20
                byteString += (payLoad as SetScheduleRequest).end_time.substring(2, 4) // 21 - 22
                byteString += (payLoad as SetScheduleRequest).end_time.substring(4, 6) // 23 - 24
                byteString += (payLoad as SetScheduleRequest).r_value // 25
                byteString += (payLoad as SetScheduleRequest).g_value // 26
                byteString += (payLoad as SetScheduleRequest).b_value // 27
                byteString += (payLoad as SetScheduleRequest).w_value // 28
                byteString += (payLoad as SetScheduleRequest).c_value // 29
                byteString += (payLoad as SetScheduleRequest).brightness_value // 30

                byteArray = Utilities.stringToHexByteArray(byteString, 31)
            }

            PacketType.GET_FIRMWARE_VERSION -> {
                byteString += Constants.DELIMITER // 0
                byteString += PacketLength.GET_FIRMWARE_VERSION.value // 1 - 2
                byteString += macId // 3 - 8
                byteString += sequenceNumber // 9 - 12
                byteString += PacketType.GET_FIRMWARE_VERSION.value // 13 - 14

                byteArray = Utilities.stringToHexByteArray(byteString, 15)
            }
        }
        return byteArray
    }

}