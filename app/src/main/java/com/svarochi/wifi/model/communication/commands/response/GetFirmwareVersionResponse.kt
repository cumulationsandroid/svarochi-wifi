package com.svarochi.wifi.model.communication.commands.response

import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.model.communication.BasePayload

class GetFirmwareVersionResponse(override var byteArray: ByteArray, override var sequenceNumber: Int) : BasePayload() {
    var isSuccess: Boolean = false
    var firmwareVersion: String = ""


    init {
        val responseStringBuilder = StringBuilder()
        for (b1 in byteArray) {
            responseStringBuilder.append(String.format("%02X ", b1))
        }
        var responseString = responseStringBuilder.toString().replace(" ", "", true)
        responseString.replace(" ", "", true)
        isSuccess = responseString.substring(0,2).equals("00")
        firmwareVersion = Utilities.convertToFWVersion(responseString.substring(2))
    }
}
