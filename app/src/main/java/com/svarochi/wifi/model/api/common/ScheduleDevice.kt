package com.svarochi.wifi.model.api.common

data class ScheduleDevice(
    val group_id: Long?,
    val mac_id: String,
    val slot: Int,
    val red: Int,
    val green: Int,
    val blue:Int,
    val warm: Int,
    val cool: Int,
    val brightness: Int
)