package com.svarochi.wifi.model.api.response

data class GetSharedProjectsResponse(
        var status: String,
        var description: String?,
        var records: ArrayList<Project>?
)