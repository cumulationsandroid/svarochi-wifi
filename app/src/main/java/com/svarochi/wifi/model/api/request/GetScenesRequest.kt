package com.svarochi.wifi.model.api.request

data class GetScenesRequest(
    val network_id: Int,
    val project_id: Int,
    val user_id: String
)