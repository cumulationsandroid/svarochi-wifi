package com.svarochi.wifi.model.api.request

data class AddDeviceRequest(
        val device_name: String,
        val device_type: String,
        val mac_id: String,
        val network_id: Int,
        val project_id: Int,
        val user_id: String
)