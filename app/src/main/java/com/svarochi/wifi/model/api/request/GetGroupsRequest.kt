package com.svarochi.wifi.model.api.request

data class GetGroupsRequest(
    val network_id: Int,
    val project_id: Int,
    val user_id: String
)