package com.svarochi.wifi.model.mqtt.commands

import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.model.communication.SceneType
import org.json.JSONObject
import com.svarochi.wifi.model.mqtt.MqttPacketType

class SetPresetSceneRequest(var macId: String, var frameNum: String?, var sceneType: SceneType) {

    fun getRequestJson(): JSONObject {
        var requestJson = JSONObject()

        requestJson.put(Constants.JSON_PACKET_ID, MqttPacketType.SET_PRESET_SCENE.value)
        requestJson.put(Constants.JSON_MAC_ID, macId)

        if (frameNum == null) {
            frameNum = Utilities.getRandomSequenceNumber()
        }
        requestJson.put(Constants.JSON_FRAME_NUM, frameNum!!.toInt())
        requestJson.put(Constants.JSON_PRESET_SCENE_VALUE, Utilities.convertHexStringToInt(sceneType.value))

        return requestJson
    }
}