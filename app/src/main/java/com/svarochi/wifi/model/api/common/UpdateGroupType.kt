package com.svarochi.wifi.model.api.common

enum class UpdateGroupType {
    UPDATE_NAME,
    ADD_LIGHT,
    EDIT_LIGHTS,
    REMOVE_LIGHT
}