package com.svarochi.wifi.model.api.request

data class AddGroupRequest(
    val mac_ids: List<String>,
    val group_name: String,
    val network_id: Int,
    val project_id: Int,
    val user_id: String
)