package com.svarochi.wifi.model.api.request


data class AddSceneRequest(
        val network_id: Int,
        val project_id: Int,
        val scene_devices: List<SceneDevice>,
        val scene_name: String,
        val user_id: String
)

data class SceneDevice(
        val mac_id: String,
        val blue: Int,
        val brightness: Int,
        val daylight_control: Int,
        val green: Int,
        val red: Int,
        val status: String,
        val scene_value: Int
)
