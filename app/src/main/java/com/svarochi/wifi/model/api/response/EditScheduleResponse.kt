package com.svarochi.wifi.model.api.response

data class EditScheduleResponse(
    val description: String?,
    val schedule_id: Int?,
    val status: String
)