package com.svarochi.wifi.model.communication.commands.request

import com.svarochi.wifi.model.communication.BasePayload
import com.svarochi.wifi.model.communication.SceneType

class SetLampSceneRequest(var sceneType: SceneType): BasePayload(){

}