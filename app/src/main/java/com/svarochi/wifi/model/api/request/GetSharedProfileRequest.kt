package com.svarochi.wifi.model.api.request

data class GetSharedProfilesRequest(var shared_by:String)