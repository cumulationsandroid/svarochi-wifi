package com.svarochi.wifi.model.api.request

data class GetSharedDevicesRequest(
        var profile_id: String,
        var project_id: Int,
        var network_id: Int
)