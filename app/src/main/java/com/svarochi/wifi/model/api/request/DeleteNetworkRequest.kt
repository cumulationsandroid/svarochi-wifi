package com.svarochi.wifi.model.api.request

data class DeleteNetworkRequest(var user_id: String, var network_id: Int, var project_id: Int)