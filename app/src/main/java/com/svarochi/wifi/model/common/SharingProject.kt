package com.svarochi.wifi.model.common

data class SharingProject(
        var project_id:Long,
        var networks:ArrayList<Long>
)