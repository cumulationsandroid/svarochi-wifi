package com.svarochi.wifi.model.common

import com.svarochi.wifi.database.entity.Profile

data class CustomProfile(var profile: Profile, var isSelected: Boolean = false)