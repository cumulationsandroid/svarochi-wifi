package com.svarochi.wifi.model.communication.commands.request

import com.svarochi.wifi.model.communication.BasePayload

class SetScheduleRequest(
    var slot_id: String,
    var start_time: String,
    var end_time: String,
    var r_value: String,
    var g_value: String,
    var b_value: String,
    var w_value: String,
    var c_value: String,
    var brightness_value: String
) : BasePayload() {
}