package com.svarochi.wifi.model.common

import com.svarochi.wifi.database.entity.Light
import java.io.Serializable

class CustomLight(
        var light: Light
) : Serializable {
    private var isLocallyAvailable: Boolean = false
    private var selected: Boolean = false

    fun updateLight(light: Light) {
        this.light = light
    }

    fun setLocallyAvailable(locallyAvailable: Boolean) {
        isLocallyAvailable = locallyAvailable
    }

    fun setSelected(selected: Boolean) {
        this.selected = selected
    }

    fun isLocallyAvailable() = isLocallyAvailable

    fun isSelected() = selected
}