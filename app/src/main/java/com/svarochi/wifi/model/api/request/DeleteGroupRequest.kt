package com.svarochi.wifi.model.api.request

data class DeleteGroupRequest(
    val user_id:String,
    val project_id:Int,
    val network_id:Int,
    val group_id:Int
)