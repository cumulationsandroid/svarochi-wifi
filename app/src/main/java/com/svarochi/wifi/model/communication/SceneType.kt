package com.svarochi.wifi.model.communication

enum class SceneType(var value:String) {
    DEFAULT("00"),
    MOON_LIGHT("01"),
    CANDLE_LIGHT("03"),
    STUDY("06"),
    SUNSET("02"),
    ENERGISE("05"),
    AQUA("07"),
    MEDITATION("04"),
    PARTY_MODE("11"),
    RAINBOW("12"),
    COLOR_BLAST("19")
}