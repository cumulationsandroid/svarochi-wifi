package com.svarochi.wifi.model.api.response

data class UpdateSceneResponse(
    val description: String?,
    val scene_id: Int?,
    val status: String
)