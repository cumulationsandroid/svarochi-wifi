package com.svarochi.wifi.model.common

data class LightStatusResponse(
        var macId:String,
        var isFromTcp:Boolean,
        var rssiValue:Int
)