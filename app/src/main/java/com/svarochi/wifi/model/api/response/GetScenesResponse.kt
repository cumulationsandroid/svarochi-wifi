package com.svarochi.wifi.model.api.response

data class GetScenesResponse(
        val description: String?,
        val records: List<GetScenesRecord>?,
        val status: String
)

data class GetScenesRecord(
        val devices: List<GetScenesDevice>,
        val network_id: Int,
        val project_id: Int,
        val scene_id: Int,
        val scene_name: String,
        val user_id: String
)

data class GetScenesDevice(
        val blue: Int,
        val brightness: Int,
        val daylight_control: Int,
        val green: Int,
        val mac_id: String,
        val red: Int,
        val status: String,
        val scene_value: Int
)