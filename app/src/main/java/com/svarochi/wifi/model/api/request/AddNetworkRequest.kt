package com.svarochi.wifi.model.api.request

data class AddNetworkRequest(
    val network_name: String,
    val project_id: Int,
    val user_id: String,
    val ssid: String,
    val password: String,
    val authentication_mode: String,
    val encryption_type: String
)