package com.svarochi.wifi.model.api.response


data class GetNetworksResponse(
        val description: String?,
        val records: List<Network>?,
        val status: String
)


data class Network(
        val network_id: Int,
        val network_name: String,
        val project_id: Int,
        val ssid: String,
        val password: String,
        val authentication_mode: String,
        val encryption_type: String
)