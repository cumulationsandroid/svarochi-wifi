package com.svarochi.wifi.model.api.response

data class DeleteNetworkResponse(var description: String?, var network_id: Int?, var status: String)