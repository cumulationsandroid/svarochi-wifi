data class GetSchedulesResponse(
        val records: List<ApiSchedule>?,
        val status: String,
        val description: String?
)

data class ApiSchedule(
        val end_blue: Int,
        val end_brightness: Int,
        val end_cool: Int,
        val end_green: Int,
        val end_power: String,
        val end_red: Int,
        val end_scene: Int,
        val end_time: String,
        val end_warm: Int,
        val network: Int,
        val project: Int,
        val schedule_days: List<String>,
        val schedule_devices: List<String>,
        val schedule_groups: List<Int>,
        val schedule_id: Int,
        val schedule_name: String,
        val start_blue: Int,
        val start_brightness: Int,
        val start_cool: Int,
        val start_green: Int,
        val start_power: String,
        val start_red: Int,
        val start_scene: Int,
        val start_time: String,
        val start_warm: Int,
        val user: String
)