package com.svarochi.wifi.model.mqtt.commands

import com.svarochi.wifi.common.Constants.Companion.JSON_FRAME_NUM
import com.svarochi.wifi.common.Constants.Companion.JSON_MAC_ID
import com.svarochi.wifi.common.Constants.Companion.JSON_PACKET_ID
import com.svarochi.wifi.common.Utilities
import org.json.JSONObject
import com.svarochi.wifi.model.mqtt.MqttPacketType

class GetLightStatusRequest(
        var macId: String,
        var frameNum: String?
) {
    // It takes in values in Hex String and later convert to Int for json, as it would be similar to TCP model packets

    fun getRequestJson(): JSONObject {

        var requestJson = JSONObject()

        requestJson.put(JSON_PACKET_ID, MqttPacketType.GET_LIGHT_STATUS.value)
        requestJson.put(JSON_MAC_ID, macId)
        if (frameNum == null) {
            frameNum = Utilities.getRandomSequenceNumber()
        }
        requestJson.put(JSON_FRAME_NUM, frameNum!!.toInt())

        return requestJson
    }
}