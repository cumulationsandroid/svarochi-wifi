package com.svarochi.wifi.model.api.response


data class GetSceneDetailsResponse(
        val description: String?,
        val record: SceneDetails?,
        val status: String
)

data class SceneDetails(
        val details: List<DeviceDetails>,
        val network_id: Int,
        val project_id: Int,
        val scene_id: Int,
        val scene_name: String,
        val user_id: String
)

data class DeviceDetails(
        val blue: Int,
        val brightness: Int,
        val daylight_control: Int,
        val green: Int,
        val id: Int,
        val mac_id: String,
        val red: Int,
        val scene_id: Int,
        val status: String
)
