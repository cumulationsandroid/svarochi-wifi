package com.svarochi.wifi.model.api.response

data class GetGroupDetailsResponse(
        val description: String?,
        val record: Group?,
        val status: String
)

data class Group(
        val devices: List<String>,
        val group_id: Int,
        val group_name: String,
        val network_id: Int,
        val project_id: Int,
        val user_id: String
)