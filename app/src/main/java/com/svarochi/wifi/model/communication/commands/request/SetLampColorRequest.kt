package com.svarochi.wifi.model.communication.commands.request

import com.svarochi.wifi.model.communication.BasePayload


class SetLampColorRequest(
    var r_value: String,
    var g_value: String,
    var b_value: String,
    var w_value: String,
    var cct_value: String,
    var brightness_value: Int,
    var on_off_status: Boolean
) : BasePayload() {
}