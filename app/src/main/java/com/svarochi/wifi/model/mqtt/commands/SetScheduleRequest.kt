package com.svarochi.wifi.model.mqtt.commands

import org.json.JSONObject
import com.svarochi.wifi.common.Constants
import com.svarochi.wifi.common.Utilities
import com.svarochi.wifi.model.mqtt.MqttPacketType

class SetScheduleRequest(
    var macId: String,
    var frameNum: String?,
    var scheduleId: Int,
    var startTime: Long,
    var stopTime: Long,
    var rValue: Any,
    var gValue: Any,
    var bValue: Any,
    var wValue: Any,
    var cValue: Any,
    var brightnessValue: Int
) {

    fun getRequestJson(): JSONObject {
        var requestJson = JSONObject()

        requestJson.put(Constants.JSON_PACKET_ID, MqttPacketType.SET_SCHEDULE.value)
        requestJson.put(Constants.JSON_MAC_ID, macId)

        if (frameNum == null) {
            frameNum = Utilities.getRandomSequenceNumber()
        }
        requestJson.put(Constants.JSON_FRAME_NUM, frameNum!!.toInt())

        requestJson.put(Constants.JSON_SCHEDULE_ID, scheduleId)
        requestJson.put(Constants.JSON_SCHEDULE_START_TIME, startTime)
        requestJson.put(Constants.JSON_SCHEDULE_STOP_TIME, stopTime)

        if (rValue is String) {
            requestJson.put(Constants.JSON_SCHEDULE_R_VALUE, Utilities.convertHexStringToInt(rValue as String))
        } else if (rValue is Int) {
            requestJson.put(Constants.JSON_SCHEDULE_R_VALUE, rValue as Int)
        }

        if (gValue is String) {
            requestJson.put(Constants.JSON_SCHEDULE_G_VALUE, Utilities.convertHexStringToInt(gValue as String))
        } else if (rValue is Int) {
            requestJson.put(Constants.JSON_SCHEDULE_G_VALUE, gValue as Int)
        }

        if (bValue is String) {
            requestJson.put(Constants.JSON_SCHEDULE_B_VALUE, Utilities.convertHexStringToInt(bValue as String))
        } else if (rValue is Int) {
            requestJson.put(Constants.JSON_SCHEDULE_B_VALUE, bValue as Int)
        }

        if (wValue is String) {
            requestJson.put(Constants.JSON_SCHEDULE_W_VALUE, Utilities.convertHexStringToInt(wValue as String))
        } else if (rValue is Int) {
            requestJson.put(Constants.JSON_SCHEDULE_W_VALUE, wValue as Int)
        }

        if (cValue is String) {
            requestJson.put(Constants.JSON_SCHEDULE_C_VALUE, Utilities.convertHexStringToInt(cValue as String))
        } else if (rValue is Int) {
            requestJson.put(Constants.JSON_SCHEDULE_C_VALUE, cValue as Int)
        }

        requestJson.put(Constants.JSON_SCHEDULE_BRIGHTNESS_VALUE, brightnessValue)


        return requestJson
    }

    fun getRequestMap():Map<String, Any>{
        var requestMap = HashMap<String, Any>()

        requestMap.put(Constants.JSON_PACKET_ID, MqttPacketType.SET_SCHEDULE.value)
        requestMap.put(Constants.JSON_MAC_ID, macId)

        if (frameNum == null) {
            frameNum = Utilities.getRandomSequenceNumber()
        }
        requestMap.put(Constants.JSON_FRAME_NUM, frameNum!!.toInt())

        requestMap.put(Constants.JSON_SCHEDULE_ID, scheduleId)
        requestMap.put(Constants.JSON_SCHEDULE_START_TIME, startTime)
        requestMap.put(Constants.JSON_SCHEDULE_STOP_TIME, stopTime)

        if (rValue is String) {
            requestMap.put(Constants.JSON_SCHEDULE_R_VALUE, Utilities.convertHexStringToInt(rValue as String))
        } else if (rValue is Int) {
            requestMap.put(Constants.JSON_SCHEDULE_R_VALUE, rValue as Int)
        }

        if (gValue is String) {
            requestMap.put(Constants.JSON_SCHEDULE_G_VALUE, Utilities.convertHexStringToInt(gValue as String))
        } else if (rValue is Int) {
            requestMap.put(Constants.JSON_SCHEDULE_G_VALUE, gValue as Int)
        }

        if (bValue is String) {
            requestMap.put(Constants.JSON_SCHEDULE_B_VALUE, Utilities.convertHexStringToInt(bValue as String))
        } else if (rValue is Int) {
            requestMap.put(Constants.JSON_SCHEDULE_B_VALUE, bValue as Int)
        }

        if (wValue is String) {
            requestMap.put(Constants.JSON_SCHEDULE_W_VALUE, Utilities.convertHexStringToInt(wValue as String))
        } else if (rValue is Int) {
            requestMap.put(Constants.JSON_SCHEDULE_W_VALUE, wValue as Int)
        }

        if (cValue is String) {
            requestMap.put(Constants.JSON_SCHEDULE_C_VALUE, Utilities.convertHexStringToInt(cValue as String))
        } else if (rValue is Int) {
            requestMap.put(Constants.JSON_SCHEDULE_C_VALUE, cValue as Int)
        }

        requestMap.put(Constants.JSON_SCHEDULE_BRIGHTNESS_VALUE, brightnessValue)


        return requestMap
    }
}