package com.svarochi.wifi.model.api.request

data class UpdateScheduleRequest(
    val end_time: Long,
    val ids: List<Int>,
    val network_id: Int,
    val project_id: Int,
    val schedule_id: Int,
    val schedule_name: String,
    val schedule_target_type: String,
    val start_time: Long,
    val user_id: String
)