package com.svarochi.wifi.model.api.response

data class AddScheduleResponse(
    val description: String?,
    val schedule_id: Int?,
    val status: String
)