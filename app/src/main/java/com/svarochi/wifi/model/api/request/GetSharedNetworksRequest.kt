package com.svarochi.wifi.model.api.request

data class GetSharedNetworksRequest(
        var profile_id:String,
        var project_id:Int
)