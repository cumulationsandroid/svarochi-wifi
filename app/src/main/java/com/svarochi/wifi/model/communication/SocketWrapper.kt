package com.svarochi.wifi.model.communication

import moe.codeest.rxsocketclient.SocketClient

data class SocketWrapper(var socket:SocketClient, var lastCommandSentTimestamp:Long, var retryCount:Int = 0)