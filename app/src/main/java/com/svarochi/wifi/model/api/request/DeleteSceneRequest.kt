package com.svarochi.wifi.model.api.request

data class DeleteSceneRequest(
    val user_id:String,
    val project_id:Int,
    val network_id:Int,
    val scene_id:Int
)