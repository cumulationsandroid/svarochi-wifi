package com.svarochi.wifi.model.communication

enum class PacketType(val value:String) {
    SET_COLOR("9000"),
    SET_WIFI("9003"),
    GET_IP("4131"),
    RESET_LAMP("9005"),
    SET_SCENE("9001"),
    LIGHT_STATUS("9009"),
    SET_SCHEDULE("9002"),
    GET_FIRMWARE_VERSION("9007")
}