package com.svarochi.wifi.model.common

import com.svarochi.wifi.database.entity.Network
import java.io.Serializable

class CustomNetwork(
        var network: Network
) : Serializable {
    var selected: Boolean = false
}