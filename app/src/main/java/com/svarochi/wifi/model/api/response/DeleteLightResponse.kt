package com.svarochi.wifi.model.api.response

data class DeleteLightResponse(
    val description: String?,
    val status: String
)