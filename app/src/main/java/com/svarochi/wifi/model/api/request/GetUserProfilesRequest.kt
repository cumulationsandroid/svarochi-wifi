package com.svarochi.wifi.model.api.request

data class GetUserProfilesRequest(
        var shared_with:String
)