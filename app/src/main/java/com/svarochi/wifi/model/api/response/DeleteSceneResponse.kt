package com.svarochi.wifi.model.api.response

data class DeleteSceneResponse(
    val description:String?,
    val status:String
)