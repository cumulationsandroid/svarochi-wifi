package com.svarochi.wifi.model.api.response

data class GetGroupsResponse(
        val description: String?,
        val records: List<GroupWithDevices>?,
        val status: String
)

data class GroupWithDevices(
        val group_id: Int,
        val group_name: String,
        val network_id: Int,
        val project_id: Int,
        val user_id: String,
        val devices:List<String>
)