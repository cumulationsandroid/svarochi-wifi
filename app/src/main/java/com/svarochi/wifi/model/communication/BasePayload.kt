package com.svarochi.wifi.model.communication

import java.io.Serializable

open class BasePayload: Serializable {
    open var byteArray:ByteArray = ByteArray(0)
    open var sequenceNumber:Int = 0
}