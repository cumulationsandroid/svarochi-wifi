package com.svarochi.wifi.model.api.response

data class EditLightNameResponse(
    val description: String?,
    val mac_id:String?,
    val status: String
)