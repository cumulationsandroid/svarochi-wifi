package com.svarochi.wifi.model.communication.commands.response

import com.svarochi.wifi.model.communication.BasePayload
import com.svarochi.wifi.model.communication.LampType


class SetWifiCredResponse(
    override var byteArray: ByteArray,
    override var sequenceNumber: Int,
    var macId: String
): BasePayload() {
    var isSuccess:Boolean = false
    var lampType: LampType

    init {
        val responseStringBuilder = StringBuilder()
        for (b1 in byteArray) {
            responseStringBuilder.append(String.format("%02X ", b1))
        }
        var responseString = responseStringBuilder.toString().replace(" ", "", true)
        responseString.replace(" ", "", true)
        isSuccess = responseString.substring(0,2).equals("00")

        if(responseString.substring(2).equals("00")){
            lampType = LampType.BRIGHT_AND_DIM
        }else if(responseString.substring(2).equals("04")){
            lampType = LampType.WARM_AND_COOL
        }else if(responseString.substring(2).equals("06")){
            lampType = LampType.COLOR_AND_DAYLIGHT
        }else{
            lampType = LampType.NONE
        }
    }


}