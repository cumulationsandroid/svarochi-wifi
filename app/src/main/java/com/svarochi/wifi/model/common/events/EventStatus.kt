package com.svarochi.wifi.model.common.events

enum class EventStatus(value:Int) {
    DEFAULT(0),
    LOADING(1),
    SUCCESS(2),
    ERROR(3),
    FAILURE(4),
    COMPLETE(5),
    RESPONSE(6),
    WIFI(7)
}