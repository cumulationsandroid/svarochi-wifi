package com.svarochi.wifi.model.api.request

data class DeleteLightRequest(
    val user_id: String,
    val project_id:Int,
    val network_id:Int,
    val mac_id:String
)