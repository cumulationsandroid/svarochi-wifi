package com.svarochi.wifi.model.api.common

import java.io.Serializable

data class SharedWithData(val user_id: String, val email: String?, val phone: String?) : Serializable
