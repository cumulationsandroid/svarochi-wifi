package com.svarochi.wifi.network

import GetSchedulesResponse
import com.svarochi.wifi.common.Utilities.Companion.isEmail
import com.svarochi.wifi.model.api.common.EndAction
import com.svarochi.wifi.model.api.common.StartAction
import com.svarochi.wifi.model.api.request.*
import com.svarochi.wifi.model.api.response.*
import com.svarochi.wifi.model.common.SharingProject
import io.reactivex.Observable
import retrofit2.Response
import timber.log.Timber

class ApiRequestParser {

    companion object {
        fun getProjects(userId: String): Observable<GetProjectsResponse> {
            var request = GetProjectsRequest(userId)
            Timber.d("Get Projects API request body - ${request.toString()}")

            return ApiClient.getApiServiceInstance().getProjects(request)
        }

        fun addProject(userId: String, projectName: String): Observable<Response<AddProjectResponse>> {
            var request = AddProjectRequest(userId, projectName)
            Timber.d("Add Project API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().addProject(request)
        }

        fun getNetworks(userId: String, projectId: Int): Observable<Response<GetNetworksResponse>> {
            var request = GetNetworksRequest(projectId, userId)
            Timber.d("Get Networks API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getNetworks(request)
        }

        fun getDevices(userId: String, projectId: Int, networkId: Int): Observable<Response<GetDevicesResponse>> {
            var request = GetDevicesRequest(networkId, projectId, userId)
            Timber.d("Get Devices API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getDevices(request)
        }

        fun addGroup(userId: String, macIds: List<String>, groupName: String, networkId: Int, projectId: Int):
                Observable<Response<AddGroupResponse>> {
            var request = AddGroupRequest(macIds, groupName, networkId, projectId, userId)
            Timber.d("Add Group API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().addGroup(request)
        }

        fun addDevice(
                userId: String,
                deviceName: String,
                deviceType: String,
                macId: String,
                networkId: Int,
                projectId: Int
        ): Observable<Response<AddDeviceResponse>> {
            var request = AddDeviceRequest(deviceName, deviceType, macId, networkId, projectId, userId)
            Timber.d("Add Device API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().addDevice(request)
        }

        fun addNetwork(
                userId: String,
                networkName: String,
                projectId: Int,
                ssid: String,
                pw: String,
                authenticationMode: String,
                encryptionType: String
        ):
                Observable<Response<AddNetworkResponse>> {
            var request = AddNetworkRequest(networkName, projectId, userId, ssid, pw, authenticationMode, encryptionType)
            Timber.d("Add Network API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().addNetwork(request)
        }

        fun updateGroup(
                userId: String,
                macIds: List<String>,
                groupId: Int,
                groupName: String,
                networkId: Int,
                projectId: Int
        ):
                Observable<Response<UpdateGroupResponse>> {
            var request = UpdateGroupRequest(macIds, groupId, groupName, networkId, projectId, userId)
            Timber.d("Update Group API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().updateGroup(request)
        }

        fun getGroups(userId: String, networkId: Int, projectId: Int):
                Observable<Response<GetGroupsResponse>> {
            var request = GetGroupsRequest(networkId, projectId, userId)
            Timber.d("Get Groups API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getGroups(request)
        }

        fun getGroupDetails(userId: String, groupId: Int, networkId: Int, projectId: Int):
                Observable<GetGroupDetailsResponse> {
            var request = GetGroupDetailsRequest(groupId, networkId, projectId, userId)
            Timber.d("Get Group details API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getGroupDetails(request)
        }

        fun addScene(
                userId: String,
                sceneDevices: List<SceneDevice>,
                sceneName: String,
                networkId: Int,
                projectId: Int
        ):
                Observable<Response<AddSceneResponse>> {
            var request = AddSceneRequest(networkId, projectId, sceneDevices, sceneName, userId)
            Timber.d("Add Scene API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().addScene(request)
        }

        fun updateScene(
                userId: String,
                deviceStates: List<DeviceState>,
                sceneId: Int,
                sceneName: String,
                networkId: Int,
                projectId: Int
        ):
                Observable<Response<UpdateSceneResponse>> {
            var request = UpdateSceneRequest(deviceStates, networkId, projectId, sceneId, sceneName, userId)
            Timber.d("Update Scene API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().updateScene(request)
        }

        fun getSceneDetails(userId: String, sceneId: Int, networkId: Int, projectId: Int):
                Observable<GetSceneDetailsResponse> {
            var request = GetSceneDetailsRequest(networkId, projectId, sceneId, userId)
            Timber.d("Get Scene Details API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getSceneDetails(request)
        }

        fun getScenes(userId: String, networkId: Int, projectId: Int):
                Observable<Response<GetScenesResponse>> {
            var request = GetScenesRequest(networkId, projectId, userId)
            Timber.d("Get Scenes API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getScenes(request)
        }

        fun deleteLamp(userId: String, projectId: Int, networkId: Int, macId: String): Observable<Response<DeleteLightResponse>> {
            var request = DeleteLightRequest(
                    userId,
                    projectId,
                    networkId,
                    macId
            )
            Timber.d("Delete Light API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().deleteLamp(request)
        }

        fun editNetwork(
                userId: String,
                projectId: Int,
                networkId: Int,
                networkName: String,
                ssid: String,
                pw: String,
                authenticationMode: String,
                encryptionType: String
        ): Observable<Response<EditNetworkResponse>> {
            var request = EditNetworkRequest(
                    userId,
                    projectId,
                    networkId,
                    networkName,
                    ssid,
                    pw,
                    authenticationMode,
                    encryptionType
            )
            Timber.d("Edit Network API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().editNetwork(request)
        }

        fun editProject(
                userId: String,
                projectId: Int,
                projectName: String
        ): Observable<Response<EditProjectResponse>> {
            var request = EditProjectRequest(
                    userId,
                    projectId,
                    projectName
            )
            Timber.d("Edit Project API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().editProject(request)
        }

        fun editLightName(
                userId: String,
                projectId: Int,
                networkId: Int,
                deviceId: String,
                deviceName: String
        ): Observable<Response<EditLightNameResponse>> {
            var request = EditLightNameRequest(
                    userId,
                    projectId,
                    networkId,
                    deviceId,
                    deviceName
            )
            Timber.d("Edit Light Name API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().editLightName(request)
        }

        fun addNewSchedule(
                userId: String,
                projectId: Int,
                networkId: Int,
                scheduleName: String,
                days: ArrayList<String>,
                startTime: String,
                startAction: StartAction,
                endTime: String,
                endAction: EndAction,
                devices: ArrayList<String>,
                groups: ArrayList<Int>
        ): Observable<Response<AddScheduleResponse>> {
            var request = AddScheduleRequest(
                    userId,
                    projectId,
                    networkId,
                    scheduleName,
                    days,
                    startTime,
                    startAction.start_power,
                    startAction.start_red,
                    startAction.start_green,
                    startAction.start_blue,
                    startAction.start_brightness,
                    startAction.start_warm,
                    startAction.start_cool,
                    startAction.start_scene,
                    endTime,
                    endAction.end_power,
                    endAction.end_red,
                    endAction.end_green,
                    endAction.end_blue,
                    endAction.end_brightness,
                    endAction.end_warm,
                    endAction.end_cool,
                    endAction.end_scene,
                    devices,
                    groups
            )
            Timber.d("Add New Schedule API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().addSchedule(request)
        }

        fun getSchedules(userId: String, networkId: Int, projectId: Int): Observable<Response<GetSchedulesResponse>> {
            var request = GetSchedulesRequest(networkId, projectId, userId)
            Timber.d("Get Schedules API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getSchedules(request)
        }

        fun editSchedule(
                userId: String,
                projectId: Int,
                networkId: Int,
                scheduleId: Long,
                scheduleName: String,
                days: ArrayList<String>,
                startTime: String,
                startAction: StartAction,
                endTime: String,
                endAction: EndAction,
                devices: ArrayList<String>,
                groups: ArrayList<Int>
        ): Observable<Response<EditScheduleResponse>> {
            var request = EditScheduleRequest(
                    userId,
                    projectId,
                    networkId,
                    scheduleId.toInt(),
                    scheduleName,
                    days,
                    startTime,
                    startAction.start_power,
                    startAction.start_red,
                    startAction.start_green,
                    startAction.start_blue,
                    startAction.start_brightness,
                    startAction.start_warm,
                    startAction.start_cool,
                    startAction.start_scene,
                    endTime,
                    endAction.end_power,
                    endAction.end_red,
                    endAction.end_green,
                    endAction.end_blue,
                    endAction.end_brightness,
                    endAction.end_warm,
                    endAction.end_cool,
                    endAction.end_scene,
                    devices,
                    groups
            )
            Timber.d("Edit Schedule API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().editSchedule(request)
        }

        fun deleteSchedule(
                userId: String,
                networkId: Int,
                projectId: Int,
                scheduleId: Int
        ): Observable<Response<DeleteScheduleResponse>> {
            var request = DeleteScheduleRequest(
                    networkId,
                    projectId,
                    scheduleId,
                    userId
            )
            Timber.d("Delete Schedule API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().deleteSchedule(request)
        }

        fun deleteGroup(
                userId: String,
                networkId: Int,
                projectId: Int,
                groupId: Int
        ): Observable<Response<DeleteGroupResponse>> {
            var request = DeleteGroupRequest(
                    userId,
                    projectId,
                    networkId,
                    groupId
            )
            Timber.d("Delete Group API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().deleteGroup(request)
        }

        fun deleteScene(
                userId: String,
                networkId: Int,
                projectId: Int,
                sceneId: Int
        ): Observable<Response<DeleteSceneResponse>> {
            var request = DeleteSceneRequest(
                    userId,
                    projectId,
                    networkId,
                    sceneId
            )
            Timber.d("Delete Scene API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().deleteScene(request)
        }

        fun checkUser(
                value: String
        ): Observable<Response<CheckUserResponse>> {
            var email: String? = null
            var phone: String? = null

            if (isEmail(value)) {
                email = value
            } else {
                phone = value
            }
            var request = CheckUserRequest(
                    email, phone
            )
            Timber.d("Check User API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().checkUser(request)
        }

        fun updateProfile(
                profileId: String,
                profileName: String,
                sharedBy: String,
                sharedWith: String,
                shareData: ArrayList<SharingProject>
        ): Observable<Response<UpdateProfileResponse>> {
            var request = UpdateProfileRequest(
                    profileId,
                    profileName,
                    sharedBy,
                    sharedWith,
                    shareData
            )
            Timber.d("Update Profile API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().updateProfile(request)
        }

        fun createProfile(
                profileName: String,
                sharedBy: String,
                sharedWith: String,
                shareData: ArrayList<SharingProject>
        ): Observable<Response<CreateProfileResponse>> {
            var request = CreateProfileRequest(
                    profileName,
                    sharedBy,
                    sharedWith,
                    shareData
            )
            Timber.d("Create Profile API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().createProfile(request)
        }


        fun getSharedProfiles(
                sharedBy: String
        ): Observable<Response<GetSharedProfilesResponse>> {
            var request = GetSharedProfilesRequest(
                    sharedBy
            )
            Timber.d("Get Shared Profiles API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getSharedProfiles(request)
        }

        fun getSharedProjects(profileId: String): Observable<Response<GetSharedProjectsResponse>> {
            var request = GetSharedProjectsRequest(profileId)
            Timber.d("Get Shared Projects API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getSharedProjects(request)
        }

        fun getUserProfiles(sharedWith: String): Observable<Response<GetUserProfilesResponse>> {
            var request = GetUserProfilesRequest(sharedWith)
            Timber.d("Get User Profiles API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getUserProfiles(request)
        }

        fun getSharedDevices(profileId: String, projectId: Int, networkId: Int): Observable<Response<GetDevicesResponse>> {
            var request = GetSharedDevicesRequest(profileId, projectId, networkId)
            Timber.d("Get Shared Devices API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getSharedDevices(request)
        }

        fun getSharedNetworks(profileId: String, projectId: Int): Observable<Response<GetNetworksResponse>> {
            var request = GetSharedNetworksRequest(profileId, projectId)
            Timber.d("Get Shared Networks API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().getSharedNetworks(request)
        }

        fun deleteProject(userId: String, projectId: Int): Observable<Response<DeleteProjectResponse>> {
            var request = DeleteProjectRequest(projectId, userId)
            Timber.d("Delete Project API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().deleteProject(request)
        }

        fun deleteNetwork(userId: String, networkId: Int, projectId: Int): Observable<Response<DeleteNetworkResponse>> {
            var request = DeleteNetworkRequest(userId, networkId, projectId)
            Timber.d("Delete Network API request body - ${request.toString()}")
            return ApiClient.getApiServiceInstance().deleteNetwork(request)
        }
    }
}