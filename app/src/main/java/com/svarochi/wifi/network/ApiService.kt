package com.svarochi.wifi.network

import GetSchedulesResponse
import com.svarochi.wifi.common.Urls.Companion.ADD_DEVICE
import com.svarochi.wifi.common.Urls.Companion.ADD_GROUP
import com.svarochi.wifi.common.Urls.Companion.ADD_NETWORK
import com.svarochi.wifi.common.Urls.Companion.ADD_PROJECT
import com.svarochi.wifi.common.Urls.Companion.ADD_SCENE
import com.svarochi.wifi.common.Urls.Companion.ADD_SCHEDULE
import com.svarochi.wifi.common.Urls.Companion.CHECK_USER
import com.svarochi.wifi.common.Urls.Companion.CREATE_PROFILE
import com.svarochi.wifi.common.Urls.Companion.DELETE_DEVICE
import com.svarochi.wifi.common.Urls.Companion.DELETE_GROUP
import com.svarochi.wifi.common.Urls.Companion.DELETE_NETWORK
import com.svarochi.wifi.common.Urls.Companion.DELETE_PROJECT
import com.svarochi.wifi.common.Urls.Companion.DELETE_SCENE
import com.svarochi.wifi.common.Urls.Companion.DELETE_SCHEDULE
import com.svarochi.wifi.common.Urls.Companion.EDIT_SCHEDULE
import com.svarochi.wifi.common.Urls.Companion.GET_DEVICES
import com.svarochi.wifi.common.Urls.Companion.GET_GROUPS
import com.svarochi.wifi.common.Urls.Companion.GET_GROUP_DETAILS
import com.svarochi.wifi.common.Urls.Companion.GET_NETWORKS
import com.svarochi.wifi.common.Urls.Companion.GET_PROFILES
import com.svarochi.wifi.common.Urls.Companion.GET_PROJECTS
import com.svarochi.wifi.common.Urls.Companion.GET_SCENES
import com.svarochi.wifi.common.Urls.Companion.GET_SCENE_DETAILS
import com.svarochi.wifi.common.Urls.Companion.GET_SCHEDULES
import com.svarochi.wifi.common.Urls.Companion.GET_SHARED_DEVICES
import com.svarochi.wifi.common.Urls.Companion.GET_SHARED_NETWORKS
import com.svarochi.wifi.common.Urls.Companion.GET_SHARED_PROFILES
import com.svarochi.wifi.common.Urls.Companion.GET_SHARED_PROJECTS
import com.svarochi.wifi.common.Urls.Companion.UPDATE_DEVICE
import com.svarochi.wifi.common.Urls.Companion.UPDATE_GROUP
import com.svarochi.wifi.common.Urls.Companion.UPDATE_NETWORK
import com.svarochi.wifi.common.Urls.Companion.UPDATE_PROFILE
import com.svarochi.wifi.common.Urls.Companion.UPDATE_PROJECT
import com.svarochi.wifi.common.Urls.Companion.UPDATE_SCENE
import com.svarochi.wifi.model.api.request.*
import com.svarochi.wifi.model.api.response.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {

    @POST(GET_PROJECTS)
    fun getProjects(@Body request: GetProjectsRequest): Observable<GetProjectsResponse>

    @POST(ADD_PROJECT)
    fun addProject(@Body request: AddProjectRequest): Observable<Response<AddProjectResponse>>

    @POST(GET_NETWORKS)
    fun getNetworks(@Body request: GetNetworksRequest): Observable<Response<GetNetworksResponse>>

    @POST(ADD_NETWORK)
    fun addNetwork(@Body request: AddNetworkRequest): Observable<Response<AddNetworkResponse>>

    @POST(GET_DEVICES)
    fun getDevices(@Body request: GetDevicesRequest): Observable<Response<GetDevicesResponse>>

    @POST(ADD_DEVICE)
    fun addDevice(@Body request: AddDeviceRequest): Observable<Response<AddDeviceResponse>>

    @POST(ADD_GROUP)
    fun addGroup(@Body request: AddGroupRequest): Observable<Response<AddGroupResponse>>

    @POST(UPDATE_GROUP)
    fun updateGroup(@Body request: UpdateGroupRequest): Observable<Response<UpdateGroupResponse>>

    @POST(GET_GROUPS)
    fun getGroups(@Body request: GetGroupsRequest): Observable<Response<GetGroupsResponse>>

    @POST(GET_GROUP_DETAILS)
    fun getGroupDetails(@Body request: GetGroupDetailsRequest): Observable<GetGroupDetailsResponse>

    @POST(ADD_SCENE)
    fun addScene(@Body request: AddSceneRequest): Observable<Response<AddSceneResponse>>

    @POST(UPDATE_SCENE)
    fun updateScene(@Body request: UpdateSceneRequest): Observable<Response<UpdateSceneResponse>>

    @POST(GET_SCENES)
    fun getScenes(@Body request: GetScenesRequest): Observable<Response<GetScenesResponse>>

    @POST(GET_SCENE_DETAILS)
    fun getSceneDetails(@Body request: GetSceneDetailsRequest): Observable<GetSceneDetailsResponse>

    @POST(GET_SCHEDULES)
    fun getSchedules(@Body request: GetSchedulesRequest): Observable<Response<GetSchedulesResponse>>

    @POST(ADD_SCHEDULE)
    fun addSchedule(@Body request: AddScheduleRequest): Observable<Response<AddScheduleResponse>>

    @POST(EDIT_SCHEDULE)
    fun editSchedule(@Body request: EditScheduleRequest): Observable<Response<EditScheduleResponse>>

    @POST(DELETE_SCHEDULE)
    fun deleteSchedule(@Body request: DeleteScheduleRequest): Observable<Response<DeleteScheduleResponse>>

    @POST(DELETE_DEVICE)
    fun deleteLamp(@Body request: DeleteLightRequest): Observable<Response<DeleteLightResponse>>

    @POST(UPDATE_NETWORK)
    fun editNetwork(@Body request: EditNetworkRequest): Observable<Response<EditNetworkResponse>>

    @POST(UPDATE_PROJECT)
    fun editProject(@Body request: EditProjectRequest): Observable<Response<EditProjectResponse>>

    @POST(UPDATE_DEVICE)
    fun editLightName(@Body request: EditLightNameRequest): Observable<Response<EditLightNameResponse>>

    @POST(DELETE_GROUP)
    fun deleteGroup(@Body request: DeleteGroupRequest): Observable<Response<DeleteGroupResponse>>

    @POST(DELETE_SCENE)
    fun deleteScene(@Body request: DeleteSceneRequest): Observable<Response<DeleteSceneResponse>>

    @POST(CHECK_USER)
    fun checkUser(@Body request: CheckUserRequest): Observable<Response<CheckUserResponse>>

    @POST(UPDATE_PROFILE)
    fun updateProfile(@Body request: UpdateProfileRequest): Observable<Response<UpdateProfileResponse>>

    @POST(CREATE_PROFILE)
    fun createProfile(@Body request: CreateProfileRequest): Observable<Response<CreateProfileResponse>>

    @POST(GET_SHARED_PROFILES)
    fun getSharedProfiles(@Body request: GetSharedProfilesRequest): Observable<Response<GetSharedProfilesResponse>>

    @POST(GET_PROFILES)
    fun getUserProfiles(@Body request: GetUserProfilesRequest): Observable<Response<GetUserProfilesResponse>>

    @POST(GET_SHARED_PROJECTS)
    fun getSharedProjects(@Body request: GetSharedProjectsRequest): Observable<Response<GetSharedProjectsResponse>>

    @POST(GET_SHARED_DEVICES)
    fun getSharedDevices(@Body request: GetSharedDevicesRequest): Observable<Response<GetDevicesResponse>>

    @POST(GET_SHARED_NETWORKS)
    fun getSharedNetworks(@Body request: GetSharedNetworksRequest): Observable<Response<GetNetworksResponse>>

    @POST(DELETE_PROJECT)
    fun deleteProject(@Body request: DeleteProjectRequest): Observable<Response<DeleteProjectResponse>>

    @POST(DELETE_NETWORK)
    fun deleteNetwork(@Body request: DeleteNetworkRequest): Observable<Response<DeleteNetworkResponse>>
}