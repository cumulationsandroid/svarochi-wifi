package com.svarochi.wifi.widgets;

public interface IEditTextDialogClickListener {
        void onPositiveClick(String text);
        void onNegativeClick();
}