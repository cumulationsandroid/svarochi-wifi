package com.svarochi.wifi.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.svarochi.wifi.common.FontManager;

import bizbrolly.svarochiapp.R;

import static com.svarochi.wifi.common.FontManager.FONT_BOLD;
import static com.svarochi.wifi.common.FontManager.FONT_BOLD_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_EXTRA_LIGHT;
import static com.svarochi.wifi.common.FontManager.FONT_EXTRA_LIGHT_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_LIGHT;
import static com.svarochi.wifi.common.FontManager.FONT_LIGHT_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_REGULAR;
import static com.svarochi.wifi.common.FontManager.FONT_ROMAN;
import static com.svarochi.wifi.common.FontManager.FONT_SEMI_BOLD;
import static com.svarochi.wifi.common.FontManager.FONT_SEMI_BOLD_ITALIC;

public class AppCheckBox extends androidx.appcompat.widget.AppCompatCheckBox {

    private int font;

    public AppCheckBox(Context context) {
        super(context);
    }

    public AppCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public AppCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AppCheckBox);
            String str = a.getString(R.styleable.AppCheckBox_fontType);
            str = TextUtils.isEmpty(str) ? "1" : str;
            switch (Integer.parseInt(str)) {
                case 1:
                    str = FONT_REGULAR;
                    break;
                case 2:
                    str = FONT_ITALIC;
                    break;
                case 3:
                    str = FONT_BOLD;
                    break;
                case 4:
                    str = FONT_SEMI_BOLD;
                    break;
                case 5:
                    str = FONT_BOLD_ITALIC;
                    break;
                case 6:
                    str = FONT_SEMI_BOLD_ITALIC;
                    break;
                case 7:
                    str = FONT_LIGHT_ITALIC;
                    break;
                case 8:
                    str = FONT_EXTRA_LIGHT_ITALIC;
                    break;
                case 9:
                    str = FONT_LIGHT;
                    break;
                case 10:
                    str = FONT_EXTRA_LIGHT;
                    break;
                default:
                    str = FONT_ROMAN;
                    break;
            }
            setTypeface(FontManager.getInstance(getContext()).loadFont(str));
            a.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
