package com.svarochi.wifi.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;

import androidx.appcompat.widget.AppCompatEditText;

import com.svarochi.wifi.common.FontManager;

import bizbrolly.svarochiapp.R;

import static com.svarochi.wifi.common.FontManager.FONT_BOLD;
import static com.svarochi.wifi.common.FontManager.FONT_BOLD_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_EXTRA_LIGHT;
import static com.svarochi.wifi.common.FontManager.FONT_EXTRA_LIGHT_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_LIGHT;
import static com.svarochi.wifi.common.FontManager.FONT_LIGHT_ITALIC;
import static com.svarochi.wifi.common.FontManager.FONT_REGULAR;
import static com.svarochi.wifi.common.FontManager.FONT_ROMAN;
import static com.svarochi.wifi.common.FontManager.FONT_SEMI_BOLD;
import static com.svarochi.wifi.common.FontManager.FONT_SEMI_BOLD_ITALIC;

public class AppEditText extends AppCompatEditText {

    public AppEditText(Context context) {
        super(context);
    }

    public AppEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public AppEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context, attrs);
    }


    private void initView(Context context, AttributeSet attrs) {
        if (isInEditMode())
            return;
        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AppEditText);
            String str = a.getString(R.styleable.AppEditText_fontType);
            if (str == null)
                str = "1";
            switch (Integer.parseInt(str)) {
                case 1:
                    str = FONT_REGULAR;
                    break;
                case 2:
                    str = FONT_ITALIC;
                    break;
                case 3:
                    str = FONT_BOLD;
                    break;
                case 4:
                    str = FONT_SEMI_BOLD;
                    break;
                case 5:
                    str = FONT_BOLD_ITALIC;
                    break;
                case 6:
                    str = FONT_SEMI_BOLD_ITALIC;
                    break;
                case 7:
                    str = FONT_LIGHT_ITALIC;
                    break;
                case 8:
                    str = FONT_EXTRA_LIGHT_ITALIC;
                    break;
                case 9:
                    str = FONT_LIGHT;
                    break;
                case 10:
                    str = FONT_EXTRA_LIGHT;
                    break;
                case 11:
                    str = FONT_ROMAN;
                    break;
                default:
                    str = FONT_ROMAN;
                    break;
            }
            setTypeface(FontManager.getInstance(getContext()).loadFont(str));

            //Setting compound drawable
/*
            Drawable drawableLeft = null;
            Drawable drawableRight = null;
            Drawable drawableBottom = null;
            Drawable drawableTop = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableLeft = a.getDrawable(R.styleable.CustomEditText_drawableLeftCompat);
                drawableRight = a.getDrawable(R.styleable.CustomEditText_drawableRightCompat);
                drawableBottom = a.getDrawable(R.styleable.CustomEditText_drawableBottomCompat);
                drawableTop = a.getDrawable(R.styleable.CustomEditText_drawableTopCompat);
            } else {
                final int drawableLeftId = a.getResourceId(R.styleable.CustomEditText_drawableLeftCompat, -1);
                final int drawableRightId = a.getResourceId(R.styleable.CustomEditText_drawableRightCompat, -1);
                final int drawableBottomId = a.getResourceId(R.styleable.CustomEditText_drawableBottomCompat, -1);
                final int drawableTopId = a.getResourceId(R.styleable.CustomEditText_drawableTopCompat, -1);

                if (drawableLeftId != -1)
                    drawableLeft = AppCompatResources.getDrawable(context, drawableLeftId);
                if (drawableRightId != -1)
                    drawableRight = AppCompatResources.getDrawable(context, drawableRightId);
                if (drawableBottomId != -1)
                    drawableBottom = AppCompatResources.getDrawable(context, drawableBottomId);
                if (drawableTopId != -1)
                    drawableTop = AppCompatResources.getDrawable(context, drawableTopId);
            }
            setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);*/
            a.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * code to detect back press event when keyboard is open up.
     */
    private KeyImeChangeListener keyImeChangeListenerListener;
    public void setKeyImeChangeListenerListener(KeyImeChangeListener listener)
    {
        keyImeChangeListenerListener = listener;
    }

    public interface KeyImeChangeListener
    {
        void onKeyIme(int keyCode, KeyEvent event);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event)
    {
        if (keyImeChangeListenerListener != null)
        {
            keyImeChangeListenerListener.onKeyIme(keyCode, event);
        }
        return false;
    }

}
