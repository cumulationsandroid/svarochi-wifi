package com.svarochi.wifi.audio.recorder.core

import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.os.Process
import android.util.Log
import com.svarochi.wifi.audio.recorder.callback.AudioRecorderCallback

class WifiAudioRecorder(callback: AudioRecorderCallback) {

    private val audioSource = MediaRecorder.AudioSource.DEFAULT
    private val channelConfig = AudioFormat.CHANNEL_IN_MONO
    private val audioEncoding = AudioFormat.ENCODING_PCM_16BIT
    private val sampleRate = 44100
    private var thread: Thread? = null
    private var callback: AudioRecorderCallback? = callback

    fun setCallback(callback: AudioRecorderCallback) {
        this.callback = callback
    }

    fun start() {
        if (thread != null) return
        thread = Thread(Runnable {
            Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO)

            val minBufferSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioEncoding)
            val recorder = AudioRecord(audioSource, sampleRate, channelConfig, audioEncoding, minBufferSize)

            if (recorder.state == AudioRecord.STATE_UNINITIALIZED) {
                Thread.currentThread().interrupt()
                return@Runnable
            } else {
                Log.i(WifiAudioRecorder::class.java.simpleName, "Started.")
                //callback.onStart();
            }
            val buffer = ByteArray(minBufferSize)
            recorder.startRecording()

            while (thread != null && !thread!!.isInterrupted && recorder.read(buffer, 0, minBufferSize) > 0) {
                callback!!.onBufferAvailable(buffer)
            }
            recorder.stop()
            recorder.release()
        }, WifiAudioRecorder::class.java.name)
        thread!!.start()
    }

    fun stop() {
        if (thread != null) {
            thread!!.interrupt()
            thread = null
        }
    }
}
