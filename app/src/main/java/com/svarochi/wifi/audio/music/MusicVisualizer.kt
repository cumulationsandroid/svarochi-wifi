package com.svarochi.wifi.audio.music

import android.media.MediaPlayer
import android.media.audiofx.Visualizer
import android.util.Log
import com.svarochi.wifi.audio.music.callback.MusicCallback
import com.svarochi.wifi.common.Utilities
import java.util.*

class MusicVisualizer {

    private var mFFTVisualizerBytes: ByteArray? = null
    private var mFFTVisualizer: Visualizer? = null
    private var visualizerReleased: Boolean = true
    private var numberOfThreads = 0
    private var numberOfThreadsExecuted = 0
    private var mMediaPlayer: MediaPlayer? = null
    private var mAudioSessionId = 0
    private var callback: MusicCallback? = null


    fun setPlayer(mediaPlayer: MediaPlayer, audioSessionId: Int) {
        mMediaPlayer = mediaPlayer
        mAudioSessionId = audioSessionId
    }

    fun setFFTVisualizer() {
        numberOfThreads = 0
        numberOfThreadsExecuted = 0
        if (mAudioSessionId == -1) {
            return
        }
        mFFTVisualizer = Visualizer(mAudioSessionId)
        mFFTVisualizer!!.captureSize = Visualizer.getCaptureSizeRange()[1]
        mFFTVisualizer!!.setDataCaptureListener(object : Visualizer.OnDataCaptureListener {
            override fun onWaveFormDataCapture(visualizer: Visualizer, bytes: ByteArray, samplingRate: Int) {
                mFFTVisualizerBytes = bytes
                Thread(Runnable { onFFTDataCapture("onWaveFormDataCapture") }).start()
            }

            override fun onFftDataCapture(visualizer: Visualizer, bytes: ByteArray, samplingRate: Int) {
                mFFTVisualizerBytes = bytes
                //Log.e("BaseVisualizer capture", ++numberOfThreads + "")
                Thread(Runnable {
                    // Log.e("BaseVisualizer exe cap", numberOfThreads + "")
                    onFFTDataCapture("onFftDataCapture")
                    //Log.e("BaseVisualizer executed", ++numberOfThreadsExecuted + "")
                }).start()
                //invalidate();
            }
        }, Visualizer.getMaxCaptureRate() / 2, false, true)

        mFFTVisualizer!!.enabled = true
    }

    private fun onFFTDataCapture(tag: String) {
        try {
            if (mMediaPlayer != null && mMediaPlayer!!.isPlaying) {
                Log.e("BaseVisualizer", tag + " mFFTVisualizerBytes " + Arrays.toString(mFFTVisualizerBytes))
                val into = Utilities.byteArrayToIntArray(mFFTVisualizerBytes!!)
                Log.e("BaseVisualizer", tag + " " + Arrays.toString(into))
                val longo = Utilities.unsignedIntToLong(mFFTVisualizerBytes!!)
                Log.e("BaseVisualizer", "$tag longo $longo")
                val inti = Utilities.unsignedShortToInt(mFFTVisualizerBytes!!)
                Log.e("BaseVisualizer", "$tag inti $inti")
                val byto = Utilities.intArrayToByteArray(into)
                Log.e("BaseVisualizer", tag + " byto " + Arrays.toString(byto))
                if (mFFTVisualizerBytes != null && mFFTVisualizerBytes!!.isNotEmpty()) {
                    Log.e(
                            "BaseVisualizer",
                            tag + " mFFTVisualizerBytes[0] " + Math.abs(mFFTVisualizerBytes!![0].toInt()) + " byto[0] " + Math.abs(
                                    byto[0].toInt()
                            ) + " bytoAv[0] " + Math.abs(byto[0].toInt())
                    )
                    try {
                        var iTByting = 0
                        var iTAvByting = 0
                        for (bByting in mFFTVisualizerBytes!!) {
                            val sByting = bByting.toString()
                            var iByting = Integer.parseInt(sByting)
                            iTByting = iTByting + iByting
                            iByting = Math.abs(iByting)
                            iTAvByting = iTAvByting + iByting
                        }
                        Log.e(
                                "BaseVisualizer",
                                tag + " mFFTVisualizerBytes.length " + mFFTVisualizerBytes!!.size + " byting " + iTByting + " avByting " + iTAvByting
                        )

                        var rgb = Utilities.WaveLengthToRGB(Math.abs(iTByting).toDouble())
                        val byting = mFFTVisualizerBytes!![0].toInt()
                        val intensity = if (byting == 0) 0 else byting + 128
                        if (intensity == 0) {
                            rgb = intArrayOf(0, 0, 0)
                        }
                        /*if (lastIntensity == 0 && intensity == 0) {
                            return
                        }
                        lastIntensity = intensity*/
                        //MusicVisualizerActivity.fireIntensityEvent(intensity)
                        callback!!.setRgbIntensity(rgb, intensity)
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                }
            } else {
                Log.e("BaseVisualizer", tag + "Music is not playing")
                release()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun release() {
        try {
            if (mFFTVisualizer != null) {
                mFFTVisualizer!!.enabled = false
                mFFTVisualizer!!.release()
                visualizerReleased = true
            }
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    fun setCallback(callback: MusicCallback) {
        this.callback = callback
    }
}