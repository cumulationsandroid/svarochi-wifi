package com.svarochi.wifi.audio.music.callback

interface MusicCallback {

    fun setRgbIntensity(rgb:IntArray, intensity:Int)
}