package com.svarochi.wifi.audio.recorder.callback

interface AudioRecorderCallback {
    fun onBufferAvailable(buffer: ByteArray)
}