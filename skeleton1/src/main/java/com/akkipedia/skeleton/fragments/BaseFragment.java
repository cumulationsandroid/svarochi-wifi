package com.akkipedia.skeleton.fragments;

import androidx.fragment.app.Fragment;

/**
 * Created by Akash on 08/12/16.
 */

public abstract class BaseFragment extends Fragment {

    public abstract void onBackPressed();
}
