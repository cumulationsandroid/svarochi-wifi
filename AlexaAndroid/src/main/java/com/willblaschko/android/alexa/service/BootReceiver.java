package com.willblaschko.android.alexa.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.util.Log;

/**
 * @author will on 4/17/2016.
 */
public class BootReceiver extends BroadcastReceiver {

    private static final String TAG = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        //start our service in the background
        try {
            Intent stickyIntent = new Intent(context, DownChannelService.class);
            if (context != null)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(stickyIntent);
                }else{
                    context.startService(stickyIntent);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Started down channel service.");
    }
}
