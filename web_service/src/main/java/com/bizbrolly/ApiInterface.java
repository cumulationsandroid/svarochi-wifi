package com.bizbrolly;


import com.bizbrolly.entities.AddDbDetailsResponse;
import com.bizbrolly.entities.AddDetailsResponse;
import com.bizbrolly.entities.AddProjectResponse;
import com.bizbrolly.entities.AddSchedulerDetailsResponse;
import com.bizbrolly.entities.AppUpdateResponse;
import com.bizbrolly.entities.CommonResponse;
import com.bizbrolly.entities.DeleteProjectResponse;
import com.bizbrolly.entities.DeleteSchedulerDetailsResponse;
import com.bizbrolly.entities.ForgotPasswordResponse;
import com.bizbrolly.entities.GetAssociateUserNetworkResponse;
import com.bizbrolly.entities.GetAssociateUsersResponse;
import com.bizbrolly.entities.GetDbDetailsResponse;
import com.bizbrolly.entities.GetInvitationProfilesResponse;
import com.bizbrolly.entities.GetProjectsResponse;
import com.bizbrolly.entities.GetSchedulerResponse;
import com.bizbrolly.entities.GetUserDetailsResponse;
import com.bizbrolly.entities.InviteUserResponse;
import com.bizbrolly.entities.PushUserDetailsResponse;
import com.bizbrolly.entities.ResendOTPResponse;
import com.bizbrolly.entities.SendTokenResponse;
import com.bizbrolly.entities.SetSecurityKeyResponse;
import com.bizbrolly.entities.UpdateAssociateUserNetworkResponse;
import com.bizbrolly.entities.UpdateProjectResponse;
import com.bizbrolly.entities.UpdateSchedulerDetailsResponse;
import com.bizbrolly.entities.VerifyOTPResponse;
import com.bizbrolly.entities.VerifyTokenResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by Akash on 14/05/17.
 */

public interface ApiInterface {

    @POST(Constants.Partials.GETDBDETAILS)
    Call<GetDbDetailsResponse> getDb(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.VERIFYOTP)
    Call<VerifyOTPResponse> verifyOTP(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.ADDDBDETAILS)
    Call<AddDbDetailsResponse> saveDb(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.ADDDETAILS)
    Call<AddDetailsResponse> createUser(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.GETDETAILS)
    Call<ForgotPasswordResponse> forgotPassword(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.RESENDOTP)
    Call<ResendOTPResponse> resendOTP(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.SENDTOKEN)
    Call<SendTokenResponse> sendToken(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.VERIFYTOKEN)
    Call<VerifyTokenResponse> verifyToken(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.SETSECURITYKEY)
    Call<SetSecurityKeyResponse> setSecurityKey(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.APPFORCEUPDATE)
    Call<AppUpdateResponse> appUpdate(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.SCHEDULER)
    Call<GetSchedulerResponse> getScheduler(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.ADDSCHEDULER)
    Call<AddSchedulerDetailsResponse> addScheduler(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.DELETESCHEDULER)
    Call<DeleteSchedulerDetailsResponse> deleteScheduler(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.UPDATESCHEDULER)
    Call<UpdateSchedulerDetailsResponse> updateScheduler(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.ADD_PROJECT)
    Call<AddProjectResponse> addProject(@Body HashMap<String, Object> body);

    @PUT(Constants.Partials.UPDATE_PROJECT)
    Call<UpdateProjectResponse> updateProject(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.DELETE_PROJECT)
    Call<DeleteProjectResponse> deleteProject(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.GET_PROJECTS)
    Call<GetProjectsResponse> getProjects(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.GET_USER_DETAILS)
    Call<GetUserDetailsResponse> getUserDetails(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.INVITE_USER)
    Call<InviteUserResponse> inviteUser(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.GET_INVITATION_PROFILES)
    Call<GetInvitationProfilesResponse> getInvitationProfiles(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.GET_ASSOCIATE_USERS)
    Call<GetAssociateUsersResponse> getAssociateUsers(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.GET_ASSOCIATE_USER_NETWORK)
    Call<GetAssociateUserNetworkResponse> getAssociateUserNetwork(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.UPDATE_ASSOCIATE_USER_NETWORK)
    Call<UpdateAssociateUserNetworkResponse> updateAssociateUserNetwork(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.PUSH_USER_DETAILS)
    Call<PushUserDetailsResponse> pushUserDetails(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.CHANGE_PASSWORD)
    Call<CommonResponse> changePassword(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.SEND_OTP)
    Call<CommonResponse> sendOtp(@Body HashMap<String, Object> body);

    @POST(Constants.Partials.RESET_PASSWORD)
    Call<CommonResponse> resetPassword(@Body HashMap<String, Object> body);
}
