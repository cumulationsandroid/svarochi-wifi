package com.bizbrolly;

/**
 * Created by Akash on 14/05/17.
 */

public class Constants {
    //static final String BB_LOCAL_URL = "http://bizbrollydev.cloudapp.net/SvarochiAPI_Local/Svarochi.svc/";
    //static final String BB_LIVE_URL = "http://bizbrollydev.cloudapp.net/SvarochiAPI/Svarochi.svc/";
    //static final String IBHAN_PRODUCTION_URL = "http://ibahn.southindia.cloudapp.azure.com:90/SvarochiServices/Svarochi.svc/";
    //static final String LOCAL_URL = "http://192.168.2.39:8587/Svarochi.svc/";
    //static final String STAGING_URL = "http://125.63.92.168:8587/Svarochi.svc/";
    //static final String PS_DEV_LOCAL_URL = "http://192.168.2.39:1111/Svarochi.svc/";
    //static final String PS_PRODUCTION_URL = "http://13.71.18.232:8080/Svarochi.svc/";
    //static final String SVAROCHI_UAT_URL = "http://13.71.18.232:8079/Svarochi.svc/";
    //static final String SVAROCHI_PRODUCTION_WEB_API_URL = "http://13.71.18.232:8078/api/";
    //static final String SVAROCHI_QA_WEB_API_URL = "http://104.211.102.47:8078/api/";

    static final String SVAROCHI_PRODUCTION_URL = "http://www.svarochiapi.com:8087/api/v3/";
    //"http://104.211.78.157:8087/api/";
    //"http://13.71.18.232:8077/api/";
    // "http://svarochiapi.com:8080/Svarochi.svc/";
    static final String SVAROCHI_DEV_URL = "http://104.211.92.227:8087/api/v3/";

    static final String BASE_URL = SVAROCHI_PRODUCTION_URL;

    public static final String TERMS_AND_CONDITIONS = "http://svarochi.com/terms_and_conditions/";
    public static final String PRIVACY_POLICY = "http://svarochi.com/privacy_policy/";
    public static final String ABOUT = "http://svarochi.com/about/";

    static final String API_KEY = "EV-OCT-EVENT-BIZBR-2016OLLY";
    public static final String API_VERSION = "3";
    public static final String APP_VERSION = "V3";

    static class Partials {
        static final String GETDBDETAILS = "Register/GetDBDetails/";
        static final String VERIFYOTP = "Register/VerifyOTP/";
        static final String ADDDBDETAILS = "Register/AddDBDetails/";
        static final String ADDDETAILS = "Register/AddDetails/";
        static final String GETDETAILS = "Register/GetDetails/";
        static final String RESENDOTP = "Register/ResendOTP/";
        static final String SENDTOKEN = "Token/SendToken/";
        static final String SETSECURITYKEY = "Register/SetSecurityKey/";
        static final String VERIFYTOKEN = "Token/VerifyToken/";
        static final String APPFORCEUPDATE = "ForceUpdate/AppForceUpdate/";
        static final String SCHEDULER = "Scheduler/GetScheduler/";
        static final String ADDSCHEDULER = "Scheduler/AddSchedulerDetails";
        static final String DELETESCHEDULER = "Scheduler/DeleteScheduler";
        static final String UPDATESCHEDULER = "Scheduler/UpdateScheduler";
        static final String ADD_PROJECT = "Project/Add";
        static final String UPDATE_PROJECT = "Project/Update";
        static final String DELETE_PROJECT = "Project/Delete";
        static final String GET_PROJECTS = "Project/Get";
        static final String GET_USER_DETAILS = "UserAssociation/GetUserDetails";//Verify Invitee Account before sending an invitation for the profile share
        static final String INVITE_USER = "UserAssociation/InviteUser";//Invite user with the shared profile; This is used as invite and revoke invite by the isActive parameter usages isActive = 1 means invite | isActive = 0 means revoke the invite permission
        static final String GET_INVITATION_PROFILES = "UserAssociation/GetInvitationProfiles";//To see all the invitation which user has received from multiple users as a admin or guest operator
        static final String GET_ASSOCIATE_USERS = "UserAssociation/GetAssociateUsers";//To see list of all the shared networks by the user, shared networks to other user can be as Admin or Guest
        static final String GET_ASSOCIATE_USER_NETWORK = "UserAssociation/GetAssociateUserNetwork";//To check the shared network detail, it behaves like of GetDbDetails for the invitee account
        static final String UPDATE_ASSOCIATE_USER_NETWORK = "UserAssociation/UpdateAssociateUserNetwork";//To check the shared network detail, it behaves like of GetDbDetails for the invitee account
        static final String PUSH_USER_DETAILS = "Register/PushUserDetails/";
        static final String CHANGE_PASSWORD = "Register/ChangePassword/";
        static final String RESET_PASSWORD = "Register/ResetPassword/";
        static final String SEND_OTP = "Token/SendOTP/";
    }

    static class Keys {
        static final String APIKey = "APIKey";
        static final String DBScript = "DBScript";
        static final String UpdatedDBScript = "UpdatedDBScript";
        static final String Email = "Email";
        static final String IsMaster = "IsMaster";
        static final String NetworkPassword = "NetworkPassword";
        static final String OTP = "OTP";
        static final String PhoneNumber = "PhoneNumber";
        static final String EmailOrPhone = "EmailOrPhone";
        static final String SecurityKey = "SecurityKey";
        static final String Version = "Version";
        static final String VersionValue = "VersionValue";
        static final String RoomId = "RoomId";
        static final String SchedulerId = "SchedulerId";
        static final String ScheduleDate = "ScheduleDate";
        static final String ScheduleTime = "ScheduleTime";
        static final String ScheduleHoldTime = "HoldTime";
        static final String ScheduleIndex = "Index";
        static final String ScheduleName = "ScheduleName";
        static final String CmdDetails = "CmdDetails";
        static final String ProjectId = "ProjectId";
        static final String ProjectName = "ProjectName";
        static final String HostEmailId = "HostEmailId";
        static final String InviteeEmailId = "InviteeEmailId";
        static final String IsActive = "IsActive";//0 - to revoke invite permission| 1 - to invite
        static final String AssociationType = "AssociationType";//A - Master/Admin | G - Guest/Slave
        static final String InviteeUserId = "InviteeUserId";
        static final String HostPhoneNumber = "HostPhoneNumber";
        static final String InviteePhoneNumber = "InviteePhoneNumber";
        static final String SharedNetworkName = "SharedNetworkName";
        static final String HostUserId = "HostUserId";
        static final String Phone_Number = "Phone_Number";
        static final String OnBehalfOfUserId = "OnBehalfOfUserId";
        static final String ModifiedDate = "ModifiedDate";
        static final String AssociationId = "AssociationId";
        static final String UserId = "UserId";
        static final String CurrentPassword = "CurrentPassword";
        static final String NewPassword = "NewPassword";
        static final String EmailOrPhoneNumber = "EmailOrPhoneNumber";
        static final String TokenType = "TokenType";
    }
}
