package com.bizbrolly;

import com.bizbrolly.entities.AddDbDetailsResponse;
import com.bizbrolly.entities.AddDetailsResponse;
import com.bizbrolly.entities.AddProjectResponse;
import com.bizbrolly.entities.AddSchedulerDetailsResponse;
import com.bizbrolly.entities.AppUpdateResponse;
import com.bizbrolly.entities.CommonResponse;
import com.bizbrolly.entities.DeleteProjectResponse;
import com.bizbrolly.entities.DeleteSchedulerDetailsResponse;
import com.bizbrolly.entities.ForgotPasswordResponse;
import com.bizbrolly.entities.GetAssociateUserNetworkResponse;
import com.bizbrolly.entities.GetAssociateUsersResponse;
import com.bizbrolly.entities.GetDbDetailsResponse;
import com.bizbrolly.entities.GetInvitationProfilesResponse;
import com.bizbrolly.entities.GetProjectsResponse;
import com.bizbrolly.entities.GetSchedulerResponse;
import com.bizbrolly.entities.GetUserDetailsResponse;
import com.bizbrolly.entities.InviteUserResponse;
import com.bizbrolly.entities.PushUserDetailsResponse;
import com.bizbrolly.entities.ResendOTPResponse;
import com.bizbrolly.entities.SendTokenResponse;
import com.bizbrolly.entities.SetSecurityKeyResponse;
import com.bizbrolly.entities.UpdateAssociateUserNetworkResponse;
import com.bizbrolly.entities.UpdateProjectResponse;
import com.bizbrolly.entities.UpdateSchedulerDetailsResponse;
import com.bizbrolly.entities.VerifyOTPResponse;
import com.bizbrolly.entities.VerifyTokenResponse;

import java.util.HashMap;

import jdk.nashorn.internal.scripts.JS;
import retrofit2.Call;
import retrofit2.Callback;

public class WebServiceRequests {
    private static WebServiceRequests mInstance;
    //private boolean isLive = false;

    public static WebServiceRequests getInstance() {
        if (mInstance == null) {
            mInstance = new WebServiceRequests();
        }
        return mInstance;
    }

    /**
     * Registration
     *
     * @param email
     * @param phone
     * @param securityKey
     * @param networkPassword
     * @param responseCallback
     */
    public void createUser(String email, String phone, String securityKey, String networkPassword, Callback<AddDetailsResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        //params.put(Constants.Keys.SecurityKey, securityKey);
        /*} else {
            params.put(Constants.Keys.Email, email);
        }*/
        params.put(Constants.Keys.NetworkPassword, networkPassword);
        params.put(Constants.Keys.Version, Constants.API_VERSION);

        System.out.println("request createUser: " + Constants.BASE_URL + Constants.Partials.ADDDETAILS + params.toString());

        Call<AddDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).createUser(params);

        call.enqueue(responseCallback);
    }

    /**
     * Resend OTP
     *
     * @param email
     * @param phone
     * @param responseCallback
     */
    public void resendOTP(String email, String phone, Callback<ResendOTPResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);

        System.out.println("request resentOTP: " + Constants.BASE_URL + Constants.Partials.RESENDOTP + params.toString());

        Call<ResendOTPResponse> call = ApiClient.getClient().create(ApiInterface.class).resendOTP(params);

        call.enqueue(responseCallback);
    }

    /**
     * Verify OTP
     *
     * @param OTP
     * @param email
     * @param Phone
     * @param networkPassword
     * @param responseCallback
     */
    public void verifyOTP(String OTP, String email, String Phone, String networkPassword, Callback<VerifyOTPResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.OTP, OTP);
        params.put(Constants.Keys.Email, email);
        //if (!isLive) {
        params.put(Constants.Keys.PhoneNumber, Phone);
        //}
        params.put(Constants.Keys.NetworkPassword, networkPassword);

        System.out.println("request verifyOTP: " + Constants.BASE_URL + Constants.Partials.VERIFYOTP + params.toString());

        Call<VerifyOTPResponse> call = ApiClient.getClient().create(ApiInterface.class).verifyOTP(params);

        call.enqueue(responseCallback);
    }

    /**
     * Forgot Password
     *
     * @param emailOrPhone
     * @param responseCallback
     */
    public void forgotPassword(String emailOrPhone, Callback<ForgotPasswordResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.EmailOrPhone, emailOrPhone);
        /*} else {
            params.put(Constants.Keys.Email, emailOrPhone);
        }*/

        System.out.println("request forgotPassword: " + Constants.BASE_URL + Constants.Partials.GETDETAILS + params.toString());

        Call<ForgotPasswordResponse> call = ApiClient.getClient().create(ApiInterface.class).forgotPassword(params);

        call.enqueue(responseCallback);
    }

    /**
     * Login | Get database from cloud
     *
     * @param emailOrPhone
     * @param securityKey
     * @param networkPassword
     * @param responseCallback
     */
    public void getDbDetails(String emailOrPhone, String securityKey, String networkPassword, Callback<GetDbDetailsResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.EmailOrPhone, emailOrPhone);
        //params.put(Constants.Keys.SecurityKey, securityKey);
        /*} else {
            params.put(Constants.Keys.Email, emailOrPhone);
        }*/
        params.put(Constants.Keys.NetworkPassword, networkPassword);

        System.out.println("request getDbDetails: " + Constants.BASE_URL + Constants.Partials.GETDBDETAILS + params.toString());

        Call<GetDbDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).getDb(params);

        call.enqueue(responseCallback);
    }

    /**
     * Save database to cloud
     *
     * @param emailOrPhone
     * @param dbScript
     * @param responseCallback
     */
    public void addDbDetails(String emailOrPhone, String dbScript, Callback<AddDbDetailsResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.EmailOrPhone, emailOrPhone);
        /*} else {
            params.put(Constants.Keys.Email, emailOrPhone);
        }*/
        params.put(Constants.Keys.DBScript, dbScript);
        params.put(Constants.Keys.Version, Constants.API_VERSION);

        System.out.println("request addDbDetails: " + Constants.BASE_URL + Constants.Partials.ADDDBDETAILS + params.toString());

        Call<AddDbDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).saveDb(params);

        call.enqueue(responseCallback);
    }

    /**
     * Send token
     *
     * @param email
     * @param phone
     * @param password
     * @param responseCallback
     */
    public void sendToken(String email, String phone, String password, Callback<SendTokenResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.NetworkPassword, password);

        System.out.println("request sendToken: " + Constants.BASE_URL + Constants.Partials.SENDTOKEN + params.toString());

        Call<SendTokenResponse> call = ApiClient.getClient().create(ApiInterface.class).sendToken(params);

        call.enqueue(responseCallback);
    }

    /**
     * Verify Token
     *
     * @param email
     * @param phone
     * @param OTP
     * @param responseCallback
     */
    public void verifyToken(String email, String phone, String OTP, Callback<VerifyTokenResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.OTP, OTP);

        System.out.println("request verifyToken: " + Constants.BASE_URL + Constants.Partials.VERIFYTOKEN + params.toString());

        Call<VerifyTokenResponse> call = ApiClient.getClient().create(ApiInterface.class).verifyToken(params);

        call.enqueue(responseCallback);
    }

    /**
     * Set Security Key
     *
     * @param email
     * @param phone
     * @param secretCode
     * @param responseCallback
     */
    public void setSecurityKey(String email, String phone, String secretCode, Callback<SetSecurityKeyResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.SecurityKey, secretCode);

        System.out.println("request setSecurityKey: " + Constants.BASE_URL + Constants.Partials.SETSECURITYKEY + params.toString());

        Call<SetSecurityKeyResponse> call = ApiClient.getClient().create(ApiInterface.class).setSecurityKey(params);

        call.enqueue(responseCallback);
    }

    public void getDbDetails(String emailOrPhone, String securityKey, String networkPassword, boolean isAdmin, Callback<GetDbDetailsResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.EmailOrPhone, emailOrPhone);
        params.put(Constants.Keys.IsMaster, /*isAdmin ? 1 : */0);
        params.put(Constants.Keys.SecurityKey, securityKey);
        params.put(Constants.Keys.NetworkPassword, networkPassword);

        System.out.println("request getDbDetails: " + params.toString());

        Call<GetDbDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).getDb(params);

        call.enqueue(responseCallback);
    }

    /**
     * App Update
     *
     * @param responseCallback
     */
    public void appUpdate(Callback<AppUpdateResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.VersionValue, Constants.APP_VERSION);

        System.out.println("request appUpdate: " + Constants.BASE_URL + Constants.Partials.APPFORCEUPDATE + params.toString());

        Call<AppUpdateResponse> call = ApiClient.getClient().create(ApiInterface.class).appUpdate(params);

        call.enqueue(responseCallback);
    }


    //Avinash schdeuler
    public void getScheduler(String email, String phone, int roomId
            , Callback<GetSchedulerResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.RoomId, roomId);


        System.out.println("request getScheduler: " + Constants.BASE_URL + Constants.Partials.SCHEDULER + params.toString());
        Call<GetSchedulerResponse> call = ApiClient.getClient().create(ApiInterface.class).getScheduler(params);
        call.enqueue(responseCallback);
    }

    //Avinash adding scheduler

    public void addScheduler(String email, String phone, int roomId, String ScheduleDate, String ScheduleTime, String ScheduleName,
                             String ScheduleHoldTime, int ScheduleIndex, String CmdDetails, Callback<AddSchedulerDetailsResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.RoomId, roomId);
        params.put(Constants.Keys.ScheduleDate, ScheduleDate);
        params.put(Constants.Keys.ScheduleTime, ScheduleTime);
        params.put(Constants.Keys.ScheduleHoldTime, ScheduleHoldTime);
        params.put(Constants.Keys.ScheduleIndex, ScheduleIndex);
        params.put(Constants.Keys.ScheduleName, ScheduleName);
        params.put(Constants.Keys.CmdDetails, CmdDetails);


        System.out.println("request getDbDetails: " + Constants.BASE_URL + Constants.Partials.ADDSCHEDULER + params.toString());
        Call<AddSchedulerDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).addScheduler(params);
        call.enqueue(responseCallback);
    }

    //Avinash : delete scheduler
    public void deleteScheduler(String email, String phone, int roomId,
                                int SchedulerId, Callback<DeleteSchedulerDetailsResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.RoomId, roomId);
        params.put(Constants.Keys.SchedulerId, SchedulerId);


        System.out.println("request getDbDetails: " + Constants.BASE_URL + Constants.Partials.DELETESCHEDULER + params.toString());
        Call<DeleteSchedulerDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).deleteScheduler(params);
        call.enqueue(responseCallback);
    }

    //Avinash : update scheduler

    public void updateScheduler(String email, String phone, int roomId, String ScheduleDate, String ScheduleTime, String ScheduleName,
                                int SchedulerId, String ScheduleHoldTime, int ScheduleIndex, String CmdDetails, Callback<UpdateSchedulerDetailsResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.RoomId, roomId);
        params.put(Constants.Keys.ScheduleDate, ScheduleDate);
        params.put(Constants.Keys.ScheduleTime, ScheduleTime);
        params.put(Constants.Keys.ScheduleHoldTime, ScheduleHoldTime);
        params.put(Constants.Keys.ScheduleIndex, ScheduleIndex);
        params.put(Constants.Keys.ScheduleName, ScheduleName);
        params.put(Constants.Keys.SchedulerId, SchedulerId);
        params.put(Constants.Keys.CmdDetails, CmdDetails);

        System.out.println("request getDbDetails: " + Constants.BASE_URL + Constants.Partials.UPDATESCHEDULER + params.toString());
        Call<UpdateSchedulerDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).updateScheduler(params);
        call.enqueue(responseCallback);
    }

    public void addProject(String email, String phone, String projectName, int onBehalfOfUserId, Callback<AddProjectResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.ProjectName, projectName);
        params.put(Constants.Keys.OnBehalfOfUserId, onBehalfOfUserId);

        System.out.println("request addProject: " + Constants.BASE_URL + Constants.Partials.ADD_PROJECT + params.toString());
        Call<AddProjectResponse> call = ApiClient.getClient().create(ApiInterface.class).addProject(params);
        call.enqueue(responseCallback);
    }

    public void updateProject(String email, String phone, int projectId, String projectName, int onBehalfOfUserId, Callback<UpdateProjectResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.ProjectId, projectId);
        params.put(Constants.Keys.ProjectName, projectName);
        params.put(Constants.Keys.OnBehalfOfUserId, onBehalfOfUserId);

        System.out.println("request updateProject: " + Constants.BASE_URL + Constants.Partials.UPDATE_PROJECT + params.toString());
        Call<UpdateProjectResponse> call = ApiClient.getClient().create(ApiInterface.class).updateProject(params);
        call.enqueue(responseCallback);
    }

    public void deleteProject(String email, String phone, int projectId, Callback<DeleteProjectResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);
        params.put(Constants.Keys.ProjectId, projectId);

        System.out.println("request deleteProject: " + Constants.BASE_URL + Constants.Partials.DELETE_PROJECT + params.toString());
        Call<DeleteProjectResponse> call = ApiClient.getClient().create(ApiInterface.class).deleteProject(params);
        call.enqueue(responseCallback);
    }

    public void getProjects(String email, String phone, Callback<GetProjectsResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phone);

        System.out.println("request getProjects: " + Constants.BASE_URL + Constants.Partials.GET_PROJECTS + params.toString());
        Call<GetProjectsResponse> call = ApiClient.getClient().create(ApiInterface.class).getProjects(params);
        call.enqueue(responseCallback);
    }

    public void getUserDetails(String email, String phone, Callback<GetUserDetailsResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.Phone_Number, phone);

        System.out.println("request getUserDetails: " + Constants.BASE_URL + Constants.Partials.GET_USER_DETAILS + params.toString());
        Call<GetUserDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).getUserDetails(params);
        call.enqueue(responseCallback);
    }

    public void inviteUser(String hostEmailId, String hostPhoneNumber, String inviteeEmailId, String inviteePhoneNumber, int isActive, String DBScript, String associationType, String sharedNetworkName, Callback<InviteUserResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.HostEmailId, hostEmailId);//User who is inviting to other user
        params.put(Constants.Keys.HostPhoneNumber, hostPhoneNumber);//User who is inviting to other user
        params.put(Constants.Keys.InviteeEmailId, inviteeEmailId);//User who is getting invitation
        params.put(Constants.Keys.InviteePhoneNumber, inviteePhoneNumber);//User who is getting invitation
        params.put(Constants.Keys.IsActive, isActive);//0 or 1
        //DBScript = DBScript.replace("\"", "\\\"");
        params.put(Constants.Keys.DBScript, DBScript);
        params.put(Constants.Keys.AssociationType, associationType);//M or G
        params.put(Constants.Keys.SharedNetworkName, sharedNetworkName.trim());

        System.out.println("request " + (isActive == 0 ? "revokeUser using inviteUser API: " : "inviteUser: ") + Constants.BASE_URL + Constants.Partials.INVITE_USER + params.toString());
        Call<InviteUserResponse> call = ApiClient.getClient().create(ApiInterface.class).inviteUser(params);
        call.enqueue(responseCallback);
    }

    public void getInvitationProfiles(String hostEmailId, String hostPhoneNumber, Callback<GetInvitationProfilesResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.HostEmailId, hostEmailId);//Logged in user's email
        params.put(Constants.Keys.HostPhoneNumber, hostPhoneNumber);//Logged in user's phone number

        System.out.println("request getInvitationProfiles: " + Constants.BASE_URL + Constants.Partials.GET_INVITATION_PROFILES + params.toString());
        Call<GetInvitationProfilesResponse> call = ApiClient.getClient().create(ApiInterface.class).getInvitationProfiles(params);
        call.enqueue(responseCallback);
    }

    public void getAssociateUsers(String hostEmailId, String hostPhoneNumber, Callback<GetAssociateUsersResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.HostEmailId, hostEmailId);//Logged in user's email
        params.put(Constants.Keys.HostPhoneNumber, hostPhoneNumber);//Logged in user's phone number

        System.out.println("request getAssociateUsers: " + Constants.BASE_URL + Constants.Partials.GET_ASSOCIATE_USERS + params.toString());
        Call<GetAssociateUsersResponse> call = ApiClient.getClient().create(ApiInterface.class).getAssociateUsers(params);
        call.enqueue(responseCallback);
    }

    public void getAssociateUserNetwork(int hostUserId, int inviteeUserId, String associationType, Callback<GetAssociateUserNetworkResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.HostUserId, hostUserId);//User that shared this profile network
        params.put(Constants.Keys.InviteeUserId, inviteeUserId);//User who received this profile network
        params.put(Constants.Keys.AssociationType, associationType);

        System.out.println("request getAssociateUserNetwork: " + Constants.BASE_URL + Constants.Partials.GET_ASSOCIATE_USER_NETWORK + params.toString());
        Call<GetAssociateUserNetworkResponse> call = ApiClient.getClient().create(ApiInterface.class).getAssociateUserNetwork(params);
        call.enqueue(responseCallback);
    }

    public void updateAssociateUserNetwork(int hostUserId, int inviteeUserId, String associationType, String dbScript, String modifiedDate, int associationId, Callback<UpdateAssociateUserNetworkResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.HostUserId, hostUserId);//User that shared this profile network
        params.put(Constants.Keys.InviteeUserId, inviteeUserId);//User who received this profile network
        params.put(Constants.Keys.AssociationType, associationType);
        params.put(Constants.Keys.DBScript, dbScript);
        params.put(Constants.Keys.ModifiedDate, modifiedDate);
        params.put(Constants.Keys.AssociationId, associationId);

        System.out.println("request updateAssociateUserNetwork: " + Constants.BASE_URL + Constants.Partials.UPDATE_ASSOCIATE_USER_NETWORK + params.toString());
        Call<UpdateAssociateUserNetworkResponse> call = ApiClient.getClient().create(ApiInterface.class).updateAssociateUserNetwork(params);
        call.enqueue(responseCallback);
    }

    /**
     * Used this API due to Azure VM crash database tables are lost and this is the one way to recover users data by pushing code to the cloud before doing anything
     * This logic is with the combination of stop users to getDBDetails call without saving anything over cloud as we restored database for 8th Dec 2018
     * where database crash happen on 6th April 2019 so users activities within these 4 months are not on cloud these are limited to the users app cache
     * now motive is to recover that app cache to the cloud by pushing complete json to the server
     * One more thing if user is not there in this old database backup but it was really there after 8th Dec 2018's database then we are trying creat new row in database for that user as a new user registration
     * with complete cache network in his/her phone.
     *
     * @param email
     * @param phoneNumber
     * @param password
     * @param dbScript
     * @param responseCallback
     */
    public void pushUserDetails(String email, String phoneNumber, String password,
                                String dbScript, Callback<PushUserDetailsResponse> responseCallback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        params.put(Constants.Keys.Email, email);
        params.put(Constants.Keys.PhoneNumber, phoneNumber);
        params.put(Constants.Keys.NetworkPassword, password);
        //params.put(Constants.Keys.SecurityKey, null);//We don't have user's security key so sending it static and with next release we are going to remove this secret key logic from app so no worries
        params.put(Constants.Keys.UpdatedDBScript, dbScript);
        params.put(Constants.Keys.DBScript, dbScript);//Not in use just passwing
        params.put(Constants.Keys.Version, Constants.API_VERSION);

        System.out.println("request pushUserDetails: " + Constants.BASE_URL + Constants.Partials.PUSH_USER_DETAILS + params.toString());

        Call<PushUserDetailsResponse> call = ApiClient.getClient().create(ApiInterface.class).pushUserDetails(params);

        call.enqueue(responseCallback);
    }

    /**
     * Change password
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @param responseCallback
     */
    public void changePassword(int userId, String oldPassword, String newPassword, Callback<CommonResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.UserId, userId);
        params.put(Constants.Keys.CurrentPassword, oldPassword);
        params.put(Constants.Keys.NewPassword, newPassword);
        /*} else {
            params.put(Constants.Keys.Email, email);
        }*/
        System.out.println("request changePassword: " + Constants.BASE_URL + Constants.Partials.CHANGE_PASSWORD + params.toString());

        Call<CommonResponse> call = ApiClient.getClient().create(ApiInterface.class).changePassword(params);

        call.enqueue(responseCallback);
    }

    /**
     * Send Otp
     *
     * @param emailOrPhoneNumber
     * @param tokenType
     * @param responseCallback
     */
    public void sendOtp(String emailOrPhoneNumber, int tokenType, Callback<CommonResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.EmailOrPhoneNumber, emailOrPhoneNumber);
        params.put(Constants.Keys.TokenType, tokenType);
        /*} else {
            params.put(Constants.Keys.Email, email);
        }*/
        System.out.println("request sendOtp: " + Constants.BASE_URL + Constants.Partials.SEND_OTP + params.toString());

        Call<CommonResponse> call = ApiClient.getClient().create(ApiInterface.class).sendOtp(params);

        call.enqueue(responseCallback);
    }

    /**
     * Reset password
     *
     * @param emailOrPhoneNumber
     * @param otp
     * @param newPassword
     * @param responseCallback
     */
    public void resetPassword(String emailOrPhoneNumber, String otp, String newPassword, Callback<CommonResponse> responseCallback) {

        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.Keys.APIKey, Constants.API_KEY);
        //if (!isLive) {
        params.put(Constants.Keys.EmailOrPhoneNumber, emailOrPhoneNumber);
        params.put(Constants.Keys.OTP, otp);
        params.put(Constants.Keys.NewPassword, newPassword);
        /*} else {
            params.put(Constants.Keys.Email, email);
        }*/
        System.out.println("request resetPassword: " + Constants.BASE_URL + Constants.Partials.RESET_PASSWORD + params.toString());

        Call<CommonResponse> call = ApiClient.getClient().create(ApiInterface.class).resetPassword(params);

        call.enqueue(responseCallback);
    }

}
