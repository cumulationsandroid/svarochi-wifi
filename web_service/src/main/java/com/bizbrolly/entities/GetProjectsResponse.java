package com.bizbrolly.entities;

import java.io.Serializable;
import java.util.List;

public class GetProjectsResponse {

    private GetProjectsResponseBean projectResponse;

    public void setProjectsResponse(GetProjectsResponseBean projectResponse) {
        this.projectResponse = projectResponse;
    }

    public GetProjectsResponseBean getProjectsResponse() {
        return projectResponse;
    }

    @Override
    public String toString() {
        return "userAssociationResponse: " + (projectResponse != null ? projectResponse.toString() : "null");
    }

    public static class GetProjectsResponseBean {

        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;
        private List<DataEntity> ResponseData;

        public void setResponseData(List<DataEntity> ResponseData) {
            this.ResponseData = ResponseData;
        }

        public List<DataEntity> getResponseData() {
            return ResponseData;
        }

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        @Override
        public String toString() {
            return "ResponseData: " + (ResponseData != null ? ResponseData.toString() : "null") + " ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage;
        }

        public static class DataEntity implements Serializable {

            private int ProjectId;
            private String ProjectName;
            private int UserId;
            private String InsertDate;
            private String InsertBy;
            private boolean IsActive;
            private String ModifiedDate;
            private String ModifiedBy;
            private int OnBehalfOfUserId;

            public int getProjectId() {
                return ProjectId;
            }

            public void setProjectId(int projectId) {
                ProjectId = projectId;
            }

            public String getProjectName() {
                return ProjectName;
            }

            public void setProjectName(String projectName) {
                ProjectName = projectName;
            }

            public int getUserId() {
                return UserId;
            }

            public void setUserId(int userId) {
                UserId = userId;
            }

            public String getInsertDate() {
                return InsertDate;
            }

            public void setInsertDate(String insertDate) {
                InsertDate = insertDate;
            }

            public String getInsertBy() {
                return InsertBy;
            }

            public void setInsertBy(String insertBy) {
                InsertBy = insertBy;
            }

            public boolean isActive() {
                return IsActive;
            }

            public void setActive(boolean active) {
                IsActive = active;
            }

            public String getModifiedDate() {
                return ModifiedDate;
            }

            public void setModifiedDate(String modifiedDate) {
                ModifiedDate = modifiedDate;
            }

            public String getModifiedBy() {
                return ModifiedBy;
            }

            public void setModifiedBy(String modifiedBy) {
                ModifiedBy = modifiedBy;
            }

            public int getOnBehalfOfUserId() {
                return OnBehalfOfUserId;
            }

            public void setOnBehalfOfUserId(int onBehalfOfUserId) {
                OnBehalfOfUserId = onBehalfOfUserId;
            }

            @Override
            public String toString() {
                return "ProjectId: " + ProjectId + " ProjectName: " + ProjectName + " UserId: " + UserId
                        + " InsertDate: " + InsertDate + " IsActive: " + IsActive
                        + " ModifiedDate: " + ModifiedDate + " ModifiedBy: " + ModifiedBy
                        + " OnBehalfOfUserId: " + OnBehalfOfUserId;
            }
        }
    }

}
