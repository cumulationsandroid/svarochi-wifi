package com.bizbrolly.entities;

/**
 * Created by AyushDev on 9/14/2017.
 */

public class AppUpdateResponse {

    /*{
        "AppForceUpdateResult": {
        "IsForceUpdate": true,
                "VersionValue": "V2"
    }
    }*/

    private AppForceUpdateResultBean AppForceUpdateResult;

    public AppForceUpdateResultBean getAppForceUpdateResult() {
        return AppForceUpdateResult;
    }

    public void setAppForceUpdateResult(AppForceUpdateResultBean AppForceUpdateResult) {
        this.AppForceUpdateResult = AppForceUpdateResult;
    }

    @Override
    public String toString() {
        return "AppForceUpdateResult: " + (AppForceUpdateResult != null ? AppForceUpdateResult.toString() : "null");
    }

    public static class AppForceUpdateResultBean {

        private boolean IsForceUpdate;
        private boolean IsIgnore;
        private String VersionValue;

        public boolean isForceUpdate() {
            return IsForceUpdate;
        }

        public void setForceUpdate(boolean forceUpdate) {
            IsForceUpdate = forceUpdate;
        }

        public String getVersionValue() {
            return VersionValue;
        }

        public void setVersionValue(String versionValue) {
            VersionValue = versionValue;
        }

        @Override
        public String toString() {
            return "IsForceUpdate: " + IsForceUpdate + " IsIgnore: " + IsIgnore + " VersionValue: " + VersionValue;
        }

        public boolean isIgnore() {
            return IsIgnore;
        }

        public void setIgnore(boolean ignore) {
            IsIgnore = ignore;
        }
    }
}
