package com.bizbrolly.entities;

import java.io.Serializable;
import java.util.List;

public class GetAssociateUsersResponse {

    private GetAssociateUsersResponseBean userAssociationResponse;

    public void setAssociateUsersResponse(GetAssociateUsersResponseBean userAssociationResponse) {
        this.userAssociationResponse = userAssociationResponse;
    }

    public GetAssociateUsersResponseBean getAssociateUsersResponse() {
        return userAssociationResponse;
    }

    @Override
    public String toString() {
        return "userAssociationResponse: " + (userAssociationResponse != null ? userAssociationResponse.toString() : "null");
    }

    public static class GetAssociateUsersResponseBean {

        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;
        private List<DataEntity> ResponseData;

        public void setResponseData(List<DataEntity> ResponseData) {
            this.ResponseData = ResponseData;
        }

        public List<DataEntity> getResponseData() {
            return ResponseData;
        }

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        @Override
        public String toString() {
            return "ResponseData: " + (ResponseData != null ? ResponseData.toString() : "null") + " ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage;
        }

        public static class DataEntity implements Serializable {

            private String HostEmailId;
            private String HostPhoneNumber;
            private String InviteeEmailId;
            private String InviteePhoneNumber;
            private String AssociationType;
            private boolean IsActive;
            private String DbScript;
            private String SharedNetworkName;
            private int HostUserId;
            private int InviteeUserId;

            public String getHostEmailId() {
                return HostEmailId;
            }

            public void setHostEmailId(String hostEmailId) {
                HostEmailId = hostEmailId;
            }

            public String getHostPhoneNumber() {
                return HostPhoneNumber;
            }

            public void setHostPhoneNumber(String hostPhoneNumber) {
                HostPhoneNumber = hostPhoneNumber;
            }

            public String getInviteeEmailId() {
                return InviteeEmailId;
            }

            public void setInviteeEmailId(String inviteeEmailId) {
                InviteeEmailId = inviteeEmailId;
            }

            public String getInviteePhoneNumber() {
                return InviteePhoneNumber;
            }

            public void setInviteePhoneNumber(String inviteePhoneNumber) {
                InviteePhoneNumber = inviteePhoneNumber;
            }

            public String getAssociationType() {
                return AssociationType;
            }

            public void setAssociationType(String associationType) {
                AssociationType = associationType;
            }

            public boolean isActive() {
                return IsActive;
            }

            public void setActive(boolean active) {
                IsActive = active;
            }

            public String getDbScript() {
                return DbScript;
            }

            public void setDbScript(String dbScript) {
                DbScript = dbScript;
            }

            public String getSharedNetworkName() {
                return SharedNetworkName;
            }

            public void setSharedNetworkName(String sharedNetworkName) {
                SharedNetworkName = sharedNetworkName;
            }

            public int getHostUserId() {
                return HostUserId;
            }

            public void setHostUserId(int hostUserId) {
                HostUserId = hostUserId;
            }

            public int getInviteeUserId() {
                return InviteeUserId;
            }

            public void setInviteeUserId(int inviteeUserId) {
                InviteeUserId = inviteeUserId;
            }

            @Override
            public String toString() {
                return "HostEmailId: " + HostEmailId + " HostPhoneNumber: " + HostPhoneNumber + " InviteeEmailId: " + InviteeEmailId
                        + " InviteePhoneNumber: " + InviteePhoneNumber + " AssociationType: " + AssociationType
                        + " IsActive: " + IsActive + " DbScript: " + DbScript + " SharedNetworkName: " + SharedNetworkName
                        + " HostUserId: " + HostUserId + " InviteeUserId: " + InviteeUserId;
            }
        }
    }

}
