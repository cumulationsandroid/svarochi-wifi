package com.bizbrolly.entities;

public class GetAssociateUserNetworkResponse {

    private GetAssociateUserNetworkResponseBean userAssociationResponse;

    public void setAssociateUserNetworkResponseBean(GetAssociateUserNetworkResponseBean userAssociationResponse) {
        this.userAssociationResponse = userAssociationResponse;
    }

    public GetAssociateUserNetworkResponseBean getAssociateUserNetworkResponseBean() {
        return userAssociationResponse;
    }

    /*@Override
    public String toString() {
        return "AddProjectResult: " + (projectResponse != null ? projectResponse.toString() : "null");
    }*/

    public static class GetAssociateUserNetworkResponseBean {
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;
        private ResponseDataResponseBean ResponseData;

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        public ResponseDataResponseBean getResponseData() {
            return ResponseData;
        }

        public void setResponseData(ResponseDataResponseBean responseData) {
            this.ResponseData = responseData;
        }

        /*@Override
        public String toString() {
            return "ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage + " ResponseData: " + (ResponseData != null ? ResponseData.toString() : "null");
        }*/
    }

    public static class ResponseDataResponseBean {
        private String HostEmailId;
        private String HostPhoneNumber;
        private String InviteeEmailId;
        private String InviteePhoneNumber;
        private String AssociationType;
        private boolean IsActive;
        private String DbScript;
        private String SharedNetworkName;
        private int HostUserId;
        private int InviteeUserId;
        private String ModifiedDate;
        private int AssociationId;

        public String getHostEmailId() {
            return HostEmailId;
        }

        public void setHostEmailId(String hostEmailId) {
            HostEmailId = hostEmailId;
        }

        public String getHostPhoneNumber() {
            return HostPhoneNumber;
        }

        public void setHostPhoneNumber(String hostPhoneNumber) {
            HostPhoneNumber = hostPhoneNumber;
        }

        public String getInviteeEmailId() {
            return InviteeEmailId;
        }

        public void setInviteeEmailId(String inviteeEmailId) {
            InviteeEmailId = inviteeEmailId;
        }

        public String getInviteePhoneNumber() {
            return InviteePhoneNumber;
        }

        public void setInviteePhoneNumber(String inviteePhoneNumber) {
            InviteePhoneNumber = inviteePhoneNumber;
        }

        public String getAssociationType() {
            return AssociationType;
        }

        public void setAssociationType(String associationType) {
            AssociationType = associationType;
        }

        public boolean isActive() {
            return IsActive;
        }

        public void setActive(boolean active) {
            IsActive = active;
        }

        public String getDbScript() {
            return DbScript;
        }

        public void setDbScript(String dbScript) {
            DbScript = dbScript;
        }

        public String getSharedNetworkName() {
            return SharedNetworkName;
        }

        public void setSharedNetworkName(String sharedNetworkName) {
            SharedNetworkName = sharedNetworkName;
        }

        public int getHostUserId() {
            return HostUserId;
        }

        public void setHostUserId(int hostUserId) {
            HostUserId = hostUserId;
        }

        public int getInviteeUserId() {
            return InviteeUserId;
        }

        public void setInviteeUserId(int inviteeUserId) {
            InviteeUserId = inviteeUserId;
        }

        public String getModifiedDate() {
            return ModifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            ModifiedDate = modifiedDate;
        }

        public int getAssociationId() {
            return AssociationId;
        }

        public void setAssociationId(int associationId) {
            AssociationId = associationId;
        }

        @Override
        public String toString() {
            return " HostEmailId: " + HostEmailId + " InviteeEmailId: " + InviteeEmailId
                    + " HostPhoneNumber: " + HostPhoneNumber + " InviteePhoneNumber: " + InviteePhoneNumber
                    + " AssociationType: " + AssociationType + " IsActive: " + IsActive
                    + " SharedNetworkName: " + SharedNetworkName + " HostUserId: " + HostUserId
                    + " InviteeUserId: " + InviteeUserId + " ModifiedDate: " + ModifiedDate
                    + " AssociationId: " + AssociationId + " DbScript: " + DbScript;
        }
    }
}
