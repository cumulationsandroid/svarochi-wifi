package com.bizbrolly.entities;

/**
 * Created by Akash on 26/06/17.
 */

public class ResendOTPResponse {

    /*{
        "GetDBDetailsResult": {
        "Data": {
            "__type": "clsdetails:#Svarochi.Model",
                    "BulbId": "",
                    "DBScript": null,
                    "Email": "ps01email+123@gmail.com",
                    "Id": 103094,
                    "NetworkPassword": "123456789",
                    "OTP": null,
                    "PhoneNumber": "8511604678",
                    "SecurityKey": "qwerty"
        },
        "ErrorDetail": {
            "ErrorDetails": "",
                    "ErrorMessage": ""
        },
        "Result": true
    }
    }*/

    private ResendOTPResultBean GetDBDetailsResult;

    public ResendOTPResultBean getGetDBDetailsResult() {
        return GetDBDetailsResult;
    }

    public void setGetDBDetailsResult(ResendOTPResultBean ResendOTPResult) {
        this.GetDBDetailsResult = ResendOTPResult;
    }

    @Override
    public String toString() {
        return "resendOTPResult: " + (GetDBDetailsResult != null ? GetDBDetailsResult.toString() : "null");
    }

    public static class ResendOTPResultBean {
        private DataEntity Data;

        /**
         * ErrorDetails :
         * ErrorMessage :
         */
        private ErrorDetailBean ErrorDetail;

        private boolean Result;

        public DataEntity getData() {
            return Data;
        }

        public void setData(DataEntity Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        @Override
        public String toString() {
            return "Data: " + (Data != null ? Data.toString() : "null") + " ErrorDetail: " + (ErrorDetail != null ? ErrorDetail.toString() : "null") + " Result: " + Result;
        }

        public static class DataEntity {
            /**
             "Data": {
             "__type": "clsdetails:#Svarochi.Model",
             "BulbId": "",
             "DBScript": null,
             "Email": "ps01email+123@gmail.com",
             "Id": 103094,
             "NetworkPassword": "123456789",
             "OTP": null,
             "PhoneNumber": "8511604678",
             "SecurityKey": "qwerty"
             }
             */

            private String __type;
            private String BulbId;
            private String DBScript;
            private String Email;
            private int Id;
            private String NetworkPassword;
            private String OTP;
            private String PhoneNumber;
            private String SecurityKey;

            public String getOTP() {
                return OTP;
            }

            public void setOTP(String OTP) {
                this.OTP = OTP;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public void setBulbId(String BulbId) {
                this.BulbId = BulbId;
            }

            public void setDBScript(String DBScript) {
                this.DBScript = DBScript;
            }

            public void setEmail(String Email) {
                this.Email = Email;
            }

            public void setId(int Id) {
                this.Id = Id;
            }

            public void setNetworkPassword(String NetworkPassword) {
                this.NetworkPassword = NetworkPassword;
            }

            public String get__type() {
                return __type;
            }

            public String getBulbId() {
                return BulbId;
            }

            public String getDBScript() {
                return DBScript;
            }

            public String getEmail() {
                return Email;
            }

            public int getId() {
                return Id;
            }

            public String getNetworkPassword() {
                return NetworkPassword;
            }

            public String getPhoneNumber() {
                return PhoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                PhoneNumber = phoneNumber;
            }

            public String getSecurityKey() {
                return SecurityKey;
            }

            public void setSecurityKey(String securityKey) {
                SecurityKey = securityKey;
            }

            @Override
            public String toString() {
                return "__type: " + __type + " BulbId: " + BulbId + " DBScript: " + DBScript + " Email: " + Email + " Id: " + Id
                        + " NetworkPassword: " + NetworkPassword + " PhoneNumber: " + PhoneNumber + " SecurityKey: " + SecurityKey;
            }
        }

        public static class ErrorDetailBean {
            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }

            @Override
            public String toString() {
                return "ErrorDetails: " + ErrorDetails + " ErrorMessage:" + ErrorMessage;
            }
        }
    }
}
