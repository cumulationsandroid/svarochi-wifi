package com.bizbrolly.entities;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Akash on 19/05/17.
 */

public class GetSchedulerResponse {

    private GetSchedulerResultEntity schedulerResponse;

    public void setSchedulerResponse(GetSchedulerResultEntity schedulerResponse) {
        this.schedulerResponse = schedulerResponse;
    }

    public GetSchedulerResultEntity getSchedulerResponse() {
        return schedulerResponse;
    }

    @Override
    public String toString() {
        return "schedulerResponse: " + (schedulerResponse != null ? schedulerResponse.toString() : "null");
    }

    public static class GetSchedulerResultEntity {

        private List<DataEntity> ResponseData;
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;

        public void setResponseData(List<DataEntity> ResponseData) {
            this.ResponseData = ResponseData;
        }

        public List<DataEntity> getResponseData() {
            return ResponseData;
        }

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        public static class DataEntity implements Serializable {

            private int UserId;
            private int RoomId;
            private int SchedulerId;
            private String ScheduleDate;
            private String ScheduleTime;
            private String ScheduleName;
            private long HoldTime;
            private int Index;
            private String CmdDetails;
            private int Count = 5;
            private boolean isSwitchOn;

            public int getUserId() {
                return UserId;
            }

            public void setUserId(int UserId) {
                this.UserId = UserId;
            }

            public int getRoomId() {
                return RoomId;
            }

            public void setRoomId(int RoomId) {
                this.RoomId = RoomId;
            }

            public int getSchedulerId() {
                return SchedulerId;
            }

            public void setSchedulerId(int SchedulerId) {
                this.SchedulerId = SchedulerId;
            }

            public String getScheduleDate() {
                return ScheduleDate;
            }

            public void setScheduleDate(String ScheduleDate) {
                this.ScheduleDate = ScheduleDate;
            }

            public String getScheduleTime() {
                return ScheduleTime;
            }

            public void setScheduleTime(String ScheduleTime) {
                this.ScheduleTime = ScheduleTime;
            }

            public String getScheduleName() {
                return ScheduleName;
            }

            public void setScheduleName(String ScheduleName) {
                this.ScheduleName = ScheduleName;
            }

            public String getCmdDetails() {
                return CmdDetails;
            }

            public void setCmdDetails(String CmdDetails) {
                this.CmdDetails = CmdDetails;
            }

            public int getCount() {
                return Count;
            }

            public void setCount(int Count) {
                this.Count = Count;
            }

            @Override
            public String toString() {
                return "UserId: " + UserId + " RoomId: " + RoomId + " SchedulerId: " + SchedulerId + " ScheduleDate: " + ScheduleDate + " ScheduleTime: " + ScheduleTime
                        + " ScheduleName: " + ScheduleName + " CmdDetails: " + CmdDetails + " Count: " + Count+ " Index: " + Index;
            }

            public long getScheduleHoldTime() {
                return HoldTime;
            }

            public void setScheduleHoldTime(long scheduleHoldTime) {
                HoldTime = scheduleHoldTime;
            }

            public int getIndex() {
                return Index;
            }

            public void setIndex(int index) {
                Index = index;
            }

            public boolean isSwitchOn() {
                return isSwitchOn;
            }

            public void setSwitchOn(boolean switchOn) {
                isSwitchOn = switchOn;
            }
        }

        @Override
        public String toString() {
            return "ResponseData: " + (ResponseData != null ? ResponseData.toString() : "null") + " ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage;
        }
    }
}
