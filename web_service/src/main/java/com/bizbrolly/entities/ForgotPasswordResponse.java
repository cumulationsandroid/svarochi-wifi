package com.bizbrolly.entities;

/**
 * Created by AyushDev on 9/14/2017.
 */

public class ForgotPasswordResponse {

    /**
     * GetDBDetailsResult : {"Data":"An email with Network Password has been sent to your email address","ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private GetDetailsResultBean GetDBDetailsResult;

    public GetDetailsResultBean getGetDBDetailsResult() {
        return GetDBDetailsResult;
    }

    public void setGetDBDetailsResult(GetDetailsResultBean GetDetailsResult) {
        this.GetDBDetailsResult = GetDetailsResult;
    }

    @Override
    public String toString() {
        return "GetDBDetailsResult: " + (GetDBDetailsResult != null ? GetDBDetailsResult.toString() : "null");
    }

    public static class GetDetailsResultBean {
        /**
         * Data : An email with Network Password has been sent to your email address
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private String Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public String getData() {
            return Data;
        }

        public void setData(String Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        @Override
        public String toString() {
            return "Data: " + Data + " ErrorDetail: " + (ErrorDetail != null ? ErrorDetail.toString() : "null") + " Result: " + Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }

            @Override
            public String toString() {
                return "ErrorDetails: " + ErrorDetails + " ErrorMessage:" + ErrorMessage;
            }
        }
    }
}
