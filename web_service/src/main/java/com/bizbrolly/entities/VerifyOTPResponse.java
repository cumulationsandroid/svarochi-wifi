package com.bizbrolly.entities;

/**
 * Created by hp on 8/22/2017.
 */

public class VerifyOTPResponse {

    /**
     * GetDBDetailsResult : {"Data":{"__type":"clsdetails:#Svarochi.Model","BulbId":"","DBScript":"","Email":"kshitij.mudgal@bizbrolly.com","Id":210,"NetworkPassword":"123456","OTP":"7610"},"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private VerifyOTPResultEntity GetDBDetailsResult;

    public VerifyOTPResultEntity getGetDBDetailsResult() {
        return GetDBDetailsResult;
    }

    public void setGetDBDetailsResult(VerifyOTPResultEntity VerifyOTPResult) {
        this.GetDBDetailsResult = VerifyOTPResult;
    }

    @Override
    public String toString() {
        return "GetDBDetailsResult: " + (GetDBDetailsResult != null ? GetDBDetailsResult.toString() : "null");
    }

    /*public static class VerifyOTPResultEntity {
        *//**
         * Data : {"__type":"clsdetails:#Svarochi.Model","BulbId":"","DBScript":"","Email":"kshitij.mudgal@bizbrolly.com","Id":210,"NetworkPassword":"123456","OTP":"7610"}
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         *//*

        private boolean Data;
        private ErrorDetailEntity ErrorDetail;
        private boolean Result;

        public boolean getData() {
            return Data;
        }

        public void setData(boolean Data) {
            this.Data = Data;
        }

        public ErrorDetailEntity getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailEntity ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        @Override
        public String toString() {
            return "Data: " + Data + " ErrorDetail: " + (ErrorDetail != null ? ErrorDetail.toString() : "null") + " Result: " + Result;
        }


        public static class ErrorDetailEntity {
            *//**
             * ErrorDetails :
             * ErrorMessage :
             *//*

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }

            @Override
            public String toString() {
                return "ErrorDetails: " + ErrorDetails + " ErrorMessage:" + ErrorMessage;
            }
        }
    }*/

    public static class VerifyOTPResultEntity {
        //private DataEntity Data;
        private ErrorDetailEntity ErrorDetail;
        private boolean Result;

        /*public DataEntity getData() {
            return Data;
        }*/

        /*public void setData(DataEntity Data) {
            this.Data = Data;
        }*/

        public ErrorDetailEntity getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailEntity ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        @Override
        public String toString() {
            return /*"Data: " + (Data != null ? Data.toString() : "null") +*/ " ErrorDetail: " + (ErrorDetail != null ? ErrorDetail.toString() : "null") + " Result: " + Result;
        }

        public static class DataEntity {
            /*"Data": {
                "__type": "clsdetails:#Svarochi.Model",
                        "BulbId": "1",
                        "DBScript": null,
                        "Email": "puresoftware12+982@gmail.com",
                        "Id": 103492,
                        "NetworkPassword": "123456",
                        "OTP": "",
                        "PhoneNumber": "",
                        "ResponseStatus": null,
                        "SecurityKey": null,
                        "Version": null,
                        "VersionName": null
            }*/
            private String __type;
            private String BulbId;
            private String DBScript;
            private String Email;
            private int Id;
            private String NetworkPassword;
            private String OTP;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public String getBulbId() {
                return BulbId;
            }

            public void setBulbId(String BulbId) {
                this.BulbId = BulbId;
            }

            public String getDBScript() {
                return DBScript;
            }

            public void setDBScript(String DBScript) {
                this.DBScript = DBScript;
            }

            public String getEmail() {
                return Email;
            }

            public void setEmail(String Email) {
                this.Email = Email;
            }

            public int getId() {
                return Id;
            }

            public void setId(int Id) {
                this.Id = Id;
            }

            public String getNetworkPassword() {
                return NetworkPassword;
            }

            public void setNetworkPassword(String NetworkPassword) {
                this.NetworkPassword = NetworkPassword;
            }

            public String getOTP() {
                return OTP;
            }

            public void setOTP(String OTP) {
                this.OTP = OTP;
            }

            @Override
            public String toString() {
                return "__type: " + __type + " BulbId: " + BulbId + " DBScript: " + DBScript + " Email: " + Email + " Id: " + Id
                        + " NetworkPassword: " + NetworkPassword + " OTP: " + OTP;
            }
        }

        public static class ErrorDetailEntity {

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }

            @Override
            public String toString() {
                return "ErrorDetails: " + ErrorDetails + " ErrorMessage:" + ErrorMessage;
            }
        }
    }
}
