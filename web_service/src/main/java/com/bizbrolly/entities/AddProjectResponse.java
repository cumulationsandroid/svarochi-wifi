package com.bizbrolly.entities;

public class AddProjectResponse {

    private AddProjectResultBean projectResponse;

    public void setAddProjectResult(AddProjectResultBean projectResponse) {
        this.projectResponse = projectResponse;
    }

    public AddProjectResultBean getAddProjectResult() {
        return projectResponse;
    }

    /*@Override
    public String toString() {
        return "AddProjectResult: " + (projectResponse != null ? projectResponse.toString() : "null");
    }*/

    public static class AddProjectResultBean {
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;
        private ResponseDataResultBean ResponseData;

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        public ResponseDataResultBean getResponseData() {
            return ResponseData;
        }

        public void setResponseData(ResponseDataResultBean responseData) {
            this.ResponseData = responseData;
        }

        /*@Override
        public String toString() {
            return "ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage + " ResponseData: " + (ResponseData != null ? ResponseData.toString() : "null");
        }*/
    }

    public static class ResponseDataResultBean {
        private int ProjectId;
        private String ProjectName;

        public int getProjectId() {
            return ProjectId;
        }

        public void setProjectId(int projectId) {
            this.ProjectId = projectId;
        }

        public String getProjectName() {
            return ProjectName;
        }

        public void setProjectName(String projectName) {
            this.ProjectName = projectName;
        }

        /*@Override
        public String toString() {
            return " ProjectId: " + ProjectId + " ProjectName: " + ProjectName;
        }*/
    }
}
