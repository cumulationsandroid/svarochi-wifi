package com.bizbrolly.entities;

/**
 * Created by Akash on 19/05/17.
 */

public class AddSchedulerDetailsResponse {




    private AddSchedulerDetailsResultBean schedulerResponse;

    public void setAddSchedulerDetailsResult(AddSchedulerDetailsResultBean schedulerResponse) {
        this.schedulerResponse = schedulerResponse;
    }

    public AddSchedulerDetailsResultBean getAddSchedulerDetailsResult() {
        return schedulerResponse;
    }

    @Override
    public String toString() {
        return "AddSchedulerDetailsResult: " + (schedulerResponse != null ? schedulerResponse.toString() : "null");
    }

    public static class AddSchedulerDetailsResultBean {
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        @Override
        public String toString() {
            return " ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage;
        }
    }
}
