package com.bizbrolly.entities;

public class DeleteProjectResponse {

    private DeleteProjectResultBean projectResponse;

    public void setDeleteProjectResult(DeleteProjectResultBean projectResponse) {
        this.projectResponse = projectResponse;
    }

    public DeleteProjectResultBean getDeleteProjectResult() {
        return projectResponse;
    }

    @Override
    public String toString() {
        return "DeleteProjectResult: " + (projectResponse != null ? projectResponse.toString() : "null");
    }

    public static class DeleteProjectResultBean {
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        @Override
        public String toString() {
            return "ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage ;
        }
    }

}
