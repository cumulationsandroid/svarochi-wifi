package com.bizbrolly.entities;

public class InviteUserResponse {

    private InviteUserResponseBean userAssociationResponse;

    public void setInviteUserResponse(InviteUserResponseBean userAssociationResponse) {
        this.userAssociationResponse = userAssociationResponse;
    }

    public InviteUserResponseBean getInviteUserResponse() {
        return userAssociationResponse;
    }

    @Override
    public String toString() {
        return "InviteUserResponse: " + (userAssociationResponse != null ? userAssociationResponse.toString() : "null");
    }

    public static class InviteUserResponseBean {
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        @Override
        public String toString() {
            return "ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage;
        }
    }

}
