package com.bizbrolly.entities;

public class UpdateProjectResponse {

    private UpdateProjectResultBean projectResponse;

    public void setUpdateProjectResult(UpdateProjectResultBean projectResponse) {
        this.projectResponse = projectResponse;
    }

    public UpdateProjectResultBean getUpdateProjectResult() {
        return projectResponse;
    }

    @Override
    public String toString() {
        return "UpdateProjectResult: " + (projectResponse != null ? projectResponse.toString() : "null");
    }

    public static class UpdateProjectResultBean {
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        @Override
        public String toString() {
            return "ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage ;
        }
    }

}
