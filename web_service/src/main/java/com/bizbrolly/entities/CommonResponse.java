package com.bizbrolly.entities;

public class CommonResponse {
    /*{
        "GetDBDetailsResult":{
        "Result":false,
                "ErrorDetail":{
            "ErrorMessage":"Invalid UserId or Password",
                    "ErrorDetails":"Invalid UserId or Password"
        }
    }
    }*/

    private CommonResponseBean GetDBDetailsResult;

    public CommonResponseBean getDBDetailsResult() {
        return GetDBDetailsResult;
    }

    public void setDBDetailsResult(CommonResponseBean GetDBDetailsResult) {
        this.GetDBDetailsResult = GetDBDetailsResult;
    }

    @Override
    public String toString() {
        return "GetDBDetailsResult: " + (GetDBDetailsResult != null ? GetDBDetailsResult.toString() : "null");
    }

    public static class CommonResponseBean {
        private boolean Result;
        private ErrorDetailBean ErrorDetail;

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        @Override
        public String toString() {
            return "ErrorDetail: " + (ErrorDetail != null ? ErrorDetail.toString() : "null") + " Result: " + Result;
        }

        public static class ErrorDetailBean {
            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }

            @Override
            public String toString() {
                return "ErrorDetails: " + ErrorDetails + " ErrorMessage:" + ErrorMessage;
            }
        }
    }
}
