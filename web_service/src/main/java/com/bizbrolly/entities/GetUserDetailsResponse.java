package com.bizbrolly.entities;

import java.io.Serializable;

public class GetUserDetailsResponse {

    private GetUserDetailsResponseBean userAssociationResponse;

    public void setUserDetailsResponseBean(GetUserDetailsResponseBean userAssociationResponse) {
        this.userAssociationResponse = userAssociationResponse;
    }

    public GetUserDetailsResponseBean getUserDetailsResponseBean() {
        return userAssociationResponse;
    }

    /*@Override
    public String toString() {
        return "AddProjectResult: " + (projectResponse != null ? projectResponse.toString() : "null");
    }*/

    public static class GetUserDetailsResponseBean {
        private int ResponseStatusCode;
        private String ResponseStatus;
        private String ResponseMessage;
        private ResponseDataResponseBean ResponseData;

        public int getResponseStatusCode() {
            return ResponseStatusCode;
        }

        public void setResponseStatusCode(int ResponseStatusCode) {
            this.ResponseStatusCode = ResponseStatusCode;
        }

        public String getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(String ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        public ResponseDataResponseBean getResponseData() {
            return ResponseData;
        }

        public void setResponseData(ResponseDataResponseBean responseData) {
            this.ResponseData = responseData;
        }

        /*@Override
        public String toString() {
            return "ResponseStatusCode: " + ResponseStatusCode + " ResponseStatus: " + ResponseStatus + " ResponseMessage: " + ResponseMessage + " ResponseData: " + (ResponseData != null ? ResponseData.toString() : "null");
        }*/
    }

    public static class ResponseDataResponseBean implements Serializable {
        private int id;
        private String Email;
        private String Phone_Number;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPhone_Number() {
            return Phone_Number;
        }

        public void setPhone_Number(String phone_Number) {
            Phone_Number = phone_Number;
        }

        /*@Override
        public String toString() {
            return " id: " + id + " Email: " + Email+ " Phone_Number: " + Phone_Number;
        }*/
    }
}
