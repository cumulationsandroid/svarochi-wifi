package com.bizbrolly.entities;

/**
 * Created by AyushDev on 9/14/2017.
 */

public class SetSecurityKeyResponse {

    /*{
        "GetDBDetailsResult": {
        "Data": true,
                "ErrorDetail": {
            "ErrorDetails": "",
                    "ErrorMessage": ""
        },
        "Result": true,
                "UserVersion": null,
                "VersionName": null
    }
    }*/

    private SetSecurityKeyResultBean GetDBDetailsResult;

    public SetSecurityKeyResultBean getGetDBDetailsResult() {
        return GetDBDetailsResult;
    }

    public void setGetDBDetailsResult(SetSecurityKeyResultBean SetSecurityKeyResult) {
        this.GetDBDetailsResult = SetSecurityKeyResult;
    }

    @Override
    public String toString() {
        return "GetDBDetailsResult: " + (GetDBDetailsResult != null ? GetDBDetailsResult.toString() : "null");
    }

    public static class SetSecurityKeyResultBean {

        private boolean Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public boolean getData() {
            return Data;
        }

        public void setData(boolean Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        @Override
        public String toString() {
            return "Data: " + Data + " ErrorDetail: " + (ErrorDetail != null ? ErrorDetail.toString() : "null") + " Result: " + Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }

            @Override
            public String toString() {
                return "ErrorDetails: " + ErrorDetails + " ErrorMessage:" + ErrorMessage;
            }
        }
    }
}
