package com.ablanco.imageprovider

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix

/**
 * Created by Álvaro Blanco Cabrero on 16/09/2018.
 * ImageProvider.
 */
internal class GalleryImageSource(private val activity: Activity) : ImageProviderSource {

    private val requestHandler: RequestHandler by lazy {
        RequestHandler()
    }

    override fun getImage(callback: (Bitmap?) -> Unit) {
        val intent = Intent(Intent.ACTION_PICK).setType("image/*")
        requestHandler.startForResult(activity, intent) { result, data ->
            callback(if (result == Activity.RESULT_OK) onImageResult(data) else null)
        }
    }

    private fun onImageResult(data: Intent?): Bitmap? {
        try {
            var bitmap = activity.contentResolver.openFileDescriptor(data?.data, "r")?.use {
                BitmapFactory.decodeFileDescriptor(it.fileDescriptor)

            }

            bitmap = bitmap?.let { image ->
                /*Check image orientation and rotate it to 0 degree orientation*/
                data?.data.let {
                    ExifInterfaceHelper.fromUri(activity, it!!)?.let {
                        val matrix = Matrix().apply {
                            postRotate(it.orientation.toFloat())
                        }
                        image.applyMatrix(matrix)
                    } ?: image
                }
            }
            return bitmap

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

}