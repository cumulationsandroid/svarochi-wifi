package com.ablanco.imageprovider

import android.content.Context
import android.net.Uri
import androidx.exifinterface.media.ExifInterface
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream

/**
 * Created by Álvaro Blanco Cabrero on 16/09/2018.
 * ImageProvider.
 */
internal class ExifInterfaceHelper private constructor(inputStream: InputStream) {

    private var exifInterface: androidx.exifinterface.media.ExifInterface? = null

    companion object {
        fun fromFile(file: File): ExifInterfaceHelper? =
            try {
                ExifInterfaceHelper(FileInputStream(file))
            } catch (e: Exception) {
                null
            }

        fun fromUri(context: Context, uri: Uri): ExifInterfaceHelper? =
            try {
                ExifInterfaceHelper(context.contentResolver.openInputStream(uri))
            } catch (e: Exception) {
                null
            }
    }

    init {
        try {
            exifInterface = androidx.exifinterface.media.ExifInterface(inputStream)
        } catch (e: IOException) {
            // Handle any errors
        } finally {
            try {
                inputStream.close()
            } catch (ignored: IOException) {
            }
        }
    }

    val orientation: Int
        get() {
            val orientation = exifInterface?.getAttributeInt(
                androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION,
                androidx.exifinterface.media.ExifInterface.ORIENTATION_NORMAL
            )
            return when (orientation) {
                androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_90 -> 90
                androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_180 -> 180
                androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_270 -> 270
                else -> 0
            }
        }

    val isPortrait: Boolean
        get() = orientation == androidx.exifinterface.media.ExifInterface.ORIENTATION_NORMAL
}